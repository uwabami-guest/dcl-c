/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__0 = 0;
static doublereal c_b7 = 2.;
static integer c__100 = 100;
static real c_b15 = 0.f;
static real c_b16 = 1.f;
static real c_b19 = .2f;
static integer c__300 = 300;
static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static real c_b34 = -1.f;
static real c_b38 = .1f;
static real c_b39 = .9f;
static real c_b53 = .5f;

/* Main program */ int MAIN__(void)
{
    /* System generated locals */
    integer i__1;
    doublereal d__1, d__2;

    /* Builtin functions */
    double pow_dd(doublereal *, doublereal *), exp(doublereal);
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static real b[10000]	/* was [100][100] */, g[10000]	/* was [100][
	    100] */;
    static integer i__, j;
    static real r__[10000]	/* was [100][100] */, bc[10000]	/* was [100][
	    100] */, gc[10000]	/* was [100][100] */;
    static integer ib[256];
    static real cl[256];
    static integer ig[256];
    static real rc[10000]	/* was [100][100] */;
    static integer ir[256];
    static real rx, ry, wx, wy;
    static integer ix1, iy1, nu1, nv1, nu2, nv2, ix2, iy2, iws, image[300];
    extern /* Subroutine */ int grcls_(void), grfrm_(void), gropn_(integer *);
    static real vxmin, vymin, vxmax, vymax;
    extern /* Subroutine */ int uipd3z_(real *, real *, real *, integer *, 
	    integer *, integer *, integer *, integer *), stfpr2_(real *, real 
	    *, real *, real *), uifpac_(integer *, integer *, integer *, 
	    integer *), prccls_(char *, ftnlen), msgdmp_(char *, char *, char 
	    *, ftnlen, ftnlen, ftnlen);
    static integer iwidth;
    extern /* Subroutine */ int uiscmp_(integer *, integer *, integer *, 
	    integer *), prcopn_(char *, ftnlen), grswnd_(real *, real *, real 
	    *, real *), grstrf_(void), rnorml_(real *, real *, integer *, 
	    integer *, real *, real *), swfint_(real *, real *, integer *, 
	    integer *), swiset_(char *, integer *, ftnlen), grstrn_(integer *)
	    , uxaxdv_(char *, real *, real *, ftnlen), sgpwsn_(void), uyaxdv_(
	    char *, real *, real *, ftnlen), sgqvpt_(real *, real *, real *, 
	    real *), grsvpt_(real *, real *, real *, real *), stfwtr_(real *, 
	    real *, real *, real *);

    /* Fortran I/O blocks */
    static cilist io___13 = { 0, 6, 0, 0, 0 };
    static cilist io___14 = { 0, 5, 0, 0, 0 };


    for (i__ = 1; i__ <= 256; ++i__) {
	cl[i__ - 1] = (i__ - 1) * 1.f / 255;
	i__1 = 256 - i__;
	uifpac_(&i__1, &c__0, &c__0, &ir[i__ - 1]);
	i__1 = 256 - i__;
	uifpac_(&c__0, &i__1, &c__0, &ig[i__ - 1]);
	i__1 = 256 - i__;
	uifpac_(&c__0, &c__0, &i__1, &ib[i__ - 1]);
    }
    for (i__ = 1; i__ <= 100; ++i__) {
	for (j = 1; j <= 100; ++j) {
	    d__1 = (doublereal) (i__ - 50.f);
	    d__2 = (doublereal) (j - 50.f);
	    r__[i__ + j * 100 - 101] = exp(-(pow_dd(&d__1, &c_b7) + pow_dd(&
		    d__2, &c_b7)) / 5e3f);
	    d__1 = (doublereal) (i__ - 90.909090909090907f);
	    d__2 = (doublereal) (j - 83.333333333333343f);
	    g[i__ + j * 100 - 101] = exp(-(pow_dd(&d__1, &c_b7) + pow_dd(&
		    d__2, &c_b7)) / 1e3f);
	    d__1 = (doublereal) (i__ - 33.333333333333336f);
	    d__2 = (doublereal) (j - 20.f);
	    b[i__ + j * 100 - 101] = exp(-(pow_dd(&d__1, &c_b7) + pow_dd(&
		    d__2, &c_b7)) / 3e3f);
	}
    }
    rnorml_(r__, rc, &c__100, &c__100, &c_b15, &c_b16);
    rnorml_(g, gc, &c__100, &c__100, &c_b19, &c_b16);
    rnorml_(b, bc, &c__100, &c__100, &c_b15, &c_b16);
    swiset_("WINDOW_HEIGHT", &c__300, (ftnlen)13);
    swiset_("WINDOW_WIDTH", &c__300, (ftnlen)12);
    s_wsle(&io___13);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I) ? ;", (ftnlen)23);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___14);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    gropn_(&iws);
    grfrm_();
    grswnd_(&c_b34, &c_b16, &c_b34, &c_b16);
    grsvpt_(&c_b38, &c_b39, &c_b38, &c_b39);
    grstrn_(&c__1);
    grstrf_();
    uiscmp_(ig, &ig[255], ib, &ib[255]);
    prcopn_("DclPaintGrid3", (ftnlen)13);
    if (nu1 != nv1 || nu2 != nv2) {
	msgdmp_("E", "DclPaintGrid2", "Size of arrays are not consistent.", (
		ftnlen)1, (ftnlen)13, (ftnlen)34);
    }
    sgqvpt_(&vxmin, &vxmax, &vymin, &vymax);
    stfpr2_(&vxmin, &vymin, &rx, &ry);
    stfwtr_(&rx, &ry, &wx, &wy);
    swfint_(&wx, &wy, &ix1, &iy1);
    stfpr2_(&vxmax, &vymax, &rx, &ry);
    stfwtr_(&rx, &ry, &wx, &wy);
    swfint_(&wx, &wy, &ix2, &iy2);
    uipd3z_(rc, gc, bc, &c__100, &c__100, &c__100, image, &iwidth);
    prccls_("DclPaintGrid3", (ftnlen)13);
    uxaxdv_("T", &c_b38, &c_b53, (ftnlen)1);
    uxaxdv_("B", &c_b38, &c_b53, (ftnlen)1);
    uyaxdv_("L", &c_b38, &c_b53, (ftnlen)1);
    uyaxdv_("R", &c_b38, &c_b53, (ftnlen)1);
    grcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int uipk03_ () { MAIN__ (); return 0; }

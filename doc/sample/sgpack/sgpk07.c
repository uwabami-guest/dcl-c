/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static real c_b8 = .05f;
static real c_b9 = .95f;
static integer c__2 = 2;
static logical c_true = TRUE_;
static real c_b16 = .5f;
static integer c__152 = 152;
static integer c__153 = 153;
static integer c__154 = 154;
static integer c__155 = 155;
static integer c__156 = 156;
static integer c__157 = 157;
static integer c__158 = 158;
static integer c__159 = 159;
static integer c__160 = 160;
static integer c__161 = 161;
static integer c__10 = 10;
static integer c__130 = 130;
static integer c__131 = 131;
static integer c__135 = 135;
static integer c__138 = 138;
static integer c__141 = 141;
static integer c__143 = 143;
static integer c__145 = 145;
static integer c__148 = 148;
static integer c__150 = 150;
static integer c__151 = 151;
static integer c__189 = 189;
static integer c__190 = 190;
static integer c__191 = 191;
static integer c__192 = 192;
static integer c__193 = 193;
static integer c__210 = 210;
static integer c__211 = 211;
static integer c__212 = 212;
static integer c__217 = 217;
static integer c__218 = 218;

/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* System generated locals */
    address a__1[10];
    integer i__1[10];
    char ch__1[3], ch__2[3], ch__3[3], ch__4[3], ch__5[3], ch__6[3], ch__7[3],
	     ch__8[3], ch__9[3], ch__10[3];

    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen);

    /* Local variables */
    static integer i__;
    static real y[9], x1, x2, xc;
    static integer iws;
    extern /* Character */ VOID usgi_(char *, ftnlen, integer *);
    extern /* Subroutine */ int sgcls_(void), sgfrm_(void), sgopn_(integer *),
	     sglnv_(real *, real *, real *, real *), sgtxv_(real *, real *, 
	    char *, ftnlen);
    static char greek1[10], greek2[10];
    extern /* Subroutine */ int sgiset_(char *, integer *, ftnlen), sgslni_(
	    integer *), sglset_(char *, logical *, ftnlen);
    static char symbol[10];
    extern /* Subroutine */ int sgrset_(char *, real *, ftnlen), sgpwsn_(void)
	    , sgstxi_(integer *), sgstxs_(real *);

    /* Fortran I/O blocks */
    static cilist io___1 = { 0, 6, 0, 0, 0 };
    static cilist io___2 = { 0, 5, 0, 0, 0 };


    s_wsle(&io___1);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___2);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    sgopn_(&iws);
    sgfrm_();
    x1 = .1f;
    x2 = .9f;
    xc = .5f;
    sgslni_(&c__1);
    for (i__ = 1; i__ <= 9; ++i__) {
	y[i__ - 1] = (10 - i__) * .1f;
	sglnv_(&x1, &y[i__ - 1], &x2, &y[i__ - 1]);
/* L10: */
    }
    sglnv_(&xc, &c_b8, &xc, &c_b9);
/* ----------------------- SUPER/SUB SCRIPT ------------------------------ */
    sgtxv_(&xc, y, "SGTXV|SUP\"RST_SUB\"", (ftnlen)18);
    sgstxi_(&c__2);
    sglset_("LCNTL", &c_true, (ftnlen)5);
/* <-- */
    sgtxv_(&xc, &y[1], "SGTXV|SUP\"RST_SUB\"", (ftnlen)18);
    sgrset_("SHIFT", &c_b16, (ftnlen)5);
/* <-- */
    sgrset_("SMALL", &c_b16, (ftnlen)5);
/* <-- */
    sgtxv_(&xc, &y[2], "SGTXV|SUP\"RST_SUB\"", (ftnlen)18);
/* ------------------------ FONT SELECTION ------------------------------- */
    sgstxi_(&c__1);
    sgstxs_(&c_b8);
    sgtxv_(&xc, &y[3], "ABCDEFG abcdefg", (ftnlen)15);
    sgiset_("IFONT", &c__2, (ftnlen)5);
/* <-- */
    sgtxv_(&xc, &y[4], "ABCDEFG abcdefg", (ftnlen)15);
    sgstxi_(&c__3);
    sgtxv_(&xc, &y[5], "ABCDEFG abcdefg", (ftnlen)15);
/* ------------------------- GREEK LETTERS ------------------------------- */
/* Writing concatenation */
    usgi_(ch__1, (ftnlen)3, &c__152);
    i__1[0] = 3, a__1[0] = ch__1;
    usgi_(ch__2, (ftnlen)3, &c__153);
    i__1[1] = 3, a__1[1] = ch__2;
    usgi_(ch__3, (ftnlen)3, &c__154);
    i__1[2] = 3, a__1[2] = ch__3;
    usgi_(ch__4, (ftnlen)3, &c__155);
    i__1[3] = 3, a__1[3] = ch__4;
    usgi_(ch__5, (ftnlen)3, &c__156);
    i__1[4] = 3, a__1[4] = ch__5;
    usgi_(ch__6, (ftnlen)3, &c__157);
    i__1[5] = 3, a__1[5] = ch__6;
    usgi_(ch__7, (ftnlen)3, &c__158);
    i__1[6] = 3, a__1[6] = ch__7;
    usgi_(ch__8, (ftnlen)3, &c__159);
    i__1[7] = 3, a__1[7] = ch__8;
    usgi_(ch__9, (ftnlen)3, &c__160);
    i__1[8] = 3, a__1[8] = ch__9;
    usgi_(ch__10, (ftnlen)3, &c__161);
    i__1[9] = 3, a__1[9] = ch__10;
    s_cat(greek1, a__1, i__1, &c__10, (ftnlen)10);
/* Writing concatenation */
    usgi_(ch__1, (ftnlen)3, &c__130);
    i__1[0] = 3, a__1[0] = ch__1;
    usgi_(ch__2, (ftnlen)3, &c__131);
    i__1[1] = 3, a__1[1] = ch__2;
    usgi_(ch__3, (ftnlen)3, &c__135);
    i__1[2] = 3, a__1[2] = ch__3;
    usgi_(ch__4, (ftnlen)3, &c__138);
    i__1[3] = 3, a__1[3] = ch__4;
    usgi_(ch__5, (ftnlen)3, &c__141);
    i__1[4] = 3, a__1[4] = ch__5;
    usgi_(ch__6, (ftnlen)3, &c__143);
    i__1[5] = 3, a__1[5] = ch__6;
    usgi_(ch__7, (ftnlen)3, &c__145);
    i__1[6] = 3, a__1[6] = ch__7;
    usgi_(ch__8, (ftnlen)3, &c__148);
    i__1[7] = 3, a__1[7] = ch__8;
    usgi_(ch__9, (ftnlen)3, &c__150);
    i__1[8] = 3, a__1[8] = ch__9;
    usgi_(ch__10, (ftnlen)3, &c__151);
    i__1[9] = 3, a__1[9] = ch__10;
    s_cat(greek2, a__1, i__1, &c__10, (ftnlen)10);
    sgtxv_(&xc, &y[6], greek1, (ftnlen)10);
    sgtxv_(&xc, &y[7], greek2, (ftnlen)10);
/* ----------------------------- SYMBOLS --------------------------------- */
/* Writing concatenation */
    usgi_(ch__1, (ftnlen)3, &c__189);
    i__1[0] = 3, a__1[0] = ch__1;
    usgi_(ch__2, (ftnlen)3, &c__190);
    i__1[1] = 3, a__1[1] = ch__2;
    usgi_(ch__3, (ftnlen)3, &c__191);
    i__1[2] = 3, a__1[2] = ch__3;
    usgi_(ch__4, (ftnlen)3, &c__192);
    i__1[3] = 3, a__1[3] = ch__4;
    usgi_(ch__5, (ftnlen)3, &c__193);
    i__1[4] = 3, a__1[4] = ch__5;
    usgi_(ch__6, (ftnlen)3, &c__210);
    i__1[5] = 3, a__1[5] = ch__6;
    usgi_(ch__7, (ftnlen)3, &c__211);
    i__1[6] = 3, a__1[6] = ch__7;
    usgi_(ch__8, (ftnlen)3, &c__212);
    i__1[7] = 3, a__1[7] = ch__8;
    usgi_(ch__9, (ftnlen)3, &c__217);
    i__1[8] = 3, a__1[8] = ch__9;
    usgi_(ch__10, (ftnlen)3, &c__218);
    i__1[9] = 3, a__1[9] = ch__10;
    s_cat(symbol, a__1, i__1, &c__10, (ftnlen)10);
    sgtxv_(&xc, &y[8], symbol, (ftnlen)10);
    sgcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int sgpk07_ () { MAIN__ (); return 0; }

/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static real c_b6 = 0.f;
static real c_b7 = 10.f;
static real c_b11 = 1.f;
static real c_b15 = .03f;
static integer c__0 = 0;
static integer c__2 = 2;
static logical c_true = TRUE_;
static integer c__73 = 73;
static logical c_false = FALSE_;

/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Initialized data */

    static real xorg[4] = { 2.5f,7.5f,2.5f,7.5f };
    static real yorg[4] = { 7.5f,7.5f,2.5f,2.5f };

    /* System generated locals */
    real r__1, r__2;

    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);
    double cos(doublereal), sin(doublereal);

    /* Local variables */
    static integer i__, k;
    static real th, ux[73], uy[73];
    static integer iws;
    extern /* Subroutine */ int sgcls_(void), sgfrm_(void), sgopn_(integer *),
	     sglnu_(real *, real *, real *, real *), sgplu_(integer *, real *,
	     real *), sgnplc_(void), sgsplc_(char *, ftnlen), sgslni_(integer 
	    *), sgspli_(integer *), sglset_(char *, logical *, ftnlen), 
	    sgswnd_(real *, real *, real *, real *), sgstrf_(void), sgstxc_(
	    integer *), sgsplt_(integer *), sgstrn_(integer *), sgpwsn_(void),
	     sgstxi_(integer *), sgsvpt_(real *, real *, real *, real *), 
	    sgstxs_(real *);

    /* Fortran I/O blocks */
    static cilist io___3 = { 0, 6, 0, 0, 0 };
    static cilist io___4 = { 0, 5, 0, 0, 0 };


    s_wsle(&io___3);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I) ? ;", (ftnlen)23);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___4);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    sgopn_(&iws);
    sgfrm_();
    sgswnd_(&c_b6, &c_b7, &c_b6, &c_b7);
    sgsvpt_(&c_b6, &c_b11, &c_b6, &c_b11);
    sgstrn_(&c__1);
    sgstrf_();
    sgstxs_(&c_b15);
    sgstxi_(&c__3);
    sgstxc_(&c__0);
    sgsplc_("K=1", (ftnlen)3);
    for (k = 1; k <= 4; ++k) {
	sgslni_(&c__1);
	r__1 = xorg[k - 1] - 2.f;
	r__2 = xorg[k - 1] + 2.f;
	sglnu_(&r__1, &yorg[k - 1], &r__2, &yorg[k - 1]);
	r__1 = yorg[k - 1] - 2.f;
	r__2 = yorg[k - 1] + 2.f;
	sglnu_(&xorg[k - 1], &r__1, &xorg[k - 1], &r__2);
	for (i__ = 1; i__ <= 73; ++i__) {
	    th = (i__ - 1) * 6.2831840000000003f / 72;
	    ux[i__ - 1] = xorg[k - 1] + cos(th + (k - 1) * 3.141592f / 7) * 
		    2.f;
	    uy[i__ - 1] = yorg[k - 1] + sin(k * th) * 2.f;
/* L10: */
	}
	sgspli_(&c__2);
	sgsplt_(&k);
	sglset_("LCHAR", &c_true, (ftnlen)5);
	sgplu_(&c__73, ux, uy);
	sglset_("LCHAR", &c_false, (ftnlen)5);
	sgnplc_();
/* L20: */
    }
    sgcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int sgpk09_ () { MAIN__ (); return 0; }

/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static logical c_true = TRUE_;
static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static real c_b11 = 0.f;
static real c_b12 = 1.f;
static real c_b13 = -.8f;
static real c_b14 = .2f;
static real c_b15 = .15f;
static real c_b16 = .85f;
static real c_b17 = .1f;
static real c_b18 = .9f;
static integer c__41 = 41;
static real c_b24 = -.7f;
static real c_b25 = .3f;
static integer c__2 = 2;
static real c_b30 = -.6f;
static real c_b31 = .4f;
static real c_b36 = -.4f;
static real c_b37 = .6f;
static real c_b42 = -.3f;
static real c_b43 = .7f;
static real c_b48 = -.2f;
static real c_b49 = .8f;
static integer c__4 = 4;
static real c_b63 = .006f;
static integer c__32 = 32;

/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Builtin functions */
    double sin(doublereal);
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static integer i__;
    static real x[41], y[41];
    static integer n1, n2;
    static real dt;
    static integer iws;
    extern /* Subroutine */ int sgcls_(void), sgfrm_(void), sgopn_(integer *),
	     sgplu_(integer *, real *, real *);
    static integer itype;
    extern /* Subroutine */ int bitpci_(char *, integer *, ftnlen), gllset_(
	    char *, logical *, ftnlen), sgiset_(char *, integer *, ftnlen), 
	    sgspli_(integer *), sgswnd_(real *, real *, real *, real *), 
	    sgrset_(char *, real *, ftnlen), sgstrf_(void), sgsplt_(integer *)
	    , sgstrn_(integer *), sgpwsn_(void), sgsvpt_(real *, real *, real 
	    *, real *), slpvpr_(integer *), swcstx_(char *, char *, ftnlen, 
	    ftnlen), swlstx_(char *, logical *, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___5 = { 0, 6, 0, 0, 0 };
    static cilist io___6 = { 0, 5, 0, 0, 0 };


    dt = .31415899999999997f;
    for (i__ = 1; i__ <= 41; ++i__) {
	y[i__ - 1] = sin(dt * (i__ - 1)) * .15f;
	x[i__ - 1] = (real) (i__ - 1) / 40.f;
/* L10: */
    }
    swcstx_("FNAME", "SGPK03", (ftnlen)5, (ftnlen)6);
    swlstx_("LSEP", &c_true, (ftnlen)4);
    s_wsle(&io___5);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___6);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    sgopn_(&iws);
/* ----------------------------- page 1 --------------------------------- */
    sgfrm_();
    sgswnd_(&c_b11, &c_b12, &c_b13, &c_b14);
    sgsvpt_(&c_b15, &c_b16, &c_b17, &c_b18);
    sgstrn_(&c__1);
    sgstrf_();
    slpvpr_(&c__1);
    sgplu_(&c__41, x, y);
/* <-- 1 */
    sgswnd_(&c_b11, &c_b12, &c_b24, &c_b25);
    sgstrf_();
    sgspli_(&c__2);
/* <-- Line INDEX */
    sgplu_(&c__41, x, y);
/* <-- 2 */
    sgswnd_(&c_b11, &c_b12, &c_b30, &c_b31);
    sgstrf_();
    sgspli_(&c__3);
/* <-- Line INDEX */
    sgplu_(&c__41, x, y);
/* <-- 3 */
    sgswnd_(&c_b11, &c_b12, &c_b36, &c_b37);
    sgstrf_();
    sgsplt_(&c__2);
/* <-- Line TYPE 設定 ( */
    sgplu_(&c__41, x, y);
/* <-- 4 */
    sgswnd_(&c_b11, &c_b12, &c_b42, &c_b43);
    sgstrf_();
    sgsplt_(&c__3);
/* <-- Line TYPE 設定 ( */
    sgplu_(&c__41, x, y);
/* <-- 5 */
    sgswnd_(&c_b11, &c_b12, &c_b48, &c_b49);
    sgstrf_();
    sgsplt_(&c__4);
/* <-- Line TYPE 設定 ( */
    sgplu_(&c__41, x, y);
/* ----------------------------- page 2 --------------------------------- */
/* <-- 6 */
    sgfrm_();
    sgswnd_(&c_b11, &c_b12, &c_b13, &c_b14);
    sgsvpt_(&c_b15, &c_b16, &c_b17, &c_b18);
    sgstrn_(&c__1);
    sgstrf_();
    slpvpr_(&c__1);
    sgrset_("BITLEN", &c_b63, (ftnlen)6);
/* <-- */
    sgsplt_(&c__4);
    sgplu_(&c__41, x, y);
/* <-- 1 */
    sgswnd_(&c_b11, &c_b12, &c_b30, &c_b31);
    sgstrf_();
    sgspli_(&c__2);
    bitpci_("1111111100100100", &itype, (ftnlen)16);
/* <-- */
    sgsplt_(&itype);
/* <-- */
    sgplu_(&c__41, x, y);
/* <-- 2 */
    sgswnd_(&c_b11, &c_b12, &c_b36, &c_b37);
    sgstrf_();
    sgiset_("NBITS", &c__32, (ftnlen)5);
    bitpci_("10010010011111000111110001111100", &itype, (ftnlen)32);
    sgsplt_(&itype);
    sgplu_(&c__41, x, y);
    sgswnd_(&c_b11, &c_b12, &c_b48, &c_b49);
    sgstrf_();
    sgspli_(&c__1);
    sgsplt_(&c__1);
    n1 = 10;
    y[n1 - 2] = 999.f;
/* <-- */
    y[n1 - 1] = 999.f;
    y[n1] = 999.f;
    n2 = n1 * 3;
    y[n2 - 2] = 999.f;
    y[n2] = 999.f;
    gllset_("LMISS", &c_true, (ftnlen)5);
    sgplu_(&c__41, x, y);
/* <-- 4 */
    sgcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int sgpk03_ () { MAIN__ (); return 0; }

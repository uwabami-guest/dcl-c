/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static logical c_true = TRUE_;
static integer c__5 = 5;
static real c_b9 = 0.f;
static integer c__194 = 194;
static real c_b14 = .05f;
static integer c__0 = 0;

/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Initialized data */

    static real x[5] = { -7.5f,7.5f,7.5f,-7.5f,-7.5f };
    static real y[5] = { -7.5f,-7.5f,7.5f,7.5f,-7.5f };

    /* System generated locals */
    address a__1[3];
    integer i__1[3];
    real r__1, r__2, r__3, r__4;
    char ch__1[13], ch__2[3];

    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen);

    /* Local variables */
    static integer iws;
    extern /* Character */ VOID usgi_(char *, ftnlen, integer *);
    extern /* Subroutine */ int sgcls_(void), sgfrm_(void);
    static real xmaxf, ymaxf;
    extern /* Subroutine */ int slrat_(real *, real *), sgopn_(integer *), 
	    sgplu_(integer *, real *, real *), sglset_(char *, logical *, 
	    ftnlen), slform_(real *, real *), sgswnd_(real *, real *, real *, 
	    real *), sgstrf_(void), sgpwsn_(void), sgtxzu_(real *, real *, 
	    char *, real *, integer *, integer *, integer *, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___3 = { 0, 6, 0, 0, 0 };
    static cilist io___4 = { 0, 5, 0, 0, 0 };


    s_wsle(&io___3);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___4);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    sgopn_(&iws);
    xmaxf = 26.f;
    ymaxf = 18.5f;
    slform_(&xmaxf, &ymaxf);
/* <-- */
    slrat_(&xmaxf, &ymaxf);
    sglset_("LFULL", &c_true, (ftnlen)5);
    sgfrm_();
    r__1 = -xmaxf / 2.f;
    r__2 = xmaxf / 2.f;
    r__3 = -ymaxf / 2.f;
    r__4 = ymaxf / 2.f;
    sgswnd_(&r__1, &r__2, &r__3, &r__4);
    sgstrf_();
    sgplu_(&c__5, x, y);
/* Writing concatenation */
    i__1[0] = 5, a__1[0] = "15cm ";
    usgi_(ch__2, (ftnlen)3, &c__194);
    i__1[1] = 3, a__1[1] = ch__2;
    i__1[2] = 5, a__1[2] = " 15cm";
    s_cat(ch__1, a__1, i__1, &c__3, (ftnlen)13);
    sgtxzu_(&c_b9, &c_b9, ch__1, &c_b14, &c__0, &c__0, &c__3, (ftnlen)13);
    sgcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int slpk04_ () { MAIN__ (); return 0; }

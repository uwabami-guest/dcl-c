/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static real c_b6 = 0.f;
static real c_b8 = .05f;
static real c_b10 = 1.f;
static real c_b15 = -1.f;
static real c_b16 = .02f;
static integer c__2 = 2;
static real c_b31 = .5f;
static integer c__0 = 0;

/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static integer iws;
    extern /* Subroutine */ int sgcls_(void), sgfrm_(void), slmgn_(real *, 
	    real *, real *, real *), slrat_(real *, real *), sgopn_(integer *)
	    , sgpwsn_(void), slsttl_(char *, char *, real *, real *, real *, 
	    integer *, ftnlen, ftnlen), slpwwr_(integer *), sgtxzv_(real *, 
	    real *, char *, real *, integer *, integer *, integer *, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___1 = { 0, 6, 0, 0, 0 };
    static cilist io___2 = { 0, 5, 0, 0, 0 };


    s_wsle(&io___1);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___2);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    sgopn_(&iws);
    slmgn_(&c_b6, &c_b6, &c_b8, &c_b8);
/* <-- */
    slrat_(&c_b10, &c_b10);
/* <-- */
    slsttl_("FIGURE TITLE", "T", &c_b6, &c_b15, &c_b16, &c__1, (ftnlen)12, (
	    ftnlen)1);
/* <-+ */
    slsttl_("page:#PAGE", "B", &c_b10, &c_b10, &c_b16, &c__2, (ftnlen)10, (
	    ftnlen)1);
/* <-+ */
    slsttl_("PROG.NAME", "B", &c_b15, &c_b10, &c_b16, &c__3, (ftnlen)9, (
	    ftnlen)1);
/* <-+ */
    sgfrm_();
/* <-- */
    slpwwr_(&c__1);
    sgtxzv_(&c_b31, &c_b31, "FIGURE", &c_b8, &c__0, &c__0, &c__1, (ftnlen)6);
    sgcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int slpk02_ () { MAIN__ (); return 0; }

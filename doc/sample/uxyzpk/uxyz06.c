/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static real c_b6 = .8f;
static real c_b7 = -180.f;
static real c_b8 = 180.f;
static real c_b9 = 0.f;
static real c_b10 = .2f;
static real c_b16 = 10.f;
static real c_b17 = 60.f;
static integer c_b25 = 19811201;
static integer c__720 = 720;
static logical c_true = TRUE_;
static real c_b30 = 20.f;
static real c_b31 = 100.f;

/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static real rnd;
    static integer iws;
    extern /* Subroutine */ int grcls_(void), grfrm_(void), gropn_(integer *),
	     ucyacl_(char *, integer *, integer *, ftnlen), uzfact_(real *), 
	    sgswnd_(real *, real *, real *, real *), sgstrf_(void), uxaxdv_(
	    char *, real *, real *, ftnlen), sgstrn_(integer *), sgpwsn_(void)
	    , uyaxdv_(char *, real *, real *, ftnlen), sgsvpt_(real *, real *,
	     real *, real *), uzlset_(char *, logical *, ftnlen), uxmttl_(
	    char *, char *, real *, ftnlen, ftnlen), uxsttl_(char *, char *, 
	    real *, ftnlen, ftnlen), uysttl_(char *, char *, real *, ftnlen, 
	    ftnlen);

    /* Fortran I/O blocks */
    static cilist io___1 = { 0, 6, 0, 0, 0 };
    static cilist io___2 = { 0, 5, 0, 0, 0 };


    s_wsle(&io___1);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___2);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    gropn_(&iws);
    uzfact_(&c_b6);
    grfrm_();
    rnd = 720.f;
    sgswnd_(&c_b7, &c_b8, &rnd, &c_b9);
    sgsvpt_(&c_b10, &c_b6, &c_b10, &c_b6);
    sgstrn_(&c__1);
    sgstrf_();
    uxaxdv_("B", &c_b16, &c_b17, (ftnlen)1);
    uxaxdv_("T", &c_b16, &c_b17, (ftnlen)1);
    uxsttl_("B", "LONGITUDE", &c_b9, (ftnlen)1, (ftnlen)9);
    ucyacl_("L", &c_b25, &c__720, (ftnlen)1);
    uzlset_("LABELYR", &c_true, (ftnlen)7);
    uyaxdv_("R", &c_b30, &c_b31, (ftnlen)1);
    uysttl_("R", "DAY NUMBER", &c_b9, (ftnlen)1, (ftnlen)10);
    uxmttl_("T", "UXAXDV/UCYACL", &c_b9, (ftnlen)1, (ftnlen)13);
    grcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int uxyz06_ () { MAIN__ (); return 0; }

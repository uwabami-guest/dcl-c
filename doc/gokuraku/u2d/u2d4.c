/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static logical c_true = TRUE_;
static real c_b9 = -10.f;
static real c_b10 = 10.f;
static integer c__21 = 21;

/* Main program */ int MAIN__(void)
{
    /* System generated locals */
    real r__1, r__2;

    /* Builtin functions */
    double exp(doublereal);
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static integer i__, j, k;
    static real p[441]	/* was [21][21] */, u[441]	/* was [21][21] */, v[
	    441]	/* was [21][21] */, x, y, dp;
    static integer iws, ipat;
    static real tlev1, tlev2;
    extern /* Subroutine */ int grcls_(void), grfrm_(void), gropn_(integer *),
	     ugvect_(real *, integer *, real *, integer *, integer *, integer 
	    *), udcntr_(real *, integer *, integer *, integer *), uetone_(
	    real *, integer *, integer *, integer *), sglset_(char *, logical 
	    *, ftnlen), grswnd_(real *, real *, real *, real *), grstrf_(void)
	    , usdaxs_(void), uspfit_(void), sgpwsn_(void), uestlv_(real *, 
	    real *, integer *);

    /* Fortran I/O blocks */
    static cilist io___8 = { 0, 6, 0, 0, 0 };
    static cilist io___9 = { 0, 5, 0, 0, 0 };


    for (j = 1; j <= 21; ++j) {
	for (i__ = 1; i__ <= 21; ++i__) {
	    x = (i__ - 1) * 20.f / 20 - 10.f;
	    y = (j - 1) * 20.f / 20 - 10.f;
	    u[i__ + j * 21 - 22] = x;
	    v[i__ + j * 21 - 22] = -y;
/* Computing 2nd power */
	    r__1 = x;
/* Computing 2nd power */
	    r__2 = y;
	    p[i__ + j * 21 - 22] = exp(-(r__1 * r__1) / 64 - r__2 * r__2 / 25)
		    ;
/* L10: */
	}
    }
    s_wsle(&io___8);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___9);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    gropn_(&iws);
    sglset_("LSOFTF", &c_true, (ftnlen)6);
    grfrm_();
    grswnd_(&c_b9, &c_b10, &c_b9, &c_b10);
    uspfit_();
    grstrf_();
    usdaxs_();
    ugvect_(u, &c__21, v, &c__21, &c__21, &c__21);
    udcntr_(p, &c__21, &c__21, &c__21);
    dp = .20000000000000001f;
    for (k = 1; k <= 5; ++k) {
	tlev1 = (k - 1) * dp;
	tlev2 = tlev1 + dp;
	ipat = k + 599;
	uestlv_(&tlev1, &tlev2, &ipat);
/* L20: */
    }
    uetone_(p, &c__21, &c__21, &c__21);
    grcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int u2d4_ () { MAIN__ (); return 0; }

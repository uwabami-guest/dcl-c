/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static real c_b7 = -6.f;
static real c_b8 = 6.f;
static real c_b11 = .15f;
static real c_b12 = .45f;
static real c_b13 = .65f;
static real c_b14 = .95f;
static integer c__400 = 400;
static real c_b22 = -1.1f;
static real c_b23 = 1.1f;
static real c_b28 = .1f;
static real c_b29 = .5f;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Builtin functions */
    double sin(doublereal), cos(doublereal);
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static integer n;
    static real t, x[400], y[400], dt;
    static integer iws;
    extern real rngu0_(integer *);
    static integer iseed;
    extern /* Subroutine */ int grfig_(void), grcls_(void), grfrm_(void), 
	    gropn_(integer *), uulin_(integer *, real *, real *), uumrk_(
	    integer *, real *, real *), grswnd_(real *, real *, real *, real *
	    ), grstrf_(void), usdaxs_(void), grstrn_(integer *), sgpwsn_(void)
	    , grsvpt_(real *, real *, real *, real *), ussttl_(char *, char *,
	     char *, char *, ftnlen, ftnlen, ftnlen, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___6 = { 0, 6, 0, 0, 0 };
    static cilist io___7 = { 0, 5, 0, 0, 0 };


/* -- データ 1 ---- */
    dt = .015747318295739349f;
    for (n = 1; n <= 400; ++n) {
	t = dt * (n - 1);
	x[n - 1] = sin(t * 4.f) * 5.f;
	y[n - 1] = cos(t * 5.f) * 5.f;
/* L10: */
    }
/* -- グラフ 1 ---- */
    s_wsle(&io___6);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___7);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    gropn_(&iws);
    grfrm_();
    grswnd_(&c_b7, &c_b8, &c_b7, &c_b8);
    grsvpt_(&c_b11, &c_b12, &c_b13, &c_b14);
    grstrn_(&c__1);
    grstrf_();
    ussttl_("X1", "", "Y1", "", (ftnlen)2, (ftnlen)0, (ftnlen)2, (ftnlen)0);
    usdaxs_();
    uulin_(&c__400, x, y);
/* -- データ 2 ---- */
    iseed = 1;
    x[0] = (rngu0_(&iseed) - .5f) * 2.f;
    for (n = 2; n <= 400; ++n) {
	x[n - 1] = (rngu0_(&iseed) - .5f) * 2.f;
	y[n - 2] = x[n - 1];
/* L20: */
    }
    y[399] = x[0];
/* -- グラフ 2 ---- */
    grfig_();
    grswnd_(&c_b22, &c_b23, &c_b22, &c_b23);
    grsvpt_(&c_b11, &c_b14, &c_b28, &c_b29);
    grstrn_(&c__1);
    grstrf_();
    ussttl_("X2-TITLE", "", "Y2-TITLE", "", (ftnlen)8, (ftnlen)0, (ftnlen)8, (
	    ftnlen)0);
    usdaxs_();
    uumrk_(&c__400, x, y);
    grcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int jump1_ () { MAIN__ (); return 0; }

/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c_n1 = -1;
static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static integer c__201 = 201;
static integer c__2 = 2;

/* Main program */ int MAIN__(void)
{
    /* Builtin functions */
    integer pow_ii(integer *, integer *);
    double cos(doublereal);
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static real a[5];
    static integer i__, n;
    static real t, x[201], y0[201], y1[201], y2[201];
    static integer ii;
    static real pi;
    static integer iws;
    extern /* Subroutine */ int grcls_(void), grfrm_(void), gropn_(integer *),
	     uulin_(integer *, real *, real *), grstrf_(void), usdaxs_(void), 
	    uspfit_(void), uuslni_(integer *), sgpwsn_(void), uuslnt_(integer 
	    *), usspnt_(integer *, real *, real *), ussttl_(char *, char *, 
	    char *, char *, ftnlen, ftnlen, ftnlen, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___11 = { 0, 6, 0, 0, 0 };
    static cilist io___12 = { 0, 5, 0, 0, 0 };


    pi = 3.14159f;
    for (i__ = 1; i__ <= 5; ++i__) {
	ii = (i__ << 1) - 1;
	a[i__ - 1] = pow_ii(&c_n1, &i__) * 2.f / (ii * pi);
/* L10: */
    }
    for (n = 1; n <= 201; ++n) {
	x[n - 1] = (n - 1) * 1.f / 200;
	t = pi * 2.f * x[n - 1];
	if (t < pi / 2.f || t >= pi * 3.f / 2.f) {
	    y0[n - 1] = 0.f;
	} else {
	    y0[n - 1] = 1.f;
	}
	y1[n - 1] = a[0] * cos(t) + .5f;
	y2[n - 1] = .5f;
	for (i__ = 1; i__ <= 5; ++i__) {
	    ii = (i__ << 1) - 1;
	    y2[n - 1] += a[i__ - 1] * cos(ii * t);
/* L30: */
	}
/* L20: */
    }
    s_wsle(&io___11);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___12);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    gropn_(&iws);
    grfrm_();
    usspnt_(&c__201, x, y0);
    usspnt_(&c__201, x, y1);
    usspnt_(&c__201, x, y2);
    uspfit_();
    grstrf_();
    ussttl_("FREQUENCY", "/DAY", "RESPONSE", " ", (ftnlen)9, (ftnlen)4, (
	    ftnlen)8, (ftnlen)1);
    usdaxs_();
    uulin_(&c__201, x, y0);
    uuslnt_(&c__2);
    uuslni_(&c__3);
    uulin_(&c__201, x, y1);
    uuslnt_(&c__3);
    uulin_(&c__201, x, y2);
    grcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int uspac2_ () { MAIN__ (); return 0; }

/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static real c_b6 = .7f;
static logical c_true = TRUE_;
static real c_b9 = 0.f;
static real c_b10 = 180.f;
static real c_b12 = 100.f;
static real c_b13 = .4f;
static real c_b14 = .9f;
static real c_b15 = .3f;
static real c_b16 = .8f;
static integer c_b19 = 19911201;
static integer c__180 = 180;
static real c_b26 = 10.f;
static real c_b27 = 20.f;
static real c_b32 = 5.f;
static real c_b42 = 273.15f;
static real c_b44 = 1.f;
static real c_b53 = 32.f;
static real c_b55 = 1.8f;

/* Main program */ int MAIN__(void)
{
    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static integer iws;
    extern /* Subroutine */ int grcls_(void), grfrm_(void), gropn_(integer *),
	     ucxacl_(char *, integer *, integer *, ftnlen), uzfact_(real *), 
	    grswnd_(real *, real *, real *, real *), grstrf_(void), grstrn_(
	    integer *), uxaxdv_(char *, real *, real *, ftnlen), sgpwsn_(void)
	    , uyaxdv_(char *, real *, real *, ftnlen), grsvpt_(real *, real *,
	     real *, real *), uzlset_(char *, logical *, ftnlen), uxsaxs_(
	    char *, ftnlen), uysaxs_(char *, ftnlen), uzrset_(char *, real *, 
	    ftnlen), uxmttl_(char *, char *, real *, ftnlen, ftnlen), uxsttl_(
	    char *, char *, real *, ftnlen, ftnlen), uysttl_(char *, char *, 
	    real *, ftnlen, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___1 = { 0, 6, 0, 0, 0 };
    static cilist io___2 = { 0, 5, 0, 0, 0 };


    s_wsle(&io___1);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___2);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    gropn_(&iws);
    uzfact_(&c_b6);
    uzlset_("LOFFSET", &c_true, (ftnlen)7);
    grfrm_();
    grswnd_(&c_b9, &c_b10, &c_b9, &c_b12);
    grsvpt_(&c_b13, &c_b14, &c_b15, &c_b16);
    grstrn_(&c__1);
    grstrf_();
    ucxacl_("B", &c_b19, &c__180, (ftnlen)1);
    ucxacl_("T", &c_b19, &c__180, (ftnlen)1);
    uxsaxs_("B", (ftnlen)1);
    uxaxdv_("B", &c_b26, &c_b27, (ftnlen)1);
    uxsttl_("B", "DAY NUMBER", &c_b9, (ftnlen)1, (ftnlen)10);
    uyaxdv_("L", &c_b32, &c_b26, (ftnlen)1);
    uyaxdv_("R", &c_b32, &c_b26, (ftnlen)1);
    uysttl_("L", "CELSIUS SCALE", &c_b9, (ftnlen)1, (ftnlen)13);
    uysaxs_("L", (ftnlen)1);
    uzrset_("YOFFSET", &c_b42, (ftnlen)7);
    uzrset_("YFACT  ", &c_b44, (ftnlen)7);
    uyaxdv_("L", &c_b32, &c_b26, (ftnlen)1);
    uysttl_("L", "KELVIN SCALE", &c_b9, (ftnlen)1, (ftnlen)12);
    uysaxs_("L", (ftnlen)1);
    uzrset_("YOFFSET", &c_b53, (ftnlen)7);
    uzrset_("YFACT  ", &c_b55, (ftnlen)7);
    uyaxdv_("L", &c_b26, &c_b27, (ftnlen)1);
    uysttl_("L", "FAHRENHEIT SCALE", &c_b9, (ftnlen)1, (ftnlen)16);
    uxsttl_("T", "(LOFFSET=.TRUE.)", &c_b9, (ftnlen)1, (ftnlen)16);
    uxmttl_("T", "UXSAXS/UYSAXS", &c_b9, (ftnlen)1, (ftnlen)13);
    grcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int uxyz8_ () { MAIN__ (); return 0; }

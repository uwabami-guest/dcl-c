/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static logical c_true = TRUE_;
static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static integer c__300 = 300;
static real c_b12 = .85f;
static real c_b13 = 1.f;
static real c_b14 = 0.f;
static real c_b16 = .05f;
static real c_b17 = .1f;
static real c_b21 = -.5f;
static real c_b22 = .025f;
static integer c__2 = 2;
static real c_b30 = .01f;
static integer c__4 = 4;

/* Main program */ int MAIN__(void)
{
    /* Initialized data */

    static real xbox[4] = { 0.f,1.f,1.f,0.f };
    static real ybox[4] = { 0.f,0.f,1.f,1.f };

    /* System generated locals */
    integer i__1;
    real r__1;

    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void), s_wsfi(icilist *), 
	    do_fio(integer *, char *, ftnlen), e_wsfi(void);

    /* Local variables */
    static integer i__, j;
    static real x1, y1, x2, y2;
    static integer ibc;
    static char chr[5];
    static integer iws;
    extern /* Subroutine */ int sgcls_(void), sgfrm_(void), slmgn_(real *, 
	    real *, real *, real *);
    static integer itpat;
    extern /* Subroutine */ int slrat_(real *, real *), sgopn_(integer *), 
	    sgtnu_(integer *, real *, real *), sgtxv_(real *, real *, char *, 
	    ftnlen), sgiget_(char *, integer *, ftnlen), gliset_(char *, 
	    integer *, ftnlen), sgscmn_(integer *), sglset_(char *, logical *,
	     ftnlen), sgswnd_(real *, real *, real *, real *), sgstrf_(void), 
	    sgstxc_(integer *), sgstnp_(integer *), sgstrn_(integer *), 
	    swlset_(char *, logical *, ftnlen), sgpwsn_(void), slsttl_(char *,
	     char *, real *, real *, real *, integer *, ftnlen, ftnlen), 
	    sgsvpt_(real *, real *, real *, real *), slpvpr_(integer *), 
	    sgstxs_(real *);

    /* Fortran I/O blocks */
    static cilist io___3 = { 0, 6, 0, 0, 0 };
    static cilist io___4 = { 0, 5, 0, 0, 0 };
    static icilist io___15 = { 0, chr, 0, "(I5)", 5, 1 };


    swlset_("LCMCH", &c_true, (ftnlen)5);
    s_wsle(&io___3);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I) ? ; ", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___4);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    gliset_("MAXMSG", &c__300, (ftnlen)6);
    i__1 = -abs(iws);
    sgopn_(&i__1);
    sglset_("LFULL", &c_true, (ftnlen)5);
    slrat_(&c_b12, &c_b13);
    slmgn_(&c_b14, &c_b14, &c_b16, &c_b17);
    slsttl_("TEST OF COLORMAP", "T", &c_b14, &c_b21, &c_b22, &c__1, (ftnlen)
	    16, (ftnlen)1);
/*      DO 20 INUM=1,78 */
    sgscmn_(&c__2);
    sgfrm_();
    sgswnd_(&c_b14, &c_b13, &c_b14, &c_b13);
    sgstrn_(&c__1);
    sgstxs_(&c_b30);
    sgstxc_(&c__1);
    for (j = 0; j <= 9; ++j) {
	for (i__ = 0; i__ <= 9; ++i__) {
	    x1 = i__ * .076000000000000012f + .003f + .11999999999999994f;
	    y1 = (9 - j) * .10000000000000001f + .003f;
	    x2 = x1 + .07f;
	    y2 = y1 + .07f;
	    itpat = (i__ + j * 10) * 1000 + 999;
	    sgsvpt_(&x1, &x2, &y1, &y2);
	    sgstrf_();
	    sgiget_("IBGCLI", &ibc, (ftnlen)6);
	    if (itpat == 999) {
		itpat = ibc * 1000 + 999;
	    }
	    slpvpr_(&c__1);
	    sgstnp_(&itpat);
	    sgtnu_(&c__4, xbox, ybox);
	    s_wsfi(&io___15);
	    do_fio(&c__1, (char *)&itpat, (ftnlen)sizeof(integer));
	    e_wsfi();
	    r__1 = y2 + .014999999999999999f;
	    sgtxv_(&x2, &r__1, chr, (ftnlen)5);
/* L10: */
	}
    }
/*   20 CONTINUE */
    sgcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int color1_ () { MAIN__ (); return 0; }

/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static logical c_true = TRUE_;
static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static real c_b14 = 2.f;
static real c_b15 = 3.f;
static integer c__2 = 2;
static real c_b22 = 45.f;
static real c_b24 = 30.f;
static logical c_false = FALSE_;
static real c_b29 = .035f;
static real c_b32 = .1f;
static real c_b33 = .9f;
static real c_b36 = 0.f;
static real c_b39 = 90.f;
static real c_b41 = -180.f;
static real c_b42 = 180.f;
static real c_b47 = -90.f;
static real c_b51 = .5f;
static real c_b52 = .95f;
static real c_b57 = .05f;

/* Main program */ int MAIN__(void)
{
    /* Initialized data */

    static integer itr[14] = { 10,11,12,13,14,15,20,21,22,23,30,31,32,33 };
    static real fct[14] = { .12f,.12f,.14f,.14f,.14f,.14f,.11f,.16f,.12f,.12f,
	    .4f,.12f,.12f,.17f };

    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_wsfi(icilist *), do_fio(integer *, char *, ftnlen), e_wsfi(void)
	    ;

    /* Local variables */
    static integer i__, iws;
    static char cttl[32];
    extern /* Subroutine */ int sgcls_(void), sgfrm_(void), sldiv_(char *, 
	    integer *, integer *, ftnlen), slrat_(real *, real *), sgopn_(
	    integer *), sgtxr_(real *, real *, char *, ftnlen), umpglb_(void),
	     umpmap_(char *, ftnlen), sglset_(char *, logical *, ftnlen), 
	    sgssim_(real *, real *, real *), umiset_(char *, integer *, 
	    ftnlen), sgrset_(char *, real *, ftnlen), sgsmpl_(real *, real *, 
	    real *), umlset_(char *, logical *, ftnlen), sgstrf_(void), 
	    sgtrnl_(integer *, char *, ftnlen), sgstrn_(integer *), sgpwsn_(
	    void), sgstxi_(integer *), sgsvpt_(real *, real *, real *, real *)
	    , slpvpr_(integer *), swcstx_(char *, char *, ftnlen, ftnlen), 
	    sgstxs_(real *), slpwwr_(integer *), sgstxy_(real *, real *, real 
	    *, real *), swlstx_(char *, logical *, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___3 = { 0, 6, 0, 0, 0 };
    static cilist io___4 = { 0, 5, 0, 0, 0 };
    static icilist io___8 = { 0, cttl+4, 0, "(I2)", 2, 1 };


    swcstx_("FNAME", "map3d1", (ftnlen)5, (ftnlen)6);
    swlstx_("LSEP", &c_true, (ftnlen)4);
    s_wsle(&io___3);
    do_lio(&c__9, &c__1, " WORKSTATION IS (I) ? ;", (ftnlen)23);
    e_wsle();
    swcstx_("FNAME", "MAP3D1", (ftnlen)5, (ftnlen)6);
    swlstx_("LSEP", &c_true, (ftnlen)4);
    sgpwsn_();
    s_rsle(&io___4);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    i__1 = -abs(iws);
    sgopn_(&i__1);
    slrat_(&c_b14, &c_b15);
    sldiv_("Y", &c__2, &c__3, (ftnlen)1);
    sglset_("LCLIP", &c_true, (ftnlen)5);
    sgrset_("STLAT1", &c_b22, (ftnlen)6);
    sgrset_("STLAT2", &c_b24, (ftnlen)6);
    umlset_("LGRIDMN", &c_false, (ftnlen)7);
    umiset_("INDEXMJ", &c__1, (ftnlen)7);
    sgstxs_(&c_b29);
    sgstxi_(&c__3);
    for (i__ = 1; i__ <= 14; ++i__) {
	sgfrm_();
	sgsvpt_(&c_b32, &c_b33, &c_b32, &c_b33);
	sgssim_(&fct[i__ - 1], &c_b36, &c_b36);
	sgsmpl_(&c_b36, &c_b39, &c_b36);
	if (itr[i__ - 1] == 30) {
	    sgstxy_(&c_b41, &c_b42, &c_b36, &c_b39);
	} else {
	    sgstxy_(&c_b41, &c_b42, &c_b47, &c_b39);
	}
	sgstrn_(&itr[i__ - 1]);
	sgstrf_();
	slpwwr_(&c__1);
	slpvpr_(&c__1);
	sgtrnl_(&itr[i__ - 1], cttl, (ftnlen)32);
	sgtxr_(&c_b51, &c_b52, cttl, (ftnlen)32);
	s_copy(cttl, "ITR=", (ftnlen)32, (ftnlen)4);
	s_wsfi(&io___8);
	do_fio(&c__1, (char *)&itr[i__ - 1], (ftnlen)sizeof(integer));
	e_wsfi();
	sgtxr_(&c_b51, &c_b57, cttl, (ftnlen)32);
	umpmap_("coast_world", (ftnlen)11);
	umpglb_();
	if (itr[i__ - 1] == 23) {
	    sgfrm_();
	    sgfrm_();
	}
/* L10: */
    }
    sgcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int map3d1_ () { MAIN__ (); return 0; }

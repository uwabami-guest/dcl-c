/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static logical c_true = TRUE_;
static real c_b8 = 0.f;
static real c_b10 = .08f;
static real c_b12 = 1.f;
static real c_b13 = .6f;
static real c_b18 = .03f;
static real c_b22 = -1.f;
static real c_b24 = .02f;
static integer c__2 = 2;
static real c_b33 = .5f;

/* Main program */ int MAIN__(void)
{
    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static integer iws;
    extern /* Subroutine */ int sgcls_(void), sgfrm_(void), slmgn_(real *, 
	    real *, real *, real *), slrat_(real *, real *), sgopn_(integer *)
	    , sgtxv_(real *, real *, char *, ftnlen), sglset_(char *, logical 
	    *, ftnlen), sgpwsn_(void), slsttl_(char *, char *, real *, real *,
	     real *, integer *, ftnlen, ftnlen), slpvpr_(integer *);

    /* Fortran I/O blocks */
    static cilist io___1 = { 0, 6, 0, 0, 0 };
    static cilist io___2 = { 0, 5, 0, 0, 0 };


    s_wsle(&io___1);
    do_lio(&c__9, &c__1, "WORKSTATION ID ? ", (ftnlen)17);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___2);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    sgopn_(&iws);
    sglset_("LFULL", &c_true, (ftnlen)5);
    slmgn_(&c_b8, &c_b8, &c_b10, &c_b10);
    slrat_(&c_b12, &c_b13);
    slsttl_("FIGURE TITLE", "T", &c_b8, &c_b8, &c_b18, &c__1, (ftnlen)12, (
	    ftnlen)1);
    slsttl_("PROGRAM.NAME", "B", &c_b22, &c_b22, &c_b24, &c__2, (ftnlen)12, (
	    ftnlen)1);
    slsttl_("page:#PAGE", "B", &c_b12, &c_b22, &c_b24, &c__3, (ftnlen)10, (
	    ftnlen)1);
    sgfrm_();
    slpvpr_(&c__1);
    sgtxv_(&c_b33, &c_b33, "FIGURE", (ftnlen)6);
    sgcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int lay3_ () { MAIN__ (); return 0; }

/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static logical c_true = TRUE_;
static integer c__6 = 6;
static integer c__601 = 601;
static integer c__41 = 41;

/* Main program */ int MAIN__(void)
{
    /* Initialized data */

    static real xc[4] = { .25f,.75f,.25f,.75f };
    static real yc[4] = { .75f,.75f,.25f,.25f };

    /* Builtin functions */
    double cos(doublereal), sin(doublereal);
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static integer i__, n;
    static real x[164]	/* was [41][4] */, y[164]	/* was [41][4] */, dt;
    static integer iws;
    extern /* Subroutine */ int sgcls_(void), sgfrm_(void), sgopn_(integer *),
	     sgtnv_(integer *, real *, real *), sglset_(char *, logical *, 
	    ftnlen), sgstnp_(integer *), sgpwsn_(void);

    /* Fortran I/O blocks */
    static cilist io___8 = { 0, 6, 0, 0, 0 };
    static cilist io___9 = { 0, 5, 0, 0, 0 };


    dt = 1.0471966666666666f;
    for (n = 0; n <= 5; ++n) {
	for (i__ = 1; i__ <= 3; ++i__) {
	    x[n + i__ * 41 - 41] = cos(n * dt) * .2f + xc[i__ - 1];
	    y[n + i__ * 41 - 41] = sin(n * dt) * .2f + yc[i__ - 1];
/* L10: */
	}
    }
    dt = .31415899999999997f;
    for (n = 0; n <= 40; ++n) {
	x[n + 123] = (real) n * .4f / 40.f - .2f + xc[3];
	y[n + 123] = sin(n * dt) * .2f + yc[3];
/* L20: */
    }
    s_wsle(&io___8);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___9);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    sgopn_(&iws);
    sglset_("LSOFTF", &c_true, (ftnlen)6);
    sgfrm_();
/* -- デフォルト ---- */
    sgtnv_(&c__6, x, y);
/* -- トーンパターン ---- */
    sgstnp_(&c__3);
    sgtnv_(&c__6, &x[41], &y[41]);
    sgstnp_(&c__601);
    sgtnv_(&c__6, &x[82], &y[82]);
/* -- ねじれた多角形 ---- */
    sgtnv_(&c__41, &x[123], &y[123]);
    sgcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int kihon5_ () { MAIN__ (); return 0; }

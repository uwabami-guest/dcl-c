/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static logical c_true = TRUE_;
static real c_b9 = 0.f;
static real c_b10 = 12.56636f;
static real c_b11 = -1.f;
static real c_b12 = 1.f;
static real c_b15 = .9f;
static integer c__41 = 41;
static real c_b47 = .4f;
static real c_b48 = .5f;
static real c_b50 = .01f;
static real c_b53 = .02f;
static real c_b60 = .3f;
static integer c__90 = 90;
static logical c_false = FALSE_;
static real c_b77 = .2f;
static real c_b81 = 5.f;
static real c_b85 = 30.f;
static real c_b92 = .1f;

/* Main program */ int MAIN__(void)
{
    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    double sin(doublereal);
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static integer i__, n;
    static real x[41], y[41], dt;
    static integer iws;
    extern /* Subroutine */ int sgcls_(void), sgfrm_(void), sgopn_(integer *),
	     sgplu_(integer *, real *, real *);
    static real vymin, vymax;
    extern /* Subroutine */ int sgnplc_(void), sgsplc_(char *, ftnlen), 
	    sgiset_(char *, integer *, ftnlen), sglset_(char *, logical *, 
	    ftnlen), sgswnd_(real *, real *, real *, real *), sgrset_(char *, 
	    real *, ftnlen), sgstrf_(void), sgspls_(real *), sgsplt_(integer *
	    ), sgstrn_(integer *), sgpwsn_(void), sgsvpt_(real *, real *, 
	    real *, real *);

    /* Fortran I/O blocks */
    static cilist io___5 = { 0, 6, 0, 0, 0 };
    static cilist io___6 = { 0, 5, 0, 0, 0 };


    dt = .31415899999999997f;
    for (n = 0; n <= 40; ++n) {
	x[n] = n * dt;
	y[n] = sin(x[n]);
/* L10: */
    }
    s_wsle(&io___5);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___6);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    sgopn_(&iws);
    sgfrm_();
    sglset_("LCHAR", &c_true, (ftnlen)5);
/* -- ラベルつき折れ線 ---- */
    sgswnd_(&c_b9, &c_b10, &c_b11, &c_b12);
    sgsvpt_(&c_b9, &c_b12, &c_b15, &c_b12);
    sgstrn_(&c__1);
    sgstrf_();
    sgsplc_("A", (ftnlen)1);
    sgsplt_(&c__1);
    sgplu_(&c__41, x, y);
/* -- 順序ラベル: A,B,C,... ---- */
    for (i__ = 1; i__ <= 2; ++i__) {
	vymin = .9f - i__ * .1f;
	vymax = vymin + .1f;
	sgswnd_(&c_b9, &c_b10, &c_b11, &c_b12);
	sgsvpt_(&c_b9, &c_b12, &vymin, &vymax);
	sgstrn_(&c__1);
	sgstrf_();
	sgnplc_();
	sgplu_(&c__41, x, y);
/* L20: */
    }
/* -- 順序ラベル: K=1,K=2,... ---- */
    sgsplc_("K=1", (ftnlen)3);
    for (i__ = 3; i__ <= 4; ++i__) {
	vymin = .9f - i__ * .1f;
	vymax = vymin + .1f;
	sgswnd_(&c_b9, &c_b10, &c_b11, &c_b12);
	sgsvpt_(&c_b9, &c_b12, &vymin, &vymax);
	sgstrn_(&c__1);
	sgstrf_();
	i__1 = i__ - 1;
	sgsplt_(&i__1);
	sgplu_(&c__41, x, y);
	sgnplc_();
/* L30: */
    }
    sgsplt_(&c__1);
/* --  ラベルの文字列の高さ ---- */
    sgswnd_(&c_b9, &c_b10, &c_b11, &c_b12);
    sgsvpt_(&c_b9, &c_b12, &c_b47, &c_b48);
    sgstrn_(&c__1);
    sgstrf_();
    sgspls_(&c_b50);
    sgsplc_("small", (ftnlen)5);
    sgplu_(&c__41, x, y);
    sgspls_(&c_b53);
/* --  ラベルの角度 ---- */
    sgswnd_(&c_b9, &c_b10, &c_b11, &c_b12);
    sgsvpt_(&c_b9, &c_b12, &c_b60, &c_b47);
    sgstrn_(&c__1);
    sgstrf_();
    sglset_("LROT", &c_true, (ftnlen)4);
    sgiset_("IROT", &c__90, (ftnlen)4);
    sgsplc_("ROT", (ftnlen)3);
    sgplu_(&c__41, x, y);
    sglset_("LROT", &c_false, (ftnlen)4);
/* --  ラベルの間隔 ---- */
    sgswnd_(&c_b9, &c_b10, &c_b11, &c_b12);
    sgsvpt_(&c_b9, &c_b12, &c_b77, &c_b60);
    sgstrn_(&c__1);
    sgstrf_();
    sgrset_("CWL", &c_b81, (ftnlen)3);
    sgsplc_("CWL", (ftnlen)3);
    sgplu_(&c__41, x, y);
    sgrset_("CWL", &c_b85, (ftnlen)3);
/* --  ラベルの書き始め ---- */
    sgswnd_(&c_b9, &c_b10, &c_b11, &c_b12);
    sgsvpt_(&c_b9, &c_b12, &c_b92, &c_b77);
    sgstrn_(&c__1);
    sgstrf_();
    sgrset_("FFCT", &c_b15, (ftnlen)4);
    sgsplc_("FCT", (ftnlen)3);
    sgplu_(&c__41, x, y);
    sgcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int kihona_ () { MAIN__ (); return 0; }

/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static logical c_true = TRUE_;
static real c_b9 = -10.f;
static real c_b10 = 10.f;
static real c_b13 = .2f;
static real c_b14 = .8f;
static real c_b19 = 1.f;
static real c_b20 = 5.f;
static real c_b26 = 0.f;
static integer c__21 = 21;
static real c_b47 = .85f;
static real c_b48 = .9f;
static real c_b49 = .45f;
static real c_b50 = .75f;
static logical c_false = FALSE_;
static integer c__2 = 2;
static integer c__6 = 6;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* System generated locals */
    real r__1, r__2;

    /* Builtin functions */
    double exp(doublereal);
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static integer i__, j, k;
    static real p[441]	/* was [21][21] */, u[441]	/* was [21][21] */, v[
	    441]	/* was [21][21] */, x, y, dp, pi[12]	/* was [2][6] 
	    */;
    static integer iws, ipat;
    static real tlev1, tlev2;
    extern /* Subroutine */ int grcls_(void), grfrm_(void), gropn_(integer *),
	     uzfact_(real *), uetone_(real *, integer *, integer *, integer *)
	    , udlset_(char *, logical *, ftnlen), sglset_(char *, logical *, 
	    ftnlen), udcntr_(real *, integer *, integer *, integer *), 
	    grswnd_(real *, real *, real *, real *), grstrf_(void), grstrn_(
	    integer *), uxaxdv_(char *, real *, real *, ftnlen), sgpwsn_(void)
	    , uyaxdv_(char *, real *, real *, ftnlen), uestlv_(real *, real *,
	     integer *), grsvpt_(real *, real *, real *, real *), slpvpr_(
	    integer *), uzlset_(char *, logical *, ftnlen), uysfmt_(char *, 
	    ftnlen), uxmttl_(char *, char *, real *, ftnlen, ftnlen), uxsttl_(
	    char *, char *, real *, ftnlen, ftnlen), uysttl_(char *, char *, 
	    real *, ftnlen, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___8 = { 0, 6, 0, 0, 0 };
    static cilist io___9 = { 0, 5, 0, 0, 0 };


    for (j = 1; j <= 21; ++j) {
	for (i__ = 1; i__ <= 21; ++i__) {
	    x = (i__ - 1) * 20.f / 20 - 10.f;
	    y = (j - 1) * 20.f / 20 - 10.f;
	    u[i__ + j * 21 - 22] = x;
	    v[i__ + j * 21 - 22] = -y;
/* Computing 2nd power */
	    r__1 = x;
/* Computing 2nd power */
	    r__2 = y;
	    p[i__ + j * 21 - 22] = exp(-(r__1 * r__1) / 64 - r__2 * r__2 / 25)
		    ;
/* L10: */
	}
    }
    s_wsle(&io___8);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___9);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    gropn_(&iws);
    sglset_("LSOFTF", &c_true, (ftnlen)6);
    grfrm_();
    grswnd_(&c_b9, &c_b10, &c_b9, &c_b10);
    grsvpt_(&c_b13, &c_b14, &c_b13, &c_b14);
    grstrn_(&c__1);
    grstrf_();
    uxaxdv_("B", &c_b19, &c_b20, (ftnlen)1);
    uxaxdv_("T", &c_b19, &c_b20, (ftnlen)1);
    uxsttl_("B", "X-axis", &c_b26, (ftnlen)1, (ftnlen)6);
    uxmttl_("T", "FIGURE TITLE", &c_b26, (ftnlen)1, (ftnlen)12);
    uyaxdv_("L", &c_b19, &c_b20, (ftnlen)1);
    uyaxdv_("R", &c_b19, &c_b20, (ftnlen)1);
    uysttl_("L", "Y-axis", &c_b26, (ftnlen)1, (ftnlen)6);
    dp = .20000000000000001f;
    for (k = 1; k <= 5; ++k) {
	tlev1 = (k - 1) * dp;
	tlev2 = tlev1 + dp;
	ipat = k + 599;
	uestlv_(&tlev1, &tlev2, &ipat);
/* L20: */
    }
    uetone_(p, &c__21, &c__21, &c__21);
/* -- トーンバー ---- */
    grswnd_(&c_b26, &c_b19, &c_b26, &c_b19);
    grsvpt_(&c_b47, &c_b48, &c_b49, &c_b50);
    grstrn_(&c__1);
    grstrf_();
    for (k = 1; k <= 6; ++k) {
	pi[(k << 1) - 2] = (k - 1) * dp + 0.f;
	pi[(k << 1) - 1] = (k - 1) * dp + 0.f;
/* L30: */
    }
    udlset_("LMSG", &c_false, (ftnlen)4);
    udcntr_(pi, &c__2, &c__2, &c__6);
    uetone_(pi, &c__2, &c__2, &c__6);
    slpvpr_(&c__3);
    uzlset_("LABELYR", &c_true, (ftnlen)7);
    uzfact_(&c_b14);
    uysfmt_("(F4.1)", (ftnlen)6);
    uyaxdv_("R", &dp, &dp, (ftnlen)1);
    grcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int u2d7a_ () { MAIN__ (); return 0; }

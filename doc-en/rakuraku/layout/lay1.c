/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static logical c_true = TRUE_;
static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static real c_b10 = .1f;
static real c_b12 = .05f;
static integer c__2 = 2;
static real c_b25 = .5f;

/* Main program */ int MAIN__(void)
{
    /* Initialized data */

    static char ctxt[7] = "FRAME??";

    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void), s_wsfi(icilist *), 
	    do_fio(integer *, char *, ftnlen), e_wsfi(void);

    /* Local variables */
    static integer i__, iws;
    extern /* Subroutine */ int sgcls_(void), sgfrm_(void), slmgn_(real *, 
	    real *, real *, real *), sldiv_(char *, integer *, integer *, 
	    ftnlen), sgopn_(integer *), sgtxv_(real *, real *, char *, ftnlen)
	    , sgpwsn_(void), slpvpr_(integer *), swcstx_(char *, char *, 
	    ftnlen, ftnlen), swlstx_(char *, logical *, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___2 = { 0, 6, 0, 0, 0 };
    static cilist io___3 = { 0, 5, 0, 0, 0 };
    static icilist io___6 = { 0, ctxt+5, 0, "(I2.2)", 2, 1 };


    swcstx_("FNAME", "lay1", (ftnlen)5, (ftnlen)4);
    swlstx_("LSEP", &c_true, (ftnlen)4);
    s_wsle(&io___2);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___3);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    sgopn_(&iws);
    slmgn_(&c_b10, &c_b10, &c_b12, &c_b12);
    sldiv_("Y", &c__3, &c__2, (ftnlen)1);
    slmgn_(&c_b12, &c_b12, &c_b12, &c_b12);
    for (i__ = 1; i__ <= 12; ++i__) {
	sgfrm_();
	slpvpr_(&c__1);
	s_wsfi(&io___6);
	do_fio(&c__1, (char *)&i__, (ftnlen)sizeof(integer));
	e_wsfi();
	sgtxv_(&c_b25, &c_b25, ctxt, (ftnlen)7);
/* L10: */
    }
    sgcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int lay1_ () { MAIN__ (); return 0; }

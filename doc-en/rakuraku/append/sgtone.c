/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static integer c__300 = 300;
static logical c_true = TRUE_;
static logical c_false = FALSE_;
static real c_b21 = .85f;
static real c_b22 = 1.f;
static real c_b23 = 0.f;
static real c_b25 = .05f;
static real c_b26 = .1f;
static real c_b32 = -.5f;
static real c_b33 = .03f;
static integer c__5 = 5;
static real c_b48 = .015f;
static integer c__0 = 0;

/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Initialized data */

    static real xbox[5] = { 0.f,1.f,1.f,0.f,0.f };
    static real ybox[5] = { 0.f,0.f,1.f,1.f,0.f };

    /* System generated locals */
    real r__1;

    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_wsfi(icilist *), do_fio(integer *, char *, ftnlen), e_wsfi(void)
	    ;

    /* Local variables */
    static integer i__, j, n;
    static real x1, x2, y1, y2;
    static integer iw;
    static char chr[5];
    static integer ifn;
    static char cttl[32];
    static integer level;
    extern /* Subroutine */ int sgcls_(void), sgfrm_(void), slmgn_(real *, 
	    real *, real *, real *), slrat_(real *, real *), sgopn_(integer *)
	    , gliset_(char *, integer *, ftnlen), sgiset_(char *, integer *, 
	    ftnlen), sglset_(char *, logical *, ftnlen), sgswnd_(real *, real 
	    *, real *, real *), sgstrf_(void), sgstrn_(integer *), sgpwsn_(
	    void), sgplzu_(integer *, real *, real *, integer *, integer *), 
	    slsttl_(char *, char *, real *, real *, real *, integer *, ftnlen,
	     ftnlen), sgsvpt_(real *, real *, real *, real *), sgtnzu_(
	    integer *, real *, real *, integer *), sgtxzv_(real *, real *, 
	    char *, real *, integer *, integer *, integer *, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___3 = { 0, 6, 0, 0, 0 };
    static cilist io___4 = { 0, 5, 0, 0, 0 };
    static cilist io___7 = { 0, 6, 0, 0, 0 };
    static cilist io___8 = { 0, 5, 0, 0, 0 };
    static icilist io___11 = { 0, cttl+15, 0, "(I1)", 1, 1 };
    static icilist io___20 = { 0, chr, 0, "(I5)", 5, 1 };


    s_wsle(&io___3);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I) ? ; ", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___4);
    do_lio(&c__3, &c__1, (char *)&iw, (ftnlen)sizeof(integer));
    e_rsle();
/*     WRITE(*,*) ' SOFT FILL=1/HARD FILL=2 (I) ? ;' */
/*     READ(*,*) IFN */
    ifn = 1;
    s_wsle(&io___7);
    do_lio(&c__9, &c__1, " TONE PATTERN (0-6) (I) ? ;", (ftnlen)27);
    e_wsle();
    s_rsle(&io___8);
    do_lio(&c__3, &c__1, (char *)&n, (ftnlen)sizeof(integer));
    e_rsle();
    gliset_("MAXMSG", &c__300, (ftnlen)6);
    sgopn_(&iw);
    if (ifn == 1) {
	sglset_("LSOFTF", &c_true, (ftnlen)6);
    } else {
	sglset_("LSOFTF", &c_false, (ftnlen)6);
    }
    sglset_("LFULL", &c_true, (ftnlen)5);
    sgiset_("INDEX", &c__3, (ftnlen)5);
    slrat_(&c_b21, &c_b22);
    slmgn_(&c_b23, &c_b23, &c_b25, &c_b26);
    s_copy(cttl, "TONE PATTERN = #XX", (ftnlen)32, (ftnlen)18);
    s_wsfi(&io___11);
    do_fio(&c__1, (char *)&n, (ftnlen)sizeof(integer));
    e_wsfi();
    slsttl_(cttl, "T", &c_b23, &c_b32, &c_b33, &c__1, (ftnlen)32, (ftnlen)1);
    sgfrm_();
    sgswnd_(&c_b23, &c_b22, &c_b23, &c_b22);
    sgstrn_(&c__1);
    for (j = 0; j <= 5; ++j) {
	for (i__ = 0; i__ <= 5; ++i__) {
	    x1 = i__ * .14000000000000001f + .005f + .07999999999999996f;
	    x2 = x1 + .13f;
	    y1 = (5 - j) * .16666666666666666f + .005f;
	    y2 = y1 + .13f;
	    level = i__ + j * 10 + n * 100;
	    sgsvpt_(&x1, &x2, &y1, &y2);
	    sgstrf_();
	    s_wsfi(&io___20);
	    do_fio(&c__1, (char *)&level, (ftnlen)sizeof(integer));
	    e_wsfi();
	    sgtnzu_(&c__5, xbox, ybox, &level);
	    sgplzu_(&c__5, xbox, ybox, &c__1, &c__1);
	    r__1 = y2 + .018333333333333326f;
	    sgtxzv_(&x2, &r__1, chr, &c_b48, &c__0, &c__1, &c__3, (ftnlen)5);
/* L10: */
	}
/* L20: */
    }
    sgcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int sgtone_ () { MAIN__ (); return 0; }

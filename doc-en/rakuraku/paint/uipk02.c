/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__300 = 300;
static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static real c_b10 = -1.f;
static real c_b11 = 1.f;
static real c_b14 = .1f;
static real c_b15 = .9f;
static integer c__50 = 50;
static real c_b24 = .5f;

/* Main program */ int MAIN__(void)
{
    /* System generated locals */
    real r__1, r__2;

    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static integer i__, j;
    static real z__[2500]	/* was [50][50] */;
    static integer iws;
    extern /* Subroutine */ int grcls_(void), grfrm_(void), gropn_(integer *),
	     uipdat_(real *, integer *, integer *, integer *), grswnd_(real *,
	     real *, real *, real *), grstrf_(void), swiset_(char *, integer *
	    , ftnlen), grstrn_(integer *), uxaxdv_(char *, real *, real *, 
	    ftnlen), sgpwsn_(void), uyaxdv_(char *, real *, real *, ftnlen), 
	    grsvpt_(real *, real *, real *, real *);

    /* Fortran I/O blocks */
    static cilist io___4 = { 0, 6, 0, 0, 0 };
    static cilist io___5 = { 0, 5, 0, 0, 0 };


    for (i__ = 1; i__ <= 50; ++i__) {
	for (j = 1; j <= 50; ++j) {
/* Computing 2nd power */
	    r__1 = i__ - 25.f;
/* Computing 2nd power */
	    r__2 = j - 25.f;
	    z__[i__ + j * 50 - 51] = -(r__1 * r__1) - r__2 * r__2;
	}
    }
    swiset_("WINDOW_HEIGHT", &c__300, (ftnlen)13);
    swiset_("WINDOW_WIDTH", &c__300, (ftnlen)12);
    s_wsle(&io___4);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I) ? ;", (ftnlen)23);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___5);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    gropn_(&iws);
    grfrm_();
    grswnd_(&c_b10, &c_b11, &c_b10, &c_b11);
    grsvpt_(&c_b14, &c_b15, &c_b14, &c_b15);
    grstrn_(&c__1);
    grstrf_();
    uipdat_(z__, &c__50, &c__50, &c__50);
    uxaxdv_("T", &c_b14, &c_b24, (ftnlen)1);
    uxaxdv_("B", &c_b14, &c_b24, (ftnlen)1);
    uyaxdv_("L", &c_b14, &c_b24, (ftnlen)1);
    uyaxdv_("R", &c_b14, &c_b24, (ftnlen)1);
    grcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int sample02_ () { MAIN__ (); return 0; }

/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static logical c_true = TRUE_;
static real c_b9 = 0.f;
static real c_b10 = 360.f;
static real c_b11 = -90.f;
static real c_b12 = 90.f;
static real c_b13 = .2f;
static real c_b14 = .8f;
static integer c__37 = 37;
static real c_b22 = .4f;

/* Main program */ int MAIN__(void)
{
    /* System generated locals */
    integer i__1;
    real r__1, r__2;

    /* Builtin functions */
    double sin(doublereal), sqrt(doublereal), cos(doublereal);
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static integer i__, j, k;
    static real p[1369]	/* was [37][37] */;
    static integer iws;
    static real alat, alon;
    static integer ipat;
    static real slat, tlev1, tlev2;
    extern /* Subroutine */ int grcls_(void), grfrm_(void), gropn_(integer *),
	     udgclb_(real *, integer *, integer *, integer *, real *), 
	    udcntr_(real *, integer *, integer *, integer *), uetone_(real *, 
	    integer *, integer *, integer *), sglset_(char *, logical *, 
	    ftnlen), grswnd_(real *, real *, real *, real *), grstrf_(void), 
	    usdaxs_(void), grstrn_(integer *), sgpwsn_(void), uestlv_(real *, 
	    real *, integer *), grsvpt_(real *, real *, real *, real *);

    /* Fortran I/O blocks */
    static cilist io___7 = { 0, 6, 0, 0, 0 };
    static cilist io___8 = { 0, 5, 0, 0, 0 };


    for (j = 1; j <= 37; ++j) {
	for (i__ = 1; i__ <= 37; ++i__) {
	    alon = ((i__ - 1) * 360.f / 36 + 0.f) * .017453277777777776f;
	    alat = ((j - 1) * 180.f / 36 - 90.f) * .017453277777777776f;
	    slat = sin(alat);
/* Computing 2nd power */
	    r__1 = slat;
/* Computing 2nd power */
	    r__2 = slat;
	    p[i__ + j * 37 - 38] = sqrt(1 - r__1 * r__1) * 3 * slat * cos(
		    alon) - (r__2 * r__2 * 3 - 1) * .5f;
/* L10: */
	}
    }
    s_wsle(&io___7);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___8);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    gropn_(&iws);
    sglset_("LSOFTF", &c_true, (ftnlen)6);
    grfrm_();
    grswnd_(&c_b9, &c_b10, &c_b11, &c_b12);
    grsvpt_(&c_b13, &c_b14, &c_b13, &c_b14);
    grstrn_(&c__1);
    grstrf_();
    for (k = -5; k <= 3; ++k) {
	tlev1 = k * .4f;
	tlev2 = tlev1 + .4f;
	if (k <= -1) {
	    ipat = (i__1 = k + 1, abs(i__1)) + 600;
	} else {
	    ipat = k + 30;
	}
	uestlv_(&tlev1, &tlev2, &ipat);
/* L20: */
    }
    usdaxs_();
    udgclb_(p, &c__37, &c__37, &c__37, &c_b22);
    udcntr_(p, &c__37, &c__37, &c__37);
    uetone_(p, &c__37, &c__37, &c__37);
    grcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int u2d5_ () { MAIN__ (); return 0; }

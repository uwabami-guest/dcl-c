/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static real c_b7 = -10.f;
static real c_b8 = 10.f;
static real c_b11 = .2f;
static real c_b12 = .8f;
static real c_b13 = .1f;
static real c_b14 = .5f;
static integer c__2 = 2;
static real c_b18 = 0.f;
static real c_b19 = -.7f;
static real c_b21 = 1.2f;
static real c_b22 = .30000000000000004f;
static real c_b23 = .20000000000000001f;
static real c_b24 = .29999999999999999f;
static real c_b26 = 1.f;
static real c_b27 = 5.f;
static real c_b36 = 4.f;
static real c_b44 = .014f;
static integer c__21 = 21;
static logical c_false = FALSE_;
static real c_b68 = 20.f;
static real c_b72 = .6f;

/* Main program */ int MAIN__(void)
{
    /* System generated locals */
    real r__1, r__2;

    /* Builtin functions */
    double exp(doublereal);
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static integer i__, j, k;
    static real p[441]	/* was [21][21] */, u[441]	/* was [21][21] */, v[
	    441]	/* was [21][21] */, x, y, dp;
    static integer iws, ipat;
    static real tlev1, tlev2;
    extern /* Subroutine */ int sgcls_(void), sgfrm_(void), sgopn_(integer *);
    static real vzlev;
    extern /* Subroutine */ int udgclb_(real *, integer *, integer *, integer 
	    *, real *), scsobj_(real *, real *, real *), scseye_(real *, real 
	    *, real *), ugvect_(real *, integer *, real *, integer *, integer 
	    *, integer *), uetone_(real *, integer *, integer *, integer *), 
	    udcntr_(real *, integer *, integer *, integer *), scspln_(integer 
	    *, integer *, real *), scsprj_(void), sgswnd_(real *, real *, 
	    real *, real *), udpset_(char *, logical *, ftnlen), ugpset_(char 
	    *, real *, ftnlen), sgstrf_(void), uxaxdv_(char *, real *, real *,
	     ftnlen), sgstrn_(integer *), sgpwsn_(void), uyaxdv_(char *, real 
	    *, real *, ftnlen), uestlv_(real *, real *, integer *), uzinit_(
	    void), sgsvpt_(real *, real *, real *, real *), slpvpr_(integer *)
	    , uxsttl_(char *, char *, real *, ftnlen, ftnlen), uysttl_(char *,
	     char *, real *, ftnlen, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___8 = { 0, 6, 0, 0, 0 };
    static cilist io___9 = { 0, 5, 0, 0, 0 };


    for (j = 1; j <= 21; ++j) {
	for (i__ = 1; i__ <= 21; ++i__) {
	    x = (i__ - 1) * 20.f / 20 - 10.f;
	    y = (j - 1) * 20.f / 20 - 10.f;
	    u[i__ + j * 21 - 22] = x;
	    v[i__ + j * 21 - 22] = -y;
/* Computing 2nd power */
	    r__1 = x;
/* Computing 2nd power */
	    r__2 = y;
	    p[i__ + j * 21 - 22] = exp(-(r__1 * r__1) / 64 - r__2 * r__2 / 25)
		    ;
/* L10: */
	}
    }
    s_wsle(&io___8);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___9);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    sgopn_(&iws);
    sgfrm_();
/* -- X-Y 平面: 下レベル ---- */
    sgswnd_(&c_b7, &c_b8, &c_b7, &c_b8);
    sgsvpt_(&c_b11, &c_b12, &c_b13, &c_b14);
    sgstrn_(&c__1);
    sgstrf_();
    scspln_(&c__1, &c__2, &c_b18);
    scseye_(&c_b19, &c_b19, &c_b21);
    scsobj_(&c_b22, &c_b23, &c_b24);
    scsprj_();
    uxaxdv_("B", &c_b26, &c_b27, (ftnlen)1);
    uxaxdv_("T", &c_b26, &c_b27, (ftnlen)1);
    uxsttl_("B", "X-axis", &c_b18, (ftnlen)1, (ftnlen)6);
    uyaxdv_("L", &c_b26, &c_b36, (ftnlen)1);
    uyaxdv_("R", &c_b26, &c_b36, (ftnlen)1);
    uysttl_("L", "Y-axis", &c_b18, (ftnlen)1, (ftnlen)6);
    ugpset_("RSIZET", &c_b44, (ftnlen)6);
    ugvect_(u, &c__21, v, &c__21, &c__21, &c__21);
/* -- X-Y 平面: 上レベル ---- */
    vzlev = .35999999999999999f;
    scspln_(&c__1, &c__2, &vzlev);
    scsprj_();
    dp = .20000000000000001f;
    for (k = 1; k <= 5; ++k) {
	tlev1 = (k - 1) * dp;
	tlev2 = tlev1 + dp;
	ipat = k + 599;
	uestlv_(&tlev1, &tlev2, &ipat);
/* L20: */
    }
    uetone_(p, &c__21, &c__21, &c__21);
    udpset_("LMSG", &c_false, (ftnlen)4);
    udgclb_(p, &c__21, &c__21, &c__21, &c_b13);
    udcntr_(p, &c__21, &c__21, &c__21);
    slpvpr_(&c__1);
/* -- X-Z 平面 ---- */
    sgswnd_(&c_b7, &c_b8, &c_b18, &c_b68);
    sgsvpt_(&c_b11, &c_b12, &c_b18, &c_b72);
    sgstrn_(&c__1);
    sgstrf_();
    scspln_(&c__1, &c__3, &c_b14);
    scsprj_();
    uzinit_();
    uyaxdv_("L", &c_b26, &c_b27, (ftnlen)1);
    uysttl_("L", "Z-axis", &c_b18, (ftnlen)1, (ftnlen)6);
    sgcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int map3d5_ () { MAIN__ (); return 0; }

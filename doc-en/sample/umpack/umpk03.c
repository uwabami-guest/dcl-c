/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static real c_b6 = 2.f;
static real c_b7 = 1.f;
static integer c__2 = 2;
static real c_b12 = 123.f;
static real c_b13 = 147.f;
static real c_b14 = 30.f;
static real c_b15 = 46.f;
static real c_b16 = .1f;
static real c_b17 = .9f;
static logical c_true = TRUE_;
static real c_b24 = .5f;
static real c_b25 = .95f;
static real c_b26 = .03f;
static integer c__0 = 0;

/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Initialized data */

    static char ctr[3*2] = "MER" "CON";

    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static integer i__, iws;
    static char cttl[32];
    extern /* Subroutine */ int grcls_(void), grfrm_(void), sldiv_(char *, 
	    integer *, integer *, ftnlen), gropn_(integer *), slrat_(real *, 
	    real *), umpglb_(void);
    extern integer isgtrc_(char *, ftnlen);
    extern /* Subroutine */ int umpmap_(char *, ftnlen), sglset_(char *, 
	    logical *, ftnlen), grswnd_(real *, real *, real *, real *), 
	    umpfit_(void), grstrf_(void), sgtrnl_(integer *, char *, ftnlen), 
	    grstrn_(integer *), sgpwsn_(void), grsvpt_(real *, real *, real *,
	     real *), slpvpr_(integer *), slpwwr_(integer *), sgtxzr_(real *, 
	    real *, char *, real *, integer *, integer *, integer *, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___2 = { 0, 6, 0, 0, 0 };
    static cilist io___3 = { 0, 5, 0, 0, 0 };


    s_wsle(&io___2);
    do_lio(&c__9, &c__1, " WORKSTATION IS (I) ? ;", (ftnlen)23);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___3);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    i__1 = abs(iws);
    gropn_(&i__1);
    slrat_(&c_b6, &c_b7);
    sldiv_("Y", &c__2, &c__1, (ftnlen)1);
    for (i__ = 1; i__ <= 2; ++i__) {
	grfrm_();
	grswnd_(&c_b12, &c_b13, &c_b14, &c_b15);
	grsvpt_(&c_b16, &c_b17, &c_b16, &c_b17);
	i__1 = isgtrc_(ctr + (i__ - 1) * 3, (ftnlen)3);
	grstrn_(&i__1);
	umpfit_();
	grstrf_();
	sglset_("LCLIP", &c_true, (ftnlen)5);
	slpwwr_(&c__1);
	slpvpr_(&c__1);
	i__1 = isgtrc_(ctr + (i__ - 1) * 3, (ftnlen)3);
	sgtrnl_(&i__1, cttl, (ftnlen)32);
	sgtxzr_(&c_b24, &c_b25, cttl, &c_b26, &c__0, &c__0, &c__3, (ftnlen)32)
		;
/*       CALL UMPMAP( 'coast_japan' ) */
	umpmap_("coast_world", (ftnlen)11);
	umpglb_();
/* L10: */
    }
    grcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int umpk03_ () { MAIN__ (); return 0; }

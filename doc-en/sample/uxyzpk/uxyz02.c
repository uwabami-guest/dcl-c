/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static real c_b6 = 1.f;
static real c_b7 = 1e5f;
static real c_b8 = 1e3f;
static real c_b10 = .2f;
static real c_b11 = .8f;
static integer c__4 = 4;
static real c_b35 = 0.f;

/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static integer iws;
    extern /* Subroutine */ int grcls_(void), grfrm_(void), gropn_(integer *),
	     sgswnd_(real *, real *, real *, real *), sgstrf_(void), ulxlog_(
	    char *, integer *, integer *, ftnlen), ulylog_(char *, integer *, 
	    integer *, ftnlen), sgstrn_(integer *), sgpwsn_(void), sgsvpt_(
	    real *, real *, real *, real *), uxmttl_(char *, char *, real *, 
	    ftnlen, ftnlen), uxsttl_(char *, char *, real *, ftnlen, ftnlen), 
	    uysttl_(char *, char *, real *, ftnlen, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___1 = { 0, 6, 0, 0, 0 };
    static cilist io___2 = { 0, 5, 0, 0, 0 };


    s_wsle(&io___1);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I) ? ;", (ftnlen)23);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___2);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    gropn_(&iws);
    grfrm_();
    sgswnd_(&c_b6, &c_b7, &c_b8, &c_b6);
    sgsvpt_(&c_b10, &c_b11, &c_b10, &c_b11);
    sgstrn_(&c__4);
    sgstrf_();
    ulxlog_("B", &c__1, &c__9, (ftnlen)1);
    ulxlog_("T", &c__1, &c__9, (ftnlen)1);
    uxsttl_("B", "[X]", &c_b6, (ftnlen)1, (ftnlen)3);
    ulylog_("L", &c__3, &c__9, (ftnlen)1);
    ulylog_("R", &c__3, &c__9, (ftnlen)1);
    uysttl_("L", "[Y]", &c_b6, (ftnlen)1, (ftnlen)3);
    uxmttl_("T", "ULXLOG/ULYLOG", &c_b35, (ftnlen)1, (ftnlen)13);
    grcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int uxyz02_ () { MAIN__ (); return 0; }

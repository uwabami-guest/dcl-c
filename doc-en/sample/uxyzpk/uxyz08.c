/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static real c_b6 = -180.f;
static real c_b7 = 180.f;
static real c_b8 = -90.f;
static real c_b9 = 90.f;
static real c_b10 = .2f;
static real c_b11 = .8f;
static real c_b12 = .3f;
static real c_b13 = .7f;
static integer c_n1 = -1;
static real c_b18 = 0.f;
static logical c_false = FALSE_;
static real c_b26 = 10.f;
static real c_b27 = 60.f;
static real c_b36 = 30.f;
static real c_b46 = .5f;
static real c_b52 = .9f;
static real c_b58 = -.9f;

/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static integer iws;
    extern /* Subroutine */ int grcls_(void), grfrm_(void), gropn_(integer *),
	     uzfact_(real *), sgswnd_(real *, real *, real *, real *), 
	    sgstrf_(void), uxaxdv_(char *, real *, real *, ftnlen), sgstrn_(
	    integer *), sgpwsn_(void), uyaxdv_(char *, real *, real *, ftnlen)
	    , uziset_(char *, integer *, ftnlen), sgsvpt_(real *, real *, 
	    real *, real *), uzlset_(char *, logical *, ftnlen), uzrset_(char 
	    *, real *, ftnlen), uxmttl_(char *, char *, real *, ftnlen, 
	    ftnlen), uxsttl_(char *, char *, real *, ftnlen, ftnlen), uysttl_(
	    char *, char *, real *, ftnlen, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___1 = { 0, 6, 0, 0, 0 };
    static cilist io___2 = { 0, 5, 0, 0, 0 };


    s_wsle(&io___1);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___2);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    gropn_(&iws);
    grfrm_();
    sgswnd_(&c_b6, &c_b7, &c_b8, &c_b9);
    sgsvpt_(&c_b10, &c_b11, &c_b12, &c_b13);
    sgstrn_(&c__1);
    sgstrf_();
    uziset_("INNER", &c_n1, (ftnlen)5);
    uzrset_("UXUSER", &c_b18, (ftnlen)6);
    uzrset_("UYUSER", &c_b18, (ftnlen)6);
    uzlset_("LABELXU", &c_false, (ftnlen)7);
    uzlset_("LABELYU", &c_false, (ftnlen)7);
    uxaxdv_("B", &c_b26, &c_b27, (ftnlen)1);
    uxaxdv_("T", &c_b26, &c_b27, (ftnlen)1);
    uxsttl_("B", "LONGITUDE", &c_b18, (ftnlen)1, (ftnlen)9);
    uyaxdv_("L", &c_b26, &c_b36, (ftnlen)1);
    uyaxdv_("R", &c_b26, &c_b36, (ftnlen)1);
    uysttl_("L", "LATITUDE", &c_b18, (ftnlen)1, (ftnlen)8);
    uxmttl_("T", "UXAXDV/UYAXDV", &c_b18, (ftnlen)1, (ftnlen)13);
    uzfact_(&c_b46);
    uxaxdv_("U", &c_b26, &c_b27, (ftnlen)1);
    uxsttl_("U", "EQ", &c_b52, (ftnlen)1, (ftnlen)2);
    uyaxdv_("U", &c_b26, &c_b36, (ftnlen)1);
    uysttl_("U", "GM", &c_b58, (ftnlen)1, (ftnlen)2);
    uziset_("INNER", &c__1, (ftnlen)5);
    uxaxdv_("U", &c_b26, &c_b27, (ftnlen)1);
    uyaxdv_("U", &c_b26, &c_b36, (ftnlen)1);
    grcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int uxyz08_ () { MAIN__ (); return 0; }

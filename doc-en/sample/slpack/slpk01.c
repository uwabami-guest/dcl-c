/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static real c_b6 = .1f;
static real c_b8 = .05f;
static integer c__5 = 5;
static real c_b17 = 1.f;
static integer c__2 = 2;
static real c_b25 = .5f;

/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void), s_wsfi(icilist *), 
	    do_fio(integer *, char *, ftnlen), e_wsfi(void);

    /* Local variables */
    static integer i__, iws;
    static char ctxt[7];
    extern /* Subroutine */ int sgcls_(void), sgfrm_(void), slmgn_(real *, 
	    real *, real *, real *), sldiv_(char *, integer *, integer *, 
	    ftnlen), slrat_(real *, real *), sgopn_(integer *), sgtxv_(real *,
	     real *, char *, ftnlen), sgpwsn_(void), sgstxi_(integer *), 
	    sgstxs_(real *), slpwwr_(integer *);

    /* Fortran I/O blocks */
    static cilist io___1 = { 0, 6, 0, 0, 0 };
    static cilist io___2 = { 0, 5, 0, 0, 0 };
    static icilist io___6 = { 0, ctxt, 0, "('FRAME',I2.2)", 7, 1 };


    s_wsle(&io___1);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___2);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    i__1 = -abs(iws);
    sgopn_(&i__1);
    slmgn_(&c_b6, &c_b6, &c_b8, &c_b8);
/* <-- 第1 */
    sldiv_("S", &c__3, &c__5, (ftnlen)1);
/* <-- フレーム分割 (第2レベル) */
    slmgn_(&c_b8, &c_b8, &c_b8, &c_b8);
/* <-- 第2 */
    slrat_(&c_b17, &c_b17);
/* <-- 第2 */
    sgstxi_(&c__2);
    sgstxs_(&c_b6);
    for (i__ = 1; i__ <= 15; ++i__) {
	sgfrm_();
/* <-- */
	slpwwr_(&c__1);
/* <    この中は普通に1 */
	s_wsfi(&io___6);
	do_fio(&c__1, (char *)&i__, (ftnlen)sizeof(integer));
	e_wsfi();
/* < */
	sgtxv_(&c_b25, &c_b25, ctxt, (ftnlen)7);
/* <-- */
/* L100: */
    }
    sgcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int slpk01_ () { MAIN__ (); return 0; }

/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static real c_b7 = 0.f;
static real c_b8 = 10.f;
static real c_b12 = 1.f;
static integer c__2 = 2;
static logical c_false = FALSE_;
static real c_b22 = .05f;
static real c_b28 = 2.f;
static real c_b29 = 3.f;

/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* System generated locals */
    real r__1, r__2;

    /* Builtin functions */
    double cos(doublereal), sin(doublereal);
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static integer i__, n;
    static real x1, y1, rx[12], ry[12];
    static integer iws;
    extern /* Subroutine */ int sgcls_(void), sgfrm_(void), sgopn_(integer *),
	     sglset_(char *, logical *, ftnlen), sgswnd_(real *, real *, real 
	    *, real *), sglazu_(real *, real *, real *, real *, integer *, 
	    integer *), sgrset_(char *, real *, ftnlen), sgstrf_(void), 
	    sgstrn_(integer *), sgpwsn_(void), sgsvpt_(real *, real *, real *,
	     real *);

    /* Fortran I/O blocks */
    static cilist io___4 = { 0, 6, 0, 0, 0 };
    static cilist io___5 = { 0, 5, 0, 0, 0 };


    for (n = 1; n <= 12; ++n) {
	rx[n - 1] = n * .25f * cos((n - 1) * .52359866666666677f);
	ry[n - 1] = n * .25f * sin((n - 1) * .52359866666666677f);
/* L10: */
    }
    s_wsle(&io___4);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___5);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    sgopn_(&iws);
    sgfrm_();
    sgswnd_(&c_b7, &c_b8, &c_b7, &c_b8);
    sgsvpt_(&c_b7, &c_b12, &c_b7, &c_b12);
    sgstrn_(&c__1);
    sgstrf_();
    x1 = 3.f;
    y1 = 7.f;
    for (n = 1; n <= 12; ++n) {
	r__1 = x1 + rx[n - 1];
	r__2 = y1 + ry[n - 1];
	sglazu_(&x1, &y1, &r__1, &r__2, &c__1, &c__2);
/* L15: */
    }
    sglset_("LPROP", &c_false, (ftnlen)5);
    sgrset_("CONST", &c_b22, (ftnlen)5);
    x1 = 7.f;
    y1 = 7.f;
    for (n = 1; n <= 12; ++n) {
	r__1 = x1 + rx[n - 1];
	r__2 = y1 + ry[n - 1];
	sglazu_(&x1, &y1, &r__1, &r__2, &c__1, &c__2);
/* L20: */
    }
    for (i__ = 1; i__ <= 8; ++i__) {
	r__1 = i__ * 10.f;
	sgrset_("ANGLE", &r__1, (ftnlen)5);
	r__1 = (real) i__;
	r__2 = (real) i__ + 1;
	sglazu_(&r__1, &c_b28, &r__2, &c_b29, &c__1, &c__2);
/* L25: */
    }
    sgcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int sgpk10_ () { MAIN__ (); return 0; }

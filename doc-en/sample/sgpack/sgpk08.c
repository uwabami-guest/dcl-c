/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static logical c_true = TRUE_;
static real c_b11 = -1.f;
static real c_b12 = 1.f;
static real c_b15 = 0.f;
static real c_b16 = .5f;
static integer c__101 = 101;
static integer c__6 = 6;
static integer c__201 = 201;
static integer c__61 = 61;
static integer c__601 = 601;

/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Builtin functions */
    double sin(doublereal), cos(doublereal);
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static real a;
    static integer i__;
    static real th;
    static integer iws;
    static real upx3[3], upy3[3], upx6[6], upy6[6], upxs[61], upys[61];
    extern /* Subroutine */ int sgcls_(void), sgfrm_(void), sgopn_(integer *),
	     sgplu_(integer *, real *, real *), sgtnu_(integer *, real *, 
	    real *), sglset_(char *, logical *, ftnlen), sgswnd_(real *, real 
	    *, real *, real *), sgstrf_(void), sgstnp_(integer *), sgstrn_(
	    integer *), sgpwsn_(void), sgsvpt_(real *, real *, real *, real *)
	    , sgtnzu_(integer *, real *, real *, integer *);

    /* Fortran I/O blocks */
    static cilist io___10 = { 0, 6, 0, 0, 0 };
    static cilist io___11 = { 0, 5, 0, 0, 0 };


    a = .8f;
    th = 2.0943933333333331f;
    for (i__ = 1; i__ <= 3; ++i__) {
	upx3[i__ - 1] = a * sin(th * i__);
	upy3[i__ - 1] = a * cos(th * i__);
/* L100: */
    }
    th = 1.0471966666666666f;
    for (i__ = 1; i__ <= 6; ++i__) {
	upx6[i__ - 1] = a * sin(th * i__);
	upy6[i__ - 1] = a * cos(th * i__);
/* L200: */
    }
    th = .20943933333333334f;
    for (i__ = 1; i__ <= 61; ++i__) {
	upxs[i__ - 1] = a * (i__ - 31) / 30.f;
	upys[i__ - 1] = a * sin(th * (i__ - 1));
/* L300: */
    }
    s_wsle(&io___10);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___11);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    sgopn_(&iws);
    sglset_("LSOFTF", &c_true, (ftnlen)6);
/* <-- */
    sgfrm_();
    sgswnd_(&c_b11, &c_b12, &c_b11, &c_b12);
    sgsvpt_(&c_b15, &c_b16, &c_b15, &c_b16);
    sgstrn_(&c__1);
    sgstrf_();
    sgplu_(&c__3, upx3, upy3);
    sgtnu_(&c__3, upx3, upy3);
/* <-- �֤��� (����) */
    sgsvpt_(&c_b15, &c_b16, &c_b16, &c_b12);
    sgstrf_();
    sgstnp_(&c__101);
    sgtnu_(&c__6, upx6, upy6);
/* <-- ���� (����) */
    sgsvpt_(&c_b16, &c_b12, &c_b15, &c_b16);
    sgstrf_();
    sgtnzu_(&c__6, upx6, upy6, &c__201);
/* <-- ���� (����) */
    sgsvpt_(&c_b16, &c_b12, &c_b16, &c_b12);
    sgstrf_();
    sgtnzu_(&c__61, upxs, upys, &c__601);
/* <-- ���� (����) */
    sgcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int sgpk08_ () { MAIN__ (); return 0; }

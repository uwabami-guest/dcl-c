/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static logical c_true = TRUE_;
static real c_b11 = 0.f;
static real c_b12 = 181.f;
static real c_b13 = .2f;
static real c_b14 = .8f;
static integer c__181 = 181;
static integer c__26 = 26;
static integer c__4 = 4;
static integer c__7 = 7;

/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Initialized data */

    static char ch[4*7] = "JAN " "FEB " "MAR " "APL " "JUN " "JULY" "    ";
    static real ux2[7] = { 0.f,31.f,59.f,90.f,120.f,151.f,181.f };

    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static doublereal a;
    static integer i__;
    static doublereal r__;
    static real t[181], y[181], ux1[26];
    static integer iws;
    extern /* Subroutine */ int grcls_(void), grfrm_(void), gropn_(integer *),
	     sgplu_(integer *, real *, real *);
    static real rundef;
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen), uxaxlb_(char 
	    *, real *, integer *, real *, char *, integer *, integer *, 
	    ftnlen, ftnlen), grswnd_(real *, real *, real *, real *), grstrf_(
	    void), uspfit_(void), uslset_(char *, logical *, ftnlen), grstrn_(
	    integer *), sgpwsn_(void), grsvpt_(real *, real *, real *, real *)
	    , uzlset_(char *, logical *, ftnlen), usspnt_(integer *, real *, 
	    real *), usyaxs_(char *, ftnlen), uxsttl_(char *, char *, real *, 
	    ftnlen, ftnlen), uysttl_(char *, char *, real *, ftnlen, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___9 = { 0, 6, 0, 0, 0 };
    static cilist io___10 = { 0, 5, 0, 0, 0 };


    r__ = .2;
    a = 4.;
    y[0] = 120.f;
    t[0] = .5f;
    for (i__ = 2; i__ <= 181; ++i__) {
	r__ = a * r__ * (1. - r__);
	y[i__ - 1] = y[i__ - 2] + (r__ - .46f) * 2;
	t[i__ - 1] = i__ - .5f;
/* L10: */
    }
    for (i__ = 1; i__ <= 26; ++i__) {
	ux1[i__ - 1] = (real) ((i__ - 1) * 7 + 1);
/* L20: */
    }
/* ----------------------------------------------------------------------- */
    s_wsle(&io___9);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___10);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    glrget_("RUNDEF", &rundef, (ftnlen)6);
    uslset_("LYINV", &c_true, (ftnlen)5);
    gropn_(&iws);
    grfrm_();
    grswnd_(&c_b11, &c_b12, &rundef, &rundef);
    grsvpt_(&c_b13, &c_b14, &c_b13, &c_b14);
    grstrn_(&c__1);
    usspnt_(&c__181, &rundef, y);
    uspfit_();
    grstrf_();
/* ------------------------------- Y-AXIS -------------------------------- */
    usyaxs_("L", (ftnlen)1);
    usyaxs_("R", (ftnlen)1);
    uysttl_("L", "YEN/DOLLAR", &c_b11, (ftnlen)1, (ftnlen)10);
/* ------------------------------- X-AXIS -------------------------------- */
    uzlset_("LBTWN", &c_true, (ftnlen)5);
    uxaxlb_("B", ux1, &c__26, ux2, ch, &c__4, &c__7, (ftnlen)1, (ftnlen)4);
    uxaxlb_("T", ux1, &c__26, ux2, ch, &c__4, &c__7, (ftnlen)1, (ftnlen)4);
    uxsttl_("T", "EXCHANGE RATE", &c_b11, (ftnlen)1, (ftnlen)13);
/* -------------------------------- LINE --------------------------------- */
    sgplu_(&c__181, t, y);
    grcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int uspk11_ () { MAIN__ (); return 0; }

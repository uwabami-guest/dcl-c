/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c_n1 = -1;
static logical c_true = TRUE_;
static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static real c_b15 = .2f;
static real c_b16 = .8f;
static real c_b18 = .6f;
static integer c__1000 = 1000;
static integer c__200 = 200;
static real c_b30 = .62f;
static real c_b31 = .82f;
static logical c_false = FALSE_;
static real c_b43 = .5f;

/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Builtin functions */
    integer pow_ii(integer *, integer *);
    double cos(doublereal);
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static real a[5];
    static integer i__, j;
    static real t, x[200], y[200];
    static integer jj;
    static real dt, pi;
    static integer ip;
    static real yc[1000]	/* was [200][5] */;
    static integer iws;
    extern /* Subroutine */ int grfig_(void), grcls_(void), grfrm_(void), 
	    gropn_(integer *);
    static real rundef;
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen), uzfact_(real 
	    *), usgrph_(integer *, real *, real *), sgpwsn_(void), grsvpt_(
	    real *, real *, real *, real *), uulinz_(integer *, real *, real *
	    , integer *, integer *), uzlset_(char *, logical *, ftnlen), 
	    swcstx_(char *, char *, ftnlen, ftnlen), usspnt_(integer *, real *
	    , real *), ussttl_(char *, char *, char *, char *, ftnlen, ftnlen,
	     ftnlen, ftnlen), swlstx_(char *, logical *, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___11 = { 0, 6, 0, 0, 0 };
    static cilist io___12 = { 0, 5, 0, 0, 0 };


    dt = .0050251256281407036f;
    pi = 3.14159f;
    for (j = 1; j <= 5; ++j) {
	jj = (j << 1) - 1;
	a[j - 1] = pow_ii(&c_n1, &j) * 2.f / (jj * pi);
/* L50: */
    }
    for (i__ = 1; i__ <= 200; ++i__) {
	t = dt * (i__ - 1) * 2 * pi;
	x[i__ - 1] = dt * (i__ - 1);
	y[i__ - 1] = 0.f;
	for (j = 1; j <= 5; ++j) {
	    jj = (j << 1) - 1;
	    yc[i__ + j * 200 - 201] = a[j - 1] * cos(jj * t);
	    y[i__ - 1] += yc[i__ + j * 200 - 201];
/* L150: */
	}
/* L100: */
    }
/* --------------------------- 1ST PAGE ---------------------------------- */
    swcstx_("FNAME", "USPK09", (ftnlen)5, (ftnlen)6);
    swlstx_("LSEP", &c_true, (ftnlen)4);
    s_wsle(&io___11);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___12);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    glrget_("RUNDEF", &rundef, (ftnlen)6);
    gropn_(&iws);
    grfrm_();
    grsvpt_(&c_b15, &c_b16, &c_b15, &c_b18);
    usspnt_(&c__1000, &rundef, yc);
    ussttl_("X-AXIS", " ", "COMPONENTS", " ", (ftnlen)6, (ftnlen)1, (ftnlen)
	    10, (ftnlen)1);
    usgrph_(&c__200, x, yc);
    for (j = 2; j <= 5; ++j) {
	ip = (j - 1) % 4 + 1;
	uulinz_(&c__200, x, &yc[j * 200 - 200], &c__1, &ip);
/* L200: */
    }
/*     --- NEW FIG --- */
    grfig_();
    grsvpt_(&c_b15, &c_b16, &c_b30, &c_b31);
    uzlset_("LABELXB", &c_false, (ftnlen)7);
    ussttl_("X-AXIS", " ", "TOTAL", " ", (ftnlen)6, (ftnlen)1, (ftnlen)5, (
	    ftnlen)1);
    usgrph_(&c__200, x, y);
/* --------------------------- 2ND PAGE ---------------------------------- */
    grfrm_();
    grsvpt_(&c_b15, &c_b16, &c_b15, &c_b18);
    uzfact_(&c_b43);
    uzlset_("LABELXB", &c_true, (ftnlen)7);
    usspnt_(&c__1000, &rundef, yc);
    ussttl_("X-AXIS", " ", "COMPONENTS", " ", (ftnlen)6, (ftnlen)1, (ftnlen)
	    10, (ftnlen)1);
    usgrph_(&c__200, x, yc);
    for (j = 2; j <= 5; ++j) {
	ip = (j - 1) % 4 + 1;
	uulinz_(&c__200, x, &yc[j * 200 - 200], &c__1, &ip);
/* L300: */
    }
/*     --- NEW FIG --- */
    grfig_();
    grsvpt_(&c_b15, &c_b16, &c_b30, &c_b31);
    uzlset_("LABELXB", &c_false, (ftnlen)7);
    ussttl_("X-AXIS", " ", "TOTAL", " ", (ftnlen)6, (ftnlen)1, (ftnlen)5, (
	    ftnlen)1);
    usgrph_(&c__200, x, y);
    grcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int uspk09_ () { MAIN__ (); return 0; }

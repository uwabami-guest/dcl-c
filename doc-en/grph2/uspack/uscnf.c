/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static real c_b15 = 0.f;
static real c_b17 = 5e3f;
static real c_b21 = 2e3f;
static integer c_n1 = -1;
static logical c_false = FALSE_;
static real c_b33 = .02f;

/* ======================================================================= */
/*     USGRPH  FIGURE FOR STANDARD UNIT. */
/* ======================================================================= */
/* Main program */ int MAIN__(void)
{
    /* System generated locals */
    real r__1, r__2, r__3, r__4;

    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);
    /* Subroutine */ int s_stop(char *, ftnlen);

    /* Local variables */
    static real x1, x2, x3, y1, y2, y3, y4, sx, sy, vx0, vy0, vy1, vy2, vx1, 
	    vx2, vx3, vy3, vy4;
    static integer iws;
    static real pad1, rszc1, rszl1, rszt2;
    extern /* Subroutine */ int grcls_(void), sglav_(real *, real *, real *, 
	    real *), grfrm_(void), gropn_(integer *);
    static real soffy;
    extern /* Subroutine */ int sglnv_(real *, real *, real *, real *);
    static real vxmin, vymin, vxmax, vymax;
    extern /* Subroutine */ int sgtxv_(real *, real *, char *, ftnlen), 
	    sgpset_(), uscset_(char *, char *, ftnlen, ftnlen), grstrf_(void),
	     usdaxs_(void), uspget_(), uspfit_(void);
    static integer mxdgts, mxdgty;
    extern /* Subroutine */ int uzpget_(char *, real *, ftnlen), sgqvpt_(real 
	    *, real *, real *, real *), uzpset_(char *, integer *, ftnlen), 
	    sgstxs_(real *);

    /* Fortran I/O blocks */
    static cilist io___1 = { 0, 6, 0, 0, 0 };
    static cilist io___2 = { 0, 5, 0, 0, 0 };


    s_wsle(&io___1);
    do_lio(&c__9, &c__1, "workstation no.? ", (ftnlen)17);
    e_wsle();
    s_rsle(&io___2);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    gropn_(&iws);
    grfrm_();
/* ------------------------ USPACK ROUTINES ------------------------------ */
    uscset_("CXTTL", "X-TITLE", (ftnlen)5, (ftnlen)7);
    uscset_("CYTTL", "Y-TITLE", (ftnlen)5, (ftnlen)7);
    uscset_("CXUNIT", "XSUB", (ftnlen)6, (ftnlen)4);
    uscset_("CYUNIT", "YSUB", (ftnlen)6, (ftnlen)4);
    sgpset_("UYMIN", &c_b15, (ftnlen)5);
    sgpset_("UYMAX", &c_b17, (ftnlen)5);
    sgpset_("UXMIN", &c_b15, (ftnlen)5);
    sgpset_("UXMAX", &c_b21, (ftnlen)5);
    uzpset_("INNER", &c_n1, (ftnlen)5);
    uspfit_();
    grstrf_();
    usdaxs_();
/* ----------------------------------------------------------------------- */
    uzpget_("RSIZET2", &rszt2, (ftnlen)7);
    uzpget_("RSIZEL1", &rszl1, (ftnlen)7);
    uzpget_("RSIZEC1", &rszc1, (ftnlen)7);
    uzpget_("PAD1", &pad1, (ftnlen)4);
    uspget_("MXDGTY", &mxdgty, (ftnlen)6);
    uspget_("MXDGTSY", &mxdgts, (ftnlen)7);
    uspget_("SOFFYLT", &soffy, (ftnlen)7);
    sgqvpt_(&vxmin, &vxmax, &vymin, &vymax);
    x1 = rszt2;
    x2 = (mxdgty + pad1) * rszl1;
    x3 = (pad1 + 1) * rszc1;
    y1 = rszt2;
    y2 = (pad1 + 1) * rszl1;
    y3 = (pad1 + 1) * rszl1;
    y4 = (pad1 + 1) * rszc1;
    sx = rszt2 + (mxdgts + pad1) * rszl1;
    sy = soffy;
/* ----------------------------- Y-AXIS ---------------------------------- */
    sgpset_("LPROP", &c_false, (ftnlen)5);
    sgstxs_(&c_b33);
    vy1 = (vymin + vymax) / 2.f;
    vy2 = vy1 + (vymax - vymin) / 5.f * 2.f;
    vy0 = (vy1 + vy2) / 2;
    vx1 = vxmin - x1;
    vx2 = vx1 - x2;
    vx3 = vx2 - x3;
    sglnv_(&vx1, &vy1, &vx1, &vy2);
    sglnv_(&vx2, &vy1, &vx2, &vy2);
    sglnv_(&vx3, &vy1, &vx3, &vy2);
    sglav_(&vx3, &vy0, &vx2, &vy0);
    sglav_(&vx2, &vy0, &vx3, &vy0);
    sglav_(&vx2, &vy0, &vx1, &vy0);
    sglav_(&vx1, &vy0, &vx2, &vy0);
    r__1 = vxmin + .02f;
    sglav_(&r__1, &vy0, &vxmin, &vy0);
    r__1 = vxmin + .02f;
    r__2 = vy0 + .02f;
    sgtxv_(&r__1, &r__2, "x_1\"", (ftnlen)4);
    r__1 = (vx1 + vx2) / 2.f;
    r__2 = vy0 + .02f;
    sgtxv_(&r__1, &r__2, "x_2\"", (ftnlen)4);
    r__1 = (vx2 + vx3) / 2.f;
    r__2 = vy0 + .02f;
    sgtxv_(&r__1, &r__2, "x_3\"", (ftnlen)4);
/* ---------------------------- SUB LABEL -------------------------------- */
    vy0 = vymax + sy * 1.5f;
    vy1 = vymax + sy * 2;
    sglnv_(&vxmin, &vymax, &vxmin, &vy1);
    r__1 = vxmin - sx;
    r__2 = vxmin - sx;
    sglnv_(&r__1, &vymax, &r__2, &vy1);
    r__1 = vxmin - sx;
    sglav_(&r__1, &vy0, &vxmin, &vy0);
    r__1 = vxmin - sx;
    sglav_(&vxmin, &vy0, &r__1, &vy0);
    r__1 = vxmin - sx / 2.f;
    r__2 = vy0 + .02f;
    sgtxv_(&r__1, &r__2, "s_x\"", (ftnlen)4);
    r__1 = vxmin - x2 / 2.f;
    r__2 = vymax + sy;
    r__3 = vxmin + x2 / 2.f;
    r__4 = vymax + sy;
    sglnv_(&r__1, &r__2, &r__3, &r__4);
    r__1 = vxmin + x2 / 3.f;
    r__2 = vymax + sy;
    r__3 = vxmin + x2 / 3.f;
    sglav_(&r__1, &r__2, &r__3, &vymax);
    r__1 = vxmin + x2 / 3.f;
    r__2 = vxmin + x2 / 3.f;
    r__3 = vymax + sy;
    sglav_(&r__1, &vymax, &r__2, &r__3);
    r__1 = vxmin + x2 / 2.f;
    r__2 = vymax + sy / 2.f;
    sgtxv_(&r__1, &r__2, "s_y\"", (ftnlen)4);
/* ----------------------------- X-AXIS ---------------------------------- */
    vy1 = vymin - y1;
    vy2 = vy1 - y2;
    vy3 = vy2 - y3;
    vy4 = vy3 - y4;
    vx1 = vxmin + (vxmax - vxmin) * 2 / 3.f;
    vx2 = vxmax + x2;
    vx0 = vx2 - .03f;
    sglnv_(&vx1, &vy1, &vx2, &vy1);
    sglnv_(&vx1, &vy2, &vx2, &vy2);
    sglnv_(&vx1, &vy3, &vx2, &vy3);
    sglnv_(&vx1, &vy4, &vx2, &vy4);
    sglnv_(&vxmax, &vymin, &vx2, &vymin);
    sglav_(&vx0, &vy4, &vx0, &vy3);
    sglav_(&vx0, &vy3, &vx0, &vy4);
    sglav_(&vx0, &vy3, &vx0, &vy2);
    sglav_(&vx0, &vy2, &vx0, &vy3);
    sglav_(&vx0, &vy2, &vx0, &vy1);
    sglav_(&vx0, &vy1, &vx0, &vy2);
    r__1 = vymin + .02f;
    sglav_(&vx0, &r__1, &vx0, &vymin);
    r__1 = vymin + .02f;
    sgtxv_(&vx2, &r__1, "y_1\"", (ftnlen)4);
    r__1 = (vy1 + vy2) / 2.f;
    sgtxv_(&vx2, &r__1, "y_2\"", (ftnlen)4);
    r__1 = (vy2 + vy3) / 2.f;
    sgtxv_(&vx2, &r__1, "y_3\"", (ftnlen)4);
    r__1 = (vy3 + vy4) / 2.f;
    sgtxv_(&vx2, &r__1, "y_4\"", (ftnlen)4);
    grcls_();
    s_stop("", (ftnlen)0);
    return 0;
} /* MAIN__ */


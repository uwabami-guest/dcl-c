/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static real c_b6 = .5f;
static real c_b7 = .9f;
static real c_b9 = .04f;
static integer c__0 = 0;
static real c_b18 = .1f;
static real c_b19 = .02f;
static integer c_n1 = -1;
static real c_b26 = .015f;
static integer c__2 = 2;

/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* System generated locals */
    address a__1[3];
    integer i__1[3];
    real r__1;
    char ch__1[18];

    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_wsfi(icilist *), do_fio(integer *, char *, ftnlen), e_wsfi(void)
	    ;
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen);

    /* Local variables */
    static integer i__;
    static real x[2], y[2];
    static integer iws;
    static char cpat[16], cttl[16];
    extern /* Subroutine */ int sgcls_(void), sgfrm_(void), sgopn_(integer *);
    static integer itype;
    extern /* Subroutine */ int bitpci_(char *, integer *, ftnlen), sgsplt_(
	    integer *), sgpwsn_(void), sgplzv_(integer *, real *, real *, 
	    integer *, integer *), sgtxzv_(real *, real *, char *, real *, 
	    integer *, integer *, integer *, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___1 = { 0, 6, 0, 0, 0 };
    static cilist io___2 = { 0, 5, 0, 0, 0 };
    static icilist io___9 = { 0, cttl+8, 0, "(I5)", 5, 1 };


    s_wsle(&io___1);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___2);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    sgopn_(&iws);
    sgfrm_();
    sgtxzv_(&c_b6, &c_b7, "LINE TYPE", &c_b9, &c__0, &c__0, &c__3, (ftnlen)9);
    x[0] = .4f;
    x[1] = .8f;
    for (i__ = 1; i__ <= 5; ++i__) {
	if (1 <= i__ && i__ <= 4) {
	    itype = i__;
	} else {
	    s_copy(cpat, "0011111111001001", (ftnlen)16, (ftnlen)16);
	    bitpci_(cpat, &itype, (ftnlen)16);
	    sgsplt_(&itype);
	}
	s_copy(cttl, "ITYPE = #####", (ftnlen)16, (ftnlen)13);
	s_wsfi(&io___9);
	do_fio(&c__1, (char *)&itype, (ftnlen)sizeof(integer));
	e_wsfi();
	y[0] = .7f - (i__ - 1) * .12f;
	y[1] = y[0];
	sgtxzv_(&c_b18, y, cttl, &c_b19, &c__0, &c_n1, &c__3, (ftnlen)16);
	if (i__ == 5) {
	    r__1 = y[0] - .05f;
/* Writing concatenation */
	    i__1[0] = 1, a__1[0] = "(";
	    i__1[1] = 16, a__1[1] = cpat;
	    i__1[2] = 1, a__1[2] = ")";
	    s_cat(ch__1, a__1, i__1, &c__3, (ftnlen)18);
	    sgtxzv_(&c_b18, &r__1, ch__1, &c_b26, &c__0, &c_n1, &c__3, (
		    ftnlen)18);
	}
	sgplzv_(&c__2, x, y, &itype, &c__3);
/* L10: */
    }
    sgcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int sgltyp_ () { MAIN__ (); return 0; }

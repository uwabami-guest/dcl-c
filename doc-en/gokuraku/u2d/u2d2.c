/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static doublereal c_b2 = .5;
static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static logical c_true = TRUE_;
static real c_b11 = 0.f;
static real c_b12 = 5.f;
static real c_b13 = 20.f;
static real c_b14 = 50.f;
static real c_b15 = .2f;
static real c_b16 = .9f;
static real c_b17 = .4f;
static real c_b18 = .8f;
static integer c__51 = 51;
static integer c__21 = 21;

/* Main program */ int MAIN__(void)
{
    /* System generated locals */
    doublereal d__1;

    /* Builtin functions */
    double exp(doublereal), pow_dd(doublereal *, doublereal *), sin(
	    doublereal);
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static integer i__, j;
    static real t, u[1071]	/* was [51][21] */, z__, uz;
    static integer iws;
    extern /* Subroutine */ int grcls_(void), grfrm_(void), gropn_(integer *),
	     udcntr_(real *, integer *, integer *, integer *), uetone_(real *,
	     integer *, integer *, integer *), sglset_(char *, logical *, 
	    ftnlen), grswnd_(real *, real *, real *, real *), grstrf_(void), 
	    usdaxs_(void), uspfit_(void), sgpwsn_(void), grsvpt_(real *, real 
	    *, real *, real *), ussttl_(char *, char *, char *, char *, 
	    ftnlen, ftnlen, ftnlen, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___7 = { 0, 6, 0, 0, 0 };
    static cilist io___8 = { 0, 5, 0, 0, 0 };


/* -- データ ---- */
    for (j = 1; j <= 21; ++j) {
	z__ = (j - 1) * 1.5f;
	d__1 = (doublereal) z__;
	uz = exp(z__ * -.2f) * pow_dd(&d__1, &c_b2);
	for (i__ = 1; i__ <= 51; ++i__) {
	    t = (i__ - 1) * .10000000000000001f - exp(z__ * -.1f) * 2.f;
	    u[i__ + j * 51 - 52] = uz * sin(t * 3.f);
/* L20: */
	}
/* L10: */
    }
/* -- グラフ ---- */
    s_wsle(&io___7);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___8);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    gropn_(&iws);
    sglset_("LSOFTF", &c_true, (ftnlen)6);
    grfrm_();
    grswnd_(&c_b11, &c_b12, &c_b13, &c_b14);
    grsvpt_(&c_b15, &c_b16, &c_b17, &c_b18);
    uspfit_();
    grstrf_();
    ussttl_("TIME", "YEAR", "HEIGHT", "km", (ftnlen)4, (ftnlen)4, (ftnlen)6, (
	    ftnlen)2);
    usdaxs_();
    udcntr_(u, &c__51, &c__51, &c__21);
    uetone_(u, &c__51, &c__51, &c__21);
    grcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int u2d2_ () { MAIN__ (); return 0; }

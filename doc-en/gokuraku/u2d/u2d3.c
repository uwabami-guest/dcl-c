/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static real c_b7 = -1.f;
static real c_b8 = 1.f;
static integer c__21 = 21;

/* Main program */ int MAIN__(void)
{
    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static integer i__, j;
    static real u[441]	/* was [21][21] */, v[441]	/* was [21][21] */, x,
	     y;
    static integer iws;
    extern /* Subroutine */ int grcls_(void), grfrm_(void), gropn_(integer *),
	     ugvect_(real *, integer *, real *, integer *, integer *, integer 
	    *), grswnd_(real *, real *, real *, real *), grstrf_(void), 
	    usdaxs_(void), uspfit_(void), sgpwsn_(void), ussttl_(char *, char 
	    *, char *, char *, ftnlen, ftnlen, ftnlen, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___7 = { 0, 6, 0, 0, 0 };
    static cilist io___8 = { 0, 5, 0, 0, 0 };


/* -- データ ---- */
    for (j = 1; j <= 21; ++j) {
	for (i__ = 1; i__ <= 21; ++i__) {
	    x = (i__ - 1) * .10000000000000001f - 1.f;
	    y = (j - 1) * .10000000000000001f - 1.f;
	    u[i__ + j * 21 - 22] = x;
	    v[i__ + j * 21 - 22] = -y;
/* L10: */
	}
    }
/* -- グラフ ---- */
    s_wsle(&io___7);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___8);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    gropn_(&iws);
    grfrm_();
    grswnd_(&c_b7, &c_b8, &c_b7, &c_b8);
    uspfit_();
    grstrf_();
    ussttl_("X", "km", "Y", "km", (ftnlen)1, (ftnlen)2, (ftnlen)1, (ftnlen)2);
    usdaxs_();
    ugvect_(u, &c__21, v, &c__21, &c__21, &c__21);
    grcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int u2d3_ () { MAIN__ (); return 0; }

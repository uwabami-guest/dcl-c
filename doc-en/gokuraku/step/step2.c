/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static integer c__100 = 100;
static integer c__5 = 5;
static real c_b13 = .015f;
static integer c__25 = 25;
static integer c__2 = 2;
static integer c__4 = 4;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static integer n;
    static real x[100], y[100];
    static integer iws;
    extern real rngu0_(integer *);
    static integer iseed;
    extern /* Subroutine */ int grcls_(void), grfrm_(void), gropn_(integer *),
	     uumrk_(integer *, real *, real *), grstrf_(void), usdaxs_(void), 
	    uspfit_(void), uusmki_(integer *), sgpwsn_(void), uusmks_(real *),
	     uusmkt_(integer *), usspnt_(integer *, real *, real *), ussttl_(
	    char *, char *, char *, char *, ftnlen, ftnlen, ftnlen, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___5 = { 0, 6, 0, 0, 0 };
    static cilist io___6 = { 0, 5, 0, 0, 0 };


/* -- データ ---- */
    iseed = 1;
    x[0] = (rngu0_(&iseed) - .5f) * 2.f;
    for (n = 2; n <= 100; ++n) {
	x[n - 1] = (rngu0_(&iseed) - .5f) * 2.f;
	y[n - 2] = x[n - 1];
/* L10: */
    }
    y[99] = x[0];
/* -- グラフ ---- */
    s_wsle(&io___5);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___6);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    gropn_(&iws);
    grfrm_();
    usspnt_(&c__100, x, y);
    uspfit_();
    grstrf_();
    ussttl_("X-TITLE", "x-unit", "Y-TITLE", "y-unit", (ftnlen)7, (ftnlen)6, (
	    ftnlen)7, (ftnlen)6);
    usdaxs_();
    uusmki_(&c__5);
    uusmks_(&c_b13);
    uumrk_(&c__25, x, y);
    uusmkt_(&c__2);
    uumrk_(&c__25, &x[25], &y[25]);
    uusmkt_(&c__3);
    uumrk_(&c__25, &x[50], &y[50]);
    uusmkt_(&c__4);
    uumrk_(&c__25, &x[75], &y[75]);
    grcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int step2_ () { MAIN__ (); return 0; }

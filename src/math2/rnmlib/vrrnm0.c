/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int vrrnm0_(real *rx, real *ry, integer *n, integer *jx, 
	integer *jy, integer *nb)
{
    /* System generated locals */
    integer i__1;

    /* Local variables */
    static integer j, kx, ky, nb2;
    extern real rave0_(real *, integer *, integer *);
    static real rmiss;
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen), msgdmp_(char 
	    *, char *, char *, ftnlen, ftnlen, ftnlen);

    /* Parameter adjustments */
    --ry;
    --rx;

    /* Function Body */
    if (! (1 <= *nb && *nb <= *n)) {
	msgdmp_("E", "VRRNM0", "AVERAGING LENGTH IS INVALID.", (ftnlen)1, (
		ftnlen)6, (ftnlen)28);
    }
    if (*nb % 2 == 0) {
	msgdmp_("E", "VRRNM0", "AVERAGING LENGTH IS EVEN NUMBER.", (ftnlen)1, 
		(ftnlen)6, (ftnlen)32);
    }
    glrget_("RMISS", &rmiss, (ftnlen)5);
    nb2 = *nb / 2;
    kx = 1 - *jx;
    ky = 1 - *jy;
    i__1 = *n;
    for (j = 1; j <= i__1; ++j) {
	kx += *jx;
	ky += *jy;
	if (nb2 + 1 <= j && j <= *n - nb2) {
	    ry[ky] = rave0_(&rx[kx - *jx * nb2], nb, jx);
	} else {
	    ry[ky] = rmiss;
	}
/* L10: */
    }
    return 0;
} /* vrrnm0_ */


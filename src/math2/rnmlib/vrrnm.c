/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     VRRNM */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int vrrnm_(real *rx, real *ry, integer *n, integer *jx, 
	integer *jy, integer *nb)
{
    static logical lmiss;
    extern /* Subroutine */ int vrrnm0_(real *, real *, integer *, integer *, 
	    integer *, integer *), vrrnm1_(real *, real *, integer *, integer 
	    *, integer *, integer *), gllget_(char *, logical *, ftnlen), 
	    msgdmp_(char *, char *, char *, ftnlen, ftnlen, ftnlen);

    /* Parameter adjustments */
    --ry;
    --rx;

    /* Function Body */
    if (! (1 <= *nb && *nb <= *n)) {
	msgdmp_("E", "VRRNM ", "AVERAGING LENGTH IS INVALID.", (ftnlen)1, (
		ftnlen)6, (ftnlen)28);
    }
    if (*nb % 2 == 0) {
	msgdmp_("E", "VRRNM ", "AVERAGING LENGTH IS EVEN NUMBER.", (ftnlen)1, 
		(ftnlen)6, (ftnlen)32);
    }
    gllget_("LMISS", &lmiss, (ftnlen)5);
    if (lmiss) {
	vrrnm1_(&rx[1], &ry[1], n, jx, jy, nb);
    } else {
	vrrnm0_(&rx[1], &ry[1], n, jx, jy, nb);
    }
    return 0;
} /* vrrnm_ */


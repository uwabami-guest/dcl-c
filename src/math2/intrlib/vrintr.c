/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int vrintr_(real *rx, integer *n, integer *jx)
{
    /* System generated locals */
    integer i__1, i__2;

    /* Local variables */
    static integer j;
    static real x1, xd;
    static integer nn, kx;
    static logical lflag;
    static integer nmiss;
    static real rmiss;
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen);

    /* Parameter adjustments */
    --rx;

    /* Function Body */
    glrget_("RMISS", &rmiss, (ftnlen)5);
    lflag = FALSE_;
    kx = 1 - *jx;
    i__1 = *n;
    for (j = 1; j <= i__1; ++j) {
	kx += *jx;
	if (lflag) {
	    if (rx[kx] == rmiss) {
		if (rx[kx - *jx] != rmiss) {
		    nmiss = 1;
		} else {
		    ++nmiss;
		}
	    } else {
		if (rx[kx - *jx] == rmiss) {
		    x1 = rx[kx - (nmiss + 1) * *jx];
		    xd = (rx[kx] - x1) / (nmiss + 1);
		    i__2 = nmiss;
		    for (nn = 1; nn <= i__2; ++nn) {
			rx[kx - (nmiss + 1 - nn) * *jx] = x1 + xd * nn;
/* L10: */
		    }
		}
	    }
	} else {
	    if (rx[kx] != rmiss) {
		lflag = TRUE_;
	    }
	}
/* L15: */
    }
    return 0;
} /* vrintr_ */


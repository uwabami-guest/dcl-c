/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int vcintr_(complex *cx, integer *n, integer *jx)
{
    /* System generated locals */
    integer i__1, i__2, i__3;
    real r__1;
    complex q__1, q__2;
    doublecomplex z__1, z__2, z__3;

    /* Builtin functions */
    void c_div(complex *, complex *, complex *), pow_zz(doublecomplex *, 
	    doublecomplex *, doublecomplex *), pow_ci(complex *, complex *, 
	    integer *);

    /* Local variables */
    static integer j;
    static complex c1, cd;
    static integer nn, kx;
    static logical lflag;
    static complex cmiss;
    static integer nmiss;
    static real rmiss;
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen);

    /* Parameter adjustments */
    --cx;

    /* Function Body */
    glrget_("RMISS", &rmiss, (ftnlen)5);
    q__1.r = rmiss, q__1.i = rmiss;
    cmiss.r = q__1.r, cmiss.i = q__1.i;
    lflag = FALSE_;
    kx = 1 - *jx;
    i__1 = *n;
    for (j = 1; j <= i__1; ++j) {
	kx += *jx;
	if (lflag) {
	    i__2 = kx;
	    if (cx[i__2].r == cmiss.r && cx[i__2].i == cmiss.i) {
		i__2 = kx - *jx;
		if (! (cx[i__2].r == cmiss.r && cx[i__2].i == cmiss.i)) {
		    nmiss = 1;
		} else {
		    ++nmiss;
		}
	    } else {
		i__2 = kx - *jx;
		if (cx[i__2].r == cmiss.r && cx[i__2].i == cmiss.i) {
		    i__2 = kx - (nmiss + 1) * *jx;
		    c1.r = cx[i__2].r, c1.i = cx[i__2].i;
		    c_div(&q__2, &cx[kx], &c1);
		    z__2.r = q__2.r, z__2.i = q__2.i;
		    r__1 = 1.f / (nmiss + 1);
		    z__3.r = r__1, z__3.i = 0.;
		    pow_zz(&z__1, &z__2, &z__3);
		    q__1.r = z__1.r, q__1.i = z__1.i;
		    cd.r = q__1.r, cd.i = q__1.i;
		    i__2 = nmiss;
		    for (nn = 1; nn <= i__2; ++nn) {
			i__3 = kx - (nmiss + 1 - nn) * *jx;
			pow_ci(&q__2, &cd, &nn);
			q__1.r = c1.r * q__2.r - c1.i * q__2.i, q__1.i = c1.r 
				* q__2.i + c1.i * q__2.r;
			cx[i__3].r = q__1.r, cx[i__3].i = q__1.i;
/* L10: */
		    }
		}
	    }
	} else {
	    i__2 = kx;
	    if (! (cx[i__2].r == cmiss.r && cx[i__2].i == cmiss.i)) {
		lflag = TRUE_;
	    }
	}
/* L15: */
    }
    return 0;
} /* vcintr_ */


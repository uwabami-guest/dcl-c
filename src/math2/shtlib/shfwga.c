/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ********************************************************************** */
/*     TRANSFORM WAVE TO GRID FROM M=M1 TO M=M2 */
/* ********************************************************************** */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int shfwga_(integer *mm, integer *jm, integer *im, integer *
	m1, integer *m2, real *w, real *g, real *h__, real *wfft)
{
    /* System generated locals */
    integer w_dim1, w_offset, g_dim1, g_offset, i__1, i__2;

    /* Local variables */
    static integer i__, j, k, m;
    extern /* Subroutine */ int shfftb_(integer *, real *, real *);

/*     IM.GE.MM+1 */
    /* Parameter adjustments */
    w_dim1 = *jm - (-(*jm)) + 1;
    w_offset = -(*jm) + w_dim1 * (-(*mm));
    w -= w_offset;
    g_dim1 = *im - (-(*im)) + 1;
    g_offset = -(*im) + g_dim1 * (-(*jm));
    g -= g_offset;
    --wfft;

    /* Function Body */
    i__1 = *jm;
    for (j = -(*jm); j <= i__1; ++j) {
	if (*m1 > 0) {
	    h__[0] = 0.f;
	} else {
	    h__[0] = w[j];
	}
	i__2 = *m1 - 1;
	for (m = 1; m <= i__2; ++m) {
	    h__[(m << 1) - 1] = 0.f;
	    h__[m * 2] = 0.f;
/* L10: */
	}
	i__2 = *m2;
	for (m = *m1; m <= i__2; ++m) {
	    h__[(m << 1) - 1] = w[j + m * w_dim1];
	    h__[m * 2] = w[j + -m * w_dim1];
/* L20: */
	}
	i__2 = (*im << 1) - 1;
	for (k = (*m2 << 1) + 1; k <= i__2; ++k) {
	    h__[k] = 0.f;
/* L30: */
	}
	i__2 = *im << 1;
	shfftb_(&i__2, h__, &wfft[1]);
	g[j * g_dim1] = h__[0];
	i__2 = *im;
	for (i__ = 1; i__ <= i__2; ++i__) {
	    g[i__ + j * g_dim1] = h__[i__];
	    g[-i__ + j * g_dim1] = h__[(*im << 1) - i__];
/* L40: */
	}
/* L50: */
    }
    return 0;
} /* shfwga_ */


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* *********************************************************************** */
/*     CALCULATE THE POSITION OF A SPECTRUM COEFFICIENT OF P_N^M */
/* *********************************************************************** */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int shnm2l_(integer *mm, integer *n, integer *m, integer *lr,
	 integer *li)
{
/*     LR: REAL PART, LI: IMAGINARY PART */
    if (*m == 0) {
	*lr = *n + 1;
	*li = *lr;
    } else {
	*lr = *m * ((*mm << 1) - *m + 2) - *mm + *n;
	*li = *lr + *mm - *m + 1;
    }
    return 0;
} /* shnm2l_ */


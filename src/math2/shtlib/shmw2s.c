/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ********************************************************************** */
/*     TRANSFORM WAVE INTO SPECTRA */
/* ********************************************************************** */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int shmw2s_(integer *mm, integer *jm, integer *isw, real *w, 
	real *s, real *sd, real *pm, real *xs, real *xc, real *px, real *wy, 
	real *wx, real *r__, real *z__)
{
    /* System generated locals */
    integer w_dim1, w_offset, pm_dim1, pm_offset, px_dim2, px_offset, wy_dim1,
	     wy_offset, wx_dim1, wx_offset, z_dim1, z_dim2, z_offset, i__1;

    /* Local variables */
    static integer m, n, li, lr;
    extern /* Subroutine */ int shnm2l_(integer *, integer *, integer *, 
	    integer *, integer *), shppma_(integer *, integer *, integer *, 
	    real *, real *, real *, real *), shlfwm_(integer *, integer *, 
	    integer *, integer *, real *, real *, real *, real *, real *, 
	    real *, real *, real *, real *), shmsri_(integer *, integer *, 
	    real *, real *);

    /* Parameter adjustments */
    --r__;
    --s;
    z_dim1 = *jm;
    z_dim2 = *jm - 0 + 1;
    z_offset = 1 + z_dim1 * (0 + z_dim2);
    z__ -= z_offset;
    wx_dim1 = *jm - 0 + 1;
    wx_offset = 0 + wx_dim1;
    wx -= wx_offset;
    wy_dim1 = *jm - 0 + 1;
    wy_offset = 0 + wy_dim1;
    wy -= wy_offset;
    px_dim2 = *jm - 0 + 1;
    px_offset = 1 + 2 * (0 + px_dim2 * 0);
    px -= px_offset;
    pm_dim1 = *mm + 1 - 0 + 1;
    pm_offset = 0 + pm_dim1 * 0;
    pm -= pm_offset;
    w_dim1 = *jm - (-(*jm)) + 1;
    w_offset = -(*jm) + w_dim1 * (-(*mm));
    w -= w_offset;

    /* Function Body */
    m = 0;
    if (*isw == -1) {
	i__1 = *mm;
	for (n = 0; n <= i__1; ++n) {
	    s[n + 1] = 0.f;
/* L10: */
	}
    } else {
	shppma_(mm, jm, &m, &pm[pm_offset], xs, &px[px_offset], &r__[1]);
	shlfwm_(mm, jm, &m, isw, &w[-(*jm)], &s[1], sd, xc, &pm[pm_offset], &
		wy[wy_offset], &wx[wx_offset], &r__[1], &z__[z_offset]);
    }
    i__1 = *mm;
    for (m = 1; m <= i__1; ++m) {
	shnm2l_(mm, &m, &m, &lr, &li);
	shppma_(mm, jm, &m, &pm[pm_offset], xs, &px[px_offset], &r__[1]);
	shlfwm_(mm, jm, &m, isw, &w[-(*jm) + m * w_dim1], &s[lr], sd, xc, &pm[
		pm_offset], &wy[wy_offset], &wx[wx_offset], &r__[1], &z__[
		z_offset]);
	shlfwm_(mm, jm, &m, isw, &w[-(*jm) + -m * w_dim1], &s[li], sd, xc, &
		pm[pm_offset], &wy[wy_offset], &wx[wx_offset], &r__[1], &z__[
		z_offset]);
	if (*isw == -1) {
	    shmsri_(mm, &m, &s[lr], &s[li]);
	}
/* L20: */
    }
    return 0;
} /* shmw2s_ */


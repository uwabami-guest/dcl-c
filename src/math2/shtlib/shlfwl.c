/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ********************************************************************** */
/*     FORWARD LEGENDRE TRANSFORMATION (LOWER LEVEL) */
/* ********************************************************************** */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int shlfwl_(integer *mm, integer *jm, integer *n, integer *m,
	 real *wx, real *sd, real *pm)
{
    /* System generated locals */
    integer wx_dim1, wx_offset, pm_dim1, pm_offset, i__1;

    /* Local variables */
    static integer j;
    static doublereal sum;

    /* Parameter adjustments */
    pm_dim1 = *mm + 1 - 0 + 1;
    pm_offset = 0 + pm_dim1 * 0;
    pm -= pm_offset;
    wx_dim1 = *jm - 0 + 1;
    wx_offset = 0 + wx_dim1;
    wx -= wx_offset;

    /* Function Body */
    if ((*n - *m) % 2 == 0) {
	sum = wx[wx_dim1] * .5f * pm[*n];
	i__1 = *jm;
	for (j = 1; j <= i__1; ++j) {
	    sum += wx[j + wx_dim1] * pm[*n + j * pm_dim1];
/* L10: */
	}
    } else {
	sum = 0.;
	i__1 = *jm;
	for (j = 1; j <= i__1; ++j) {
	    sum += wx[j + (wx_dim1 << 1)] * pm[*n + j * pm_dim1];
/* L20: */
	}
    }
    *sd = sum;
    return 0;
} /* shlfwl_ */


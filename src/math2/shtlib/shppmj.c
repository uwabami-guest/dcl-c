/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ********************************************************************** */
/*     CALCULATE LEGENDRE FUNCTIONS OF ZONAL WAVENUMBER M AT A LATITUDE */
/* ********************************************************************** */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int shppmj_(integer *mm, integer *jm, integer *m, integer *j,
	 real *pmj, real *ys, real *py, real *r__)
{
    /* System generated locals */
    integer py_dim2, py_offset, i__1;

    /* Local variables */
    static integer n, la, lb;

    /* Parameter adjustments */
    --r__;
    py_dim2 = *jm - 0 + 1;
    py_offset = 1 + 2 * (0 + py_dim2 * 0);
    py -= py_offset;

    /* Function Body */
    la = *m * ((*mm << 1) - *m) + *mm;
    lb = la - *mm + *m - 1;
    pmj[*m] = py[(*j + *m * py_dim2 << 1) + 1];
    pmj[*m + 1] = py[(*j + *m * py_dim2 << 1) + 2];
    i__1 = *mm + 1;
    for (n = *m + 2; n <= i__1; ++n) {
	pmj[n] = r__[la + n] * (ys[*j] * pmj[n - 1] - r__[lb + n] * pmj[n - 2]
		);
/* L10: */
    }
    return 0;
} /* shppmj_ */


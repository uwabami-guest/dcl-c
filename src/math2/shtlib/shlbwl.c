/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ********************************************************************** */
/*     BACKWARD LEGENDRE TRANSFORMATION (LOWER LEVEL) */
/* ********************************************************************** */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int shlbwl_(integer *mm, integer *m, real *sd, real *ws, 
	real *wa, real *pmj)
{
    /* System generated locals */
    integer i__1;

    /* Local variables */
    static integer n;
    static doublereal suma, sums;

    n = *m;
    sums = sd[n] * pmj[n];
    suma = sd[n + 1] * pmj[n + 1];
    i__1 = *mm;
    for (n = *m + 2; n <= i__1; n += 2) {
	sums += sd[n] * pmj[n];
	suma += sd[n + 1] * pmj[n + 1];
/* L10: */
    }
    if ((*mm - *m) % 2 == 1) {
	sums += sd[*mm + 1] * pmj[*mm + 1];
    }
    *ws = sums;
    *wa = suma;
    return 0;
} /* shlbwl_ */


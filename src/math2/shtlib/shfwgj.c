/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ********************************************************************** */
/*     TRANSFORM WAVE TO GRID FROM M=M1 TO M=M2 AT A LATITUDE */
/* ********************************************************************** */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int shfwgj_(integer *mm, integer *im, integer *m1, integer *
	m2, real *wj, real *gj, real *h__, real *wfft)
{
    /* System generated locals */
    integer wj_offset, gj_offset, i__1;

    /* Local variables */
    static integer i__, k, m;
    extern /* Subroutine */ int shfftb_(integer *, real *, real *);

/*     IM.GE.MM+1 */
    /* Parameter adjustments */
    wj_offset = -(*mm);
    wj -= wj_offset;
    gj_offset = -(*im);
    gj -= gj_offset;
    --wfft;

    /* Function Body */
    if (*m1 > 0) {
	h__[0] = 0.f;
    } else {
	h__[0] = wj[0];
    }
    i__1 = *m1 - 1;
    for (m = 1; m <= i__1; ++m) {
	h__[(m << 1) - 1] = 0.f;
	h__[m * 2] = 0.f;
/* L10: */
    }
    i__1 = *m2;
    for (m = *m1; m <= i__1; ++m) {
	h__[(m << 1) - 1] = wj[m];
	h__[m * 2] = wj[-m];
/* L20: */
    }
    i__1 = (*im << 1) - 1;
    for (k = (*m2 << 1) + 1; k <= i__1; ++k) {
	h__[k] = 0.f;
/* L30: */
    }
    i__1 = *im << 1;
    shfftb_(&i__1, h__, &wfft[1]);
    gj[0] = h__[0];
    i__1 = *im;
    for (i__ = 1; i__ <= i__1; ++i__) {
	gj[i__] = h__[i__];
	gj[-i__] = h__[(*im << 1) - i__];
/* L40: */
    }
    return 0;
} /* shfwgj_ */


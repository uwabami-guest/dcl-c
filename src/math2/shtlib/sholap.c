/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* *********************************************************************** */
/*     OPERATE LAPLACIAN */
/* *********************************************************************** */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int sholap_(integer *mm, integer *ind, real *a, real *b)
{
    /* System generated locals */
    integer i__1, i__2;

    /* Local variables */
    static integer l, m, n, ir;

/*     IND=1: \nabla^2, IND=-1: \nabla^{-2} */
    /* Parameter adjustments */
    --b;
    --a;

    /* Function Body */
    if (*ind == 1) {
	m = 0;
	l = 0;
	i__1 = *mm;
	for (n = 0; n <= i__1; ++n) {
	    ++l;
	    b[l] = -n * (n + 1) * a[l];
/* L10: */
	}
	i__1 = *mm;
	for (m = 1; m <= i__1; ++m) {
	    for (ir = 1; ir <= 2; ++ir) {
		i__2 = *mm;
		for (n = m; n <= i__2; ++n) {
		    ++l;
		    b[l] = -n * (n + 1) * a[l];
/* L20: */
		}
/* L30: */
	    }
/* L40: */
	}
    } else if (*ind == -1) {
	m = 0;
	l = 1;
	b[l] = 0.f;
	i__1 = *mm;
	for (n = 1; n <= i__1; ++n) {
	    ++l;
	    b[l] = -a[l] / (n * (n + 1));
/* L50: */
	}
	i__1 = *mm;
	for (m = 1; m <= i__1; ++m) {
	    for (ir = 1; ir <= 2; ++ir) {
		i__2 = *mm;
		for (n = m; n <= i__2; ++n) {
		    ++l;
		    b[l] = -a[l] / (n * (n + 1));
/* L60: */
		}
/* L70: */
	    }
/* L80: */
	}
    }
    return 0;
} /* sholap_ */


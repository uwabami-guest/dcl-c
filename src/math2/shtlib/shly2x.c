/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ********************************************************************** */
/*     INTERPOLATION FROM GRID POINTS TO GAUSSIAN LATITUDES */
/* ********************************************************************** */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int shly2x_(integer *jm, integer *m, integer *isw, real *wm, 
	real *wx, real *wy, real *z__)
{
    /* System generated locals */
    integer wm_offset, wy_dim1, wy_offset, wx_dim1, wx_offset, z_dim1, z_dim2,
	     z_offset, i__1, i__2;

    /* Local variables */
    static integer j, j1, j2, iswa;
    static doublereal suma, sums;

/*     ISW=-1: X-DIFFERENTIAL, ISW=0: NORMAL, ISW=1: Y-DIFFERENTIAL */
    /* Parameter adjustments */
    z_dim1 = *jm;
    z_dim2 = *jm - 0 + 1;
    z_offset = 1 + z_dim1 * (0 + z_dim2);
    z__ -= z_offset;
    wy_dim1 = *jm - 0 + 1;
    wy_offset = 0 + wy_dim1;
    wy -= wy_offset;
    wx_dim1 = *jm - 0 + 1;
    wx_offset = 0 + wx_dim1;
    wx -= wx_offset;
    wm_offset = -(*jm);
    wm -= wm_offset;

    /* Function Body */
    iswa = abs(*isw);
    wy[wy_dim1] = wm[0];
    wy[wy_dim1 * 2] = 0.f;
    i__1 = *jm;
    for (j = 1; j <= i__1; ++j) {
	wy[j + wy_dim1] = (wm[j] + wm[-j]) * .5f;
	wy[j + (wy_dim1 << 1)] = (wm[j] - wm[-j]) * .5f;
/* L10: */
    }
    if ((*m + iswa) % 2 == 0) {
	wx[wx_dim1] = wy[wy_dim1];
	wx[wx_dim1 * 2] = wy[wy_dim1 * 2];
	i__1 = *jm;
	for (j1 = 1; j1 <= i__1; ++j1) {
	    sums = 0.;
	    suma = 0.;
	    i__2 = *jm;
	    for (j2 = 0; j2 <= i__2; ++j2) {
		sums += z__[j1 + (j2 + z_dim2) * z_dim1] * wy[j2 + wy_dim1];
		suma += z__[j1 + (j2 + (z_dim2 << 1)) * z_dim1] * wy[j2 + (
			wy_dim1 << 1)];
/* L20: */
	    }
	    wx[j1 + wx_dim1] = sums;
	    wx[j1 + (wx_dim1 << 1)] = suma;
/* L30: */
	}
    } else {
	wx[wx_dim1] = wy[wy_dim1];
	wx[wx_dim1 * 2] = wy[wy_dim1 * 2];
	i__1 = *jm;
	for (j1 = 1; j1 <= i__1; ++j1) {
	    sums = 0.;
	    suma = 0.;
	    i__2 = *jm - 1;
	    for (j2 = 0; j2 <= i__2; ++j2) {
		sums += z__[j1 + (j2 + z_dim2 * 3) * z_dim1] * wy[j2 + 
			wy_dim1];
		suma += z__[j1 + (j2 + (z_dim2 << 2)) * z_dim1] * wy[j2 + (
			wy_dim1 << 1)];
/* L40: */
	    }
	    wx[j1 + wx_dim1] = sums;
	    wx[j1 + (wx_dim1 << 1)] = suma;
/* L50: */
	}
    }
    return 0;
} /* shly2x_ */


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ********************************************************************** */
/*     TRANSFORM WAVE TO GRID FOR M=0 */
/* ********************************************************************** */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int shfwgz_(integer *jm, integer *im, real *wz, real *g)
{
    /* System generated locals */
    integer wz_offset, g_dim1, g_offset, i__1, i__2;

    /* Local variables */
    static integer i__, j;

    /* Parameter adjustments */
    wz_offset = -(*jm);
    wz -= wz_offset;
    g_dim1 = *im - (-(*im)) + 1;
    g_offset = -(*im) + g_dim1 * (-(*jm));
    g -= g_offset;

    /* Function Body */
    i__1 = *jm;
    for (j = -(*jm); j <= i__1; ++j) {
	i__2 = *im;
	for (i__ = -(*im); i__ <= i__2; ++i__) {
	    g[i__ + j * g_dim1] = wz[j];
/* L10: */
	}
/* L20: */
    }
    return 0;
} /* shfwgz_ */


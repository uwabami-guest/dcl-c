/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ********************************************************************** */
/*     TRANSFORM SPECTRA INTO WAVE (FOR M>0) */
/* ********************************************************************** */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int shmswm_(integer *mm, integer *jm, integer *m, integer *
	isw, real *s, real *wr, real *wi, real *sd, real *pm, real *ys, real *
	yc, real *py, real *r__)
{
    /* System generated locals */
    integer wr_offset, wi_offset, pm_dim1, pm_offset, py_dim2, py_offset;

    /* Local variables */
    static integer li, lr;
    extern /* Subroutine */ int shnm2l_(integer *, integer *, integer *, 
	    integer *, integer *), shppma_(integer *, integer *, integer *, 
	    real *, real *, real *, real *), shlbwm_(integer *, integer *, 
	    integer *, integer *, real *, real *, real *, real *, real *, 
	    real *), shmdxm_(integer *, real *, real *);

    /* Parameter adjustments */
    --r__;
    --s;
    py_dim2 = *jm - 0 + 1;
    py_offset = 1 + 2 * (0 + py_dim2 * 0);
    py -= py_offset;
    pm_dim1 = *mm + 1 - 0 + 1;
    pm_offset = 0 + pm_dim1 * 0;
    pm -= pm_offset;
    wi_offset = -(*jm);
    wi -= wi_offset;
    wr_offset = -(*jm);
    wr -= wr_offset;

    /* Function Body */
    shnm2l_(mm, m, m, &lr, &li);
    shppma_(mm, jm, m, &pm[pm_offset], ys, &py[py_offset], &r__[1]);
    shlbwm_(mm, jm, m, isw, &s[lr], &wr[wr_offset], sd, &pm[pm_offset], yc, &
	    r__[1]);
    shlbwm_(mm, jm, m, isw, &s[li], &wi[wi_offset], sd, &pm[pm_offset], yc, &
	    r__[1]);
    if (*isw == -1) {
	shmdxm_(jm, &wr[-(*jm)], &wi[-(*jm)]);
    }
    return 0;
} /* shmswm_ */


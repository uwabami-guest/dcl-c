/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ********************************************************************** */
/*     TRANSFORM GRID TO WAVE */
/* ********************************************************************** */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int shfg2w_(integer *mm, integer *jm, integer *im, real *g, 
	real *w, real *h__, real *wfft)
{
    /* System generated locals */
    integer g_dim1, g_offset, w_dim1, w_offset, i__1, i__2;

    /* Local variables */
    static integer i__, j, k;
    extern /* Subroutine */ int shfftf_(integer *, real *, real *);

/*     IM.GE.MM+1 */
    /* Parameter adjustments */
    w_dim1 = *jm - (-(*jm)) + 1;
    w_offset = -(*jm) + w_dim1 * (-(*mm));
    w -= w_offset;
    g_dim1 = *im - (-(*im)) + 1;
    g_offset = -(*im) + g_dim1 * (-(*jm));
    g -= g_offset;
    --wfft;

    /* Function Body */
    i__1 = *jm;
    for (j = -(*jm); j <= i__1; ++j) {
	h__[0] = g[j * g_dim1];
	i__2 = *im;
	for (i__ = 1; i__ <= i__2; ++i__) {
	    h__[i__] = g[i__ + j * g_dim1];
	    h__[(*im << 1) - i__] = g[-i__ + j * g_dim1];
/* L10: */
	}
	i__2 = *im << 1;
	shfftf_(&i__2, h__, &wfft[1]);
	w[j] = h__[0] / (*im << 1);
	i__2 = *mm;
	for (k = 1; k <= i__2; ++k) {
	    w[j + k * w_dim1] = h__[(k << 1) - 1] / (*im << 1);
	    w[j + -k * w_dim1] = h__[k * 2] / (*im << 1);
/* L20: */
	}
/* L30: */
    }
    return 0;
} /* shfg2w_ */


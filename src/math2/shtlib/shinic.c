/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* *********************************************************************** */
/*     OPERATE GAUSSIAN WEIGHT TO LEGENDRE FUNCTIONS */
/* *********************************************************************** */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int shinic_(integer *mm, integer *jm, real *xw, real *px)
{
    /* System generated locals */
    integer px_dim2, px_offset, i__1, i__2;

    /* Local variables */
    static integer j, m;

    /* Parameter adjustments */
    px_dim2 = *jm - 0 + 1;
    px_offset = 1 + 2 * (0 + px_dim2 * 0);
    px -= px_offset;

    /* Function Body */
    i__1 = *mm;
    for (m = 0; m <= i__1; ++m) {
	i__2 = *jm;
	for (j = 0; j <= i__2; ++j) {
	    px[(j + m * px_dim2 << 1) + 1] = xw[j] * px[(j + m * px_dim2 << 1)
		     + 1];
	    px[(j + m * px_dim2 << 1) + 2] = xw[j] * px[(j + m * px_dim2 << 1)
		     + 2];
/* L10: */
	}
/* L20: */
    }
    return 0;
} /* shinic_ */


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* *********************************************************************** */
/*     INITIALIZE ALL VARIABLES TO SAVE */
/* *********************************************************************** */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int shinit_(integer *mm, integer *jm, integer *im, real *y, 
	real *ys, real *yc, real *x, real *xs, real *xc, real *xw, real *z__, 
	real *py, real *px, real *r__, real *wfft)
{
    /* System generated locals */
    integer z_dim1, z_dim2, z_offset, py_dim2, py_offset, px_dim2, px_offset, 
	    i__1;

    /* Local variables */
    extern /* Subroutine */ int shinic_(integer *, integer *, real *, real *),
	     shinif_(integer *, real *), shinip_(integer *, integer *, real *,
	     real *, real *), shinir_(integer *, real *), shinix_(integer *, 
	    real *, real *, real *, real *), shiniy_(integer *, real *, real *
	    , real *), shiniz_(integer *, real *, real *, real *);

    /* Parameter adjustments */
    --r__;
    px_dim2 = *jm - 0 + 1;
    px_offset = 1 + 2 * (0 + px_dim2 * 0);
    px -= px_offset;
    py_dim2 = *jm - 0 + 1;
    py_offset = 1 + 2 * (0 + py_dim2 * 0);
    py -= py_offset;
    z_dim1 = *jm;
    z_dim2 = *jm - 0 + 1;
    z_offset = 1 + z_dim1 * (0 + z_dim2);
    z__ -= z_offset;
    --wfft;

    /* Function Body */
    shiniy_(jm, y, ys, yc);
    shinix_(jm, x, xs, xc, xw);
    shiniz_(jm, x, y, &z__[z_offset]);
    shinip_(mm, jm, ys, yc, &py[py_offset]);
    shinip_(mm, jm, xs, xc, &px[px_offset]);
    shinic_(mm, jm, xw, &px[px_offset]);
    shinir_(mm, &r__[1]);
    i__1 = *im << 1;
    shinif_(&i__1, &wfft[1]);
    return 0;
} /* shinit_ */


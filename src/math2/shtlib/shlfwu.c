/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ********************************************************************** */
/*     FORWARD LEGENDRE TRANSFORMATION (UPPER LEVEL) */
/* ********************************************************************** */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int shlfwu_(integer *mm, integer *jm, integer *m, integer *
	isw, real *wm, real *sm, real *sd, real *xs, real *xc, real *pm, real 
	*wy, real *wx, real *px, real *r__, real *z__)
{
    /* System generated locals */
    integer sm_offset, wm_offset, pm_dim1, pm_offset, px_dim2, px_offset, 
	    wy_dim1, wy_offset, wx_dim1, wx_offset, z_dim1, z_dim2, z_offset;

    /* Local variables */
    extern /* Subroutine */ int shppma_(integer *, integer *, integer *, real 
	    *, real *, real *, real *), shlfwm_(integer *, integer *, integer 
	    *, integer *, real *, real *, real *, real *, real *, real *, 
	    real *, real *, real *);

    /* Parameter adjustments */
    --r__;
    z_dim1 = *jm;
    z_dim2 = *jm - 0 + 1;
    z_offset = 1 + z_dim1 * (0 + z_dim2);
    z__ -= z_offset;
    px_dim2 = *jm - 0 + 1;
    px_offset = 1 + 2 * (0 + px_dim2 * 0);
    px -= px_offset;
    wx_dim1 = *jm - 0 + 1;
    wx_offset = 0 + wx_dim1;
    wx -= wx_offset;
    wy_dim1 = *jm - 0 + 1;
    wy_offset = 0 + wy_dim1;
    wy -= wy_offset;
    pm_dim1 = *mm + 1 - 0 + 1;
    pm_offset = 0 + pm_dim1 * 0;
    pm -= pm_offset;
    wm_offset = -(*jm);
    wm -= wm_offset;
    sm_offset = *m;
    sm -= sm_offset;

    /* Function Body */
    shppma_(mm, jm, m, &pm[pm_offset], xs, &px[px_offset], &r__[1]);
    shlfwm_(mm, jm, m, isw, &wm[wm_offset], &sm[sm_offset], sd, xc, &pm[
	    pm_offset], &wy[wy_offset], &wx[wx_offset], &r__[1], &z__[
	    z_offset]);
    return 0;
} /* shlfwu_ */


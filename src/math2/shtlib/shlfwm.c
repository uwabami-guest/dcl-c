/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ********************************************************************** */
/*     FORWARD LEGENDRE TRANSFORMATION (MIDDLE LEVEL) */
/* ********************************************************************** */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int shlfwm_(integer *mm, integer *jm, integer *m, integer *
	isw, real *wm, real *sm, real *sd, real *xc, real *pm, real *wy, real 
	*wx, real *r__, real *z__)
{
    /* System generated locals */
    integer sm_offset, wm_offset, pm_dim1, pm_offset, wy_dim1, wy_offset, 
	    wx_dim1, wx_offset, z_dim1, z_dim2, z_offset, i__1;

    /* Local variables */
    static integer j, n;
    extern /* Subroutine */ int shly2x_(integer *, integer *, integer *, real 
	    *, real *, real *, real *), shlfwl_(integer *, integer *, integer 
	    *, integer *, real *, real *, real *), shlsds_(integer *, integer 
	    *, integer *, real *, real *, real *);

    /* Parameter adjustments */
    --r__;
    z_dim1 = *jm;
    z_dim2 = *jm - 0 + 1;
    z_offset = 1 + z_dim1 * (0 + z_dim2);
    z__ -= z_offset;
    wx_dim1 = *jm - 0 + 1;
    wx_offset = 0 + wx_dim1;
    wx -= wx_offset;
    wy_dim1 = *jm - 0 + 1;
    wy_offset = 0 + wy_dim1;
    wy -= wy_offset;
    pm_dim1 = *mm + 1 - 0 + 1;
    pm_offset = 0 + pm_dim1 * 0;
    pm -= pm_offset;
    wm_offset = -(*jm);
    wm -= wm_offset;
    sm_offset = *m;
    sm -= sm_offset;

    /* Function Body */
    if (*isw == -1 && *m == 0) {
	i__1 = *mm;
	for (n = *m; n <= i__1; ++n) {
	    sm[n] = 0.f;
/* L10: */
	}
    } else {
	shly2x_(jm, m, isw, &wm[wm_offset], &wx[wx_offset], &wy[wy_offset], &
		z__[z_offset]);
	if (*isw == 0 && *m != 0) {
	    i__1 = *jm;
	    for (j = 0; j <= i__1; ++j) {
		wx[j + wx_dim1] *= xc[j];
		wx[j + (wx_dim1 << 1)] *= xc[j];
/* L20: */
	    }
	} else if (*isw == 1 && *m == 0) {
	    i__1 = *jm;
	    for (j = 0; j <= i__1; ++j) {
		wx[j + wx_dim1] /= xc[j];
		wx[j + (wx_dim1 << 1)] /= xc[j];
/* L30: */
	    }
	}
	i__1 = *mm + 1;
	for (n = *m; n <= i__1; ++n) {
	    shlfwl_(mm, jm, &n, m, &wx[wx_offset], &sd[n], &pm[pm_offset]);
/* L40: */
	}
	shlsds_(mm, m, isw, sd, &sm[sm_offset], &r__[1]);
    }
    return 0;
} /* shlfwm_ */


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* *********************************************************************** */
/*     INITIALIZE GAUSSIAN LATITUDES AND WEIGHT */
/* *********************************************************************** */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int shinix_(integer *jm, real *x, real *xs, real *xc, real *
	xw)
{
    /* System generated locals */
    integer i__1, i__2;

    /* Builtin functions */
    double sin(doublereal), asin(doublereal), sqrt(doublereal);

    /* Local variables */
    static integer j, n;
    static doublereal p0, p1, p2;
    static integer nm;
    static doublereal xd, dx, dp1;

    nm = (*jm << 1) + 1;
    i__1 = *jm;
    for (j = 0; j <= i__1; ++j) {
	xd = sin(j * 6.2831853071795862 / ((nm << 1) + 1));
L10:
	p0 = 1.;
	p1 = xd;
	i__2 = nm;
	for (n = 2; n <= i__2; ++n) {
	    p2 = (((n << 1) - 1) * xd * p1 - (n - 1) * p0) / n;
	    p0 = p1;
	    p1 = p2;
/* L20: */
	}
	dp1 = nm * (p0 - xd * p1) / (1 - xd * xd);
	dx = p1 / dp1;
	xd -= dx;
	if (abs(dx) > 1e-15) {
	    goto L10;
	}
	x[j] = asin(xd);
	xs[j] = xd;
	xc[j] = sqrt(1 - xd * xd);
	xw[j] = 2. / (p0 * p0 * nm * nm) * xc[j] * xc[j];
/* L30: */
    }
    return 0;
} /* shinix_ */


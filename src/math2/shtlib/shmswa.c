/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ********************************************************************** */
/*     TRANSFORM SPECTRA INTO WAVE (FROM M=M1 TO M=M2) */
/* ********************************************************************** */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int shmswa_(integer *mm, integer *jm, integer *isw, integer *
	m1, integer *m2, real *s, real *w, real *sd, real *pm, real *ys, real 
	*yc, real *py, real *r__)
{
    /* System generated locals */
    integer w_dim1, w_offset, pm_dim1, pm_offset, py_dim2, py_offset, i__1, 
	    i__2;

    /* Local variables */
    static integer j, m;
    extern /* Subroutine */ int shmswm_(integer *, integer *, integer *, 
	    integer *, real *, real *, real *, real *, real *, real *, real *,
	     real *, real *), shmswz_(integer *, integer *, integer *, real *,
	     real *, real *, real *, real *, real *, real *, real *);

    /* Parameter adjustments */
    --r__;
    --s;
    py_dim2 = *jm - 0 + 1;
    py_offset = 1 + 2 * (0 + py_dim2 * 0);
    py -= py_offset;
    pm_dim1 = *mm + 1 - 0 + 1;
    pm_offset = 0 + pm_dim1 * 0;
    pm -= pm_offset;
    w_dim1 = *jm - (-(*jm)) + 1;
    w_offset = -(*jm) + w_dim1 * (-(*mm));
    w -= w_offset;

    /* Function Body */
    if (*m1 > 0) {
	i__1 = *jm;
	for (j = -(*jm); j <= i__1; ++j) {
	    w[j] = 0.f;
/* L10: */
	}
    } else {
	shmswz_(mm, jm, isw, &s[1], &w[-(*jm)], sd, &pm[pm_offset], ys, yc, &
		py[py_offset], &r__[1]);
    }
    i__1 = *m2;
    for (m = *m1; m <= i__1; ++m) {
	shmswm_(mm, jm, &m, isw, &s[1], &w[-(*jm) + m * w_dim1], &w[-(*jm) + 
		-m * w_dim1], sd, &pm[pm_offset], ys, yc, &py[py_offset], &
		r__[1]);
/* L20: */
    }
    i__1 = *mm;
    for (m = *m2 + 1; m <= i__1; ++m) {
	i__2 = *jm;
	for (j = -(*jm); j <= i__2; ++j) {
	    w[j + m * w_dim1] = 0.f;
	    w[j + -m * w_dim1] = 0.f;
/* L30: */
	}
/* L40: */
    }
    return 0;
} /* shmswa_ */


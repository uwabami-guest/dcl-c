/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ********************************************************************** */
/*     TRANSFORM SM TO SD */
/* ********************************************************************** */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int shlssd_(integer *mm, integer *m, integer *isw, real *sm, 
	real *sd, real *r__)
{
    /* System generated locals */
    integer sm_offset, i__1;

    /* Local variables */
    static integer n, lb;

/*     ISW=-1: X-DIFFERENTIAL, ISW=0: NORMAL, ISW=1: Y-DIFFERENTIAL */
    /* Parameter adjustments */
    --r__;
    sm_offset = *m;
    sm -= sm_offset;

    /* Function Body */
    if (*isw == 0) {
	i__1 = *mm;
	for (n = *m; n <= i__1; ++n) {
	    sd[n] = sm[n];
/* L10: */
	}
	sd[*mm + 1] = 0.f;
    } else if (*isw == -1) {
	i__1 = *mm;
	for (n = *m; n <= i__1; ++n) {
	    sd[n] = *m * sm[n];
/* L20: */
	}
	sd[*mm + 1] = 0.f;
    } else {
	lb = *m * ((*mm << 1) - *m) + *m;
	if (*m != *mm) {
	    n = *m;
	    sd[n] = (n + 2) * r__[lb + n + 1] * sm[n + 1];
	    i__1 = *mm - 1;
	    for (n = *m + 1; n <= i__1; ++n) {
		sd[n] = -(n - 1) * r__[lb + n] * sm[n - 1] + (n + 2) * r__[lb 
			+ n + 1] * sm[n + 1];
/* L30: */
	    }
	    n = *mm;
	    sd[n] = -(n - 1) * r__[lb + n] * sm[n - 1];
	    n = *mm + 1;
	    sd[n] = -(n - 1) * r__[lb + n] * sm[n - 1];
	} else if (*m == *mm) {
	    n = *mm;
	    sd[n] = 0.f;
	    n = *mm + 1;
	    sd[n] = -(n - 1) * r__[lb + n] * sm[n - 1];
	}
    }
    return 0;
} /* shlssd_ */


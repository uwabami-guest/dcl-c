/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* *********************************************************************** */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int shiniy_(integer *jm, real *y, real *ys, real *yc)
{
    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    double sin(doublereal), cos(doublereal);

    /* Local variables */
    static integer j;
    static real yd;

    i__1 = *jm;
    for (j = 0; j <= i__1; ++j) {
	yd = j * 3.1415926535897932385f / (*jm << 1);
	y[j] = yd;
	ys[j] = sin(yd);
	yc[j] = cos(yd);
/* L10: */
    }
    return 0;
} /* shiniy_ */


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ********************************************************************** */
/*     BACKWARD LEGENDRE TRANSFORMATION (AT A LATITUDE) */
/* ********************************************************************** */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int shlbwj_(integer *mm, integer *jm, integer *m, integer *j,
	 integer *isw, real *sm, real *wmj, real *sd, real *pmj, real *yc, 
	real *r__)
{
    /* System generated locals */
    integer sm_offset;

    /* Local variables */
    static integer jd;
    static real wa, ws;
    extern /* Subroutine */ int shlbwl_(integer *, integer *, real *, real *, 
	    real *, real *), shlssd_(integer *, integer *, integer *, real *, 
	    real *, real *);

/*     ISW=-1: X-DIFFERENTIAL, ISW=0: NORMAL, ISW=1: Y-DIFFERENTIAL */
    /* Parameter adjustments */
    --r__;
    sm_offset = *m;
    sm -= sm_offset;

    /* Function Body */
    shlssd_(mm, m, isw, &sm[sm_offset], sd, &r__[1]);
    shlbwl_(mm, m, sd, &ws, &wa, pmj);
    jd = abs(*j);
    if (*isw == 0 && *m != 0) {
	ws *= yc[jd];
	wa *= yc[jd];
    } else if (*isw != 0 && *m == 0) {
	if (jd == *jm) {
	    ws = 0.f;
	    wa = 0.f;
	} else {
	    ws /= yc[jd];
	    wa /= yc[jd];
	}
    }
    if (*j >= 0) {
	*wmj = ws + wa;
    } else {
	*wmj = ws - wa;
    }
    return 0;
} /* shlbwj_ */


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     ODRGET / ODRSET / ODRSTX */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int odrget_0_(int n__, char *cp, real *rpara, ftnlen cp_len)
{
    static char cl[40], cx[8];
    static real rp;
    static integer idx;
    extern /* Subroutine */ int odrqid_(char *, integer *, ftnlen), odrqcl_(
	    integer *, char *, ftnlen), odrqcp_(integer *, char *, ftnlen), 
	    rlrget_(char *, real *, integer *, ftnlen), odrqvl_(integer *, 
	    real *), rtrget_(char *, char *, real *, integer *, ftnlen, 
	    ftnlen), odrsvl_(integer *, real *);

    switch(n__) {
	case 1: goto L_odrset;
	case 2: goto L_odrstx;
	}

    odrqid_(cp, &idx, cp_len);
    odrqvl_(&idx, rpara);
    return 0;
/* ----------------------------------------------------------------------- */

L_odrset:
    odrqid_(cp, &idx, cp_len);
    odrsvl_(&idx, rpara);
    return 0;
/* ----------------------------------------------------------------------- */

L_odrstx:
    rp = *rpara;
    odrqid_(cp, &idx, cp_len);
/*     / SHORT NAME / */
    odrqcp_(&idx, cx, (ftnlen)8);
    rtrget_("OD", cx, &rp, &c__1, (ftnlen)2, (ftnlen)8);
/*     / LONG NAME / */
    odrqcl_(&idx, cl, (ftnlen)40);
    rlrget_(cl, &rp, &c__1, (ftnlen)40);
    odrsvl_(&idx, &rp);
    return 0;
} /* odrget_ */

/* Subroutine */ int odrget_(char *cp, real *rpara, ftnlen cp_len)
{
    return odrget_0_(0, cp, rpara, cp_len);
    }

/* Subroutine */ int odrset_(char *cp, real *rpara, ftnlen cp_len)
{
    return odrget_0_(1, cp, rpara, cp_len);
    }

/* Subroutine */ int odrstx_(char *cp, real *rpara, ftnlen cp_len)
{
    return odrget_0_(2, cp, rpara, cp_len);
    }


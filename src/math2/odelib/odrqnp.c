/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__3 = 3;
static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     REAL PARAMETER CONTROL */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int odrqnp_0_(int n__, integer *ncp, char *cp, integer *idx, 
	real *rpara, integer *in, ftnlen cp_len)
{
    /* Initialized data */

    static char cparas[8*1] = "EPSILON ";
    static real rx[1] = { 1e-5f };
    static char cparal[40*1] = "EPSILON                                 ";
    static logical lfirst = TRUE_;

    /* System generated locals */
    address a__1[3];
    integer i__1[3];

    /* Builtin functions */
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen),
	     s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer n, ix;
    extern integer lenc_(char *, ftnlen);
    static char cmsg[80];
    extern logical lchreq_(char *, char *, ftnlen, ftnlen);
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), rllget_(char *, integer *, integer *, ftnlen), 
	    rlrget_(char *, integer *, integer *, ftnlen), rtlget_(char *, 
	    char *, integer *, integer *, ftnlen, ftnlen), rtrget_(char *, 
	    char *, integer *, integer *, ftnlen, ftnlen);

    switch(n__) {
	case 1: goto L_odrqid;
	case 2: goto L_odrqcp;
	case 3: goto L_odrqcl;
	case 4: goto L_odrqvl;
	case 5: goto L_odrsvl;
	case 6: goto L_odrqin;
	}

    *ncp = 1;
    return 0;
/* ----------------------------------------------------------------------- */

L_odrqid:
    for (n = 1; n <= 1; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *idx = n;
	    return 0;
	}
/* L10: */
    }
/* Writing concatenation */
    i__1[0] = 11, a__1[0] = "PARAMETER '";
    i__1[1] = lenc_(cp, cp_len), a__1[1] = cp;
    i__1[2] = 17, a__1[2] = "' IS NOT DEFINED.";
    s_cat(cmsg, a__1, i__1, &c__3, (ftnlen)80);
    msgdmp_("E", "ODRQID", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
    return 0;
/* ----------------------------------------------------------------------- */

L_odrqcp:
    if (1 <= *idx && *idx <= 1) {
	s_copy(cp, cparas + (*idx - 1 << 3), cp_len, (ftnlen)8);
    } else {
	msgdmp_("E", "ODRQCP", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_odrqcl:
    if (1 <= *idx && *idx <= 1) {
	s_copy(cp, cparal + (*idx - 1) * 40, cp_len, (ftnlen)40);
    } else {
	msgdmp_("E", "ODRQCL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_odrqvl:
    if (lfirst) {
	rtlget_("OD", cparas, &ix, &c__1, (ftnlen)2, (ftnlen)8);
	rllget_(cparal, &ix, &c__1, (ftnlen)40);
	lfirst = FALSE_;
    }
    if (1 <= *idx && *idx <= 1) {
	*rpara = rx[*idx - 1];
    } else {
	msgdmp_("E", "ODRQVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_odrsvl:
    if (lfirst) {
	rtrget_("OD", cparas, &ix, &c__1, (ftnlen)2, (ftnlen)8);
	rlrget_(cparal, &ix, &c__1, (ftnlen)40);
	lfirst = FALSE_;
    }
    if (1 <= *idx && *idx <= 1) {
	rx[*idx - 1] = *rpara;
    } else {
	msgdmp_("E", "ODRSVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_odrqin:
    for (n = 1; n <= 1; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *in = n;
	    return 0;
	}
/* L20: */
    }
    *in = 0;
    return 0;
} /* odrqnp_ */

/* Subroutine */ int odrqnp_(integer *ncp)
{
    return odrqnp_0_(0, ncp, (char *)0, (integer *)0, (real *)0, (integer *)0,
	     (ftnint)0);
    }

/* Subroutine */ int odrqid_(char *cp, integer *idx, ftnlen cp_len)
{
    return odrqnp_0_(1, (integer *)0, cp, idx, (real *)0, (integer *)0, 
	    cp_len);
    }

/* Subroutine */ int odrqcp_(integer *idx, char *cp, ftnlen cp_len)
{
    return odrqnp_0_(2, (integer *)0, cp, idx, (real *)0, (integer *)0, 
	    cp_len);
    }

/* Subroutine */ int odrqcl_(integer *idx, char *cp, ftnlen cp_len)
{
    return odrqnp_0_(3, (integer *)0, cp, idx, (real *)0, (integer *)0, 
	    cp_len);
    }

/* Subroutine */ int odrqvl_(integer *idx, real *rpara)
{
    return odrqnp_0_(4, (integer *)0, (char *)0, idx, rpara, (integer *)0, (
	    ftnint)0);
    }

/* Subroutine */ int odrsvl_(integer *idx, real *rpara)
{
    return odrqnp_0_(5, (integer *)0, (char *)0, idx, rpara, (integer *)0, (
	    ftnint)0);
    }

/* Subroutine */ int odrqin_(char *cp, integer *in, ftnlen cp_len)
{
    return odrqnp_0_(6, (integer *)0, cp, (integer *)0, (real *)0, in, cp_len)
	    ;
    }


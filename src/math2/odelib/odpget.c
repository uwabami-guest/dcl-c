/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     ODPGET / ODPSET / ODPSTX */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int odpget_0_(int n__, char *cp, integer *ipara, ftnlen 
	cp_len)
{
    static char cl[40];
    static integer ip;
    static char cx[8];
    static integer it, idx;
    extern /* Subroutine */ int odiqid_(char *, integer *, ftnlen), odlqid_(
	    char *, integer *, ftnlen), odpqid_(char *, integer *, ftnlen), 
	    odpqcl_(integer *, char *, ftnlen), odrqid_(char *, integer *, 
	    ftnlen), odpqcp_(integer *, char *, ftnlen), rliget_(char *, 
	    integer *, integer *, ftnlen), rllget_(char *, integer *, integer 
	    *, ftnlen), rtiget_(char *, char *, integer *, integer *, ftnlen, 
	    ftnlen), odpqit_(integer *, integer *), odisvl_(integer *, 
	    integer *), rtlget_(char *, char *, integer *, integer *, ftnlen, 
	    ftnlen), odlsvl_(integer *, integer *), rlrget_(char *, integer *,
	     integer *, ftnlen), odpqvl_(integer *, integer *), odpsvl_(
	    integer *, integer *), rtrget_(char *, char *, integer *, integer 
	    *, ftnlen, ftnlen), odrsvl_(integer *, integer *);

    switch(n__) {
	case 1: goto L_odpset;
	case 2: goto L_odpstx;
	}

    odpqid_(cp, &idx, cp_len);
    odpqvl_(&idx, ipara);
    return 0;
/* ----------------------------------------------------------------------- */

L_odpset:
    odpqid_(cp, &idx, cp_len);
    odpsvl_(&idx, ipara);
    return 0;
/* ----------------------------------------------------------------------- */

L_odpstx:
    ip = *ipara;
    odpqid_(cp, &idx, cp_len);
    odpqit_(&idx, &it);
    odpqcp_(&idx, cx, (ftnlen)8);
    odpqcl_(&idx, cl, (ftnlen)40);
    if (it == 1) {
	rtiget_("OD", cx, &ip, &c__1, (ftnlen)2, (ftnlen)8);
	rliget_(cl, &ip, &c__1, (ftnlen)40);
	odiqid_(cp, &idx, cp_len);
	odisvl_(&idx, &ip);
    } else if (it == 2) {
	rtlget_("OD", cx, &ip, &c__1, (ftnlen)2, (ftnlen)8);
	rllget_(cl, &ip, &c__1, (ftnlen)40);
	odlqid_(cp, &idx, cp_len);
	odlsvl_(&idx, &ip);
    } else if (it == 3) {
	rtrget_("OD", cx, &ip, &c__1, (ftnlen)2, (ftnlen)8);
	rlrget_(cl, &ip, &c__1, (ftnlen)40);
	odrqid_(cp, &idx, cp_len);
	odrsvl_(&idx, &ip);
    }
    return 0;
} /* odpget_ */

/* Subroutine */ int odpget_(char *cp, integer *ipara, ftnlen cp_len)
{
    return odpget_0_(0, cp, ipara, cp_len);
    }

/* Subroutine */ int odpset_(char *cp, integer *ipara, ftnlen cp_len)
{
    return odpget_0_(1, cp, ipara, cp_len);
    }

/* Subroutine */ int odpstx_(char *cp, integer *ipara, ftnlen cp_len)
{
    return odpget_0_(2, cp, ipara, cp_len);
    }


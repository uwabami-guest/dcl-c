/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*    Runge-Kutta dirver (Variable step) */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int odrkdv_(integer *n, S_fp steper, U_fp fcn, real *t, real 
	*tend, real *dt, real *x, real *work)
{
    /* System generated locals */
    integer work_dim1, work_offset, i__1;

    /* Local variables */
    static integer i__;
    static real t0;
    static integer ns;
    static real dtt, epsl;
    extern /* Subroutine */ int odiget_(char *, integer *, ftnlen), odrget_(
	    char *, real *, ftnlen), msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), odiset_(char *, integer *, ftnlen);
    static integer mxstep;

/*                     WORK(N,7) FOR ODRK4R */
/*                     WORK(N,5) FOR RKGR */
    /* Parameter adjustments */
    work_dim1 = *n;
    work_offset = 1 + work_dim1;
    work -= work_offset;
    --x;

    /* Function Body */
    if (*dt * (*tend - *t) <= 0.f) {
	msgdmp_("E", "ODRKDV", "INVALID TEND OR DT.", (ftnlen)1, (ftnlen)6, (
		ftnlen)19);
    }
    odiget_("MAXSTEP", &mxstep, (ftnlen)7);
    odrget_("EPSILON", &epsl, (ftnlen)7);
    t0 = *t;
    dtt = *dt;
    i__1 = mxstep;
    for (i__ = 1; i__ <= i__1; ++i__) {
	*dt = dtt;
	if ((*t + *dt * 2 - *tend) * (*t + *dt * 2 - t0) > 0.f) {
	    dtt = (*tend - *t) / 2.f;
	}
	(*steper)(n, (U_fp)fcn, t, &dtt, &epsl, &x[1], &work[work_offset]);
	ns = i__;
	if ((*t - *tend) * (*tend - t0) >= 0.f) {
	    odiset_("NSTEP", &ns, (ftnlen)5);
	    return 0;
	}
/* L10: */
    }
    msgdmp_("W", "ODRKDV", "TOO MANY STEPS.", (ftnlen)1, (ftnlen)6, (ftnlen)
	    15);
    return 0;
} /* odrkdv_ */


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*    Runge-Kutta Stepper. */
/*                                                 Oct. 5, 1990  S.Sakai */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int odrk4s_(integer *n, S_fp fcn, real *t, real *dt, real *
	epsl, real *x, real *work)
{
    /* System generated locals */
    integer work_dim1, work_offset, i__1;
    real r__1, r__2, r__3;
    doublereal d__1, d__2;

    /* Builtin functions */
    double pow_dd(doublereal *, doublereal *);

    /* Local variables */
    static integer i__;
    static real t0, dt2, tiny;
    extern /* Subroutine */ int odrk4_(integer *, S_fp, real *, real *, real *
	    , real *, real *, real *);
    static real xscal, pgrow;
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen);
    static real errmax, pshrnk;

    /* Parameter adjustments */
    work_dim1 = *n;
    work_offset = 1 + work_dim1;
    work -= work_offset;
    --x;

    /* Function Body */
    pgrow = -.2f;
    pshrnk = -.25f;
    tiny = 1e-30f;
    t0 = *t;
/* -------------------- Integration with DT & DT*2 ----------------------- */
/* L100: */
    dt2 = *dt * 2;
    (*fcn)(n, &t0, &x[1], &work[work_dim1 + 1]);
    odrk4_(n, (S_fp)fcn, &t0, &dt2, &x[1], &work[work_dim1 + 1], &work[(
	    work_dim1 << 1) + 1], &work[work_dim1 * 3 + 1]);
    odrk4_(n, (S_fp)fcn, &t0, dt, &x[1], &work[work_dim1 + 1], &x[1], &work[
	    work_dim1 * 3 + 1]);
    *t = t0 + *dt;
    (*fcn)(n, t, &x[1], &work[work_dim1 + 1]);
    odrk4_(n, (S_fp)fcn, t, dt, &x[1], &work[work_dim1 + 1], &x[1], &work[
	    work_dim1 * 3 + 1]);
    *t = t0 + dt2;
/* ------------------------- ERROR EVALUATION ---------------------------- */
    errmax = 0.f;
    i__1 = *n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	work[i__ + work_dim1 * 3] = x[i__] - work[i__ + (work_dim1 << 1)];
	xscal = (r__1 = x[i__], abs(r__1)) + (r__2 = dt2 * work[i__ + 
		work_dim1], abs(r__2)) + tiny;
/* Computing MAX */
	r__2 = errmax, r__3 = (r__1 = work[i__ + work_dim1 * 3] / xscal, abs(
		r__1));
	errmax = max(r__2,r__3);
/* L200: */
    }
    errmax /= *epsl;
    if (errmax > 1.f) {
	d__1 = (doublereal) errmax;
	d__2 = (doublereal) pshrnk;
	*dt = *dt * .8f * pow_dd(&d__1, &d__2);
	if (errmax > 10.f) {
	    msgdmp_("W", "ODRK4S", "ERROR EXCEEDED 10 x EPSL.", (ftnlen)1, (
		    ftnlen)6, (ftnlen)25);
	}
    } else {
	if (errmax > 6e-4f) {
	    d__1 = (doublereal) errmax;
	    d__2 = (doublereal) pgrow;
	    *dt = *dt * .8f * pow_dd(&d__1, &d__2);
	} else {
	    *dt *= 4.f;
	}
    }
    return 0;
} /* odrk4s_ */


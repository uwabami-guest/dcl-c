/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     4th order Runge-Kutta algorithm routine. */
/*                                                 Oct. 5, 1990  S.Sakai */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int odrk4_(integer *n, S_fp fcn, real *t, real *dt, real *x, 
	real *dx, real *xout, real *work)
{
    /* System generated locals */
    integer work_dim1, work_offset, i__1;

    /* Local variables */
    static integer i__;
    static real tt, dtt;

    /* Parameter adjustments */
    work_dim1 = *n;
    work_offset = 1 + work_dim1;
    work -= work_offset;
    --xout;
    --dx;
    --x;

    /* Function Body */
    dtt = *dt / 2.f;
    tt = *t + dtt;
    i__1 = *n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	work[i__ + work_dim1] = x[i__] + dtt * dx[i__];
/* L10: */
    }
    (*fcn)(n, &tt, &work[work_dim1 + 1], &work[(work_dim1 << 1) + 1]);
    i__1 = *n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	work[i__ + work_dim1] = x[i__] + dtt * work[i__ + (work_dim1 << 1)];
/* L20: */
    }
    (*fcn)(n, &tt, &work[work_dim1 + 1], &work[work_dim1 * 3 + 1]);
    tt = *t + *dt;
    i__1 = *n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	work[i__ + work_dim1] = x[i__] + *dt * work[i__ + work_dim1 * 3];
	work[i__ + work_dim1 * 3] = work[i__ + (work_dim1 << 1)] + work[i__ + 
		work_dim1 * 3];
/* L30: */
    }
    (*fcn)(n, &tt, &work[work_dim1 + 1], &work[(work_dim1 << 1) + 1]);
    dtt = *dt / 6.f;
    i__1 = *n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	xout[i__] = x[i__] + dtt * (dx[i__] + work[i__ + work_dim1 * 3] * 2.f 
		+ work[i__ + (work_dim1 << 1)]);
/* L40: */
    }
    return 0;
} /* odrk4_ */


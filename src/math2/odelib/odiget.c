/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     ODIGET / ODISET / ODISTX */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int odiget_0_(int n__, char *cp, integer *ipara, ftnlen 
	cp_len)
{
    static char cl[40];
    static integer ip;
    static char cx[8];
    static integer idx;
    extern /* Subroutine */ int odiqid_(char *, integer *, ftnlen), odiqcl_(
	    integer *, char *, ftnlen), odiqcp_(integer *, char *, ftnlen), 
	    rliget_(char *, integer *, integer *, ftnlen), odiqvl_(integer *, 
	    integer *), rtiget_(char *, char *, integer *, integer *, ftnlen, 
	    ftnlen), odisvl_(integer *, integer *);

    switch(n__) {
	case 1: goto L_odiset;
	case 2: goto L_odistx;
	}

    odiqid_(cp, &idx, cp_len);
    odiqvl_(&idx, ipara);
    return 0;
/* ----------------------------------------------------------------------- */

L_odiset:
    odiqid_(cp, &idx, cp_len);
    odisvl_(&idx, ipara);
    return 0;
/* ----------------------------------------------------------------------- */

L_odistx:
    ip = *ipara;
    odiqid_(cp, &idx, cp_len);
/*     / SHORT NAME / */
    odiqcp_(&idx, cx, (ftnlen)8);
    rtiget_("OD", cx, &ip, &c__1, (ftnlen)2, (ftnlen)8);
/*     / LONG NAME / */
    odiqcl_(&idx, cl, (ftnlen)40);
    rliget_(cl, &ip, &c__1, (ftnlen)40);
    odisvl_(&idx, &ip);
    return 0;
} /* odiget_ */

/* Subroutine */ int odiget_(char *cp, integer *ipara, ftnlen cp_len)
{
    return odiget_0_(0, cp, ipara, cp_len);
    }

/* Subroutine */ int odiset_(char *cp, integer *ipara, ftnlen cp_len)
{
    return odiget_0_(1, cp, ipara, cp_len);
    }

/* Subroutine */ int odistx_(char *cp, integer *ipara, ftnlen cp_len)
{
    return odiget_0_(2, cp, ipara, cp_len);
    }


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     2nd order Runge-Kutta algorithm routine. */
/*                                                 Oct. 5, 1990  S.Sakai */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int odrk2_(integer *n, S_fp fcn, real *t, real *dt, real *x, 
	real *dx, real *xout, real *work)
{
    /* System generated locals */
    integer i__1;

    /* Local variables */
    static integer i__;
    static real tt, dtt;

    /* Parameter adjustments */
    --work;
    --xout;
    --dx;
    --x;

    /* Function Body */
    i__1 = *n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	xout[i__] = x[i__] + *dt * dx[i__];
/* L10: */
    }
    tt = *t + *dt;
    (*fcn)(n, &tt, &xout[1], &work[1]);
    dtt = *dt / 2.f;
    i__1 = *n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	xout[i__] += dtt * (work[i__] - dx[i__]);
/* L20: */
    }
    return 0;
} /* odrk2_ */


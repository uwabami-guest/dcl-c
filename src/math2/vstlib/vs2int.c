/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;
static integer c__0 = 0;

/* ----------------------------------------------------------------------- */
/*     VS2INT / VS2DIN / VS2OUT */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int vs2int_0_(int n__, real *wz, integer *nw, integer *ix, 
	integer *iy, real *x, real *y)
{
    /* System generated locals */
    integer nw_dim1, nw_offset, wz_dim1, wz_dim2, wz_offset, i__1, i__2;

    /* Local variables */
    static integer i__, j;
    static real xave, yave, xvar, yvar, xycv;
    extern /* Subroutine */ int iset0_(integer *, integer *, integer *, 
	    integer *), rset0_(real *, integer *, integer *, integer *);
    static logical lmiss;
    static real rmiss;
    extern /* Subroutine */ int gllget_(char *, logical *, ftnlen), glrget_(
	    char *, real *, ftnlen);

    /* Parameter adjustments */
    if (x) {
	--x;
	}
    wz_dim1 = *ix;
    wz_dim2 = *iy;
    wz_offset = 1 + wz_dim1 * (1 + wz_dim2);
    wz -= wz_offset;
    nw_dim1 = *ix;
    nw_offset = 1 + nw_dim1;
    nw -= nw_offset;
    if (y) {
	--y;
	}

    /* Function Body */
    switch(n__) {
	case 1: goto L_vs2din;
	case 2: goto L_vs2out;
	}

    gllget_("LMISS", &lmiss, (ftnlen)5);
    glrget_("RMISS", &rmiss, (ftnlen)5);
    i__1 = *ix * *iy * 5;
    rset0_(&wz[wz_offset], &i__1, &c__1, &c__0);
    i__1 = *ix * *iy;
    iset0_(&nw[nw_offset], &i__1, &c__1, &c__0);
    return 0;
/* ----------------------------------------------------------------------- */

L_vs2din:
    i__1 = *iy;
    for (j = 1; j <= i__1; ++j) {
	i__2 = *ix;
	for (i__ = 1; i__ <= i__2; ++i__) {
	    if (! (lmiss && (x[i__] == rmiss || y[j] == rmiss))) {
		++nw[i__ + j * nw_dim1];
		wz[i__ + (j + wz_dim2) * wz_dim1] += x[i__];
		wz[i__ + (j + (wz_dim2 << 1)) * wz_dim1] += y[j];
		wz[i__ + (j + wz_dim2 * 3) * wz_dim1] += x[i__] * x[i__];
		wz[i__ + (j + (wz_dim2 << 2)) * wz_dim1] += y[j] * y[j];
		wz[i__ + (j + wz_dim2 * 5) * wz_dim1] += x[i__] * y[j];
	    }
/* L10: */
	}
/* L15: */
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_vs2out:
    i__1 = *iy;
    for (j = 1; j <= i__1; ++j) {
	i__2 = *ix;
	for (i__ = 1; i__ <= i__2; ++i__) {
	    if (nw[i__ + j * nw_dim1] != 0) {
		xave = wz[i__ + (j + wz_dim2) * wz_dim1] / nw[i__ + j * 
			nw_dim1];
		yave = wz[i__ + (j + (wz_dim2 << 1)) * wz_dim1] / nw[i__ + j *
			 nw_dim1];
		xvar = wz[i__ + (j + wz_dim2 * 3) * wz_dim1] / nw[i__ + j * 
			nw_dim1] - xave * xave;
		yvar = wz[i__ + (j + (wz_dim2 << 2)) * wz_dim1] / nw[i__ + j *
			 nw_dim1] - yave * yave;
		xycv = wz[i__ + (j + wz_dim2 * 5) * wz_dim1] / nw[i__ + j * 
			nw_dim1] - xave * yave;
		wz[i__ + (j + wz_dim2) * wz_dim1] = xave;
		wz[i__ + (j + (wz_dim2 << 1)) * wz_dim1] = yave;
		wz[i__ + (j + wz_dim2 * 3) * wz_dim1] = xvar;
		wz[i__ + (j + (wz_dim2 << 2)) * wz_dim1] = yvar;
		wz[i__ + (j + wz_dim2 * 5) * wz_dim1] = xycv;
	    } else {
		wz[i__ + (j + wz_dim2) * wz_dim1] = rmiss;
		wz[i__ + (j + (wz_dim2 << 1)) * wz_dim1] = rmiss;
		wz[i__ + (j + wz_dim2 * 3) * wz_dim1] = rmiss;
		wz[i__ + (j + (wz_dim2 << 2)) * wz_dim1] = rmiss;
		wz[i__ + (j + wz_dim2 * 5) * wz_dim1] = rmiss;
	    }
/* L20: */
	}
/* L25: */
    }
    return 0;
} /* vs2int_ */

/* Subroutine */ int vs2int_(real *wz, integer *nw, integer *ix, integer *iy)
{
    return vs2int_0_(0, wz, nw, ix, iy, (real *)0, (real *)0);
    }

/* Subroutine */ int vs2din_(real *wz, integer *nw, integer *ix, integer *iy, 
	real *x, real *y)
{
    return vs2int_0_(1, wz, nw, ix, iy, x, y);
    }

/* Subroutine */ int vs2out_(real *wz, integer *nw, integer *ix, integer *iy)
{
    return vs2int_0_(2, wz, nw, ix, iy, (real *)0, (real *)0);
    }


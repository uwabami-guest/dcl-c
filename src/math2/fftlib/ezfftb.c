/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     SUBROUTINE EZFFTB(N,R,AZERO,A,B,WSAVE) */

/*     SUBROUTINE EZFFTB COMPUTES A REAL PERODIC SEQUENCE FROM ITS */
/*     FOURIER COEFFICIENTS (FOURIER SYNTHESIS). THE TRANSFORM IS */
/*     DEFINED BELOW AT OUTPUT PARAMETER R. EZFFTB IS A SIMPLIFIED */
/*     BUT SLOWER VERSION OF RFFTB. */

/*     INPUT PARAMETERS */

/*     N       THE LENGTH OF THE OUTPUT ARRAY R.  THE METHOD IS MOST */
/*             EFFICIENT WHEN N IS THE PRODUCT OF SMALL PRIMES. */

/*     AZERO   THE CONSTANT FOURIER COEFFICIENT */

/*     A,B     ARRAYS WHICH CONTAIN THE REMAINING FOURIER COEFFICIENTS */
/*             THESE ARRAYS ARE NOT DESTROYED. */

/*             THE LENGTH OF THESE ARRAYS DEPENDS ON WHETHER N IS EVEN OR */
/*             ODD. */

/*             IF N IS EVEN N/2    LOCATIONS ARE REQUIRED */
/*             IF N IS ODD (N-1)/2 LOCATIONS ARE REQUIRED */

/*     WSAVE   A WORK ARRAY WHICH MUST BE DIMENSIONED AT LEAST 3*N+15. */
/*             IN THE PROGRAM THAT CALLS EZFFTB. THE WSAVE ARRAY MUST BE */
/*             INITIALIZED BY CALLING SUBROUTINE EZFFTI(N,WSAVE) AND A */
/*             DIFFERENT WSAVE ARRAY MUST BE USED FOR EACH DIFFERENT */
/*             VALUE OF N. THIS INITIALIZATION DOES NOT HAVE TO BE */
/*             REPEATED SO LONG AS N REMAINS UNCHANGED THUS SUBSEQUENT */
/*             TRANSFORMS CAN BE OBTAINED FASTER THAN THE FIRST. */
/*             THE SAME WSAVE ARRAY CAN BE USED BY EZFFTF AND EZFFTB. */


/*     OUTPUT PARAMETERS */

/*     R       IF N IS EVEN DEFINE KMAX=N/2 */
/*             IF N IS ODD  DEFINE KMAX=(N-1)/2 */

/*             THEN FOR I=1,...,N */

/*                  R(I)=AZERO PLUS THE SUM FROM K=1 TO K=KMAX OF */

/*                  A(K)*COS(K*(I-1)*2*PI/N)+B(K)*SIN(K*(I-1)*2*PI/N) */

/*     ********************* COMPLEX NOTATION ************************** */

/*             FOR J=1,...,N */

/*             R(J) EQUALS THE SUM FROM K=-KMAX TO K=KMAX OF */

/*                  C(K)*EXP(I*K*(J-1)*2*PI/N) */

/*             WHERE */

/*                  C(K) = .5*CMPLX(A(K),-B(K))   FOR K=1,...,KMAX */

/*                  C(-K) = CONJG(C(K)) */

/*                  C(0) = AZERO */

/*                       AND I=SQRT(-1) */

/*     *************** AMPLITUDE - PHASE NOTATION *********************** */

/*             FOR I=1,...,N */

/*             R(I) EQUALS AZERO PLUS THE SUM FROM K=1 TO K=KMAX OF */

/*                  ALPHA(K)*COS(K*(I-1)*2*PI/N+BETA(K)) */

/*             WHERE */

/*                  ALPHA(K) = SQRT(A(K)*A(K)+B(K)*B(K)) */

/*                  COS(BETA(K))=A(K)/ALPHA(K) */

/*                  SIN(BETA(K))=-B(K)/ALPHA(K) */

/* Subroutine */ int ezfftb_(integer *n, real *r__, real *azero, real *a, 
	real *b, real *wsave)
{
    /* System generated locals */
    integer i__1;

    /* Local variables */
    static integer i__, ns2;
    extern /* Subroutine */ int rfftb_(integer *, real *, real *);


    /* Parameter adjustments */
    --wsave;
    --b;
    --a;
    --r__;

    /* Function Body */
    if ((i__1 = *n - 2) < 0) {
	goto L101;
    } else if (i__1 == 0) {
	goto L102;
    } else {
	goto L103;
    }
L101:
    r__[1] = *azero;
    return 0;
L102:
    r__[1] = *azero + a[1];
    r__[2] = *azero - a[1];
    return 0;
L103:
    ns2 = (*n - 1) / 2;
    i__1 = ns2;
    for (i__ = 1; i__ <= i__1; ++i__) {
	r__[i__ * 2] = a[i__] * .5f;
	r__[(i__ << 1) + 1] = b[i__] * -.5f;
/* L104: */
    }
    r__[1] = *azero;
    if (*n % 2 == 0) {
	r__[*n] = a[ns2 + 1];
    }
    rfftb_(n, &r__[1], &wsave[*n + 1]);
    return 0;
} /* ezfftb_ */


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     SUBROUTINE COSQB(N,X,WSAVE) */

/*     SUBROUTINE COSQB COMPUTES THE FAST FOURIER TRANSFORM OF QUARTER */
/*     WAVE DATA. THAT IS , COSQB COMPUTES A SEQUENCE FROM ITS */
/*     REPRESENTATION IN TERMS OF A COSINE SERIES WITH ODD WAVE NUMBERS. */
/*     THE TRANSFORM IS DEFINED BELOW AT OUTPUT PARAMETER X. */

/*     COSQB IS THE UNNORMALIZED INVERSE OF COSQF SINCE A CALL OF COSQB */
/*     FOLLOWED BY A CALL OF COSQF WILL MULTIPLY THE INPUT SEQUENCE X */
/*     BY 4*N. */

/*     THE ARRAY WSAVE WHICH IS USED BY SUBROUTINE COSQB MUST BE */
/*     INITIALIZED BY CALLING SUBROUTINE COSQI(N,WSAVE). */


/*     INPUT PARAMETERS */

/*     N       THE LENGTH OF THE ARRAY X TO BE TRANSFORMED.  THE METHOD */
/*             IS MOST EFFICIENT WHEN N IS A PRODUCT OF SMALL PRIMES. */

/*     X       AN ARRAY WHICH CONTAINS THE SEQUENCE TO BE TRANSFORMED */

/*     WSAVE   A WORK ARRAY THAT MUST BE DIMENSIONED AT LEAST 3*N+15 */
/*             IN THE PROGRAM THAT CALLS COSQB. THE WSAVE ARRAY MUST BE */
/*             INITIALIZED BY CALLING SUBROUTINE COSQI(N,WSAVE) AND A */
/*             DIFFERENT WSAVE ARRAY MUST BE USED FOR EACH DIFFERENT */
/*             VALUE OF N. THIS INITIALIZATION DOES NOT HAVE TO BE */
/*             REPEATED SO LONG AS N REMAINS UNCHANGED THUS SUBSEQUENT */
/*             TRANSFORMS CAN BE OBTAINED FASTER THAN THE FIRST. */

/*     OUTPUT PARAMETERS */

/*     X       FOR I=1,...,N */

/*                  X(I)= THE SUM FROM K=1 TO K=N OF */

/*                    4*X(K)*COS((2*K-1)*(I-1)*PI/(2*N)) */

/*                  A CALL OF COSQB FOLLOWED BY A CALL OF */
/*                  COSQF WILL MULTIPLY THE SEQUENCE X BY 4*N. */
/*                  THEREFORE COSQF IS THE UNNORMALIZED INVERSE */
/*                  OF COSQB. */

/*     WSAVE   CONTAINS INITIALIZATION CALCULATIONS WHICH MUST NOT */
/*             BE DESTROYED BETWEEN CALLS OF COSQB OR COSQF. */

/* Subroutine */ int cosqb_(integer *n, real *x, real *wsave)
{
    /* Initialized data */

    static real tsqrt2 = 2.82842712474619f;

    /* System generated locals */
    integer i__1;

    /* Local variables */
    static real x1;
    extern /* Subroutine */ int cosqb1_(integer *, real *, real *, real *);

    /* Parameter adjustments */
    --wsave;
    --x;

    /* Function Body */

    if ((i__1 = *n - 2) < 0) {
	goto L101;
    } else if (i__1 == 0) {
	goto L102;
    } else {
	goto L103;
    }
L101:
    x[1] *= 4.f;
    return 0;
L102:
    x1 = (x[1] + x[2]) * 4.f;
    x[2] = tsqrt2 * (x[1] - x[2]);
    x[1] = x1;
    return 0;
L103:
    cosqb1_(n, &x[1], &wsave[1], &wsave[*n + 1]);
    return 0;
} /* cosqb_ */


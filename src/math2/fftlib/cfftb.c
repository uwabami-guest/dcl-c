/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     SUBROUTINE CFFTB(N,C,WSAVE) */

/*     SUBROUTINE CFFTB COMPUTES THE BACKWARD COMPLEX DISCRETE FOURIER */
/*     TRANSFORM (THE FOURIER SYNTHESIS). EQUIVALENTLY , CFFTB COMPUTES */
/*     A COMPLEX PERIODIC SEQUENCE FROM ITS FOURIER COEFFICIENTS. */
/*     THE TRANSFORM IS DEFINED BELOW AT OUTPUT PARAMETER C. */

/*     A CALL OF CFFTF FOLLOWED BY A CALL OF CFFTB WILL MULTIPLY THE */
/*     SEQUENCE BY N. */

/*     THE ARRAY WSAVE WHICH IS USED BY SUBROUTINE CFFTB MUST BE */
/*     INITIALIZED BY CALLING SUBROUTINE CFFTI(N,WSAVE). */

/*     INPUT PARAMETERS */


/*     N      THE LENGTH OF THE COMPLEX SEQUENCE C. THE METHOD IS */
/*            MORE EFFICIENT WHEN N IS THE PRODUCT OF SMALL PRIMES. */

/*     C      A COMPLEX ARRAY OF LENGTH N WHICH CONTAINS THE SEQUENCE */

/*     WSAVE   A REAL WORK ARRAY WHICH MUST BE DIMENSIONED AT LEAST 4N+15 */
/*             IN THE PROGRAM THAT CALLS CFFTB. THE WSAVE ARRAY MUST BE */
/*             INITIALIZED BY CALLING SUBROUTINE CFFTI(N,WSAVE) AND A */
/*             DIFFERENT WSAVE ARRAY MUST BE USED FOR EACH DIFFERENT */
/*             VALUE OF N. THIS INITIALIZATION DOES NOT HAVE TO BE */
/*             REPEATED SO LONG AS N REMAINS UNCHANGED THUS SUBSEQUENT */
/*             TRANSFORMS CAN BE OBTAINED FASTER THAN THE FIRST. */
/*             THE SAME WSAVE ARRAY CAN BE USED BY CFFTF AND CFFTB. */

/*     OUTPUT PARAMETERS */

/*     C      FOR J=1,...,N */

/*                C(J)=THE SUM FROM K=1,...,N OF */

/*                      C(K)*EXP(I*(J-1)*(K-1)*2*PI/N) */

/*                            WHERE I=SQRT(-1) */

/*     WSAVE   CONTAINS INITIALIZATION CALCULATIONS WHICH MUST NOT BE */
/*             DESTROYED BETWEEN CALLS OF SUBROUTINE CFFTF OR CFFTB */

/* Subroutine */ int cfftb_(integer *n, real *c__, real *wsave)
{
    static integer iw1, iw2;
    extern /* Subroutine */ int cfftb1_(integer *, real *, real *, real *, 
	    real *);


    /* Parameter adjustments */
    --wsave;
    --c__;

    /* Function Body */
    if (*n == 1) {
	return 0;
    }
    iw1 = *n + *n + 1;
    iw2 = iw1 + *n + *n;
    cfftb1_(n, &c__[1], &wsave[1], &wsave[iw1], &wsave[iw2]);
    return 0;
} /* cfftb_ */


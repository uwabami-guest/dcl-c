/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     SUBROUTINE EZFFTF(N,R,AZERO,A,B,WSAVE) */

/*     SUBROUTINE EZFFTF COMPUTES THE FOURIER COEFFICIENTS OF A REAL */
/*     PERODIC SEQUENCE (FOURIER ANALYSIS). THE TRANSFORM IS DEFINED */
/*     BELOW AT OUTPUT PARAMETERS AZERO,A AND B. EZFFTF IS A SIMPLIFIED */
/*     BUT SLOWER VERSION OF RFFTF. */

/*     INPUT PARAMETERS */

/*     N       THE LENGTH OF THE ARRAY R TO BE TRANSFORMED.  THE METHOD */
/*             IS MUST EFFICIENT WHEN N IS THE PRODUCT OF SMALL PRIMES. */

/*     R       A REAL ARRAY OF LENGTH N WHICH CONTAINS THE SEQUENCE */
/*             TO BE TRANSFORMED. R IS NOT DESTROYED. */


/*     WSAVE   A WORK ARRAY WHICH MUST BE DIMENSIONED AT LEAST 3*N+15. */
/*             IN THE PROGRAM THAT CALLS EZFFTF. THE WSAVE ARRAY MUST BE */
/*             INITIALIZED BY CALLING SUBROUTINE EZFFTI(N,WSAVE) AND A */
/*             DIFFERENT WSAVE ARRAY MUST BE USED FOR EACH DIFFERENT */
/*             VALUE OF N. THIS INITIALIZATION DOES NOT HAVE TO BE */
/*             REPEATED SO LONG AS N REMAINS UNCHANGED THUS SUBSEQUENT */
/*             TRANSFORMS CAN BE OBTAINED FASTER THAN THE FIRST. */
/*             THE SAME WSAVE ARRAY CAN BE USED BY EZFFTF AND EZFFTB. */

/*     OUTPUT PARAMETERS */

/*     AZERO   THE SUM FROM I=1 TO I=N OF R(I)/N */

/*     A,B     FOR N EVEN B(N/2)=0. AND A(N/2) IS THE SUM FROM I=1 TO */
/*             I=N OF (-1)**(I-1)*R(I)/N */

/*             FOR N EVEN DEFINE KMAX=N/2-1 */
/*             FOR N ODD  DEFINE KMAX=(N-1)/2 */

/*             THEN FOR  K=1,...,KMAX */

/*                  A(K) EQUALS THE SUM FROM I=1 TO I=N OF */

/*                       2./N*R(I)*COS(K*(I-1)*2*PI/N) */

/*                  B(K) EQUALS THE SUM FROM I=1 TO I=N OF */

/*                       2./N*R(I)*SIN(K*(I-1)*2*PI/N) */


/* Subroutine */ int ezfftf_(integer *n, real *r__, real *azero, real *a, 
	real *b, real *wsave)
{
    /* System generated locals */
    integer i__1;

    /* Local variables */
    static integer i__;
    static real cf;
    static integer ns2;
    static real cfm;
    static integer ns2m;
    extern /* Subroutine */ int rfftf_(integer *, real *, real *);


    /* Parameter adjustments */
    --wsave;
    --b;
    --a;
    --r__;

    /* Function Body */
    if ((i__1 = *n - 2) < 0) {
	goto L101;
    } else if (i__1 == 0) {
	goto L102;
    } else {
	goto L103;
    }
L101:
    *azero = r__[1];
    return 0;
L102:
    *azero = (r__[1] + r__[2]) * .5f;
    a[1] = (r__[1] - r__[2]) * .5f;
    return 0;
L103:
    i__1 = *n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	wsave[i__] = r__[i__];
/* L104: */
    }
    rfftf_(n, &wsave[1], &wsave[*n + 1]);
    cf = 2.f / (real) (*n);
    cfm = -cf;
    *azero = cf * .5f * wsave[1];
    ns2 = (*n + 1) / 2;
    ns2m = ns2 - 1;
    i__1 = ns2m;
    for (i__ = 1; i__ <= i__1; ++i__) {
	a[i__] = cf * wsave[i__ * 2];
	b[i__] = cfm * wsave[(i__ << 1) + 1];
/* L105: */
    }
    if (*n % 2 == 1) {
	return 0;
    }
    a[ns2] = cf * .5f * wsave[*n];
    b[ns2] = 0.f;
    return 0;
} /* ezfftf_ */


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     SUBROUTINE SINTI(N,WSAVE) */

/*     SUBROUTINE SINTI INITIALIZES THE ARRAY WSAVE WHICH IS USED IN */
/*     SUBROUTINE SINT. THE PRIME FACTORIZATION OF N TOGETHER WITH */
/*     A TABULATION OF THE TRIGONOMETRIC FUNCTIONS ARE COMPUTED AND */
/*     STORED IN WSAVE. */

/*     INPUT PARAMETER */

/*     N       THE LENGTH OF THE SEQUENCE TO BE TRANSFORMED.  THE METHOD */
/*             IS MOST EFFICIENT WHEN N+1 IS A PRODUCT OF SMALL PRIMES. */

/*     OUTPUT PARAMETER */

/*     WSAVE   A WORK ARRAY WITH AT LEAST INT(2.5*N+15) LOCATIONS. */
/*             DIFFERENT WSAVE ARRAYS ARE REQUIRED FOR DIFFERENT VALUES */
/*             OF N. THE CONTENTS OF WSAVE MUST NOT BE CHANGED BETWEEN */
/*             CALLS OF SINT. */

/* Subroutine */ int sinti_(integer *n, real *wsave)
{
    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    double sin(doublereal);

    /* Local variables */
    static integer k;
    static real dt, pi;
    static integer np1, ns2;
    extern real rfpi_(void);
    extern /* Subroutine */ int rffti_(integer *, real *);


    /* Parameter adjustments */
    --wsave;

    /* Function Body */
    pi = rfpi_();
    if (*n <= 1) {
	return 0;
    }
    ns2 = *n / 2;
    np1 = *n + 1;
    dt = pi / (real) np1;
    i__1 = ns2;
    for (k = 1; k <= i__1; ++k) {
	wsave[k] = sin(k * dt) * 2.f;
/* L101: */
    }
    rffti_(&np1, &wsave[ns2 + 1]);
    return 0;
} /* sinti_ */


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     SUBROUTINE SINT(N,X,WSAVE) */

/*     SUBROUTINE SINT COMPUTES THE DISCRETE FOURIER SINE TRANSFORM */
/*     OF AN ODD SEQUENCE X(I). THE TRANSFORM IS DEFINED BELOW AT */
/*     OUTPUT PARAMETER X. */

/*     SINT IS THE UNNORMALIZED INVERSE OF ITSELF SINCE A CALL OF SINT */
/*     FOLLOWED BY ANOTHER CALL OF SINT WILL MULTIPLY THE INPUT SEQUENCE */
/*     X BY 2*(N+1). */

/*     THE ARRAY WSAVE WHICH IS USED BY SUBROUTINE SINT MUST BE */
/*     INITIALIZED BY CALLING SUBROUTINE SINTI(N,WSAVE). */

/*     INPUT PARAMETERS */

/*     N       THE LENGTH OF THE SEQUENCE TO BE TRANSFORMED.  THE METHOD */
/*             IS MOST EFFICIENT WHEN N+1 IS THE PRODUCT OF SMALL PRIMES. */

/*     X       AN ARRAY WHICH CONTAINS THE SEQUENCE TO BE TRANSFORMED */


/*     WSAVE   A WORK ARRAY WITH DIMENSION AT LEAST INT(2.5*N+15) */
/*             IN THE PROGRAM THAT CALLS SINT. THE WSAVE ARRAY MUST BE */
/*             INITIALIZED BY CALLING SUBROUTINE SINTI(N,WSAVE) AND A */
/*             DIFFERENT WSAVE ARRAY MUST BE USED FOR EACH DIFFERENT */
/*             VALUE OF N. THIS INITIALIZATION DOES NOT HAVE TO BE */
/*             REPEATED SO LONG AS N REMAINS UNCHANGED THUS SUBSEQUENT */
/*             TRANSFORMS CAN BE OBTAINED FASTER THAN THE FIRST. */

/*     OUTPUT PARAMETERS */

/*     X       FOR I=1,...,N */

/*                  X(I)= THE SUM FROM K=1 TO K=N */

/*                       2*X(K)*SIN(K*I*PI/(N+1)) */

/*                  A CALL OF SINT FOLLOWED BY ANOTHER CALL OF */
/*                  SINT WILL MULTIPLY THE SEQUENCE X BY 2*(N+1). */
/*                  HENCE SINT IS THE UNNORMALIZED INVERSE */
/*                  OF ITSELF. */

/*     WSAVE   CONTAINS INITIALIZATION CALCULATIONS WHICH MUST NOT BE */
/*             DESTROYED BETWEEN CALLS OF SINT. */

/* Subroutine */ int sint_(integer *n, real *x, real *wsave)
{
    static integer np1, iw1, iw2, iw3;
    extern /* Subroutine */ int sint1_(integer *, real *, real *, real *, 
	    real *, real *);


    /* Parameter adjustments */
    --wsave;
    --x;

    /* Function Body */
    np1 = *n + 1;
    iw1 = *n / 2 + 1;
    iw2 = iw1 + np1;
    iw3 = iw2 + np1;
    sint1_(n, &x[1], &wsave[1], &wsave[iw1], &wsave[iw2], &wsave[iw3]);
    return 0;
} /* sint_ */


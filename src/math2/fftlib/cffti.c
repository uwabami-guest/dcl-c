/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     SUBROUTINE CFFTI(N,WSAVE) */

/*     SUBROUTINE CFFTI INITIALIZES THE ARRAY WSAVE WHICH IS USED IN */
/*     BOTH CFFTF AND CFFTB. THE PRIME FACTORIZATION OF N TOGETHER WITH */
/*     A TABULATION OF THE TRIGONOMETRIC FUNCTIONS ARE COMPUTED AND */
/*     STORED IN WSAVE. */

/*     INPUT PARAMETER */

/*     N       THE LENGTH OF THE SEQUENCE TO BE TRANSFORMED */

/*     OUTPUT PARAMETER */

/*     WSAVE   A WORK ARRAY WHICH MUST BE DIMENSIONED AT LEAST 4*N+15 */
/*             THE SAME WORK ARRAY CAN BE USED FOR BOTH CFFTF AND CFFTB */
/*             AS LONG AS N REMAINS UNCHANGED. DIFFERENT WSAVE ARRAYS */
/*             ARE REQUIRED FOR DIFFERENT VALUES OF N. THE CONTENTS OF */
/*             WSAVE MUST NOT BE CHANGED BETWEEN CALLS OF CFFTF OR CFFTB. */

/* Subroutine */ int cffti_(integer *n, real *wsave)
{
    static integer iw1, iw2;
    extern /* Subroutine */ int cffti1_(integer *, real *, real *);


    /* Parameter adjustments */
    --wsave;

    /* Function Body */
    if (*n == 1) {
	return 0;
    }
    iw1 = *n + *n + 1;
    iw2 = iw1 + *n + *n;
    cffti1_(n, &wsave[iw1], &wsave[iw2]);
    return 0;
} /* cffti_ */


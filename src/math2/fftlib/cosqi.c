/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     SUBROUTINE COSQI(N,WSAVE) */

/*     SUBROUTINE COSQI INITIALIZES THE ARRAY WSAVE WHICH IS USED IN */
/*     BOTH COSQF AND COSQB. THE PRIME FACTORIZATION OF N TOGETHER WITH */
/*     A TABULATION OF THE TRIGONOMETRIC FUNCTIONS ARE COMPUTED AND */
/*     STORED IN WSAVE. */

/*     INPUT PARAMETER */

/*     N       THE LENGTH OF THE ARRAY TO BE TRANSFORMED.  THE METHOD */
/*             IS MOST EFFICIENT WHEN N IS A PRODUCT OF SMALL PRIMES. */

/*     OUTPUT PARAMETER */

/*     WSAVE   A WORK ARRAY WHICH MUST BE DIMENSIONED AT LEAST 3*N+15. */
/*             THE SAME WORK ARRAY CAN BE USED FOR BOTH COSQF AND COSQB */
/*             AS LONG AS N REMAINS UNCHANGED. DIFFERENT WSAVE ARRAYS */
/*             ARE REQUIRED FOR DIFFERENT VALUES OF N. THE CONTENTS OF */
/*             WSAVE MUST NOT BE CHANGED BETWEEN CALLS OF COSQF OR COSQB. */

/* Subroutine */ int cosqi_(integer *n, real *wsave)
{
    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    double cos(doublereal);

    /* Local variables */
    static integer k;
    static real fk, dt, pih;
    extern real rfpi_(void);
    extern /* Subroutine */ int rffti_(integer *, real *);


    /* Parameter adjustments */
    --wsave;

    /* Function Body */
    pih = rfpi_() * .5f;
    dt = pih / (real) (*n);
    fk = 0.f;
    i__1 = *n;
    for (k = 1; k <= i__1; ++k) {
	fk += 1.f;
	wsave[k] = cos(fk * dt);
/* L101: */
    }
    rffti_(n, &wsave[*n + 1]);
    return 0;
} /* cosqi_ */


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     SUBROUTINE SINQB(N,X,WSAVE) */

/*     SUBROUTINE SINQB COMPUTES THE FAST FOURIER TRANSFORM OF QUARTER */
/*     WAVE DATA. THAT IS , SINQB COMPUTES A SEQUENCE FROM ITS */
/*     REPRESENTATION IN TERMS OF A SINE SERIES WITH ODD WAVE NUMBERS. */
/*     THE TRANSFORM IS DEFINED BELOW AT OUTPUT PARAMETER X. */

/*     SINQF IS THE UNNORMALIZED INVERSE OF SINQB SINCE A CALL OF SINQB */
/*     FOLLOWED BY A CALL OF SINQF WILL MULTIPLY THE INPUT SEQUENCE X */
/*     BY 4*N. */

/*     THE ARRAY WSAVE WHICH IS USED BY SUBROUTINE SINQB MUST BE */
/*     INITIALIZED BY CALLING SUBROUTINE SINQI(N,WSAVE). */


/*     INPUT PARAMETERS */

/*     N       THE LENGTH OF THE ARRAY X TO BE TRANSFORMED.  THE METHOD */
/*             IS MOST EFFICIENT WHEN N IS A PRODUCT OF SMALL PRIMES. */

/*     X       AN ARRAY WHICH CONTAINS THE SEQUENCE TO BE TRANSFORMED */

/*     WSAVE   A WORK ARRAY WHICH MUST BE DIMENSIONED AT LEAST 3*N+15. */
/*             IN THE PROGRAM THAT CALLS SINQB. THE WSAVE ARRAY MUST BE */
/*             INITIALIZED BY CALLING SUBROUTINE SINQI(N,WSAVE) AND A */
/*             DIFFERENT WSAVE ARRAY MUST BE USED FOR EACH DIFFERENT */
/*             VALUE OF N. THIS INITIALIZATION DOES NOT HAVE TO BE */
/*             REPEATED SO LONG AS N REMAINS UNCHANGED THUS SUBSEQUENT */
/*             TRANSFORMS CAN BE OBTAINED FASTER THAN THE FIRST. */

/*     OUTPUT PARAMETERS */

/*     X       FOR I=1,...,N */

/*                  X(I)= THE SUM FROM K=1 TO K=N OF */

/*                    4*X(K)*SIN((2K-1)*I*PI/(2*N)) */

/*                  A CALL OF SINQB FOLLOWED BY A CALL OF */
/*                  SINQF WILL MULTIPLY THE SEQUENCE X BY 4*N. */
/*                  THEREFORE SINQF IS THE UNNORMALIZED INVERSE */
/*                  OF SINQB. */

/*     WSAVE   CONTAINS INITIALIZATION CALCULATIONS WHICH MUST NOT */
/*             BE DESTROYED BETWEEN CALLS OF SINQB OR SINQF. */

/* Subroutine */ int sinqb_(integer *n, real *x, real *wsave)
{
    /* System generated locals */
    integer i__1;

    /* Local variables */
    static integer k, kc, ns2;
    extern /* Subroutine */ int cosqb_(integer *, real *, real *);
    static real xhold;


    /* Parameter adjustments */
    --wsave;
    --x;

    /* Function Body */
    if (*n > 1) {
	goto L101;
    }
    x[1] *= 4.f;
    return 0;
L101:
    ns2 = *n / 2;
    i__1 = *n;
    for (k = 2; k <= i__1; k += 2) {
	x[k] = -x[k];
/* L102: */
    }
    cosqb_(n, &x[1], &wsave[1]);
    i__1 = ns2;
    for (k = 1; k <= i__1; ++k) {
	kc = *n - k;
	xhold = x[k];
	x[k] = x[kc + 1];
	x[kc + 1] = xhold;
/* L103: */
    }
    return 0;
} /* sinqb_ */


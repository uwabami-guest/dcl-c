/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     SUBROUTINE CFFTF(N,C,WSAVE) */

/*     SUBROUTINE CFFTF COMPUTES THE FORWARD COMPLEX DISCRETE FOURIER */
/*     TRANSFORM (THE FOURIER ANALYSIS). EQUIVALENTLY , CFFTF COMPUTES */
/*     THE FOURIER COEFFICIENTS OF A COMPLEX PERIODIC SEQUENCE. */
/*     THE TRANSFORM IS DEFINED BELOW AT OUTPUT PARAMETER C. */

/*     THE TRANSFORM IS NOT NORMALIZED. TO OBTAIN A NORMALIZED TRANSFORM */
/*     THE OUTPUT MUST BE DIVIDED BY N. OTHERWISE A CALL OF CFFTF */
/*     FOLLOWED BY A CALL OF CFFTB WILL MULTIPLY THE SEQUENCE BY N. */

/*     THE ARRAY WSAVE WHICH IS USED BY SUBROUTINE CFFTF MUST BE */
/*     INITIALIZED BY CALLING SUBROUTINE CFFTI(N,WSAVE). */

/*     INPUT PARAMETERS */


/*     N      THE LENGTH OF THE COMPLEX SEQUENCE C. THE METHOD IS */
/*            MORE EFFICIENT WHEN N IS THE PRODUCT OF SMALL PRIMES. N */

/*     C      A COMPLEX ARRAY OF LENGTH N WHICH CONTAINS THE SEQUENCE */

/*     WSAVE   A REAL WORK ARRAY WHICH MUST BE DIMENSIONED AT LEAST 4N+15 */
/*             IN THE PROGRAM THAT CALLS CFFTF. THE WSAVE ARRAY MUST BE */
/*             INITIALIZED BY CALLING SUBROUTINE CFFTI(N,WSAVE) AND A */
/*             DIFFERENT WSAVE ARRAY MUST BE USED FOR EACH DIFFERENT */
/*             VALUE OF N. THIS INITIALIZATION DOES NOT HAVE TO BE */
/*             REPEATED SO LONG AS N REMAINS UNCHANGED THUS SUBSEQUENT */
/*             TRANSFORMS CAN BE OBTAINED FASTER THAN THE FIRST. */
/*             THE SAME WSAVE ARRAY CAN BE USED BY CFFTF AND CFFTB. */

/*     OUTPUT PARAMETERS */

/*     C      FOR J=1,...,N */

/*                C(J)=THE SUM FROM K=1,...,N OF */

/*                      C(K)*EXP(-I*(J-1)*(K-1)*2*PI/N) */

/*                            WHERE I=SQRT(-1) */

/*     WSAVE   CONTAINS INITIALIZATION CALCULATIONS WHICH MUST NOT BE */
/*             DESTROYED BETWEEN CALLS OF SUBROUTINE CFFTF OR CFFTB */

/* Subroutine */ int cfftf_(integer *n, real *c__, real *wsave)
{
    static integer iw1, iw2;
    extern /* Subroutine */ int cfftf1_(integer *, real *, real *, real *, 
	    real *);


    /* Parameter adjustments */
    --wsave;
    --c__;

    /* Function Body */
    if (*n == 1) {
	return 0;
    }
    iw1 = *n + *n + 1;
    iw2 = iw1 + *n + *n;
    cfftf1_(n, &c__[1], &wsave[1], &wsave[iw1], &wsave[iw2]);
    return 0;
} /* cfftf_ */


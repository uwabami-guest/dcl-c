/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     CDBLK */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int cdblk_(char *chr, ftnlen chr_len)
{
    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    integer i_len(char *, ftnlen);

    /* Local variables */
    static integer i__, j, n, nc;
    static logical lblk;

    lblk = TRUE_;
    nc = i_len(chr, chr_len);
    n = 0;
    i__1 = nc;
    for (i__ = 1; i__ <= i__1; ++i__) {
	if (! (*(unsigned char *)&chr[i__ - 1] == ' ' && lblk)) {
	    ++n;
	    if (i__ != n) {
		*(unsigned char *)&chr[n - 1] = *(unsigned char *)&chr[i__ - 
			1];
	    }
	}
	lblk = *(unsigned char *)&chr[i__ - 1] == ' ';
/* L10: */
    }
    i__1 = nc;
    for (j = n + 1; j <= i__1; ++j) {
	*(unsigned char *)&chr[j - 1] = ' ';
/* L20: */
    }
    return 0;
} /* cdblk_ */


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     LCHR */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
logical lchr_(char *char__, char *cref, ftnlen char_len, ftnlen cref_len)
{
    /* System generated locals */
    integer i__1;
    logical ret_val;

    /* Builtin functions */
    integer i_len(char *, ftnlen);

    /* Local variables */
    static integer i__, nch, ncr;
    extern logical lchra_(char *, ftnlen), lchrb_(char *, ftnlen), lchrc_(
	    char *, ftnlen), lchrd_(char *, ftnlen), lchrf_(char *, ftnlen), 
	    lchrl_(char *, ftnlen), lchrs_(char *, ftnlen);
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen);

    nch = i_len(char__, char_len);
    ncr = i_len(cref, cref_len);
    if (nch != ncr) {
	msgdmp_("E", "LCHR  ", "LENGTH OF CHAR IS NOT EQUAL TO THAT OF CREF.",
		 (ftnlen)1, (ftnlen)6, (ftnlen)44);
    }
    ret_val = TRUE_;
    i__1 = nch;
    for (i__ = 1; i__ <= i__1; ++i__) {
	if (*(unsigned char *)&cref[i__ - 1] == 'F') {
	    ret_val = ret_val && lchrf_(char__ + (i__ - 1), (ftnlen)1);
	} else if (*(unsigned char *)&cref[i__ - 1] == 'A') {
	    ret_val = ret_val && lchra_(char__ + (i__ - 1), (ftnlen)1);
	} else if (*(unsigned char *)&cref[i__ - 1] == 'S') {
	    ret_val = ret_val && lchrs_(char__ + (i__ - 1), (ftnlen)1);
	} else if (*(unsigned char *)&cref[i__ - 1] == 'L') {
	    ret_val = ret_val && lchrl_(char__ + (i__ - 1), (ftnlen)1);
	} else if (*(unsigned char *)&cref[i__ - 1] == 'D') {
	    ret_val = ret_val && lchrd_(char__ + (i__ - 1), (ftnlen)1);
	} else if (*(unsigned char *)&cref[i__ - 1] == 'B') {
	    ret_val = ret_val && lchrb_(char__ + (i__ - 1), (ftnlen)1);
	} else if (*(unsigned char *)&cref[i__ - 1] == 'C') {
	    ret_val = ret_val && lchrc_(char__ + (i__ - 1), (ftnlen)1);
	} else {
	    msgdmp_("E", "LCHR  ", "INVALID REFERENCE NAME.", (ftnlen)1, (
		    ftnlen)6, (ftnlen)23);
	}
	if (! ret_val) {
	    return ret_val;
	}
/* L10: */
    }
    return ret_val;
} /* lchr_ */


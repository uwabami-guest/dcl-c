/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     LCHRC */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
logical lchrc_(char *ch, ftnlen ch_len)
{
    /* Initialized data */

    static char clst[1*1] = "$";

    /* System generated locals */
    integer i__1;
    logical ret_val;

    /* Builtin functions */
    integer i_len(char *, ftnlen);

    /* Local variables */
    static integer i__, nch, nidx;
    extern integer indxcf_(char *, integer *, integer *, char *, ftnlen, 
	    ftnlen);

    nch = i_len(ch, ch_len);
    ret_val = TRUE_;
    i__1 = nch;
    for (i__ = 1; i__ <= i__1; ++i__) {
	nidx = indxcf_(clst, &c__1, &c__1, ch + (i__ - 1), (ftnlen)1, (ftnlen)
		1);
	ret_val = ret_val && nidx != 0;
	if (! ret_val) {
	    return ret_val;
	}
/* L10: */
    }
    return ret_val;
} /* lchrc_ */


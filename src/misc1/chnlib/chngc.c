/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;
static integer c__3 = 3;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int chngc_(char *ch, char *ca, char *cb, ftnlen ch_len, 
	ftnlen ca_len, ftnlen cb_len)
{
    /* System generated locals */
    address a__1[3];
    integer i__1, i__2[3];
    char ch__1[1051];

    /* Builtin functions */
    integer i_len(char *, ftnlen);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen), s_cat(char *,
	     char **, integer *, integer *, ftnlen);

    /* Local variables */
    static integer na, nb, nh, nn, id1, id2;
    static char cpat[1024];
    extern integer indxnf_(char *, integer *, integer *, char *, ftnlen, 
	    ftnlen);
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen);

    nh = i_len(ch, ch_len);
    na = i_len(ca, ca_len);
    nb = i_len(cb, cb_len);
    s_copy(cpat, ca, (ftnlen)1024, na);
    if (! (na == nb)) {
	msgdmp_("E", "CHNGC", "CHARACTER LENGTH IS INCONSISTENT.", (ftnlen)1, 
		(ftnlen)5, (ftnlen)33);
    }
    nn = na;
    if (! (nh >= nn)) {
	msgdmp_("E", "CHNGC", "TEXT LENGTH IS TOO SHORT.", (ftnlen)1, (ftnlen)
		5, (ftnlen)25);
    }
    i__1 = nh - nn + 1;
    id1 = indxnf_(ch, &i__1, &c__1, ca, ch_len, ca_len);
    id2 = id1 + nn - 1;
    if (id1 == 0) {
/* Writing concatenation */
	i__2[0] = 10, a__1[0] = "PATTERN < ";
	i__2[1] = na, a__1[1] = cpat;
	i__2[2] = 17, a__1[2] = " > WAS NOT FOUND.";
	s_cat(ch__1, a__1, i__2, &c__3, (ftnlen)1051);
	msgdmp_("W", "CHNGC", ch__1, (ftnlen)1, (ftnlen)5, na + 27);
    } else {
	s_copy(ch + (id1 - 1), cb, id2 - (id1 - 1), cb_len);
    }
    return 0;
} /* chngc_ */


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;
static integer c__0 = 0;
static integer c__127 = 127;
static integer c__7 = 7;
static integer c_b8 = 16777215;
static integer c__8 = 8;
static integer c__24 = 24;
static integer c_n31 = -31;
static integer c_n24 = -24;
static real c_b13 = 16.f;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
real r4ibm_(integer *ii)
{
    /* Initialized data */

    static logical lfrst = TRUE_;

    /* System generated locals */
    integer i__1;
    real ret_val;

    /* Builtin functions */
    double pow_ri(real *, integer *);

    /* Local variables */
    static integer ie, is, ix;
    extern /* Subroutine */ int sbyte_(integer *, integer *, integer *, 
	    integer *);
    static integer iemask;
    extern integer ishift_(integer *, integer *);
    static integer ismask, ixmask;

    if (lfrst) {
	ismask = 0;
	sbyte_(&ismask, &c__1, &c__0, &c__1);
	iemask = 0;
	sbyte_(&iemask, &c__127, &c__1, &c__7);
	ixmask = 0;
	sbyte_(&ixmask, &c_b8, &c__8, &c__24);
	lfrst = FALSE_;
    }
    if (*ii == 0) {
	ret_val = 0.f;
	return ret_val;
    }
    i__1 = ismask & *ii;
    is = ishift_(&i__1, &c_n31);
    i__1 = iemask & *ii;
    ie = ishift_(&i__1, &c_n24);
    ix = ixmask & *ii;
    if (is != 0) {
	i__1 = ie - 65;
	ret_val = -(ix / 1048576.f) * pow_ri(&c_b13, &i__1);
    } else {
	i__1 = ie - 65;
	ret_val = ix / 1048576.f * pow_ri(&c_b13, &i__1);
    }
    return ret_val;
} /* r4ibm_ */


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;
static integer c__0 = 0;
static integer c__255 = 255;
static integer c__8 = 8;
static integer c_b8 = 8388607;
static integer c__9 = 9;
static integer c__23 = 23;
static integer c_n31 = -31;
static integer c_n23 = -23;
static integer c_n1 = -1;
static real c_b14 = 2.f;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
real r4ieee_(integer *ii)
{
    /* Initialized data */

    static logical lfrst = TRUE_;

    /* System generated locals */
    integer i__1;
    real ret_val;

    /* Builtin functions */
    integer pow_ii(integer *, integer *);
    double pow_ri(real *, integer *);

    /* Local variables */
    static integer ie, is, ix;
    extern /* Subroutine */ int sbyte_(integer *, integer *, integer *, 
	    integer *);
    static integer iemask;
    extern integer ishift_(integer *, integer *);
    static integer ismask, ixmask;

    if (lfrst) {
	ismask = 0;
	sbyte_(&ismask, &c__1, &c__0, &c__1);
	iemask = 0;
	sbyte_(&iemask, &c__255, &c__1, &c__8);
	ixmask = 0;
	sbyte_(&ixmask, &c_b8, &c__9, &c__23);
	lfrst = FALSE_;
    }
    if (*ii == 0) {
	ret_val = 0.f;
	return ret_val;
    }
    i__1 = ismask & *ii;
    is = ishift_(&i__1, &c_n31);
    i__1 = iemask & *ii;
    ie = ishift_(&i__1, &c_n23);
    ix = ixmask & *ii;
    if (ie == 0) {
	i__1 = ie - 126;
	ret_val = pow_ii(&c_n1, &is) * (ix / 8388608.f) * pow_ri(&c_b14, &
		i__1);
    } else {
	i__1 = ie - 127;
	ret_val = pow_ii(&c_n1, &is) * (ix / 8388608.f + 1) * pow_ri(&c_b14, &
		i__1);
    }
    return ret_val;
} /* r4ieee_ */


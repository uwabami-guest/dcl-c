/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     DATEF3 */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int datef3_(integer *n, integer *iy, integer *im, integer *
	id, integer *ny, integer *nm, integer *nd)
{
    static integer itd, ntd;
    extern /* Subroutine */ int date32_(integer *, integer *, integer *, 
	    integer *), date23_(integer *, integer *, integer *, integer *), 
	    datef2_(integer *, integer *, integer *, integer *, integer *);

    date32_(iy, im, id, &itd);
    datef2_(n, iy, &itd, ny, &ntd);
    date23_(ny, nm, nd, &ntd);
    return 0;
} /* datef3_ */


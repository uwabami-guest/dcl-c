/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     DATE23 */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int date23_(integer *iy, integer *im, integer *id, integer *
	itd)
{
    /* Initialized data */

    static integer mn[12] = { 31,28,31,30,31,30,31,31,30,31,30,31 };

    static logical leap;

/*     IY   : YEAR                                               (I/O) */
/*     IM   : MONTH                                              ( /O) */
/*     ID   : DAY                                                ( /O) */
/*     ITD  : TOTAL DAY                                          (I/ ) */
    leap = *iy % 4 == 0 && *iy % 100 != 0 || *iy % 400 == 0;
    if (leap) {
	mn[1] = 29;
    } else {
	mn[1] = 28;
    }
    *id = *itd;
    *im = 1;
L11:
    if (! (*id > mn[*im - 1])) {
	goto L10;
    }
    *id -= mn[*im - 1];
    ++(*im);
    goto L11;
L10:
    return 0;
} /* date23_ */


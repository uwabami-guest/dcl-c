/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     IWEEK1 */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
integer iweek1_(integer *idate)
{
    /* System generated locals */
    integer ret_val;

    /* Local variables */
    static integer iy, itd;
    extern /* Subroutine */ int date12_(integer *, integer *, integer *);
    extern integer iweek2_(integer *, integer *);

    date12_(idate, &iy, &itd);
    ret_val = iweek2_(&iy, &itd);
    return ret_val;
} /* iweek1_ */


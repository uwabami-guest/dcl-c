/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     DATEF2 */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int datef2_(integer *n, integer *iy, integer *itd, integer *
	ny, integer *ntd)
{
    /* System generated locals */
    integer i__1;

    /* Local variables */
    extern integer ndyear_(integer *);

    *ny = *iy;
    *ntd = *itd + *n;
    if (*ntd <= 0) {
L11:
	if (! (*ntd <= 0)) {
	    goto L10;
	}
	i__1 = *ny - 1;
	*ntd += ndyear_(&i__1);
	--(*ny);
	goto L11;
L10:
	;
    } else {
L16:
	if (! (*ntd > ndyear_(ny))) {
	    goto L15;
	}
	*ntd -= ndyear_(ny);
	++(*ny);
	goto L16;
L15:
	;
    }
    return 0;
} /* datef2_ */


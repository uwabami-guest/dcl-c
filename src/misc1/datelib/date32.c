/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     DATE32 */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int date32_(integer *iy, integer *im, integer *id, integer *
	itd)
{
    /* Initialized data */

    static integer mn[12] = { 31,28,31,30,31,30,31,31,30,31,30,31 };

    /* System generated locals */
    integer i__1;

    /* Local variables */
    static integer i__;
    static logical leap;

/*     IY   : YEAR                                               (I/O) */
/*     IM   : MONTH                                              (I/ ) */
/*     ID   : DAY                                                (I/ ) */
/*     ITD  : TOTAL DAY                                          ( /O) */
    leap = *iy % 4 == 0 && *iy % 100 != 0 || *iy % 400 == 0;
    if (leap) {
	mn[1] = 29;
    } else {
	mn[1] = 28;
    }
    *itd = *id;
    i__1 = *im - 1;
    for (i__ = 1; i__ <= i__1; ++i__) {
	*itd += mn[i__ - 1];
/* L10: */
    }
    return 0;
} /* date32_ */


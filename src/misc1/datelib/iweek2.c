/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1989 = 1989;
static integer c__365 = 365;
static integer c__7 = 7;

/* ----------------------------------------------------------------------- */
/*     IWEEK2 */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
integer iweek2_(integer *iy, integer *itd)
{
    /* System generated locals */
    integer ret_val;

    /* Local variables */
    static integer n;
    extern integer imod_(integer *, integer *);
    extern /* Subroutine */ int dateg2_(integer *, integer *, integer *, 
	    integer *, integer *);

    dateg2_(&n, &c__1989, &c__365, iy, itd);
    ret_val = imod_(&n, &c__7) + 1;
    return ret_val;
} /* iweek2_ */


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;
static integer c__10 = 10;

/* ----------------------------------------------------------------------- */
/*     DATEC3 */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int datec3_(char *cform, integer *iy, integer *im, integer *
	id, ftnlen cform_len)
{
    /* Initialized data */

    static char cd[1*3] = "Y" "M" "D";

    /* System generated locals */
    integer i__1;
    char ch__1[9], ch__2[9];
    icilist ici__1;

    /* Builtin functions */
    integer s_wsfi(icilist *), do_fio(integer *, char *, ftnlen), e_wsfi(void)
	    , pow_ii(integer *, integer *);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer i__, jd[3], mc, ncm, ncw, idx1, idx2;
    extern integer lenc_(char *, ftnlen);
    static char cfmt[4];
    extern /* Character */ VOID cmon_(char *, ftnlen, integer *);
    extern /* Subroutine */ int cradj_(char *, ftnlen);
    extern /* Character */ VOID cweek_(char *, ftnlen, integer *);
    extern integer iweek3_(integer *, integer *, integer *), indxcf_(char *, 
	    integer *, integer *, char *, ftnlen, ftnlen), indxcl_(char *, 
	    integer *, integer *, char *, ftnlen, ftnlen);

    /* Fortran I/O blocks */
    static icilist io___9 = { 0, cfmt, 0, "(A2,I1,A1)", 4, 1 };
    static icilist io___10 = { 0, cfmt, 0, "(A2,I1,A1)", 4, 1 };
    static icilist io___12 = { 0, cfmt, 0, "(A2,I1,A1)", 4, 1 };


    mc = lenc_(cform, cform_len);
    jd[0] = *iy;
    jd[1] = *im;
    jd[2] = *id;
    for (i__ = 1; i__ <= 3; ++i__) {
	idx1 = indxcf_(cform, &mc, &c__1, cd + (i__ - 1), cform_len, (ftnlen)
		1);
	idx2 = indxcl_(cform, &mc, &c__1, cd + (i__ - 1), cform_len, (ftnlen)
		1);
	if (idx1 > 0) {
	    ncm = idx2 - idx1 + 1;
	    s_wsfi(&io___9);
	    do_fio(&c__1, "(I", (ftnlen)2);
	    do_fio(&c__1, (char *)&ncm, (ftnlen)sizeof(integer));
	    do_fio(&c__1, ")", (ftnlen)1);
	    e_wsfi();
	    ici__1.icierr = 0;
	    ici__1.icirnum = 1;
	    ici__1.icirlen = idx2 - (idx1 - 1);
	    ici__1.iciunit = cform + (idx1 - 1);
	    ici__1.icifmt = cfmt;
	    s_wsfi(&ici__1);
	    i__1 = jd[i__ - 1] % pow_ii(&c__10, &ncm);
	    do_fio(&c__1, (char *)&i__1, (ftnlen)sizeof(integer));
	    e_wsfi();
	}
/* L10: */
    }
    idx1 = indxcf_(cform, &mc, &c__1, "C", cform_len, (ftnlen)1);
    idx2 = indxcl_(cform, &mc, &c__1, "C", cform_len, (ftnlen)1);
    if (idx1 > 0) {
	ncm = idx2 - idx1 + 1;
	s_wsfi(&io___10);
	do_fio(&c__1, "(A", (ftnlen)2);
	do_fio(&c__1, (char *)&ncm, (ftnlen)sizeof(integer));
	do_fio(&c__1, ")", (ftnlen)1);
	e_wsfi();
	ici__1.icierr = 0;
	ici__1.icirnum = 1;
	ici__1.icirlen = idx2 - (idx1 - 1);
	ici__1.iciunit = cform + (idx1 - 1);
	ici__1.icifmt = cfmt;
	s_wsfi(&ici__1);
	cmon_(ch__2, (ftnlen)9, &jd[1]);
	s_copy(ch__1, ch__2, (ftnlen)9, (ftnlen)9);
	do_fio(&c__1, ch__1, (ftnlen)9);
	e_wsfi();
	cradj_(cform + (idx1 - 1), idx2 - (idx1 - 1));
    }
    idx1 = indxcf_(cform, &mc, &c__1, "W", cform_len, (ftnlen)1);
    idx2 = indxcl_(cform, &mc, &c__1, "W", cform_len, (ftnlen)1);
    if (idx1 > 0) {
	ncw = idx2 - idx1 + 1;
	s_wsfi(&io___12);
	do_fio(&c__1, "(A", (ftnlen)2);
	do_fio(&c__1, (char *)&ncw, (ftnlen)sizeof(integer));
	do_fio(&c__1, ")", (ftnlen)1);
	e_wsfi();
	ici__1.icierr = 0;
	ici__1.icirnum = 1;
	ici__1.icirlen = idx2 - (idx1 - 1);
	ici__1.iciunit = cform + (idx1 - 1);
	ici__1.icifmt = cfmt;
	s_wsfi(&ici__1);
	i__1 = iweek3_(iy, im, id);
	cweek_(ch__2, (ftnlen)9, &i__1);
	s_copy(ch__1, ch__2, (ftnlen)9, (ftnlen)9);
	do_fio(&c__1, ch__1, (ftnlen)9);
	e_wsfi();
	cradj_(cform + (idx1 - 1), idx2 - (idx1 - 1));
    }
    return 0;
} /* datec3_ */


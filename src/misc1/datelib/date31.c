/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     DATE31 */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int date31_(integer *idate, integer *iy, integer *im, 
	integer *id)
{
/*     IDATE : DATE (IY*10000+IM*100+ID)                         ( /O) */
/*     IY    : YEAR                                              (I/ ) */
/*     IM    : MONTH                                             (I/ ) */
/*     ID    : DAY                                               (I/ ) */
    *idate = *iy * 10000 + *im * 100 + *id;
    return 0;
} /* date31_ */


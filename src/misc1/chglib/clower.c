/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     CLOWER */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int clower_(char *ch, ftnlen ch_len)
{
    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    integer i_len(char *, ftnlen);

    /* Local variables */
    static integer i__, lch, idx;

    lch = i_len(ch, ch_len);
    i__1 = lch;
    for (i__ = 1; i__ <= i__1; ++i__) {
	idx = *(unsigned char *)&ch[i__ - 1];
	if (65 <= idx && idx <= 90) {
	    *(unsigned char *)&ch[i__ - 1] = (char) (idx + 32);
	}
/* L10: */
    }
    return 0;
} /* clower_ */


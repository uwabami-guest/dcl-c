/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/* Subroutine */ int cllsrg_(real *h__, real *l, real *s, integer *r__, 
	integer *g, integer *b, integer *n, integer *m)
{
    /* System generated locals */
    integer r_dim1, r_offset, g_dim1, g_offset, b_dim1, b_offset, h_dim1, 
	    h_offset, l_dim1, l_offset, s_dim1, s_offset, i__1, i__2;

    /* Local variables */
    static integer i__, j, min__, max__;

    /* Parameter adjustments */
    b_dim1 = *n;
    b_offset = 1 + b_dim1;
    b -= b_offset;
    g_dim1 = *n;
    g_offset = 1 + g_dim1;
    g -= g_offset;
    r_dim1 = *n;
    r_offset = 1 + r_dim1;
    r__ -= r_offset;
    s_dim1 = *n;
    s_offset = 1 + s_dim1;
    s -= s_offset;
    l_dim1 = *n;
    l_offset = 1 + l_dim1;
    l -= l_offset;
    h_dim1 = *n;
    h_offset = 1 + h_dim1;
    h__ -= h_offset;

    /* Function Body */
    i__1 = *n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	i__2 = *m;
	for (j = 1; j <= i__2; ++j) {
	    if (l[i__ + j * l_dim1] <= 50.f) {
		max__ = (l[i__ + j * l_dim1] + l[i__ + j * l_dim1] * s[i__ + 
			j * s_dim1] / 100.f) * 2.55f;
		min__ = (l[i__ + j * l_dim1] - l[i__ + j * l_dim1] * s[i__ + 
			j * s_dim1] / 100.f) * 2.55f;
	    } else {
		max__ = (l[i__ + j * l_dim1] + (100.f - l[i__ + j * l_dim1]) *
			 (s[i__ + j * s_dim1] / 100.f)) * 2.55f;
		min__ = (l[i__ + j * l_dim1] - (100.f - l[i__ + j * l_dim1]) *
			 (s[i__ + j * s_dim1] / 100.f)) * 2.55f;
	    }
	    if (300.f <= h__[i__ + j * h_dim1]) {
		r__[i__ + j * r_dim1] = max__;
		g[i__ + j * g_dim1] = min__;
		b[i__ + j * b_dim1] = (360.f - h__[i__ + j * h_dim1]) / 60.f *
			 (max__ - min__) + min__;
	    } else if (240.f <= h__[i__ + j * h_dim1]) {
		r__[i__ + j * r_dim1] = (h__[i__ + j * h_dim1] - 4.f) * (
			max__ - min__) + min__;
		g[i__ + j * g_dim1] = min__;
		b[i__ + j * b_dim1] = max__;
	    } else if (180.f <= h__[i__ + j * h_dim1]) {
		r__[i__ + j * r_dim1] = min__;
		g[i__ + j * g_dim1] = (240.f - h__[i__ + j * h_dim1]) / 60.f *
			 (max__ - min__) + min__;
		b[i__ + j * b_dim1] = max__;
	    } else if (120.f <= h__[i__ + j * h_dim1]) {
		r__[i__ + j * r_dim1] = min__;
		g[i__ + j * g_dim1] = max__;
		b[i__ + j * b_dim1] = (h__[i__ + j * h_dim1] - 120.f) / 60.f *
			 (max__ - min__) + min__;
	    } else if (60.f <= h__[i__ + j * h_dim1]) {
		r__[i__ + j * r_dim1] = (120.f - h__[i__ + j * h_dim1]) / 
			60.f * (max__ - min__) + min__;
		g[i__ + j * g_dim1] = max__;
		b[i__ + j * b_dim1] = min__;
	    } else {
		r__[i__ + j * r_dim1] = max__;
		g[i__ + j * g_dim1] = h__[i__ + j * h_dim1] / 60.f * (max__ - 
			min__) + min__;
		b[i__ + j * b_dim1] = min__;
	    }
/* L20: */
	}
/* L10: */
    }
    return 0;
} /* cllsrg_ */


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/* Subroutine */ int clsvrg_(real *h__, real *s, real *v, integer *r__, 
	integer *g, integer *b, integer *n, integer *m)
{
    /* System generated locals */
    integer r_dim1, r_offset, g_dim1, g_offset, b_dim1, b_offset, h_dim1, 
	    h_offset, s_dim1, s_offset, v_dim1, v_offset, i__1, i__2;

    /* Local variables */
    static real f;
    static integer i__, j;
    static real p, q, t;
    static integer hi;
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen);

    /* Parameter adjustments */
    b_dim1 = *n;
    b_offset = 1 + b_dim1;
    b -= b_offset;
    g_dim1 = *n;
    g_offset = 1 + g_dim1;
    g -= g_offset;
    r_dim1 = *n;
    r_offset = 1 + r_dim1;
    r__ -= r_offset;
    v_dim1 = *n;
    v_offset = 1 + v_dim1;
    v -= v_offset;
    s_dim1 = *n;
    s_offset = 1 + s_dim1;
    s -= s_offset;
    h_dim1 = *n;
    h_offset = 1 + h_dim1;
    h__ -= h_offset;

    /* Function Body */
    i__1 = *n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	i__2 = *m;
	for (j = 1; j <= i__2; ++j) {
	    if (s[i__ + j * s_dim1] == 0.f) {
		r__[i__ + j * r_dim1] = v[i__ + j * v_dim1];
		g[i__ + j * g_dim1] = v[i__ + j * v_dim1];
		b[i__ + j * b_dim1] = v[i__ + j * v_dim1];
	    } else {
		hi = h__[i__ + j * h_dim1] / 60.f;
		hi %= 6;
		f = h__[i__ + j * h_dim1] / 60.f - hi;
		p = v[i__ + j * v_dim1] * (1.f - s[i__ + j * s_dim1]);
		q = v[i__ + j * v_dim1] * (1.f - f * s[i__ + j * s_dim1]);
		t = v[i__ + j * v_dim1] * (1.f - (1.f - f) * s[i__ + j * 
			s_dim1]);
		if (hi == 0) {
		    r__[i__ + j * r_dim1] = v[i__ + j * v_dim1];
		    g[i__ + j * g_dim1] = t;
		    b[i__ + j * b_dim1] = p;
		} else if (hi == 1) {
		    r__[i__ + j * r_dim1] = q;
		    g[i__ + j * g_dim1] = v[i__ + j * v_dim1];
		    b[i__ + j * b_dim1] = p;
		} else if (hi == 2) {
		    r__[i__ + j * r_dim1] = p;
		    g[i__ + j * g_dim1] = v[i__ + j * v_dim1];
		    b[i__ + j * b_dim1] = t;
		} else if (hi == 3) {
		    r__[i__ + j * r_dim1] = p;
		    g[i__ + j * g_dim1] = q;
		    b[i__ + j * b_dim1] = v[i__ + j * v_dim1];
		} else if (hi == 4) {
		    r__[i__ + j * r_dim1] = t;
		    g[i__ + j * g_dim1] = p;
		    b[i__ + j * b_dim1] = v[i__ + j * v_dim1];
		} else if (hi == 5) {
		    r__[i__ + j * r_dim1] = v[i__ + j * v_dim1];
		    g[i__ + j * g_dim1] = p;
		    b[i__ + j * b_dim1] = q;
		} else {
		    msgdmp_("E", "CLSVRG", "CAN NOT CONVERT HSV TO RGB.", (
			    ftnlen)1, (ftnlen)6, (ftnlen)27);
		}
	    }
/* L20: */
	}
/* L10: */
    }
    return 0;
} /* clsvrg_ */


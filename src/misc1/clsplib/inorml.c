/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/* Subroutine */ int inorml_(integer *v, real *w, integer *n, integer *m, 
	real *x, real *y)
{
    /* System generated locals */
    integer v_dim1, v_offset, w_dim1, w_offset, i__1, i__2;

    /* Local variables */
    static integer i__, j;
    static real r__, min__, max__;

    /* Parameter adjustments */
    w_dim1 = *n;
    w_offset = 1 + w_dim1;
    w -= w_offset;
    v_dim1 = *n;
    v_offset = 1 + v_dim1;
    v -= v_offset;

    /* Function Body */
    max__ = 0.f;
    min__ = 0.f;
    i__1 = *n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	i__2 = *m;
	for (j = 1; j <= i__2; ++j) {
	    if ((real) v[i__ + j * v_dim1] >= max__) {
		max__ = (real) v[i__ + j * v_dim1];
	    }
	    if ((real) v[i__ + j * v_dim1] <= min__) {
		min__ = (real) v[i__ + j * v_dim1];
	    }
/* L20: */
	}
/* L10: */
    }
    if (max__ != min__) {
	r__ = (*y - *x) / (max__ - min__);
    }
    if (max__ != min__) {
	i__1 = *n;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    i__2 = *m;
	    for (j = 1; j <= i__2; ++j) {
		w[i__ + j * w_dim1] = *x + r__ * (v[i__ + j * v_dim1] - min__)
			;
/* L40: */
	    }
/* L30: */
	}
    } else if (max__ >= *y) {
	i__1 = *n;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    i__2 = *m;
	    for (j = 1; j <= i__2; ++j) {
		w[i__ + j * w_dim1] = *y;
/* L60: */
	    }
/* L50: */
	}
    } else if (min__ <= *x) {
	i__1 = *n;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    i__2 = *m;
	    for (j = 1; j <= i__2; ++j) {
		w[i__ + j * w_dim1] = *x;
/* L80: */
	    }
/* L70: */
	}
    } else {
	i__1 = *n;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    i__2 = *m;
	    for (j = 1; j <= i__2; ++j) {
		w[i__ + j * w_dim1] = (real) v[i__ + j * v_dim1];
/* L100: */
	    }
/* L90: */
	}
    }
    return 0;
} /* inorml_ */


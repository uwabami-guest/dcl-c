/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     GBYTES */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int gbytes_(integer *npack, integer *isam, integer *ibit, 
	integer *nbits, integer *nskip, integer *iter)
{
    /* Initialized data */

    static integer ncall = 0;

    /* System generated locals */
    integer i__1, i__2;

    /* Local variables */
    static integer i__, bpo, wpo, msk1, mask[32], shft, btmpl, rbits, btmpr;
    extern /* Subroutine */ int gliget_(char *, integer *, ftnlen);
    extern integer ishift_(integer *, integer *);
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen);
    static integer bitsep, nbitpw;

    /* Parameter adjustments */
    --isam;
    --npack;

    /* Function Body */
    if (ncall == 0) {
	gliget_("NBITSPW", &nbitpw, (ftnlen)7);
	if (nbitpw != 32) {
	    msgdmp_("E", "GBYTES", "NUMBER OF BITS PER ONE WORD IS INVALID /"
		    " CHECK BPERI IN THE PARAMETER STATEMENT OF GBYTES AND CH"
		    "ANGE IT CORRECTLY.", (ftnlen)1, (ftnlen)6, (ftnlen)114);
	}
	mask[0] = 1;
	for (i__ = 2; i__ <= 32; ++i__) {
	    mask[i__ - 1] = ishift_(&mask[i__ - 2], &c__1) | 1;
/* L10: */
	}
	ncall = 1;
    }
    if (! (*nbits <= 32 && *nbits > 0)) {
	msgdmp_("E", "GBYTES", "NBITS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)19);
    }
    msk1 = mask[*nbits - 1];
    bitsep = *nbits + *nskip;
    i__1 = *iter;
    for (i__ = 1; i__ <= i__1; ++i__) {
	bpo = *ibit + (i__ - 1) * bitsep;
	wpo = bpo / 32 + 1;
	rbits = (wpo << 5) - bpo;
	shft = *nbits - rbits;
	if (shft <= 0) {
	    isam[i__] = msk1 & ishift_(&npack[wpo], &shft);
	} else {
	    i__2 = npack[wpo] & mask[rbits - 1];
	    btmpl = ishift_(&i__2, &shft);
	    i__2 = shft - 32;
	    btmpr = ishift_(&npack[wpo + 1], &i__2) & mask[shft - 1];
	    isam[i__] = btmpl | btmpr;
	}
/* L20: */
    }
    return 0;
} /* gbytes_ */


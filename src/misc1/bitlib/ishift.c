#include "../../../config.h"
/*
 *	ishift
 */
#ifndef WINDOWS
DCL_INT ishift_(DCL_INT *i, DCL_INT *nshift)
#else
DCL_INT ISHIFT(DCL_INT *i, DCL_INT *nshift)
#endif
{
    DCL_INT jshift, nbits;

    if (*nshift < 0) {
	nbits = (*nshift < -32 ? 32 : -*nshift);
	jshift = (*i >> nbits) & (017777777777 >> (nbits - 1));
    }
    else {
	nbits = *nshift % 32;
	jshift = (*i << nbits) | ((*i >> (32 - nbits))
				  & (~(037777777777 << nbits)));
    }
    return (jshift);
}

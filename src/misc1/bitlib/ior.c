#include "../../../config.h"
/*
 *	ior
 */

#ifndef WINDOWS
DCL_INT ior_(DCL_INT *i, DCL_INT *j)
#else
DCL_INT IOR(DCL_INT *i, DCL_INT *j)
#endif
{
    return (*i | *j);
}

/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     SBYTES */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int sbytes_(integer *npack, integer *isam, integer *ibit, 
	integer *nbits, integer *nskip, integer *iter)
{
    /* Initialized data */

    static integer ncall = 0;

    /* System generated locals */
    integer i__1, i__2, i__3;

    /* Local variables */
    static integer i__, bpo, wpo, msk1, msk2, mask[32], btmp, rbits, sbits, 
	    ubits;
    extern /* Subroutine */ int gliget_(char *, integer *, ftnlen);
    extern integer ishift_(integer *, integer *);
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen);
    static integer bitsep, lshift, rshift, nbitpw;

    /* Parameter adjustments */
    --isam;
    --npack;

    /* Function Body */
    if (ncall == 0) {
	gliget_("NBITSPW", &nbitpw, (ftnlen)7);
	if (nbitpw != 32) {
	    msgdmp_("E", "SBYTES", "NUMBER OF BITS PER ONE WORD IS INVALID /"
		    " CHECK BPERI IN THE PARAMETER STATEMENT OF GBYTES AND CH"
		    "ANGE IT CORRECTLY.", (ftnlen)1, (ftnlen)6, (ftnlen)114);
	}
	mask[0] = 1;
	for (i__ = 2; i__ <= 32; ++i__) {
	    mask[i__ - 1] = ishift_(&mask[i__ - 2], &c__1) | 1;
/* L10: */
	}
	ncall = 1;
    }
    if (! (*nbits <= 32 && *nbits > 0)) {
	msgdmp_("E", "SBYTES", "NBITS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)19);
    }
    msk1 = mask[*nbits - 1];
    msk2 = mask[32 - *nbits - 1];
    bitsep = *nbits + *nskip;
    i__1 = *iter;
    for (i__ = 1; i__ <= i__1; ++i__) {
	sbits = msk1 & isam[i__];
	bpo = *ibit + (i__ - 1) * bitsep;
	wpo = bpo / 32 + 1;
	rbits = (wpo << 5) - bpo;
	ubits = 32 - rbits;
	if (rbits >= *nbits) {
	    lshift = 32 - ubits - *nbits;
	    i__2 = lshift + *nbits;
	    npack[wpo] &= ishift_(&msk2, &i__2);
	    npack[wpo] |= ishift_(&sbits, &lshift);
	} else {
	    rshift = rbits - *nbits;
	    btmp = ishift_(&sbits, &rshift);
	    npack[wpo] &= ishift_(&mask[ubits - 1], &rbits);
	    npack[wpo] |= btmp;
	    i__2 = mask[-rshift - 1] & sbits;
	    i__3 = rshift + 32;
	    btmp = ishift_(&i__2, &i__3);
	    npack[wpo + 1] &= mask[rshift + 31];
	    npack[wpo + 1] |= btmp;
	}
/* L20: */
    }
    return 0;
} /* sbytes_ */


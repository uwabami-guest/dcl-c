/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     BITPCI */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int bitpci_(char *cp, integer *ip, ftnlen cp_len)
{
    /* Initialized data */

    static logical lfst = TRUE_;

    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    integer i_len(char *, ftnlen);

    /* Local variables */
    static integer i__, ii, nbc, mask[32];
    extern /* Subroutine */ int gliget_(char *, integer *, ftnlen);
    extern integer ishift_(integer *, integer *);
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen);
    static integer nbitpw;

    if (lfst) {
	gliget_("NBITSPW", &nbitpw, (ftnlen)7);
	if (nbitpw != 32) {
	    msgdmp_("E", "BITPCI", "NUMBER OF BITS PER ONE WORD IS INVALID /"
		    " CHECK NB IN THE PARAMETER STATEMENT OF BITPCI AND CHANG"
		    "E IT CORRECTLY.", (ftnlen)1, (ftnlen)6, (ftnlen)111);
	}
	mask[0] = 1;
	for (i__ = 2; i__ <= 32; ++i__) {
	    mask[i__ - 1] = ishift_(&mask[i__ - 2], &c__1);
/* L10: */
	}
	lfst = FALSE_;
    }
    nbc = i_len(cp, cp_len);
    *ip = 0;
    i__1 = min(nbc,32);
    for (i__ = 1; i__ <= i__1; ++i__) {
	ii = nbc - i__ + 1;
	if (*(unsigned char *)&cp[ii - 1] != '0') {
	    *ip += mask[i__ - 1];
	}
/* L15: */
    }
    return 0;
} /* bitpci_ */


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     TIMEC1 */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int timec1_(char *cform, integer *itime, ftnlen cform_len)
{
    static integer ih, im, is;
    extern /* Subroutine */ int time13_(integer *, integer *, integer *, 
	    integer *), timec3_(char *, integer *, integer *, integer *, 
	    ftnlen);

    time13_(itime, &ih, &im, &is);
    timec3_(cform, &ih, &im, &is, cform_len);
    return 0;
} /* timec1_ */


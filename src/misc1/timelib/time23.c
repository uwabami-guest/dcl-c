/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     TIME23 */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int time23_(integer *ih, integer *im, integer *is, integer *
	itt)
{
/*     IH   : HOUR                                               (I/ ) */
/*     IM   : MINUTE                                             (I/ ) */
/*     IS   : SECOND                                             (I/ ) */
/*     ITT  : TOTAL TIME                                         ( /O) */
    *ih = *itt / 3600;
    *im = (*itt - *ih * 3600) / 60;
    *is = *itt - *im * 60 - *ih * 3600;
    return 0;
} /* time23_ */


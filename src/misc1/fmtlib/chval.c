/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static real c_b11 = 360.f;
static integer c__1 = 1;
static real c_b18 = 10.f;
static integer c__2 = 2;

/* ----------------------------------------------------------------------- */
/*     CHVAL */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int chval_(char *cfmt, real *val, char *cval, ftnlen 
	cfmt_len, ftnlen cval_len)
{
    /* Initialized data */

    static logical lfrst = TRUE_;

    /* System generated locals */
    address a__1[2];
    integer i__1, i__2[2];
    real r__1;

    /* Builtin functions */
    integer i_len(char *, ftnlen);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_wsfi(icilist *), do_fio(integer *, char *, ftnlen), e_wsfi(void)
	    , s_rsfi(icilist *), e_rsfi(void);
    double pow_ri(real *, integer *);
    integer s_cmp(char *, char *, ftnlen, ftnlen), i_nint(real *);
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen);

    /* Local variables */
    static char cf[1];
    static integer ie, mc, nc;
    static real rb;
    static integer nz, id1, ide, idp;
    static logical lat;
    static char chx[16];
    static logical lon;
    extern integer lenc_(char *, ftnlen);
    extern real rmod_(real *, real *);
    static char copt[3];
    static real valx, valz;
    extern /* Subroutine */ int cladj_(char *, ftnlen);
    static integer idxdc;
    static char cfmtx[16];
    extern integer indxcf_(char *, integer *, integer *, char *, ftnlen, 
	    ftnlen);
    extern logical lchreq_(char *, char *, ftnlen, ftnlen);
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), clower_(char *, ftnlen), cupper_(char *, ftnlen);

    /* Fortran I/O blocks */
    static icilist io___10 = { 0, chx, 0, cfmtx, 16, 1 };
    static icilist io___11 = { 0, chx+6, 0, "(I3)", 3, 1 };
    static icilist io___13 = { 0, chx, 0, "(F5.2)", 5, 1 };
    static icilist io___16 = { 0, cfmtx+4, 0, "(I1)", 1, 1 };
    static icilist io___18 = { 0, chx, 0, cfmtx, 16, 1 };
    static icilist io___19 = { 0, chx, 0, cfmtx, 16, 1 };


/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

/*     THIS ROUTINE RETURNS CHARACTERIZED VALUE "CVAL" OF "VAL" USING */
/*     USER SPECIFIED FORMAT "CFMT". IF ONE OF THE FOLLOWING OPTINONS IS */
/*     SPECIFIED AS "CFMT", FORMAT WILL BE GENERATED AUTOMATICALLY TO */
/*     REPRESENT 3 SIGNIFICANT DIGITS. */

/*     CFMT   (C*(*)) : FORMAT OR OPTION NAME (I/ ). */
/*                    : FORMAT SHOULD BEGIN WITH '('. */
/*                    : ONE OF THE FOLLOWING OPTIONS CAN BE SPECIFIED. */
/*                    : 'A' - FORMAT IS SET AUTOMATICALLY. */
/*                    : 'B' - 'A' AND TRAILING ZERO AND DECIMAL POINT ARE */
/*                    :       DELETED. */
/*                    : 'C' - 'B' AND ZERO BEFORE DECIMAL POINT AND '+' */
/*                    :       ARE DELETED. */
/*                    : 'D' - 'C' BUT ONLY FOR THE EXPONENT TYPE. */
/*     VAL    (R)     : NUMERIC VALUE THAT SHOULD BE CHARACTERIZED (I/ ). */
/*     CVAL   (C*(*)) : CHARACTERIZED VALUE OF "VAL" ( /O). */
/*                    : LEN(CVAL) SHOULD BE 8 OR MORE. */

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
/*     / CHECK LENGTH OF OUTPUT CHARACTER / */
    if (i_len(cval, cval_len) < 8 && lfrst) {
	msgdmp_("W", "CHVAL ", "LENGTH OF CHARACTER IS LESS THAN 8.", (ftnlen)
		1, (ftnlen)6, (ftnlen)35);
	lfrst = FALSE_;
    }
/*     / CHECK MAIN OPTION / */
    *(unsigned char *)cf = *(unsigned char *)cfmt;
    cupper_(cf, (ftnlen)1);
/*     / CHECK SUB OPTION / */
    nc = lenc_(cfmt, cfmt_len);
    lon = FALSE_;
    lat = FALSE_;
    if (nc >= 3) {
	if (*(unsigned char *)&cfmt[1] == '+') {
	    s_copy(copt, cfmt + 2, (ftnlen)3, nc - 2);
	    if (lchreq_(copt, "X", (ftnlen)3, (ftnlen)1) || lchreq_(copt, 
		    "LON", (ftnlen)3, (ftnlen)3)) {
		lon = TRUE_;
	    } else if (lchreq_(copt, "Y", (ftnlen)3, (ftnlen)1) || lchreq_(
		    copt, "LAT", (ftnlen)3, (ftnlen)3)) {
		lat = TRUE_;
	    }
	}
    }
    if (*(unsigned char *)cf != '(') {
/*       / LONGITUDE (X) OR LATITUDE (Y) OPTION / */
	if (lon) {
	    r__1 = *val + 180;
	    valz = rmod_(&r__1, &c_b11) - 180;
	} else if (lat) {
	    valz = *val;
	} else {
	    valz = *val;
	}
/*       / AUTOMATIC GENERATION (NOT USER FORMAT) / */
/*       / PICK UP 3 SIGNIFICANT DIGITS AND EXPONENT / */
	s_copy(cfmtx, "(1P,E9.2E2)", (ftnlen)16, (ftnlen)11);
	s_wsfi(&io___10);
	do_fio(&c__1, (char *)&valz, (ftnlen)sizeof(real));
	e_wsfi();
	s_rsfi(&io___11);
	do_fio(&c__1, (char *)&ie, (ftnlen)sizeof(integer));
	e_rsfi();
	s_rsfi(&io___13);
	do_fio(&c__1, (char *)&rb, (ftnlen)sizeof(real));
	e_rsfi();
	valx = rb * pow_ri(&c_b18, &ie);
/*       / FORMAT / */
	if (0 <= ie && ie <= 2) {
/*         / DECIMAL / */
	    s_copy(cfmtx, "(F6. )", (ftnlen)16, (ftnlen)6);
	    s_wsfi(&io___16);
	    i__1 = 2 - ie;
	    do_fio(&c__1, (char *)&i__1, (ftnlen)sizeof(integer));
	    e_wsfi();
	} else if (3 <= ie && ie <= 4) {
/*         / INTEGER / */
	    s_copy(cfmtx, "(I6)", (ftnlen)16, (ftnlen)4);
	} else if (-3 <= ie && ie <= -1) {
/*         / DECIMAL OR EXPONENT / */
/*         / COUNT TRAILING ZERO / */
	    nz = 0;
L10:
	    i__1 = 5 - nz - 1;
	    if (! (s_cmp(chx + i__1, "0", 5 - nz - i__1, (ftnlen)1) == 0)) {
		goto L15;
	    }
	    ++nz;
	    goto L10;
L15:
/*         / IF -IE .LE. TRAILING ZERO +1 THEN DECIMAL ELSE EXPONENT / */
	    if (-ie <= nz) {
		s_copy(cfmtx, "(F6.2)", (ftnlen)16, (ftnlen)6);
	    } else if (-ie <= nz + 1) {
		s_copy(cfmtx, "(F6.3)", (ftnlen)16, (ftnlen)6);
	    } else {
		s_copy(cfmtx, "(1P,E8.2E1)", (ftnlen)16, (ftnlen)11);
	    }
	} else if (-9 <= ie && ie <= 9) {
/*         / EXPONENT ( SIGNIFICANT DIGITS = 3 ) / */
	    s_copy(cfmtx, "(1P,E8.2E1)", (ftnlen)16, (ftnlen)11);
	} else {
/*         / EXPONENT ( SIGNIFICANT DIGITS = 2 ) / */
	    s_copy(cfmtx, "(1P,E8.1E2)", (ftnlen)16, (ftnlen)11);
	}
    } else {
/*       / USER FORMAT / */
	s_copy(cfmtx, cfmt, (ftnlen)16, cfmt_len);
	valx = *val;
    }
/*     / ENCODING / */
    s_copy(chx, " ", (ftnlen)16, (ftnlen)1);
    if (lchreq_(cfmtx + 1, "I", (ftnlen)1, (ftnlen)1)) {
	s_wsfi(&io___18);
	i__1 = i_nint(&valx);
	do_fio(&c__1, (char *)&i__1, (ftnlen)sizeof(integer));
	e_wsfi();
    } else {
	s_wsfi(&io___19);
	do_fio(&c__1, (char *)&valx, (ftnlen)sizeof(real));
	e_wsfi();
    }
/*     / CHECK BLANK BEFORE DECIMAL POINT (SYSTEM DEPENDENT) / */
    i__1 = lenc_(chx, (ftnlen)16);
    idxdc = indxcf_(chx, &i__1, &c__1, ".", (ftnlen)16, (ftnlen)1);
    if (idxdc != 0) {
	id1 = idxdc - 1;
	if (*(unsigned char *)&chx[id1 - 1] == ' ') {
	    *(unsigned char *)&chx[id1 - 1] = '0';
	} else if (*(unsigned char *)&chx[id1 - 1] == '-') {
	    i__1 = id1 - 2;
	    s_copy(chx + i__1, "-0", id1 - i__1, (ftnlen)2);
	}
    }
/*     / LEFT ADJUST / */
    cladj_(chx, (ftnlen)16);
    nc = lenc_(chx, (ftnlen)16);
    if (*(unsigned char *)chx == '+') {
	s_copy(cval, chx + 1, nc - 1, nc - 1);
	--nc;
	s_copy(chx, cval, (ftnlen)16, nc);
    }
/*     / OPTION / */
    if ((*(unsigned char *)cf == 'B' || *(unsigned char *)cf == 'C') && 
	    indxcf_(chx, &nc, &c__1, ".", (ftnlen)16, (ftnlen)1) != 0 || *(
	    unsigned char *)cf == 'D' && indxcf_(chx, &nc, &c__1, "E", (
	    ftnlen)16, (ftnlen)1) != 0) {
/*       / DELETE TRAILING ZERO AND DECIMAL POINT / */
/*       / CHECK EXPONENT OR DECIMAL / */
	ide = indxcf_(chx, &nc, &c__1, "E", (ftnlen)16, (ftnlen)1);
	if (ide == 0) {
/*         / DECIMAL / */
	    mc = nc;
	} else {
/*         / EXPONENT / */
	    mc = ide - 1;
	}
/*       / COUNT TRAILING ZERO / */
L25:
	if (! (*(unsigned char *)&chx[mc - 1] == '0')) {
	    goto L20;
	}
	--mc;
	goto L25;
L20:
/*       / CHECK DECIMAL POINT / */
	if (*(unsigned char *)&chx[mc - 1] == '.') {
	    --mc;
	}
/*       / AVAILABLE LENGTH / */
	if (ide == 0) {
	    nc = mc;
	} else {
/* Writing concatenation */
	    i__2[0] = mc, a__1[0] = chx;
	    i__2[1] = nc - (ide - 1), a__1[1] = chx + (ide - 1);
	    s_cat(cval, a__1, i__2, &c__2, cval_len);
	    nc = mc + nc - ide + 1;
	    s_copy(chx, cval, nc, nc);
	}
/*       / 'C' & 'D' OPTION / */
	if ((*(unsigned char *)cf == 'C' || *(unsigned char *)cf == 'D') && 
		nc > 1) {
/*         / DELETE ZERO BEFORE DECIMAL POINT / */
	    if (*(unsigned char *)chx == '0') {
		s_copy(cval, chx + 1, cval_len, nc - 1);
		--nc;
		s_copy(chx, cval, nc, nc);
	    } else if (*(unsigned char *)chx == '-' && *(unsigned char *)&chx[
		    1] == '0') {
/* Writing concatenation */
		i__2[0] = 1, a__1[0] = chx;
		i__2[1] = nc - 2, a__1[1] = chx + 2;
		s_cat(cval, a__1, i__2, &c__2, cval_len);
		--nc;
		s_copy(chx, cval, nc, nc);
	    }
/*         / DELETE '+' IN EXPONENT PART / */
	    idp = indxcf_(chx, &nc, &c__1, "+", (ftnlen)16, (ftnlen)1);
	    if (idp != 0) {
		i__1 = idp;
/* Writing concatenation */
		i__2[0] = idp - 1, a__1[0] = chx;
		i__2[1] = nc - i__1, a__1[1] = chx + i__1;
		s_cat(cval, a__1, i__2, &c__2, nc - 1);
		--nc;
		s_copy(chx, cval, nc, nc);
	    }
	}
    }
/*     / RETURN CHARACTER / */
    if (*(unsigned char *)cf != '(') {
	if (lon) {
	    if (valz == 0.f || valz == -180.f) {
		if (valz == 0.f) {
		    s_copy(cval, chx, cval_len, nc);
		} else if (valz == -180.f) {
		    s_copy(cval, chx + 1, cval_len, nc - 1);
		}
	    } else {
		if (*(unsigned char *)chx == '-') {
/* Writing concatenation */
		    i__2[0] = nc - 1, a__1[0] = chx + 1;
		    i__2[1] = 1, a__1[1] = "W";
		    s_cat(cval, a__1, i__2, &c__2, cval_len);
		} else {
/* Writing concatenation */
		    i__2[0] = nc, a__1[0] = chx;
		    i__2[1] = 1, a__1[1] = "E";
		    s_cat(cval, a__1, i__2, &c__2, cval_len);
		}
	    }
	} else if (lat) {
	    if (valz == 0.f) {
/*           CVAL=CHX(1:NC) */
		s_copy(cval, "EQ", cval_len, (ftnlen)2);
	    } else {
		if (*(unsigned char *)chx == '-') {
/* Writing concatenation */
		    i__2[0] = nc - 1, a__1[0] = chx + 1;
		    i__2[1] = 1, a__1[1] = "S";
		    s_cat(cval, a__1, i__2, &c__2, cval_len);
		} else {
/* Writing concatenation */
		    i__2[0] = nc, a__1[0] = chx;
		    i__2[1] = 1, a__1[1] = "N";
		    s_cat(cval, a__1, i__2, &c__2, cval_len);
		}
	    }
	} else {
	    s_copy(cval, chx, cval_len, nc);
	    clower_(cval, cval_len);
	}
    } else {
	s_copy(cval, chx, cval_len, nc);
	clower_(cval, cval_len);
    }
    return 0;
} /* chval_ */


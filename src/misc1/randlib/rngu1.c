/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     RANDOM NUMBER GENERATOR (LINEAR CONGRUENTIAL METHOD) */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
real rngu1_(integer *iseed)
{
    /* Initialized data */

    static logical lfirst = TRUE_;

    /* System generated locals */
    real ret_val;

    /* Local variables */
    static integer jseed;
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen);

    if (*iseed != 0) {
	jseed = abs(*iseed);
	*iseed = 0;
	lfirst = FALSE_;
    }
    if (lfirst) {
	msgdmp_("E", "RNGU1", "ISEED MUST BE > 0 FOR 1ST CALL.", (ftnlen)1, (
		ftnlen)5, (ftnlen)31);
    }
    jseed = (jseed * 7141 + 54773) % 259200;
    ret_val = (real) jseed * 3.8580246913580248e-6f;
    return ret_val;
} /* rngu1_ */


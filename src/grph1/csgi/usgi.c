/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__2 = 2;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Character */ VOID usgi_(char *ret_val, ftnlen ret_val_len, integer *ic)
{
    /* System generated locals */
    address a__1[2];
    integer i__1[2];
    char ch__2[1];

    /* Builtin functions */
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen);

/* Writing concatenation */
    i__1[0] = 2, a__1[0] = "\\x";
    *(unsigned char *)&ch__2[0] = *ic;
    i__1[1] = 1, a__1[1] = ch__2;
    s_cat(ret_val, a__1, i__1, &c__2, (ftnlen)3);
} /* usgi_ */


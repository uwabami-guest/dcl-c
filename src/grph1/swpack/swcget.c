/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int swcget_0_(int n__, char *cp, char *cpara, ftnlen cp_len, 
	ftnlen cpara_len)
{
    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static char cx[40];
    static integer idx;
    static char cpval[80];
    extern /* Subroutine */ int rlcget_(char *, char *, integer *, ftnlen, 
	    ftnlen), rtcget_(char *, char *, char *, integer *, ftnlen, 
	    ftnlen, ftnlen), swcqid_(char *, integer *, ftnlen), swcqcl_(
	    integer *, char *, ftnlen), swcqcp_(integer *, char *, ftnlen), 
	    swcqvl_(integer *, char *, ftnlen), swcsvl_(integer *, char *, 
	    ftnlen);

    switch(n__) {
	case 1: goto L_swcset;
	case 2: goto L_swcstx;
	}

    swcqid_(cp, &idx, cp_len);
    swcqvl_(&idx, cpara, cpara_len);
    return 0;
/* ----------------------------------------------------------------------- */

L_swcset:
    swcqid_(cp, &idx, cp_len);
    swcsvl_(&idx, cpara, cpara_len);
    return 0;
/* ----------------------------------------------------------------------- */

L_swcstx:
    s_copy(cpval, cpara, (ftnlen)80, cpara_len);
    swcqid_(cp, &idx, cp_len);
    swcqcp_(&idx, cx, (ftnlen)40);
    rtcget_("SW", cx, cpval, &c__1, (ftnlen)2, (ftnlen)40, (ftnlen)80);
    swcqcl_(&idx, cx, (ftnlen)40);
    rlcget_(cx, cpval, &c__1, (ftnlen)40, (ftnlen)80);
    swcsvl_(&idx, cpval, (ftnlen)80);
    return 0;
} /* swcget_ */

/* Subroutine */ int swcget_(char *cp, char *cpara, ftnlen cp_len, ftnlen 
	cpara_len)
{
    return swcget_0_(0, cp, cpara, cp_len, cpara_len);
    }

/* Subroutine */ int swcset_(char *cp, char *cpara, ftnlen cp_len, ftnlen 
	cpara_len)
{
    return swcget_0_(1, cp, cpara, cp_len, cpara_len);
    }

/* Subroutine */ int swcstx_(char *cp, char *cpara, ftnlen cp_len, ftnlen 
	cpara_len)
{
    return swcget_0_(2, cp, cpara, cp_len, cpara_len);
    }


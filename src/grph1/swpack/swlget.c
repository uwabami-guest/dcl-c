/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     SWLGET / SWLSET / SWLSTX */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int swlget_0_(int n__, char *cp, logical *lpara, ftnlen 
	cp_len)
{
    static char cl[40], cx[8];
    static logical lp;
    static integer idx;
    extern /* Subroutine */ int rllget_(char *, logical *, integer *, ftnlen),
	     rtlget_(char *, char *, logical *, integer *, ftnlen, ftnlen), 
	    swlqid_(char *, integer *, ftnlen), swlqcl_(integer *, char *, 
	    ftnlen), swlqcp_(integer *, char *, ftnlen), swlqvl_(integer *, 
	    logical *), swlsvl_(integer *, logical *);

    switch(n__) {
	case 1: goto L_swlset;
	case 2: goto L_swlstx;
	}

    swlqid_(cp, &idx, cp_len);
    swlqvl_(&idx, lpara);
    return 0;
/* ----------------------------------------------------------------------- */

L_swlset:
    swlqid_(cp, &idx, cp_len);
    swlsvl_(&idx, lpara);
    return 0;
/* ----------------------------------------------------------------------- */

L_swlstx:
    lp = *lpara;
    swlqid_(cp, &idx, cp_len);
/*     / SHORT NAME / */
    swlqcp_(&idx, cx, (ftnlen)8);
    rtlget_("SW", cx, &lp, &c__1, (ftnlen)2, (ftnlen)8);
/*     / LONG NAME / */
    swlqcl_(&idx, cl, (ftnlen)40);
    rllget_(cl, &lp, &c__1, (ftnlen)40);
    swlsvl_(&idx, &lp);
    return 0;
} /* swlget_ */

/* Subroutine */ int swlget_(char *cp, logical *lpara, ftnlen cp_len)
{
    return swlget_0_(0, cp, lpara, cp_len);
    }

/* Subroutine */ int swlset_(char *cp, logical *lpara, ftnlen cp_len)
{
    return swlget_0_(1, cp, lpara, cp_len);
    }

/* Subroutine */ int swlstx_(char *cp, logical *lpara, ftnlen cp_len)
{
    return swlget_0_(2, cp, lpara, cp_len);
    }


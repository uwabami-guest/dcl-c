/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__3 = 3;
static integer c__14 = 14;

/* ----------------------------------------------------------------------- */
/*     SWLQNP / SWLQID / SWLQCP / SWLQVL / SWLSVL */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int swlqnp_0_(int n__, integer *ncp, char *cp, integer *idx, 
	logical *lpara, integer *in, ftnlen cp_len)
{
    /* Initialized data */

    static char cparas[8*14] = "LWAIT   " "LWAIT0  " "LWAIT1  " "LKEY    " 
	    "LDUMP   " "LALT    " "LCOLOR  " "LSEP    " "LPRINT  " "LWND    " 
	    "LFGBG   " "LCMCH   " "LSTDOT  " "LSYSFNT ";
    static logical lx[14] = { TRUE_,FALSE_,TRUE_,TRUE_,FALSE_,FALSE_,TRUE_,
	    FALSE_,FALSE_,TRUE_,FALSE_,TRUE_,FALSE_,FALSE_ };
    static char cparal[40*14] = "WAIT                                    " 
	    "WAIT_OPENING                            " "WAIT_CLOSING        "
	    "                    " "KEYCLICK                                " 
	    "DUMP                                    " "ALTERNATE           "
	    "                    " "ENABLE_COLOR_PS                         " 
	    "SEPARATE                                " "PRINT               "
	    "                    " "SHOW_WINDOW                             " 
	    "****LFGBG                               " "CMAP_CHANGABLE      "
	    "                    " "PNG_STDOUT                              " 
	    "USE_SYSTEM_FONT                         ";
    static logical lfirst = TRUE_;

    /* System generated locals */
    address a__1[3];
    integer i__1[3];

    /* Builtin functions */
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen),
	     s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer n;
    extern integer lenc_(char *, ftnlen);
    static char cmsg[80];
    extern logical lchreq_(char *, char *, ftnlen, ftnlen);
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), rllget_(char *, logical *, integer *, ftnlen), 
	    rtlget_(char *, char *, logical *, integer *, ftnlen, ftnlen);

/* ---- SHORT NAME ---- */
    switch(n__) {
	case 1: goto L_swlqid;
	case 2: goto L_swlqcp;
	case 3: goto L_swlqcl;
	case 4: goto L_swlqvl;
	case 5: goto L_swlsvl;
	case 6: goto L_swlqin;
	}

/* ---- LONG NAME ---- */
    *ncp = 14;
    return 0;
/* ----------------------------------------------------------------------- */

L_swlqid:
    for (n = 1; n <= 14; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *idx = n;
	    return 0;
	}
/* L10: */
    }
/* Writing concatenation */
    i__1[0] = 11, a__1[0] = "PARAMETER '";
    i__1[1] = lenc_(cp, cp_len), a__1[1] = cp;
    i__1[2] = 17, a__1[2] = "' IS NOT DEFINED.";
    s_cat(cmsg, a__1, i__1, &c__3, (ftnlen)80);
    msgdmp_("E", "SWLQID", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
    return 0;
/* ----------------------------------------------------------------------- */

L_swlqcp:
    if (1 <= *idx && *idx <= 14) {
	s_copy(cp, cparas + (*idx - 1 << 3), cp_len, (ftnlen)8);
    } else {
	msgdmp_("E", "SWLQCP", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_swlqcl:
    if (1 <= *idx && *idx <= 14) {
	s_copy(cp, cparal + (*idx - 1) * 40, cp_len, (ftnlen)40);
    } else {
	msgdmp_("E", "SWLQCL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_swlqvl:
    if (lfirst) {
	rtlget_("SW", cparas, lx, &c__14, (ftnlen)2, (ftnlen)8);
	rllget_(cparal, lx, &c__14, (ftnlen)40);
	lfirst = FALSE_;
    }
    if (1 <= *idx && *idx <= 14) {
	*lpara = lx[*idx - 1];
    } else {
	msgdmp_("E", "SWLQVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_swlsvl:
    if (lfirst) {
	rtlget_("SW", cparas, lx, &c__14, (ftnlen)2, (ftnlen)8);
	rllget_(cparal, lx, &c__14, (ftnlen)40);
	lfirst = FALSE_;
    }
    if (1 <= *idx && *idx <= 14) {
	lx[*idx - 1] = *lpara;
    } else {
	msgdmp_("E", "SWLSVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_swlqin:
    for (n = 1; n <= 14; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *in = n;
	    return 0;
	}
/* L20: */
    }
    *in = 0;
    return 0;
} /* swlqnp_ */

/* Subroutine */ int swlqnp_(integer *ncp)
{
    return swlqnp_0_(0, ncp, (char *)0, (integer *)0, (logical *)0, (integer *
	    )0, (ftnint)0);
    }

/* Subroutine */ int swlqid_(char *cp, integer *idx, ftnlen cp_len)
{
    return swlqnp_0_(1, (integer *)0, cp, idx, (logical *)0, (integer *)0, 
	    cp_len);
    }

/* Subroutine */ int swlqcp_(integer *idx, char *cp, ftnlen cp_len)
{
    return swlqnp_0_(2, (integer *)0, cp, idx, (logical *)0, (integer *)0, 
	    cp_len);
    }

/* Subroutine */ int swlqcl_(integer *idx, char *cp, ftnlen cp_len)
{
    return swlqnp_0_(3, (integer *)0, cp, idx, (logical *)0, (integer *)0, 
	    cp_len);
    }

/* Subroutine */ int swlqvl_(integer *idx, logical *lpara)
{
    return swlqnp_0_(4, (integer *)0, (char *)0, idx, lpara, (integer *)0, (
	    ftnint)0);
    }

/* Subroutine */ int swlsvl_(integer *idx, logical *lpara)
{
    return swlqnp_0_(5, (integer *)0, (char *)0, idx, lpara, (integer *)0, (
	    ftnint)0);
    }

/* Subroutine */ int swlqin_(char *cp, integer *in, ftnlen cp_len)
{
    return swlqnp_0_(6, (integer *)0, cp, (integer *)0, (logical *)0, in, 
	    cp_len);
    }


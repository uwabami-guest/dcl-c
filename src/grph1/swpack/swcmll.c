/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__3 = 3;
static integer c__1 = 1;
static integer c__9 = 9;

/* ----------------------------------------------------------------------- */
/*     COLORMAP LIST LOADING */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int swcmll_0_(int n__, integer *nn, integer *ntx, char *ctf, 
	char *ctd, ftnlen ctf_len, ftnlen ctd_len)
{
    /* Initialized data */

    static logical lfirst = TRUE_;

    /* System generated locals */
    integer i__1;
    olist o__1;
    cllist cl__1;
    alist al__1;

    /* Builtin functions */
    integer s_cmp(char *, char *, ftnlen, ftnlen), f_open(olist *), f_rew(
	    alist *), s_rsle(cilist *), do_lio(integer *, integer *, char *, 
	    ftnlen), e_rsle(void), f_clos(cllist *);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer n, nc, iu, ios;
    static char ctr1[80*99], ctr2[80*99], cmsg[80], cfile[1024];
    extern /* Subroutine */ int chngi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen);
    static integer nnmax;
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen);
    extern integer iufopn_(void);
    extern /* Subroutine */ int swqfnm_(char *, char *, ftnlen, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___5 = { 1, 0, 1, 0, 0 };
    static cilist io___8 = { 1, 0, 1, 0, 0 };


    switch(n__) {
	case 1: goto L_swqcmn;
	case 2: goto L_swqcmf;
	case 3: goto L_swqcmd;
	}

    if (! lfirst) {
	return 0;
    }
/*     / OPEN LIST FILE : MAYBE SYSTEM DEPENDENT / */
    swqfnm_("CMAPLIST", cfile, (ftnlen)8, (ftnlen)1024);
    if (s_cmp(cfile, " ", (ftnlen)1024, (ftnlen)1) == 0) {
	msgdmp_("E", "SWCMLL", "COLORMAP LIST DOES NOT EXIST.", (ftnlen)1, (
		ftnlen)6, (ftnlen)29);
    }
    iu = iufopn_();
    o__1.oerr = 0;
    o__1.ounit = iu;
    o__1.ofnmlen = 1024;
    o__1.ofnm = cfile;
    o__1.orl = 0;
    o__1.osta = 0;
    o__1.oacc = 0;
    o__1.ofm = "FORMATTED";
    o__1.oblnk = 0;
    f_open(&o__1);
    al__1.aerr = 0;
    al__1.aunit = iu;
    f_rew(&al__1);
/*     / LOAD COLORMAP LIST / */
    io___5.ciunit = iu;
    ios = s_rsle(&io___5);
    if (ios != 0) {
	goto L100001;
    }
    ios = do_lio(&c__3, &c__1, (char *)&nc, (ftnlen)sizeof(integer));
    if (ios != 0) {
	goto L100001;
    }
    ios = e_rsle();
L100001:
    if (ios != 0) {
	msgdmp_("E", "SWCMLL", "COLORMAP LIST IS INVALID", (ftnlen)1, (ftnlen)
		6, (ftnlen)24);
    }
    if (nc > 99) {
	msgdmp_("E", "SWCMLL", "COLORMAP LIST OVERFLOWS", (ftnlen)1, (ftnlen)
		6, (ftnlen)23);
    }
    i__1 = nc;
    for (n = 1; n <= i__1; ++n) {
	io___8.ciunit = iu;
	ios = s_rsle(&io___8);
	if (ios != 0) {
	    goto L100002;
	}
	ios = do_lio(&c__9, &c__1, ctr1 + (n - 1) * 80, (ftnlen)80);
	if (ios != 0) {
	    goto L100002;
	}
	ios = do_lio(&c__9, &c__1, ctr2 + (n - 1) * 80, (ftnlen)80);
	if (ios != 0) {
	    goto L100002;
	}
	ios = e_rsle();
L100002:
	if (ios != 0) {
	    msgdmp_("E", "SWCMLL", "COLORMAP LIST IS INVALID", (ftnlen)1, (
		    ftnlen)6, (ftnlen)24);
	}
/* L10: */
    }
    nnmax = nc;
    lfirst = FALSE_;
/*     / CLOSE TABLE FILE / */
    cl__1.cerr = 0;
    cl__1.cunit = iu;
    cl__1.csta = 0;
    f_clos(&cl__1);
    return 0;
/* ----------------------------------------------------------------------- */

L_swqcmn:
    if (lfirst) {
	msgdmp_("E", "SWQCMN", "COLORMAP LIST FILE SHOULD BE READ FIRST USIN"
		"G \"SWCMLL\"", (ftnlen)1, (ftnlen)6, (ftnlen)54);
    }
    *nn = nnmax;
    return 0;
/* ----------------------------------------------------------------------- */

L_swqcmf:
    if (lfirst) {
	msgdmp_("E", "SWQCMF", "COLORMAP LIST FILE SHOULD BE READ FIRST USIN"
		"G \"SWCMLL\"", (ftnlen)1, (ftnlen)6, (ftnlen)54);
    }
    if (1 <= *ntx && *ntx <= nnmax) {
	s_copy(ctf, ctr1 + (*ntx - 1) * 80, ctf_len, (ftnlen)80);
    } else {
	s_copy(cmsg, "COLORMAP NUMBER <##> IS OUT OF RANGE.", (ftnlen)80, (
		ftnlen)37);
	chngi_(cmsg, "##", ntx, "(I2)", (ftnlen)80, (ftnlen)2, (ftnlen)4);
	msgdmp_("E", "SWQCMF", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_swqcmd:
    if (lfirst) {
	msgdmp_("E", "SWQCMD", "COLORMAP LIST FILE SHOULD BE READ FIRST USIN"
		"G \"SWCMLL\"", (ftnlen)1, (ftnlen)6, (ftnlen)54);
    }
    if (1 <= *ntx && *ntx <= nnmax) {
	s_copy(ctd, ctr2 + (*ntx - 1) * 80, ctd_len, (ftnlen)80);
    } else {
	s_copy(cmsg, "COLORMAP NUMBER <##> IS OUT OF RANGE.", (ftnlen)80, (
		ftnlen)37);
	chngi_(cmsg, "##", ntx, "(I2)", (ftnlen)80, (ftnlen)2, (ftnlen)4);
	msgdmp_("E", "SWQCMD", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
    }
    return 0;
} /* swcmll_ */

/* Subroutine */ int swcmll_(void)
{
    return swcmll_0_(0, (integer *)0, (integer *)0, (char *)0, (char *)0, (
	    ftnint)0, (ftnint)0);
    }

/* Subroutine */ int swqcmn_(integer *nn)
{
    return swcmll_0_(1, nn, (integer *)0, (char *)0, (char *)0, (ftnint)0, (
	    ftnint)0);
    }

/* Subroutine */ int swqcmf_(integer *ntx, char *ctf, ftnlen ctf_len)
{
    return swcmll_0_(2, (integer *)0, ntx, ctf, (char *)0, ctf_len, (ftnint)0)
	    ;
    }

/* Subroutine */ int swqcmd_(integer *ntx, char *ctd, ftnlen ctd_len)
{
    return swcmll_0_(3, (integer *)0, ntx, (char *)0, ctd, (ftnint)0, ctd_len)
	    ;
    }


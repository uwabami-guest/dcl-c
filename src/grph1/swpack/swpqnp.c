/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__3 = 3;

/* ----------------------------------------------------------------------- */
/*     SWPQNP / SWPQID / SWPQCP / SWPQVL / SWPSVL */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int swpqnp_0_(int n__, integer *ncp, char *cp, integer *idx, 
	integer *itp, integer *ipara, integer *in, ftnlen cp_len)
{
    /* Initialized data */

    static char cparas[8*27] = "MAXWNU  " "IWS     " "IPOSX   " "IPOSY   " 
	    "IWIDTH  " "IHEIGHT " "LWAIT   " "LWAIT0  " "LWAIT1  " "LKEY    " 
	    "LDUMP   " "LALT    " "LCOLOR  " "LSEP    " "LPRINT  " "LWND    " 
	    "MODE    " "NLNSIZE " "ICLRMAP " "LFGBG   " "LSYSFNT " "IDMPDGT " 
	    "MAXFNU  " "IFL     " "IBGPAGE " "LCMCH   " "LSTDOT  ";
    static integer itype[27] = { 1,0,1,1,1,1,2,2,2,2,2,2,2,2,2,2,1,1,1,2,2,1,
	    1,1,1,2,2 };
    static char cparal[40*27] = "****MAXWNU                              " 
	    "****IWS                                 " "WINDOW_X_POS        "
	    "                    " "WINDOW_Y_POS                            " 
	    "WINDOW_WIDTH                            " "WINDOW_HEIGHT       "
	    "                    " "WAIT                                    " 
	    "WAIT_OPENING                            " "WAIT_CLOSING        "
	    "                    " "KEYCLICK                                " 
	    "DUMP                                    " "ALTERNATE           "
	    "                    " "ENABLE_COLOR_PS                         " 
	    "SEPARATE                                " "PRINT               "
	    "                    " "SHOW_WINDOW                             " 
	    "****MODE                                " "****NLNSIZE         "
	    "                    " "COLORMAP_NUMBER                         " 
	    "****LFGBG                               " "USE_SYSTEM_FONT     "
	    "                    " "DMPFILE_DIGITS                          " 
	    "****MAXFNU                              " "****IFL             "
	    "                    " "BEGIN PAGE                              " 
	    "CMAP_CHANGABLE                          " "PNG_STDOUT          "
	    "                    ";

    /* System generated locals */
    address a__1[3];
    integer i__1[3];

    /* Builtin functions */
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen),
	     s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer n, id;
    extern integer lenc_(char *, ftnlen);
    static char cmsg[80];
    extern logical lchreq_(char *, char *, ftnlen, ftnlen);
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), swiqid_(char *, integer *, ftnlen), swlqid_(char 
	    *, integer *, ftnlen), swrqid_(char *, integer *, ftnlen), 
	    swiqvl_(integer *, integer *), swlqvl_(integer *, integer *), 
	    swisvl_(integer *, integer *), swlsvl_(integer *, integer *), 
	    swrqvl_(integer *, integer *), swrsvl_(integer *, integer *);

/*     / SHORT NAME / */
    switch(n__) {
	case 1: goto L_swpqid;
	case 2: goto L_swpqcp;
	case 3: goto L_swpqcl;
	case 4: goto L_swpqit;
	case 5: goto L_swpqvl;
	case 6: goto L_swpsvl;
	case 7: goto L_swpqin;
	}

/*     / LONG NAME / */
    *ncp = 27;
    return 0;
/* ----------------------------------------------------------------------- */

L_swpqid:
    for (n = 1; n <= 27; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *idx = n;
	    return 0;
	}
/* L10: */
    }
/* Writing concatenation */
    i__1[0] = 11, a__1[0] = "PARAMETER '";
    i__1[1] = lenc_(cp, cp_len), a__1[1] = cp;
    i__1[2] = 17, a__1[2] = "' IS NOT DEFINED.";
    s_cat(cmsg, a__1, i__1, &c__3, (ftnlen)80);
    msgdmp_("E", "SWPQID", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
    return 0;
/* ----------------------------------------------------------------------- */

L_swpqcp:
    if (1 <= *idx && *idx <= 27) {
	s_copy(cp, cparas + (*idx - 1 << 3), cp_len, (ftnlen)8);
    } else {
	msgdmp_("E", "SWPQCP", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_swpqcl:
    if (1 <= *idx && *idx <= 27) {
	s_copy(cp, cparal + (*idx - 1) * 40, cp_len, (ftnlen)40);
    } else {
	msgdmp_("E", "SWPQCL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_swpqit:
    if (1 <= *idx && *idx <= 27) {
	*itp = itype[*idx - 1];
    } else {
	msgdmp_("E", "SWPQIT", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_swpqvl:
    if (1 <= *idx && *idx <= 27) {
	if (itype[*idx - 1] == 1) {
	    swiqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    swiqvl_(&id, ipara);
	} else if (itype[*idx - 1] == 2) {
	    swlqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    swlqvl_(&id, ipara);
	} else if (itype[*idx - 1] == 3) {
	    swrqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    swrqvl_(&id, ipara);
	}
    } else {
	msgdmp_("E", "SWPQVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_swpsvl:
    if (1 <= *idx && *idx <= 27) {
	if (itype[*idx - 1] == 1) {
	    swiqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    swisvl_(&id, ipara);
	} else if (itype[*idx - 1] == 2) {
	    swlqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    swlsvl_(&id, ipara);
	} else if (itype[*idx - 1] == 3) {
	    swrqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    swrsvl_(&id, ipara);
	}
    } else {
	msgdmp_("E", "SWPSVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_swpqin:
    for (n = 1; n <= 27; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *in = n;
	    return 0;
	}
/* L20: */
    }
    *in = 0;
    return 0;
} /* swpqnp_ */

/* Subroutine */ int swpqnp_(integer *ncp)
{
    return swpqnp_0_(0, ncp, (char *)0, (integer *)0, (integer *)0, (integer *
	    )0, (integer *)0, (ftnint)0);
    }

/* Subroutine */ int swpqid_(char *cp, integer *idx, ftnlen cp_len)
{
    return swpqnp_0_(1, (integer *)0, cp, idx, (integer *)0, (integer *)0, (
	    integer *)0, cp_len);
    }

/* Subroutine */ int swpqcp_(integer *idx, char *cp, ftnlen cp_len)
{
    return swpqnp_0_(2, (integer *)0, cp, idx, (integer *)0, (integer *)0, (
	    integer *)0, cp_len);
    }

/* Subroutine */ int swpqcl_(integer *idx, char *cp, ftnlen cp_len)
{
    return swpqnp_0_(3, (integer *)0, cp, idx, (integer *)0, (integer *)0, (
	    integer *)0, cp_len);
    }

/* Subroutine */ int swpqit_(integer *idx, integer *itp)
{
    return swpqnp_0_(4, (integer *)0, (char *)0, idx, itp, (integer *)0, (
	    integer *)0, (ftnint)0);
    }

/* Subroutine */ int swpqvl_(integer *idx, integer *ipara)
{
    return swpqnp_0_(5, (integer *)0, (char *)0, idx, (integer *)0, ipara, (
	    integer *)0, (ftnint)0);
    }

/* Subroutine */ int swpsvl_(integer *idx, integer *ipara)
{
    return swpqnp_0_(6, (integer *)0, (char *)0, idx, (integer *)0, ipara, (
	    integer *)0, (ftnint)0);
    }

/* Subroutine */ int swpqin_(char *cp, integer *in, ftnlen cp_len)
{
    return swpqnp_0_(7, (integer *)0, cp, (integer *)0, (integer *)0, (
	    integer *)0, in, cp_len);
    }


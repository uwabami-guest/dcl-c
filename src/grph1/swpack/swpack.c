/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__0 = 0;
static integer c__2 = 2;

/* ----------------------------------------------------------------------- */
/*     SWPACK */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int swdopn_0_(int n__, char *cobj, char *comm, logical *
	lfcatr, integer *lfcmod, integer *iwdidx, integer *iclidx, integer *
	icolor, integer *iwidx, real *wx, real *wy, integer *np, real *wpx, 
	real *wpy, integer *itpat, integer *iwx, integer *iwy, integer *imw, 
	integer *imh, real *wx1, real *wy1, real *wx2, real *wy2, real *wx3, 
	real *wy3, real *wx4, real *wy4, integer *image, integer *nlen, 
	integer *mb, logical *lwdatr, logical *lclatr, logical *ltnatr, 
	logical *limatr, logical *lptatr, real *wsxmn, real *wsxmx, real *
	wsymn, real *wsymx, real *fact, integer *iwtrot, char *chars, integer 
	*nchz, real *wxch, real *wych, char *fontname, integer *fntnum, 
	integer *inum, integer *ifmax, real *qsizexrate, integer *ncz, 
	integer *irota, integer *icentz, ftnlen cobj_len, ftnlen comm_len, 
	ftnlen chars_len, ftnlen fontname_len)
{
    /* Initialized data */

    static char cnopn[26] = "WORKSTATION IS NOT OPENED.";
    static logical lopen = FALSE_;

    /* System generated locals */
    address a__1[2];
    integer i__1, i__2[2];
    char ch__1[92];

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_cmp(char *, char *, ftnlen, ftnlen);
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen);

    /* Local variables */
    extern /* Subroutine */ int zgfontname_(integer *, char *, integer *, 
	    ftnlen), zgnumfonts_(integer *);
    static integer ifl, iws, jws;
    static char cmap[80];
    extern integer lenc_(char *, ftnlen);
    extern /* Subroutine */ int zglistfonts_(void);
    static char cmsg[80];
    static logical lalt;
    static integer isub;
    static logical lsep, lkey, lwnd;
    static char cttl[80];
    extern integer lenz_(char *, ftnlen);
    static char cout[1024];
    static integer isup, irst;
    extern /* Subroutine */ int zgselectfont_(char *, ftnlen);
    static logical lfgbg;
    extern /* Subroutine */ int chngi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen);
    static integer npage;
    static logical lclip;
    static real small;
    static logical lcntl;
    static real shift;
    static logical lwait, ldump;
    extern /* Subroutine */ int zgsfw_(integer *);
    static real vxmin, vymin, vxmax, vymax, wxmin, wxmax, wymin, wymax;
    static integer iposx, iposy;
    extern /* Subroutine */ int zgtxt_(real *, real *, real *, char *, 
	    integer *, integer *, integer *, ftnlen);
    static logical lwait0, lwait1;
    static integer iheigh;
    extern /* Subroutine */ int glcget_(char *, char *, ftnlen, ftnlen);
    static char cbtmap[1024], clrmap[1024];
    extern /* Subroutine */ int sgiget_(char *, integer *, ftnlen);
    static char dspath[80];
    extern /* Subroutine */ int osgarg_(integer *, char *, ftnlen), dclvnm_(
	    char *, ftnlen);
    static integer iclrmp;
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen);
    static integer iwidth;
    extern /* Subroutine */ int clower_(char *, ftnlen), swcget_(char *, char 
	    *, ftnlen, ftnlen), sglget_(char *, logical *, ftnlen), sgrget_(
	    char *, real *, ftnlen), swqcmf_(integer *, char *, ftnlen), 
	    swcmll_(void);
    static logical lfprop;
    extern /* Subroutine */ int swiget_(char *, integer *, ftnlen), swlget_(),
	     swcset_(char *, char *, ftnlen, ftnlen), swqfnm_(char *, char *, 
	    ftnlen, ftnlen);
    static integer lstdot;
    extern /* Subroutine */ int swrget_(char *, real *, ftnlen), zglset_(char 
	    *, integer *, logical *, ftnlen), zgiset_(char *, integer *, 
	    integer *, ftnlen), zgrset_(char *, integer *, real *, ftnlen), 
	    zgdopn_(integer *, integer *, integer *, integer *, integer *, 
	    integer *, integer *, real *, logical *, logical *, logical *, 
	    logical *, integer *, logical *, logical *, logical *, logical *, 
	    logical *, integer *, char *, real *, char *, char *, char *, 
	    char *, ftnlen, ftnlen, ftnlen, ftnlen, ftnlen), zgdcls_(void), 
	    zgpopn_(void), zgpcls_(void), zgoopn_(char *, char *, ftnlen, 
	    ftnlen), zgocls_(char *, ftnlen), zgsfcm_(integer *), zgswdi_(
	    integer *), zgscli_(integer *), zgslcl_(integer *), zggopn_(void),
	     zggmov_(real *, real *), zggplt_(real *, real *), zggcls_(void), 
	    zgstcl_(integer *), zggton_(integer *, real *, real *, integer *),
	     zgiopn_(integer *, integer *, integer *, integer *), zgidat_(
	    integer *, integer *), zgiclr_(integer *, integer *), zgicls_(
	    void), zgqpnt_(real *, real *, integer *);
    static integer ibgpage;
    extern /* Subroutine */ int zgfint_(real *, real *, integer *, integer *),
	     zgiint_(integer *, integer *, real *, real *), zgqwdc_(logical *)
	    , zgqclc_(logical *), zgqtnc_(logical *), zgqimc_(logical *), 
	    zgqptc_(logical *), zgqrct_(real *, real *, real *, real *, real *
	    ), zgsrot_(integer *), zgqtxw_(char *, integer *, real *, real *, 
	    ftnlen);
    static char cimgfmt[8];
    extern /* Subroutine */ int zgftfc_(char *, ftnlen);
    static integer idmpdgt;
    static real rimgcmp;
    extern /* Subroutine */ int zgflash_(void), zgclini_(char *, logical *, 
	    ftnlen), sgpget_(char *, logical *, ftnlen);
    static real rlwfact;
    extern /* Subroutine */ int zgrclp_(void), sgqvpt_(real *, real *, real *,
	     real *), stfwtr_(real *, real *, real *, real *), zgclip_(real *,
	     real *, real *, real *);

    /* Parameter adjustments */
    if (wpx) {
	--wpx;
	}
    if (wpy) {
	--wpy;
	}
    if (image) {
	--image;
	}

    /* Function Body */
    switch(n__) {
	case 1: goto L_swdcls;
	case 2: goto L_swpopn;
	case 3: goto L_swflsh;
	case 4: goto L_swpcls;
	case 5: goto L_swoopn;
	case 6: goto L_swocls;
	case 7: goto L_swqfcc;
	case 8: goto L_swsfcm;
	case 9: goto L_swswdi;
	case 10: goto L_swscli;
	case 11: goto L_swslcl;
	case 12: goto L_swsfw;
	case 13: goto L_swgopn;
	case 14: goto L_swgmov;
	case 15: goto L_swgplt;
	case 16: goto L_swgcls;
	case 17: goto L_swstcl;
	case 18: goto L_swgton;
	case 19: goto L_swiopn;
	case 20: goto L_swidat;
	case 21: goto L_swiclr;
	case 22: goto L_swicls;
	case 23: goto L_swqpnt;
	case 24: goto L_swfint;
	case 25: goto L_swiint;
	case 26: goto L_swqwdc;
	case 27: goto L_swqclc;
	case 28: goto L_swqtnc;
	case 29: goto L_swqimc;
	case 30: goto L_swqptc;
	case 31: goto L_swqrct;
	case 32: goto L_swsrot;
	case 33: goto L_swclch;
	case 34: goto L_swqtxw;
	case 35: goto L_swftfc;
	case 36: goto L_swftnm;
	case 37: goto L_swslft;
	case 38: goto L_swgtft;
	case 39: goto L_swlsft;
	case 40: goto L_swtxt;
	}

/*     / OPEN ( AND ACTIVATE ) WORKSTATION / */
    sgiget_("IWS", &jws, (ftnlen)3);
    swiget_("IWS", &iws, (ftnlen)3);
    swiget_("IFL", &ifl, (ftnlen)3);
    s_copy(cmsg, "GRPH1 : STARTED / IWS = ##.", (ftnlen)80, (ftnlen)27);
    chngi_(cmsg, "##", &jws, "(I2)", (ftnlen)80, (ftnlen)2, (ftnlen)4);
    msgdmp_("M", "SWDOPN", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
/*     / DEVICE DEPENDENT PARAMETERS / */
    if (iws == 1 || iws == 2) {
	swiget_("IWIDTH ", &iwidth, (ftnlen)7);
	swiget_("IHEIGHT", &iheigh, (ftnlen)7);
	swlget_("LFGBG  ", &lfgbg, (ftnlen)7);
	swcget_("FNAME  ", cout, (ftnlen)7, (ftnlen)1024);
	swcget_("TITLE  ", cttl, (ftnlen)7, (ftnlen)80);
	swiget_("IDMPDGT ", &idmpdgt, (ftnlen)8);
	swiget_("IBGPAGE ", &ibgpage, (ftnlen)8);
	swiget_("ICLRMAP", &iclrmp, (ftnlen)7);
	swcmll_();
	swqcmf_(&iclrmp, clrmap, (ftnlen)1024);
	swcset_("CLRMAP", clrmap, (ftnlen)6, (ftnlen)1024);
	swqfnm_("CLRMAP ", clrmap, (ftnlen)7, (ftnlen)1024);
	glcget_("DSPATH", dspath, (ftnlen)6, (ftnlen)80);
	if (s_cmp(clrmap, " ", (ftnlen)1024, (ftnlen)1) == 0) {
	    msgdmp_("E", "SWDOPN", "COLORMAP FILE DOES NOT EXIST.", (ftnlen)1,
		     (ftnlen)6, (ftnlen)29);
	}
	if (s_cmp(cout, " ", (ftnlen)1024, (ftnlen)1) == 0 || s_cmp(cout, 
		"*", (ftnlen)1024, (ftnlen)1) == 0) {
	    osgarg_(&c__0, cout, (ftnlen)1024);
	}
	if (s_cmp(cout, " ", (ftnlen)1024, (ftnlen)1) == 0) {
	    s_copy(cout, "DCL", (ftnlen)1024, (ftnlen)3);
	    clower_(cout, (ftnlen)1024);
	}
	if (s_cmp(cttl, " ", (ftnlen)80, (ftnlen)1) == 0 || s_cmp(cttl, "*", (
		ftnlen)80, (ftnlen)1) == 0) {
	    dclvnm_(cttl, (ftnlen)80);
	}
    }
    swiget_("IPOSX  ", &iposx, (ftnlen)7);
    swiget_("IPOSY  ", &iposy, (ftnlen)7);
    swlget_("LWAIT  ", &lwait, (ftnlen)7);
    swlget_("LWAIT0 ", &lwait0, (ftnlen)7);
    swlget_("LWAIT1 ", &lwait1, (ftnlen)7);
    swlget_("LKEY   ", &lkey, (ftnlen)7);
    swlget_("LALT   ", &lalt, (ftnlen)7);
    swlget_("LDUMP  ", &ldump, (ftnlen)7);
    swlget_("LSTDOT ", &lstdot, (ftnlen)7);
    swqfnm_("BITMAP ", cbtmap, (ftnlen)7, (ftnlen)1024);
    if (s_cmp(cbtmap, " ", (ftnlen)1024, (ftnlen)1) == 0) {
	msgdmp_("E", "SWDOPN", "BITMAP FILE DOES NOT EXIST.", (ftnlen)1, (
		ftnlen)6, (ftnlen)27);
    }
    swlget_("LWND   ", &lwnd, (ftnlen)7);
    swcget_("CIMGFMT ", cimgfmt, (ftnlen)8, (ftnlen)8);
    swrget_("RIMGCMP ", &rimgcmp, (ftnlen)8);
    swlget_("LSEP   ", &lsep, (ftnlen)7);
    swrget_("RLWFACT", &rlwfact, (ftnlen)7);
    sglget_("LFPROP ", &lfprop, (ftnlen)7);
    sglget_("LCNTL  ", &lcntl, (ftnlen)7);
    sgiget_("ISUP   ", &isup, (ftnlen)7);
    sgiget_("ISUB   ", &isub, (ftnlen)7);
    sgiget_("IRST   ", &irst, (ftnlen)7);
    sgrget_("SMALL  ", &small, (ftnlen)7);
    sgrget_("SHIFT  ", &shift, (ftnlen)7);
    i__1 = lenz_("lfprop", (ftnlen)6);
    zglset_("lfprop  ", &i__1, &lfprop, (ftnlen)8);
    i__1 = lenz_("lcntl", (ftnlen)5);
    zglset_("lcntl   ", &i__1, &lcntl, (ftnlen)8);
    i__1 = lenz_("isup", (ftnlen)4);
    zgiset_("isup    ", &i__1, &isup, (ftnlen)8);
    i__1 = lenz_("isub", (ftnlen)4);
    zgiset_("isub    ", &i__1, &isub, (ftnlen)8);
    i__1 = lenz_("irst", (ftnlen)4);
    zgiset_("irst    ", &i__1, &irst, (ftnlen)8);
    i__1 = lenz_("small", (ftnlen)5);
    zgrset_("small   ", &i__1, &small, (ftnlen)8);
    i__1 = lenz_("shift", (ftnlen)5);
    zgrset_("shift   ", &i__1, &shift, (ftnlen)8);
    zgdopn_(&jws, &iwidth, &iheigh, &iposx, &iposy, &idmpdgt, &ibgpage, &
	    rlwfact, &lwait, &lwait0, &lwait1, &lalt, &lstdot, &lkey, &ldump, 
	    &lwnd, &lfgbg, &lsep, &ifl, cimgfmt, &rimgcmp, clrmap, cbtmap, 
	    cout, cttl, (ftnlen)8, (ftnlen)1024, (ftnlen)1024, (ftnlen)1024, (
	    ftnlen)80);
/* Writing concatenation */
    i__2[0] = lenc_(dspath, (ftnlen)80), a__1[0] = dspath;
    i__2[1] = 12, a__1[1] = "colormap.x11";
    s_cat(ch__1, a__1, i__2, &c__2, (ftnlen)92);
    if (s_cmp(clrmap, ch__1, (ftnlen)1024, lenc_(dspath, (ftnlen)80) + 12) == 
	    0) {
	swqcmf_(&iclrmp, cmap, (ftnlen)80);
	swcset_("CLRMAP ", cmap, (ftnlen)7, (ftnlen)80);
    }
    lopen = TRUE_;
    return 0;
/* ----------------------------------------------------------------------- */

L_swdcls:
/*     / ( DEACTIVATE AND ) CLOSE WORKSTATION / */
    zgdcls_();
    s_copy(cmsg, "GRPH1 : TERMINATED.", (ftnlen)80, (ftnlen)19);
    msgdmp_("M", "SWDCLS", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
    lopen = FALSE_;
    return 0;
/* ----------------------------------------------------------------------- */
/*     PAGE */
/* ----------------------------------------------------------------------- */

L_swpopn:
/*     / OPEN PAGE ( OR SCREEN ) / */
    if (! lopen) {
	msgdmp_("E", "SWPOPN", cnopn, (ftnlen)1, (ftnlen)6, (ftnlen)26);
    }
    zgpopn_();
    return 0;
/* ----------------------------------------------------------------------- */

L_swflsh:
/*     / PAGE FLASH EXPLICIT / */
    zgflash_();
    return 0;
/* ----------------------------------------------------------------------- */

L_swpcls:
/*     / CLOSE PAGE ( OR SCREEN ) / */
    if (! lopen) {
	msgdmp_("E", "SWPCLS", cnopn, (ftnlen)1, (ftnlen)6, (ftnlen)26);
    }
    ++npage;
    s_copy(cmsg, "GRPH1 : PAGE = ### COMPLETED.", (ftnlen)80, (ftnlen)29);
    chngi_(cmsg, "###", &npage, "(I3)", (ftnlen)80, (ftnlen)3, (ftnlen)4);
    msgdmp_("M", "SWPCLS", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
    zgpcls_();
    return 0;
/* ----------------------------------------------------------------------- */
/*     OBJECT */
/* ----------------------------------------------------------------------- */

L_swoopn:
/*     / OPEN OBJECT / */
    if (! lopen) {
	msgdmp_("E", "SWOOPN", cnopn, (ftnlen)1, (ftnlen)6, (ftnlen)26);
    }
    zgoopn_(cobj, comm, cobj_len, comm_len);
    return 0;
/* ----------------------------------------------------------------------- */

L_swocls:
/*     / CLOSE OBJECT / */
    if (! lopen) {
	msgdmp_("E", "SWOCLS", cnopn, (ftnlen)1, (ftnlen)6, (ftnlen)26);
    }
    zgocls_(cobj, cobj_len);
    return 0;
/* ----------------------------------------------------------------------- */
/*     FULL COLOR (2000/12/10 TEST) */
/* ----------------------------------------------------------------------- */

L_swqfcc:
/*     / INQUIRE FULL COLOR CAPABILITY / */
    *lfcatr = TRUE_;
    return 0;
/* ----------------------------------------------------------------------- */

L_swsfcm:
/*     / SET FULL COLOR MODE / */
    zgsfcm_(lfcmod);
    return 0;
/* ----------------------------------------------------------------------- */
/*     LINE */
/* ----------------------------------------------------------------------- */

L_swswdi:
/*     / SET LINE WIDTH INDEX / */
    if (! lopen) {
	msgdmp_("E", "SWSWDI", cnopn, (ftnlen)1, (ftnlen)6, (ftnlen)26);
    }
    zgswdi_(iwdidx);
    return 0;
/* ----------------------------------------------------------------------- */

L_swscli:
/*     / SET LINE COLOR INDEX / */
    if (! lopen) {
	msgdmp_("E", "SWSCLI", cnopn, (ftnlen)1, (ftnlen)6, (ftnlen)26);
    }
    zgscli_(iclidx);
    return 0;
/* ----------------------------------------------------------------------- */

L_swslcl:
/*     / SET LINE COLOR IN 24BIT RGB / */
    zgslcl_(icolor);
    return 0;
/* ----------------------------------------------------------------------- */

L_swsfw:
/*     / SET FONT WEIGHT / */
    zgsfw_(iwidx);
    return 0;
/* ----------------------------------------------------------------------- */
/*     OPEN */
/* ----------------------------------------------------------------------- */

L_swgopn:
/*     / OPEN GRAPHIC SEGMENT / */
    if (! lopen) {
	msgdmp_("E", "SWGOPN", cnopn, (ftnlen)1, (ftnlen)6, (ftnlen)26);
    }
    zggopn_();
    return 0;
/* ----------------------------------------------------------------------- */

L_swgmov:
/*     / PEN-UP MOVE / */
    if (! lopen) {
	msgdmp_("E", "SWGMOV", cnopn, (ftnlen)1, (ftnlen)6, (ftnlen)26);
    }
    zggmov_(wx, wy);
    return 0;
/* ----------------------------------------------------------------------- */

L_swgplt:
/*     / PEN-DOWN MOVE / */
    if (! lopen) {
	msgdmp_("E", "SWGPLT", cnopn, (ftnlen)1, (ftnlen)6, (ftnlen)26);
    }
    zggplt_(wx, wy);
    return 0;
/* ----------------------------------------------------------------------- */
/*     CLOSE */
/* ----------------------------------------------------------------------- */

L_swgcls:
/*     / CLOSE GRAPHIC SEGMENT / */
    if (! lopen) {
	msgdmp_("E", "SWGCLS", cnopn, (ftnlen)1, (ftnlen)6, (ftnlen)26);
    }
    zggcls_();
    return 0;
/* ----------------------------------------------------------------------- */
/*     TONE */
/* ----------------------------------------------------------------------- */

L_swstcl:
/*     / SET TONE COLOR IN 24BIT RGB / */
    zgstcl_(icolor);
    return 0;
/* ----------------------------------------------------------------------- */

L_swgton:
/*     / HARD FILL / */
    if (! lopen) {
	msgdmp_("E", "SWGTON", cnopn, (ftnlen)1, (ftnlen)6, (ftnlen)26);
    }
    zggton_(np, &wpx[1], &wpy[1], itpat);
    return 0;
/* ----------------------------------------------------------------------- */
/*     IMAGE */
/* ----------------------------------------------------------------------- */

L_swiopn:
/*     / OPEN IMAGE / */
    if (! lopen) {
	msgdmp_("E", "SWIOPN", cnopn, (ftnlen)1, (ftnlen)6, (ftnlen)26);
    }
    zgiopn_(iwx, iwy, imw, imh);
    return 0;
/* ----------------------------------------------------------------------- */

L_swidat:
/*     / IMAGE DATA / */
    if (! lopen) {
	msgdmp_("E", "SWIDAT", cnopn, (ftnlen)1, (ftnlen)6, (ftnlen)26);
    }
    zgidat_(&image[1], nlen);
    return 0;
/* ----------------------------------------------------------------------- */

L_swiclr:
/*     / IMAGE DATA (FULL COLOR) / */
    zgiclr_(&image[1], nlen);
    return 0;
/* ----------------------------------------------------------------------- */

L_swicls:
/*     / IMAGE CLOSE / */
    if (! lopen) {
	msgdmp_("E", "SWICLS", cnopn, (ftnlen)1, (ftnlen)6, (ftnlen)26);
    }
    zgicls_();
    return 0;
/* ----------------------------------------------------------------------- */
/*     MOUSE */
/* ----------------------------------------------------------------------- */

L_swqpnt:
/*     / INQUIRE POSITION OF POINTER / */
    if (! lopen) {
	msgdmp_("E", "SWQPNT", cnopn, (ftnlen)1, (ftnlen)6, (ftnlen)26);
    }
    zgqpnt_(wx, wy, mb);
    return 0;
/* ----------------------------------------------------------------------- */
/*     TRANSFORMATION */
/* ----------------------------------------------------------------------- */

L_swfint:
/*     / REAL TO INT / */
    if (! lopen) {
	msgdmp_("E", "SWFINT", cnopn, (ftnlen)1, (ftnlen)6, (ftnlen)26);
    }
    zgfint_(wx, wy, iwx, iwy);
    return 0;
/* ----------------------------------------------------------------------- */

L_swiint:
/*     / INT TO REAL / */
    if (! lopen) {
	msgdmp_("E", "SWIINT", cnopn, (ftnlen)1, (ftnlen)6, (ftnlen)26);
    }
    zgiint_(iwx, iwy, wx, wy);
    return 0;
/* ----------------------------------------------------------------------- */
/*     INQUIRY */
/* ----------------------------------------------------------------------- */

L_swqwdc:
/*     / INQUIRE LINE WIDTH CAPABILITY / */
    zgqwdc_(lwdatr);
    return 0;
/* ----------------------------------------------------------------------- */

L_swqclc:
/*     / INQUIRE LINE COLOR CAPABILITY / */
    if (! lopen) {
	msgdmp_("E", "SWQCLC", cnopn, (ftnlen)1, (ftnlen)6, (ftnlen)26);
    }
    zgqclc_(lclatr);
    return 0;
/* ----------------------------------------------------------------------- */

L_swqtnc:
/*     / INQUIRE HARD FILL CAPABILITY / */
    zgqtnc_(ltnatr);
    return 0;
/* ----------------------------------------------------------------------- */

L_swqimc:
/*     / INQUIRE BIT IMAGE CAPABILITY / */
    zgqimc_(limatr);
    return 0;
/* ----------------------------------------------------------------------- */

L_swqptc:
/*     / INQUIRE MOUSE POINT CAPABILITY / */
    zgqptc_(lptatr);
    return 0;
/* ----------------------------------------------------------------------- */

L_swqrct:
/*     / INQUIRE WORKSTATION RECTANGLE / */
    if (! lopen) {
	msgdmp_("E", "SWQRCT", cnopn, (ftnlen)1, (ftnlen)6, (ftnlen)26);
    }
    zgqrct_(wsxmn, wsxmx, wsymn, wsymx, fact);
    return 0;
/* ----------------------------------------------------------------------- */

L_swsrot:
/*     / SET FRAME ROTATION FLAG / */
    if (! lopen) {
	msgdmp_("E", "SWSROT", cnopn, (ftnlen)1, (ftnlen)6, (ftnlen)26);
    }
    zgsrot_(iwtrot);
    return 0;
/* ----------------------------------------------------------------------- */

L_swclch:
/*     / CHANGE COLORMAP IMMIDIATELY / */
    swiget_("ICLRMAP", &iclrmp, (ftnlen)7);
    swqcmf_(&iclrmp, cmap, (ftnlen)80);
    swcset_("CLRMAP", cmap, (ftnlen)6, (ftnlen)80);
    swqfnm_("CLRMAP ", clrmap, (ftnlen)7, (ftnlen)1024);
    swlget_("LFGBG", &lfgbg, (ftnlen)5);
    if (s_cmp(clrmap, " ", (ftnlen)1024, (ftnlen)1) == 0) {
	msgdmp_("E", "SWDOPN", "COLORMAP FILE DOES NOT EXIST.", (ftnlen)1, (
		ftnlen)6, (ftnlen)29);
    }
    zgclini_(clrmap, &lfgbg, (ftnlen)1024);
    return 0;
/* ----------------------------------------------------------------------- */
/*     TEXT */
/* ----------------------------------------------------------------------- */

L_swqtxw:
/*       / PASS SG* PARAMETERS TO ZGPACK / */
    sglget_("LCNTL", &lcntl, (ftnlen)5);
    i__1 = lenz_("lcntl", (ftnlen)5);
    zglset_("lcntl ", &i__1, &lcntl, (ftnlen)6);
    if (lcntl) {
	sgiget_("ISUB", &isub, (ftnlen)4);
	sgiget_("ISUP", &isup, (ftnlen)4);
	sgiget_("IRST", &irst, (ftnlen)4);
	sgrget_("SMALL", &small, (ftnlen)5);
	sgrget_("SHIFT", &shift, (ftnlen)5);
	i__1 = lenz_("isub", (ftnlen)4);
	zgiset_("isub ", &i__1, &isub, (ftnlen)5);
	i__1 = lenz_("isup", (ftnlen)4);
	zgiset_("isup ", &i__1, &isup, (ftnlen)5);
	i__1 = lenz_("irst", (ftnlen)4);
	zgiset_("irst ", &i__1, &irst, (ftnlen)5);
	i__1 = lenz_("small", (ftnlen)5);
	zgrset_("small ", &i__1, &small, (ftnlen)6);
	i__1 = lenz_("shift", (ftnlen)5);
	zgrset_("shift ", &i__1, &shift, (ftnlen)6);
    }
    zgqtxw_(chars, nchz, wxch, wych, chars_len);
    return 0;
/* ----------------------------------------------------------------------- */

L_swftfc:
    zgftfc_(fontname, fontname_len);
    return 0;
/* ----------------------------------------------------------------------- */

L_swftnm:
    zgnumfonts_(fntnum);
    return 0;
/* ----------------------------------------------------------------------- */

L_swslft:
    if (iws == 2 && ifl != 1) {
	msgdmp_("M", "SWSLFT", "DIALOG NEEDS WINDOW.DEFAULT FONT IS USED", (
		ftnlen)1, (ftnlen)6, (ftnlen)40);
    } else {
	zgselectfont_(fontname, fontname_len);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_swgtft:
    zgfontname_(inum, fontname, ifmax, fontname_len);
    return 0;
/* ----------------------------------------------------------------------- */

L_swlsft:
    zglistfonts_();
    return 0;
/* ----------------------------------------------------------------------- */

L_swtxt:
    sgpget_("LCLIP", &lclip, (ftnlen)5);
    zgrclp_();
    if (lclip) {
	sgqvpt_(&vxmin, &vxmax, &vymin, &vymax);
	stfwtr_(&vxmin, &vymin, &wxmin, &wymin);
	stfwtr_(&vxmax, &vymax, &wxmax, &wymax);
	zgclip_(&wxmin, &wxmax, &wymin, &wymax);
    }
/*       / PASS SG* PARAMETERS TO ZGPACK / */
    sglget_("LCNTL", &lcntl, (ftnlen)5);
    i__1 = lenz_("lcntl", (ftnlen)5);
    zglset_("lcntl ", &i__1, &lcntl, (ftnlen)6);
    if (lcntl) {
	sgiget_("ISUB", &isub, (ftnlen)4);
	sgiget_("ISUP", &isup, (ftnlen)4);
	sgiget_("IRST", &irst, (ftnlen)4);
	sgrget_("SMALL", &small, (ftnlen)5);
	sgrget_("SHIFT", &shift, (ftnlen)5);
	i__1 = lenz_("isub", (ftnlen)4);
	zgiset_("isub ", &i__1, &isub, (ftnlen)5);
	i__1 = lenz_("isup", (ftnlen)4);
	zgiset_("isup ", &i__1, &isup, (ftnlen)5);
	i__1 = lenz_("irst", (ftnlen)4);
	zgiset_("irst ", &i__1, &irst, (ftnlen)5);
	i__1 = lenz_("small", (ftnlen)5);
	zgrset_("small ", &i__1, &small, (ftnlen)6);
	i__1 = lenz_("shift", (ftnlen)5);
	zgrset_("shift ", &i__1, &shift, (ftnlen)6);
    }
    zgtxt_(wx, wy, qsizexrate, chars, ncz, irota, icentz, chars_len);
    zgrclp_();
    return 0;
} /* swdopn_ */

/* Subroutine */ int swdopn_(void)
{
    return swdopn_0_(0, (char *)0, (char *)0, (logical *)0, (integer *)0, (
	    integer *)0, (integer *)0, (integer *)0, (integer *)0, (real *)0, 
	    (real *)0, (integer *)0, (real *)0, (real *)0, (integer *)0, (
	    integer *)0, (integer *)0, (integer *)0, (integer *)0, (real *)0, 
	    (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, 
	    (real *)0, (integer *)0, (integer *)0, (integer *)0, (logical *)0,
	     (logical *)0, (logical *)0, (logical *)0, (logical *)0, (real *)
	    0, (real *)0, (real *)0, (real *)0, (real *)0, (integer *)0, (
	    char *)0, (integer *)0, (real *)0, (real *)0, (char *)0, (integer 
	    *)0, (integer *)0, (integer *)0, (real *)0, (integer *)0, (
	    integer *)0, (integer *)0, (ftnint)0, (ftnint)0, (ftnint)0, (
	    ftnint)0);
    }

/* Subroutine */ int swdcls_(void)
{
    return swdopn_0_(1, (char *)0, (char *)0, (logical *)0, (integer *)0, (
	    integer *)0, (integer *)0, (integer *)0, (integer *)0, (real *)0, 
	    (real *)0, (integer *)0, (real *)0, (real *)0, (integer *)0, (
	    integer *)0, (integer *)0, (integer *)0, (integer *)0, (real *)0, 
	    (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, 
	    (real *)0, (integer *)0, (integer *)0, (integer *)0, (logical *)0,
	     (logical *)0, (logical *)0, (logical *)0, (logical *)0, (real *)
	    0, (real *)0, (real *)0, (real *)0, (real *)0, (integer *)0, (
	    char *)0, (integer *)0, (real *)0, (real *)0, (char *)0, (integer 
	    *)0, (integer *)0, (integer *)0, (real *)0, (integer *)0, (
	    integer *)0, (integer *)0, (ftnint)0, (ftnint)0, (ftnint)0, (
	    ftnint)0);
    }

/* Subroutine */ int swpopn_(void)
{
    return swdopn_0_(2, (char *)0, (char *)0, (logical *)0, (integer *)0, (
	    integer *)0, (integer *)0, (integer *)0, (integer *)0, (real *)0, 
	    (real *)0, (integer *)0, (real *)0, (real *)0, (integer *)0, (
	    integer *)0, (integer *)0, (integer *)0, (integer *)0, (real *)0, 
	    (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, 
	    (real *)0, (integer *)0, (integer *)0, (integer *)0, (logical *)0,
	     (logical *)0, (logical *)0, (logical *)0, (logical *)0, (real *)
	    0, (real *)0, (real *)0, (real *)0, (real *)0, (integer *)0, (
	    char *)0, (integer *)0, (real *)0, (real *)0, (char *)0, (integer 
	    *)0, (integer *)0, (integer *)0, (real *)0, (integer *)0, (
	    integer *)0, (integer *)0, (ftnint)0, (ftnint)0, (ftnint)0, (
	    ftnint)0);
    }

/* Subroutine */ int swflsh_(void)
{
    return swdopn_0_(3, (char *)0, (char *)0, (logical *)0, (integer *)0, (
	    integer *)0, (integer *)0, (integer *)0, (integer *)0, (real *)0, 
	    (real *)0, (integer *)0, (real *)0, (real *)0, (integer *)0, (
	    integer *)0, (integer *)0, (integer *)0, (integer *)0, (real *)0, 
	    (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, 
	    (real *)0, (integer *)0, (integer *)0, (integer *)0, (logical *)0,
	     (logical *)0, (logical *)0, (logical *)0, (logical *)0, (real *)
	    0, (real *)0, (real *)0, (real *)0, (real *)0, (integer *)0, (
	    char *)0, (integer *)0, (real *)0, (real *)0, (char *)0, (integer 
	    *)0, (integer *)0, (integer *)0, (real *)0, (integer *)0, (
	    integer *)0, (integer *)0, (ftnint)0, (ftnint)0, (ftnint)0, (
	    ftnint)0);
    }

/* Subroutine */ int swpcls_(void)
{
    return swdopn_0_(4, (char *)0, (char *)0, (logical *)0, (integer *)0, (
	    integer *)0, (integer *)0, (integer *)0, (integer *)0, (real *)0, 
	    (real *)0, (integer *)0, (real *)0, (real *)0, (integer *)0, (
	    integer *)0, (integer *)0, (integer *)0, (integer *)0, (real *)0, 
	    (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, 
	    (real *)0, (integer *)0, (integer *)0, (integer *)0, (logical *)0,
	     (logical *)0, (logical *)0, (logical *)0, (logical *)0, (real *)
	    0, (real *)0, (real *)0, (real *)0, (real *)0, (integer *)0, (
	    char *)0, (integer *)0, (real *)0, (real *)0, (char *)0, (integer 
	    *)0, (integer *)0, (integer *)0, (real *)0, (integer *)0, (
	    integer *)0, (integer *)0, (ftnint)0, (ftnint)0, (ftnint)0, (
	    ftnint)0);
    }

/* Subroutine */ int swoopn_(char *cobj, char *comm, ftnlen cobj_len, ftnlen 
	comm_len)
{
    return swdopn_0_(5, cobj, comm, (logical *)0, (integer *)0, (integer *)0, 
	    (integer *)0, (integer *)0, (integer *)0, (real *)0, (real *)0, (
	    integer *)0, (real *)0, (real *)0, (integer *)0, (integer *)0, (
	    integer *)0, (integer *)0, (integer *)0, (real *)0, (real *)0, (
	    real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (
	    integer *)0, (integer *)0, (integer *)0, (logical *)0, (logical *)
	    0, (logical *)0, (logical *)0, (logical *)0, (real *)0, (real *)0,
	     (real *)0, (real *)0, (real *)0, (integer *)0, (char *)0, (
	    integer *)0, (real *)0, (real *)0, (char *)0, (integer *)0, (
	    integer *)0, (integer *)0, (real *)0, (integer *)0, (integer *)0, 
	    (integer *)0, cobj_len, comm_len, (ftnint)0, (ftnint)0);
    }

/* Subroutine */ int swocls_(char *cobj, ftnlen cobj_len)
{
    return swdopn_0_(6, cobj, (char *)0, (logical *)0, (integer *)0, (integer 
	    *)0, (integer *)0, (integer *)0, (integer *)0, (real *)0, (real *)
	    0, (integer *)0, (real *)0, (real *)0, (integer *)0, (integer *)0,
	     (integer *)0, (integer *)0, (integer *)0, (real *)0, (real *)0, (
	    real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (
	    integer *)0, (integer *)0, (integer *)0, (logical *)0, (logical *)
	    0, (logical *)0, (logical *)0, (logical *)0, (real *)0, (real *)0,
	     (real *)0, (real *)0, (real *)0, (integer *)0, (char *)0, (
	    integer *)0, (real *)0, (real *)0, (char *)0, (integer *)0, (
	    integer *)0, (integer *)0, (real *)0, (integer *)0, (integer *)0, 
	    (integer *)0, cobj_len, (ftnint)0, (ftnint)0, (ftnint)0);
    }

/* Subroutine */ int swqfcc_(logical *lfcatr)
{
    return swdopn_0_(7, (char *)0, (char *)0, lfcatr, (integer *)0, (integer *
	    )0, (integer *)0, (integer *)0, (integer *)0, (real *)0, (real *)
	    0, (integer *)0, (real *)0, (real *)0, (integer *)0, (integer *)0,
	     (integer *)0, (integer *)0, (integer *)0, (real *)0, (real *)0, (
	    real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (
	    integer *)0, (integer *)0, (integer *)0, (logical *)0, (logical *)
	    0, (logical *)0, (logical *)0, (logical *)0, (real *)0, (real *)0,
	     (real *)0, (real *)0, (real *)0, (integer *)0, (char *)0, (
	    integer *)0, (real *)0, (real *)0, (char *)0, (integer *)0, (
	    integer *)0, (integer *)0, (real *)0, (integer *)0, (integer *)0, 
	    (integer *)0, (ftnint)0, (ftnint)0, (ftnint)0, (ftnint)0);
    }

/* Subroutine */ int swsfcm_(integer *lfcmod)
{
    return swdopn_0_(8, (char *)0, (char *)0, (logical *)0, lfcmod, (integer *
	    )0, (integer *)0, (integer *)0, (integer *)0, (real *)0, (real *)
	    0, (integer *)0, (real *)0, (real *)0, (integer *)0, (integer *)0,
	     (integer *)0, (integer *)0, (integer *)0, (real *)0, (real *)0, (
	    real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (
	    integer *)0, (integer *)0, (integer *)0, (logical *)0, (logical *)
	    0, (logical *)0, (logical *)0, (logical *)0, (real *)0, (real *)0,
	     (real *)0, (real *)0, (real *)0, (integer *)0, (char *)0, (
	    integer *)0, (real *)0, (real *)0, (char *)0, (integer *)0, (
	    integer *)0, (integer *)0, (real *)0, (integer *)0, (integer *)0, 
	    (integer *)0, (ftnint)0, (ftnint)0, (ftnint)0, (ftnint)0);
    }

/* Subroutine */ int swswdi_(integer *iwdidx)
{
    return swdopn_0_(9, (char *)0, (char *)0, (logical *)0, (integer *)0, 
	    iwdidx, (integer *)0, (integer *)0, (integer *)0, (real *)0, (
	    real *)0, (integer *)0, (real *)0, (real *)0, (integer *)0, (
	    integer *)0, (integer *)0, (integer *)0, (integer *)0, (real *)0, 
	    (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, 
	    (real *)0, (integer *)0, (integer *)0, (integer *)0, (logical *)0,
	     (logical *)0, (logical *)0, (logical *)0, (logical *)0, (real *)
	    0, (real *)0, (real *)0, (real *)0, (real *)0, (integer *)0, (
	    char *)0, (integer *)0, (real *)0, (real *)0, (char *)0, (integer 
	    *)0, (integer *)0, (integer *)0, (real *)0, (integer *)0, (
	    integer *)0, (integer *)0, (ftnint)0, (ftnint)0, (ftnint)0, (
	    ftnint)0);
    }

/* Subroutine */ int swscli_(integer *iclidx, logical *lfcatr)
{
    return swdopn_0_(10, (char *)0, (char *)0, lfcatr, (integer *)0, (integer 
	    *)0, iclidx, (integer *)0, (integer *)0, (real *)0, (real *)0, (
	    integer *)0, (real *)0, (real *)0, (integer *)0, (integer *)0, (
	    integer *)0, (integer *)0, (integer *)0, (real *)0, (real *)0, (
	    real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (
	    integer *)0, (integer *)0, (integer *)0, (logical *)0, (logical *)
	    0, (logical *)0, (logical *)0, (logical *)0, (real *)0, (real *)0,
	     (real *)0, (real *)0, (real *)0, (integer *)0, (char *)0, (
	    integer *)0, (real *)0, (real *)0, (char *)0, (integer *)0, (
	    integer *)0, (integer *)0, (real *)0, (integer *)0, (integer *)0, 
	    (integer *)0, (ftnint)0, (ftnint)0, (ftnint)0, (ftnint)0);
    }

/* Subroutine */ int swslcl_(integer *icolor)
{
    return swdopn_0_(11, (char *)0, (char *)0, (logical *)0, (integer *)0, (
	    integer *)0, (integer *)0, icolor, (integer *)0, (real *)0, (real 
	    *)0, (integer *)0, (real *)0, (real *)0, (integer *)0, (integer *)
	    0, (integer *)0, (integer *)0, (integer *)0, (real *)0, (real *)0,
	     (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0,
	     (integer *)0, (integer *)0, (integer *)0, (logical *)0, (logical 
	    *)0, (logical *)0, (logical *)0, (logical *)0, (real *)0, (real *)
	    0, (real *)0, (real *)0, (real *)0, (integer *)0, (char *)0, (
	    integer *)0, (real *)0, (real *)0, (char *)0, (integer *)0, (
	    integer *)0, (integer *)0, (real *)0, (integer *)0, (integer *)0, 
	    (integer *)0, (ftnint)0, (ftnint)0, (ftnint)0, (ftnint)0);
    }

/* Subroutine */ int swsfw_(integer *iwidx)
{
    return swdopn_0_(12, (char *)0, (char *)0, (logical *)0, (integer *)0, (
	    integer *)0, (integer *)0, (integer *)0, iwidx, (real *)0, (real *
	    )0, (integer *)0, (real *)0, (real *)0, (integer *)0, (integer *)
	    0, (integer *)0, (integer *)0, (integer *)0, (real *)0, (real *)0,
	     (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0,
	     (integer *)0, (integer *)0, (integer *)0, (logical *)0, (logical 
	    *)0, (logical *)0, (logical *)0, (logical *)0, (real *)0, (real *)
	    0, (real *)0, (real *)0, (real *)0, (integer *)0, (char *)0, (
	    integer *)0, (real *)0, (real *)0, (char *)0, (integer *)0, (
	    integer *)0, (integer *)0, (real *)0, (integer *)0, (integer *)0, 
	    (integer *)0, (ftnint)0, (ftnint)0, (ftnint)0, (ftnint)0);
    }

/* Subroutine */ int swgopn_(void)
{
    return swdopn_0_(13, (char *)0, (char *)0, (logical *)0, (integer *)0, (
	    integer *)0, (integer *)0, (integer *)0, (integer *)0, (real *)0, 
	    (real *)0, (integer *)0, (real *)0, (real *)0, (integer *)0, (
	    integer *)0, (integer *)0, (integer *)0, (integer *)0, (real *)0, 
	    (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, 
	    (real *)0, (integer *)0, (integer *)0, (integer *)0, (logical *)0,
	     (logical *)0, (logical *)0, (logical *)0, (logical *)0, (real *)
	    0, (real *)0, (real *)0, (real *)0, (real *)0, (integer *)0, (
	    char *)0, (integer *)0, (real *)0, (real *)0, (char *)0, (integer 
	    *)0, (integer *)0, (integer *)0, (real *)0, (integer *)0, (
	    integer *)0, (integer *)0, (ftnint)0, (ftnint)0, (ftnint)0, (
	    ftnint)0);
    }

/* Subroutine */ int swgmov_(real *wx, real *wy)
{
    return swdopn_0_(14, (char *)0, (char *)0, (logical *)0, (integer *)0, (
	    integer *)0, (integer *)0, (integer *)0, (integer *)0, wx, wy, (
	    integer *)0, (real *)0, (real *)0, (integer *)0, (integer *)0, (
	    integer *)0, (integer *)0, (integer *)0, (real *)0, (real *)0, (
	    real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (
	    integer *)0, (integer *)0, (integer *)0, (logical *)0, (logical *)
	    0, (logical *)0, (logical *)0, (logical *)0, (real *)0, (real *)0,
	     (real *)0, (real *)0, (real *)0, (integer *)0, (char *)0, (
	    integer *)0, (real *)0, (real *)0, (char *)0, (integer *)0, (
	    integer *)0, (integer *)0, (real *)0, (integer *)0, (integer *)0, 
	    (integer *)0, (ftnint)0, (ftnint)0, (ftnint)0, (ftnint)0);
    }

/* Subroutine */ int swgplt_(real *wx, real *wy)
{
    return swdopn_0_(15, (char *)0, (char *)0, (logical *)0, (integer *)0, (
	    integer *)0, (integer *)0, (integer *)0, (integer *)0, wx, wy, (
	    integer *)0, (real *)0, (real *)0, (integer *)0, (integer *)0, (
	    integer *)0, (integer *)0, (integer *)0, (real *)0, (real *)0, (
	    real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (
	    integer *)0, (integer *)0, (integer *)0, (logical *)0, (logical *)
	    0, (logical *)0, (logical *)0, (logical *)0, (real *)0, (real *)0,
	     (real *)0, (real *)0, (real *)0, (integer *)0, (char *)0, (
	    integer *)0, (real *)0, (real *)0, (char *)0, (integer *)0, (
	    integer *)0, (integer *)0, (real *)0, (integer *)0, (integer *)0, 
	    (integer *)0, (ftnint)0, (ftnint)0, (ftnint)0, (ftnint)0);
    }

/* Subroutine */ int swgcls_(void)
{
    return swdopn_0_(16, (char *)0, (char *)0, (logical *)0, (integer *)0, (
	    integer *)0, (integer *)0, (integer *)0, (integer *)0, (real *)0, 
	    (real *)0, (integer *)0, (real *)0, (real *)0, (integer *)0, (
	    integer *)0, (integer *)0, (integer *)0, (integer *)0, (real *)0, 
	    (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, 
	    (real *)0, (integer *)0, (integer *)0, (integer *)0, (logical *)0,
	     (logical *)0, (logical *)0, (logical *)0, (logical *)0, (real *)
	    0, (real *)0, (real *)0, (real *)0, (real *)0, (integer *)0, (
	    char *)0, (integer *)0, (real *)0, (real *)0, (char *)0, (integer 
	    *)0, (integer *)0, (integer *)0, (real *)0, (integer *)0, (
	    integer *)0, (integer *)0, (ftnint)0, (ftnint)0, (ftnint)0, (
	    ftnint)0);
    }

/* Subroutine */ int swstcl_(integer *icolor)
{
    return swdopn_0_(17, (char *)0, (char *)0, (logical *)0, (integer *)0, (
	    integer *)0, (integer *)0, icolor, (integer *)0, (real *)0, (real 
	    *)0, (integer *)0, (real *)0, (real *)0, (integer *)0, (integer *)
	    0, (integer *)0, (integer *)0, (integer *)0, (real *)0, (real *)0,
	     (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0,
	     (integer *)0, (integer *)0, (integer *)0, (logical *)0, (logical 
	    *)0, (logical *)0, (logical *)0, (logical *)0, (real *)0, (real *)
	    0, (real *)0, (real *)0, (real *)0, (integer *)0, (char *)0, (
	    integer *)0, (real *)0, (real *)0, (char *)0, (integer *)0, (
	    integer *)0, (integer *)0, (real *)0, (integer *)0, (integer *)0, 
	    (integer *)0, (ftnint)0, (ftnint)0, (ftnint)0, (ftnint)0);
    }

/* Subroutine */ int swgton_(integer *np, real *wpx, real *wpy, integer *
	itpat)
{
    return swdopn_0_(18, (char *)0, (char *)0, (logical *)0, (integer *)0, (
	    integer *)0, (integer *)0, (integer *)0, (integer *)0, (real *)0, 
	    (real *)0, np, wpx, wpy, itpat, (integer *)0, (integer *)0, (
	    integer *)0, (integer *)0, (real *)0, (real *)0, (real *)0, (real 
	    *)0, (real *)0, (real *)0, (real *)0, (real *)0, (integer *)0, (
	    integer *)0, (integer *)0, (logical *)0, (logical *)0, (logical *)
	    0, (logical *)0, (logical *)0, (real *)0, (real *)0, (real *)0, (
	    real *)0, (real *)0, (integer *)0, (char *)0, (integer *)0, (real 
	    *)0, (real *)0, (char *)0, (integer *)0, (integer *)0, (integer *)
	    0, (real *)0, (integer *)0, (integer *)0, (integer *)0, (ftnint)0,
	     (ftnint)0, (ftnint)0, (ftnint)0);
    }

/* Subroutine */ int swiopn_(integer *iwx, integer *iwy, integer *imw, 
	integer *imh, real *wx1, real *wy1, real *wx2, real *wy2, real *wx3, 
	real *wy3, real *wx4, real *wy4)
{
    return swdopn_0_(19, (char *)0, (char *)0, (logical *)0, (integer *)0, (
	    integer *)0, (integer *)0, (integer *)0, (integer *)0, (real *)0, 
	    (real *)0, (integer *)0, (real *)0, (real *)0, (integer *)0, iwx, 
	    iwy, imw, imh, wx1, wy1, wx2, wy2, wx3, wy3, wx4, wy4, (integer *)
	    0, (integer *)0, (integer *)0, (logical *)0, (logical *)0, (
	    logical *)0, (logical *)0, (logical *)0, (real *)0, (real *)0, (
	    real *)0, (real *)0, (real *)0, (integer *)0, (char *)0, (integer 
	    *)0, (real *)0, (real *)0, (char *)0, (integer *)0, (integer *)0, 
	    (integer *)0, (real *)0, (integer *)0, (integer *)0, (integer *)0,
	     (ftnint)0, (ftnint)0, (ftnint)0, (ftnint)0);
    }

/* Subroutine */ int swidat_(integer *image, integer *nlen)
{
    return swdopn_0_(20, (char *)0, (char *)0, (logical *)0, (integer *)0, (
	    integer *)0, (integer *)0, (integer *)0, (integer *)0, (real *)0, 
	    (real *)0, (integer *)0, (real *)0, (real *)0, (integer *)0, (
	    integer *)0, (integer *)0, (integer *)0, (integer *)0, (real *)0, 
	    (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, 
	    (real *)0, image, nlen, (integer *)0, (logical *)0, (logical *)0, 
	    (logical *)0, (logical *)0, (logical *)0, (real *)0, (real *)0, (
	    real *)0, (real *)0, (real *)0, (integer *)0, (char *)0, (integer 
	    *)0, (real *)0, (real *)0, (char *)0, (integer *)0, (integer *)0, 
	    (integer *)0, (real *)0, (integer *)0, (integer *)0, (integer *)0,
	     (ftnint)0, (ftnint)0, (ftnint)0, (ftnint)0);
    }

/* Subroutine */ int swiclr_(integer *image, integer *nlen)
{
    return swdopn_0_(21, (char *)0, (char *)0, (logical *)0, (integer *)0, (
	    integer *)0, (integer *)0, (integer *)0, (integer *)0, (real *)0, 
	    (real *)0, (integer *)0, (real *)0, (real *)0, (integer *)0, (
	    integer *)0, (integer *)0, (integer *)0, (integer *)0, (real *)0, 
	    (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, 
	    (real *)0, image, nlen, (integer *)0, (logical *)0, (logical *)0, 
	    (logical *)0, (logical *)0, (logical *)0, (real *)0, (real *)0, (
	    real *)0, (real *)0, (real *)0, (integer *)0, (char *)0, (integer 
	    *)0, (real *)0, (real *)0, (char *)0, (integer *)0, (integer *)0, 
	    (integer *)0, (real *)0, (integer *)0, (integer *)0, (integer *)0,
	     (ftnint)0, (ftnint)0, (ftnint)0, (ftnint)0);
    }

/* Subroutine */ int swicls_(void)
{
    return swdopn_0_(22, (char *)0, (char *)0, (logical *)0, (integer *)0, (
	    integer *)0, (integer *)0, (integer *)0, (integer *)0, (real *)0, 
	    (real *)0, (integer *)0, (real *)0, (real *)0, (integer *)0, (
	    integer *)0, (integer *)0, (integer *)0, (integer *)0, (real *)0, 
	    (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, 
	    (real *)0, (integer *)0, (integer *)0, (integer *)0, (logical *)0,
	     (logical *)0, (logical *)0, (logical *)0, (logical *)0, (real *)
	    0, (real *)0, (real *)0, (real *)0, (real *)0, (integer *)0, (
	    char *)0, (integer *)0, (real *)0, (real *)0, (char *)0, (integer 
	    *)0, (integer *)0, (integer *)0, (real *)0, (integer *)0, (
	    integer *)0, (integer *)0, (ftnint)0, (ftnint)0, (ftnint)0, (
	    ftnint)0);
    }

/* Subroutine */ int swqpnt_(real *wx, real *wy, integer *mb)
{
    return swdopn_0_(23, (char *)0, (char *)0, (logical *)0, (integer *)0, (
	    integer *)0, (integer *)0, (integer *)0, (integer *)0, wx, wy, (
	    integer *)0, (real *)0, (real *)0, (integer *)0, (integer *)0, (
	    integer *)0, (integer *)0, (integer *)0, (real *)0, (real *)0, (
	    real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (
	    integer *)0, (integer *)0, mb, (logical *)0, (logical *)0, (
	    logical *)0, (logical *)0, (logical *)0, (real *)0, (real *)0, (
	    real *)0, (real *)0, (real *)0, (integer *)0, (char *)0, (integer 
	    *)0, (real *)0, (real *)0, (char *)0, (integer *)0, (integer *)0, 
	    (integer *)0, (real *)0, (integer *)0, (integer *)0, (integer *)0,
	     (ftnint)0, (ftnint)0, (ftnint)0, (ftnint)0);
    }

/* Subroutine */ int swfint_(real *wx, real *wy, integer *iwx, integer *iwy)
{
    return swdopn_0_(24, (char *)0, (char *)0, (logical *)0, (integer *)0, (
	    integer *)0, (integer *)0, (integer *)0, (integer *)0, wx, wy, (
	    integer *)0, (real *)0, (real *)0, (integer *)0, iwx, iwy, (
	    integer *)0, (integer *)0, (real *)0, (real *)0, (real *)0, (real 
	    *)0, (real *)0, (real *)0, (real *)0, (real *)0, (integer *)0, (
	    integer *)0, (integer *)0, (logical *)0, (logical *)0, (logical *)
	    0, (logical *)0, (logical *)0, (real *)0, (real *)0, (real *)0, (
	    real *)0, (real *)0, (integer *)0, (char *)0, (integer *)0, (real 
	    *)0, (real *)0, (char *)0, (integer *)0, (integer *)0, (integer *)
	    0, (real *)0, (integer *)0, (integer *)0, (integer *)0, (ftnint)0,
	     (ftnint)0, (ftnint)0, (ftnint)0);
    }

/* Subroutine */ int swiint_(integer *iwx, integer *iwy, real *wx, real *wy)
{
    return swdopn_0_(25, (char *)0, (char *)0, (logical *)0, (integer *)0, (
	    integer *)0, (integer *)0, (integer *)0, (integer *)0, wx, wy, (
	    integer *)0, (real *)0, (real *)0, (integer *)0, iwx, iwy, (
	    integer *)0, (integer *)0, (real *)0, (real *)0, (real *)0, (real 
	    *)0, (real *)0, (real *)0, (real *)0, (real *)0, (integer *)0, (
	    integer *)0, (integer *)0, (logical *)0, (logical *)0, (logical *)
	    0, (logical *)0, (logical *)0, (real *)0, (real *)0, (real *)0, (
	    real *)0, (real *)0, (integer *)0, (char *)0, (integer *)0, (real 
	    *)0, (real *)0, (char *)0, (integer *)0, (integer *)0, (integer *)
	    0, (real *)0, (integer *)0, (integer *)0, (integer *)0, (ftnint)0,
	     (ftnint)0, (ftnint)0, (ftnint)0);
    }

/* Subroutine */ int swqwdc_(logical *lwdatr)
{
    return swdopn_0_(26, (char *)0, (char *)0, (logical *)0, (integer *)0, (
	    integer *)0, (integer *)0, (integer *)0, (integer *)0, (real *)0, 
	    (real *)0, (integer *)0, (real *)0, (real *)0, (integer *)0, (
	    integer *)0, (integer *)0, (integer *)0, (integer *)0, (real *)0, 
	    (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, 
	    (real *)0, (integer *)0, (integer *)0, (integer *)0, lwdatr, (
	    logical *)0, (logical *)0, (logical *)0, (logical *)0, (real *)0, 
	    (real *)0, (real *)0, (real *)0, (real *)0, (integer *)0, (char *)
	    0, (integer *)0, (real *)0, (real *)0, (char *)0, (integer *)0, (
	    integer *)0, (integer *)0, (real *)0, (integer *)0, (integer *)0, 
	    (integer *)0, (ftnint)0, (ftnint)0, (ftnint)0, (ftnint)0);
    }

/* Subroutine */ int swqclc_(logical *lclatr)
{
    return swdopn_0_(27, (char *)0, (char *)0, (logical *)0, (integer *)0, (
	    integer *)0, (integer *)0, (integer *)0, (integer *)0, (real *)0, 
	    (real *)0, (integer *)0, (real *)0, (real *)0, (integer *)0, (
	    integer *)0, (integer *)0, (integer *)0, (integer *)0, (real *)0, 
	    (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, 
	    (real *)0, (integer *)0, (integer *)0, (integer *)0, (logical *)0,
	     lclatr, (logical *)0, (logical *)0, (logical *)0, (real *)0, (
	    real *)0, (real *)0, (real *)0, (real *)0, (integer *)0, (char *)
	    0, (integer *)0, (real *)0, (real *)0, (char *)0, (integer *)0, (
	    integer *)0, (integer *)0, (real *)0, (integer *)0, (integer *)0, 
	    (integer *)0, (ftnint)0, (ftnint)0, (ftnint)0, (ftnint)0);
    }

/* Subroutine */ int swqtnc_(logical *ltnatr)
{
    return swdopn_0_(28, (char *)0, (char *)0, (logical *)0, (integer *)0, (
	    integer *)0, (integer *)0, (integer *)0, (integer *)0, (real *)0, 
	    (real *)0, (integer *)0, (real *)0, (real *)0, (integer *)0, (
	    integer *)0, (integer *)0, (integer *)0, (integer *)0, (real *)0, 
	    (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, 
	    (real *)0, (integer *)0, (integer *)0, (integer *)0, (logical *)0,
	     (logical *)0, ltnatr, (logical *)0, (logical *)0, (real *)0, (
	    real *)0, (real *)0, (real *)0, (real *)0, (integer *)0, (char *)
	    0, (integer *)0, (real *)0, (real *)0, (char *)0, (integer *)0, (
	    integer *)0, (integer *)0, (real *)0, (integer *)0, (integer *)0, 
	    (integer *)0, (ftnint)0, (ftnint)0, (ftnint)0, (ftnint)0);
    }

/* Subroutine */ int swqimc_(logical *limatr)
{
    return swdopn_0_(29, (char *)0, (char *)0, (logical *)0, (integer *)0, (
	    integer *)0, (integer *)0, (integer *)0, (integer *)0, (real *)0, 
	    (real *)0, (integer *)0, (real *)0, (real *)0, (integer *)0, (
	    integer *)0, (integer *)0, (integer *)0, (integer *)0, (real *)0, 
	    (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, 
	    (real *)0, (integer *)0, (integer *)0, (integer *)0, (logical *)0,
	     (logical *)0, (logical *)0, limatr, (logical *)0, (real *)0, (
	    real *)0, (real *)0, (real *)0, (real *)0, (integer *)0, (char *)
	    0, (integer *)0, (real *)0, (real *)0, (char *)0, (integer *)0, (
	    integer *)0, (integer *)0, (real *)0, (integer *)0, (integer *)0, 
	    (integer *)0, (ftnint)0, (ftnint)0, (ftnint)0, (ftnint)0);
    }

/* Subroutine */ int swqptc_(logical *lptatr)
{
    return swdopn_0_(30, (char *)0, (char *)0, (logical *)0, (integer *)0, (
	    integer *)0, (integer *)0, (integer *)0, (integer *)0, (real *)0, 
	    (real *)0, (integer *)0, (real *)0, (real *)0, (integer *)0, (
	    integer *)0, (integer *)0, (integer *)0, (integer *)0, (real *)0, 
	    (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, 
	    (real *)0, (integer *)0, (integer *)0, (integer *)0, (logical *)0,
	     (logical *)0, (logical *)0, (logical *)0, lptatr, (real *)0, (
	    real *)0, (real *)0, (real *)0, (real *)0, (integer *)0, (char *)
	    0, (integer *)0, (real *)0, (real *)0, (char *)0, (integer *)0, (
	    integer *)0, (integer *)0, (real *)0, (integer *)0, (integer *)0, 
	    (integer *)0, (ftnint)0, (ftnint)0, (ftnint)0, (ftnint)0);
    }

/* Subroutine */ int swqrct_(real *wsxmn, real *wsxmx, real *wsymn, real *
	wsymx, real *fact)
{
    return swdopn_0_(31, (char *)0, (char *)0, (logical *)0, (integer *)0, (
	    integer *)0, (integer *)0, (integer *)0, (integer *)0, (real *)0, 
	    (real *)0, (integer *)0, (real *)0, (real *)0, (integer *)0, (
	    integer *)0, (integer *)0, (integer *)0, (integer *)0, (real *)0, 
	    (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, 
	    (real *)0, (integer *)0, (integer *)0, (integer *)0, (logical *)0,
	     (logical *)0, (logical *)0, (logical *)0, (logical *)0, wsxmn, 
	    wsxmx, wsymn, wsymx, fact, (integer *)0, (char *)0, (integer *)0, 
	    (real *)0, (real *)0, (char *)0, (integer *)0, (integer *)0, (
	    integer *)0, (real *)0, (integer *)0, (integer *)0, (integer *)0, 
	    (ftnint)0, (ftnint)0, (ftnint)0, (ftnint)0);
    }

/* Subroutine */ int swsrot_(integer *iwtrot)
{
    return swdopn_0_(32, (char *)0, (char *)0, (logical *)0, (integer *)0, (
	    integer *)0, (integer *)0, (integer *)0, (integer *)0, (real *)0, 
	    (real *)0, (integer *)0, (real *)0, (real *)0, (integer *)0, (
	    integer *)0, (integer *)0, (integer *)0, (integer *)0, (real *)0, 
	    (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, 
	    (real *)0, (integer *)0, (integer *)0, (integer *)0, (logical *)0,
	     (logical *)0, (logical *)0, (logical *)0, (logical *)0, (real *)
	    0, (real *)0, (real *)0, (real *)0, (real *)0, iwtrot, (char *)0, 
	    (integer *)0, (real *)0, (real *)0, (char *)0, (integer *)0, (
	    integer *)0, (integer *)0, (real *)0, (integer *)0, (integer *)0, 
	    (integer *)0, (ftnint)0, (ftnint)0, (ftnint)0, (ftnint)0);
    }

/* Subroutine */ int swclch_(void)
{
    return swdopn_0_(33, (char *)0, (char *)0, (logical *)0, (integer *)0, (
	    integer *)0, (integer *)0, (integer *)0, (integer *)0, (real *)0, 
	    (real *)0, (integer *)0, (real *)0, (real *)0, (integer *)0, (
	    integer *)0, (integer *)0, (integer *)0, (integer *)0, (real *)0, 
	    (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, 
	    (real *)0, (integer *)0, (integer *)0, (integer *)0, (logical *)0,
	     (logical *)0, (logical *)0, (logical *)0, (logical *)0, (real *)
	    0, (real *)0, (real *)0, (real *)0, (real *)0, (integer *)0, (
	    char *)0, (integer *)0, (real *)0, (real *)0, (char *)0, (integer 
	    *)0, (integer *)0, (integer *)0, (real *)0, (integer *)0, (
	    integer *)0, (integer *)0, (ftnint)0, (ftnint)0, (ftnint)0, (
	    ftnint)0);
    }

/* Subroutine */ int swqtxw_(char *chars, integer *nchz, real *wxch, real *
	wych, ftnlen chars_len)
{
    return swdopn_0_(34, (char *)0, (char *)0, (logical *)0, (integer *)0, (
	    integer *)0, (integer *)0, (integer *)0, (integer *)0, (real *)0, 
	    (real *)0, (integer *)0, (real *)0, (real *)0, (integer *)0, (
	    integer *)0, (integer *)0, (integer *)0, (integer *)0, (real *)0, 
	    (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, 
	    (real *)0, (integer *)0, (integer *)0, (integer *)0, (logical *)0,
	     (logical *)0, (logical *)0, (logical *)0, (logical *)0, (real *)
	    0, (real *)0, (real *)0, (real *)0, (real *)0, (integer *)0, 
	    chars, nchz, wxch, wych, (char *)0, (integer *)0, (integer *)0, (
	    integer *)0, (real *)0, (integer *)0, (integer *)0, (integer *)0, 
	    (ftnint)0, (ftnint)0, chars_len, (ftnint)0);
    }

/* Subroutine */ int swftfc_(char *fontname, ftnlen fontname_len)
{
    return swdopn_0_(35, (char *)0, (char *)0, (logical *)0, (integer *)0, (
	    integer *)0, (integer *)0, (integer *)0, (integer *)0, (real *)0, 
	    (real *)0, (integer *)0, (real *)0, (real *)0, (integer *)0, (
	    integer *)0, (integer *)0, (integer *)0, (integer *)0, (real *)0, 
	    (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, 
	    (real *)0, (integer *)0, (integer *)0, (integer *)0, (logical *)0,
	     (logical *)0, (logical *)0, (logical *)0, (logical *)0, (real *)
	    0, (real *)0, (real *)0, (real *)0, (real *)0, (integer *)0, (
	    char *)0, (integer *)0, (real *)0, (real *)0, fontname, (integer *
	    )0, (integer *)0, (integer *)0, (real *)0, (integer *)0, (integer 
	    *)0, (integer *)0, (ftnint)0, (ftnint)0, (ftnint)0, fontname_len);
    }

/* Subroutine */ int swftnm_(integer *fntnum)
{
    return swdopn_0_(36, (char *)0, (char *)0, (logical *)0, (integer *)0, (
	    integer *)0, (integer *)0, (integer *)0, (integer *)0, (real *)0, 
	    (real *)0, (integer *)0, (real *)0, (real *)0, (integer *)0, (
	    integer *)0, (integer *)0, (integer *)0, (integer *)0, (real *)0, 
	    (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, 
	    (real *)0, (integer *)0, (integer *)0, (integer *)0, (logical *)0,
	     (logical *)0, (logical *)0, (logical *)0, (logical *)0, (real *)
	    0, (real *)0, (real *)0, (real *)0, (real *)0, (integer *)0, (
	    char *)0, (integer *)0, (real *)0, (real *)0, (char *)0, fntnum, (
	    integer *)0, (integer *)0, (real *)0, (integer *)0, (integer *)0, 
	    (integer *)0, (ftnint)0, (ftnint)0, (ftnint)0, (ftnint)0);
    }

/* Subroutine */ int swslft_(char *fontname, ftnlen fontname_len)
{
    return swdopn_0_(37, (char *)0, (char *)0, (logical *)0, (integer *)0, (
	    integer *)0, (integer *)0, (integer *)0, (integer *)0, (real *)0, 
	    (real *)0, (integer *)0, (real *)0, (real *)0, (integer *)0, (
	    integer *)0, (integer *)0, (integer *)0, (integer *)0, (real *)0, 
	    (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, 
	    (real *)0, (integer *)0, (integer *)0, (integer *)0, (logical *)0,
	     (logical *)0, (logical *)0, (logical *)0, (logical *)0, (real *)
	    0, (real *)0, (real *)0, (real *)0, (real *)0, (integer *)0, (
	    char *)0, (integer *)0, (real *)0, (real *)0, fontname, (integer *
	    )0, (integer *)0, (integer *)0, (real *)0, (integer *)0, (integer 
	    *)0, (integer *)0, (ftnint)0, (ftnint)0, (ftnint)0, fontname_len);
    }

/* Subroutine */ int swgtft_(integer *inum, char *fontname, integer *ifmax, 
	ftnlen fontname_len)
{
    return swdopn_0_(38, (char *)0, (char *)0, (logical *)0, (integer *)0, (
	    integer *)0, (integer *)0, (integer *)0, (integer *)0, (real *)0, 
	    (real *)0, (integer *)0, (real *)0, (real *)0, (integer *)0, (
	    integer *)0, (integer *)0, (integer *)0, (integer *)0, (real *)0, 
	    (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, 
	    (real *)0, (integer *)0, (integer *)0, (integer *)0, (logical *)0,
	     (logical *)0, (logical *)0, (logical *)0, (logical *)0, (real *)
	    0, (real *)0, (real *)0, (real *)0, (real *)0, (integer *)0, (
	    char *)0, (integer *)0, (real *)0, (real *)0, fontname, (integer *
	    )0, inum, ifmax, (real *)0, (integer *)0, (integer *)0, (integer *
	    )0, (ftnint)0, (ftnint)0, (ftnint)0, fontname_len);
    }

/* Subroutine */ int swlsft_(void)
{
    return swdopn_0_(39, (char *)0, (char *)0, (logical *)0, (integer *)0, (
	    integer *)0, (integer *)0, (integer *)0, (integer *)0, (real *)0, 
	    (real *)0, (integer *)0, (real *)0, (real *)0, (integer *)0, (
	    integer *)0, (integer *)0, (integer *)0, (integer *)0, (real *)0, 
	    (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, 
	    (real *)0, (integer *)0, (integer *)0, (integer *)0, (logical *)0,
	     (logical *)0, (logical *)0, (logical *)0, (logical *)0, (real *)
	    0, (real *)0, (real *)0, (real *)0, (real *)0, (integer *)0, (
	    char *)0, (integer *)0, (real *)0, (real *)0, (char *)0, (integer 
	    *)0, (integer *)0, (integer *)0, (real *)0, (integer *)0, (
	    integer *)0, (integer *)0, (ftnint)0, (ftnint)0, (ftnint)0, (
	    ftnint)0);
    }

/* Subroutine */ int swtxt_(real *wx, real *wy, real *qsizexrate, char *chars,
	 integer *ncz, integer *irota, integer *icentz, ftnlen chars_len)
{
    return swdopn_0_(40, (char *)0, (char *)0, (logical *)0, (integer *)0, (
	    integer *)0, (integer *)0, (integer *)0, (integer *)0, wx, wy, (
	    integer *)0, (real *)0, (real *)0, (integer *)0, (integer *)0, (
	    integer *)0, (integer *)0, (integer *)0, (real *)0, (real *)0, (
	    real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (
	    integer *)0, (integer *)0, (integer *)0, (logical *)0, (logical *)
	    0, (logical *)0, (logical *)0, (logical *)0, (real *)0, (real *)0,
	     (real *)0, (real *)0, (real *)0, (integer *)0, chars, (integer *)
	    0, (real *)0, (real *)0, (char *)0, (integer *)0, (integer *)0, (
	    integer *)0, qsizexrate, ncz, irota, icentz, (ftnint)0, (ftnint)0,
	     chars_len, (ftnint)0);
    }


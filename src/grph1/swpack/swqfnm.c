/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__2 = 2;
static integer c__3 = 3;

/* *----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int swqfnm_(char *cpara, char *cfname, ftnlen cpara_len, 
	ftnlen cfname_len)
{
    /* System generated locals */
    address a__1[2], a__2[3];
    integer i__1[2], i__2[3];

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen), s_cat(char *,
	     char **, integer *, integer *, ftnlen);

    /* Local variables */
    static integer nf, np;
    static char cx11[4];
    static integer lcp;
    static char cpx[1024];
    static integer iws;
    extern integer lenc_(char *, ftnlen);
    static char cmsg[1024], cpsx[4];
    extern /* Subroutine */ int glcget_(char *, char *, ftnlen, ftnlen), 
	    cfsrch_(char *, integer *, char *, integer *, char *, ftnlen, 
	    ftnlen, ftnlen);
    extern logical lchreq_(char *, char *, ftnlen, ftnlen);
    static char cflist[1024*3];
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), clower_(char *, ftnlen), swcget_(char *, char *, 
	    ftnlen, ftnlen);
    static char cplist[1024*3];
    extern /* Subroutine */ int swiget_(char *, integer *, ftnlen);

    s_copy(cplist, " ", (ftnlen)1024, (ftnlen)1);
    glcget_("DUPATH", cplist + 1024, (ftnlen)6, (ftnlen)1024);
    glcget_("DSPATH", cplist + 2048, (ftnlen)6, (ftnlen)1024);
    np = 3;
    swcget_(cpara, cpx, cpara_len, (ftnlen)1024);
    lcp = lenc_(cpx, (ftnlen)1024);
    swiget_("IWS", &iws, (ftnlen)3);
    s_copy(cx11, ".X11", (ftnlen)4, (ftnlen)4);
    clower_(cx11, (ftnlen)4);
    s_copy(cpsx, ".PSX", (ftnlen)4, (ftnlen)4);
    clower_(cpsx, (ftnlen)4);
    if (lchreq_(cpara, "FONT1", cpara_len, (ftnlen)5) || lchreq_(cpara, "FON"
	    "T2", cpara_len, (ftnlen)5) || lchreq_(cpara, "CL2TN", cpara_len, (
	    ftnlen)5) || lchreq_(cpara, "CMAPLIST", cpara_len, (ftnlen)8)) {
	s_copy(cflist, cpx, (ftnlen)1024, lcp);
	nf = 1;
    } else if (lchreq_(cpara, "CLRMAP", cpara_len, (ftnlen)6) || lchreq_(
	    cpara, "BITMAP", cpara_len, (ftnlen)6)) {
	if (iws == 2 || iws == 3) {
/* Writing concatenation */
	    i__1[0] = lcp, a__1[0] = cpx;
	    i__1[1] = 4, a__1[1] = cpsx;
	    s_cat(cflist, a__1, i__1, &c__2, (ftnlen)1024);
	    s_copy(cflist + 1024, cpx, (ftnlen)1024, lcp);
/* Writing concatenation */
	    i__1[0] = lcp, a__1[0] = cpx;
	    i__1[1] = 4, a__1[1] = cx11;
	    s_cat(cflist + 2048, a__1, i__1, &c__2, (ftnlen)1024);
	} else {
/* Writing concatenation */
	    i__1[0] = lcp, a__1[0] = cpx;
	    i__1[1] = 4, a__1[1] = cx11;
	    s_cat(cflist, a__1, i__1, &c__2, (ftnlen)1024);
	    s_copy(cflist + 1024, cpx, (ftnlen)1024, lcp);
/* Writing concatenation */
	    i__1[0] = lcp, a__1[0] = cpx;
	    i__1[1] = 4, a__1[1] = cpsx;
	    s_cat(cflist + 2048, a__1, i__1, &c__2, (ftnlen)1024);
	}
	nf = 3;
    } else {
/* Writing concatenation */
	i__2[0] = 16, a__2[0] = "PARAMETER NAME <";
	i__2[1] = lenc_(cpara, cpara_len), a__2[1] = cpara;
	i__2[2] = 13, a__2[2] = "> IS INVALID.";
	s_cat(cmsg, a__2, i__2, &c__3, (ftnlen)1024);
	msgdmp_("E", "SWQFNM", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)1024);
    }
    cfsrch_(cplist, &np, cflist, &nf, cfname, (ftnlen)1024, (ftnlen)1024, 
	    cfname_len);
    return 0;
} /* swqfnm_ */


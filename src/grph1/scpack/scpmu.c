/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     POLYMARKER PRIMITIVE */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int scpmu_0_(int n__, integer *n, real *upx, real *upy, real 
	*upz, real *vpx, real *vpy, real *vpz, integer *itype, integer *index,
	 real *rsize)
{
    /* Initialized data */

    static integer itypez = 1;
    static integer indexz = 1;
    static real rsizez = .01f;

    extern /* Subroutine */ int szm3cl_(void), szm3op_(integer *, integer *, 
	    real *), szm3zu_(integer *, real *, real *, real *), szm3zv_(
	    integer *, real *, real *, real *), msgdmp_(char *, char *, char *
	    , ftnlen, ftnlen, ftnlen);

    /* Parameter adjustments */
    if (upx) {
	--upx;
	}
    if (upy) {
	--upy;
	}
    if (upz) {
	--upz;
	}
    if (vpx) {
	--vpx;
	}
    if (vpy) {
	--vpy;
	}
    if (vpz) {
	--vpz;
	}

    /* Function Body */
    switch(n__) {
	case 1: goto L_scpmv;
	case 2: goto L_scspmt;
	case 3: goto L_scqpmt;
	case 4: goto L_scspmi;
	case 5: goto L_scqpmi;
	case 6: goto L_scspms;
	case 7: goto L_scqpms;
	}

    if (*n < 1) {
	msgdmp_("E", "SCPMU", "NUMBER OF POINTS IS LESS THAN 1.", (ftnlen)1, (
		ftnlen)5, (ftnlen)32);
    }
    if (itypez == 0) {
	msgdmp_("M", "SCPMU", "MARKER TYPE IS 0 / DO NOTHING.", (ftnlen)1, (
		ftnlen)5, (ftnlen)30);
	return 0;
    }
    if (indexz == 0) {
	msgdmp_("M", "SCPMU", "POLYMARKER INDEX IS 0 / DO NOTHING.", (ftnlen)
		1, (ftnlen)5, (ftnlen)35);
	return 0;
    }
    if (indexz < 0) {
	msgdmp_("E", "SCPMU", "POLYMARKER INDEX IS LESS THAN 0.", (ftnlen)1, (
		ftnlen)5, (ftnlen)32);
    }
    if (rsizez == 0.f) {
	msgdmp_("M", "SCPMU", "MARKER SIZE IS 0 / DO NOTHING.", (ftnlen)1, (
		ftnlen)5, (ftnlen)30);
	return 0;
    }
    if (rsizez < 0.f) {
	msgdmp_("E", "SCPMU", "MARKER SIZE IS LESS THAN ZERO.", (ftnlen)1, (
		ftnlen)5, (ftnlen)30);
    }
    szm3op_(&itypez, &indexz, &rsizez);
    szm3zu_(n, &upx[1], &upy[1], &upz[1]);
    szm3cl_();
    return 0;
/* ----------------------------------------------------------------------- */

L_scpmv:
    if (*n < 1) {
	msgdmp_("E", "SCPMV", "NUMBER OF POINTS IS LESS THAN 1.", (ftnlen)1, (
		ftnlen)5, (ftnlen)32);
    }
    if (itypez == 0) {
	msgdmp_("M", "SCPMV", "MARKER TYPE IS 0 / DO NOTHING.", (ftnlen)1, (
		ftnlen)5, (ftnlen)30);
	return 0;
    }
    if (indexz == 0) {
	msgdmp_("M", "SCPMV", "POLYMARKER INDEX IS 0 / DO NOTHING.", (ftnlen)
		1, (ftnlen)5, (ftnlen)35);
	return 0;
    }
    if (indexz < 0) {
	msgdmp_("E", "SCPMV", "POLYMARKER INDEX IS LESS THAN 0.", (ftnlen)1, (
		ftnlen)5, (ftnlen)32);
    }
    if (rsizez == 0.f) {
	msgdmp_("M", "SCPMV", "MARKER SIZE IS 0 / DO NOTHING.", (ftnlen)1, (
		ftnlen)5, (ftnlen)30);
	return 0;
    }
    if (rsizez < 0.f) {
	msgdmp_("E", "SCPMV", "MARKER SIZE IS LESS THAN ZERO.", (ftnlen)1, (
		ftnlen)5, (ftnlen)30);
    }
    szm3op_(&itypez, &indexz, &rsizez);
    szm3zv_(n, &vpx[1], &vpy[1], &vpz[1]);
    szm3cl_();
    return 0;
/* ----------------------------------------------------------------------- */

L_scspmt:
    itypez = *itype;
    return 0;
/* ----------------------------------------------------------------------- */

L_scqpmt:
    *itype = itypez;
    return 0;
/* ----------------------------------------------------------------------- */

L_scspmi:
    indexz = *index;
    return 0;
/* ----------------------------------------------------------------------- */

L_scqpmi:
    *index = indexz;
    return 0;
/* ----------------------------------------------------------------------- */

L_scspms:
    rsizez = *rsize;
    return 0;
/* ----------------------------------------------------------------------- */

L_scqpms:
    *rsize = rsizez;
    return 0;
} /* scpmu_ */

/* Subroutine */ int scpmu_(integer *n, real *upx, real *upy, real *upz)
{
    return scpmu_0_(0, n, upx, upy, upz, (real *)0, (real *)0, (real *)0, (
	    integer *)0, (integer *)0, (real *)0);
    }

/* Subroutine */ int scpmv_(integer *n, real *vpx, real *vpy, real *vpz)
{
    return scpmu_0_(1, n, (real *)0, (real *)0, (real *)0, vpx, vpy, vpz, (
	    integer *)0, (integer *)0, (real *)0);
    }

/* Subroutine */ int scspmt_(integer *itype)
{
    return scpmu_0_(2, (integer *)0, (real *)0, (real *)0, (real *)0, (real *)
	    0, (real *)0, (real *)0, itype, (integer *)0, (real *)0);
    }

/* Subroutine */ int scqpmt_(integer *itype)
{
    return scpmu_0_(3, (integer *)0, (real *)0, (real *)0, (real *)0, (real *)
	    0, (real *)0, (real *)0, itype, (integer *)0, (real *)0);
    }

/* Subroutine */ int scspmi_(integer *index)
{
    return scpmu_0_(4, (integer *)0, (real *)0, (real *)0, (real *)0, (real *)
	    0, (real *)0, (real *)0, (integer *)0, index, (real *)0);
    }

/* Subroutine */ int scqpmi_(integer *index)
{
    return scpmu_0_(5, (integer *)0, (real *)0, (real *)0, (real *)0, (real *)
	    0, (real *)0, (real *)0, (integer *)0, index, (real *)0);
    }

/* Subroutine */ int scspms_(real *rsize)
{
    return scpmu_0_(6, (integer *)0, (real *)0, (real *)0, (real *)0, (real *)
	    0, (real *)0, (real *)0, (integer *)0, (integer *)0, rsize);
    }

/* Subroutine */ int scqpms_(real *rsize)
{
    return scpmu_0_(7, (integer *)0, (real *)0, (real *)0, (real *)0, (real *)
	    0, (real *)0, (real *)0, (integer *)0, (integer *)0, rsize);
    }


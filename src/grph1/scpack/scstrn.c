/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int scstrn_0_(int n__, integer *itr3)
{
    extern /* Subroutine */ int sgiget_(char *, integer *, ftnlen), sgiset_(
	    char *, integer *, ftnlen);

    switch(n__) {
	case 1: goto L_scqtrn;
	}

    sgiset_("ITR3", itr3, (ftnlen)4);
    return 0;
/* ----------------------------------------------------------------------- */

L_scqtrn:
    sgiget_("ITR3", itr3, (ftnlen)4);
    return 0;
} /* scstrn_ */

/* Subroutine */ int scstrn_(integer *itr3)
{
    return scstrn_0_(0, itr3);
    }

/* Subroutine */ int scqtrn_(integer *itr3)
{
    return scstrn_0_(1, itr3);
    }


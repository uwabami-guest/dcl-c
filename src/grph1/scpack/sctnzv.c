/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int sctnzv_(real *vpx, real *vpy, real *vpz, integer *itpat1,
	 integer *itpat2)
{
    extern /* Subroutine */ int szt3cl_(void), szt3op_(integer *, integer *), 
	    szt3zv_(real *, real *, real *), msgdmp_(char *, char *, char *, 
	    ftnlen, ftnlen, ftnlen);

    /* Parameter adjustments */
    --vpz;
    --vpy;
    --vpx;

    /* Function Body */
    if (*itpat1 == 0 || *itpat2 == 0) {
	msgdmp_("M", "SCTNZV", "TONE PAT. INDEX IS 0 / DO NOTHING.", (ftnlen)
		1, (ftnlen)6, (ftnlen)34);
	return 0;
    }
    if (*itpat1 < 0 || *itpat2 <= 0) {
	msgdmp_("E", "SCTNZV", "TONE PAT. INDEX IS LESS THAN 0.", (ftnlen)1, (
		ftnlen)6, (ftnlen)31);
    }
    szt3op_(itpat1, itpat2);
    szt3zv_(&vpx[1], &vpy[1], &vpz[1]);
    szt3cl_();
    return 0;
} /* sctnzv_ */


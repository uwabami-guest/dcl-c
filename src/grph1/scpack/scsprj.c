/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__0 = 0;
static real c_b17 = 0.f;
static logical c_false = FALSE_;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int scsprj_(void)
{
    /* System generated locals */
    real r__1, r__2, r__3;

    /* Builtin functions */
    double sqrt(doublereal), atan2(doublereal, doublereal), sin(doublereal);

    /* Local variables */
    static real x0, y0, cp, rr, rz, fac, the, phi, psi, sec3;
    static integer ixc3, iyc3, itr3;
    static logical ldeg;
    extern real rfpi_(void);
    static real xobj, yobj, zobj, xoff, yoff, xeye, yeye, zeye, tilt, angle;
    static integer iwtrf;
    static real rxmin, rymin, rxmax, rymax, wxmin, wymin, wxmax, wymax;
    extern /* Subroutine */ int stspr2_(integer *, integer *, real *), 
	    stspr3_(real *, real *, real *, real *, real *, real *, real *, 
	    real *, real *, real *), scqobj_(real *, real *, real *), sgiget_(
	    char *, integer *, ftnlen), sglget_(char *, logical *, ftnlen), 
	    msgdmp_(char *, char *, char *, ftnlen, ftnlen, ftnlen), scqeye_(
	    real *, real *, real *), sgrget_(char *, real *, ftnlen);
    static logical lprjct;
    extern /* Subroutine */ int swlget_(char *, logical *, ftnlen), swlset_(
	    char *, logical *, ftnlen), stqwtr_(real *, real *, real *, real *
	    , real *, real *, real *, real *, integer *);
    static logical lsysfnt;

    scqobj_(&xobj, &yobj, &zobj);
    scqeye_(&xeye, &yeye, &zeye);
    sgiget_("ITR3", &itr3, (ftnlen)4);
    sgrget_("XOFF3", &xoff, (ftnlen)5);
    sgrget_("YOFF3", &yoff, (ftnlen)5);
    sgrget_("TILT3", &tilt, (ftnlen)5);
    sgrget_("ANGLE3", &angle, (ftnlen)6);
    sglget_("LDEG", &ldeg, (ftnlen)4);
    if (ldeg) {
	cp = rfpi_() / 180;
    } else {
	cp = 1.f;
    }
/* Computing 2nd power */
    r__1 = xeye - xobj;
/* Computing 2nd power */
    r__2 = yeye - yobj;
    rz = sqrt(r__1 * r__1 + r__2 * r__2);
/* Computing 2nd power */
    r__1 = xeye - xobj;
/* Computing 2nd power */
    r__2 = yeye - yobj;
/* Computing 2nd power */
    r__3 = zeye - zobj;
    rr = sqrt(r__1 * r__1 + r__2 * r__2 + r__3 * r__3);
    the = atan2(rz, zeye - zobj);
    phi = atan2(yeye - yobj, xeye - xobj);
    psi = rfpi_() / 2 - cp * tilt;
    stqwtr_(&rxmin, &rxmax, &rymin, &rymax, &wxmin, &wxmax, &wymin, &wymax, &
	    iwtrf);
    x0 = (rxmin + rxmax) / 2 + xoff;
    y0 = (rymin + rymax) / 2 + yoff;
    if (angle == 0.f) {
	msgdmp_("E", "SCSPRJ", "ANGLE MUST NOT BE ZERO.", (ftnlen)1, (ftnlen)
		6, (ftnlen)23);
    } else {
	fac = (r__1 = .5f / (rr * sin(cp * angle / 2.f)), abs(r__1));
    }
    if (angle <= 0.f) {
	rr = -rr;
    }
    stspr3_(&xobj, &yobj, &zobj, &the, &phi, &psi, &fac, &rr, &x0, &y0);
/*     / 2-D PROJECTION / */
    sglget_("L2TO3", &lprjct, (ftnlen)5);
    if (lprjct) {
	sgiget_("IXC3", &ixc3, (ftnlen)4);
	sgiget_("IYC3", &iyc3, (ftnlen)4);
	sgrget_("SEC3", &sec3, (ftnlen)4);
	stspr2_(&ixc3, &iyc3, &sec3);
    } else {
	stspr2_(&c__0, &c__0, &c_b17);
    }
    swlget_("LSYSFNT", &lsysfnt, (ftnlen)7);
    if (lsysfnt) {
	msgdmp_("W", "SCSPRJ", "SYSTEM FONT IS NOT SUPPORTED IN SCPACK.", (
		ftnlen)1, (ftnlen)6, (ftnlen)39);
	msgdmp_("W", "SCSPRJ", "NOTE : LSYSFNT IS SET TO FALSE.", (ftnlen)1, (
		ftnlen)6, (ftnlen)31);
	swlset_("LSYSFNT", &c_false, (ftnlen)7);
    }
    return 0;
} /* scsprj_ */


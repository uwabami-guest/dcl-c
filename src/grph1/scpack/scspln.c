/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static logical c_true = TRUE_;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int scspln_0_(int n__, integer *ixax, integer *iyax, real *
	sect)
{
    extern /* Subroutine */ int sgiget_(char *, integer *, ftnlen), sgrget_(
	    char *, real *, ftnlen), sgiset_(char *, integer *, ftnlen), 
	    sglset_(char *, logical *, ftnlen), sgrset_(char *, real *, 
	    ftnlen);

    switch(n__) {
	case 1: goto L_scqpln;
	}

    sgiset_("IXC3", ixax, (ftnlen)4);
    sgiset_("IYC3", iyax, (ftnlen)4);
    sgrset_("SEC3", sect, (ftnlen)4);
    sglset_("L2TO3", &c_true, (ftnlen)5);
    return 0;
/* ----------------------------------------------------------------------- */

L_scqpln:
    sgiget_("IXC3", ixax, (ftnlen)4);
    sgiget_("IYC3", iyax, (ftnlen)4);
    sgrget_("SEC3", sect, (ftnlen)4);
    return 0;
} /* scspln_ */

/* Subroutine */ int scspln_(integer *ixax, integer *iyax, real *sect)
{
    return scspln_0_(0, ixax, iyax, sect);
    }

/* Subroutine */ int scqpln_(integer *ixax, integer *iyax, real *sect)
{
    return scspln_0_(1, ixax, iyax, sect);
    }


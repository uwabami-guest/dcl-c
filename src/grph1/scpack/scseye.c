/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int scseye_0_(int n__, real *xeye3, real *yeye3, real *zeye3)
{
    extern /* Subroutine */ int sgrget_(char *, real *, ftnlen), sgrset_(char 
	    *, real *, ftnlen);

    switch(n__) {
	case 1: goto L_scqeye;
	}

    sgrset_("XEYE3", xeye3, (ftnlen)5);
    sgrset_("YEYE3", yeye3, (ftnlen)5);
    sgrset_("ZEYE3", zeye3, (ftnlen)5);
    return 0;
/* ----------------------------------------------------------------------- */

L_scqeye:
    sgrget_("XEYE3", xeye3, (ftnlen)5);
    sgrget_("YEYE3", yeye3, (ftnlen)5);
    sgrget_("ZEYE3", zeye3, (ftnlen)5);
    return 0;
} /* scseye_ */

/* Subroutine */ int scseye_(real *xeye3, real *yeye3, real *zeye3)
{
    return scseye_0_(0, xeye3, yeye3, zeye3);
    }

/* Subroutine */ int scqeye_(real *xeye3, real *yeye3, real *zeye3)
{
    return scseye_0_(1, xeye3, yeye3, zeye3);
    }


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     TONE PRIMITIVE */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int sctnu_0_(int n__, real *upx, real *upy, real *upz, real *
	vpx, real *vpy, real *vpz, integer *itpat1, integer *itpat2)
{
    /* Initialized data */

    static integer itpt1z = 1;
    static integer itpt2z = 1;

    extern /* Subroutine */ int szt3cl_(void), szt3op_(integer *, integer *), 
	    szt3zu_(real *, real *, real *), szt3zv_(real *, real *, real *), 
	    msgdmp_(char *, char *, char *, ftnlen, ftnlen, ftnlen);

    /* Parameter adjustments */
    if (upx) {
	--upx;
	}
    if (upy) {
	--upy;
	}
    if (upz) {
	--upz;
	}
    if (vpx) {
	--vpx;
	}
    if (vpy) {
	--vpy;
	}
    if (vpz) {
	--vpz;
	}

    /* Function Body */
    switch(n__) {
	case 1: goto L_sctnv;
	case 2: goto L_scstnp;
	case 3: goto L_scqtnp;
	}

    if (itpt1z == 0 || itpt2z == 0) {
	msgdmp_("M", "SCTNU", "TONE PAT. INDEX IS 0 / DO NOTHING.", (ftnlen)1,
		 (ftnlen)5, (ftnlen)34);
	return 0;
    }
    if (itpt1z < 0 || itpt2z < 0) {
	msgdmp_("E", "SCTNU", "TONE PAT. INDEX IS LESS THAN 0.", (ftnlen)1, (
		ftnlen)5, (ftnlen)31);
    }
    szt3op_(&itpt1z, &itpt2z);
    szt3zu_(&upx[1], &upy[1], &upz[1]);
    szt3cl_();
    return 0;
/* ----------------------------------------------------------------------- */

L_sctnv:
    if (itpt1z == 0 || itpt2z == 0) {
	msgdmp_("M", "SCTNV", "TONE PAT. INDEX IS 0 / DO NOTHING.", (ftnlen)1,
		 (ftnlen)5, (ftnlen)34);
	return 0;
    }
    if (itpt1z < 0 || itpt2z < 0) {
	msgdmp_("E", "SCTNV", "TONE PAT. INDEX IS LESS THAN 0.", (ftnlen)1, (
		ftnlen)5, (ftnlen)31);
    }
    szt3op_(&itpt1z, &itpt2z);
    szt3zv_(&vpx[1], &vpy[1], &vpz[1]);
    szt3cl_();
    return 0;
/* ----------------------------------------------------------------------- */

L_scstnp:
    itpt1z = *itpat1;
    itpt2z = *itpat2;
    return 0;
/* ----------------------------------------------------------------------- */

L_scqtnp:
    *itpat1 = itpt1z;
    *itpat2 = itpt2z;
    return 0;
} /* sctnu_ */

/* Subroutine */ int sctnu_(real *upx, real *upy, real *upz)
{
    return sctnu_0_(0, upx, upy, upz, (real *)0, (real *)0, (real *)0, (
	    integer *)0, (integer *)0);
    }

/* Subroutine */ int sctnv_(real *vpx, real *vpy, real *vpz)
{
    return sctnu_0_(1, (real *)0, (real *)0, (real *)0, vpx, vpy, vpz, (
	    integer *)0, (integer *)0);
    }

/* Subroutine */ int scstnp_(integer *itpat1, integer *itpat2)
{
    return sctnu_0_(2, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, 
	    (real *)0, itpat1, itpat2);
    }

/* Subroutine */ int scqtnp_(integer *itpat1, integer *itpat2)
{
    return sctnu_0_(3, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, 
	    (real *)0, itpat1, itpat2);
    }


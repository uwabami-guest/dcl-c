/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     POLYLINE PRIMITIVE */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int scplu_0_(int n__, integer *n, real *upx, real *upy, real 
	*upz, real *vpx, real *vpy, real *vpz, integer *index)
{
    /* Initialized data */

    static integer indexz = 1;

    extern /* Subroutine */ int szl3cl_(void), szl3op_(integer *), szl3zu_(
	    integer *, real *, real *, real *), szl3zv_(integer *, real *, 
	    real *, real *), msgdmp_(char *, char *, char *, ftnlen, ftnlen, 
	    ftnlen);

    /* Parameter adjustments */
    if (upx) {
	--upx;
	}
    if (upy) {
	--upy;
	}
    if (upz) {
	--upz;
	}
    if (vpx) {
	--vpx;
	}
    if (vpy) {
	--vpy;
	}
    if (vpz) {
	--vpz;
	}

    /* Function Body */
    switch(n__) {
	case 1: goto L_scplv;
	case 2: goto L_scspli;
	case 3: goto L_scqpli;
	}

    if (*n < 2) {
	msgdmp_("E", "SCPLU", "NUMBER OF POINTS IS LESS THAN 2.", (ftnlen)1, (
		ftnlen)5, (ftnlen)32);
    }
    if (indexz == 0) {
	msgdmp_("M", "SCPLU", "POLYLINE INDEX IS 0 / DO NOTHING.", (ftnlen)1, 
		(ftnlen)5, (ftnlen)33);
	return 0;
    }
    if (indexz < 0) {
	msgdmp_("E", "SCPLU", "POLYLINE INDEX IS LESS THAN 0.", (ftnlen)1, (
		ftnlen)5, (ftnlen)30);
    }
    szl3op_(&indexz);
    szl3zu_(n, &upx[1], &upy[1], &upz[1]);
    szl3cl_();
    return 0;
/* ----------------------------------------------------------------------- */

L_scplv:
    if (*n < 2) {
	msgdmp_("E", "SCPLV", "NUMBER OF POINTS IS LESS THAN 2.", (ftnlen)1, (
		ftnlen)5, (ftnlen)32);
    }
    if (indexz == 0) {
	msgdmp_("M", "SCPLV", "POLYLINE INDEX IS 0 / DO NOTHING.", (ftnlen)1, 
		(ftnlen)5, (ftnlen)33);
	return 0;
    }
    if (indexz < 0) {
	msgdmp_("E", "SCPLV", "POLYLINE INDEX IS LESS THAN 0.", (ftnlen)1, (
		ftnlen)5, (ftnlen)30);
    }
    szl3op_(&indexz);
    szl3zv_(n, &vpx[1], &vpy[1], &vpz[1]);
    szl3cl_();
    return 0;
/* ----------------------------------------------------------------------- */

L_scspli:
    indexz = *index;
    return 0;
/* ----------------------------------------------------------------------- */

L_scqpli:
    *index = indexz;
    return 0;
} /* scplu_ */

/* Subroutine */ int scplu_(integer *n, real *upx, real *upy, real *upz)
{
    return scplu_0_(0, n, upx, upy, upz, (real *)0, (real *)0, (real *)0, (
	    integer *)0);
    }

/* Subroutine */ int scplv_(integer *n, real *vpx, real *vpy, real *vpz)
{
    return scplu_0_(1, n, (real *)0, (real *)0, (real *)0, vpx, vpy, vpz, (
	    integer *)0);
    }

/* Subroutine */ int scspli_(integer *index)
{
    return scplu_0_(2, (integer *)0, (real *)0, (real *)0, (real *)0, (real *)
	    0, (real *)0, (real *)0, index);
    }

/* Subroutine */ int scqpli_(integer *index)
{
    return scplu_0_(3, (integer *)0, (real *)0, (real *)0, (real *)0, (real *)
	    0, (real *)0, (real *)0, index);
    }


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int scswnd_0_(int n__, real *uxmin, real *uxmax, real *uymin,
	 real *uymax, real *uzmin, real *uzmax)
{
    extern /* Subroutine */ int sgrget_(char *, real *, ftnlen), sgrset_(char 
	    *, real *, ftnlen);

    switch(n__) {
	case 1: goto L_scqwnd;
	}

    sgrset_("UXMIN3", uxmin, (ftnlen)6);
    sgrset_("UXMAX3", uxmax, (ftnlen)6);
    sgrset_("UYMIN3", uymin, (ftnlen)6);
    sgrset_("UYMAX3", uymax, (ftnlen)6);
    sgrset_("UZMIN3", uzmin, (ftnlen)6);
    sgrset_("UZMAX3", uzmax, (ftnlen)6);
    return 0;
/* ----------------------------------------------------------------------- */

L_scqwnd:
    sgrget_("UXMIN3", uxmin, (ftnlen)6);
    sgrget_("UXMAX3", uxmax, (ftnlen)6);
    sgrget_("UYMIN3", uymin, (ftnlen)6);
    sgrget_("UYMAX3", uymax, (ftnlen)6);
    sgrget_("UZMIN3", uzmin, (ftnlen)6);
    sgrget_("UZMAX3", uzmax, (ftnlen)6);
    return 0;
} /* scswnd_ */

/* Subroutine */ int scswnd_(real *uxmin, real *uxmax, real *uymin, real *
	uymax, real *uzmin, real *uzmax)
{
    return scswnd_0_(0, uxmin, uxmax, uymin, uymax, uzmin, uzmax);
    }

/* Subroutine */ int scqwnd_(real *uxmin, real *uxmax, real *uymin, real *
	uymax, real *uzmin, real *uzmax)
{
    return scswnd_0_(1, uxmin, uxmax, uymin, uymax, uzmin, uzmax);
    }


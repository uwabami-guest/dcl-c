/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int scpmzu_(integer *n, real *upx, real *upy, real *upz, 
	integer *itype, integer *index, real *rsize)
{
    extern /* Subroutine */ int szm3cl_(void), szm3op_(integer *, integer *, 
	    real *), szm3zu_(integer *, real *, real *, real *), msgdmp_(char 
	    *, char *, char *, ftnlen, ftnlen, ftnlen);

    /* Parameter adjustments */
    --upz;
    --upy;
    --upx;

    /* Function Body */
    if (*n < 1) {
	msgdmp_("E", "SCPMZU", "NUMBER OF POINTS IS LESS THAN 1.", (ftnlen)1, 
		(ftnlen)6, (ftnlen)32);
    }
    if (*itype == 0) {
	msgdmp_("M", "SCPMZU", "MARKER TYPE IS 0 / DO NOTHING.", (ftnlen)1, (
		ftnlen)6, (ftnlen)30);
	return 0;
    }
    if (*index == 0) {
	msgdmp_("M", "SCPMZU", "POLYMARKER INDEX IS 0 / DO NOTHING.", (ftnlen)
		1, (ftnlen)6, (ftnlen)35);
	return 0;
    }
    if (*index < 0) {
	msgdmp_("E", "SCPMZU", "POLYMARKER INDEX IS LESS THAN 0.", (ftnlen)1, 
		(ftnlen)6, (ftnlen)32);
    }
    if (*rsize == 0.f) {
	msgdmp_("M", "SCPMZU", "MARKER SIZE IS 0 / DO NOTHING.", (ftnlen)1, (
		ftnlen)6, (ftnlen)30);
	return 0;
    }
    if (*rsize < 0.f) {
	msgdmp_("E", "SCPMZU", "MARKER SIZE IS LESS THAN ZERO.", (ftnlen)1, (
		ftnlen)6, (ftnlen)30);
    }
    szm3op_(itype, index, rsize);
    szm3zu_(n, &upx[1], &upy[1], &upz[1]);
    szm3cl_();
    return 0;
} /* scpmzu_ */


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     3-D NORMALIZATION TRANSFORMATION */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int scsvpt_0_(int n__, real *vxmin, real *vxmax, real *vymin,
	 real *vymax, real *vzmin, real *vzmax)
{
    extern /* Subroutine */ int sgrget_(char *, real *, ftnlen), sgrset_(char 
	    *, real *, ftnlen);

    switch(n__) {
	case 1: goto L_scqvpt;
	}

    sgrset_("VXMIN3", vxmin, (ftnlen)6);
    sgrset_("VXMAX3", vxmax, (ftnlen)6);
    sgrset_("VYMIN3", vymin, (ftnlen)6);
    sgrset_("VYMAX3", vymax, (ftnlen)6);
    sgrset_("VZMIN3", vzmin, (ftnlen)6);
    sgrset_("VZMAX3", vzmax, (ftnlen)6);
    return 0;
/* ----------------------------------------------------------------------- */

L_scqvpt:
    sgrget_("VXMIN3", vxmin, (ftnlen)6);
    sgrget_("VXMAX3", vxmax, (ftnlen)6);
    sgrget_("VYMIN3", vymin, (ftnlen)6);
    sgrget_("VYMAX3", vymax, (ftnlen)6);
    sgrget_("VZMIN3", vzmin, (ftnlen)6);
    sgrget_("VZMAX3", vzmax, (ftnlen)6);
    return 0;
} /* scsvpt_ */

/* Subroutine */ int scsvpt_(real *vxmin, real *vxmax, real *vymin, real *
	vymax, real *vzmin, real *vzmax)
{
    return scsvpt_0_(0, vxmin, vxmax, vymin, vymax, vzmin, vzmax);
    }

/* Subroutine */ int scqvpt_(real *vxmin, real *vxmax, real *vymin, real *
	vymax, real *vzmin, real *vzmax)
{
    return scsvpt_0_(1, vxmin, vxmax, vymin, vymax, vzmin, vzmax);
    }


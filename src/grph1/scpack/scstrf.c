/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static logical c_false = FALSE_;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int scstrf_(void)
{
    /* System generated locals */
    real r__1;

    /* Builtin functions */
    double r_lg10(real *);

    /* Local variables */
    static real cx, cy, cz, vx0, vy0, vz0, fac;
    static integer itr3;
    static logical ldeg, lxlog, lylog, lzlog;
    static real uxmin, vxmin, vymin, vxmax, vymax, vzmin, vzmax, uxmax, uymin,
	     uymax, uzmin, uzmax;
    extern /* Subroutine */ int stslg3_(logical *, logical *, logical *), 
	    stsrd3_(logical *, logical *, logical *), ststr3_(integer *, real 
	    *, real *, real *, real *, real *, real *), sgiget_(char *, 
	    integer *, ftnlen), sglget_(char *, logical *, ftnlen), msgdmp_(
	    char *, char *, char *, ftnlen, ftnlen, ftnlen), scqlog_(logical *
	    , logical *, logical *), sgrget_(char *, real *, ftnlen), scqwnd_(
	    real *, real *, real *, real *, real *, real *), scqvpt_(real *, 
	    real *, real *, real *, real *, real *);

    sgiget_("ITR3", &itr3, (ftnlen)4);
    sglget_("LDEG", &ldeg, (ftnlen)4);
    scqlog_(&lxlog, &lylog, &lzlog);
    if (itr3 == 1) {
/*       / LINEAR AND LOG / */
	scqvpt_(&vxmin, &vxmax, &vymin, &vymax, &vzmin, &vzmax);
	if (! (vxmin < vxmax && vymin < vymax && vzmin < vzmax)) {
	    msgdmp_("E", "SCSTRF", "VIEWPORT DEFINITION IS INVALID.", (ftnlen)
		    1, (ftnlen)6, (ftnlen)31);
	}
	scqwnd_(&uxmin, &uxmax, &uymin, &uymax, &uzmin, &uzmax);
	if (! lxlog) {
	    cx = (vxmax - vxmin) / (uxmax - uxmin);
	    vx0 = vxmin - cx * uxmin;
	} else {
	    if (uxmin * uxmax <= 0.f) {
		msgdmp_("E", "SCSTRF", "THE REGION STRADDLES 0 FOR LOG TRANS"
			"FORMATION (X).", (ftnlen)1, (ftnlen)6, (ftnlen)50);
	    }
	    r__1 = uxmax / uxmin;
	    cx = (vxmax - vxmin) / r_lg10(&r__1);
	    r__1 = abs(uxmin);
	    vx0 = vxmin - cx * r_lg10(&r__1);
	}
	if (! lylog) {
	    cy = (vymax - vymin) / (uymax - uymin);
	    vy0 = vymin - cy * uymin;
	} else {
	    if (uymin * uymax <= 0.f) {
		msgdmp_("E", "SCSTRF", "THE REGION STRADDLES 0 FOR LOG TRANS"
			"FORMATION (Y).", (ftnlen)1, (ftnlen)6, (ftnlen)50);
	    }
	    r__1 = uymax / uymin;
	    cy = (vymax - vymin) / r_lg10(&r__1);
	    r__1 = abs(uymin);
	    vy0 = vymin - cy * r_lg10(&r__1);
	}
	if (! lzlog) {
	    cz = (vzmax - vzmin) / (uzmax - uzmin);
	    vz0 = vzmin - cz * uzmin;
	} else {
	    if (uzmin * uzmax <= 0.f) {
		msgdmp_("E", "SCSTRF", "THE REGION STRADDLES 0 FOR LOG TRANS"
			"FORMATION (Z).", (ftnlen)1, (ftnlen)6, (ftnlen)50);
	    }
	    r__1 = uzmax / uzmin;
	    cz = (vzmax - vzmin) / r_lg10(&r__1);
	    r__1 = abs(uzmin);
	    vz0 = vzmin - cz * r_lg10(&r__1);
	}
	ststr3_(&itr3, &cx, &cy, &cz, &vx0, &vy0, &vz0);
	stslg3_(&lxlog, &lylog, &lzlog);
	stsrd3_(&c_false, &c_false, &c_false);
    } else if (itr3 == 2) {
/*       / CYLINDRICAL / */
	sgrget_("SIMFAC3", &fac, (ftnlen)7);
	sgrget_("VXORG3", &vx0, (ftnlen)6);
	sgrget_("VYORG3", &vy0, (ftnlen)6);
	sgrget_("VZORG3", &vz0, (ftnlen)6);
	ststr3_(&itr3, &fac, &fac, &fac, &vx0, &vy0, &vz0);
	stslg3_(&c_false, &c_false, &c_false);
	stsrd3_(&c_false, &ldeg, &c_false);
    } else if (itr3 == 3) {
/*       / SPHERICAL / */
	sgrget_("SIMFAC3", &fac, (ftnlen)7);
	sgrget_("VXORG3", &vx0, (ftnlen)6);
	sgrget_("VYORG3", &vy0, (ftnlen)6);
	sgrget_("VZORG3", &vz0, (ftnlen)6);
	ststr3_(&itr3, &fac, &fac, &fac, &vx0, &vy0, &vz0);
	stslg3_(&c_false, &c_false, &c_false);
	stsrd3_(&c_false, &ldeg, &ldeg);
    } else {
	msgdmp_("E", "SCSTRF", "TRANSFORMATION FUNCTION NUMBER IS INVALID.", (
		ftnlen)1, (ftnlen)6, (ftnlen)42);
    }
    return 0;
} /* scstrf_ */


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int scsorg_0_(int n__, real *simfac, real *vxorg3, real *
	vyorg3, real *vzorg3)
{
    extern /* Subroutine */ int sgrget_(char *, real *, ftnlen), sgrset_(char 
	    *, real *, ftnlen);

    switch(n__) {
	case 1: goto L_scqorg;
	}

    sgrset_("SIMFAC3", simfac, (ftnlen)7);
    sgrset_("VXORG3", vxorg3, (ftnlen)6);
    sgrset_("VYORG3", vyorg3, (ftnlen)6);
    sgrset_("VZORG3", vzorg3, (ftnlen)6);
    return 0;
/* ----------------------------------------------------------------------- */

L_scqorg:
    sgrget_("SIMFAC3", simfac, (ftnlen)7);
    sgrget_("VXORG3", vxorg3, (ftnlen)6);
    sgrget_("VYORG3", vyorg3, (ftnlen)6);
    sgrget_("VZORG3", vzorg3, (ftnlen)6);
    return 0;
} /* scsorg_ */

/* Subroutine */ int scsorg_(real *simfac, real *vxorg3, real *vyorg3, real *
	vzorg3)
{
    return scsorg_0_(0, simfac, vxorg3, vyorg3, vzorg3);
    }

/* Subroutine */ int scqorg_(real *simfac, real *vxorg3, real *vyorg3, real *
	vzorg3)
{
    return scsorg_0_(1, simfac, vxorg3, vyorg3, vzorg3);
    }


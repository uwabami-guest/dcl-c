/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int suswnd_(real *uxmin, real *uxmax, real *uymin, real *
	uymax, integer *intrf)
{
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), sgswnd_(real *, real *, real *, real *), sgstrf_(
	    void), sgstrn_(integer *);

    msgdmp_("M", "SUSWND", "THIS IS OLD INTERFACE - USE SGSWND !", (ftnlen)1, 
	    (ftnlen)6, (ftnlen)36);
    sgswnd_(uxmin, uxmax, uymin, uymax);
    sgstrn_(intrf);
    sgstrf_();
    return 0;
} /* suswnd_ */


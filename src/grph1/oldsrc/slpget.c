/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     SLPACK */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int slpget_0_(int n__, char *cp, integer *ipara, ftnlen 
	cp_len)
{
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), sgpget_(char *, integer *, ftnlen), sgpset_(char 
	    *, integer *, ftnlen);

    switch(n__) {
	case 1: goto L_slpset;
	}

    msgdmp_("M", "SLPGET", "THIS IS OLD INTERFACE - USE SGPGET !", (ftnlen)1, 
	    (ftnlen)6, (ftnlen)36);
    sgpget_(cp, ipara, cp_len);
    return 0;
/* ----------------------------------------------------------------------- */

L_slpset:
    msgdmp_("M", "SLPSET", "THIS IS OLD INTERFACE - USE SGPSET !", (ftnlen)1, 
	    (ftnlen)6, (ftnlen)36);
    sgpset_(cp, ipara, cp_len);
    return 0;
} /* slpget_ */

/* Subroutine */ int slpget_(char *cp, integer *ipara, ftnlen cp_len)
{
    return slpget_0_(0, cp, ipara, cp_len);
    }

/* Subroutine */ int slpset_(char *cp, integer *ipara, ftnlen cp_len)
{
    return slpget_0_(1, cp, ipara, cp_len);
    }


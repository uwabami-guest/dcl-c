/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     CLIPPING (OLD INTERFACE, BUT EFFICIENT) */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int sgsclp_0_(int n__, integer *iclip)
{
    /* System generated locals */
    logical L__1;

    /* Local variables */
    static logical lclip;
    extern /* Subroutine */ int sglget_(char *, logical *, ftnlen), msgdmp_(
	    char *, char *, char *, ftnlen, ftnlen, ftnlen), sglset_(char *, 
	    logical *, ftnlen);

    switch(n__) {
	case 1: goto L_sgqclp;
	}

    msgdmp_("M", "SGSCLP", "THIS IS OLD INTERFACE - USE SGLSET !", (ftnlen)1, 
	    (ftnlen)6, (ftnlen)36);
    if (! (0 <= *iclip && *iclip <= 1)) {
	msgdmp_("E", "SGSCLP", "CLIPPING INDICATOR IS INVALID.", (ftnlen)1, (
		ftnlen)6, (ftnlen)30);
    }
    L__1 = *iclip == 1;
    sglset_("LCLIP", &L__1, (ftnlen)5);
    return 0;
/* ----------------------------------------------------------------------- */

L_sgqclp:
    msgdmp_("M", "SGQCLP", "THIS IS OLD INTERFACE - USE SGLGET !", (ftnlen)1, 
	    (ftnlen)6, (ftnlen)36);
    sglget_("LCLIP", &lclip, (ftnlen)5);
    if (lclip) {
	*iclip = 1;
    } else {
	*iclip = 0;
    }
    return 0;
} /* sgsclp_ */

/* Subroutine */ int sgsclp_(integer *iclip)
{
    return sgsclp_0_(0, iclip);
    }

/* Subroutine */ int sgqclp_(integer *iclip)
{
    return sgsclp_0_(1, iclip);
    }


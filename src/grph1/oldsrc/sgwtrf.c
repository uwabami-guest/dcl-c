/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int sgwtrf_0_(int n__, real *vx, real *vy, real *wx, real *
	wy, real *vxmin, real *vxmax, real *vymin, real *vymax, real *wxmin, 
	real *wxmax, real *wymin, real *wymax, integer *iwtrf, real *wsxmn, 
	real *wsxmx, real *wsymn, real *wsymx)
{
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), stqwrc_(real *, real *, real *, real *), stswrc_(
	    real *, real *, real *, real *), stfwtr_(real *, real *, real *, 
	    real *), stqwtr_(real *, real *, real *, real *, real *, real *, 
	    real *, real *, integer *), stswtr_(real *, real *, real *, real *
	    , real *, real *, real *, real *, integer *);

    switch(n__) {
	case 1: goto L_sgswtr;
	case 2: goto L_sgqwtr;
	case 3: goto L_sgswrc;
	case 4: goto L_sgqwrc;
	}

    msgdmp_("M", "SGWTRF", "THIS IS OLD INTERFACE - USE STFWTR !", (ftnlen)1, 
	    (ftnlen)6, (ftnlen)36);
    stfwtr_(vx, vy, wx, wy);
    return 0;
/* ----------------------------------------------------------------------- */

L_sgswtr:
    msgdmp_("M", "SGSWTR", "THIS IS OLD INTERFACE - USE STSWTR !", (ftnlen)1, 
	    (ftnlen)6, (ftnlen)36);
    stswtr_(vxmin, vxmax, vymin, vymax, wxmin, wxmax, wymin, wymax, iwtrf);
    return 0;
/* ----------------------------------------------------------------------- */

L_sgqwtr:
    msgdmp_("M", "SGQWTR", "THIS IS OLD INTERFACE - USE STQWTR !", (ftnlen)1, 
	    (ftnlen)6, (ftnlen)36);
    stqwtr_(vxmin, vxmax, vymin, vymax, wxmin, wxmax, wymin, wymax, iwtrf);
    return 0;
/* ----------------------------------------------------------------------- */

L_sgswrc:
    msgdmp_("M", "SGSWRC", "THIS IS OLD INTERFACE - USE STSWRC !", (ftnlen)1, 
	    (ftnlen)6, (ftnlen)36);
    stswrc_(wsxmn, wsxmx, wsymn, wsymx);
    return 0;
/* ----------------------------------------------------------------------- */

L_sgqwrc:
    msgdmp_("M", "SGQWRC", "THIS IS OLD INTERFACE - USE STQWRC !", (ftnlen)1, 
	    (ftnlen)6, (ftnlen)36);
    stqwrc_(wsxmn, wsxmx, wsymn, wsymx);
    return 0;
} /* sgwtrf_ */

/* Subroutine */ int sgwtrf_(real *vx, real *vy, real *wx, real *wy)
{
    return sgwtrf_0_(0, vx, vy, wx, wy, (real *)0, (real *)0, (real *)0, (
	    real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (integer *)
	    0, (real *)0, (real *)0, (real *)0, (real *)0);
    }

/* Subroutine */ int sgswtr_(real *vxmin, real *vxmax, real *vymin, real *
	vymax, real *wxmin, real *wxmax, real *wymin, real *wymax, integer *
	iwtrf)
{
    return sgwtrf_0_(1, (real *)0, (real *)0, (real *)0, (real *)0, vxmin, 
	    vxmax, vymin, vymax, wxmin, wxmax, wymin, wymax, iwtrf, (real *)0,
	     (real *)0, (real *)0, (real *)0);
    }

/* Subroutine */ int sgqwtr_(real *vxmin, real *vxmax, real *vymin, real *
	vymax, real *wxmin, real *wxmax, real *wymin, real *wymax, integer *
	iwtrf)
{
    return sgwtrf_0_(2, (real *)0, (real *)0, (real *)0, (real *)0, vxmin, 
	    vxmax, vymin, vymax, wxmin, wxmax, wymin, wymax, iwtrf, (real *)0,
	     (real *)0, (real *)0, (real *)0);
    }

/* Subroutine */ int sgswrc_(real *wsxmn, real *wsxmx, real *wsymn, real *
	wsymx)
{
    return sgwtrf_0_(3, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0,
	     (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0,
	     (real *)0, (integer *)0, wsxmn, wsxmx, wsymn, wsymx);
    }

/* Subroutine */ int sgqwrc_(real *wsxmn, real *wsxmx, real *wsymn, real *
	wsymx)
{
    return sgwtrf_0_(4, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0,
	     (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0,
	     (real *)0, (integer *)0, wsxmn, wsxmx, wsymn, wsymx);
    }


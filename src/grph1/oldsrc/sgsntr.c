/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int sgsntr_0_(int n__, real *uxmin, real *uxmax, real *uymin,
	 real *uymax, real *vxmin, real *vxmax, real *vymin, real *vymax, 
	integer *itr)
{
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), sgqwnd_(real *, real *, real *, real *), sgswnd_(
	    real *, real *, real *, real *), sgstrf_(void), sgqtrn_(integer *)
	    , sgstrn_(integer *), sgqvpt_(real *, real *, real *, real *), 
	    sgsvpt_(real *, real *, real *, real *);

    switch(n__) {
	case 1: goto L_sgqntr;
	}

    msgdmp_("M", "SGSNTR", "THIS IS OLD INTERFACE.", (ftnlen)1, (ftnlen)6, (
	    ftnlen)22);
    if (! (1 <= *itr && *itr <= 4)) {
	msgdmp_("W", "SGSNTR", "INVALID TRANSFORMATION NUMBER.", (ftnlen)1, (
		ftnlen)6, (ftnlen)30);
    }
    sgswnd_(uxmin, uxmax, uymin, uymax);
    sgsvpt_(vxmin, vxmax, vymin, vymax);
    sgstrn_(itr);
    sgstrf_();
    return 0;
/* ----------------------------------------------------------------------- */

L_sgqntr:
    msgdmp_("M", "SGQNTR", "THIS IS OLD INTERFACE.", (ftnlen)1, (ftnlen)6, (
	    ftnlen)22);
    sgqtrn_(itr);
    if (! (1 <= *itr && *itr <= 4)) {
	msgdmp_("W", "SGQNTR", "INVALID TRANSFORMATION NUMBER.", (ftnlen)1, (
		ftnlen)6, (ftnlen)30);
    }
    sgqwnd_(uxmin, uxmax, uymin, uymax);
    sgqvpt_(vxmin, vxmax, vymin, vymax);
    return 0;
} /* sgsntr_ */

/* Subroutine */ int sgsntr_(real *uxmin, real *uxmax, real *uymin, real *
	uymax, real *vxmin, real *vxmax, real *vymin, real *vymax, integer *
	itr)
{
    return sgsntr_0_(0, uxmin, uxmax, uymin, uymax, vxmin, vxmax, vymin, 
	    vymax, itr);
    }

/* Subroutine */ int sgqntr_(real *uxmin, real *uxmax, real *uymin, real *
	uymax, real *vxmin, real *vxmax, real *vymin, real *vymax, integer *
	itr)
{
    return sgsntr_0_(1, uxmin, uxmax, uymin, uymax, vxmin, vxmax, vymin, 
	    vymax, itr);
    }


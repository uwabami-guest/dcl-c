/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     UNIT CONVERSION (DEG<->RAD) */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int stfrad_0_(int n__, real *x, real *y, real *rx, real *ry, 
	logical *lxdeg, logical *lydeg)
{
    static real cp;
    static logical lxd, lyd;
    extern real rfpi_(void);

    switch(n__) {
	case 1: goto L_stirad;
	case 2: goto L_stsrad;
	}

    if (lxd) {
	*rx = *x * cp;
    } else {
	*rx = *x;
    }
    if (lyd) {
	*ry = *y * cp;
    } else {
	*ry = *y;
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_stirad:
    if (lxd) {
	*x = *rx / cp;
    } else {
	*x = *rx;
    }
    if (lyd) {
	*y = *ry / cp;
    } else {
	*y = *ry;
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_stsrad:
    lxd = *lxdeg;
    lyd = *lydeg;
    cp = rfpi_() / 180;
    return 0;
} /* stfrad_ */

/* Subroutine */ int stfrad_(real *x, real *y, real *rx, real *ry)
{
    return stfrad_0_(0, x, y, rx, ry, (logical *)0, (logical *)0);
    }

/* Subroutine */ int stirad_(real *rx, real *ry, real *x, real *y)
{
    return stfrad_0_(1, x, y, rx, ry, (logical *)0, (logical *)0);
    }

/* Subroutine */ int stsrad_(logical *lxdeg, logical *lydeg)
{
    return stfrad_0_(2, (real *)0, (real *)0, (real *)0, (real *)0, lxdeg, 
	    lydeg);
    }


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static real c_b2 = 1.f;

/* ----------------------------------------------------------------------- */
/*     3-D TRANSFORMATION */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int stftr3_0_(int n__, real *ux, real *uy, real *uz, real *
	vx, real *vy, real *vz, integer *itr, real *cxa, real *cya, real *cza,
	 real *vxoff, real *vyoff, real *vzoff, logical *lxrd, logical *lyrd, 
	logical *lzrd, logical *lxlg, logical *lylg, logical *lzlg)
{
    /* Builtin functions */
    double r_sign(real *, real *), r_lg10(real *);

    /* Local variables */
    static real cp, cx, cy, cz, xx, yy, zz, vx0, vy0, vz0, xxx, yyy, zzz;
    extern real rfpi_(void);
    static integer itrz;
    extern /* Subroutine */ int ct2pc_(real *, real *, real *, real *), 
	    ct3sc_(real *, real *, real *, real *, real *, real *);
    static logical lmiss;
    static real rmiss;
    static logical lxlgz, lylgz, lzlgz, lxrdz, lyrdz, lzrdz;
    static real uxmin3, uymin3, uxmax3, uymax3, uzmin3, uzmax3;
    extern /* Subroutine */ int gllget_(char *, logical *, ftnlen), glrget_(
	    char *, real *, ftnlen), msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), scqwnd_(real *, real *, real *, real *, real *, 
	    real *);

    switch(n__) {
	case 1: goto L_ststr3;
	case 2: goto L_stsrd3;
	case 3: goto L_stslg3;
	}

    if (lmiss && (*ux == rmiss || *uy == rmiss || *uz == rmiss)) {
	*vx = rmiss;
	*vy = rmiss;
	*vz = rmiss;
	return 0;
    }
    xx = *ux;
    yy = *uy;
    zz = *uz;
    if (itrz == 1) {
	if (lxlgz || lylgz || lzlgz) {
	    scqwnd_(&uxmin3, &uxmax3, &uymin3, &uymax3, &uzmin3, &uzmax3);
	}
	if (lxlgz) {
	    xx = r_sign(&c_b2, &uxmin3) * xx;
	    if (xx > 0.f) {
		xx = r_lg10(&xx);
	    } else {
		msgdmp_("E", "STFTR3", "UX HAS INVALID SIGN FOR LOG TRANSFOR"
			"MATION OR IS 0", (ftnlen)1, (ftnlen)6, (ftnlen)50);
	    }
	}
	if (lylgz) {
	    yy = r_sign(&c_b2, &uymin3) * yy;
	    if (yy > 0.f) {
		yy = r_lg10(&yy);
	    } else {
		msgdmp_("E", "STFTR3", "UY HAS INVALID SIGN FOR LOG TRANSFOR"
			"MATION OR IS 0", (ftnlen)1, (ftnlen)6, (ftnlen)50);
	    }
	}
	if (lzlgz) {
	    zz = r_sign(&c_b2, &uzmin3) * zz;
	    if (zz > 0.f) {
		zz = r_lg10(&zz);
	    } else {
		msgdmp_("E", "STFTR3", "UZ HAS INVALID SIGN FOR LOG TRANSFOR"
			"MATION OR IS 0", (ftnlen)1, (ftnlen)6, (ftnlen)50);
	    }
	}
	xxx = xx;
	yyy = yy;
	zzz = zz;
    } else if (itrz == 2) {
	if (lxlgz || lzlgz) {
	    scqwnd_(&uxmin3, &uxmax3, &uymin3, &uymax3, &uzmin3, &uzmax3);
	}
	if (lxlgz) {
	    xx = r_sign(&c_b2, &uxmin3) * xx;
	    if (xx > 0.f) {
		xx = r_lg10(&xx);
	    } else {
		msgdmp_("E", "STFTR3", "UX HAS INVALID SIGN FOR LOG TRANSFOR"
			"MATION OR IS 0", (ftnlen)1, (ftnlen)6, (ftnlen)50);
	    }
	}
	if (lyrdz) {
	    yy *= cp;
	}
	if (lzlgz) {
	    zz = r_sign(&c_b2, &uzmin3) * zz;
	    if (zz > 0.f) {
		zz = r_lg10(&zz);
	    } else {
		msgdmp_("E", "STFTR3", "UZ HAS INVALID SIGN FOR LOG TRANSFOR"
			"MATION OR IS 0", (ftnlen)1, (ftnlen)6, (ftnlen)50);
	    }
	}
	zzz = zz;
	ct2pc_(&xx, &yy, &xxx, &yyy);
    } else if (itrz == 3) {
	if (lxlgz) {
	    scqwnd_(&uxmin3, &uxmax3, &uymin3, &uymax3, &uzmin3, &uzmax3);
	}
	if (lxlgz) {
	    xx = r_sign(&c_b2, &uxmin3) * xx;
	    if (xx > 0.f) {
		xx = r_lg10(&xx);
	    } else {
		msgdmp_("E", "STFTR3", "UX HAS INVALID SIGN FOR LOG TRANSFOR"
			"MATION OR IS 0", (ftnlen)1, (ftnlen)6, (ftnlen)50);
	    }
	}
	if (lyrdz) {
	    yy *= cp;
	}
	if (lzrdz) {
	    zz *= cp;
	}
	ct3sc_(&xx, &yy, &zz, &xxx, &yyy, &zzz);
    }
    *vx = cx * xxx + vx0;
    *vy = cy * yyy + vy0;
    *vz = cz * zzz + vz0;
    return 0;
/* ----------------------------------------------------------------------- */

L_ststr3:
    itrz = *itr;
    cx = *cxa;
    cy = *cya;
    cz = *cza;
    vx0 = *vxoff;
    vy0 = *vyoff;
    vz0 = *vzoff;
    cp = rfpi_() / 180;
    gllget_("LMISS", &lmiss, (ftnlen)5);
    glrget_("RMISS", &rmiss, (ftnlen)5);
    return 0;
/* ----------------------------------------------------------------------- */

L_stsrd3:
    lxrdz = *lxrd;
    lyrdz = *lyrd;
    lzrdz = *lzrd;
    return 0;
/* ----------------------------------------------------------------------- */

L_stslg3:
    lxlgz = *lxlg;
    lylgz = *lylg;
    lzlgz = *lzlg;
    return 0;
} /* stftr3_ */

/* Subroutine */ int stftr3_(real *ux, real *uy, real *uz, real *vx, real *vy,
	 real *vz)
{
    return stftr3_0_(0, ux, uy, uz, vx, vy, vz, (integer *)0, (real *)0, (
	    real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (logical *)
	    0, (logical *)0, (logical *)0, (logical *)0, (logical *)0, (
	    logical *)0);
    }

/* Subroutine */ int ststr3_(integer *itr, real *cxa, real *cya, real *cza, 
	real *vxoff, real *vyoff, real *vzoff)
{
    return stftr3_0_(1, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0,
	     (real *)0, itr, cxa, cya, cza, vxoff, vyoff, vzoff, (logical *)0,
	     (logical *)0, (logical *)0, (logical *)0, (logical *)0, (logical 
	    *)0);
    }

/* Subroutine */ int stsrd3_(logical *lxrd, logical *lyrd, logical *lzrd)
{
    return stftr3_0_(2, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0,
	     (real *)0, (integer *)0, (real *)0, (real *)0, (real *)0, (real *
	    )0, (real *)0, (real *)0, lxrd, lyrd, lzrd, (logical *)0, (
	    logical *)0, (logical *)0);
    }

/* Subroutine */ int stslg3_(logical *lxlg, logical *lylg, logical *lzlg)
{
    return stftr3_0_(3, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0,
	     (real *)0, (integer *)0, (real *)0, (real *)0, (real *)0, (real *
	    )0, (real *)0, (real *)0, (logical *)0, (logical *)0, (logical *)
	    0, lxlg, lylg, lzlg);
    }


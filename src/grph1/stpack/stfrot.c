/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ---------------------------------------------------------------------- */
/*     ROTATION OF LON-LAT COORDINATE */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int stfrot_0_(int n__, real *ux, real *uy, real *tx, real *
	ty, real *theta, real *phi, real *psi)
{
    /* System generated locals */
    real r__1, r__2, r__3, r__4;

    /* Local variables */
    static real y1, pi2;
    extern /* Subroutine */ int cr3s_(real *, real *, real *, real *, real *, 
	    real *, real *);
    extern real rfpi_(void);
    static real phiz, psiz, thetaz;

    switch(n__) {
	case 1: goto L_stirot;
	case 2: goto L_stsrot;
	}

    r__1 = pi2 - *uy;
    cr3s_(&thetaz, &phiz, &psiz, &r__1, ux, &y1, tx);
    *ty = pi2 - y1;
    return 0;
/* ----------------------------------------------------------------------- */

L_stirot:
    r__1 = -thetaz;
    r__2 = -psiz;
    r__3 = -phiz;
    r__4 = pi2 - *ty;
    cr3s_(&r__1, &r__2, &r__3, &r__4, tx, &y1, ux);
    *uy = pi2 - y1;
    return 0;
/* ----------------------------------------------------------------------- */

L_stsrot:
    thetaz = *theta;
    phiz = *phi;
    psiz = *psi;
    pi2 = rfpi_() / 2;
    return 0;
} /* stfrot_ */

/* Subroutine */ int stfrot_(real *ux, real *uy, real *tx, real *ty)
{
    return stfrot_0_(0, ux, uy, tx, ty, (real *)0, (real *)0, (real *)0);
    }

/* Subroutine */ int stirot_(real *tx, real *ty, real *ux, real *uy)
{
    return stfrot_0_(1, ux, uy, tx, ty, (real *)0, (real *)0, (real *)0);
    }

/* Subroutine */ int stsrot_(real *theta, real *phi, real *psi)
{
    return stfrot_0_(2, (real *)0, (real *)0, (real *)0, (real *)0, theta, 
	    phi, psi);
    }


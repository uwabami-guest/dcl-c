/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     NORMALIZATION TRANSFORMATION (INCLUDING MAP PROJECTION) */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int stftrf_0_(int n__, real *ux, real *uy, real *vx, real *
	vy, logical *lmapa)
{
    static real tx, ty, ux1, uy1, rna;
    static logical lmap, lmiss;
    static real rmiss;
    extern /* Subroutine */ int gllget_(char *, logical *, ftnlen), stfrad_(
	    real *, real *, real *, real *), glrget_(char *, real *, ftnlen), 
	    stirad_(real *, real *, real *, real *), stftrn_(real *, real *, 
	    real *, real *), stfrot_(real *, real *, real *, real *), stitrn_(
	    real *, real *, real *, real *), stirot_(real *, real *, real *, 
	    real *);

    switch(n__) {
	case 1: goto L_stitrf;
	case 2: goto L_stqtrf;
	case 3: goto L_ststrf;
	}

    if (lmiss && (*ux == rmiss || *uy == rmiss)) {
	*vx = rmiss;
	*vy = rmiss;
	return 0;
    }
    stfrad_(ux, uy, &ux1, &uy1);
    if (lmap) {
	stfrot_(&ux1, &uy1, &tx, &ty);
	stftrn_(&tx, &ty, vx, vy);
    } else {
	stftrn_(&ux1, &uy1, vx, vy);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_stitrf:
    if (lmap) {
	stitrn_(vx, vy, &tx, &ty);
	if (tx == rna || ty == rna) {
	    *ux = rna;
	    *uy = rna;
	    return 0;
	}
	stirot_(&tx, &ty, &ux1, &uy1);
    } else {
	stitrn_(vx, vy, &ux1, &uy1);
    }
    stirad_(&ux1, &uy1, ux, uy);
    return 0;
/* ----------------------------------------------------------------------- */

L_stqtrf:
    *lmapa = lmap;
    return 0;
/* ----------------------------------------------------------------------- */

L_ststrf:
    lmap = *lmapa;
    gllget_("LMISS", &lmiss, (ftnlen)5);
    glrget_("RMISS", &rmiss, (ftnlen)5);
    glrget_("RUNDEF", &rna, (ftnlen)6);
    return 0;
} /* stftrf_ */

/* Subroutine */ int stftrf_(real *ux, real *uy, real *vx, real *vy)
{
    return stftrf_0_(0, ux, uy, vx, vy, (logical *)0);
    }

/* Subroutine */ int stitrf_(real *vx, real *vy, real *ux, real *uy)
{
    return stftrf_0_(1, ux, uy, vx, vy, (logical *)0);
    }

/* Subroutine */ int stqtrf_(logical *lmapa)
{
    return stftrf_0_(2, (real *)0, (real *)0, (real *)0, (real *)0, lmapa);
    }

/* Subroutine */ int ststrf_(logical *lmapa)
{
    return stftrf_0_(3, (real *)0, (real *)0, (real *)0, (real *)0, lmapa);
    }


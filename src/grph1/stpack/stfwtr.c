/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__0 = 0;

/* ----------------------------------------------------------------------- */
/*     WORKSTATION TRANSFORMATION */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int stfwtr_0_(int n__, real *rx, real *ry, real *wx, real *
	wy, real *rxmin, real *rxmax, real *rymin, real *rymax, real *wxmin, 
	real *wxmax, real *wymin, real *wymax, integer *iwtrf, real *wsxmn, 
	real *wsxmx, real *wsymn, real *wsymx)
{
    /* Initialized data */

    static logical lwtrfz = FALSE_;
    static logical lgswrc = FALSE_;
    static logical lprmsg = TRUE_;

    /* System generated locals */
    real r__1, r__2;

    /* Local variables */
    static real cx, cy, dx, dy, wx0, wy0, dwx, dwy, rwn, rvp, wxx, wyy;
    extern logical lrne1_(real *, real *);
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), szscll_(real *, real *, real *, real *, integer *
	    );
    static integer iwtrfz;
    static real rxminz, rxmaxz, ryminz, wxminz, wxmaxz, wyminz, wymaxz, 
	    rymaxz, wsxmnz, wsymnz, wsxmxz, wsymxz;

    switch(n__) {
	case 1: goto L_stiwtr;
	case 2: goto L_stswtr;
	case 3: goto L_stqwtr;
	case 4: goto L_stswrc;
	case 5: goto L_stqwrc;
	}

    if (! lwtrfz) {
	msgdmp_("E", "STFWTR", "WORKSTATION TRANSFORMATION IS NOT DEFINED.", (
		ftnlen)1, (ftnlen)6, (ftnlen)42);
    }
    if (! lgswrc) {
	msgdmp_("E", "STFWTR", "WORKSTATION RECTANGLE IS NOT DEFINED.", (
		ftnlen)1, (ftnlen)6, (ftnlen)37);
    }
    wxx = cx * *rx + wx0;
    wyy = cy * *ry + wy0;
    if (iwtrfz == 1) {
	*wx = wxx + wsxmnz;
	*wy = wyy + wsymnz;
    } else if (iwtrfz == 2) {
	*wx = wyy + wsxmnz;
	*wy = -wxx + wsymxz;
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_stiwtr:
    if (! lwtrfz) {
	msgdmp_("E", "STIWTR", "WORKSTATION TRANSFORMATION IS NOT DEFINED.", (
		ftnlen)1, (ftnlen)6, (ftnlen)42);
    }
    if (! lgswrc) {
	msgdmp_("E", "STIWTR", "WORKSTATION RECTANGLE IS NOT DEFINED.", (
		ftnlen)1, (ftnlen)6, (ftnlen)37);
    }
    if (iwtrfz == 1) {
	wxx = *wx - wsxmnz;
	wyy = *wy - wsymnz;
    } else if (iwtrfz == 2) {
	wxx = -(*wy) + wsymxz;
	wyy = *wx - wsxmnz;
    }
    *rx = (wxx - wx0) / cx;
    *ry = (wyy - wy0) / cy;
    return 0;
/* ----------------------------------------------------------------------- */

L_stswtr:
    if (! (*rxmin < *rxmax && *rymin < *rymax)) {
	msgdmp_("E", "STSWTR", "WORKSTATION WINDOW DEFINITION IS INVALID.", (
		ftnlen)1, (ftnlen)6, (ftnlen)41);
    }
    if (! (0.f <= *rxmin && *rxmax <= 1.f && (0.f <= *rymin && *rymax <= 1.f))
	    ) {
	msgdmp_("E", "STSWTR", "WORKSTATION WINDOW IS NOT WITHIN THE NORMALI"
		"ZED DEVICE COORDINATE UNIT SQUARE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)78);
    }
    if (! (*iwtrf == 1 || *iwtrf == 2)) {
	msgdmp_("E", "STSWTR", "TRANSFORMATION FUNCTION NUMBER IS INVALID.", (
		ftnlen)1, (ftnlen)6, (ftnlen)42);
    }
    rwn = (*rymax - *rymin) / (*rxmax - *rxmin);
    rvp = (*wymax - *wymin) / (*wxmax - *wxmin);
    if (lrne1_(&rwn, &rvp) && lprmsg) {
	msgdmp_("W", "STSWTR", "WORKSTATION VIEWPORT WAS MODIFIED.", (ftnlen)
		1, (ftnlen)6, (ftnlen)34);
	lprmsg = FALSE_;
    }
    dx = *wxmax - *wxmin;
    dy = *wymax - *wymin;
/* Computing MIN */
    r__1 = dx, r__2 = dy / rwn;
    dwx = min(r__1,r__2);
/* Computing MIN */
    r__1 = dx * rwn;
    dwy = min(r__1,dy);
    wxminz = (*wxmin + *wxmax - dwx) / 2;
    wxmaxz = (*wxmin + *wxmax + dwx) / 2;
    wyminz = (*wymin + *wymax - dwy) / 2;
    wymaxz = (*wymin + *wymax + dwy) / 2;
    rxminz = *rxmin;
    rxmaxz = *rxmax;
    ryminz = *rymin;
    rymaxz = *rymax;
    iwtrfz = *iwtrf;
    lwtrfz = TRUE_;
    cx = (wxmaxz - wxminz) / (rxmaxz - rxminz);
    cy = (wymaxz - wyminz) / (rymaxz - ryminz);
    wx0 = wxminz - cx * rxminz;
    wy0 = wyminz - cy * ryminz;
    szscll_(&rxminz, &rxmaxz, &ryminz, &rymaxz, &c__0);
    return 0;
/* ----------------------------------------------------------------------- */

L_stqwtr:
    if (! lwtrfz) {
	msgdmp_("E", "STQWTR", "WORKSTATION TRANSFORMATION IS NOT DEFINED.", (
		ftnlen)1, (ftnlen)6, (ftnlen)42);
    }
    *rxmin = rxminz;
    *rxmax = rxmaxz;
    *rymin = ryminz;
    *rymax = rymaxz;
    *wxmin = wxminz;
    *wxmax = wxmaxz;
    *wymin = wyminz;
    *wymax = wymaxz;
    *iwtrf = iwtrfz;
    return 0;
/* ----------------------------------------------------------------------- */

L_stswrc:
    if (! (*wsxmn < *wsxmx && *wsymn < *wsymx)) {
	msgdmp_("E", "STSWRC", "WORKSTATION RECTANGLE IS INVALID.", (ftnlen)1,
		 (ftnlen)6, (ftnlen)33);
    }
    wsxmnz = *wsxmn;
    wsxmxz = *wsxmx;
    wsymnz = *wsymn;
    wsymxz = *wsymx;
    lgswrc = TRUE_;
    return 0;
/* ----------------------------------------------------------------------- */

L_stqwrc:
    if (! lgswrc) {
	msgdmp_("E", "STQWRC", "WORKSTATION RECTANGLE IS NOT DEFINED.", (
		ftnlen)1, (ftnlen)6, (ftnlen)37);
    }
    *wsxmn = wsxmnz;
    *wsxmx = wsxmxz;
    *wsymn = wsymnz;
    *wsymx = wsymxz;
    return 0;
} /* stfwtr_ */

/* Subroutine */ int stfwtr_(real *rx, real *ry, real *wx, real *wy)
{
    return stfwtr_0_(0, rx, ry, wx, wy, (real *)0, (real *)0, (real *)0, (
	    real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (integer *)
	    0, (real *)0, (real *)0, (real *)0, (real *)0);
    }

/* Subroutine */ int stiwtr_(real *wx, real *wy, real *rx, real *ry)
{
    return stfwtr_0_(1, rx, ry, wx, wy, (real *)0, (real *)0, (real *)0, (
	    real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (integer *)
	    0, (real *)0, (real *)0, (real *)0, (real *)0);
    }

/* Subroutine */ int stswtr_(real *rxmin, real *rxmax, real *rymin, real *
	rymax, real *wxmin, real *wxmax, real *wymin, real *wymax, integer *
	iwtrf)
{
    return stfwtr_0_(2, (real *)0, (real *)0, (real *)0, (real *)0, rxmin, 
	    rxmax, rymin, rymax, wxmin, wxmax, wymin, wymax, iwtrf, (real *)0,
	     (real *)0, (real *)0, (real *)0);
    }

/* Subroutine */ int stqwtr_(real *rxmin, real *rxmax, real *rymin, real *
	rymax, real *wxmin, real *wxmax, real *wymin, real *wymax, integer *
	iwtrf)
{
    return stfwtr_0_(3, (real *)0, (real *)0, (real *)0, (real *)0, rxmin, 
	    rxmax, rymin, rymax, wxmin, wxmax, wymin, wymax, iwtrf, (real *)0,
	     (real *)0, (real *)0, (real *)0);
    }

/* Subroutine */ int stswrc_(real *wsxmn, real *wsxmx, real *wsymn, real *
	wsymx)
{
    return stfwtr_0_(4, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0,
	     (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0,
	     (real *)0, (integer *)0, wsxmn, wsxmx, wsymn, wsymx);
    }

/* Subroutine */ int stqwrc_(real *wsxmn, real *wsxmx, real *wsymn, real *
	wsymx)
{
    return stfwtr_0_(5, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0,
	     (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0,
	     (real *)0, (integer *)0, wsxmn, wsxmx, wsymn, wsymx);
    }


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static logical c_true = TRUE_;

/* ----------------------------------------------------------------------- */
/*     USER SUPPLIED FUNCTION (SAMPLE: additional map projections) */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */

/*     An original transformation can be defined by the user */
/*     when the following entries are provided: */

/*         STFUSR(UX,UY,XX,YY)    The forward transformation */
/*         STIUSR(XX,XY,UX,UY)    The inverse transformation */
/*         STSUSR                 Initialization of functions */

/* ----------------------------------------------------------------------- */
/* Subroutine */ int stfusr_0_(int n__, real *xlon, real *ylat, real *x, real 
	*y, integer *kproj, integer *i__, char *cttl, ftnlen cttl_len)
{
    /* Initialized data */

    static integer jproj = -1;

    /* System generated locals */
    real r__1, r__2, r__3;

    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void);
    /* Subroutine */ int s_stop(char *, ftnlen), s_copy(char *, char *, 
	    ftnlen, ftnlen);

    /* Local variables */
    static real cp, vx0, vy0;
    static integer itr;
    static real plx, ply;
    static logical ldeg;
    extern real rfpi_(void);
    static real rsat, vxoff, vyoff, txmin, plrot, vxmin, vymin, vxmax, vymax, 
	    txmax, tymin, tymax, stlat1, stlat2, simfac;
    extern /* Subroutine */ int mpfglb_(real *, real *, real *, real *), 
	    mpiglb_(real *, real *, real *, real *), mpfcct_(real *, real *, 
	    real *, real *), mpfcya_(real *, real *, real *, real *), mpfcyb_(
	    real *, real *, real *, real *), mpfcyc_(real *, real *, real *, 
	    real *), mpicya_(real *, real *, real *, real *), mpicyb_(real *, 
	    real *, real *, real *), mpicyc_(real *, real *, real *, real *), 
	    mpicct_(real *, real *, real *, real *), sglget_(char *, logical *
	    , ftnlen), mpscct_(real *), sgrget_(char *, real *, ftnlen), 
	    mpscyb_(real *), stsrad_(logical *, logical *), sgqsim_(real *, 
	    real *, real *), sgqmpl_(real *, real *, real *), mpsotg_(real *),
	     sgqvpt_(real *, real *, real *, real *), ststrf_(logical *), 
	    szsclx_(real *, real *), ststri_(integer *), szscly_(real *, real 
	    *), stsrot_(real *, real *, real *), ststrp_(real *, real *, real 
	    *, real *), sgqtxy_(real *, real *, real *, real *);

    /* Fortran I/O blocks */
    static cilist io___2 = { 0, 6, 0, 0, 0 };
    static cilist io___3 = { 0, 6, 0, 0, 0 };


    switch(n__) {
	case 1: goto L_stiusr;
	case 2: goto L_stsusr;
	case 3: goto L_stnusr;
	case 4: goto L_stcusr;
	}

    if (jproj == 100) {
	mpfcyc_(xlon, ylat, x, y);
    } else if (jproj == 101) {
	mpfcya_(xlon, ylat, x, y);
    } else if (jproj == 102) {
	mpfcyb_(xlon, ylat, x, y);
    } else if (jproj == 103) {
	mpfglb_(xlon, ylat, x, y);
    } else if (jproj == 104) {
	mpfcct_(xlon, ylat, x, y);
    } else {
	s_wsle(&io___2);
	do_lio(&c__9, &c__1, "CALL STNUSR(i) before calling STFUSR", (ftnlen)
		36);
	e_wsle();
	s_stop("16", (ftnlen)2);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_stiusr:
    if (jproj == 100) {
	mpicyc_(x, y, xlon, ylat);
    } else if (jproj == 101) {
	mpicya_(x, y, xlon, ylat);
    } else if (jproj == 102) {
	mpicyb_(x, y, xlon, ylat);
    } else if (jproj == 103) {
	mpiglb_(x, y, xlon, ylat);
    } else if (jproj == 104) {
	mpicct_(x, y, xlon, ylat);
    } else {
	s_wsle(&io___3);
	do_lio(&c__9, &c__1, "CALL STNUSR(i) before calling STIUSR", (ftnlen)
		36);
	e_wsle();
	s_stop("16", (ftnlen)2);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_stsusr:
    sglget_("LDEG", &ldeg, (ftnlen)4);
    if (ldeg) {
	cp = rfpi_() / 180;
    } else {
	cp = 1.f;
    }
    itr = 99;
    sgqvpt_(&vxmin, &vxmax, &vymin, &vymax);
    sgqsim_(&simfac, &vxoff, &vyoff);
    sgqmpl_(&plx, &ply, &plrot);
    vx0 = (vxmax + vxmin) / 2 + vxoff;
    vy0 = (vymax + vymin) / 2 + vyoff;
    stsrad_(&ldeg, &ldeg);
    r__1 = rfpi_() / 2 - cp * ply;
    r__2 = cp * plx;
    r__3 = cp * plrot;
    stsrot_(&r__1, &r__2, &r__3);
    ststrf_(&c_true);
/*       CALL STSTRN(ITR, SIMFAC, SIMFAC, VX0, VY0) */
    ststri_(&itr);
    ststrp_(&simfac, &simfac, &vx0, &vy0);
/*       / SET CLIPPING REGION / */
    sgqtxy_(&txmin, &txmax, &tymin, &tymax);
    r__1 = cp * txmin;
    r__2 = cp * txmax;
    szsclx_(&r__1, &r__2);
    r__1 = cp * tymin;
    r__2 = cp * tymax;
    szscly_(&r__1, &r__2);
/*       / SATELITE VIEW / */
    sgrget_("RSAT", &rsat, (ftnlen)4);
    mpsotg_(&rsat);
/*       / SET STANDARD LATITUDE / */
    sgrget_("STLAT1", &stlat1, (ftnlen)6);
    sgrget_("STLAT2", &stlat2, (ftnlen)6);
/*       / JPROJ-DRIVEN CONFIG / */
    if (jproj == 102) {
	r__1 = cp * stlat1;
	mpscyb_(&r__1);
    } else if (jproj == 104) {
	r__1 = cp * stlat1;
	mpscct_(&r__1);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_stnusr:
    jproj = *kproj;
    return 0;
/* ----------------------------------------------------------------------- */

L_stcusr:
    if (*i__ == 100) {
	s_copy(cttl, "CENTRAL CYLINDRICAL", cttl_len, (ftnlen)19);
    } else if (*i__ == 101) {
	s_copy(cttl, "EQ.-AREA CYLINDRICAL", cttl_len, (ftnlen)20);
    } else if (*i__ == 102) {
	s_copy(cttl, "BRAUN CYLINDRICAL", cttl_len, (ftnlen)17);
    } else if (*i__ == 103) {
	s_copy(cttl, "BACON'S GLOBULAR", cttl_len, (ftnlen)16);
    } else if (*i__ == 104) {
	s_copy(cttl, "CENTRAL CONICAL", cttl_len, (ftnlen)15);
    } else {
	s_copy(cttl, " ", cttl_len, (ftnlen)1);
    }
    return 0;
} /* stfusr_ */

/* Subroutine */ int stfusr_(real *xlon, real *ylat, real *x, real *y)
{
    return stfusr_0_(0, xlon, ylat, x, y, (integer *)0, (integer *)0, (char *)
	    0, (ftnint)0);
    }

/* Subroutine */ int stiusr_(real *x, real *y, real *xlon, real *ylat)
{
    return stfusr_0_(1, xlon, ylat, x, y, (integer *)0, (integer *)0, (char *)
	    0, (ftnint)0);
    }

/* Subroutine */ int stsusr_(void)
{
    return stfusr_0_(2, (real *)0, (real *)0, (real *)0, (real *)0, (integer *
	    )0, (integer *)0, (char *)0, (ftnint)0);
    }

/* Subroutine */ int stnusr_(integer *kproj)
{
    return stfusr_0_(3, (real *)0, (real *)0, (real *)0, (real *)0, kproj, (
	    integer *)0, (char *)0, (ftnint)0);
    }

/* Subroutine */ int stcusr_(integer *i__, char *cttl, ftnlen cttl_len)
{
    return stfusr_0_(4, (real *)0, (real *)0, (real *)0, (real *)0, (integer *
	    )0, i__, cttl, cttl_len);
    }


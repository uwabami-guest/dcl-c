/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static real c_b4 = 0.f;
static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     3-D PROJECTION */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int stfpr3_0_(int n__, real *x, real *y, real *z__, real *rx,
	 real *ry, real *xfc, real *yfc, real *zfc, real *theta, real *phi, 
	real *psi, real *fac, real *zview, real *rxoff, real *ryoff, integer *
	ix, integer *iy, real *sect)
{
    /* Initialized data */

    static logical laffin = FALSE_;
    static logical lprjct = FALSE_;
    static integer nx = 1;
    static integer ny = 2;
    static real signx = 1.f;
    static real signy = 1.f;
    static real sec = 0.f;
    static real a[9]	/* was [3][3] */ = { 1.f,0.f,0.f,0.f,1.f,0.f,0.f,0.f,
	    1.f };

    /* Builtin functions */
    integer i_sign(integer *, integer *);

    /* Local variables */
    static integer i__;
    static real x0[3], x1[3], x2[3], b11, b12, cc, b13, b21, b22, b23, bx, by;
    static integer ns;
    static real rx0, ry0, rz0, det, rna, ppx, ppy;
    extern /* Subroutine */ int cr3c_(real *, real *, real *, real *, real *, 
	    real *, real *, real *, real *), glrget_(char *, real *, ftnlen), 
	    msgdmp_(char *, char *, char *, ftnlen, ftnlen, ftnlen);
    static logical lprjcz;

    switch(n__) {
	case 1: goto L_stspr3;
	case 2: goto L_stfpr2;
	case 3: goto L_stipr2;
	case 4: goto L_stspr2;
	case 5: goto L_stepr2;
	case 6: goto L_strpr2;
	}

    x1[0] = *x;
    x1[1] = *y;
    x1[2] = *z__;
    for (i__ = 1; i__ <= 3; ++i__) {
	x1[i__ - 1] -= x0[i__ - 1];
/* L10: */
    }
    for (i__ = 1; i__ <= 3; ++i__) {
	x2[i__ - 1] = a[i__ - 1] * x1[0] + a[i__ + 2] * x1[1] + a[i__ + 5] * 
		x1[2];
/* L15: */
    }
    if (laffin) {
	*rx = x2[0] + rx0;
	*ry = x2[1] + ry0;
    } else {
	cc = rz0 / (rz0 - x2[2]);
	*rx = cc * x2[0] + rx0;
	*ry = cc * x2[1] + rx0;
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_stspr3:
    cr3c_(theta, phi, psi, fac, &c_b4, &c_b4, a, &a[1], &a[2]);
    cr3c_(theta, phi, psi, &c_b4, fac, &c_b4, &a[3], &a[4], &a[5]);
    cr3c_(theta, phi, psi, &c_b4, &c_b4, fac, &a[6], &a[7], &a[8]);
    x0[0] = *xfc;
    x0[1] = *yfc;
    x0[2] = *zfc;
    rx0 = *rxoff;
    ry0 = *ryoff;
    rz0 = *zview;
    laffin = *zview <= 0.f;
    return 0;
/* ----------------------------------------------------------------------- */

L_stfpr2:
    if (lprjct) {
	x1[nx - 1] = *x * signx;
	x1[ny - 1] = *y * signy;
	x1[ns - 1] = sec;
	for (i__ = 1; i__ <= 3; ++i__) {
	    x1[i__ - 1] -= x0[i__ - 1];
/* L20: */
	}
	for (i__ = 1; i__ <= 3; ++i__) {
	    x2[i__ - 1] = a[i__ - 1] * x1[0] + a[i__ + 2] * x1[1] + a[i__ + 5]
		     * x1[2];
/* L25: */
	}
	if (laffin) {
	    *rx = x2[0] + rx0;
	    *ry = x2[1] + ry0;
	} else {
	    cc = rz0 / (rz0 - x2[2]);
	    *rx = cc * x2[0] + rx0;
	    *ry = cc * x2[1] + ry0;
	}
    } else {
	*rx = *x;
	*ry = *y;
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_stipr2:
    if (lprjct) {
	ppx = (*rx - rx0) / rz0;
	ppy = (*ry - ry0) / rz0;
	if (laffin) {
	    b11 = a[nx * 3 - 3];
	    b12 = a[ny * 3 - 3];
	    b13 = a[ns * 3 - 3];
	    b21 = a[nx * 3 - 2];
	    b22 = a[ny * 3 - 2];
	    b23 = a[ns * 3 - 2];
	} else {
	    b11 = a[nx * 3 - 3] + a[nx * 3 - 1] * ppx;
	    b12 = a[ny * 3 - 3] + a[ny * 3 - 1] * ppx;
	    b13 = a[ns * 3 - 3] + a[ns * 3 - 1] * ppx;
	    b21 = a[nx * 3 - 2] + a[nx * 3 - 1] * ppy;
	    b22 = a[ny * 3 - 2] + a[ny * 3 - 1] * ppy;
	    b23 = a[ns * 3 - 2] + a[ns * 3 - 1] * ppy;
	}
	bx = rz0 * ppx - b13 * (sec - x0[ns - 1]);
	by = rz0 * ppy - b23 * (sec - x0[ns - 1]);
	det = b11 * b22 - b12 * b21;
	if (det == 0.f) {
	    glrget_("RUNDEF", &rna, (ftnlen)6);
	    *x = rna;
	    *y = rna;
	} else {
	    *x = ((b22 * bx - b12 * by) / det + x0[nx - 1]) * signx;
	    *y = ((-b21 * bx + b11 * by) / det + x0[ny - 1]) * signy;
	}
    } else {
	*x = *rx;
	*y = *ry;
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_stspr2:
    nx = abs(*ix);
    ny = abs(*iy);
    lprjct = nx != 0 && ny != 0;
    if (! lprjct) {
	return 0;
    }
    if (! (1 <= nx && nx <= 3)) {
	msgdmp_("E", "STSPR2", "INVALID COORDINATE NUMBER (IX).", (ftnlen)1, (
		ftnlen)6, (ftnlen)31);
    }
    if (! (1 <= ny && ny <= 3)) {
	msgdmp_("E", "STSPR2", "INVALID COORDINATE NUMBER (IY).", (ftnlen)1, (
		ftnlen)6, (ftnlen)31);
    }
    signx = (real) i_sign(&c__1, ix);
    signy = (real) i_sign(&c__1, iy);
    sec = *sect;
    for (i__ = 1; i__ <= 3; ++i__) {
	if (i__ != nx && i__ != ny) {
	    goto L55;
	}
/* L50: */
    }
L55:
    ns = i__;
    return 0;
/* ----------------------------------------------------------------------- */

L_stepr2:
    lprjcz = lprjct;
    lprjct = FALSE_;
    return 0;
/* ----------------------------------------------------------------------- */

L_strpr2:
    lprjct = lprjcz;
    return 0;
} /* stfpr3_ */

/* Subroutine */ int stfpr3_(real *x, real *y, real *z__, real *rx, real *ry)
{
    return stfpr3_0_(0, x, y, z__, rx, ry, (real *)0, (real *)0, (real *)0, (
	    real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (
	    real *)0, (integer *)0, (integer *)0, (real *)0);
    }

/* Subroutine */ int stspr3_(real *xfc, real *yfc, real *zfc, real *theta, 
	real *phi, real *psi, real *fac, real *zview, real *rxoff, real *
	ryoff)
{
    return stfpr3_0_(1, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0,
	     xfc, yfc, zfc, theta, phi, psi, fac, zview, rxoff, ryoff, (
	    integer *)0, (integer *)0, (real *)0);
    }

/* Subroutine */ int stfpr2_(real *x, real *y, real *rx, real *ry)
{
    return stfpr3_0_(2, x, y, (real *)0, rx, ry, (real *)0, (real *)0, (real *
	    )0, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (real *
	    )0, (real *)0, (integer *)0, (integer *)0, (real *)0);
    }

/* Subroutine */ int stipr2_(real *rx, real *ry, real *x, real *y)
{
    return stfpr3_0_(3, x, y, (real *)0, rx, ry, (real *)0, (real *)0, (real *
	    )0, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (real *
	    )0, (real *)0, (integer *)0, (integer *)0, (real *)0);
    }

/* Subroutine */ int stspr2_(integer *ix, integer *iy, real *sect)
{
    return stfpr3_0_(4, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0,
	     (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0,
	     (real *)0, (real *)0, (real *)0, (real *)0, ix, iy, sect);
    }

/* Subroutine */ int stepr2_(void)
{
    return stfpr3_0_(5, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0,
	     (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0,
	     (real *)0, (real *)0, (real *)0, (real *)0, (integer *)0, (
	    integer *)0, (real *)0);
    }

/* Subroutine */ int strpr2_(void)
{
    return stfpr3_0_(6, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0,
	     (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0,
	     (real *)0, (real *)0, (real *)0, (real *)0, (integer *)0, (
	    integer *)0, (real *)0);
    }


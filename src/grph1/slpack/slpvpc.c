/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     SLPVPC : PLOT VIEWPORT CORNER MARKS */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int slpvpc_(integer *index, real *rc)
{
    /* System generated locals */
    real r__1;

    /* Local variables */
    static real vxmin, vymin, vxmax, vymax;
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), szcllv_(void), sgqvpt_(real *, real *, real *, 
	    real *), szslti_(integer *, integer *), szpllv_(real *, real *), 
	    szoplv_(void), szmvlv_(real *, real *);

    if (*index <= 0) {
	if (*index == 0) {
	    msgdmp_("M", "SLPVPC", "LINE INDEX IS ZERO / DO NOTHING.", (
		    ftnlen)1, (ftnlen)6, (ftnlen)32);
	    return 0;
	} else {
	    msgdmp_("E", "SLPVPC", "LINE INDEX IS LESS THAN ZERO.", (ftnlen)1,
		     (ftnlen)6, (ftnlen)29);
	}
    }
    if (*rc <= 0.f) {
	if (*rc == 0.f) {
	    msgdmp_("E", "SLPVPC", "LENGTH OF CORNER MARKS IS ZERO / DO NOTH"
		    "ING.", (ftnlen)1, (ftnlen)6, (ftnlen)44);
	    return 0;
	} else {
	    msgdmp_("E", "SLPVPC", "LENGTH OF CORNER MARKS IS LESS THAN ZERO."
		    , (ftnlen)1, (ftnlen)6, (ftnlen)41);
	}
    }
    sgqvpt_(&vxmin, &vxmax, &vymin, &vymax);
    szslti_(&c__1, index);
    szoplv_();
    r__1 = vymin + *rc;
    szmvlv_(&vxmin, &r__1);
    szpllv_(&vxmin, &vymin);
    r__1 = vxmin + *rc;
    szpllv_(&r__1, &vymin);
    szcllv_();
    szoplv_();
    r__1 = vxmax - *rc;
    szmvlv_(&r__1, &vymin);
    szpllv_(&vxmax, &vymin);
    r__1 = vymin + *rc;
    szpllv_(&vxmax, &r__1);
    szcllv_();
    szoplv_();
    r__1 = vymax - *rc;
    szmvlv_(&vxmax, &r__1);
    szpllv_(&vxmax, &vymax);
    r__1 = vxmax - *rc;
    szpllv_(&r__1, &vymax);
    szcllv_();
    szoplv_();
    r__1 = vxmin + *rc;
    szmvlv_(&r__1, &vymax);
    szpllv_(&vxmin, &vymax);
    r__1 = vymax - *rc;
    szpllv_(&vxmin, &r__1);
    szcllv_();
    return 0;
} /* slpvpc_ */


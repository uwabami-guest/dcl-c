/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Common Block Declarations */

struct {
    real xmin[1000], xmax[1000], ymin[1000], ymax[1000];
    integer nn[4];
} slblk1_;

#define slblk1_1 slblk1_

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int sldiv_(char *cform, integer *ix, integer *iy, ftnlen 
	cform_len)
{
    /* System generated locals */
    integer i__1;

    /* Local variables */
    static integer l1, l2, nd, li, ln;
    static char cf1[1];
    static integer lev, nxy;
    extern integer isum0_(integer *, integer *, integer *);
    extern logical lchreq_(char *, char *, ftnlen, ftnlen);
    extern /* Subroutine */ int sgiget_(char *, integer *, ftnlen), msgdmp_(
	    char *, char *, char *, ftnlen, ftnlen, ftnlen), sgiset_(char *, 
	    integer *, ftnlen), sldivz_(real *, real *, real *, real *, char *
	    , integer *, integer *, real *, real *, real *, real *, ftnlen);

    *(unsigned char *)cf1 = *(unsigned char *)cform;
    if (! (lchreq_(cf1, "T", (ftnlen)1, (ftnlen)1) || lchreq_(cf1, "Y", (
	    ftnlen)1, (ftnlen)1) || lchreq_(cf1, "L", (ftnlen)1, (ftnlen)1) ||
	     lchreq_(cf1, "S", (ftnlen)1, (ftnlen)1))) {
	msgdmp_("E", "SLDIV ", "DIVISION DIRECTION IS INVALID.", (ftnlen)1, (
		ftnlen)6, (ftnlen)30);
    }
    sgiget_("NLEVEL", &lev, (ftnlen)6);
    if (lev >= 3) {
	msgdmp_("E", "SLDIV ", "NUMBER OF DIVISION IS IN EXCESS OF MUXIMUM.", 
		(ftnlen)1, (ftnlen)6, (ftnlen)43);
    }
    ++lev;
    nxy = *ix * *iy;
    slblk1_1.nn[lev] = nxy * slblk1_1.nn[lev - 1];
    i__1 = lev + 1;
    nd = isum0_(slblk1_1.nn, &i__1, &c__1);
    if (nd > 1000) {
	msgdmp_("E", "SLDIV ", "TOTAL FRAME NUMBER IS IN EXCESS OF MAXIMUM.", 
		(ftnlen)1, (ftnlen)6, (ftnlen)43);
    }
    i__1 = lev - 1;
    l1 = isum0_(slblk1_1.nn, &i__1, &c__1) + 1;
    l2 = isum0_(slblk1_1.nn, &lev, &c__1);
    ln = l2 + 1 - nxy;
    i__1 = l2;
    for (li = l1; li <= i__1; ++li) {
	ln += nxy;
	sldivz_(&slblk1_1.xmin[li - 1], &slblk1_1.xmax[li - 1], &
		slblk1_1.ymin[li - 1], &slblk1_1.ymax[li - 1], cform, ix, iy, 
		&slblk1_1.xmin[ln - 1], &slblk1_1.xmax[ln - 1], &
		slblk1_1.ymin[ln - 1], &slblk1_1.ymax[ln - 1], cform_len);
/* L10: */
    }
    sgiset_("NLEVEL", &lev, (ftnlen)6);
    return 0;
} /* sldiv_ */


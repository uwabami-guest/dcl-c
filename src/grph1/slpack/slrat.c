/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int slrat_(real *rx, real *ry)
{
    static real xx, yy;
    static integer lev;
    static real arat, brat, xamin, yamin, xamax, yamax;
    extern /* Subroutine */ int slmgn_(real *, real *, real *, real *), 
	    sgiget_(char *, integer *, ftnlen), msgdmp_(char *, char *, char *
	    , ftnlen, ftnlen, ftnlen), slqrct_(integer *, integer *, real *, 
	    real *, real *, real *);

    if (*rx < 0.f || *ry < 0.f) {
	msgdmp_("E", "SLRAT ", "PROPORTION PARAMETER IS LESS THAN ZERO.", (
		ftnlen)1, (ftnlen)6, (ftnlen)39);
    }
    sgiget_("NLEVEL", &lev, (ftnlen)6);
    slqrct_(&lev, &c__1, &xamin, &xamax, &yamin, &yamax);
    arat = (yamax - yamin) / (xamax - xamin);
    brat = *ry / *rx;
    if (arat >= brat) {
	xx = 0.f;
	yy = (1 - brat / arat) / 2;
    } else {
	xx = (1 - arat / brat) / 2;
	yy = 0.f;
    }
    slmgn_(&xx, &xx, &yy, &yy);
    return 0;
} /* slrat_ */


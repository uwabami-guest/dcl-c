/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int slmgnz_(real *xamin, real *xamax, real *yamin, real *
	yamax, real *xl, real *xr, real *yb, real *yt)
{
    static real wxa, wya;
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen);

    wxa = *xamax - *xamin;
    wya = *yamax - *yamin;
    if (wxa < 0.f || wya < 0.f) {
	msgdmp_("E", "SLMGNZ", "RECTANGLE DEFINITION IS INVALID.", (ftnlen)1, 
		(ftnlen)6, (ftnlen)32);
    }
    if (*xl < 0.f || *xr < 0.f || *yb < 0.f || *yt < 0.f) {
	msgdmp_("E", "SLMGNZ", "MARGIN PARAMETER IS LESS THAN ZERO.", (ftnlen)
		1, (ftnlen)6, (ftnlen)35);
    }
    if (*xl + *xr >= 1.f || *yb + *yt >= 1.f) {
	msgdmp_("E", "SLMGNZ", "SUM OF MARGIN PARAMETERS IS LARGER THAN 1.0.",
		 (ftnlen)1, (ftnlen)6, (ftnlen)44);
    }
    *xamin += wxa * *xl;
    *xamax -= wxa * *xr;
    *yamin += wya * *yb;
    *yamax -= wya * *yt;
    return 0;
} /* slmgnz_ */


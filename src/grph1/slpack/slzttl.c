/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__0 = 0;

/* ----------------------------------------------------------------------- */
/*     SLZTTL : PLOT TEXT BETWEEN WS-WINDOW AND MAX-DRAWING-REGION */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int slzttl_(char *cside, char *cttl, real *px, real *py, 
	real *rsize, integer *index, ftnlen cside_len, ftnlen cttl_len)
{
    static char cs[1];
    static integer itr;
    static real dwx, dwy, xtl, ytl;
    extern integer lenc_(char *, ftnlen);
    static real xpad, ypad, tlwx, tlwy, vxmn1, vymn1, wxmn1, wymn1, vxmn2, 
	    vymn2, vxmx1, vymx1, wxmx1, wymx1, vxmx2, vymx2;
    static integer nchar;
    static logical lwide;
    static integer nline;
    static real cxmin, cymin, cxmax, cymax, xcent, ycent, vxmin, wxmin, wymin,
	     wxmax, wymax, vxmax, vymin, vymax;
    extern logical lchreq_(char *, char *, ftnlen, ftnlen);
    extern /* Subroutine */ int sglget_(char *, logical *, ftnlen), msgdmp_(
	    char *, char *, char *, ftnlen, ftnlen, ftnlen), sgrget_(char *, 
	    real *, ftnlen), szqcll_(real *, real *, real *, real *, integer *
	    ), szscll_(real *, real *, real *, real *, integer *), stqwrc_(
	    real *, real *, real *, real *), stiwtr_(real *, real *, real *, 
	    real *), sgtxzr_(real *, real *, char *, real *, integer *, 
	    integer *, integer *, ftnlen), stqwtr_(real *, real *, real *, 
	    real *, real *, real *, real *, real *, integer *);

    *(unsigned char *)cs = *(unsigned char *)cside;
    stqwtr_(&vxmn1, &vxmx1, &vymn1, &vymx1, &wxmn1, &wxmx1, &wymn1, &wymx1, &
	    itr);
    if (itr == 1) {
	stqwrc_(&wxmin, &wxmax, &wymin, &wymax);
    } else {
	stqwrc_(&wxmin, &wxmax, &wymax, &wymin);
    }
    szqcll_(&cxmin, &cxmax, &cymin, &cymax, &c__0);
    stiwtr_(&wxmin, &wymin, &vxmn2, &vymn2);
    stiwtr_(&wxmax, &wymax, &vxmx2, &vymx2);
    sgrget_("XPAD", &xpad, (ftnlen)4);
    sgrget_("YPAD", &ypad, (ftnlen)4);
    sglget_("LWIDE", &lwide, (ftnlen)5);
    szscll_(&vxmn2, &vxmx2, &vymn2, &vymx2, &c__0);
    nchar = lenc_(cttl, cttl_len);
    nline = 1;
    tlwx = *rsize * (xpad + nchar);
    tlwy = *rsize * (ypad + nline);
    if (lwide) {
	vxmin = vxmn2;
	vxmax = vxmx2;
    } else {
	vxmin = vxmn1;
	vxmax = vxmx1;
    }
    if (lchreq_(cs, "T", (ftnlen)1, (ftnlen)1)) {
	vymin = vymx1;
	vymax = vymx2;
    } else if (lchreq_(cs, "B", (ftnlen)1, (ftnlen)1)) {
	vymin = vymn2;
	vymax = vymn1;
    } else {
	msgdmp_("E", "SLZTTL", "INVALID SIDE PARAMETER.", (ftnlen)1, (ftnlen)
		6, (ftnlen)23);
    }
    xcent = (vxmin + vxmax) / 2;
    ycent = (vymin + vymax) / 2;
    dwx = (vxmax - vxmin - tlwx) / 2;
    dwy = (vymax - vymin - tlwy) / 2;
    if (dwx < 0.f) {
	msgdmp_("W", "SLZTTL", "SPACE FOR TITLE IS NOT ENOUGH.", (ftnlen)1, (
		ftnlen)6, (ftnlen)30);
	msgdmp_("M", "-CNT.-", "PX = 0.0 IS ASSUMED.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
	dwx = 0.f;
    }
    if (dwy < 0.f) {
	msgdmp_("W", "SLZTTL", "SPACE FOR TITLE IS NOT ENOUGH.", (ftnlen)1, (
		ftnlen)6, (ftnlen)30);
	msgdmp_("M", "-CNT.-", "PY = 0.0 IS ASSUMED.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
	dwy = 0.f;
    }
    xtl = xcent + dwx * *px;
    ytl = ycent + dwy * *py;
    sgtxzr_(&xtl, &ytl, cttl, rsize, &c__0, &c__0, index, cttl_len);
    szscll_(&cxmin, &cxmax, &cymin, &cymax, &c__0);
    return 0;
} /* slzttl_ */


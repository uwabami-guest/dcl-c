/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int slqsiz_(char *csize, real *dx, real *dy, ftnlen 
	csize_len)
{
    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    integer s_rsfi(icilist *), do_fio(integer *, char *, ftnlen), e_rsfi(void)
	    ;

    /* Local variables */
    static integer i__, ns;
    static char cs1[1], cs2[1];
    static real dx2;
    extern logical lchrd_(char *, ftnlen), lchreq_(char *, char *, ftnlen, 
	    ftnlen);
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen);

    /* Fortran I/O blocks */
    static icilist io___3 = { 0, cs2, 0, "(I1)", 1, 1 };


    *(unsigned char *)cs1 = *(unsigned char *)csize;
    *(unsigned char *)cs2 = *(unsigned char *)&csize[1];
    if (lchreq_(cs1, "A", (ftnlen)1, (ftnlen)1)) {
	*dx = 118.9f;
	*dy = 84.1f;
    } else if (lchreq_(cs1, "B", (ftnlen)1, (ftnlen)1)) {
	*dx = 145.6f;
	*dy = 103.f;
    } else {
	msgdmp_("E", "SLQSIZ", "SIZE PARAMETER IS INVALID.", (ftnlen)1, (
		ftnlen)6, (ftnlen)26);
    }
    if (! lchrd_(cs2, (ftnlen)1)) {
	msgdmp_("E", "SLQSIZ", "SIZE PARAMETER IS INVALID.", (ftnlen)1, (
		ftnlen)6, (ftnlen)26);
    }
    s_rsfi(&io___3);
    do_fio(&c__1, (char *)&ns, (ftnlen)sizeof(integer));
    e_rsfi();
    i__1 = ns;
    for (i__ = 1; i__ <= i__1; ++i__) {
	dx2 = *dx / 2;
	*dx = *dy;
	*dy = dx2;
/* L10: */
    }
    return 0;
} /* slqsiz_ */


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     BASIC SUBROUTINES */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int sldivz_(real *xamin, real *xamax, real *yamin, real *
	yamax, char *cform, integer *ix, integer *iy, real *xbmin, real *
	xbmax, real *ybmin, real *ybmax, ftnlen cform_len)
{
    /* System generated locals */
    integer i__1;

    /* Local variables */
    static integer i__, jx, jy;
    static char cf1[1];
    static real wxa, wya;
    extern logical lchreq_(char *, char *, ftnlen, ftnlen);

    /* Parameter adjustments */
    --ybmax;
    --ybmin;
    --xbmax;
    --xbmin;

    /* Function Body */
    *(unsigned char *)cf1 = *(unsigned char *)cform;
    wxa = (*xamax - *xamin) / *ix;
    wya = (*yamax - *yamin) / *iy;
    i__1 = *ix * *iy;
    for (i__ = 1; i__ <= i__1; ++i__) {
	if (lchreq_(cf1, "T", (ftnlen)1, (ftnlen)1) || lchreq_(cf1, "L", (
		ftnlen)1, (ftnlen)1)) {
	    jx = (i__ - 1) / *iy + 1;
	    jy = (i__ - 1) % *iy + 1;
	} else {
	    jx = (i__ - 1) % *ix + 1;
	    jy = (i__ - 1) / *ix + 1;
	}
	xbmin[i__] = *xamin + wxa * (jx - 1);
	xbmax[i__] = *xamin + wxa * jx;
	ybmin[i__] = *yamin + wya * (*iy - jy);
	ybmax[i__] = *yamin + wya * (*iy - jy + 1);
/* L10: */
    }
    return 0;
} /* sldivz_ */


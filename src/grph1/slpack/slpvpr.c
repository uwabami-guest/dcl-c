/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     BASIC PLOT SUBROUTINES */
/* ----------------------------------------------------------------------- */
/*     SLPVPR : PLOT VIEWPORT RECTANGLE */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int slpvpr_(integer *index)
{
    static real vxmin, vymin, vxmax, vymax;
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), szcllv_(void), sgqvpt_(real *, real *, real *, 
	    real *), szslti_(integer *, integer *), szpllv_(real *, real *), 
	    szoplv_(void), szmvlv_(real *, real *);

    if (*index <= 0) {
	if (*index == 0) {
	    msgdmp_("M", "SLPVPR", "LINE INDEX IS ZERO / DO NOTHING.", (
		    ftnlen)1, (ftnlen)6, (ftnlen)32);
	    return 0;
	} else {
	    msgdmp_("E", "SLPVPR", "LINE INDEX IS LESS THAN ZERO.", (ftnlen)1,
		     (ftnlen)6, (ftnlen)29);
	}
    }
    sgqvpt_(&vxmin, &vxmax, &vymin, &vymax);
    szslti_(&c__1, index);
    szoplv_();
    szmvlv_(&vxmin, &vymin);
    szpllv_(&vxmax, &vymin);
    szpllv_(&vxmax, &vymax);
    szpllv_(&vxmin, &vymax);
    szpllv_(&vxmin, &vymin);
    szcllv_();
    return 0;
} /* slpvpr_ */


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__0 = 0;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int sltlcv_(char *ctlin, char *ctlout, integer *lout, ftnlen 
	ctlin_len, ftnlen ctlout_len)
{
    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer ic, id, jh, im, in, jm, js, np, iy, itl;
    static char cst[4];
    extern integer lenc_(char *, ftnlen);
    static char ctmp[16];
    static integer ipage;
    extern /* Subroutine */ int chngi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen);
    static logical lpara;
    extern /* Subroutine */ int datec3_(char *, integer *, integer *, integer 
	    *, ftnlen), dateq3_(integer *, integer *, integer *), timec3_(
	    char *, integer *, integer *, integer *, ftnlen), timeq3_(integer 
	    *, integer *, integer *);
    extern logical lchreq_(char *, char *, ftnlen, ftnlen);
    extern /* Subroutine */ int sgiget_(char *, integer *, ftnlen), osgarg_(
	    integer *, char *, ftnlen), osgenv_(char *, char *, ftnlen, 
	    ftnlen);

    itl = lenc_(ctlin, ctlin_len);
    in = 0;
    ic = 0;
L10:
    ++in;
    ++ic;
    if (*(unsigned char *)&ctlin[in - 1] == '#') {
	i__1 = in;
	s_copy(cst, ctlin + i__1, (ftnlen)4, in + 4 - i__1);
	if (lchreq_(cst, "PAGE", (ftnlen)4, (ftnlen)4)) {
	    s_copy(ctmp, "##", (ftnlen)16, (ftnlen)2);
	    sgiget_("NPAGE", &ipage, (ftnlen)5);
	    chngi_(ctmp, "##", &ipage, "(I2)", (ftnlen)16, (ftnlen)2, (ftnlen)
		    4);
	    lpara = TRUE_;
	} else if (lchreq_(cst, "DATE", (ftnlen)4, (ftnlen)4)) {
	    s_copy(ctmp, "YY/MM/DD", (ftnlen)16, (ftnlen)8);
	    dateq3_(&iy, &im, &id);
	    datec3_(ctmp, &iy, &im, &id, (ftnlen)16);
	    lpara = TRUE_;
	} else if (lchreq_(cst, "TIME", (ftnlen)4, (ftnlen)4)) {
	    s_copy(ctmp, "HH:MM:SS", (ftnlen)16, (ftnlen)8);
	    timeq3_(&jh, &jm, &js);
	    timec3_(ctmp, &jh, &jm, &js, (ftnlen)16);
	    lpara = TRUE_;
	} else if (lchreq_(cst, "PROG", (ftnlen)4, (ftnlen)4)) {
	    osgarg_(&c__0, ctmp, (ftnlen)16);
	    lpara = TRUE_;
	} else if (lchreq_(cst, "USER", (ftnlen)4, (ftnlen)4)) {
	    osgenv_("USER", ctmp, (ftnlen)4, (ftnlen)16);
	    lpara = TRUE_;
	} else {
	    lpara = FALSE_;
	}
	if (lpara) {
	    np = lenc_(ctmp, (ftnlen)16) - 1;
	    s_copy(ctlout + (ic - 1), ctmp, ic + np - (ic - 1), (ftnlen)16);
	    in += 4;
	    ic += np;
	} else {
	    *(unsigned char *)&ctlout[ic - 1] = '#';
	}
    } else {
	*(unsigned char *)&ctlout[ic - 1] = *(unsigned char *)&ctlin[in - 1];
    }
    if (! (in >= itl)) {
	goto L10;
    }
    *lout = ic;
    return 0;
} /* sltlcv_ */


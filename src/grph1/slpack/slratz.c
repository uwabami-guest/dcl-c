/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int slratz_(real *xamin, real *xamax, real *yamin, real *
	yamax, real *rx, real *ry)
{
    static real xx, yy, wxa, wya, arat, brat;
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), slmgnz_(real *, real *, real *, real *, real *, 
	    real *, real *, real *);

    wxa = *xamax - *xamin;
    wya = *yamax - *yamin;
    if (wxa < 0.f || wya < 0.f) {
	msgdmp_("E", "SLRATZ", "RECTANGLE DEFINITION IS INVALID.", (ftnlen)1, 
		(ftnlen)6, (ftnlen)32);
    }
    if (*rx < 0.f || *ry < 0.f) {
	msgdmp_("E", "SLRATZ", "PROPORTION PARAMETER IS LESS THAN ZERO.", (
		ftnlen)1, (ftnlen)6, (ftnlen)39);
    }
    arat = wya / wxa;
    brat = *ry / *rx;
    if (arat >= brat) {
	xx = 0.f;
	yy = (1 - brat / arat) / 2;
    } else {
	xx = (1 - arat / brat) / 2;
	yy = 0.f;
    }
    slmgnz_(xamin, xamax, yamin, yamax, &xx, &xx, &yy, &yy);
    return 0;
} /* slratz_ */


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Common Block Declarations */

struct {
    real xmin[1000], xmax[1000], ymin[1000], ymax[1000];
    integer nn[4];
} slblk1_;

#define slblk1_1 slblk1_

/* Table of constant values */

static integer c__1 = 1;
static integer c__4 = 4;
static integer c__3 = 3;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int slinit_0_(int n__, real *wxmax, real *wymax, real *fact, 
	char *cszez, real *dxa, real *dya, ftnlen cszez_len)
{
    /* System generated locals */
    address a__1[4], a__2[3];
    integer i__1[4], i__2[3];
    char ch__1[17], ch__2[39];

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen), s_cat(char *,
	     char **, integer *, integer *, ftnlen);

    /* Local variables */
    static real dx, dy, xx, yy;
    static char cs1[1], cs2[1], cs3[1];
    static real bwx, bwy, dxx, dyy;
    extern integer lenc_(char *, ftnlen);
    extern logical lchrd_(char *, ftnlen);
    static real factr;
    static char csize[3];
    extern logical lchreq_(char *, char *, ftnlen, ftnlen);
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), sgrget_(char *, real *, ftnlen), sgiset_(char *, 
	    integer *, ftnlen), sgrset_(char *, real *, ftnlen), slmgnz_(real 
	    *, real *, real *, real *, real *, real *, real *, real *), 
	    slqsiz_(char *, real *, real *, ftnlen);

    switch(n__) {
	case 1: goto L_slsize;
	case 2: goto L_slform;
	}

    if (! (*wxmax > 0.f && *wymax > 0.f)) {
	msgdmp_("E", "SLINIT", "WIDTH OF LAYOUT RECTANGLE IS LESS THAN ZERO.",
		 (ftnlen)1, (ftnlen)6, (ftnlen)44);
    }
    if (! (*fact > 0.f)) {
	msgdmp_("E", "SLINIT", "SCALING FACTOR IS LESS THAN ZERO.", (ftnlen)1,
		 (ftnlen)6, (ftnlen)33);
    }
    slblk1_1.nn[0] = 1;
    slblk1_1.xmin[0] = 0.f;
    slblk1_1.xmax[0] = *wxmax;
    slblk1_1.ymin[0] = 0.f;
    slblk1_1.ymax[0] = *wymax;
    slblk1_1.nn[1] = 1;
    slblk1_1.xmin[1] = 0.f;
    slblk1_1.xmax[1] = *wxmax;
    slblk1_1.ymin[1] = 0.f;
    slblk1_1.ymax[1] = *wymax;
    sgiset_("NLEVEL", &c__1, (ftnlen)6);
    sgrset_("FACTOR", fact, (ftnlen)6);
    return 0;
/* ----------------------------------------------------------------------- */

L_slsize:
    s_copy(csize, cszez, (ftnlen)3, cszez_len);
    *(unsigned char *)cs1 = *(unsigned char *)csize;
    *(unsigned char *)cs2 = *(unsigned char *)&csize[1];
    *(unsigned char *)cs3 = *(unsigned char *)&csize[2];
    if (! (lchreq_(cs1, "A", (ftnlen)1, (ftnlen)1) || lchreq_(cs1, "B", (
	    ftnlen)1, (ftnlen)1))) {
	msgdmp_("E", "SLSIZE", "SIZE PARAMETER IS INVALID.", (ftnlen)1, (
		ftnlen)6, (ftnlen)26);
    }
    if (! lchrd_(cs2, (ftnlen)1)) {
	msgdmp_("E", "SLSIZE", "SIZE PARAMETER IS INVALID.", (ftnlen)1, (
		ftnlen)6, (ftnlen)26);
    }
    slqsiz_(csize, &dxx, &dyy, (ftnlen)3);
    if (lchreq_(cs3, "Y", (ftnlen)1, (ftnlen)1) || lchreq_(cs3, "L", (ftnlen)
	    1, (ftnlen)1)) {
	dx = dxx;
	dy = dyy;
    } else if (lchreq_(cs3, "T", (ftnlen)1, (ftnlen)1) || lchreq_(cs3, "P", (
	    ftnlen)1, (ftnlen)1)) {
	dx = dyy;
	dy = dxx;
    } else {
	if (! (lenc_(csize, (ftnlen)3) == 2 || lchreq_(cs3, "A", (ftnlen)1, (
		ftnlen)1))) {
	    msgdmp_("W", "SLSIZE", "SIZE PARAMETER IS INVALID.", (ftnlen)1, (
		    ftnlen)6, (ftnlen)26);
/* Writing concatenation */
	    i__1[0] = 1, a__1[0] = "'";
	    i__1[1] = 2, a__1[1] = csize;
	    i__1[2] = 1, a__1[2] = "A";
	    i__1[3] = 13, a__1[3] = "' IS ASSUMED.";
	    s_cat(ch__1, a__1, i__1, &c__4, (ftnlen)17);
	    msgdmp_("M", "-CNT.-", ch__1, (ftnlen)1, (ftnlen)6, (ftnlen)17);
	}
	if (slblk1_1.xmax[0] - slblk1_1.xmin[0] > slblk1_1.ymax[0] - 
		slblk1_1.ymin[0]) {
	    dx = dxx;
	    dy = dyy;
	} else {
	    dx = dyy;
	    dy = dxx;
	}
    }
    sgrget_("FACTOR", &factr, (ftnlen)6);
    bwx = (slblk1_1.xmax[1] - slblk1_1.xmin[1]) * factr;
    bwy = (slblk1_1.ymax[1] - slblk1_1.ymin[1]) * factr;
    if (bwx < dx || bwy < dy) {
/* Writing concatenation */
	i__2[0] = 15, a__2[0] = "SELECTED SIZE '";
	i__2[1] = 3, a__2[1] = csize;
	i__2[2] = 21, a__2[2] = "' CANNOT BE ASSIGNED.";
	s_cat(ch__2, a__2, i__2, &c__3, (ftnlen)39);
	msgdmp_("W", "SLSIZE", ch__2, (ftnlen)1, (ftnlen)6, (ftnlen)39);
	msgdmp_("M", "-CNT.-", "DO NOTHING.", (ftnlen)1, (ftnlen)6, (ftnlen)
		11);
	return 0;
    }
    xx = (1 - dx / bwx) / 2;
    yy = (1 - dy / bwy) / 2;
    slmgnz_(slblk1_1.xmin, slblk1_1.xmax, slblk1_1.ymin, slblk1_1.ymax, &xx, &
	    xx, &yy, &yy);
    slblk1_1.xmin[1] = slblk1_1.xmin[0];
    slblk1_1.xmax[1] = slblk1_1.xmax[0];
    slblk1_1.ymin[1] = slblk1_1.ymin[0];
    slblk1_1.ymax[1] = slblk1_1.ymax[0];
    return 0;
/* ----------------------------------------------------------------------- */

L_slform:
    dx = *dxa;
    dy = *dya;
    sgrget_("FACTOR", &factr, (ftnlen)6);
    bwx = (slblk1_1.xmax[1] - slblk1_1.xmin[1]) * factr;
    bwy = (slblk1_1.ymax[1] - slblk1_1.ymin[1]) * factr;
    if (bwx < dx || bwy < dy) {
	msgdmp_("W", "SLFORM", "SELECTED SIZE CANNOT BE ASSIGNED.", (ftnlen)1,
		 (ftnlen)6, (ftnlen)33);
	msgdmp_("M", "-CNT.-", "DO NOTHING.", (ftnlen)1, (ftnlen)6, (ftnlen)
		11);
	return 0;
    }
    xx = (1 - dx / bwx) / 2;
    yy = (1 - dy / bwy) / 2;
    slmgnz_(slblk1_1.xmin, slblk1_1.xmax, slblk1_1.ymin, slblk1_1.ymax, &xx, &
	    xx, &yy, &yy);
    slblk1_1.xmin[1] = slblk1_1.xmin[0];
    slblk1_1.xmax[1] = slblk1_1.xmax[0];
    slblk1_1.ymin[1] = slblk1_1.ymin[0];
    slblk1_1.ymax[1] = slblk1_1.ymax[0];
    return 0;
} /* slinit_ */

/* Subroutine */ int slinit_(real *wxmax, real *wymax, real *fact)
{
    return slinit_0_(0, wxmax, wymax, fact, (char *)0, (real *)0, (real *)0, (
	    ftnint)0);
    }

/* Subroutine */ int slsize_(char *cszez, ftnlen cszez_len)
{
    return slinit_0_(1, (real *)0, (real *)0, (real *)0, cszez, (real *)0, (
	    real *)0, cszez_len);
    }

/* Subroutine */ int slform_(real *dxa, real *dya)
{
    return slinit_0_(2, (real *)0, (real *)0, (real *)0, (char *)0, dxa, dya, 
	    (ftnint)0);
    }


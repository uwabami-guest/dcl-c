/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Common Block Declarations */

struct {
    real xmin[1000], xmax[1000], ymin[1000], ymax[1000];
    integer nn[4];
} slblk1_;

#define slblk1_1 slblk1_

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int slpage_(integer *lev, integer *ifrm, integer *ipage)
{
    static integer levc;
    extern /* Subroutine */ int sgiget_(char *, integer *, ftnlen), msgdmp_(
	    char *, char *, char *, ftnlen, ftnlen, ftnlen);

    sgiget_("NLEVEL", &levc, (ftnlen)6);
    if (! (0 <= *lev && *lev <= levc)) {
	msgdmp_("E", "SLPAGE", "LEVEL NUMBER IS INVALID.", (ftnlen)1, (ftnlen)
		6, (ftnlen)24);
    }
    if (*ifrm <= 0) {
	msgdmp_("E", "SLPAGE", "FRAME NUMBER IS LESS THAN ZERO.", (ftnlen)1, (
		ftnlen)6, (ftnlen)31);
    }
    *ipage = (*ifrm - 1) / slblk1_1.nn[*lev] + 1;
    *ifrm = (*ifrm - 1) % slblk1_1.nn[*lev] + 1;
    return 0;
} /* slpage_ */


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     SLPWWR : PLOT WORKSTATION-WINDOW RECTANGLE */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int slpwwr_(integer *index)
{
    static integer itr;
    static real rxmin, rymin, rxmax, rymax, wxmin, wymin, wxmax, wymax;
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), szcllr_(void), szpllr_(real *, real *), szslti_(
	    integer *, integer *), szoplr_(void), szmvlr_(real *, real *), 
	    stqwtr_(real *, real *, real *, real *, real *, real *, real *, 
	    real *, integer *);

    if (*index <= 0) {
	if (*index == 0) {
	    msgdmp_("M", "SLPWWR", "LINE INDEX IS ZERO / DO NOTHING.", (
		    ftnlen)1, (ftnlen)6, (ftnlen)32);
	    return 0;
	} else {
	    msgdmp_("E", "SLPWWR", "LINE INDEX IS LESS THAN ZERO.", (ftnlen)1,
		     (ftnlen)6, (ftnlen)29);
	}
    }
    stqwtr_(&rxmin, &rxmax, &rymin, &rymax, &wxmin, &wxmax, &wymin, &wymax, &
	    itr);
    szslti_(&c__1, index);
    szoplr_();
    szmvlr_(&rxmin, &rymin);
    szpllr_(&rxmax, &rymin);
    szpllr_(&rxmax, &rymax);
    szpllr_(&rxmin, &rymax);
    szpllr_(&rxmin, &rymin);
    szcllr_();
    return 0;
} /* slpwwr_ */


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     LINE SUBPRIMITIVE */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int szlnop_0_(int n__, integer *index)
{
    /* Builtin functions */
    integer s_wsfi(icilist *), do_fio(integer *, char *, ftnlen), e_wsfi(void)
	    ;

    /* Local variables */
    static char cobj[80];
    extern /* Subroutine */ int cdblk_(char *, ftnlen), swocls_(char *, 
	    ftnlen), swoopn_(char *, char *, ftnlen, ftnlen), szslti_(integer 
	    *, integer *);

    /* Fortran I/O blocks */
    static icilist io___2 = { 0, cobj, 0, "(I8)", 80, 1 };


    switch(n__) {
	case 1: goto L_szlncl;
	}

    s_wsfi(&io___2);
    do_fio(&c__1, (char *)&(*index), (ftnlen)sizeof(integer));
    e_wsfi();
    cdblk_(cobj, (ftnlen)80);
    swoopn_("SZLN", cobj, (ftnlen)4, (ftnlen)80);
    szslti_(&c__1, index);
    return 0;
/* ----------------------------------------------------------------------- */

L_szlncl:
    swocls_("SZLN", (ftnlen)4);
    return 0;
} /* szlnop_ */

/* Subroutine */ int szlnop_(integer *index)
{
    return szlnop_0_(0, index);
    }

/* Subroutine */ int szlncl_(void)
{
    return szlnop_0_(1, (integer *)0);
    }


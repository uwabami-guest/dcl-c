/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     TONE ROUTINE ON UC */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int szoptu_0_(int n__, real *ux, real *uy)
{
    static real tx, ty, xx, vx, yy, vy, ux0, ux1, uy1, uy0;
    static logical lmap, lcont;
    extern /* Subroutine */ int stfrad_(real *, real *, real *, real *);
    static logical lfirst;
    extern /* Subroutine */ int szgipl_(real *, real *, logical *), stftrn_(
	    real *, real *, real *, real *), szpipl_(real *, real *, real *, 
	    real *, integer *), stfrot_(real *, real *, real *, real *), 
	    stqtrf_(logical *), szcltt_(void), szcltv_(void), szoptt_(void), 
	    szoptv_(void), szsttt_(real *, real *), szsttv_(real *, real *);

    switch(n__) {
	case 1: goto L_szsttu;
	case 2: goto L_szcltu;
	}

    stqtrf_(&lmap);
    if (lmap) {
	szoptt_();
    } else {
	szoptv_();
    }
    lfirst = TRUE_;
    return 0;
/* ----------------------------------------------------------------------- */

L_szsttu:
    stfrad_(ux, uy, &ux1, &uy1);
    if (lfirst) {
	lfirst = FALSE_;
	ux0 = ux1;
	uy0 = uy1;
    }
    szpipl_(&ux0, &uy0, &ux1, &uy1, &c__1);
L10:
    szgipl_(&xx, &yy, &lcont);
    if (lmap) {
	stfrot_(&xx, &yy, &tx, &ty);
	szsttt_(&tx, &ty);
    } else {
	stftrn_(&xx, &yy, &vx, &vy);
	szsttv_(&vx, &vy);
    }
    if (lcont) {
	goto L10;
    }
    ux0 = ux1;
    uy0 = uy1;
    return 0;
/* ----------------------------------------------------------------------- */

L_szcltu:
    if (lmap) {
	szcltt_();
    } else {
	szcltv_();
    }
    return 0;
} /* szoptu_ */

/* Subroutine */ int szoptu_(void)
{
    return szoptu_0_(0, (real *)0, (real *)0);
    }

/* Subroutine */ int szsttu_(real *ux, real *uy)
{
    return szoptu_0_(1, ux, uy);
    }

/* Subroutine */ int szcltu_(void)
{
    return szoptu_0_(2, (real *)0, (real *)0);
    }


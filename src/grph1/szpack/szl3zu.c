/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Common Block Declarations */

struct {
    logical lmiss;
    real rmiss;
} szbpl1_;

#define szbpl1_1 szbpl1_

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int szl3zu_(integer *n, real *u3x, real *u3y, real *u3z)
{
    /* System generated locals */
    integer i__1;

    /* Local variables */
    static integer i__;
    static real vx, vy, vz;
    static logical lflag;
    extern /* Subroutine */ int szcll3_(void), stftr3_(real *, real *, real *,
	     real *, real *, real *), szpll3_(real *, real *, real *), 
	    szopl3_(void), szmvl3_(real *, real *, real *);

    /* Parameter adjustments */
    --u3z;
    --u3y;
    --u3x;

    /* Function Body */
    szopl3_();
    if (! szbpl1_1.lmiss) {
	stftr3_(&u3x[1], &u3y[1], &u3z[1], &vx, &vy, &vz);
	szmvl3_(&vx, &vy, &vz);
	i__1 = *n;
	for (i__ = 2; i__ <= i__1; ++i__) {
	    stftr3_(&u3x[i__], &u3y[i__], &u3z[i__], &vx, &vy, &vz);
	    szpll3_(&vx, &vy, &vz);
/* L10: */
	}
    } else {
	lflag = FALSE_;
	i__1 = *n;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    if (u3x[i__] == szbpl1_1.rmiss || u3y[i__] == szbpl1_1.rmiss || 
		    u3z[i__] == szbpl1_1.rmiss) {
		lflag = FALSE_;
	    } else if (lflag) {
		stftr3_(&u3x[i__], &u3y[i__], &u3z[i__], &vx, &vy, &vz);
		szpll3_(&vx, &vy, &vz);
	    } else {
		stftr3_(&u3x[i__], &u3y[i__], &u3z[i__], &vx, &vy, &vz);
		szmvl3_(&vx, &vy, &vz);
		lflag = TRUE_;
	    }
/* L20: */
	}
    }
    szcll3_();
    return 0;
} /* szl3zu_ */


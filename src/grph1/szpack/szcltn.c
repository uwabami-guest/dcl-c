/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__3 = 3;
static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     COLOR TO TONE CONVERSION TABLE LOADING */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int szcltn_(integer *icpat, integer *maxcli)
{
    /* System generated locals */
    integer i__1;
    olist o__1;
    cllist cl__1;
    alist al__1;

    /* Builtin functions */
    integer s_cmp(char *, char *, ftnlen, ftnlen), f_open(olist *), f_rew(
	    alist *), s_rsle(cilist *), do_lio(integer *, integer *, char *, 
	    ftnlen), e_rsle(void), f_clos(cllist *);

    /* Local variables */
    static integer i__, iu, ici, ios, ipat;
    static char cfile[1024];
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen);
    extern integer iufopn_(void);
    extern /* Subroutine */ int swqfnm_(char *, char *, ftnlen, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___5 = { 1, 0, 1, 0, 0 };


/*     / INITIALIZE / */
    /* Parameter adjustments */
    --icpat;

    /* Function Body */
    i__1 = *maxcli;
    for (i__ = 1; i__ <= i__1; ++i__) {
	icpat[i__] = -1;
/* L10: */
    }
/*     / OPEN TABLE FILE : MAYBE SYSTEM DEPENDENT / */
    swqfnm_("CL2TN", cfile, (ftnlen)5, (ftnlen)1024);
    if (s_cmp(cfile, " ", (ftnlen)1024, (ftnlen)1) == 0) {
	msgdmp_("E", "SZCLTN", "COLOR TO TONE CONVERSION TABLE DOES NOT EXIS"
		"T.", (ftnlen)1, (ftnlen)6, (ftnlen)46);
    }
    iu = iufopn_();
    o__1.oerr = 0;
    o__1.ounit = iu;
    o__1.ofnmlen = 1024;
    o__1.ofnm = cfile;
    o__1.orl = 0;
    o__1.osta = 0;
    o__1.oacc = 0;
    o__1.ofm = "FORMATTED";
    o__1.oblnk = 0;
    f_open(&o__1);
    al__1.aerr = 0;
    al__1.aunit = iu;
    f_rew(&al__1);
/*     / LOAD CONVERSION TABLE / */
L20:
    io___5.ciunit = iu;
    ios = s_rsle(&io___5);
    if (ios != 0) {
	goto L100001;
    }
    ios = do_lio(&c__3, &c__1, (char *)&ici, (ftnlen)sizeof(integer));
    if (ios != 0) {
	goto L100001;
    }
    ios = do_lio(&c__3, &c__1, (char *)&ipat, (ftnlen)sizeof(integer));
    if (ios != 0) {
	goto L100001;
    }
    ios = e_rsle();
L100001:
    if (ici > *maxcli || ici <= 0) {
	msgdmp_("E", "SZCLTN", "COLOR INDEX IS INVALID", (ftnlen)1, (ftnlen)6,
		 (ftnlen)22);
    }
    icpat[ici] = ipat;
    if (ios == 0) {
	goto L20;
    }
/*     / CLOSE TABLE FILE / */
    cl__1.cerr = 0;
    cl__1.cunit = iu;
    cl__1.csta = 0;
    f_clos(&cl__1);
    return 0;
} /* szcltn_ */


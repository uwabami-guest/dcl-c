/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     PLOT ROUTINE ON RC (SOLID LINE) */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int szoplz_0_(int n__, real *rx, real *ry)
{
    static real wx, wy;
    extern /* Subroutine */ int swgcls_(void), swgopn_(void), swgplt_(real *, 
	    real *), swgmov_(real *, real *), stfwtr_(real *, real *, real *, 
	    real *);

    switch(n__) {
	case 1: goto L_szmvlz;
	case 2: goto L_szpllz;
	case 3: goto L_szcllz;
	}

    swgopn_();
    return 0;
/* ----------------------------------------------------------------------- */

L_szmvlz:
    stfwtr_(rx, ry, &wx, &wy);
    swgmov_(&wx, &wy);
    return 0;
/* ----------------------------------------------------------------------- */

L_szpllz:
    stfwtr_(rx, ry, &wx, &wy);
    swgplt_(&wx, &wy);
    return 0;
/* ----------------------------------------------------------------------- */

L_szcllz:
    swgcls_();
    return 0;
} /* szoplz_ */

/* Subroutine */ int szoplz_(void)
{
    return szoplz_0_(0, (real *)0, (real *)0);
    }

/* Subroutine */ int szmvlz_(real *rx, real *ry)
{
    return szoplz_0_(1, rx, ry);
    }

/* Subroutine */ int szpllz_(real *rx, real *ry)
{
    return szoplz_0_(2, rx, ry);
    }

/* Subroutine */ int szcllz_(void)
{
    return szoplz_0_(3, (real *)0, (real *)0);
    }


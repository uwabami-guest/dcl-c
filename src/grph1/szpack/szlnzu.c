/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int szlnzu_(real *ux1, real *uy1, real *ux2, real *uy2)
{
    extern /* Subroutine */ int szcllu_(void), szpllu_(real *, real *), 
	    szoplu_(void), szmvlu_(real *, real *);

    szoplu_();
    szmvlu_(ux1, uy1);
    szpllu_(ux2, uy2);
    szcllu_();
    return 0;
} /* szlnzu_ */


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Common Block Declarations */

struct {
    logical larrwz, lpropz;
    real afactz, constz, anglez;
    logical latonz, luarwz;
    real consmz, rdunit;
} szbla1_;

#define szbla1_1 szbla1_

struct {
    logical lclipl;
} szbls2_;

#define szbls2_1 szbls2_

struct {
    logical lclipt;
} szbtx3_;

#define szbtx3_1 szbtx3_

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int szlazr_(real *rx1, real *ry1, real *rx2, real *ry2)
{
    /* System generated locals */
    real r__1, r__2;

    /* Builtin functions */
    double sqrt(doublereal);

    /* Local variables */
    static real r__, ar, pi, xe, ye, xa1, ya1, xa2, ya2;
    extern /* Subroutine */ int cr2c_(real *, real *, real *, real *, real *);
    extern real rfpi_(void);
    extern /* Subroutine */ int stepr2_(void), strpr2_(void);
    static logical lclplz, lclptz;
    extern /* Subroutine */ int szcllv_(void), szcltv_(void), szpllv_(real *, 
	    real *), szoplv_(void), szmvlv_(real *, real *), szoptv_(void), 
	    szsttv_(real *, real *);

/* Computing 2nd power */
    r__1 = *rx2 - *rx1;
/* Computing 2nd power */
    r__2 = *ry2 - *ry1;
    r__ = sqrt(r__1 * r__1 + r__2 * r__2);
    if (r__ == 0.f) {
	return 0;
    }
    pi = rfpi_();
    lclplz = szbls2_1.lclipl;
    lclptz = szbtx3_1.lclipt;
    szbls2_1.lclipl = FALSE_;
    szbtx3_1.lclipt = FALSE_;
    stepr2_();
    szoplv_();
    szmvlv_(rx1, ry1);
    szpllv_(rx2, ry2);
    szcllv_();
    if (! szbla1_1.larrwz) {
	goto L100;
    }
    if (szbla1_1.lpropz) {
	ar = r__ * szbla1_1.afactz;
    } else {
	ar = szbla1_1.constz;
    }
    xe = (*rx2 - *rx1) / r__ * ar;
    ye = (*ry2 - *ry1) / r__ * ar;
    r__1 = -(pi - szbla1_1.anglez * szbla1_1.rdunit);
    cr2c_(&r__1, &xe, &ye, &xa1, &ya1);
    r__1 = -(pi + szbla1_1.anglez * szbla1_1.rdunit);
    cr2c_(&r__1, &xe, &ye, &xa2, &ya2);
    if (szbla1_1.latonz) {
	szoptv_();
	r__1 = *rx2 + xa1;
	r__2 = *ry2 + ya1;
	szsttv_(&r__1, &r__2);
	szsttv_(rx2, ry2);
	r__1 = *rx2 + xa2;
	r__2 = *ry2 + ya2;
	szsttv_(&r__1, &r__2);
	szcltv_();
    } else {
	szoplv_();
	r__1 = *rx2 + xa1;
	r__2 = *ry2 + ya1;
	szmvlv_(&r__1, &r__2);
	szpllv_(rx2, ry2);
	r__1 = *rx2 + xa2;
	r__2 = *ry2 + ya2;
	szpllv_(&r__1, &r__2);
	szcllv_();
    }
L100:
    szbls2_1.lclipl = lclplz;
    szbtx3_1.lclipt = lclptz;
    strpr2_();
    return 0;
} /* szlazr_ */


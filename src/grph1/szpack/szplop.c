/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Common Block Declarations */

struct {
    logical lmiss;
    real rmiss;
} szbpl1_;

#define szbpl1_1 szbpl1_

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     POLYLINE PRIMITIVE */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int szplop_0_(int n__, integer *itype, integer *index)
{
    /* Builtin functions */
    integer s_wsfi(icilist *), do_fio(integer *, char *, ftnlen), e_wsfi(void)
	    ;

    /* Local variables */
    static char cobj[80];
    extern /* Subroutine */ int cdblk_(char *, ftnlen), gllget_(char *, 
	    logical *, ftnlen), glrget_(char *, real *, ftnlen), swocls_(char 
	    *, ftnlen), swoopn_(char *, char *, ftnlen, ftnlen), szslti_(
	    integer *, integer *);

    /* Fortran I/O blocks */
    static icilist io___2 = { 0, cobj, 0, "(2I8)", 80, 1 };


    switch(n__) {
	case 1: goto L_szplcl;
	}

    gllget_("LMISS", &szbpl1_1.lmiss, (ftnlen)5);
    glrget_("RMISS", &szbpl1_1.rmiss, (ftnlen)5);
    s_wsfi(&io___2);
    do_fio(&c__1, (char *)&(*itype), (ftnlen)sizeof(integer));
    do_fio(&c__1, (char *)&(*index), (ftnlen)sizeof(integer));
    e_wsfi();
    cdblk_(cobj, (ftnlen)80);
    swoopn_("SZPL", cobj, (ftnlen)4, (ftnlen)80);
    szslti_(itype, index);
    return 0;
/* ----------------------------------------------------------------------- */

L_szplcl:
    swocls_("SZPL", (ftnlen)4);
    return 0;
} /* szplop_ */

/* Subroutine */ int szplop_(integer *itype, integer *index)
{
    return szplop_0_(0, itype, index);
    }

/* Subroutine */ int szplcl_(void)
{
    return szplop_0_(1, (integer *)0, (integer *)0);
    }


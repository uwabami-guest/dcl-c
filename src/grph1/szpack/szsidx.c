/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     LINE INDEX ATTRIBUTE */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int szsidx_0_(int n__, integer *index)
{
    /* Initialized data */

    static logical lfrst = TRUE_;

    /* System generated locals */
    logical L__1;

    /* Local variables */
    static integer ibgcli, ifcidx;
    static logical lfcatr;
    static integer iclidx;
    static logical lclatr;
    extern /* Subroutine */ int sgiget_(char *, integer *, ftnlen), swqfcc_(
	    logical *);
    static integer iwdidx;
    extern /* Subroutine */ int swqclc_(logical *);
    static logical lwdatr;
    static integer indexz;
    extern /* Subroutine */ int swscli_(integer *, logical *), swqwdc_(
	    logical *), swswdi_(integer *);

    switch(n__) {
	case 1: goto L_szqidx;
	}

    indexz = *index;
    if (lfrst) {
	swqwdc_(&lwdatr);
	swqclc_(&lclatr);
	lfrst = FALSE_;
    }
    sgiget_("IBGCLI", &ibgcli, (ftnlen)6);
    sgiget_("IFCIDX", &ifcidx, (ftnlen)6);
    iwdidx = indexz % 10;
    iclidx = indexz / 10;
    if (iclidx == ibgcli) {
	iclidx = 0;
    } else {
	iclidx %= 1000;
	if (iclidx == 0) {
	    iclidx = 1;
	}
    }
    if (! lclatr && iwdidx == 0) {
	iwdidx = iclidx;
    } else if (! lwdatr && iclidx == 0) {
	iclidx = iwdidx;
    }
    swswdi_(&iwdidx);
    swqfcc_(&lfcatr);
    L__1 = ! lfcatr;
    swscli_(&iclidx, &L__1);
    return 0;
/* ----------------------------------------------------------------------- */

L_szqidx:
    *index = indexz;
    return 0;
} /* szsidx_ */

/* Subroutine */ int szsidx_(integer *index)
{
    return szsidx_0_(0, index);
    }

/* Subroutine */ int szqidx_(integer *index)
{
    return szsidx_0_(1, index);
    }


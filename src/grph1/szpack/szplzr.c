/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Common Block Declarations */

struct {
    logical lmiss;
    real rmiss;
} szbpl1_;

#define szbpl1_1 szbpl1_

struct {
    logical lclip;
} szbls2_;

#define szbls2_1 szbls2_

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int szplzr_(integer *n, real *rpx, real *rpy)
{
    /* System generated locals */
    integer i__1;

    /* Local variables */
    static integer i__;
    static logical lflag;
    extern /* Subroutine */ int stepr2_(void), strpr2_(void);
    static logical lclipz;
    extern /* Subroutine */ int szcllv_(void), szpllv_(real *, real *), 
	    szoplv_(void), szmvlv_(real *, real *);

    /* Parameter adjustments */
    --rpy;
    --rpx;

    /* Function Body */
    lclipz = szbls2_1.lclip;
    szbls2_1.lclip = FALSE_;
    stepr2_();
    szoplv_();
    if (! szbpl1_1.lmiss) {
	szmvlv_(&rpx[1], &rpy[1]);
	i__1 = *n;
	for (i__ = 2; i__ <= i__1; ++i__) {
	    szpllv_(&rpx[i__], &rpy[i__]);
/* L10: */
	}
    } else {
	lflag = FALSE_;
	i__1 = *n;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    if (rpx[i__] == szbpl1_1.rmiss || rpy[i__] == szbpl1_1.rmiss) {
		lflag = FALSE_;
	    } else if (lflag) {
		szpllv_(&rpx[i__], &rpy[i__]);
	    } else {
		szmvlv_(&rpx[i__], &rpy[i__]);
		lflag = TRUE_;
	    }
/* L20: */
	}
    }
    szcllv_();
    szbls2_1.lclip = lclipz;
    strpr2_();
    return 0;
} /* szplzr_ */


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Common Block Declarations */

struct {
    real qsize, ct, st;
    integer icentz;
} szbtx1_;

#define szbtx1_1 szbtx1_

struct {
    logical lcntl;
    integer jsup, jsub, jrst;
    real small, shift;
} szbtx2_;

#define szbtx2_1 szbtx2_

struct {
    logical lclip;
} szbtx3_;

#define szbtx3_1 szbtx3_

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int sztxop_0_(int n__, real *rsize, integer *irota, integer *
	icent, integer *index)
{
    /* System generated locals */
    real r__1;

    /* Builtin functions */
    double cos(doublereal), sin(doublereal);
    integer s_wsfi(icilist *), do_fio(integer *, char *, ftnlen), e_wsfi(void)
	    ;

    /* Local variables */
    static integer iws;
    extern real rd2r_(real *);
    static char cobj[80];
    static integer isub, isup, irst;
    extern /* Subroutine */ int cdblk_(char *, ftnlen);
    static real theta;
    static integer ifont;
    extern /* Subroutine */ int sgiget_(char *, integer *, ftnlen), sglget_(
	    char *, logical *, ftnlen), sgrget_(char *, real *, ftnlen);
    static integer indexz;
    extern /* Subroutine */ int swocls_(char *, ftnlen), szqidx_(integer *), 
	    szsidx_(integer *), swoopn_(char *, char *, ftnlen, ftnlen);

    /* Fortran I/O blocks */
    static icilist io___9 = { 0, cobj, 0, "(F8.5,5I8)", 80, 1 };


    switch(n__) {
	case 1: goto L_sztxcl;
	}

    sgiget_("IFONT", &ifont, (ftnlen)5);
    szbtx1_1.qsize = *rsize / 24.f;
    r__1 = (real) (*irota);
    theta = rd2r_(&r__1);
    szbtx1_1.ct = szbtx1_1.qsize * cos(theta);
    szbtx1_1.st = szbtx1_1.qsize * sin(theta);
    szbtx1_1.icentz = *icent;
    sglget_("LCNTL", &szbtx2_1.lcntl, (ftnlen)5);
    sgiget_("ISUP", &isup, (ftnlen)4);
    sgiget_("ISUB", &isub, (ftnlen)4);
    sgiget_("IRST", &irst, (ftnlen)4);
    sgrget_("SMALL", &szbtx2_1.small, (ftnlen)5);
    sgrget_("SHIFT", &szbtx2_1.shift, (ftnlen)5);
    sglget_("LCLIP", &szbtx3_1.lclip, (ftnlen)5);
    sgiget_("IWS", &iws, (ftnlen)3);
    szbtx2_1.jsup = isup + 1;
    szbtx2_1.jsub = isub + 1;
    szbtx2_1.jrst = irst + 1;
    szqidx_(&indexz);
    szsidx_(index);
    s_wsfi(&io___9);
    do_fio(&c__1, (char *)&(*rsize), (ftnlen)sizeof(real));
    do_fio(&c__1, (char *)&(*irota), (ftnlen)sizeof(integer));
    do_fio(&c__1, (char *)&(*icent), (ftnlen)sizeof(integer));
    do_fio(&c__1, (char *)&(*index), (ftnlen)sizeof(integer));
    do_fio(&c__1, (char *)&ifont, (ftnlen)sizeof(integer));
    do_fio(&c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_wsfi();
/*     WRITE(COBJ,'(F8.5,5I8)') RSIZE,IROTA,ICENT,INDEX,IWS */
    cdblk_(cobj, (ftnlen)80);
    swoopn_("SZTX", cobj, (ftnlen)4, (ftnlen)80);
    return 0;
/* ----------------------------------------------------------------------- */

L_sztxcl:
    szsidx_(&indexz);
    swocls_("SZTX", (ftnlen)4);
    return 0;
} /* sztxop_ */

/* Subroutine */ int sztxop_(real *rsize, integer *irota, integer *icent, 
	integer *index)
{
    return sztxop_0_(0, rsize, irota, icent, index);
    }

/* Subroutine */ int sztxcl_(void)
{
    return sztxop_0_(1, (real *)0, (integer *)0, (integer *)0, (integer *)0);
    }


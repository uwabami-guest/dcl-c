/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static logical c_true = TRUE_;
static real c_b4 = 1e-5f;
static integer c__0 = 0;

/* ----------------------------------------------------------------------- */
/*     PLOT ROUTINE ON TC (GREAT CIRCLE INTERPOLATION) */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int szoplt_0_(int n__, real *tx, real *ty)
{
    static real vx, vy, xx, yy, bx0, by0, cx0, cy0, bx1, by1, cx1, cy1, tx0, 
	    ty0, tx1, ty1;
    extern logical lreqa_(real *, real *, real *);
    static logical lvldx, lvldy, lcont1, lcont2, lcont3, lfirst, lmovex;
    extern /* Subroutine */ int szgcly_(real *, real *, logical *), szgclx_(
	    real *, real *, logical *, logical *);
    extern real xmplon_(real *);
    extern /* Subroutine */ int szcllv_(void), stftrn_(real *, real *, real *,
	     real *), szgipt_(real *, real *, logical *), szpclx_(real *, 
	    real *, real *, real *, logical *, logical *), szpcly_(real *, 
	    real *, real *, real *, logical *, logical *), szpipt_(real *, 
	    real *, real *, real *, integer *), szpllv_(real *, real *), 
	    szoplv_(void), szmvlv_(real *, real *);

    switch(n__) {
	case 1: goto L_szmvlt;
	case 2: goto L_szpllt;
	case 3: goto L_szcllt;
	}

    szoplv_();
    return 0;
/* ----------------------------------------------------------------------- */

L_szmvlt:
    tx0 = xmplon_(tx);
    ty0 = *ty;
    lfirst = TRUE_;
    szpcly_(&tx0, &ty0, &tx0, &ty0, &lvldy, &c_true);
    szpclx_(&tx0, &ty0, &tx0, &ty0, &lvldx, &c_true);
    if (lvldx && lvldy) {
	stftrn_(&tx0, &ty0, &vx, &vy);
	szmvlv_(&vx, &vy);
	lfirst = FALSE_;
    }
    bx0 = tx0;
    by0 = ty0;
    cx0 = tx0;
    cy0 = ty0;
    return 0;
/* ----------------------------------------------------------------------- */

L_szpllt:
    tx1 = xmplon_(tx);
    ty1 = *ty;
    if (! lfirst && lreqa_(&tx1, &tx0, &c_b4) && lreqa_(&ty1, &ty0, &c_b4)) {
	return 0;
    }
    szpipt_(&tx0, &ty0, &tx1, &ty1, &c__0);
L10:
    szgipt_(&bx1, &by1, &lcont1);
    szpcly_(&bx0, &by0, &bx1, &by1, &lvldy, &c_true);
    if (lvldy) {
L20:
	szgcly_(&cx1, &cy1, &lcont2);
	cx1 = xmplon_(&cx1);
	szpclx_(&cx0, &cy0, &cx1, &cy1, &lvldx, &c_true);
	if (lvldx) {
L30:
	    szgclx_(&xx, &yy, &lcont3, &lmovex);
	    stftrn_(&xx, &yy, &vx, &vy);
	    if (lmovex || lcont2 || lfirst) {
		szmvlv_(&vx, &vy);
		lfirst = FALSE_;
	    } else {
		szpllv_(&vx, &vy);
	    }
	    if (lcont3) {
		goto L30;
	    }
	}
	cx0 = cx1;
	cy0 = cy1;
	if (lcont2) {
	    goto L20;
	}
    }
    bx0 = bx1;
    by0 = by1;
    if (lcont1) {
	goto L10;
    }
    tx0 = tx1;
    ty0 = ty1;
    return 0;
/* ----------------------------------------------------------------------- */

L_szcllt:
    szcllv_();
    return 0;
} /* szoplt_ */

/* Subroutine */ int szoplt_(void)
{
    return szoplt_0_(0, (real *)0, (real *)0);
    }

/* Subroutine */ int szmvlt_(real *tx, real *ty)
{
    return szoplt_0_(1, tx, ty);
    }

/* Subroutine */ int szpllt_(real *tx, real *ty)
{
    return szoplt_0_(2, tx, ty);
    }

/* Subroutine */ int szcllt_(void)
{
    return szoplt_0_(3, (real *)0, (real *)0);
    }


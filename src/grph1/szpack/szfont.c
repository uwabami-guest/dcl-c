/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     FONT LOADING */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int szfont_(integer *ifont, integer *iposx, integer *nchar, 
	char *ckx, char *cky, integer *len, ftnlen ckx_len, ftnlen cky_len)
{
    /* System generated locals */
    integer iposx_dim1, ckx_dim1, cky_dim1, i__1;
    olist o__1;
    cllist cl__1;
    alist al__1;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_cmp(char *, char *, ftnlen, ftnlen), f_open(olist *), f_rew(
	    alist *), s_rsue(cilist *), do_uio(integer *, char *, ftnlen), 
	    e_rsue(void), f_clos(cllist *);

    /* Local variables */
    static integer iu;
    static char cdsn[1024], cpara[8];
    extern /* Subroutine */ int chngi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen), msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen);
    extern integer iufopn_(void);
    extern /* Subroutine */ int swqfnm_(char *, char *, ftnlen, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___4 = { 0, 0, 0, 0, 0 };
    static cilist io___5 = { 0, 0, 0, 0, 0 };
    static cilist io___6 = { 0, 0, 0, 0, 0 };


/*     / CHECK FONT NUMBER / */
    /* Parameter adjustments */
    iposx_dim1 = *nchar;
    --iposx;
    cky_dim1 = *len;
    --cky;
    ckx_dim1 = *len;
    --ckx;

    /* Function Body */
    if (! (1 <= *ifont && *ifont <= 2)) {
	msgdmp_("E", "SZFONT", "FONT NUMBER IS OUT OF RANGE.", (ftnlen)1, (
		ftnlen)6, (ftnlen)28);
    }
/*     / INQUIRE FONT FILE NAMES / */
    s_copy(cpara, "FONT#", (ftnlen)8, (ftnlen)5);
    chngi_(cpara, "#", ifont, "(I1)", (ftnlen)8, (ftnlen)1, (ftnlen)4);
    swqfnm_(cpara, cdsn, (ftnlen)8, (ftnlen)1024);
    if (s_cmp(cdsn, " ", (ftnlen)1024, (ftnlen)1) == 0) {
	msgdmp_("E", "SZFONT", "FONT FILE DOES NOT EXIST.", (ftnlen)1, (
		ftnlen)6, (ftnlen)25);
    }
/*     / OPEN FONT FILE : MAYBE SYSTEM DEPENDENT / */
    iu = iufopn_();
    o__1.oerr = 0;
    o__1.ounit = iu;
    o__1.ofnmlen = 1024;
    o__1.ofnm = cdsn;
    o__1.orl = 0;
    o__1.osta = 0;
    o__1.oacc = 0;
    o__1.ofm = "UNFORMATTED";
    o__1.oblnk = 0;
    f_open(&o__1);
    al__1.aerr = 0;
    al__1.aunit = iu;
    f_rew(&al__1);
/*     / LOAD TEXT FONT / */
    io___4.ciunit = iu;
    s_rsue(&io___4);
    i__1 = 1 * iposx_dim1;
    do_uio(&i__1, (char *)&iposx[1], (ftnlen)sizeof(integer));
    e_rsue();
    io___5.ciunit = iu;
    s_rsue(&io___5);
    i__1 = 1 * ckx_dim1;
    do_uio(&i__1, ckx + 1, (ftnlen)1);
    e_rsue();
    io___6.ciunit = iu;
    s_rsue(&io___6);
    i__1 = 1 * cky_dim1;
    do_uio(&i__1, cky + 1, (ftnlen)1);
    e_rsue();
/*     / CLOSE FONT FILE / */
    cl__1.cerr = 0;
    cl__1.cunit = iu;
    cl__1.csta = 0;
    f_clos(&cl__1);
    return 0;
} /* szfont_ */


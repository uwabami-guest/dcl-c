/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Common Block Declarations */

struct {
    logical lmiss;
    real rmiss;
    integer npm;
} szbpm1_;

#define szbpm1_1 szbpm1_

struct {
    char cmark[1];
} szbpm2_;

#define szbpm2_1 szbpm2_

struct {
    logical lclip;
} szbtx3_;

#define szbtx3_1 szbtx3_

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int szm3zu_(integer *n, real *upx, real *upy, real *upz)
{
    /* System generated locals */
    integer i__1, i__2;

    /* Local variables */
    static integer i__;
    static real rx, ry, vx, vy, vz;
    static logical lflag;
    extern /* Subroutine */ int stepr2_(void), stfpr3_(real *, real *, real *,
	     real *, real *), stftr3_(real *, real *, real *, real *, real *, 
	    real *), strpr2_(void);
    static logical lclipz;
    extern /* Subroutine */ int sztxzv_(real *, real *, char *, ftnlen);

    /* Parameter adjustments */
    --upz;
    --upy;
    --upx;

    /* Function Body */
    lclipz = szbtx3_1.lclip;
    szbtx3_1.lclip = FALSE_;
    stepr2_();
    i__1 = *n;
    i__2 = szbpm1_1.npm;
    for (i__ = 1; i__2 < 0 ? i__ >= i__1 : i__ <= i__1; i__ += i__2) {
	lflag = szbpm1_1.lmiss && (upx[i__] == szbpm1_1.rmiss || upy[i__] == 
		szbpm1_1.rmiss || upz[i__] == szbpm1_1.rmiss);
	if (! lflag) {
	    stftr3_(&upx[i__], &upy[i__], &upz[i__], &vx, &vy, &vz);
	    stfpr3_(&vx, &vy, &vz, &rx, &ry);
	    sztxzv_(&rx, &ry, szbpm2_1.cmark, (ftnlen)1);
	}
/* L10: */
    }
    szbtx3_1.lclip = lclipz;
    strpr2_();
    return 0;
} /* szm3zu_ */


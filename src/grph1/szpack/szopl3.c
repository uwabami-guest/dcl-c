/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     PLOT ROUTINE ON THE 3-D COORDINATE */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int szopl3_0_(int n__, real *sx, real *sy, real *sz)
{
    static real rx, ry;
    extern /* Subroutine */ int stfpr3_(real *, real *, real *, real *, real *
	    ), szcllr_(void), szpllr_(real *, real *), szoplr_(void), szmvlr_(
	    real *, real *);

    switch(n__) {
	case 1: goto L_szmvl3;
	case 2: goto L_szpll3;
	case 3: goto L_szcll3;
	}

    szoplr_();
    return 0;
/* ----------------------------------------------------------------------- */

L_szmvl3:
    stfpr3_(sx, sy, sz, &rx, &ry);
    szmvlr_(&rx, &ry);
    return 0;
/* ----------------------------------------------------------------------- */

L_szpll3:
    stfpr3_(sx, sy, sz, &rx, &ry);
    szpllr_(&rx, &ry);
    return 0;
/* ----------------------------------------------------------------------- */

L_szcll3:
    szcllr_();
    return 0;
} /* szopl3_ */

/* Subroutine */ int szopl3_(void)
{
    return szopl3_0_(0, (real *)0, (real *)0, (real *)0);
    }

/* Subroutine */ int szmvl3_(real *sx, real *sy, real *sz)
{
    return szopl3_0_(1, sx, sy, sz);
    }

/* Subroutine */ int szpll3_(real *sx, real *sy, real *sz)
{
    return szopl3_0_(2, sx, sy, sz);
    }

/* Subroutine */ int szcll3_(void)
{
    return szopl3_0_(3, (real *)0, (real *)0, (real *)0);
    }


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     PLOT ROUTINE ON VC (DASH LINE) */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int szopld_0_(int n__, real *vx, real *vy, integer *ipat)
{
    /* Initialized data */

    static logical ldash = FALSE_;

    /* System generated locals */
    integer i__1, i__2;
    real r__1, r__2;

    /* Builtin functions */
    double sqrt(doublereal), r_mod(real *, real *);

    /* Local variables */
    static real a[32], c__;
    static integer i__, j, l, m[32];
    static real s, t, bl;
    static integer na, nb;
    static real dx, dy;
    static integer ix;
    static real vxm, vym;
    static integer jpat, move, mode1, modei, modex;
    static logical lopen;
    extern /* Subroutine */ int sgiget_(char *, integer *, ftnlen);
    static integer isolid;
    extern integer ishift_(integer *, integer *);
    extern /* Subroutine */ int sgrget_(char *, real *, ftnlen), szcllp_(void)
	    , szpllp_(real *, real *), szoplp_(void), szmvlp_(real *, real *);

    switch(n__) {
	case 1: goto L_szmvld;
	case 2: goto L_szplld;
	case 3: goto L_szclld;
	case 4: goto L_szstyz;
	case 5: goto L_szqtyz;
	}

/*     / OPEN DASH LINE SEGMENT / */
    lopen = TRUE_;
    szoplp_();
    return 0;
/* ----------------------------------------------------------------------- */

L_szmvld:
/*     / PEN-UP MOVE / */
    szmvlp_(vx, vy);
    if (ldash) {
/*       / DASH / */
	if (lopen) {
/*         / BEGINNING OF DASH LINE / */
	    t = 0.f;
	    lopen = FALSE_;
	} else {
/* Computing 2nd power */
	    r__1 = *vx - vxm;
/* Computing 2nd power */
	    r__2 = *vy - vym;
	    s = sqrt(r__1 * r__1 + r__2 * r__2);
	    if (move == 1) {
		r__1 = s + t;
		t = r_mod(&r__1, &c__);
	    } else if (move == -1) {
		t = 0.f;
	    }
	}
    }
    vxm = *vx;
    vym = *vy;
    return 0;
/* ----------------------------------------------------------------------- */

L_szplld:
/*     / PEN-DOWN MOVE / */
    if (! ldash) {
/*       / SOLID / */
	szpllp_(vx, vy);
    } else {
/*       / DASH / */
/* Computing 2nd power */
	r__1 = *vx - vxm;
/* Computing 2nd power */
	r__2 = *vy - vym;
	s = sqrt(r__1 * r__1 + r__2 * r__2);
	if (s == 0.f) {
	    return 0;
	}
	dx = (*vx - vxm) / s;
	dy = (*vy - vym) / s;
/*       / FIND CURRENT POSITION INDEX / */
	ix = 0;
L10:
	++ix;
	if (a[ix - 1] < t) {
	    goto L10;
	}
/*       / CHECK LENGTH OF LINE / */
	if (t + s <= c__) {
/*         / FIT IN THE CURRENT CYCLE / */
	    i__ = ix;
L15:
	    if (t + s <= a[i__ - 1]) {
		goto L20;
	    }
	    if (m[i__ - 1] == 0) {
		r__1 = vxm + dx * (a[i__ - 1] - t);
		r__2 = vym + dy * (a[i__ - 1] - t);
		szmvlp_(&r__1, &r__2);
	    } else if (m[i__ - 1] == 1) {
		r__1 = vxm + dx * (a[i__ - 1] - t);
		r__2 = vym + dy * (a[i__ - 1] - t);
		szpllp_(&r__1, &r__2);
	    }
	    ++i__;
	    goto L15;
L20:
	    if (m[i__ - 1] == 0) {
		szmvlp_(vx, vy);
	    } else if (m[i__ - 1] == 1) {
		szpllp_(vx, vy);
	    }
	    t += s;
	} else {
/*         / CONTINUE TO THE NEXT CYCLE / */
	    i__1 = na;
	    for (i__ = ix; i__ <= i__1; ++i__) {
		if (m[i__ - 1] == 0) {
		    r__1 = vxm + dx * (a[i__ - 1] - t);
		    r__2 = vym + dy * (a[i__ - 1] - t);
		    szmvlp_(&r__1, &r__2);
		} else if (m[i__ - 1] == 1) {
		    r__1 = vxm + dx * (a[i__ - 1] - t);
		    r__2 = vym + dy * (a[i__ - 1] - t);
		    szpllp_(&r__1, &r__2);
		}
/* L25: */
	    }
	    vxm += dx * (c__ - t);
	    vym += dy * (c__ - t);
	    s -= c__ - t;
	    t = 0.f;
	    l = s / c__;
/*         / PROCESS CYCLES / */
	    i__1 = l;
	    for (j = 1; j <= i__1; ++j) {
		i__2 = na;
		for (i__ = 1; i__ <= i__2; ++i__) {
		    if (m[i__ - 1] == 0) {
			r__1 = vxm + dx * a[i__ - 1];
			r__2 = vym + dy * a[i__ - 1];
			szmvlp_(&r__1, &r__2);
		    } else if (m[i__ - 1] == 1) {
			r__1 = vxm + dx * a[i__ - 1];
			r__2 = vym + dy * a[i__ - 1];
			szpllp_(&r__1, &r__2);
		    }
/* L30: */
		}
		vxm += dx * c__;
		vym += dy * c__;
		s -= c__;
/* L35: */
	    }
/*         / FIND LAST POSITON INDEX / */
	    ix = 0;
L40:
	    ++ix;
	    if (a[ix - 1] < s) {
		goto L40;
	    }
/*         / PROCESS REMAINING CYCLE / */
	    i__1 = ix - 1;
	    for (i__ = 1; i__ <= i__1; ++i__) {
		if (m[i__ - 1] == 0) {
		    r__1 = vxm + dx * a[i__ - 1];
		    r__2 = vym + dy * a[i__ - 1];
		    szmvlp_(&r__1, &r__2);
		} else if (m[i__ - 1] == 1) {
		    r__1 = vxm + dx * a[i__ - 1];
		    r__2 = vym + dy * a[i__ - 1];
		    szpllp_(&r__1, &r__2);
		}
/* L45: */
	    }
	    if (m[ix - 1] == 0) {
		szmvlp_(vx, vy);
	    } else if (m[i__ - 1] == 1) {
		szpllp_(vx, vy);
	    }
	    t = s;
	}
    }
    vxm = *vx;
    vym = *vy;
    return 0;
/* ----------------------------------------------------------------------- */

L_szclld:
/*     / CLOSE DASHLINE SEGMENT / */
    szcllp_();
    return 0;
/* ----------------------------------------------------------------------- */

L_szstyz:
    sgrget_("BITLEN", &bl, (ftnlen)6);
    sgiget_("MOVE", &move, (ftnlen)4);
    sgiget_("NBITS", &nb, (ftnlen)5);
/*     / GENERATE SOLID PATTERN / */
    isolid = 0;
    i__1 = nb;
    for (i__ = 1; i__ <= i__1; ++i__) {
	isolid = ishift_(&isolid, &c__1) | 1;
/* L50: */
    }
/*     / CHECK IPAT / */
    jpat = isolid & *ipat;
    if (jpat == 0 || jpat == isolid) {
/*       / SOLID / */
	ldash = FALSE_;
    } else {
/*       / DASH / */
	ldash = TRUE_;
	na = 1;
	i__1 = 1 - nb;
	mode1 = ishift_(&jpat, &i__1) & 1;
	a[0] = bl;
	m[0] = mode1;
	modex = mode1;
	i__1 = nb;
	for (i__ = 2; i__ <= i__1; ++i__) {
	    i__2 = i__ - nb;
	    modei = ishift_(&jpat, &i__2) & 1;
	    if (modei != modex) {
		++na;
		a[na - 1] = a[na - 2];
		m[na - 1] = modei;
		modex = modei;
	    }
	    a[na - 1] += bl;
/* L55: */
	}
/*       / INITIALIZATION / */
	c__ = a[na - 1];
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_szqtyz:
    *ipat = jpat;
    return 0;
} /* szopld_ */

/* Subroutine */ int szopld_(void)
{
    return szopld_0_(0, (real *)0, (real *)0, (integer *)0);
    }

/* Subroutine */ int szmvld_(real *vx, real *vy)
{
    return szopld_0_(1, vx, vy, (integer *)0);
    }

/* Subroutine */ int szplld_(real *vx, real *vy)
{
    return szopld_0_(2, vx, vy, (integer *)0);
    }

/* Subroutine */ int szclld_(void)
{
    return szopld_0_(3, (real *)0, (real *)0, (integer *)0);
    }

/* Subroutine */ int szstyz_(integer *ipat)
{
    return szopld_0_(4, (real *)0, (real *)0, ipat);
    }

/* Subroutine */ int szqtyz_(integer *ipat)
{
    return szopld_0_(5, (real *)0, (real *)0, ipat);
    }


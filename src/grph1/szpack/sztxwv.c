/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Common Block Declarations */

struct {
    real qsize, ct, st;
    integer icentz;
} szbtx1_;

#define szbtx1_1 szbtx1_

struct {
    logical lcntl;
    integer jsup, jsub, jrst;
    real small, shift;
} szbtx2_;

#define szbtx2_1 szbtx2_

/* Table of constant values */

static logical c_true = TRUE_;
static integer c__92 = 92;

/* ----------------------------------------------------------------------- */
/*     BASIC TEXT PRIMITIVE ON VC */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int sztxwv_(real *vx, real *vy, char *chars, ftnlen 
	chars_len)
{
    /* Initialized data */

    static integer ncntz = 0;

    /* System generated locals */
    integer i__1, i__2;
    real r__1;
    char ch__1[1];

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    double asin(doublereal);

    /* Local variables */
    static char fontname[70];
    static integer i__, j, k, nc, ip;
    static real xc, yc;
    static integer nx, ny;
    static real xp, yp, wx, wy, vx1, wx1[2048], wx2[2048], vx2;
    static integer idx;
    static char ckx[1*6000], cky[1*6000];
    static integer ncz, ips;
    static real fnx, fny;
    static integer idx0;
    extern integer lenc_(char *, ftnlen);
    static integer mode;
    extern /* Character */ VOID csgi_(char *, ftnlen, integer *);
    extern integer isgc_(char *, ftnlen);
    static char csub[1];
    static integer isub, ncnt;
    static real offx, offy;
    extern integer lenz_(char *, ftnlen);
    static real wxch;
    static char csup[1], crst[1];
    static real wych;
    static integer isup, irst, mode0, imode;
    static char charx[2048];
    static integer index;
    static char charz[2048];
    static real factz, irota;
    static integer ifont;
    static real xrate, yrate;
    static integer isubz;
    static real rxmin, rxmax, rymin, rymax;
    static integer iposx[2048];
    static real wxmin, wxmax, wymin, wymax;
    static integer iwtrf, isupz, irstz;
    extern /* Subroutine */ int swsfw_(integer *), swtxt_(real *, real *, 
	    real *, char *, integer *, real *, integer *, ftnlen), sgiget_(
	    char *, integer *, ftnlen), sglget_(char *, logical *, ftnlen), 
	    swcget_(char *, char *, ftnlen, ftnlen), swftfc_(char *, ftnlen);
    static logical lfprop;
    extern /* Subroutine */ int swscli_(integer *, logical *), swlget_(char *,
	     logical *, ftnlen);
    static logical lcntlz;
    extern /* Subroutine */ int swocls_(char *, ftnlen), szfint_(integer *), 
	    szqidx_(integer *), szclsv_(void), szqfnt_(integer *, char *, 
	    char *, ftnlen, ftnlen), szqfnw_(real *, real *), stfwtr_(real *, 
	    real *, real *, real *), szplsv_(real *, real *), stqwtr_(real *, 
	    real *, real *, real *, real *, real *, real *, real *, integer *)
	    , sztxno_(char *, char *, logical *, ftnlen, ftnlen), szopsv_(
	    void), szmvsv_(real *, real *), szqtxw_(char *, integer *, real *,
	     real *, ftnlen);
    static logical lmrkfnt, lsysfnt;

    lcntlz = szbtx2_1.lcntl;
    sgiget_("ISUP", &isup, (ftnlen)4);
    sgiget_("ISUB", &isub, (ftnlen)4);
    sgiget_("IRST", &irst, (ftnlen)4);
    isupz = 29;
    isubz = 30;
    irstz = 31;
    csgi_(ch__1, (ftnlen)1, &isupz);
    *(unsigned char *)csup = *(unsigned char *)&ch__1[0];
    csgi_(ch__1, (ftnlen)1, &isubz);
    *(unsigned char *)csub = *(unsigned char *)&ch__1[0];
    csgi_(ch__1, (ftnlen)1, &irstz);
    *(unsigned char *)crst = *(unsigned char *)&ch__1[0];
/*      / START: CODE FOR USING SYSTEN FONTS / */
/*      / PROCCESS IF NOT MARKER / */
    swlget_("LSYSFNT", &lsysfnt, (ftnlen)7);
    if (lsysfnt) {
	szqidx_(&index);
	swcget_("FONTNAME", fontname, (ftnlen)8, (ftnlen)70);
/*     / WHEN FONTNAME IS NOT GIVEN / */
	if (lenz_(fontname, (ftnlen)70) == 0) {
	    sglget_("LFPROP", &lfprop, (ftnlen)6);
	    if (lfprop) {
		sgiget_("IFONT", &ifont, (ftnlen)5);
		if (ifont == 1) {
		    s_copy(fontname, "Sans", (ftnlen)70, (ftnlen)4);
		} else if (ifont == 2) {
		    s_copy(fontname, "Serif", (ftnlen)70, (ftnlen)5);
		}
	    } else {
		s_copy(fontname, "Monospace", (ftnlen)70, (ftnlen)9);
	    }
	    swftfc_(fontname, (ftnlen)70);
/* Computing MAX */
	    i__2 = index % 10;
	    i__1 = max(i__2,1);
	    swsfw_(&i__1);
	} else {
	    swftfc_(fontname, (ftnlen)70);
	}
	idx = isgc_(chars, (ftnlen)1);
	sglget_("LMRKFNT", &lmrkfnt, (ftnlen)7);
	if (idx > 18 || lmrkfnt) {
	    ncz = lenc_(chars, chars_len);
	    stfwtr_(vx, vy, &wx, &wy);
	    stqwtr_(&rxmin, &rxmax, &rymin, &rymax, &wxmin, &wxmax, &wymin, &
		    wymax, &iwtrf);
	    xrate = (wxmax - wxmin) / (rxmax - rxmin);
	    yrate = (wymax - wymin) / (rymax - rymin);
	    if (index > 9) {
		i__1 = index / 10;
		swscli_(&i__1, &c_true);
	    }
	    if (idx < 18) {
/* Computing MAX */
		i__2 = index % 10;
		i__1 = max(i__2,1);
		swsfw_(&i__1);
	    }
	    irota = asin(szbtx1_1.st / szbtx1_1.qsize);
	    r__1 = szbtx1_1.qsize * xrate;
	    swtxt_(&wx, &wy, &r__1, chars, &ncz, &irota, &szbtx1_1.icentz, 
		    chars_len);
	    goto L30;
	}
    }
/*      / END: CODE FOR USING SYSTEN FONTS / */
/*     / GET FONT INFORMATION / */
    szfint_(&ncnt);
    if (ncntz != ncnt) {
	szqfnt_(iposx, ckx, cky, (ftnlen)1, (ftnlen)1);
	szqfnw_(wx1, wx2);
	ncntz = ncnt;
    }
/*     /REMOVE DCL EXT CHARACTER MARKER +/ */
    ncz = lenc_(chars, chars_len);
    j = 0;
    i__1 = ncz;
    for (i__ = 1; i__ <= i__1; ++i__) {
	idx0 = isgc_(chars + (i__ - 1), (ftnlen)1);
	if (idx0 == 92) {
	    imode = 1;
	}
	if (imode == 0 || imode == 4) {
	    ++j;
	    *(unsigned char *)&charx[j - 1] = *(unsigned char *)&chars[i__ - 
		    1];
	    imode = 0;
	}
	if (imode == 3) {
	    if (idx0 == 120) {
		imode = 2;
	    } else {
		++j;
		csgi_(ch__1, (ftnlen)1, &c__92);
		*(unsigned char *)&charx[j - 1] = *(unsigned char *)&ch__1[0];
		++j;
		*(unsigned char *)&charx[j - 1] = *(unsigned char *)&chars[
			i__ - 1];
		imode = 0;
	    }
	}
	if (imode == 1 || imode == 2) {
	    imode += 2;
	}
/* L999: */
    }
    for (i__ = j + 1; i__ <= 2048; ++i__) {
	*(unsigned char *)&charx[i__ - 1] = '\0';
/* L998: */
    }
    sztxno_(chars, charz, &lcntlz, chars_len, (ftnlen)2048);
    szqtxw_(chars, &nc, &wxch, &wych, chars_len);
/*     / CALCULATE OFFSET VALUES / */
    offx = wxch * -.5f * 24.f * (szbtx1_1.icentz + 1);
    offy = 0.f;
    xc = *vx + offx * szbtx1_1.ct - offy * szbtx1_1.st;
    yc = *vy + offx * szbtx1_1.st + offy * szbtx1_1.ct;
/*     / INITIALIZATION / */
    mode0 = 0;
    mode = 0;
    factz = 1.f;
    vx1 = 0.f;
/*     / PROCESS EACH CHARACTER / */
    i__1 = ncz;
    for (k = 1; k <= i__1; ++k) {
/*       / CHARACTER CONVERSION / */
	idx = isgc_(charz + (k - 1), (ftnlen)1) + 1;
/*       / CHECK CONTROL CHARACTERS / */
	ips = iposx[idx - 1] + 1;
	if (lcntlz && (idx == isubz + 1 || idx == isupz + 1 || idx == irstz + 
		1)) {
	    if (idx == isubz + 1) {
		mode = -1;
		factz = szbtx2_1.small;
	    } else if (idx == isupz + 1) {
		mode = 1;
		factz = szbtx2_1.small;
	    } else if (idx == irstz + 1) {
		mode = 0;
		factz = 1.f;
	    }
	    goto L20;
	}
	vx2 = -wx1[idx - 1];
	if (mode0 == mode) {
	    if (mode == 0) {
		fnx = vx1 + vx2;
	    } else {
		fnx = (vx1 + vx2) * szbtx2_1.small;
	    }
	    fny = 0.f;
	} else {
	    if (mode0 * mode == 0) {
		if (mode == 0) {
		    fnx = vx1 * szbtx2_1.small + vx2;
		} else {
		    fnx = vx2 + vx1 * szbtx2_1.small;
		}
	    } else {
/*           'IS THIS BLOCK REALLY NECESSARY?' */
		fnx = (vx1 + vx2) * szbtx2_1.small;
	    }
	    fny = szbtx2_1.shift * 24.f * (mode - mode0);
	}
	mode0 = mode;
	xc = xc + fnx * szbtx1_1.ct - fny * szbtx1_1.st;
	yc = yc + fnx * szbtx1_1.st + fny * szbtx1_1.ct;
	vx1 = wx2[idx - 1];
	szopsv_();
	ip = 0;
L10:
	nx = *(unsigned char *)&ckx[ips - 1] - 64;
	fnx = nx * factz;
	ny = *(unsigned char *)&cky[ips - 1] - 64;
	fny = -ny * factz;
	if (nx != -64) {
	    xp = xc + fnx * szbtx1_1.ct - fny * szbtx1_1.st;
	    yp = yc + fnx * szbtx1_1.st + fny * szbtx1_1.ct;
	    if (ip == 0) {
		szmvsv_(&xp, &yp);
	    } else if (ip == 1) {
		szplsv_(&xp, &yp);
	    }
	    ip = 1;
	} else {
	    ip = 0;
	}
	++ips;
	if (! (nx == -64 && ny == -64)) {
	    goto L10;
	}
	szclsv_();
L20:
	;
    }
    swocls_("SZTXZ", (ftnlen)5);
L30:
    return 0;
} /* sztxwv_ */


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Common Block Declarations */

struct {
    integer irmode, irmodr;
} szbtn2_;

#define szbtn2_1 szbtn2_

struct {
    integer itpt1, itpt2;
} szbtn4_;

#define szbtn4_1 szbtn4_

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int szt3zu_(real *upx, real *upy, real *upz)
{
    /* Initialized data */

    static integer itpatz = 0;

    static integer i__;
    static real rx[3], ry[3], vx, vy, vz, rot;
    static integer itpat;
    extern /* Subroutine */ int stfpr3_(real *, real *, real *, real *, real *
	    ), stftr3_(real *, real *, real *, real *, real *, real *), 
	    szcltr_(void), szstni_(integer *), szoptr_(void), szsttr_(real *, 
	    real *);

    /* Parameter adjustments */
    --upz;
    --upy;
    --upx;

    /* Function Body */
    for (i__ = 1; i__ <= 3; ++i__) {
	stftr3_(&upx[i__], &upy[i__], &upz[i__], &vx, &vy, &vz);
	stfpr3_(&vx, &vy, &vz, &rx[i__ - 1], &ry[i__ - 1]);
/* L10: */
    }
    rot = (rx[1] - rx[0]) * (ry[2] - ry[0]) - (rx[2] - rx[0]) * (ry[1] - ry[0]
	    );
    if (rot > 0.f) {
	szbtn2_1.irmodr = szbtn2_1.irmode % 2;
	itpat = szbtn4_1.itpt1;
    } else if (rot < 0.f) {
	szbtn2_1.irmodr = (szbtn2_1.irmode + 1) % 2;
	itpat = szbtn4_1.itpt2;
    } else {
	return 0;
    }
    if (itpat != itpatz) {
	szstni_(&itpat);
	itpatz = itpat;
    }
    szoptr_();
    for (i__ = 1; i__ <= 3; ++i__) {
	szsttr_(&rx[i__ - 1], &ry[i__ - 1]);
/* L20: */
    }
    szsttr_(rx, ry);
    szcltr_();
    return 0;
} /* szt3zu_ */


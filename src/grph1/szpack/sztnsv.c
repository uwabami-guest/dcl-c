/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__180 = 180;
static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     FILL AREA PRIMITIVE (SOFT FILL - BASIC) */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int sztnsv_(integer *n, real *vpx, real *vpy, integer *irota,
	 real *rspce, integer *itype, integer *index)
{
    /* System generated locals */
    integer i__1, i__2, i__3;
    real r__1;

    /* Builtin functions */
    double cos(doublereal), sin(doublereal);

    /* Local variables */
    static real d__;
    static integer i__, j;
    static real x1, y1, x2, y2, x3, y3, x4, y4, da;
    static integer ia, ib, ic;
    static real xa, ya;
    static integer np, iz;
    static real sx[200], xs, sz, ys, da1, db1, dai, dbi;
    static integer ida;
    static real rot;
    static logical lsx, lsy;
    static real sxx, txx;
    extern real rd2r_(real *);
    static real daia, daib;
    extern integer imod_(integer *, integer *);
    static integer ncnt;
    static real rota;
    extern integer igus_(real *);
    static real xcos, xsin;
    extern real rmin0_(real *, integer *, integer *), rmax0_(real *, integer *
	    , integer *);
    static real damin, dbmin, damax, dbmax, cycle;
    static integer nbits, movez;
    static real szmax, bitlen;
    extern /* Subroutine */ int sgiget_(char *, integer *, ftnlen), msgdmp_(
	    char *, char *, char *, ftnlen, ftnlen, ftnlen);
    extern integer indxrf_(real *, integer *, integer *, real *);
    extern /* Subroutine */ int sgrget_(char *, real *, ftnlen), sgiset_(char 
	    *, integer *, ftnlen);
    static integer indexz;
    extern /* Subroutine */ int szcllv_(void), szqidx_(integer *);
    static integer itypez;
    extern /* Subroutine */ int szsidx_(integer *), szpllv_(real *, real *), 
	    szoplv_(void), szmvlv_(real *, real *), szqtyp_(integer *), 
	    szstyp_(integer *);

    /* Parameter adjustments */
    --vpy;
    --vpx;

    /* Function Body */
    sgrget_("BITLEN", &bitlen, (ftnlen)6);
    sgiget_("NBITS", &nbits, (ftnlen)5);
    sgrget_("RFAROT", &rot, (ftnlen)6);
    cycle = bitlen * nbits;
    i__1 = *irota + 90;
    rota = imod_(&i__1, &c__180) - 90 + rot;
    xcos = cos(rd2r_(&rota));
    xsin = sin(rd2r_(&rota));
    db1 = xcos * vpx[1] + xsin * vpy[1];
    dbmin = db1;
    dbmax = db1;
    i__1 = *n;
    for (i__ = 2; i__ <= i__1; ++i__) {
	dbi = xcos * vpx[i__] + xsin * vpy[i__];
	if (dbi < dbmin) {
	    dbmin = dbi;
	}
	if (dbi > dbmax) {
	    dbmax = dbi;
	}
/* L10: */
    }
    r__1 = dbmin / cycle;
    dbmin = igus_(&r__1) * cycle - cycle;
    r__1 = dbmax / cycle;
    dbmax = igus_(&r__1) * cycle + cycle;
    da1 = -xsin * vpx[1] + xcos * vpy[1];
    damin = da1;
    damax = da1;
    i__1 = *n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	dai = -xsin * vpx[i__] + xcos * vpy[i__];
	if (dai < damin) {
	    damin = dai;
	}
	if (dai > damax) {
	    damax = dai;
	}
/* L15: */
    }
    r__1 = damin / *rspce;
    damin = igus_(&r__1) * *rspce - *rspce;
    r__1 = damax / *rspce;
    damax = igus_(&r__1) * *rspce + *rspce;
    sgiget_("MOVE", &movez, (ftnlen)4);
    szqidx_(&indexz);
    szqtyp_(&itypez);
    sgiset_("MOVE", &c__1, (ftnlen)4);
    szsidx_(index);
    szstyp_(itype);
/*     DO 30 DA=DAMIN,DAMAX,RSPCE */
/* Computing MAX */
    i__1 = (integer) ((damax - damin + *rspce) / *rspce);
    ncnt = max(i__1,0);
    i__1 = ncnt;
    for (ida = 1; ida <= i__1; ++ida) {
	da = damin + *rspce * (ida - 1);
	xa = -xsin * da;
	ya = xcos * da;
	x1 = xa + xcos * dbmin;
	y1 = ya + xsin * dbmin;
	x2 = xa + xcos * dbmax;
	y2 = ya + xsin * dbmax;
	np = 0;
	i__2 = *n;
	for (i__ = 1; i__ <= i__2; ++i__) {
	    i__3 = i__ - 2;
	    ib = imod_(&i__3, n) + 1;
	    ic = i__;
	    ia = imod_(&i__, n) + 1;
	    x3 = vpx[ic];
	    y3 = vpy[ic];
	    x4 = vpx[ia];
	    y4 = vpy[ia];
	    d__ = -(x2 - x1) * (y4 - y3) + (y2 - y1) * (x4 - x3);
	    if (d__ != 0.f) {
		sxx = (-(y4 - y3) * (x3 - x1) + (x4 - x3) * (y3 - y1)) / d__;
		txx = (-(y2 - y1) * (x3 - x1) + (x2 - x1) * (y3 - y1)) / d__;
		lsx = 0.f < sxx && sxx < 1.f;
		lsy = 0.f < txx && txx < 1.f;
/*           IF (0.LT.SXX .AND. SXX.LT.1 .AND. */
/*    +          0.LT.TXX .AND. TXX.LT.1 ) THEN */
		if (lsx && lsy) {
		    ++np;
		    sx[np - 1] = sxx;
		} else if (txx == 0.f) {
		    daib = -xsin * vpx[ib] + xcos * vpy[ib];
		    daia = -xsin * vpx[ia] + xcos * vpy[ia];
		    if ((daib - da) * (daia - da) < 0.f) {
			++np;
			sx[np - 1] = sxx;
		    }
		}
	    }
/* L20: */
	}
	if (np > 200) {
	    msgdmp_("E", "SZTNSV", "WORKING AREA OVERFLOW.", (ftnlen)1, (
		    ftnlen)6, (ftnlen)22);
	}
	if (np > 0) {
	    szmax = rmax0_(sx, &np, &c__1);
	    szoplv_();
	    szmvlv_(&x1, &y1);
	    i__2 = np;
	    for (j = 1; j <= i__2; ++j) {
		sz = rmin0_(sx, &np, &c__1);
		iz = indxrf_(sx, &np, &c__1, &sz);
		xs = x1 + sz * (x2 - x1);
		ys = y1 + sz * (y2 - y1);
		if (j % 2 != 0) {
		    szmvlv_(&xs, &ys);
		} else {
		    szpllv_(&xs, &ys);
		}
		sx[iz - 1] = szmax;
/* L25: */
	    }
	    szcllv_();
	}
/* L30: */
    }
    sgiset_("MOVE", &movez, (ftnlen)4);
    szsidx_(&indexz);
    szstyp_(&itypez);
    return 0;
} /* sztnsv_ */


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Common Block Declarations */

struct {
    logical llnint, lgcint;
    real rdxr, rdyr;
} szbls1_;

#define szbls1_1 szbls1_

/* Table of constant values */

static real c_b2 = 1e-5f;

/* ----------------------------------------------------------------------- */
/*     INTERPOLATION ON UC (LINEAR INTERPOLATION) */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int szpipl_0_(int n__, real *x0, real *y0, real *x1, real *
	y1, integer *mode, real *x, real *y, logical *lcont)
{
    /* System generated locals */
    integer i__1, i__2;
    real r__1, r__2;

    /* Local variables */
    static real pi;
    static integer nn;
    static real dx, dy, xx0, xx1, yy0, yy1, dxx, dyy;
    static integer nend;
    extern real rfpi_(void);
    static logical lint;
    extern logical lreqa_(real *, real *, real *);

/*     MODE = 0 : FOR LINE */
/*            1 : FOR TONE */
/*            2 : FOR ARROW */
/*         ELSE : FOR BOUNDARY */
    switch(n__) {
	case 1: goto L_szgipl;
	}

    pi = rfpi_();
    if (*mode == 0) {
	lint = szbls1_1.llnint;
    } else if (*mode == 1) {
	r__1 = abs(*y0);
	r__2 = pi / 2;
	lint = szbls1_1.llnint && ! (lreqa_(y0, y1, &c_b2) && lreqa_(&r__1, &
		r__2, &c_b2));
    } else if (*mode == 2) {
	lint = szbls1_1.llnint;
    } else {
	lint = TRUE_;
    }
    xx0 = *x0;
    yy0 = *y0;
    xx1 = *x1;
    yy1 = *y1;
    nn = 1;
    nend = 1;
    if (! lint) {
	return 0;
    }
    dx = *x1 - *x0;
    dy = *y1 - *y0;
/* Computing MAX */
    i__1 = (r__1 = dx / szbls1_1.rdxr, (integer) abs(r__1)), i__2 = (r__2 = 
	    dy / szbls1_1.rdyr, (integer) abs(r__2));
    nend = max(i__1,i__2) + 1;
    if (nend >= 2) {
	dxx = dx / nend;
	dyy = dy / nend;
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_szgipl:
    *lcont = nn < nend;
    if (*lcont) {
	*x = xx0 + nn * dxx;
	*y = yy0 + nn * dyy;
    } else {
	*x = xx1;
	*y = yy1;
    }
    ++nn;
    return 0;
} /* szpipl_ */

/* Subroutine */ int szpipl_(real *x0, real *y0, real *x1, real *y1, integer *
	mode)
{
    return szpipl_0_(0, x0, y0, x1, y1, mode, (real *)0, (real *)0, (logical *
	    )0);
    }

/* Subroutine */ int szgipl_(real *x, real *y, logical *lcont)
{
    return szpipl_0_(1, (real *)0, (real *)0, (real *)0, (real *)0, (integer *
	    )0, x, y, lcont);
    }


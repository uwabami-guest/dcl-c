/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     TONE ROUTINE ON RC (HARD FILL - BUFFERING) */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int szoptz_0_(int n__, real *rx, real *ry, integer *itpat)
{
    static integer nn;
    static real wx[16384], wy[16384];
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen);
    static integer itpatz;
    extern /* Subroutine */ int swgton_(integer *, real *, real *, integer *),
	     stfwtr_(real *, real *, real *, real *);

    switch(n__) {
	case 1: goto L_szsttz;
	case 2: goto L_szcltz;
	case 3: goto L_sztitz;
	}

    nn = 0;
    return 0;
/* ----------------------------------------------------------------------- */

L_szsttz:
    ++nn;
    if (nn > 16384) {
	msgdmp_("E", "SZSTTZ", "WORKING AREA OVER FLOW", (ftnlen)1, (ftnlen)6,
		 (ftnlen)22);
    }
    stfwtr_(rx, ry, &wx[nn - 1], &wy[nn - 1]);
    return 0;
/* ----------------------------------------------------------------------- */

L_szcltz:
    swgton_(&nn, wx, wy, &itpatz);
    return 0;
/* ----------------------------------------------------------------------- */

L_sztitz:
    itpatz = *itpat;
    return 0;
} /* szoptz_ */

/* Subroutine */ int szoptz_(void)
{
    return szoptz_0_(0, (real *)0, (real *)0, (integer *)0);
    }

/* Subroutine */ int szsttz_(real *rx, real *ry)
{
    return szoptz_0_(1, rx, ry, (integer *)0);
    }

/* Subroutine */ int szcltz_(void)
{
    return szoptz_0_(2, (real *)0, (real *)0, (integer *)0);
    }

/* Subroutine */ int sztitz_(integer *itpat)
{
    return szoptz_0_(3, (real *)0, (real *)0, itpat);
    }


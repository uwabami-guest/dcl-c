/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Common Block Declarations */

struct {
    logical lclip;
} szbls2_;

#define szbls2_1 szbls2_

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     PLOT ROUTINE ON VC (CLIPPING) */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int szoplv_0_(int n__, real *vx, real *vy)
{
    static real xx, yy, vx0, vy0;
    static logical lcont, lmove, lvalid;
    extern /* Subroutine */ int szcllc_(void), szgcll_(real *, real *, 
	    logical *, logical *, integer *), szpcll_(real *, real *, real *, 
	    real *, logical *, integer *), szpllc_(real *, real *), szoplc_(
	    void), szmvlc_(real *, real *);

    switch(n__) {
	case 1: goto L_szmvlv;
	case 2: goto L_szpllv;
	case 3: goto L_szcllv;
	}

    szoplc_();
    return 0;
/* ----------------------------------------------------------------------- */

L_szmvlv:
    if (szbls2_1.lclip) {
	szpcll_(vx, vy, vx, vy, &lvalid, &c__1);
	if (lvalid) {
	    szmvlc_(vx, vy);
	}
	vx0 = *vx;
	vy0 = *vy;
    } else {
	szmvlc_(vx, vy);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_szpllv:
    if (szbls2_1.lclip) {
	szpcll_(&vx0, &vy0, vx, vy, &lvalid, &c__1);
	if (lvalid) {
L10:
	    szgcll_(&xx, &yy, &lcont, &lmove, &c__1);
	    if (lmove) {
		szmvlc_(&xx, &yy);
	    } else {
		szpllc_(&xx, &yy);
	    }
	    if (lcont) {
		goto L10;
	    }
	}
	vx0 = *vx;
	vy0 = *vy;
    } else {
	szpllc_(vx, vy);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_szcllv:
    szcllc_();
    return 0;
} /* szoplv_ */

/* Subroutine */ int szoplv_(void)
{
    return szoplv_0_(0, (real *)0, (real *)0);
    }

/* Subroutine */ int szmvlv_(real *vx, real *vy)
{
    return szoplv_0_(1, vx, vy);
    }

/* Subroutine */ int szpllv_(real *vx, real *vy)
{
    return szoplv_0_(2, vx, vy);
    }

/* Subroutine */ int szcllv_(void)
{
    return szoplv_0_(3, (real *)0, (real *)0);
    }


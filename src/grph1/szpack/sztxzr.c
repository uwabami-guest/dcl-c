/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Common Block Declarations */

struct {
    logical lclip;
} szbtx3_;

#define szbtx3_1 szbtx3_

/* Table of constant values */

static logical c_false = FALSE_;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int sztxzr_(real *rx, real *ry, char *chars, ftnlen 
	chars_len)
{
    extern /* Subroutine */ int stepr2_(void), strpr2_(void), sglget_(char *, 
	    logical *, ftnlen);
    static logical lclipz;
    extern /* Subroutine */ int sglset_(char *, logical *, ftnlen), sztxwv_(
	    real *, real *, char *, ftnlen);

    sglget_("LCLIP", &lclipz, (ftnlen)5);
    sglset_("LCLIP", &c_false, (ftnlen)5);
    szbtx3_1.lclip = FALSE_;
    stepr2_();
    sztxwv_(rx, ry, chars, chars_len);
    sglset_("LCLIP", &lclipz, (ftnlen)5);
    szbtx3_1.lclip = lclipz;
    strpr2_();
    return 0;
} /* sztxzr_ */


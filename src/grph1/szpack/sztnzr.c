/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Common Block Declarations */

struct {
    logical lclip;
} szbtn3_;

#define szbtn3_1 szbtn3_

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int sztnzr_(integer *n, real *rpx, real *rpy)
{
    /* System generated locals */
    integer i__1;

    /* Local variables */
    static integer i__;
    extern /* Subroutine */ int stepr2_(void), strpr2_(void);
    static logical lclipz;
    extern /* Subroutine */ int szcltv_(void), szoptv_(void), szsttv_(real *, 
	    real *);

    /* Parameter adjustments */
    --rpy;
    --rpx;

    /* Function Body */
    lclipz = szbtn3_1.lclip;
    szbtn3_1.lclip = FALSE_;
    stepr2_();
    szoptv_();
    i__1 = *n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	szsttv_(&rpx[i__], &rpy[i__]);
/* L10: */
    }
    szsttv_(&rpx[1], &rpy[1]);
    szcltv_();
    szbtn3_1.lclip = lclipz;
    strpr2_();
    return 0;
} /* sztnzr_ */


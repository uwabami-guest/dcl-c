/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Common Block Declarations */

struct {
    logical larrwz, lpropz;
    real afactz, constz, anglez;
    logical latonz, luarwz;
    real consmz, rdunit;
} szbla1_;

#define szbla1_1 szbla1_

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     ARROW SUBPRIMITIVE */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int szlaop_0_(int n__, integer *itype, integer *index)
{
    /* Builtin functions */
    integer s_wsfi(icilist *), do_fio(integer *, char *, ftnlen), e_wsfi(void)
	    ;

    /* Local variables */
    static logical ldeg;
    static char cobj[80];
    extern real rfpi_(void);
    extern /* Subroutine */ int cdblk_(char *, ftnlen);
    static integer iatone;
    extern /* Subroutine */ int sgiget_(char *, integer *, ftnlen), sglget_(
	    char *, logical *, ftnlen), sgrget_(char *, real *, ftnlen);
    static integer iatonz;
    extern /* Subroutine */ int szplcl_(void), swocls_(char *, ftnlen), 
	    sztncl_(void), szqidx_(integer *), swoopn_(char *, char *, ftnlen,
	     ftnlen), szslti_(integer *, integer *), sztnop_(integer *);

    /* Fortran I/O blocks */
    static icilist io___4 = { 0, cobj, 0, "(2I8)", 80, 1 };


    switch(n__) {
	case 1: goto L_szlacl;
	}

    szbla1_1.larrwz = TRUE_;
    sglget_("LPROP", &szbla1_1.lpropz, (ftnlen)5);
    sgrget_("AFACT", &szbla1_1.afactz, (ftnlen)5);
    sgrget_("CONST", &szbla1_1.constz, (ftnlen)5);
    sgrget_("ANGLE", &szbla1_1.anglez, (ftnlen)5);
    sglget_("LATONE", &szbla1_1.latonz, (ftnlen)6);
    sgiget_("IATONE", &iatonz, (ftnlen)6);
    sglget_("LUARW", &szbla1_1.luarwz, (ftnlen)5);
    sgrget_("CONSTM", &szbla1_1.consmz, (ftnlen)6);
    sglget_("LDEG", &ldeg, (ftnlen)4);
    if (ldeg) {
	szbla1_1.rdunit = rfpi_() / 180;
    } else {
	szbla1_1.rdunit = 1.f;
    }
    s_wsfi(&io___4);
    do_fio(&c__1, (char *)&(*itype), (ftnlen)sizeof(integer));
    do_fio(&c__1, (char *)&(*index), (ftnlen)sizeof(integer));
    e_wsfi();
    cdblk_(cobj, (ftnlen)80);
    swoopn_("SZLA", cobj, (ftnlen)4, (ftnlen)80);
    szslti_(itype, index);
    if (szbla1_1.latonz) {
	szqidx_(index);
	if (iatonz / 1000 == 0) {
	    iatone = iatonz + *index / 10 * 1000;
	}
	sztnop_(&iatone);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_szlacl:
    szbla1_1.larrwz = FALSE_;
    szplcl_();
    if (szbla1_1.latonz) {
	sztncl_();
    }
    swocls_("SZLA", (ftnlen)4);
    return 0;
} /* szlaop_ */

/* Subroutine */ int szlaop_(integer *itype, integer *index)
{
    return szlaop_0_(0, itype, index);
    }

/* Subroutine */ int szlacl_(void)
{
    return szlaop_0_(1, (integer *)0, (integer *)0);
    }


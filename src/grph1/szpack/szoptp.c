/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     TONE ROUTINE ON VC (PERSPECTIVE TRANSFORMATION & SWITHCING) */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int szoptp_0_(int n__, real *vx, real *vy, logical *lhard)
{
    static real rx, ry;
    extern /* Subroutine */ int stfpr2_(real *, real *, real *, real *);
    static logical lhardz;
    extern /* Subroutine */ int szcltr_(void), szclts_(void), szoptr_(void), 
	    szopts_(void), szsttr_(real *, real *), szstts_(real *, real *);

    switch(n__) {
	case 1: goto L_szsttp;
	case 2: goto L_szcltp;
	case 3: goto L_szstmd;
	case 4: goto L_szqtmd;
	}

    if (lhardz) {
	szoptr_();
    } else {
	szopts_();
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_szsttp:
    if (lhardz) {
	stfpr2_(vx, vy, &rx, &ry);
	szsttr_(&rx, &ry);
    } else {
	szstts_(vx, vy);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_szcltp:
    if (lhardz) {
	szcltr_();
    } else {
	szclts_();
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_szstmd:
    lhardz = *lhard;
    return 0;
/* ----------------------------------------------------------------------- */

L_szqtmd:
    *lhard = lhardz;
    return 0;
} /* szoptp_ */

/* Subroutine */ int szoptp_(void)
{
    return szoptp_0_(0, (real *)0, (real *)0, (logical *)0);
    }

/* Subroutine */ int szsttp_(real *vx, real *vy)
{
    return szoptp_0_(1, vx, vy, (logical *)0);
    }

/* Subroutine */ int szcltp_(void)
{
    return szoptp_0_(2, (real *)0, (real *)0, (logical *)0);
    }

/* Subroutine */ int szstmd_(logical *lhard)
{
    return szoptp_0_(3, (real *)0, (real *)0, lhard);
    }

/* Subroutine */ int szqtmd_(logical *lhard)
{
    return szoptp_0_(4, (real *)0, (real *)0, lhard);
    }


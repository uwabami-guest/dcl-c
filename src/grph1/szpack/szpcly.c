/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static real c_b2 = 1e-5f;

/* ----------------------------------------------------------------------- */
/*     CLIPPING ON TC - Y */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int szpcly_0_(int n__, real *tx0, real *ty0, real *tx1, real 
	*ty1, logical *lvalid, logical *lbound, real *tx, real *ty, logical *
	lcont, real *ybnd1, real *ybnd2)
{
    static integer n;
    static real yb[2], xx[2], yy[2];
    static integer ib0, ib1;
    static logical lin0, lin1;
    extern logical lrlea_(real *, real *, real *), lrlta_(real *, real *, 
	    real *);
    extern /* Subroutine */ int szsgcl_(real *, real *, real *, real *), 
	    szqgcx_(real *, real *);

    switch(n__) {
	case 1: goto L_szgcly;
	case 2: goto L_szscly;
	case 3: goto L_szqcly;
	}

    if (*lbound) {
	if (lrlta_(ty0, yb, &c_b2)) {
	    ib0 = 1;
	} else if (lrlta_(&yb[1], ty0, &c_b2)) {
	    ib0 = 2;
	} else {
	    ib0 = 0;
	}
	if (lrlta_(ty1, yb, &c_b2)) {
	    ib1 = 1;
	} else if (lrlta_(&yb[1], ty1, &c_b2)) {
	    ib1 = 2;
	} else {
	    ib1 = 0;
	}
    } else {
	if (lrlea_(ty0, yb, &c_b2)) {
	    ib0 = 1;
	} else if (lrlea_(&yb[1], ty0, &c_b2)) {
	    ib0 = 2;
	} else {
	    ib0 = 0;
	}
	if (lrlea_(ty1, yb, &c_b2)) {
	    ib1 = 1;
	} else if (lrlea_(&yb[1], ty1, &c_b2)) {
	    ib1 = 2;
	} else {
	    ib1 = 0;
	}
    }
    lin0 = ib0 == 0;
    lin1 = ib1 == 0;
    *lvalid = lin0 || lin1;
    if (*lvalid) {
	if (lin0 && lin1) {
	    xx[1] = *tx1;
	    yy[1] = *ty1;
	    n = 2;
	    return 0;
	} else {
	    if (! lin0) {
		szsgcl_(tx1, ty1, tx0, ty0);
		yy[0] = yb[ib0 - 1];
		szqgcx_(yy, xx);
		xx[1] = *tx1;
		yy[1] = *ty1;
		n = 1;
	    }
	    if (! lin1) {
		szsgcl_(tx0, ty0, tx1, ty1);
		yy[1] = yb[ib1 - 1];
		szqgcx_(&yy[1], &xx[1]);
		n = 2;
	    }
	}
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_szgcly:
    *lcont = n == 1;
    *tx = xx[n - 1];
    *ty = yy[n - 1];
    ++n;
    return 0;
/* ----------------------------------------------------------------------- */

L_szscly:
    yb[0] = *ybnd1;
    yb[1] = *ybnd2;
    return 0;
/* ----------------------------------------------------------------------- */

L_szqcly:
    *ybnd1 = yb[0];
    *ybnd2 = yb[1];
    return 0;
} /* szpcly_ */

/* Subroutine */ int szpcly_(real *tx0, real *ty0, real *tx1, real *ty1, 
	logical *lvalid, logical *lbound)
{
    return szpcly_0_(0, tx0, ty0, tx1, ty1, lvalid, lbound, (real *)0, (real *
	    )0, (logical *)0, (real *)0, (real *)0);
    }

/* Subroutine */ int szgcly_(real *tx, real *ty, logical *lcont)
{
    return szpcly_0_(1, (real *)0, (real *)0, (real *)0, (real *)0, (logical *
	    )0, (logical *)0, tx, ty, lcont, (real *)0, (real *)0);
    }

/* Subroutine */ int szscly_(real *ybnd1, real *ybnd2)
{
    return szpcly_0_(2, (real *)0, (real *)0, (real *)0, (real *)0, (logical *
	    )0, (logical *)0, (real *)0, (real *)0, (logical *)0, ybnd1, 
	    ybnd2);
    }

/* Subroutine */ int szqcly_(real *ybnd1, real *ybnd2)
{
    return szpcly_0_(3, (real *)0, (real *)0, (real *)0, (real *)0, (logical *
	    )0, (logical *)0, (real *)0, (real *)0, (logical *)0, ybnd1, 
	    ybnd2);
    }


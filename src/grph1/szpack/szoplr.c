/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__0 = 0;

/* ----------------------------------------------------------------------- */
/*     PLOT ROUTINE ON RC (CLIPPING) */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int szoplr_0_(int n__, real *rx, real *ry)
{
    static real xx, yy, rx0, ry0;
    static logical lcont, lmove, lvalid;
    extern /* Subroutine */ int szgcll_(real *, real *, logical *, logical *, 
	    integer *), szpcll_(real *, real *, real *, real *, logical *, 
	    integer *), szcllz_(void), szpllz_(real *, real *), szoplz_(void),
	     szmvlz_(real *, real *);

    switch(n__) {
	case 1: goto L_szmvlr;
	case 2: goto L_szpllr;
	case 3: goto L_szcllr;
	}

    szoplz_();
    return 0;
/* ----------------------------------------------------------------------- */

L_szmvlr:
    szpcll_(rx, ry, rx, ry, &lvalid, &c__0);
    if (lvalid) {
	szmvlz_(rx, ry);
    }
    rx0 = *rx;
    ry0 = *ry;
    return 0;
/* ----------------------------------------------------------------------- */

L_szpllr:
    szpcll_(&rx0, &ry0, rx, ry, &lvalid, &c__0);
    if (lvalid) {
L10:
	szgcll_(&xx, &yy, &lcont, &lmove, &c__0);
	if (lmove) {
	    szmvlz_(&xx, &yy);
	} else {
	    szpllz_(&xx, &yy);
	}
	if (lcont) {
	    goto L10;
	}
    }
    rx0 = *rx;
    ry0 = *ry;
    return 0;
/* ----------------------------------------------------------------------- */

L_szcllr:
    szcllz_();
    return 0;
} /* szoplr_ */

/* Subroutine */ int szoplr_(void)
{
    return szoplr_0_(0, (real *)0, (real *)0);
    }

/* Subroutine */ int szmvlr_(real *rx, real *ry)
{
    return szoplr_0_(1, rx, ry);
    }

/* Subroutine */ int szpllr_(real *rx, real *ry)
{
    return szoplr_0_(2, rx, ry);
    }

/* Subroutine */ int szcllr_(void)
{
    return szoplr_0_(3, (real *)0, (real *)0);
    }


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Common Block Declarations */

struct {
    logical lmiss;
    real rmiss;
} szbpl1_;

#define szbpl1_1 szbpl1_

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int szl3zv_(integer *n, real *v3x, real *v3y, real *v3z)
{
    /* System generated locals */
    integer i__1;

    /* Local variables */
    static integer i__;
    static logical lflag;
    extern /* Subroutine */ int szcll3_(void), szpll3_(real *, real *, real *)
	    , szopl3_(void), szmvl3_(real *, real *, real *);

    /* Parameter adjustments */
    --v3z;
    --v3y;
    --v3x;

    /* Function Body */
    szopl3_();
    if (! szbpl1_1.lmiss) {
	szmvl3_(&v3x[1], &v3y[1], &v3z[1]);
	i__1 = *n;
	for (i__ = 2; i__ <= i__1; ++i__) {
	    szpll3_(&v3x[i__], &v3y[i__], &v3z[i__]);
/* L10: */
	}
    } else {
	lflag = FALSE_;
	i__1 = *n;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    if (v3x[i__] == szbpl1_1.rmiss || v3y[i__] == szbpl1_1.rmiss || 
		    v3z[i__] == szbpl1_1.rmiss) {
		lflag = FALSE_;
	    } else if (lflag) {
		szpll3_(&v3x[i__], &v3y[i__], &v3z[i__]);
	    } else {
		szmvl3_(&v3x[i__], &v3y[i__], &v3z[i__]);
		lflag = TRUE_;
	    }
/* L20: */
	}
    }
    szcll3_();
    return 0;
} /* szl3zv_ */


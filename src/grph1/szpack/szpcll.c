/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     CLIPPING ON VC */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int szpcll_0_(int n__, real *vx0, real *vy0, real *vx1, real 
	*vy1, logical *lvalid, integer *ii, real *xx, real *yy, logical *
	lcont, logical *lmove, real *vxmin, real *vxmax, real *vymin, real *
	vymax)
{
    /* System generated locals */
    integer i__1, i__2;

    /* Local variables */
    static integer i1, k0, k1, i2, kk;
    static real yc, xi, xj[4], yj[4], yi, vx[4]	/* was [2][2] */, vy[4]	/* 
	    was [2][2] */;
    static integer kx0, ky0, kx1, ky1, kxl, kyl, indx[4];
    static real dydx, xlim[4]	/* was [2][2] */, ylim[4]	/* was [2][2] 
	    */, dxdy;
    static integer istart[2];

    switch(n__) {
	case 1: goto L_szgcll;
	case 2: goto L_szscll;
	case 3: goto L_szqcll;
	}

    if (*vx0 < xlim[*ii * 2]) {
	kx0 = 0;
    } else if (*vx0 > xlim[(*ii << 1) + 1]) {
	kx0 = 2;
    } else {
	kx0 = 1;
    }
    if (*vy0 < ylim[*ii * 2]) {
	ky0 = 0;
    } else if (*vy0 > ylim[(*ii << 1) + 1]) {
	ky0 = 2;
    } else {
	ky0 = 1;
    }
    k0 = kx0 + ky0 * 3;
    if (*vx1 < xlim[*ii * 2]) {
	kx1 = 0;
    } else if (*vx1 > xlim[(*ii << 1) + 1]) {
	kx1 = 2;
    } else {
	kx1 = 1;
    }
    if (*vy1 < ylim[*ii * 2]) {
	ky1 = 0;
    } else if (*vy1 > ylim[(*ii << 1) + 1]) {
	ky1 = 2;
    } else {
	ky1 = 1;
    }
    k1 = kx1 + ky1 * 3;
    *lvalid = TRUE_;
    if (k1 == 4 && k0 == 4) {
/*       / VISIBLE / */
	istart[*ii] = 2;
	vx[(*ii << 1) + 1] = *vx1;
	vy[(*ii << 1) + 1] = *vy1;
	return 0;
    } else if (k1 == 4 || k0 == 4) {
/*       / HALF VISIBLE / */
	if (kx1 == 1 && kx0 == 1) {
/*         / CROSSES VERTICAL EDGE / */
	    kyl = max(ky1,ky0) / 2 + 1;
	    yi = ylim[kyl + (*ii << 1) - 1];
	    xi = *vx0 + (yi - *vy0) / (*vy1 - *vy0) * (*vx1 - *vx0);
	} else if (ky1 == 1 && ky0 == 1) {
/*         / CROSSES HORIZONTAL EDGE / */
	    kxl = max(kx1,kx0) / 2 + 1;
	    xi = xlim[kxl + (*ii << 1) - 1];
	    yi = *vy0 + (xi - *vx0) / (*vx1 - *vx0) * (*vy1 - *vy0);
	} else {
/*         / CROSSES DIAGONAL CORNERS / */
	    kxl = max(kx1,kx0) / 2 + 1;
	    kyl = max(ky1,ky0) / 2 + 1;
	    dydx = (*vy1 - *vy0) / (*vx1 - *vx0);
	    yc = *vy1 - dydx * *vx1;
	    xi = xlim[kxl + (*ii << 1) - 1];
	    yi = yc + dydx * xi;
	    if (yi < ylim[*ii * 2] || yi > ylim[(*ii << 1) + 1]) {
		yi = ylim[kyl + (*ii << 1) - 1];
		xi = (yi - yc) / dydx;
	    }
	}
	if (k1 == 4) {
	    istart[*ii] = 1;
	    vx[*ii * 2] = xi;
	    vy[*ii * 2] = yi;
	    vx[(*ii << 1) + 1] = *vx1;
	    vy[(*ii << 1) + 1] = *vy1;
	} else {
	    istart[*ii] = 2;
	    vx[(*ii << 1) + 1] = xi;
	    vy[(*ii << 1) + 1] = yi;
	}
    } else if ((i__1 = kx1 - kx0, abs(i__1)) + (i__2 = ky1 - ky0, abs(i__2)) 
	    >= 2) {
/*       / LINE GOES THROUGH WINDOWED SECTION, BUT STARTS AND ENDS */
/*         ELSEWHERE.  TEST TO SEE IF VERTICAL OR HORIZONTAL. / */
	istart[*ii] = 1;
	if (*vx1 == *vx0) {
	    vx[*ii * 2] = *vx1;
	    vy[*ii * 2] = ylim[*ii * 2];
	    vx[(*ii << 1) + 1] = *vx1;
	    vy[(*ii << 1) + 1] = ylim[(*ii << 1) + 1];
	    if (kx0 % 2 == 0) {
		*lvalid = FALSE_;
	    }
	} else if (*vy1 == *vy0) {
	    vx[*ii * 2] = xlim[*ii * 2];
	    vy[*ii * 2] = *vy1;
	    vx[(*ii << 1) + 1] = xlim[(*ii << 1) + 1];
	    vy[(*ii << 1) + 1] = *vy1;
	    if (ky0 % 2 == 0) {
		*lvalid = FALSE_;
	    }
	} else {
	    dydx = (*vy1 - *vy0) / (*vx1 - *vx0);
	    dxdy = 1.f / dydx;
	    yc = *vy1 - dydx * *vx1;
	    yj[0] = ylim[(*ii << 1) + 1];
	    xj[0] = dxdy * (yj[0] - yc);
	    xj[1] = xlim[*ii * 2];
	    yj[1] = yc + dydx * xj[1];
	    xj[2] = xlim[(*ii << 1) + 1];
	    yj[2] = yc + dydx * xj[2];
	    yj[3] = ylim[*ii * 2];
	    xj[3] = dxdy * (yj[3] - yc);
	    kk = 0;
	    if (xj[0] >= xlim[*ii * 2] && xj[0] <= xlim[(*ii << 1) + 1]) {
		++kk;
		indx[kk - 1] = 1;
	    }
	    if (yj[1] >= ylim[*ii * 2] && yj[1] <= ylim[(*ii << 1) + 1]) {
		++kk;
		indx[kk - 1] = 2;
	    }
	    if (yj[2] >= ylim[*ii * 2] && yj[2] <= ylim[(*ii << 1) + 1]) {
		++kk;
		indx[kk - 1] = 3;
	    }
	    if (xj[3] >= xlim[*ii * 2] && xj[3] <= xlim[(*ii << 1) + 1]) {
		++kk;
		indx[kk - 1] = 4;
	    }
	    if (kk >= 1) {
		i1 = indx[0];
		i2 = indx[kk - 1];
		vx[*ii * 2] = xj[i1 - 1];
		vy[*ii * 2] = yj[i1 - 1];
		vx[(*ii << 1) + 1] = xj[i2 - 1];
		vy[(*ii << 1) + 1] = yj[i2 - 1];
	    } else {
		*lvalid = FALSE_;
	    }
	}
    } else {
	*lvalid = FALSE_;
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_szgcll:
    *xx = vx[istart[*ii] + (*ii << 1) - 1];
    *yy = vy[istart[*ii] + (*ii << 1) - 1];
    *lcont = istart[*ii] == 1;
    *lmove = istart[*ii] == 1;
    ++istart[*ii];
    return 0;
/* ----------------------------------------------------------------------- */

L_szscll:
    xlim[*ii * 2] = *vxmin;
    xlim[(*ii << 1) + 1] = *vxmax;
    ylim[*ii * 2] = *vymin;
    ylim[(*ii << 1) + 1] = *vymax;
    return 0;
/* ----------------------------------------------------------------------- */

L_szqcll:
    *vxmin = xlim[*ii * 2];
    *vxmax = xlim[(*ii << 1) + 1];
    *vymin = ylim[*ii * 2];
    *vymax = ylim[(*ii << 1) + 1];
    return 0;
} /* szpcll_ */

/* Subroutine */ int szpcll_(real *vx0, real *vy0, real *vx1, real *vy1, 
	logical *lvalid, integer *ii)
{
    return szpcll_0_(0, vx0, vy0, vx1, vy1, lvalid, ii, (real *)0, (real *)0, 
	    (logical *)0, (logical *)0, (real *)0, (real *)0, (real *)0, (
	    real *)0);
    }

/* Subroutine */ int szgcll_(real *xx, real *yy, logical *lcont, logical *
	lmove, integer *ii)
{
    return szpcll_0_(1, (real *)0, (real *)0, (real *)0, (real *)0, (logical *
	    )0, ii, xx, yy, lcont, lmove, (real *)0, (real *)0, (real *)0, (
	    real *)0);
    }

/* Subroutine */ int szscll_(real *vxmin, real *vxmax, real *vymin, real *
	vymax, integer *ii)
{
    return szpcll_0_(2, (real *)0, (real *)0, (real *)0, (real *)0, (logical *
	    )0, ii, (real *)0, (real *)0, (logical *)0, (logical *)0, vxmin, 
	    vxmax, vymin, vymax);
    }

/* Subroutine */ int szqcll_(real *vxmin, real *vxmax, real *vymin, real *
	vymax, integer *ii)
{
    return szpcll_0_(3, (real *)0, (real *)0, (real *)0, (real *)0, (logical *
	    )0, ii, (real *)0, (real *)0, (logical *)0, (logical *)0, vxmin, 
	    vxmax, vymin, vymax);
    }


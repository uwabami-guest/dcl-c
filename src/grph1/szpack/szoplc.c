/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Common Block Declarations */

struct {
    logical lchaz;
} szbls3_;

#define szbls3_1 szbls3_

/* Table of constant values */

static real c_b35 = 180.f;
static integer c__0 = 0;

/* ----------------------------------------------------------------------- */
/*     PLOT ROUTINE ON VC (ENCODING TEXT) */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int szoplc_0_(int n__, real *vx, real *vy, char *ch, real *
	h__, ftnlen ch_len)
{
    /* Initialized data */

    static logical lcset = FALSE_;

    /* System generated locals */
    integer i__1;
    real r__1, r__2;

    /* Builtin functions */
    double sqrt(doublereal), atan2(doublereal, doublereal), r_mod(real *, 
	    real *);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer n;
    static real r__, fc;
    static integer nc;
    static real fl, wc, xi, hz, wl;
    static integer nt;
    static real xl, xw, xl0, vx0, vy0, vx1, vy1, fwc;
    static char chz[1024];
    static real cwl, xlc;
    static integer ith;
    static real vxc, vyc, vxt[400], vyt[400];
    extern real rr2d_(real *);
    extern integer lenc_(char *, ftnlen);
    static real ffct;
    static integer nchz;
    static real wxch, wych;
    static integer irot;
    static logical lrot, lchar, lbuff;
    static integer nbuff;
    static real rbuff;
    static integer index;
    static logical lcput, lcurv;
    static real rcurv;
    extern /* Subroutine */ int sgiget_(char *, integer *, ftnlen), sglget_(
	    char *, logical *, ftnlen), msgdmp_(char *, char *, char *, 
	    ftnlen, ftnlen, ftnlen), sgrget_(char *, real *, ftnlen), szclld_(
	    void);
    static integer indexy, indexz;
    extern /* Subroutine */ int szplld_(real *, real *), szopld_(void), 
	    szmvld_(real *, real *), szqidx_(integer *), szsidx_(integer *), 
	    sztxcl_(void), sztxop_(real *, integer *, integer *, integer *), 
	    szqtxw_(char *, integer *, real *, real *, ftnlen), sztxzv_(real *
	    , real *, char *, ftnlen);

    switch(n__) {
	case 1: goto L_szmvlc;
	case 2: goto L_szpllc;
	case 3: goto L_szcllc;
	case 4: goto L_szschz;
	case 5: goto L_szqchz;
	case 6: goto L_szcrst;
	}

/*     / OPEN DASH LINE & CHARACTER SEGMENT / */
    lchar = szbls3_1.lchaz && lcset;
    if (lchar) {
	sglget_("LROT", &lrot, (ftnlen)4);
	sgiget_("IROT", &irot, (ftnlen)4);
	sgrget_("FWC", &fwc, (ftnlen)3);
	sgrget_("CWL", &cwl, (ftnlen)3);
	sgrget_("FFCT", &ffct, (ftnlen)4);
	sgiget_("INDEXC", &index, (ftnlen)6);
	sglget_("LBUFF", &lbuff, (ftnlen)5);
	sgiget_("NBUFF", &nbuff, (ftnlen)5);
	sgrget_("RBUFF", &rbuff, (ftnlen)5);
	sglget_("LCURV", &lcurv, (ftnlen)5);
	sgrget_("RCURV", &rcurv, (ftnlen)5);
	if (! (fwc >= 1.f)) {
	    msgdmp_("E", "SZOPLC", "PARAMETER 'FWC' IS LESS THAN 1.", (ftnlen)
		    1, (ftnlen)6, (ftnlen)31);
	}
	if (! (cwl > 0.f)) {
	    msgdmp_("E", "SZOPLC", "PARAMETER 'CWL' IS LESS THAN 0.", (ftnlen)
		    1, (ftnlen)6, (ftnlen)31);
	}
	if (! (0.f < ffct && ffct < 1.f)) {
	    msgdmp_("E", "SZOPLC", "PARAMETER 'FFCT' IS NOT IN THE RANGE OF "
		    "(0,1).", (ftnlen)1, (ftnlen)6, (ftnlen)46);
	}
	if (! (1 <= nbuff && nbuff <= 400)) {
	    msgdmp_("E", "SZOPLC", "PARAMETER 'NBUFF' IS NOT IN THE RANGE OF"
		    " (1,MAXTMP).", (ftnlen)1, (ftnlen)6, (ftnlen)52);
	}
	if (! (0.f < rbuff && rbuff < 1.f)) {
	    msgdmp_("E", "SZOPLC", "PARAMETER 'RBUFF' IS NOT IN THE RANGE OF"
		    " (0,1).", (ftnlen)1, (ftnlen)6, (ftnlen)47);
	}
	if (! (0.f < rcurv && rcurv < fwc)) {
	    msgdmp_("E", "SZOPLC", "PARAMETER 'RCURV' IS NOT IN THE RANGE OF"
		    " (0,FWC).", (ftnlen)1, (ftnlen)6, (ftnlen)49);
	}
	szqtxw_(chz, &nchz, &wxch, &wych, (ftnlen)1024);
	fc = wxch * fwc;
	fl = cwl;
	wc = hz * fc;
	wl = hz * fl;
	xw = wl + wc;
	xi = wl * ffct;
    }
    if (lbuff) {
	nt = 0;
    }
    szopld_();
    return 0;
/* ----------------------------------------------------------------------- */

L_szmvlc:
/*     / PEN-UP MOVE / */
    if (lbuff && nt != 0) {
	i__1 = nt;
	for (n = 1; n <= i__1; ++n) {
	    szplld_(&vxt[n - 1], &vyt[n - 1]);
/* L5: */
	}
	nt = 0;
    }
    szmvld_(vx, vy);
    vx0 = *vx;
    vy0 = *vy;
    xl0 = xi;
    return 0;
/* ----------------------------------------------------------------------- */

L_szpllc:
/*     / PEN-DOWN MOVE / */
    if (! lchar) {
/*       / THERE IS NO TEXT IN THE CYCLE / */
	szplld_(vx, vy);
    } else {
/*       / THERE EXISTS A TEXT IN THE CYCLE / */
L10:
/* Computing 2nd power */
	r__1 = *vx - vx0;
/* Computing 2nd power */
	r__2 = *vy - vy0;
	r__ = sqrt(r__1 * r__1 + r__2 * r__2);
	if (r__ == 0.f) {
	    return 0;
	}
	xl = xl0 + r__;
	if (xl < wl) {
/*           / CURRENET POSITION IS IN THE LINE PART / */
	    szplld_(vx, vy);
	    vx0 = *vx;
	    vy0 = *vy;
	    xl0 = xl;
	} else if (xl < xw) {
/*           / CURRENT POSITION IS IN THE CHARCTER PART / */
	    if (xl0 < wl) {
/*             / LAST POSITION WAS IN THE LINE PART / */
		vx1 = vx0 + (*vx - vx0) * (wl - xl0) / r__;
		vy1 = vy0 + (*vy - vy0) * (wl - xl0) / r__;
		szplld_(&vx1, &vy1);
	    }
	    vx0 = *vx;
	    vy0 = *vy;
	    xl0 = xl;
	    if (lbuff) {
/*           / BUFFERRING / */
		++nt;
		vxt[nt - 1] = *vx;
		vyt[nt - 1] = *vy;
		if (nt == nbuff) {
		    i__1 = nt;
		    for (n = 1; n <= i__1; ++n) {
			szplld_(&vxt[n - 1], &vyt[n - 1]);
/* L15: */
		    }
		    xl0 = wl * rbuff;
		    nt = 0;
		}
	    }
	} else {
/*           / CURRENT POSITION IS IN THE NEXT CYCLE / */
	    if (xl0 < wl) {
/*             / LAST POSITION WAS IN THE LINE PART / */
		vx1 = vx0 + (*vx - vx0) * (wl - xl0) / r__;
		vy1 = vy0 + (*vy - vy0) * (wl - xl0) / r__;
		szplld_(&vx1, &vy1);
	    }
/*           / POSITION OF TEXT / */
	    vx0 += (*vx - vx0) * (xw - xl0) / r__;
	    vy0 += (*vy - vy0) * (xw - xl0) / r__;
	    vxc = (vx0 + vx1) / 2;
	    vyc = (vy0 + vy1) / 2;
	    lcput = TRUE_;
	    if (lbuff && lcurv) {
/*             / CHECK CURVATURE (RUN PATH) / */
/* Computing 2nd power */
		r__1 = vx1 - vx0;
/* Computing 2nd power */
		r__2 = vy1 - vy0;
		xlc = sqrt(r__1 * r__1 + r__2 * r__2);
		if (xlc <= hz * wxch * rcurv) {
		    i__1 = nt;
		    for (n = 1; n <= i__1; ++n) {
			szplld_(&vxt[n - 1], &vyt[n - 1]);
/* L20: */
		    }
		    xl0 = wl * rbuff;
		    nt = 0;
		    szplld_(vx, vy);
		    vx0 = *vx;
		    vy0 = *vy;
		    lcput = FALSE_;
		}
	    }
	    if (lcput) {
/*             / ROTATION OPTION / */
		if (lrot) {
		    ith = irot;
		} else {
		    r__2 = atan2(vy0 - vy1, vx0 - vx1);
		    r__1 = rr2d_(&r__2) + 270;
		    ith = r_mod(&r__1, &c_b35) - 90;
		}
/*             / WRITE TEXT / */
		szqidx_(&indexz);
		if (index == 0) {
		    indexy = indexz;
		} else {
		    indexy = index;
		}
		szclld_();
/*             CALL SGTXZV(VXC,VYC,CHZ,HZ,ITH,0,INDEXY) */
		sztxop_(&hz, &ith, &c__0, &indexy);
		sztxzv_(&vxc, &vyc, chz, (ftnlen)1024);
		sztxcl_();
		szsidx_(&indexz);
		szopld_();
		szmvld_(&vx0, &vy0);
		xl0 = 0.f;
		nt = 0;
	    }
	}
	if (! (xl < xw)) {
	    goto L10;
	}
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_szcllc:
/*     / CLOSE DASHCHAR SEGMENT / */
    if (lbuff && nt != 0) {
	i__1 = nt;
	for (n = 1; n <= i__1; ++n) {
	    szplld_(&vxt[n - 1], &vyt[n - 1]);
/* L25: */
	}
    }
    szclld_();
    return 0;
/* ----------------------------------------------------------------------- */

L_szschz:
    nc = lenc_(ch, ch_len);
    s_copy(chz, ch, (ftnlen)1024, nc);
    hz = *h__;
    lcset = TRUE_;
    return 0;
/* ----------------------------------------------------------------------- */

L_szqchz:
    if (! lcset) {
	msgdmp_("E", "SZQCHZ", "TEXT HAS NOT BEEN SET YET.", (ftnlen)1, (
		ftnlen)6, (ftnlen)26);
    }
    s_copy(ch, chz, ch_len, (ftnlen)1024);
    *h__ = hz;
    return 0;
/* ----------------------------------------------------------------------- */

L_szcrst:
    lcset = FALSE_;
    return 0;
} /* szoplc_ */

/* Subroutine */ int szoplc_(void)
{
    return szoplc_0_(0, (real *)0, (real *)0, (char *)0, (real *)0, (ftnint)0)
	    ;
    }

/* Subroutine */ int szmvlc_(real *vx, real *vy)
{
    return szoplc_0_(1, vx, vy, (char *)0, (real *)0, (ftnint)0);
    }

/* Subroutine */ int szpllc_(real *vx, real *vy)
{
    return szoplc_0_(2, vx, vy, (char *)0, (real *)0, (ftnint)0);
    }

/* Subroutine */ int szcllc_(void)
{
    return szoplc_0_(3, (real *)0, (real *)0, (char *)0, (real *)0, (ftnint)0)
	    ;
    }

/* Subroutine */ int szschz_(char *ch, real *h__, ftnlen ch_len)
{
    return szoplc_0_(4, (real *)0, (real *)0, ch, h__, ch_len);
    }

/* Subroutine */ int szqchz_(char *ch, real *h__, ftnlen ch_len)
{
    return szoplc_0_(5, (real *)0, (real *)0, ch, h__, ch_len);
    }

/* Subroutine */ int szcrst_(void)
{
    return szoplc_0_(6, (real *)0, (real *)0, (char *)0, (real *)0, (ftnint)0)
	    ;
    }


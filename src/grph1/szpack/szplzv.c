/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Common Block Declarations */

struct {
    logical lmiss;
    real rmiss;
} szbpl1_;

#define szbpl1_1 szbpl1_

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int szplzv_(integer *n, real *vpx, real *vpy)
{
    /* System generated locals */
    integer i__1;

    /* Local variables */
    static integer i__;
    static logical lflag;
    extern /* Subroutine */ int szcllv_(void), szpllv_(real *, real *), 
	    szoplv_(void), szmvlv_(real *, real *);

    /* Parameter adjustments */
    --vpy;
    --vpx;

    /* Function Body */
    szoplv_();
    if (! szbpl1_1.lmiss) {
	szmvlv_(&vpx[1], &vpy[1]);
	i__1 = *n;
	for (i__ = 2; i__ <= i__1; ++i__) {
	    szpllv_(&vpx[i__], &vpy[i__]);
/* L10: */
	}
    } else {
	lflag = FALSE_;
	i__1 = *n;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    if (vpx[i__] == szbpl1_1.rmiss || vpy[i__] == szbpl1_1.rmiss) {
		lflag = FALSE_;
	    } else if (lflag) {
		szpllv_(&vpx[i__], &vpy[i__]);
	    } else {
		szmvlv_(&vpx[i__], &vpy[i__]);
		lflag = TRUE_;
	    }
/* L20: */
	}
    }
    szcllv_();
    return 0;
} /* szplzv_ */


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static real c_b2 = 1e-5f;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int szsgcl_0_(int n__, real *tx0, real *ty0, real *tx1, real 
	*ty1, real *tx, real *ty)
{
    /* System generated locals */
    real r__1, r__2, r__3, r__4;

    /* Builtin functions */
    double tan(doublereal), cos(doublereal), sin(doublereal), atan2(
	    doublereal, doublereal), r_sign(real *, real *), atan(doublereal),
	     acos(doublereal);

    /* Local variables */
    static real pi, tt, cs0, cs1, dx1, xla, xlm, txx, tyy, tan0, tan1;
    static logical lmer;
    extern real rfpi_(void);
    static real alpha;
    extern logical lreqa_(real *, real *, real *);
    extern real szxmod_(real *);

    switch(n__) {
	case 1: goto L_szqgcy;
	case 2: goto L_szqgcx;
	}

    pi = rfpi_();
    txx = *tx0;
    tyy = *ty0;
    r__1 = abs(*ty0);
    r__2 = pi / 2;
    r__3 = abs(*ty1);
    r__4 = pi / 2;
    if (lreqa_(&r__1, &r__2, &c_b2) || lreqa_(&r__3, &r__4, &c_b2)) {
	lmer = TRUE_;
    } else {
	tan0 = tan(*ty0);
	tan1 = tan(*ty1);
	if (tan0 == 0.f && tan1 == 0.f) {
	    xla = 0.f;
	} else {
	    xla = atan2(tan1 * cos(*tx0) - tan0 * cos(*tx1), tan0 * sin(*tx1) 
		    - tan1 * sin(*tx0));
	}
	r__1 = *tx1 - *tx0;
	dx1 = szxmod_(&r__1);
	r__1 = *tx0 + dx1 / 2 - xla;
	xlm = szxmod_(&r__1);
	cs0 = cos(*tx0 - xla);
	cs1 = cos(*tx1 - xla);
	if (abs(cs0) >= abs(cs1)) {
	    alpha = tan0 / cs0;
	    lmer = FALSE_;
	} else if (abs(cs1) != 0.f) {
	    alpha = tan1 / cs1;
	    lmer = FALSE_;
	} else {
	    lmer = TRUE_;
	}
    }
    if (lmer) {
	r__1 = pi / 2;
	r__2 = *ty0 + *ty1;
	tyy = r_sign(&r__1, &r__2);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_szqgcy:
    if (lmer) {
	*ty = tyy;
    } else {
	*ty = atan(alpha * cos(*tx - xla));
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_szqgcx:
    if (lmer) {
	*tx = txx;
    } else {
	if (alpha == 0.f) {
	    *tx = txx;
	} else {
	    tt = tan(*ty) / alpha;
	    if (abs(tt) >= 1.f) {
		*tx = r_sign(&pi, &xlm) + xla;
	    } else {
		r__1 = acos(tt);
		*tx = r_sign(&r__1, &xlm) + xla;
	    }
	}
    }
    return 0;
} /* szsgcl_ */

/* Subroutine */ int szsgcl_(real *tx0, real *ty0, real *tx1, real *ty1)
{
    return szsgcl_0_(0, tx0, ty0, tx1, ty1, (real *)0, (real *)0);
    }

/* Subroutine */ int szqgcy_(real *tx, real *ty)
{
    return szsgcl_0_(1, (real *)0, (real *)0, (real *)0, (real *)0, tx, ty);
    }

/* Subroutine */ int szqgcx_(real *ty, real *tx)
{
    return szsgcl_0_(2, (real *)0, (real *)0, (real *)0, (real *)0, tx, ty);
    }


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Common Block Declarations */

struct {
    integer irmode, irmodr;
} szbtn2_;

#define szbtn2_1 szbtn2_

struct {
    integer itpt1, itpt2;
} szbtn4_;

#define szbtn4_1 szbtn4_

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     TONE PRIMITIVE (3-D) */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int szt3op_0_(int n__, integer *itpat1, integer *itpat2)
{
    /* Builtin functions */
    integer s_wsfi(icilist *), do_fio(integer *, char *, ftnlen), e_wsfi(void)
	    ;

    /* Local variables */
    static char cobj[80];
    extern /* Subroutine */ int cdblk_(char *, ftnlen), sgiget_(char *, 
	    integer *, ftnlen), swocls_(char *, ftnlen), swoopn_(char *, char 
	    *, ftnlen, ftnlen);

    /* Fortran I/O blocks */
    static icilist io___2 = { 0, cobj, 0, "(2I8)", 80, 1 };


    switch(n__) {
	case 1: goto L_szt3cl;
	}

    sgiget_("IRMODE", &szbtn2_1.irmode, (ftnlen)6);
    szbtn2_1.irmode %= 2;
    s_wsfi(&io___2);
    do_fio(&c__1, (char *)&(*itpat1), (ftnlen)sizeof(integer));
    do_fio(&c__1, (char *)&(*itpat2), (ftnlen)sizeof(integer));
    e_wsfi();
    cdblk_(cobj, (ftnlen)80);
    swoopn_("SZT3", cobj, (ftnlen)4, (ftnlen)80);
    szbtn4_1.itpt1 = *itpat1;
    szbtn4_1.itpt2 = *itpat2;
    return 0;
/* ----------------------------------------------------------------------- */

L_szt3cl:
    swocls_("SZT3", (ftnlen)4);
    return 0;
} /* szt3op_ */

/* Subroutine */ int szt3op_(integer *itpat1, integer *itpat2)
{
    return szt3op_0_(0, itpat1, itpat2);
    }

/* Subroutine */ int szt3cl_(void)
{
    return szt3op_0_(1, (integer *)0, (integer *)0);
    }


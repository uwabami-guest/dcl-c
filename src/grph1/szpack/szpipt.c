/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Common Block Declarations */

struct {
    logical llnint, lgcint;
    real rdxr, rdyr;
} szbls1_;

#define szbls1_1 szbls1_

/* Table of constant values */

static real c_b2 = 1e-5f;
static real c_b6 = 0.f;

/* ----------------------------------------------------------------------- */
/*     INTERPOLATION ON TC (GREAT CIRCLE INTERPOLATION) */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int szpipt_0_(int n__, real *tx0, real *ty0, real *tx1, real 
	*ty1, integer *mode, real *tx, real *ty, logical *lcont)
{
    /* System generated locals */
    real r__1, r__2;

    /* Builtin functions */
    double r_sign(real *, real *);

    /* Local variables */
    static real pi;
    static integer nn;
    static real dx1, dy1, xx0, xx1, yy0, yy1, adx, ady, xla, yla, dxx, dyy;
    static integer nend;
    static logical lmap;
    extern real rfpi_(void);
    static logical lstd;
    extern logical lreqa_(real *, real *, real *);
    static logical lxmod, lintt;
    extern /* Subroutine */ int szsgcl_(real *, real *, real *, real *), 
	    szqgcx_(real *, real *), szqgcy_(real *, real *), stqtrf_(logical 
	    *);
    extern real szxmod_(real *);

/*     MODE=0 : FOR LINE */
/*          1 : FOR TONE */
/*          2 : FOR ARROW */
    switch(n__) {
	case 1: goto L_szgipt;
	}

    pi = rfpi_();
    if (*mode == 0) {
	lintt = szbls1_1.lgcint;
    } else if (*mode == 1) {
	r__1 = abs(*ty0);
	r__2 = pi / 2;
	lintt = szbls1_1.lgcint && ! (lreqa_(ty0, ty1, &c_b2) && lreqa_(&r__1,
		 &r__2, &c_b2));
    } else if (*mode == 2) {
	stqtrf_(&lmap);
	lintt = szbls1_1.lgcint && lmap;
    }
    xx0 = *tx0;
    yy0 = *ty0;
    xx1 = *tx1;
    yy1 = *ty1;
    nn = 1;
    nend = 1;
    if (! lintt) {
	return 0;
    }
    r__1 = *tx1 - *tx0;
    dx1 = szxmod_(&r__1);
    dy1 = *ty1 - *ty0;
    adx = abs(dx1);
    ady = abs(dy1);
    if (adx < szbls1_1.rdxr && ady < szbls1_1.rdxr) {
	return 0;
    }
    lstd = FALSE_;
    r__1 = abs(*ty0);
    r__2 = pi / 2;
    if (lreqa_(&r__1, &r__2, &c_b2)) {
	r__1 = pi / 2;
	yla = r_sign(&r__1, ty0);
	xla = *tx1;
	lxmod = adx >= ady;
    } else /* if(complicated condition) */ {
	r__1 = abs(*ty1);
	r__2 = pi / 2;
	if (lreqa_(&r__1, &r__2, &c_b2)) {
	    r__1 = pi / 2;
	    yla = r_sign(&r__1, ty1);
	    xla = *tx0;
	    lxmod = adx >= ady;
	} else if (lreqa_(&adx, &c_b6, &c_b2)) {
	    xla = *tx0;
	    lxmod = FALSE_;
	} else if (lreqa_(&adx, &pi, &c_b2)) {
	    r__1 = pi / 2;
	    r__2 = *ty0 + *ty1;
	    yla = r_sign(&r__1, &r__2);
	    lxmod = TRUE_;
	} else {
/*       / THE FOLLOWING PART IS NOT COMPLETE ! / */
	    lstd = TRUE_;
	    lxmod = adx >= ady;
	    szsgcl_(tx0, ty0, tx1, ty1);
	}
    }
    if (lxmod) {
	nend = adx / szbls1_1.rdxr + 1;
	dxx = dx1 / nend;
    } else {
	nend = ady / szbls1_1.rdyr + 1;
	dyy = dy1 / nend;
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_szgipt:
    *lcont = nn < nend;
    if (*lcont) {
	if (lxmod) {
	    *tx = xx0 + nn * dxx;
	    if (lstd) {
		szqgcy_(tx, ty);
	    } else {
		*ty = yla;
	    }
	} else {
	    *ty = yy0 + nn * dyy;
	    if (lstd) {
		szqgcx_(ty, tx);
	    } else {
		*tx = xla;
	    }
	}
    } else {
	*tx = xx1;
	*ty = yy1;
    }
    ++nn;
    return 0;
} /* szpipt_ */

/* Subroutine */ int szpipt_(real *tx0, real *ty0, real *tx1, real *ty1, 
	integer *mode)
{
    return szpipt_0_(0, tx0, ty0, tx1, ty1, mode, (real *)0, (real *)0, (
	    logical *)0);
    }

/* Subroutine */ int szgipt_(real *tx, real *ty, logical *lcont)
{
    return szpipt_0_(1, (real *)0, (real *)0, (real *)0, (real *)0, (integer *
	    )0, tx, ty, lcont);
    }


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__256 = 256;
static integer c__6000 = 6000;
static integer c__1 = 1;
static real c_b21 = -12.f;
static real c_b24 = 12.f;

/* ----------------------------------------------------------------------- */
/*     FONT LOADING */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int szfint_0_(int n__, integer *ncnt, integer *jposx, char *
	clx, char *cly, real *wx1, real *wx2, ftnlen clx_len, ftnlen cly_len)
{
    /* Initialized data */

    static integer ncntz = 0;
    static integer ifontz = 0;
    static logical lpropz = FALSE_;

    /* System generated locals */
    olist o__1;
    cllist cl__1;
    alist al__1;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_cmp(char *, char *, ftnlen, ftnlen), f_open(olist *), f_rew(
	    alist *), s_rsue(cilist *), do_uio(integer *, char *, ftnlen), 
	    e_rsue(void), f_clos(cllist *);

    /* Local variables */
    static integer n, iu;
    static real vx1[256], vx2[256];
    static char ckx[1*6000], cky[1*6000];
    static integer ips;
    static char cdsn[1024];
    extern /* Subroutine */ int rset0_(real *, integer *, integer *, real *);
    static char cpara[8];
    extern /* Subroutine */ int chngi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen);
    static integer ifont;
    static logical lprop;
    static integer iposx[256];
    extern /* Subroutine */ int viset0_(integer *, integer *, integer *, 
	    integer *, integer *), vrset0_(real *, real *, integer *, integer 
	    *, integer *), sgiget_(char *, integer *, ftnlen), sglget_(char *,
	     logical *, ftnlen), msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen);
    extern integer iufopn_(void);
    extern /* Subroutine */ int swqfnm_(char *, char *, ftnlen, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___9 = { 0, 0, 0, 0, 0 };
    static cilist io___11 = { 0, 0, 0, 0, 0 };
    static cilist io___13 = { 0, 0, 0, 0, 0 };


    /* Parameter adjustments */
    if (jposx) {
	--jposx;
	}
    if (clx) {
	--clx;
	}
    if (cly) {
	--cly;
	}
    if (wx1) {
	--wx1;
	}
    if (wx2) {
	--wx2;
	}

    /* Function Body */
    switch(n__) {
	case 1: goto L_szqfnt;
	case 2: goto L_szqfnw;
	}

/*     / CHECK FONT NUMBER & LFPROP OPTION / */
    sgiget_("IFONT", &ifont, (ftnlen)5);
    sglget_("LFPROP", &lprop, (ftnlen)6);
    if (! (1 <= ifont && ifont <= 2)) {
	msgdmp_("E", "SZINIT", "FONT NUMBER IS OUT OF RANGE.", (ftnlen)1, (
		ftnlen)6, (ftnlen)28);
    }
    if (ifont == ifontz && lpropz == lprop) {
	*ncnt = ncntz;
	return 0;
    } else {
	++ncntz;
	*ncnt = ncntz;
    }
/*     / FONT LOADING / */
    if (ifont != ifontz) {
/*       / INQUIRE FONT FILE NAMES / */
	s_copy(cpara, "FONT#", (ftnlen)8, (ftnlen)5);
	chngi_(cpara, "#", &ifont, "(I1)", (ftnlen)8, (ftnlen)1, (ftnlen)4);
	swqfnm_(cpara, cdsn, (ftnlen)8, (ftnlen)1024);
	if (s_cmp(cdsn, " ", (ftnlen)1024, (ftnlen)1) == 0) {
	    msgdmp_("E", "SZFONT", "FONT FILE DOES NOT EXIST.", (ftnlen)1, (
		    ftnlen)6, (ftnlen)25);
	}
/*       / OPEN FONT FILE : MAYBE SYSTEM DEPENDENT / */
	iu = iufopn_();
	o__1.oerr = 0;
	o__1.ounit = iu;
	o__1.ofnmlen = 1024;
	o__1.ofnm = cdsn;
	o__1.orl = 0;
	o__1.osta = 0;
	o__1.oacc = 0;
	o__1.ofm = "UNFORMATTED";
	o__1.oblnk = 0;
	f_open(&o__1);
	al__1.aerr = 0;
	al__1.aunit = iu;
	f_rew(&al__1);
/*       / LOAD TEXT FONT / */
	io___9.ciunit = iu;
	s_rsue(&io___9);
	do_uio(&c__256, (char *)&iposx[0], (ftnlen)sizeof(integer));
	e_rsue();
	io___11.ciunit = iu;
	s_rsue(&io___11);
	do_uio(&c__6000, ckx, (ftnlen)1);
	e_rsue();
	io___13.ciunit = iu;
	s_rsue(&io___13);
	do_uio(&c__6000, cky, (ftnlen)1);
	e_rsue();
/*       / CLOSE FONT FILE / */
	cl__1.cerr = 0;
	cl__1.cunit = iu;
	cl__1.csta = 0;
	f_clos(&cl__1);
	ifontz = ifont;
    }
/*     / FONT WIDTH / */
    if (lprop) {
	for (n = 1; n <= 256; ++n) {
	    ips = iposx[n - 1];
	    vx1[n - 1] = *(unsigned char *)&ckx[ips - 1] - 64 - 0.f;
	    vx2[n - 1] = *(unsigned char *)&cky[ips - 1] - 64 + 0.f;
/* L10: */
	}
    } else {
	rset0_(vx1, &c__256, &c__1, &c_b21);
	rset0_(vx2, &c__256, &c__1, &c_b24);
    }
    lpropz = lprop;
    return 0;
/* ----------------------------------------------------------------------- */

L_szqfnt:
    if (ncntz == 0) {
	msgdmp_("E", "SZQFNT", "FONT FILE HAS NOT BEEN LOADED.", (ftnlen)1, (
		ftnlen)6, (ftnlen)30);
    }
    viset0_(iposx, &jposx[1], &c__256, &c__1, &c__1);
    for (n = 1; n <= 6000; ++n) {
	*(unsigned char *)&clx[n] = *(unsigned char *)&ckx[n - 1];
	*(unsigned char *)&cly[n] = *(unsigned char *)&cky[n - 1];
/* L20: */
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_szqfnw:
    if (ncntz == 0) {
	msgdmp_("E", "SZQFNW", "FONT FILE HAS NOT BEEN LOADED.", (ftnlen)1, (
		ftnlen)6, (ftnlen)30);
    }
    vrset0_(vx1, &wx1[1], &c__256, &c__1, &c__1);
    vrset0_(vx2, &wx2[1], &c__256, &c__1, &c__1);
    return 0;
} /* szfint_ */

/* Subroutine */ int szfint_(integer *ncnt)
{
    return szfint_0_(0, ncnt, (integer *)0, (char *)0, (char *)0, (real *)0, (
	    real *)0, (ftnint)0, (ftnint)0);
    }

/* Subroutine */ int szqfnt_(integer *jposx, char *clx, char *cly, ftnlen 
	clx_len, ftnlen cly_len)
{
    return szfint_0_(1, (integer *)0, jposx, clx, cly, (real *)0, (real *)0, 
	    clx_len, cly_len);
    }

/* Subroutine */ int szqfnw_(real *wx1, real *wx2)
{
    return szfint_0_(2, (integer *)0, (integer *)0, (char *)0, (char *)0, wx1,
	     wx2, (ftnint)0, (ftnint)0);
    }


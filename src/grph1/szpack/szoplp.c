/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     PLOT ROUTINE ON VC (PERSPECTIVE TRANSFORMATION) */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int szoplp_0_(int n__, real *vx, real *vy)
{
    static real rx, ry;
    extern /* Subroutine */ int stfpr2_(real *, real *, real *, real *), 
	    szcllr_(void), szpllr_(real *, real *), szoplr_(void), szmvlr_(
	    real *, real *);

    switch(n__) {
	case 1: goto L_szmvlp;
	case 2: goto L_szpllp;
	case 3: goto L_szcllp;
	}

    szoplr_();
    return 0;
/* ----------------------------------------------------------------------- */

L_szmvlp:
    stfpr2_(vx, vy, &rx, &ry);
    szmvlr_(&rx, &ry);
    return 0;
/* ----------------------------------------------------------------------- */

L_szpllp:
    stfpr2_(vx, vy, &rx, &ry);
    szpllr_(&rx, &ry);
    return 0;
/* ----------------------------------------------------------------------- */

L_szcllp:
    szcllr_();
    return 0;
} /* szoplp_ */

/* Subroutine */ int szoplp_(void)
{
    return szoplp_0_(0, (real *)0, (real *)0);
    }

/* Subroutine */ int szmvlp_(real *vx, real *vy)
{
    return szoplp_0_(1, vx, vy);
    }

/* Subroutine */ int szpllp_(real *vx, real *vy)
{
    return szoplp_0_(2, vx, vy);
    }

/* Subroutine */ int szcllp_(void)
{
    return szoplp_0_(3, (real *)0, (real *)0);
    }


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Common Block Declarations */

struct {
    integer lmiss;
    real rmiss;
    integer npm;
} szbpm1_;

#define szbpm1_1 szbpm1_

struct {
    char cmark[1];
} szbpm2_;

#define szbpm2_1 szbpm2_

/* Table of constant values */

static integer c__1 = 1;
static integer c__0 = 0;

/* ----------------------------------------------------------------------- */
/*     POLYMARKER PRIMITIVE */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int szpmop_0_(int n__, integer *itype, integer *index, real *
	rsize)
{
    /* System generated locals */
    real r__1;
    char ch__1[1];

    /* Builtin functions */
    integer s_wsfi(icilist *), do_fio(integer *, char *, ftnlen), e_wsfi(void)
	    ;

    /* Local variables */
    static real pmf;
    static char cobj[80];
    extern /* Character */ VOID csgi_(char *, ftnlen, integer *);
    extern /* Subroutine */ int cdblk_(char *, ftnlen), gllget_(char *, 
	    integer *, ftnlen), sgiget_(char *, integer *, ftnlen), glrget_(
	    char *, real *, ftnlen), sgrget_(char *, real *, ftnlen), swocls_(
	    char *, ftnlen), swoopn_(char *, char *, ftnlen, ftnlen), sztxcl_(
	    void), sztxop_(real *, integer *, integer *, integer *);

    /* Fortran I/O blocks */
    static icilist io___3 = { 0, cobj, 0, "(2I8,F8.5)", 80, 1 };


    switch(n__) {
	case 1: goto L_szpmcl;
	}

    gllget_("LMISS", &szbpm1_1.lmiss, (ftnlen)5);
    glrget_("RMISS", &szbpm1_1.rmiss, (ftnlen)5);
    sgrget_("PMFACT", &pmf, (ftnlen)6);
    sgiget_("NPMSKIP", &szbpm1_1.npm, (ftnlen)7);
    csgi_(ch__1, (ftnlen)1, itype);
    *(unsigned char *)szbpm2_1.cmark = *(unsigned char *)&ch__1[0];
    s_wsfi(&io___3);
    do_fio(&c__1, (char *)&(*itype), (ftnlen)sizeof(integer));
    do_fio(&c__1, (char *)&(*index), (ftnlen)sizeof(integer));
    do_fio(&c__1, (char *)&(*rsize), (ftnlen)sizeof(real));
    e_wsfi();
    cdblk_(cobj, (ftnlen)80);
    swoopn_("SZPM", cobj, (ftnlen)4, (ftnlen)80);
    r__1 = *rsize * pmf;
    sztxop_(&r__1, &c__0, &c__0, index);
    return 0;
/* ----------------------------------------------------------------------- */

L_szpmcl:
    sztxcl_();
    swocls_("SZPM", (ftnlen)4);
    return 0;
} /* szpmop_ */

/* Subroutine */ int szpmop_(integer *itype, integer *index, real *rsize)
{
    return szpmop_0_(0, itype, index, rsize);
    }

/* Subroutine */ int szpmcl_(void)
{
    return szpmop_0_(1, (integer *)0, (integer *)0, (real *)0);
    }


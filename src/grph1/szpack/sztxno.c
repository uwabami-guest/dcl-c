/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Common Block Declarations */

struct {
    real qsize, ct, st;
    integer icentz;
} szbtx1_;

#define szbtx1_1 szbtx1_

struct {
    logical lcntl;
    integer jsup, jsub, jrst;
    real small, shift;
} szbtx2_;

#define szbtx2_1 szbtx2_

/* Table of constant values */

static integer c__92 = 92;
static integer c__120 = 120;
static integer c__32 = 32;
static integer c__180 = 180;
static integer c__182 = 182;

/* ----------------------------------------------------------------------- */
/*     CONTROL CHARACTER CONVERT TO OLD TYPE */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int sztxno_(char *chars, char *charz, logical *lcntlz, 
	ftnlen chars_len, ftnlen charz_len)
{

    /* System generated locals */
    integer i__1, i__2;
    char ch__1[1], ch__2[1];

    /* Builtin functions */
    integer s_cmp(char *, char *, ftnlen, ftnlen);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer i__, n, ncs, idx0;
    extern integer lenc_(char *, ftnlen);
    extern /* Character */ VOID csgi_(char *, ftnlen, integer *);
    extern integer isgc_(char *, ftnlen);
    static char csub[1];
    static integer isub;
    static char csup[1], crst[1];
    static integer isup, irst, nchar, imode;
    static char charx[2048];
    static logical lmark;
    static integer idest, nskip, isubz, isupz, irstz, iocmde, iocmod;
    extern /* Subroutine */ int sgiget_(char *, integer *, ftnlen), msgdmp_(
	    char *, char *, char *, ftnlen, ftnlen, ftnlen);

    *lcntlz = szbtx2_1.lcntl;
    sgiget_("ISUP", &isup, (ftnlen)4);
    sgiget_("ISUB", &isub, (ftnlen)4);
    sgiget_("IRST", &irst, (ftnlen)4);
    isupz = 29;
    isubz = 30;
    irstz = 31;
    csgi_(ch__1, (ftnlen)1, &isupz);
    *(unsigned char *)csup = *(unsigned char *)&ch__1[0];
    csgi_(ch__1, (ftnlen)1, &isubz);
    *(unsigned char *)csub = *(unsigned char *)&ch__1[0];
    csgi_(ch__1, (ftnlen)1, &irstz);
    *(unsigned char *)crst = *(unsigned char *)&ch__1[0];
/*     / Remove '\x' / */
    for (i__ = 1; i__ <= 2048; ++i__) {
	*(unsigned char *)&charx[i__ - 1] = ' ';
/* L20: */
    }
    ncs = lenc_(chars, chars_len);
    idest = 1;
    imode = 0;
    i__1 = ncs;
    for (i__ = 1; i__ <= i__1; ++i__) {
	i__2 = i__;
	csgi_(ch__1, (ftnlen)1, &c__92);
	csgi_(ch__2, (ftnlen)1, &c__120);
	if (*(unsigned char *)&chars[i__ - 1] == *(unsigned char *)&ch__1[0] 
		&& s_cmp(chars + i__2, ch__2, i__ + 1 - i__2, (ftnlen)1) == 0)
		 {
	    imode = 1;
	} else if (imode == 1) {
	    imode = 0;
	} else {
	    *(unsigned char *)&charx[idest - 1] = *(unsigned char *)&chars[
		    i__ - 1];
	    ++idest;
	}
/* L10: */
    }
/*     / CONVERT NEW TYPE MARKUPS TO OLD ONE / */
    ncs = lenc_(charx, (ftnlen)2048);
    nchar = lenc_(charz, charz_len);
    i__1 = nchar;
    for (n = 1; n <= i__1; ++n) {
	s_copy(charz, " ", charz_len, (ftnlen)1);
/* L1: */
    }
    nskip = 0;
    idest = 1;
    imode = 0;
    iocmde = 0;
    lmark = TRUE_;
    i__1 = ncs;
    for (n = 1; n <= i__1; ++n) {
	idx0 = isgc_(charx + (n - 1), (ftnlen)1);
	if (idx0 == isupz || idx0 == isubz || idx0 == irstz) {
	    csgi_(ch__1, (ftnlen)1, &c__32);
	    *(unsigned char *)&charx[n - 1] = *(unsigned char *)&ch__1[0];
	    idx0 = isgc_(charx + (n - 1), (ftnlen)1);
	}
	if (imode == 1) {
	    if (idx0 == 94) {
		*lcntlz = TRUE_;
		*(unsigned char *)&charz[idest - 1] = *(unsigned char *)csup;
		++idest;
		imode = 2;
	    } else if (idx0 == 95) {
		*lcntlz = TRUE_;
		*(unsigned char *)&charz[idest - 1] = *(unsigned char *)csub;
		++idest;
		imode = 2;
	    } else if (idx0 == 111) {
/*           180: UP ARROW */
		csgi_(ch__1, (ftnlen)1, &c__180);
		*(unsigned char *)&charz[idest - 1] = *(unsigned char *)&
			ch__1[0];
		++idest;
		msgdmp_("W", "SZTXWV", "TOPBAR IS NOT SUPPORT IN STROKEFONT", 
			(ftnlen)1, (ftnlen)6, (ftnlen)35);
		imode = 2;
		lmark = FALSE_;
	    } else if (idx0 == 117) {
/*           182: DOWN ARROW */
		csgi_(ch__1, (ftnlen)1, &c__182);
		*(unsigned char *)&charz[idest - 1] = *(unsigned char *)&
			ch__1[0];
		++idest;
		msgdmp_("W", "SZTXWV", "UNDERBAR IS NOT SUPPORT IN STROKEFONT"
			, (ftnlen)1, (ftnlen)6, (ftnlen)37);
		imode = 2;
		lmark = FALSE_;
	    } else if (idx0 == 92) {
		csgi_(ch__1, (ftnlen)1, &c__92);
		*(unsigned char *)&charz[idest - 1] = *(unsigned char *)&
			ch__1[0];
		++idest;
		imode = 0;
		*lcntlz = TRUE_;
	    } else if (idx0 == 120) {
		imode = 0;
	    } else {
		imode = 0;
		*(unsigned char *)&charz[idest - 1] = *(unsigned char *)&
			charx[n - 1];
		++idest;
		if (lmark) {
		    *(unsigned char *)&charz[idest - 1] = *(unsigned char *)
			    crst;
		    ++idest;
		} else {
		    lmark = TRUE_;
		}
	    }
	} else if (imode == 2) {
	    if (idx0 == 123) {
		imode = 3;
	    } else {
		imode = 0;
		*(unsigned char *)&charz[idest - 1] = *(unsigned char *)&
			charx[n - 1];
		++idest;
		*(unsigned char *)&charz[idest - 1] = *(unsigned char *)crst;
		++idest;
	    }
	} else if (imode == 3) {
	    if (isgc_(charx + (n - 1), (ftnlen)1) == 125) {
		imode = 0;
		if (lmark) {
		    *(unsigned char *)&charz[idest - 1] = *(unsigned char *)
			    crst;
		    ++idest;
		} else {
		    lmark = TRUE_;
		}
	    } else {
		*(unsigned char *)&charz[idest - 1] = *(unsigned char *)&
			charx[n - 1];
		++idest;
	    }
/*       92:\ 94:^ 95:_ 111:o 117:u 123:{ 125:} 120:x */
	} else if (idx0 == 92) {
	    imode = 1;
/* OLD TYPE CONTROL */
	} else if (idx0 == isup && szbtx2_1.lcntl) {
	    csgi_(ch__1, (ftnlen)1, &isup);
	    *(unsigned char *)&charz[idest - 1] = *(unsigned char *)&ch__1[0];
	    iocmde = idest;
	    ++idest;
	} else if (idx0 == isub && szbtx2_1.lcntl) {
	    csgi_(ch__1, (ftnlen)1, &isub);
	    *(unsigned char *)&charz[idest - 1] = *(unsigned char *)&ch__1[0];
	    iocmde = -idest;
	    ++idest;
	} else if (idx0 == irst && szbtx2_1.lcntl) {
	    if (iocmde > 0) {
		*(unsigned char *)&charz[iocmde - 1] = *(unsigned char *)csup;
		*(unsigned char *)&charz[idest - 1] = *(unsigned char *)crst;
		++idest;
		iocmod = 0;
	    } else if (iocmde < 0) {
		i__2 = -iocmde - 1;
		s_copy(charz + i__2, csub, -iocmde - i__2, (ftnlen)1);
		*(unsigned char *)&charz[idest - 1] = *(unsigned char *)crst;
		++idest;
		iocmod = 0;
	    } else {
		csgi_(ch__1, (ftnlen)1, &irst);
		*(unsigned char *)&charz[idest - 1] = *(unsigned char *)&
			ch__1[0];
		iocmde = 0;
		++idest;
	    }
	} else {
	    *(unsigned char *)&charz[idest - 1] = *(unsigned char *)&charx[n 
		    - 1];
	    ++idest;
	}
/* L3: */
    }
    if (idest <= ncs) {
	ncs = idest - 1;
    }
/*  / END CONVERT MARKUPS / */
    *lcntlz = TRUE_;
    return 0;
} /* sztxno_ */


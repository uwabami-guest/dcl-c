/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     TEXT WIDTH AND HEIGHT INQUIRY */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int szqtxw_(char *chars, integer *nc, real *wxch, real *wych,
	 ftnlen chars_len)
{
    /* Initialized data */

    static integer ncntz = 0;

    /* System generated locals */
    integer i__1, i__2;
    real r__1, r__2;
    char ch__1[1];

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static char fontname[70];
    static integer n;
    static real dz;
    static integer id1, id2, id3;
    static real wx1[256], wx2[256];
    static integer ich, nch, idx, ichk;
    extern integer lenc_(char *, ftnlen);
    extern /* Character */ VOID csgi_(char *, ftnlen, integer *);
    extern integer isgc_(char *, ftnlen);
    static char csub[1];
    static integer nctl, ncnt, nchz, msub, nsub;
    extern integer lenz_(char *, ftnlen);
    static char csup[1], crst[1];
    static integer msup, nsup, mrst, nchar;
    static real rfact;
    static integer index;
    static real small;
    static char charz[2048];
    static logical lcntl;
    static real shift;
    static integer ifont, isubz;
    extern /* Subroutine */ int swsfw_(integer *);
    static integer isupz, irstz;
    extern integer indxcf_(char *, integer *, integer *, char *, ftnlen, 
	    ftnlen);
    extern /* Subroutine */ int sgiget_(char *, integer *, ftnlen);
    extern integer nindxc_(char *, integer *, integer *, char *, ftnlen, 
	    ftnlen);
    extern /* Subroutine */ int sglget_(char *, logical *, ftnlen), msgdmp_(
	    char *, char *, char *, ftnlen, ftnlen, ftnlen), sgrget_(char *, 
	    real *, ftnlen), swcget_(char *, char *, ftnlen, ftnlen), swftfc_(
	    char *, ftnlen);
    static integer idxsub;
    static logical lfprop;
    static integer ndxsub;
    extern /* Subroutine */ int swlget_(char *, logical *, ftnlen);
    static logical lcntlz;
    static integer idxsup;
    extern /* Subroutine */ int szfint_(integer *);
    static integer idxrst, ndxsup;
    extern /* Subroutine */ int szqidx_(integer *), szqfnw_(real *, real *), 
	    sztxno_(char *, char *, logical *, ftnlen, ftnlen), swqtxw_(char *
	    , integer *, real *, real *, ftnlen);
    static logical lmrkfnt, lsysfnt;

/*     / GET FONT INFORMATION / */
    szfint_(&ncnt);
    if (ncntz != ncnt) {
	szqfnw_(wx1, wx2);
	ncntz = ncnt;
    }
    sztxno_(chars, charz, &lcntlz, chars_len, (ftnlen)2048);
    nchz = lenc_(charz, (ftnlen)2048);
/*     / FOR SYSTEM FONTS / */
/*     / PROCCESS IF NOT MARKER / */
    swlget_("LSYSFNT", &lsysfnt, (ftnlen)7);
    if (lsysfnt) {
	szqidx_(&index);
	swcget_("FONTNAME", fontname, (ftnlen)8, (ftnlen)70);
/*     / WHEN FONTNAME IS NOT GIVEN / */
	if (lenz_(fontname, (ftnlen)70) == 0) {
	    sglget_("LFPROP", &lfprop, (ftnlen)6);
	    if (lfprop) {
		sgiget_("IFONT", &ifont, (ftnlen)5);
		if (ifont == 1) {
		    s_copy(fontname, "Sans", (ftnlen)70, (ftnlen)4);
		} else if (ifont == 2) {
		    s_copy(fontname, "Serif", (ftnlen)70, (ftnlen)5);
		}
	    } else {
		s_copy(fontname, "Monospace", (ftnlen)70, (ftnlen)9);
	    }
	    swftfc_(fontname, (ftnlen)70);
/* Computing MAX */
	    i__2 = index % 10;
	    i__1 = max(i__2,1);
	    swsfw_(&i__1);
	} else {
	    swftfc_(fontname, (ftnlen)70);
	}
	idx = isgc_(charz, (ftnlen)1);
	sglget_("LMRKFNT", &lmrkfnt, (ftnlen)7);
	if (idx > 18 || lmrkfnt) {
	    if (idx < 18) {
/* Computing MAX */
		i__2 = index % 10;
		i__1 = max(i__2,1);
		swsfw_(&i__1);
	    }
	    swqtxw_(charz, &nchz, wxch, wych, (ftnlen)2048);
	    goto L40;
	}
    }
/*     / CHECK LCNTL OPTION / */
    sglget_("LCNTL", &lcntl, (ftnlen)5);
    lcntlz = TRUE_;
    if (lcntlz) {
/*       / CONTROL CHARACTERS ARE EFFECTIVE / */
/*        CALL SGIGET('ISUP',ISUP) */
/*        CALL SGIGET('ISUB',ISUB) */
/*        CALL SGIGET('IRST',IRST) */
	sgrget_("SMALL", &small, (ftnlen)5);
	sgrget_("SHIFT", &shift, (ftnlen)5);
	isupz = 29;
	isubz = 30;
	irstz = 31;
	csgi_(ch__1, (ftnlen)1, &isupz);
	*(unsigned char *)csup = *(unsigned char *)&ch__1[0];
	csgi_(ch__1, (ftnlen)1, &isubz);
	*(unsigned char *)csub = *(unsigned char *)&ch__1[0];
	csgi_(ch__1, (ftnlen)1, &irstz);
	*(unsigned char *)crst = *(unsigned char *)&ch__1[0];
	*nc = 0;
	nctl = 0;
	nsup = 0;
	nsub = 0;
	ichk = 0;
	msup = nindxc_(charz, &nchz, &c__1, csup, (ftnlen)2048, (ftnlen)1);
	msub = nindxc_(charz, &nchz, &c__1, csub, (ftnlen)2048, (ftnlen)1);
	mrst = nindxc_(charz, &nchz, &c__1, crst, (ftnlen)2048, (ftnlen)1);
	if (mrst != msup + msub) {
	    msgdmp_("E", "SZQTXW", "CONTROL CHARACTERS ARE NOT IN GOOD AGREE"
		    "MENT.", (ftnlen)1, (ftnlen)6, (ftnlen)45);
	}
L10:
	id1 = ichk + 1;
	id2 = nchz;
	nchar = id2 - id1 + 1;
	idxrst = indxcf_(charz + (id1 - 1), &nchar, &c__1, crst, id2 - (id1 - 
		1), (ftnlen)1);
	if (idxrst > 0) {
	    id3 = id1 + idxrst - 1;
	    ndxsup = nindxc_(charz + (id1 - 1), &idxrst, &c__1, csup, id3 - (
		    id1 - 1), (ftnlen)1);
	    ndxsub = nindxc_(charz + (id1 - 1), &idxrst, &c__1, csub, id3 - (
		    id1 - 1), (ftnlen)1);
	    if (ndxsup == 1 != (ndxsub == 1)) {
		++nctl;
		if (ndxsup == 1) {
		    idxsup = indxcf_(charz + (id1 - 1), &idxrst, &c__1, csup, 
			    id3 - (id1 - 1), (ftnlen)1);
		    nch = idxrst - idxsup - 1;
		    if (nch >= 1) {
			nsup += nch;
		    } else {
			msgdmp_("E", "SZQTXW", "NO VALID SUP CHARACTER.", (
				ftnlen)1, (ftnlen)6, (ftnlen)23);
		    }
		} else if (ndxsub == 1) {
		    idxsub = indxcf_(charz + (id1 - 1), &idxrst, &c__1, csub, 
			    id3 - (id1 - 1), (ftnlen)1);
		    nch = idxrst - idxsub - 1;
		    if (nch >= 1) {
			nsub += nch;
		    } else {
			msgdmp_("E", "SZQTXW", "NO VALID SUB CHARACTER.", (
				ftnlen)1, (ftnlen)6, (ftnlen)23);
		    }
		}
		ichk += idxrst;
	    } else {
		msgdmp_("E", "SZQTXW", "CONTROL CHARACTERS ARE NOT IN GOOD A"
			"GREEMENT.", (ftnlen)1, (ftnlen)6, (ftnlen)45);
	    }
	}
	if (! (idxrst == 0 || ichk == nchz)) {
	    goto L10;
	}
/* Computing MAX */
	r__1 = .5f, r__2 = shift + small / 2;
	dz = max(r__1,r__2) - .5f;
	rfact = 1.f;
	*wxch = 0.f;
	i__1 = nchz;
	for (n = 1; n <= i__1; ++n) {
	    ich = isgc_(charz + (n - 1), (ftnlen)1);
	    idx = ich + 1;
	    if (ich == isupz || ich == isubz) {
		rfact = small;
		goto L20;
	    } else if (ich == irstz) {
		rfact = 1.f;
		goto L20;
	    }
	    *wxch += (wx2[idx - 1] - wx1[idx - 1]) / 24.f * rfact;
L20:
	    ;
	}
	*wych = 1.f;
	if (nsup >= 1) {
	    *wych += dz;
	}
	if (nsub >= 1) {
	    *wych += dz;
	}
    } else {
/*       / CONTROL CHARACTERS ARE NOT EFFECTIVE / */
	*wxch = 0.f;
	i__1 = nchz;
	for (n = 1; n <= i__1; ++n) {
	    idx = isgc_(charz + (n - 1), (ftnlen)1) + 1;
	    *wxch += (wx2[idx - 1] - wx1[idx - 1]) / 24.f;
/* L30: */
	}
	*wych = 1.f;
    }
L40:
    return 0;
} /* szqtxw_ */


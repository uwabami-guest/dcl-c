/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int sgscwd_0_(int n__, real *cxmin, real *cxmax, real *cymin,
	 real *cymax)
{
    static real cx, cy, vx0, vy0, vxoff, vyoff, vxmin, vymin, vxmax, vymax, 
	    simfac;
    extern /* Subroutine */ int sgrget_(char *, real *, ftnlen), sgrset_(char 
	    *, real *, ftnlen), sgqvpt_(real *, real *, real *, real *);

    switch(n__) {
	case 1: goto L_sgqcwd;
	}

    sgrset_("CXMIN", cxmin, (ftnlen)5);
    sgrset_("CXMAX", cxmax, (ftnlen)5);
    sgrset_("CYMIN", cymin, (ftnlen)5);
    sgrset_("CYMAX", cymax, (ftnlen)5);
    sgqvpt_(&vxmin, &vxmax, &vymin, &vymax);
    cx = (vxmax - vxmin) / (*cxmax - *cxmin);
    cy = (vymax - vymin) / (*cymax - *cymin);
    vx0 = vxmin - cx * *cxmin;
    vy0 = vymin - cy * *cymin;
    simfac = cx;
    vxoff = vx0 - (vxmax + vxmin) / 2;
    vyoff = vy0 - (vymax + vymin) / 2;
    sgrset_("SIMFAC", &simfac, (ftnlen)6);
    sgrset_("VXOFF", &vxoff, (ftnlen)5);
    sgrset_("VYOFF", &vyoff, (ftnlen)5);
    return 0;
/* ----------------------------------------------------------------------- */

L_sgqcwd:
    sgrget_("CXMIN", cxmin, (ftnlen)5);
    sgrget_("CXMAX", cxmax, (ftnlen)5);
    sgrget_("CYMIN", cymin, (ftnlen)5);
    sgrget_("CYMAX", cymax, (ftnlen)5);
    return 0;
} /* sgscwd_ */

/* Subroutine */ int sgscwd_(real *cxmin, real *cxmax, real *cymin, real *
	cymax)
{
    return sgscwd_0_(0, cxmin, cxmax, cymin, cymax);
    }

/* Subroutine */ int sgqcwd_(real *cxmin, real *cxmax, real *cymin, real *
	cymax)
{
    return sgscwd_0_(1, cxmin, cxmax, cymin, cymax);
    }


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     SGIGET / SGISET / SGISTX */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int sgiget_0_(int n__, char *cp, integer *ipara, ftnlen 
	cp_len)
{
    static char cl[40];
    static integer ip;
    static char cx[8];
    static integer idx;
    extern /* Subroutine */ int sgiqid_(char *, integer *, ftnlen), sgiqcl_(
	    integer *, char *, ftnlen), sgiqcp_(integer *, char *, ftnlen), 
	    rliget_(char *, integer *, integer *, ftnlen), rtiget_(char *, 
	    char *, integer *, integer *, ftnlen, ftnlen), sgiqvl_(integer *, 
	    integer *), sgisvl_(integer *, integer *);

    switch(n__) {
	case 1: goto L_sgiset;
	case 2: goto L_sgistx;
	}

    sgiqid_(cp, &idx, cp_len);
    sgiqvl_(&idx, ipara);
    return 0;
/* ----------------------------------------------------------------------- */

L_sgiset:
    sgiqid_(cp, &idx, cp_len);
    sgisvl_(&idx, ipara);
    return 0;
/* ----------------------------------------------------------------------- */

L_sgistx:
    ip = *ipara;
    sgiqid_(cp, &idx, cp_len);
/*     / SHORT NAME / */
    sgiqcp_(&idx, cx, (ftnlen)8);
    rtiget_("SG", cx, &ip, &c__1, (ftnlen)2, (ftnlen)8);
/*     / LONG NAME / */
    sgiqcl_(&idx, cl, (ftnlen)40);
    rliget_(cl, &ip, &c__1, (ftnlen)40);
    sgisvl_(&idx, &ip);
    return 0;
} /* sgiget_ */

/* Subroutine */ int sgiget_(char *cp, integer *ipara, ftnlen cp_len)
{
    return sgiget_0_(0, cp, ipara, cp_len);
    }

/* Subroutine */ int sgiset_(char *cp, integer *ipara, ftnlen cp_len)
{
    return sgiget_0_(1, cp, ipara, cp_len);
    }

/* Subroutine */ int sgistx_(char *cp, integer *ipara, ftnlen cp_len)
{
    return sgiget_0_(2, cp, ipara, cp_len);
    }


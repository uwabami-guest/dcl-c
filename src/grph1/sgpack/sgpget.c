/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     SGPGET / SGPSET / SGPSTX */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int sgpget_0_(int n__, char *cp, integer *ipara, ftnlen 
	cp_len)
{
    static char cl[40];
    static integer ip;
    static char cx[8];
    static integer it, idx;
    extern /* Subroutine */ int sgiqid_(char *, integer *, ftnlen), sglqid_(
	    char *, integer *, ftnlen), rliget_(char *, integer *, integer *, 
	    ftnlen), sgpqid_(char *, integer *, ftnlen), sgpqcl_(integer *, 
	    char *, ftnlen), rllget_(char *, integer *, integer *, ftnlen), 
	    sgrqid_(char *, integer *, ftnlen), sgpqcp_(integer *, char *, 
	    ftnlen), rtiget_(char *, char *, integer *, integer *, ftnlen, 
	    ftnlen), rlrget_(char *, integer *, integer *, ftnlen), rtlget_(
	    char *, char *, integer *, integer *, ftnlen, ftnlen), sgpqit_(
	    integer *, integer *), sgisvl_(integer *, integer *), rtrget_(
	    char *, char *, integer *, integer *, ftnlen, ftnlen), sglsvl_(
	    integer *, integer *), sgpqvl_(integer *, integer *), sgpsvl_(
	    integer *, integer *), sgrsvl_(integer *, integer *);

    switch(n__) {
	case 1: goto L_sgpset;
	case 2: goto L_sgpstx;
	}

    sgpqid_(cp, &idx, cp_len);
    sgpqvl_(&idx, ipara);
    return 0;
/* ----------------------------------------------------------------------- */

L_sgpset:
    sgpqid_(cp, &idx, cp_len);
    sgpsvl_(&idx, ipara);
    return 0;
/* ----------------------------------------------------------------------- */

L_sgpstx:
    ip = *ipara;
    sgpqid_(cp, &idx, cp_len);
    sgpqit_(&idx, &it);
    sgpqcp_(&idx, cx, (ftnlen)8);
    sgpqcl_(&idx, cl, (ftnlen)40);
    if (it == 1) {
	rtiget_("SG", cx, &ip, &c__1, (ftnlen)2, (ftnlen)8);
	rliget_(cl, &ip, &c__1, (ftnlen)40);
	sgiqid_(cp, &idx, cp_len);
	sgisvl_(&idx, &ip);
    } else if (it == 2) {
	rtlget_("SG", cx, &ip, &c__1, (ftnlen)2, (ftnlen)8);
	rllget_(cl, &ip, &c__1, (ftnlen)40);
	sglqid_(cp, &idx, cp_len);
	sglsvl_(&idx, &ip);
    } else if (it == 3) {
	rtrget_("SG", cx, &ip, &c__1, (ftnlen)2, (ftnlen)8);
	rlrget_(cl, &ip, &c__1, (ftnlen)40);
	sgrqid_(cp, &idx, cp_len);
	sgrsvl_(&idx, &ip);
    }
    return 0;
} /* sgpget_ */

/* Subroutine */ int sgpget_(char *cp, integer *ipara, ftnlen cp_len)
{
    return sgpget_0_(0, cp, ipara, cp_len);
    }

/* Subroutine */ int sgpset_(char *cp, integer *ipara, ftnlen cp_len)
{
    return sgpget_0_(1, cp, ipara, cp_len);
    }

/* Subroutine */ int sgpstx_(char *cp, integer *ipara, ftnlen cp_len)
{
    return sgpget_0_(2, cp, ipara, cp_len);
    }


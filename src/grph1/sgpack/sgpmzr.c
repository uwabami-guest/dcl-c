/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int sgpmzr_(integer *n, real *rpx, real *rpy, integer *itype,
	 integer *index, real *rsize)
{
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), szpmcl_(void), szpmop_(integer *, integer *, 
	    real *), szpmzr_(integer *, real *, real *);

    /* Parameter adjustments */
    --rpy;
    --rpx;

    /* Function Body */
    if (*n < 1) {
	msgdmp_("E", "SGPMZR", "NUMBER OF POINTS IS LESS THAN 1.", (ftnlen)1, 
		(ftnlen)6, (ftnlen)32);
    }
    if (*itype == 0) {
	msgdmp_("M", "SGPMZR", "MARKER TYPE IS 0 / DO NOTHING.", (ftnlen)1, (
		ftnlen)6, (ftnlen)30);
	return 0;
    }
    if (*index == 0) {
	msgdmp_("M", "SGPMZR", "POLYMARKER INDEX IS 0 / DO NOTHING.", (ftnlen)
		1, (ftnlen)6, (ftnlen)35);
	return 0;
    }
    if (*index < 0) {
	msgdmp_("E", "SGPMZR", "POLYMARKER INDEX IS LESS THAN 0.", (ftnlen)1, 
		(ftnlen)6, (ftnlen)32);
    }
    if (*rsize == 0.f) {
	msgdmp_("M", "SGPMZR", "MARKER SIZE IS 0 / DO NOTHING.", (ftnlen)1, (
		ftnlen)6, (ftnlen)30);
	return 0;
    }
    if (*rsize < 0.f) {
	msgdmp_("E", "SGPMZR", "MARKER SIZE IS LESS THAN ZERO.", (ftnlen)1, (
		ftnlen)6, (ftnlen)30);
    }
    szpmop_(itype, index, rsize);
    szpmzr_(n, &rpx[1], &rpy[1]);
    szpmcl_();
    return 0;
} /* sgpmzr_ */


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static logical c_true = TRUE_;
static logical c_false = FALSE_;

/* ----------------------------------------------------------------------- */
/* Subroutine */ int sgplxr_(integer *n, real *rpx, real *rpy, integer *itype,
	 integer *index, integer *icolor)
{
    static logical lfcatr;
    extern /* Subroutine */ int swqfcc_(logical *), msgdmp_(char *, char *, 
	    char *, ftnlen, ftnlen, ftnlen), swsfcm_(logical *), swslcl_(
	    integer *), szplcl_(void), szplop_(integer *, integer *), szplzr_(
	    integer *, real *, real *);

    /* Parameter adjustments */
    --rpy;
    --rpx;

    /* Function Body */
    swqfcc_(&lfcatr);
    if (! lfcatr) {
	msgdmp_("M", "SGPLXR", "FULL COLOR MODE IS NOT SUPPORTED.", (ftnlen)1,
		 (ftnlen)6, (ftnlen)33);
    }
    if (*n < 2) {
	msgdmp_("E", "SGPLXR", "NUMBER OF POINTS IS LESS THAN 2.", (ftnlen)1, 
		(ftnlen)6, (ftnlen)32);
    }
    if (*itype == 0) {
	msgdmp_("M", "SGPLXR", "LINETYPE IS 0 / DO NOTHING.", (ftnlen)1, (
		ftnlen)6, (ftnlen)27);
	return 0;
    }
    if (*index == 0) {
	msgdmp_("M", "SGPLXR", "POLYLINE INDEX IS 0 / DO NOTHING.", (ftnlen)1,
		 (ftnlen)6, (ftnlen)33);
	return 0;
    }
    if (*index < 0) {
	msgdmp_("E", "SGPLXR", "POLYLINE INDEX IS LESS THAN 0.", (ftnlen)1, (
		ftnlen)6, (ftnlen)30);
    }
    swsfcm_(&c_true);
    swslcl_(icolor);
    szplop_(itype, index);
    szplzr_(n, &rpx[1], &rpy[1]);
    szplcl_();
    swsfcm_(&c_false);
    return 0;
} /* sgplxr_ */


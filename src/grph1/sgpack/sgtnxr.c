/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static logical c_true = TRUE_;
static logical c_false = FALSE_;

/* ----------------------------------------------------------------------- */
/* Subroutine */ int sgtnxr_(integer *n, real *rpx, real *rpy, integer *itpat,
	 integer *icolor)
{
    static integer ifcidx;
    static logical lfcatr;
    extern /* Subroutine */ int sgiget_(char *, integer *, ftnlen), swqfcc_(
	    logical *), msgdmp_(char *, char *, char *, ftnlen, ftnlen, 
	    ftnlen), swsfcm_(logical *), swslcl_(integer *);
    static integer itpatx;
    extern /* Subroutine */ int sztncl_(void), swstcl_(integer *), sztnop_(
	    integer *), sztnzr_(integer *, real *, real *);

    /* Parameter adjustments */
    --rpy;
    --rpx;

    /* Function Body */
    sgiget_("IFCIDX", &ifcidx, (ftnlen)6);
    swqfcc_(&lfcatr);
    if (! lfcatr) {
	msgdmp_("M", "SGTNXR", "FULL COLOR MODE IS NOT SUPPORTED.", (ftnlen)1,
		 (ftnlen)6, (ftnlen)33);
    }
    if (*n < 3) {
	msgdmp_("E", "SGTNXR", "NUMBER OF POINTS IS LESS THAN 3.", (ftnlen)1, 
		(ftnlen)6, (ftnlen)32);
    }
    if (*itpat == 0) {
	msgdmp_("M", "SGTNXR", "TONE PAT. INDEX IS 0 / DO NOTHING.", (ftnlen)
		1, (ftnlen)6, (ftnlen)34);
	return 0;
    }
    if (*itpat < 0) {
	msgdmp_("E", "SGTNXR", "TONE PAT. INDEX IS LESS THAN 0.", (ftnlen)1, (
		ftnlen)6, (ftnlen)31);
    }
    swsfcm_(&c_true);
    swstcl_(icolor);
    swslcl_(icolor);
    itpatx = ifcidx * 1000 + *itpat % 1000;
    sztnop_(&itpatx);
    sztnzr_(n, &rpx[1], &rpy[1]);
    sztncl_();
    swsfcm_(&c_false);
    return 0;
} /* sgtnxr_ */


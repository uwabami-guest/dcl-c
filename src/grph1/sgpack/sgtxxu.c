/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static logical c_true = TRUE_;
static logical c_false = FALSE_;

/* ----------------------------------------------------------------------- */
/* Subroutine */ int sgtxxu_(real *ux, real *uy, char *chars, real *rsize, 
	integer *irota, integer *icent, integer *index, integer *icolor, 
	ftnlen chars_len)
{
    static logical lfcatr;
    extern /* Subroutine */ int swqfcc_(logical *), msgdmp_(char *, char *, 
	    char *, ftnlen, ftnlen, ftnlen), swsfcm_(logical *), swslcl_(
	    integer *), sztxcl_(void), sztxop_(real *, integer *, integer *, 
	    integer *), sztxzu_(real *, real *, char *, ftnlen);

    swqfcc_(&lfcatr);
    if (! lfcatr) {
	msgdmp_("M", "SGTXXU", "FULL COLOR MODE IS NOT SUPPORTED.", (ftnlen)1,
		 (ftnlen)6, (ftnlen)33);
    }
    if (*rsize == 0.f) {
	msgdmp_("M", "SGTXXU", "TEXT HEIGHT IS 0 / DO NOTHING.", (ftnlen)1, (
		ftnlen)6, (ftnlen)30);
	return 0;
    }
    if (*rsize < 0.f) {
	msgdmp_("E", "SGTXXU", "TEXT HEIGHT IS LESS THAN ZERO.", (ftnlen)1, (
		ftnlen)6, (ftnlen)30);
    }
    if (! (-1 <= *icent && *icent <= 1)) {
	msgdmp_("E", "SGTXXU", "CENTERING OPTION IS INVALID.", (ftnlen)1, (
		ftnlen)6, (ftnlen)28);
    }
    if (*index == 0) {
	msgdmp_("M", "SGTXXU", "TEXT INDEX IS 0 / DO NOTHING.", (ftnlen)1, (
		ftnlen)6, (ftnlen)29);
	return 0;
    }
    if (*index < 0) {
	msgdmp_("E", "SGTXXU", "TEXT INDEX IS LESS THAN 0.", (ftnlen)1, (
		ftnlen)6, (ftnlen)26);
    }
    swsfcm_(&c_true);
    swslcl_(icolor);
    sztxop_(rsize, irota, icent, index);
    sztxzu_(ux, uy, chars, chars_len);
    sztxcl_();
    swsfcm_(&c_false);
    return 0;
} /* sgtxxu_ */


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     TEXT PRIMITIVE */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int sgtxu_0_(int n__, real *ux, real *uy, char *chars, real *
	vx, real *vy, real *rx, real *ry, real *rsize, integer *irota, 
	integer *icent, integer *index, ftnlen chars_len)
{
    /* Initialized data */

    static real rsizez = .05f;
    static integer irotaz = 0;
    static integer icentz = 0;
    static integer indexz = 1;

    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), sztxcl_(void), sztxop_(real *, integer *, 
	    integer *, integer *), sztxzr_(real *, real *, char *, ftnlen), 
	    sztxzu_(real *, real *, char *, ftnlen), sztxzv_(real *, real *, 
	    char *, ftnlen);

    switch(n__) {
	case 1: goto L_sgtxv;
	case 2: goto L_sgtxr;
	case 3: goto L_sgstxs;
	case 4: goto L_sgqtxs;
	case 5: goto L_sgstxr;
	case 6: goto L_sgqtxr;
	case 7: goto L_sgstxc;
	case 8: goto L_sgqtxc;
	case 9: goto L_sgstxi;
	case 10: goto L_sgqtxi;
	}

    if (rsizez == 0.f) {
	msgdmp_("M", "SGTXU", "TEXT HEIGHT IS 0 / DO NOTHING.", (ftnlen)1, (
		ftnlen)5, (ftnlen)30);
	return 0;
    }
    if (rsizez < 0.f) {
	msgdmp_("E", "SGTXU", "TEXT HEIGHT IS LESS THAN ZERO.", (ftnlen)1, (
		ftnlen)5, (ftnlen)30);
    }
    if (! (-1 <= icentz && icentz <= 1)) {
	msgdmp_("E", "SGTXU", "CENTERING OPTION IS INVALID.", (ftnlen)1, (
		ftnlen)5, (ftnlen)28);
    }
    if (indexz == 0) {
	msgdmp_("M", "SGTXU", "TEXT INDEX IS 0 / DO NOTHING.", (ftnlen)1, (
		ftnlen)5, (ftnlen)29);
	return 0;
    }
    if (indexz < 0) {
	msgdmp_("E", "SGTXU", "TEXT INDEX IS LESS THAN 0.", (ftnlen)1, (
		ftnlen)5, (ftnlen)26);
    }
    sztxop_(&rsizez, &irotaz, &icentz, &indexz);
    sztxzu_(ux, uy, chars, chars_len);
    sztxcl_();
    return 0;
/* ----------------------------------------------------------------------- */

L_sgtxv:
    if (rsizez == 0.f) {
	msgdmp_("M", "SGTXV", "TEXT HEIGHT IS 0 / DO NOTHING.", (ftnlen)1, (
		ftnlen)5, (ftnlen)30);
	return 0;
    }
    if (rsizez < 0.f) {
	msgdmp_("E", "SGTXV", "TEXT HEIGHT IS LESS THAN ZERO.", (ftnlen)1, (
		ftnlen)5, (ftnlen)30);
    }
    if (! (-1 <= icentz && icentz <= 1)) {
	msgdmp_("E", "SGTXV", "CENTERING OPTION IS INVALID.", (ftnlen)1, (
		ftnlen)5, (ftnlen)28);
    }
    if (indexz == 0) {
	msgdmp_("M", "SGTXV", "TEXT INDEX IS 0 / DO NOTHING.", (ftnlen)1, (
		ftnlen)5, (ftnlen)29);
	return 0;
    }
    if (indexz < 0) {
	msgdmp_("E", "SGTXV", "TEXT INDEX IS LESS THAN 0.", (ftnlen)1, (
		ftnlen)5, (ftnlen)26);
    }
    sztxop_(&rsizez, &irotaz, &icentz, &indexz);
    sztxzv_(vx, vy, chars, chars_len);
    sztxcl_();
    return 0;
/* ----------------------------------------------------------------------- */

L_sgtxr:
    if (rsizez == 0.f) {
	msgdmp_("M", "SGTXR", "TEXT HEIGHT IS 0 / DO NOTHING.", (ftnlen)1, (
		ftnlen)5, (ftnlen)30);
	return 0;
    }
    if (rsizez < 0.f) {
	msgdmp_("E", "SGTXR", "TEXT HEIGHT IS LESS THAN ZERO.", (ftnlen)1, (
		ftnlen)5, (ftnlen)30);
    }
    if (! (-1 <= icentz && icentz <= 1)) {
	msgdmp_("E", "SGTXR", "CENTERING OPTION IS INVALID.", (ftnlen)1, (
		ftnlen)5, (ftnlen)28);
    }
    if (indexz == 0) {
	msgdmp_("M", "SGTXR", "TEXT INDEX IS 0 / DO NOTHING.", (ftnlen)1, (
		ftnlen)5, (ftnlen)29);
	return 0;
    }
    if (indexz < 0) {
	msgdmp_("E", "SGTXR", "TEXT INDEX IS LESS THAN 0.", (ftnlen)1, (
		ftnlen)5, (ftnlen)26);
    }
    sztxop_(&rsizez, &irotaz, &icentz, &indexz);
    sztxzr_(rx, ry, chars, chars_len);
    sztxcl_();
    return 0;
/* ----------------------------------------------------------------------- */

L_sgstxs:
    rsizez = *rsize;
    return 0;
/* ----------------------------------------------------------------------- */

L_sgqtxs:
    *rsize = rsizez;
    return 0;
/* ----------------------------------------------------------------------- */

L_sgstxr:
    irotaz = *irota;
    return 0;
/* ----------------------------------------------------------------------- */

L_sgqtxr:
    *irota = irotaz;
    return 0;
/* ----------------------------------------------------------------------- */

L_sgstxc:
    icentz = *icent;
    return 0;
/* ----------------------------------------------------------------------- */

L_sgqtxc:
    *icent = icentz;
    return 0;
/* ----------------------------------------------------------------------- */

L_sgstxi:
    indexz = *index;
    return 0;
/* ----------------------------------------------------------------------- */

L_sgqtxi:
    *index = indexz;
    return 0;
} /* sgtxu_ */

/* Subroutine */ int sgtxu_(real *ux, real *uy, char *chars, ftnlen chars_len)
{
    return sgtxu_0_(0, ux, uy, chars, (real *)0, (real *)0, (real *)0, (real *
	    )0, (real *)0, (integer *)0, (integer *)0, (integer *)0, 
	    chars_len);
    }

/* Subroutine */ int sgtxv_(real *vx, real *vy, char *chars, ftnlen chars_len)
{
    return sgtxu_0_(1, (real *)0, (real *)0, chars, vx, vy, (real *)0, (real *
	    )0, (real *)0, (integer *)0, (integer *)0, (integer *)0, 
	    chars_len);
    }

/* Subroutine */ int sgtxr_(real *rx, real *ry, char *chars, ftnlen chars_len)
{
    return sgtxu_0_(2, (real *)0, (real *)0, chars, (real *)0, (real *)0, rx, 
	    ry, (real *)0, (integer *)0, (integer *)0, (integer *)0, 
	    chars_len);
    }

/* Subroutine */ int sgstxs_(real *rsize)
{
    return sgtxu_0_(3, (real *)0, (real *)0, (char *)0, (real *)0, (real *)0, 
	    (real *)0, (real *)0, rsize, (integer *)0, (integer *)0, (integer 
	    *)0, (ftnint)0);
    }

/* Subroutine */ int sgqtxs_(real *rsize)
{
    return sgtxu_0_(4, (real *)0, (real *)0, (char *)0, (real *)0, (real *)0, 
	    (real *)0, (real *)0, rsize, (integer *)0, (integer *)0, (integer 
	    *)0, (ftnint)0);
    }

/* Subroutine */ int sgstxr_(integer *irota)
{
    return sgtxu_0_(5, (real *)0, (real *)0, (char *)0, (real *)0, (real *)0, 
	    (real *)0, (real *)0, (real *)0, irota, (integer *)0, (integer *)
	    0, (ftnint)0);
    }

/* Subroutine */ int sgqtxr_(integer *irota)
{
    return sgtxu_0_(6, (real *)0, (real *)0, (char *)0, (real *)0, (real *)0, 
	    (real *)0, (real *)0, (real *)0, irota, (integer *)0, (integer *)
	    0, (ftnint)0);
    }

/* Subroutine */ int sgstxc_(integer *icent)
{
    return sgtxu_0_(7, (real *)0, (real *)0, (char *)0, (real *)0, (real *)0, 
	    (real *)0, (real *)0, (real *)0, (integer *)0, icent, (integer *)
	    0, (ftnint)0);
    }

/* Subroutine */ int sgqtxc_(integer *icent)
{
    return sgtxu_0_(8, (real *)0, (real *)0, (char *)0, (real *)0, (real *)0, 
	    (real *)0, (real *)0, (real *)0, (integer *)0, icent, (integer *)
	    0, (ftnint)0);
    }

/* Subroutine */ int sgstxi_(integer *index)
{
    return sgtxu_0_(9, (real *)0, (real *)0, (char *)0, (real *)0, (real *)0, 
	    (real *)0, (real *)0, (real *)0, (integer *)0, (integer *)0, 
	    index, (ftnint)0);
    }

/* Subroutine */ int sgqtxi_(integer *index)
{
    return sgtxu_0_(10, (real *)0, (real *)0, (char *)0, (real *)0, (real *)0,
	     (real *)0, (real *)0, (real *)0, (integer *)0, (integer *)0, 
	    index, (ftnint)0);
    }


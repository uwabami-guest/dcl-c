/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     ARROW SUBPRIMITIVE */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int sglau_0_(int n__, real *ux1, real *uy1, real *ux2, real *
	uy2, real *vx1, real *vy1, real *vx2, real *vy2, real *rx1, real *ry1,
	 real *rx2, real *ry2, integer *itype, integer *index)
{
    /* Initialized data */

    static integer itypez = 1;
    static integer indexz = 1;

    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), szlacl_(void), szlaop_(integer *, integer *), 
	    szlazr_(real *, real *, real *, real *), szlazu_(real *, real *, 
	    real *, real *), szlazv_(real *, real *, real *, real *);

    switch(n__) {
	case 1: goto L_sglav;
	case 2: goto L_sglar;
	case 3: goto L_sgslat;
	case 4: goto L_sgqlat;
	case 5: goto L_sgslai;
	case 6: goto L_sgqlai;
	}

    if (itypez == 0) {
	msgdmp_("M", "SGLAU", "LINETYPE IS 0 / DO NOTHING.", (ftnlen)1, (
		ftnlen)5, (ftnlen)27);
	return 0;
    }
    if (indexz == 0) {
	msgdmp_("M", "SGLAU", "LINE INDEX IS 0 / DO NOTHING.", (ftnlen)1, (
		ftnlen)5, (ftnlen)29);
	return 0;
    }
    if (indexz < 0) {
	msgdmp_("E", "SGLAU", "LINE INDEX IS INVALID.", (ftnlen)1, (ftnlen)5, 
		(ftnlen)22);
    }
    szlaop_(&itypez, &indexz);
    szlazu_(ux1, uy1, ux2, uy2);
    szlacl_();
    return 0;
/* ----------------------------------------------------------------------- */

L_sglav:
    if (itypez == 0) {
	msgdmp_("M", "SGLAV", "LINETYPE IS 0 / DO NOTHING.", (ftnlen)1, (
		ftnlen)5, (ftnlen)27);
	return 0;
    }
    if (indexz == 0) {
	msgdmp_("M", "SGLAV", "LINE INDEX IS 0 / DO NOTHING.", (ftnlen)1, (
		ftnlen)5, (ftnlen)29);
	return 0;
    }
    if (indexz < 0) {
	msgdmp_("E", "SGLAV", "LINE INDEX IS INVALID.", (ftnlen)1, (ftnlen)5, 
		(ftnlen)22);
    }
    szlaop_(&itypez, &indexz);
    szlazv_(vx1, vy1, vx2, vy2);
    szlacl_();
    return 0;
/* ----------------------------------------------------------------------- */

L_sglar:
    if (itypez == 0) {
	msgdmp_("M", "SGLAR", "LINETYPE IS 0 / DO NOTHING.", (ftnlen)1, (
		ftnlen)5, (ftnlen)27);
	return 0;
    }
    if (indexz == 0) {
	msgdmp_("M", "SGLAR", "LINE INDEX IS 0 / DO NOTHING.", (ftnlen)1, (
		ftnlen)5, (ftnlen)29);
	return 0;
    }
    if (indexz < 0) {
	msgdmp_("E", "SGLAR", "LINE INDEX IS INVALID.", (ftnlen)1, (ftnlen)5, 
		(ftnlen)22);
    }
    szlaop_(&itypez, &indexz);
    szlazr_(rx1, ry1, rx2, ry2);
    szlacl_();
    return 0;
/* ----------------------------------------------------------------------- */

L_sgslat:
    itypez = *itype;
    return 0;
/* ----------------------------------------------------------------------- */

L_sgqlat:
    *itype = itypez;
    return 0;
/* ----------------------------------------------------------------------- */

L_sgslai:
    indexz = *index;
    return 0;
/* ----------------------------------------------------------------------- */

L_sgqlai:
    *index = indexz;
    return 0;
} /* sglau_ */

/* Subroutine */ int sglau_(real *ux1, real *uy1, real *ux2, real *uy2)
{
    return sglau_0_(0, ux1, uy1, ux2, uy2, (real *)0, (real *)0, (real *)0, (
	    real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (integer *)
	    0, (integer *)0);
    }

/* Subroutine */ int sglav_(real *vx1, real *vy1, real *vx2, real *vy2)
{
    return sglau_0_(1, (real *)0, (real *)0, (real *)0, (real *)0, vx1, vy1, 
	    vx2, vy2, (real *)0, (real *)0, (real *)0, (real *)0, (integer *)
	    0, (integer *)0);
    }

/* Subroutine */ int sglar_(real *rx1, real *ry1, real *rx2, real *ry2)
{
    return sglau_0_(2, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, 
	    (real *)0, (real *)0, (real *)0, rx1, ry1, rx2, ry2, (integer *)0,
	     (integer *)0);
    }

/* Subroutine */ int sgslat_(integer *itype)
{
    return sglau_0_(3, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, 
	    (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, 
	    (real *)0, itype, (integer *)0);
    }

/* Subroutine */ int sgqlat_(integer *itype)
{
    return sglau_0_(4, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, 
	    (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, 
	    (real *)0, itype, (integer *)0);
    }

/* Subroutine */ int sgslai_(integer *index)
{
    return sglau_0_(5, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, 
	    (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, 
	    (real *)0, (integer *)0, index);
    }

/* Subroutine */ int sgqlai_(integer *index)
{
    return sglau_0_(6, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, 
	    (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, 
	    (real *)0, (integer *)0, index);
    }


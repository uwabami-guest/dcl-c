/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__3 = 3;
static integer c__26 = 26;

/* ----------------------------------------------------------------------- */
/*     LOGICAL PARAMETER CONTROL */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int sglqnp_0_(int n__, integer *ncp, char *cp, integer *idx, 
	logical *lpara, integer *in, ftnlen cp_len)
{
    /* Initialized data */

    static char cparas[8*26] = "LCLIP   " "LCHAR   " "LLNINT  " "LGCINT  " 
	    "LSOFTF  " "LCL2TN  " "LCNTL   " "LFULL   " "LFPROP  " "LROT    " 
	    "LBUFF   " "LCURV   " "LPROP   " "LUARW   " "LARROW  " "LATONE  " 
	    "LDEG    " "LCORNER " "LTITLE  " "LWIDE   " "LFGBG   " "L2TO3   " 
	    "LXLOG3  " "LYLOG3  " "LZLOG3  " "LMRKFNT ";
    static logical lx[26] = { FALSE_,FALSE_,FALSE_,TRUE_,TRUE_,FALSE_,FALSE_,
	    FALSE_,TRUE_,FALSE_,TRUE_,TRUE_,TRUE_,TRUE_,FALSE_,FALSE_,TRUE_,
	    FALSE_,TRUE_,FALSE_,FALSE_,FALSE_,FALSE_,FALSE_,FALSE_,TRUE_ };
    static char cparal[40*26] = "ENABLE_CLIPPING                         " 
	    "ENABLE_LINE_LABELING                    " "ENABLE_LINEAR_INTERP"
	    "OLATION             " "ENABLE_GREATCIRCLE_INTERPOLATION        " 
	    "ENABLE_SOFTFILL                         " "ENABLE_COLOR_SUBSTIT"
	    "UTION               " "ENABLE_CONTROL_CHAR                     " 
	    "USE_FULL_WINDOW                         " "ENABLE_PROPORTIONAL_"
	    "FONT                " "ENABLE_LINE_LABEL_ROTATION              " 
	    "ENABLE_LINE_BUFFERING                   " "DISABLE_LINE_LABEL_A"
	    "T_CURVE             " "FIX_ARROWHEAD_PROPORTION                " 
	    "ENABLE_ARROWHEAD_MAPPING                " "----LARROW          "
	    "                    " "ENABLE_ARROWHEAD_SHADE                  " 
	    "USE_DEGREE                              " "DRAW_CORNERMARK     "
	    "                    " "DRAW_PAGE_TITLE                         " 
	    "****LWIDE                               " "****LFGBG           "
	    "                    " "****L2TO3                               " 
	    "++++LXLOG3                              " "++++LYLOG3          "
	    "                    " "++++LZLOG3                              " 
	    "USE_DCLMARKER_FONT                      ";
    static logical lfirst = TRUE_;

    /* System generated locals */
    address a__1[3];
    integer i__1[3];

    /* Builtin functions */
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen),
	     s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer n;
    extern integer lenc_(char *, ftnlen);
    static char cmsg[80];
    extern logical lchreq_(char *, char *, ftnlen, ftnlen);
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), rllget_(char *, logical *, integer *, ftnlen), 
	    rtlget_(char *, char *, logical *, integer *, ftnlen, ftnlen);

/*     / SHORT NAME / */
    switch(n__) {
	case 1: goto L_sglqid;
	case 2: goto L_sglqcp;
	case 3: goto L_sglqcl;
	case 4: goto L_sglqvl;
	case 5: goto L_sglsvl;
	case 6: goto L_sglqin;
	}

/*     / LONG  NAME / */
    *ncp = 26;
    return 0;
/* ----------------------------------------------------------------------- */

L_sglqid:
    for (n = 1; n <= 26; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *idx = n;
	    return 0;
	}
/* L10: */
    }
/* Writing concatenation */
    i__1[0] = 11, a__1[0] = "PARAMETER '";
    i__1[1] = lenc_(cp, cp_len), a__1[1] = cp;
    i__1[2] = 17, a__1[2] = "' IS NOT DEFINED.";
    s_cat(cmsg, a__1, i__1, &c__3, (ftnlen)80);
    msgdmp_("E", "SGLQID", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
    return 0;
/* ----------------------------------------------------------------------- */

L_sglqcp:
    if (1 <= *idx && *idx <= 26) {
	s_copy(cp, cparas + (*idx - 1 << 3), cp_len, (ftnlen)8);
    } else {
	msgdmp_("E", "SGLQCP", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_sglqcl:
    if (1 <= *idx && *idx <= 26) {
	s_copy(cp, cparal + (*idx - 1) * 40, cp_len, (ftnlen)40);
    } else {
	msgdmp_("E", "SGLQCL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_sglqvl:
    if (lfirst) {
	rtlget_("SG", cparas, lx, &c__26, (ftnlen)2, (ftnlen)8);
	rllget_(cparal, lx, &c__26, (ftnlen)40);
	lfirst = FALSE_;
    }
    if (1 <= *idx && *idx <= 26) {
	*lpara = lx[*idx - 1];
    } else {
	msgdmp_("E", "SGLQVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_sglsvl:
    if (lfirst) {
	rtlget_("SG", cparas, lx, &c__26, (ftnlen)2, (ftnlen)8);
	rllget_(cparal, lx, &c__26, (ftnlen)40);
	lfirst = FALSE_;
    }
    if (1 <= *idx && *idx <= 26) {
	lx[*idx - 1] = *lpara;
    } else {
	msgdmp_("E", "SGLSVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_sglqin:
    for (n = 1; n <= 26; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *in = n;
	    return 0;
	}
/* L20: */
    }
    *in = 0;
    return 0;
} /* sglqnp_ */

/* Subroutine */ int sglqnp_(integer *ncp)
{
    return sglqnp_0_(0, ncp, (char *)0, (integer *)0, (logical *)0, (integer *
	    )0, (ftnint)0);
    }

/* Subroutine */ int sglqid_(char *cp, integer *idx, ftnlen cp_len)
{
    return sglqnp_0_(1, (integer *)0, cp, idx, (logical *)0, (integer *)0, 
	    cp_len);
    }

/* Subroutine */ int sglqcp_(integer *idx, char *cp, ftnlen cp_len)
{
    return sglqnp_0_(2, (integer *)0, cp, idx, (logical *)0, (integer *)0, 
	    cp_len);
    }

/* Subroutine */ int sglqcl_(integer *idx, char *cp, ftnlen cp_len)
{
    return sglqnp_0_(3, (integer *)0, cp, idx, (logical *)0, (integer *)0, 
	    cp_len);
    }

/* Subroutine */ int sglqvl_(integer *idx, logical *lpara)
{
    return sglqnp_0_(4, (integer *)0, (char *)0, idx, lpara, (integer *)0, (
	    ftnint)0);
    }

/* Subroutine */ int sglsvl_(integer *idx, logical *lpara)
{
    return sglqnp_0_(5, (integer *)0, (char *)0, idx, lpara, (integer *)0, (
	    ftnint)0);
    }

/* Subroutine */ int sglqin_(char *cp, integer *in, ftnlen cp_len)
{
    return sglqnp_0_(6, (integer *)0, cp, (integer *)0, (logical *)0, in, 
	    cp_len);
    }


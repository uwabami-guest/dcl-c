/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;
static logical c_false = FALSE_;
static logical c_true = TRUE_;
static integer c__51 = 51;
static integer c__99 = 99;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int sgstrf_(void)
{
    /* System generated locals */
    real r__1, r__2, r__3;
    logical L__1, L__2;

    /* Builtin functions */
    double r_lg10(real *);

    /* Local variables */
    static real cp, cx, cy, vx0, vy0;
    static integer itr;
    static real plx, ply;
    static logical ldeg;
    extern real rfpi_(void);
    static real rsat, cxmin;
    static logical linit;
    static real cxmax, cymin, cymax, vxoff, vyoff, txmin, uxmin, vxmin, vymin,
	     vxmax, vymax, uxmax, uymin, uymax, plrot, txmax, tymin, tymax;
    extern /* Subroutine */ int g2qcti_(logical *), g2qctm_(real *, real *, 
	    real *, real *);
    static real stlat1, stlat2, simfac;
    extern /* Subroutine */ int sgiget_(char *, integer *, ftnlen), mpscoa_(
	    real *), mpscoc_(real *, real *), sglget_(char *, logical *, 
	    ftnlen), glrget_(char *, real *, ftnlen), msgdmp_(char *, char *, 
	    char *, ftnlen, ftnlen, ftnlen), sgqcwd_(real *, real *, real *, 
	    real *);
    static real rundef;
    extern /* Subroutine */ int sgrget_(char *, real *, ftnlen), mpsbon_(real 
	    *), mpscon_(real *), stsrad_(logical *, logical *), sgqwnd_(real *
	    , real *, real *, real *), sgqsim_(real *, real *, real *), 
	    sgqmpl_(real *, real *, real *), mpsotg_(real *), szscll_(real *, 
	    real *, real *, real *, integer *), sgqvpt_(real *, real *, real *
	    , real *), ststrf_(logical *), szsclx_(real *, real *), szscly_(
	    real *, real *), ststri_(integer *), stsrot_(real *, real *, real 
	    *), ststrp_(real *, real *, real *, real *), sgqtxy_(real *, real 
	    *, real *, real *), stsusr_(void);

    sglget_("LDEG", &ldeg, (ftnlen)4);
    sgiget_("ITR", &itr, (ftnlen)3);
    if (ldeg) {
	cp = rfpi_() / 180;
    } else {
	cp = 1.f;
    }
/*     / VIEW PORT / */
    sgqvpt_(&vxmin, &vxmax, &vymin, &vymax);
    if (! (vxmin < vxmax && vymin < vymax)) {
	msgdmp_("E", "SGSTRF", "VIEWPORT DEFINITION IS INVALID.", (ftnlen)1, (
		ftnlen)6, (ftnlen)31);
    }
    szscll_(&vxmin, &vxmax, &vymin, &vymax, &c__1);
    if (1 <= itr && itr <= 4) {
/*       / LINEAR AND LOG / */
	sgqwnd_(&uxmin, &uxmax, &uymin, &uymax);
	if (itr <= 2) {
	    cx = (vxmax - vxmin) / (uxmax - uxmin);
	    vx0 = vxmin - cx * uxmin;
	} else {
	    if (uxmin * uxmax <= 0.f) {
		msgdmp_("E", "SGSTRF", "THE REGION STRADDLES 0 FOR LOG TRANS"
			"FORMATION (X).", (ftnlen)1, (ftnlen)6, (ftnlen)50);
	    }
	    r__1 = uxmax / uxmin;
	    cx = (vxmax - vxmin) / r_lg10(&r__1);
	    r__1 = abs(uxmin);
	    vx0 = vxmin - cx * r_lg10(&r__1);
	}
	if (itr % 2 == 1) {
	    cy = (vymax - vymin) / (uymax - uymin);
	    vy0 = vymin - cy * uymin;
	} else {
	    if (uymin * uymax <= 0.f) {
		msgdmp_("E", "SGSTRF", "THE REGION STRADDLES 0 FOR LOG TRANS"
			"FORMATION (Y).", (ftnlen)1, (ftnlen)6, (ftnlen)50);
	    }
	    r__1 = uymax / uymin;
	    cy = (vymax - vymin) / r_lg10(&r__1);
	    r__1 = abs(uymin);
	    vy0 = vymin - cy * r_lg10(&r__1);
	}
	stsrad_(&c_false, &c_false);
	ststrf_(&c_false);
/*       CALL STSTRN(ITR, CX, CY, VX0, VY0) */
	ststri_(&itr);
	ststrp_(&cx, &cy, &vx0, &vy0);
    } else if (5 <= itr && itr <= 6) {
/*       / ORTHOGONAL CURVE / */
	sgqsim_(&simfac, &vxoff, &vyoff);
	vx0 = (vxmax + vxmin) / 2 + vxoff;
	vy0 = (vymax + vymin) / 2 + vyoff;
	L__1 = ldeg && itr == 6;
	L__2 = ldeg && (itr == 5 || itr == 6);
	stsrad_(&L__1, &L__2);
	ststrf_(&c_false);
/*       CALL STSTRN(ITR, SIMFAC, SIMFAC, VX0, VY0) */
	ststri_(&itr);
	ststrp_(&simfac, &simfac, &vx0, &vy0);
    } else if (10 <= itr && itr <= 19 || 20 <= itr && itr <= 24 || 30 <= itr 
	    && itr <= 34) {
/*       / MAP / */
	sgqsim_(&simfac, &vxoff, &vyoff);
	sgqmpl_(&plx, &ply, &plrot);
	vx0 = (vxmax + vxmin) / 2 + vxoff;
	vy0 = (vymax + vymin) / 2 + vyoff;
	stsrad_(&ldeg, &ldeg);
	r__1 = rfpi_() / 2 - cp * ply;
	r__2 = cp * plx;
	r__3 = cp * plrot;
	stsrot_(&r__1, &r__2, &r__3);
	ststrf_(&c_true);
/*       CALL STSTRN(ITR, SIMFAC, SIMFAC, VX0, VY0) */
	ststri_(&itr);
	ststrp_(&simfac, &simfac, &vx0, &vy0);
/*       / SET CLIPPING REGION / */
	sgqtxy_(&txmin, &txmax, &tymin, &tymax);
	r__1 = cp * txmin;
	r__2 = cp * txmax;
	szsclx_(&r__1, &r__2);
	r__1 = cp * tymin;
	r__2 = cp * tymax;
	szscly_(&r__1, &r__2);
/*       / SATELITE VIEW / */
	sgrget_("RSAT", &rsat, (ftnlen)4);
	mpsotg_(&rsat);
/*       / SET STANDARD LATITUDE / */
	sgrget_("STLAT1", &stlat1, (ftnlen)6);
	sgrget_("STLAT2", &stlat2, (ftnlen)6);
	if (itr == 20) {
	    r__1 = cp * stlat1;
	    mpscon_(&r__1);
	} else if (itr == 21) {
	    r__1 = cp * stlat1;
	    mpscoa_(&r__1);
	} else if (itr == 22) {
	    r__1 = cp * stlat1;
	    r__2 = cp * stlat2;
	    mpscoc_(&r__1, &r__2);
	} else if (itr == 23) {
	    r__1 = cp * stlat1;
	    mpsbon_(&r__1);
	}
    } else if (itr == 51) {
	g2qcti_(&linit);
	if (! linit) {
	    msgdmp_("E", "SGSTRF", "(ITR:51) NOT YET INITIALIZED. CALL G2SCT"
		    "R.", (ftnlen)1, (ftnlen)6, (ftnlen)42);
	}
	sgqcwd_(&cxmin, &cxmax, &cymin, &cymax);
	glrget_("RUNDEF", &rundef, (ftnlen)6);
	if (cxmin == rundef && cxmax == rundef || cymin == rundef && cymax == 
		rundef) {
	    g2qctm_(&cxmin, &cxmax, &cymin, &cymax);
	}
	sgqvpt_(&vxmin, &vxmax, &vymin, &vymax);
	cx = (vxmax - vxmin) / (cxmax - cxmin);
	vx0 = vxmin - cx * cxmin;
	cy = (vymax - vymin) / (cymax - cymin);
	vy0 = vymin - cy * cymin;
	stsrad_(&c_false, &c_false);
	ststrf_(&c_false);
/*       CALL STSTRN(51,CX,CY,VX0,VY0) */
	ststri_(&c__51);
	ststrp_(&cx, &cy, &vx0, &vy0);
    } else if (itr == 99) {
	ststri_(&c__99);
	stsusr_();
    } else {
	msgdmp_("E", "SGSTRF", "TRANSFORMATION FUNCTION NUMBER IS INVALID.", (
		ftnlen)1, (ftnlen)6, (ftnlen)42);
    }
    return 0;
} /* sgstrf_ */


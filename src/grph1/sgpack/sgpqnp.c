/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__3 = 3;

/* ----------------------------------------------------------------------- */
/*     PARAMETER CONTROL (GENERIC) */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int sgpqnp_0_(int n__, integer *ncp, char *cp, integer *idx, 
	integer *itp, integer *ipara, integer *in, ftnlen cp_len)
{
    /* Initialized data */

    static char cparas[8*122] = "RDX     " "RDY     " "LCLIP   " "LCHAR   " 
	    "LLNINT  " "LGCINT  " "LSOFTF  " "LCL2TN  " "IRMODE  " "BITLEN  " 
	    "MOVE    " "NBITS   " "LCNTL   " "SMALL   " "SHIFT   " "ISUP    " 
	    "ISUB    " "IRST    " "LFULL   " "PMFACT  " "NPMSKIP " "RFAROT  " 
	    "TNBLEN  " "IFONT   " "ICLRMAP " "LFPROP  " "LROT    " "IROT    " 
	    "FWC     " "CWL     " "FFCT    " "INDEXC  " "LBUFF   " "NBUFF   " 
	    "RBUFF   " "LCURV   " "RCURV   " "LPROP   " "LUARW   " "AFACT   " 
	    "CONST   " "CONSTM  " "ANGLE   " "LARROW  " "LATONE  " "IATONE  " 
	    "IWS     " "ITR     " "IBGCLI  " "VXMIN   " "VXMAX   " "VYMIN   " 
	    "VYMAX   " "UXMIN   " "UXMAX   " "UYMIN   " "UYMAX   " "VXOFF   " 
	    "VYOFF   " "PLX     " "PLY     " "PLROT   " "SIMFAC  " "STLAT1  " 
	    "STLAT2  " "TXMIN   " "TXMAX   " "TYMIN   " "TYMAX   " "CXMIN   " 
	    "CXMAX   " "CYMIN   " "CYMAX   " "RSAT    " "LDEG    " "NFRAME  " 
	    "NPAGE   " "NLEVEL  " "FACTOR  " "INDEX   " "LCORNER " "LTITLE  " 
	    "CORNER  " "XPAD    " "YPAD    " "LWIDE   " "ITR3    " "TILT3   " 
	    "ANGLE3  " "XOFF3   " "YOFF3   " "XOBJ3   " "YOBJ3   " "ZOBJ3   " 
	    "XEYE3   " "YEYE3   " "ZEYE3   " "IXC3    " "IYC3    " "SEC3    " 
	    "LFGBG   " "L2TO3   " "VXMIN3  " "VXMAX3  " "VYMIN3  " "VYMAX3  " 
	    "VZMIN3  " "VZMAX3  " "UXMIN3  " "UXMAX3  " "UYMIN3  " "UYMAX3  " 
	    "UZMIN3  " "UZMAX3  " "VXORG3  " "VYORG3  " "VZORG3  " "SIMFAC3 " 
	    "LXLOG3  " "LYLOG3  " "LZLOG3  " "LMRKFNT ";
    static integer itype[122] = { 3,3,2,2,2,2,2,2,1,3,1,1,2,3,3,1,1,1,2,3,1,3,
	    3,1,1,2,2,1,3,3,3,1,2,1,3,2,3,2,2,3,3,3,3,2,2,1,0,1,1,3,3,3,3,3,3,
	    3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,2,1,1,1,3,1,2,2,3,3,3,2,1,3,
	    3,3,3,3,3,3,3,3,3,1,1,3,2,2,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,2,2,2,
	    2 };
    static char cparal[40*122] = "INTERPOLATION_ANGLE_X                   " 
	    "INTERPOLATION_ANGLE_Y                   " "ENABLE_CLIPPING     "
	    "                    " "ENABLE_LINE_LABELING                    " 
	    "ENABLE_LINEAR_INTERPOLATION             " "ENABLE_GREATCIRCLE_I"
	    "NTERPOLATION        " "ENABLE_SOFTFILL                         " 
	    "ENABLE_COLOR_SUBSTITUTION               " "BOUNDARY_DIRECTION  "
	    "                    " "LINE_BIT_LENGTH                         " 
	    "LINE_RESUME_MODE                        " "PATTERN_BIT_LENGTH  "
	    "                    " "ENABLE_CONTROL_CHAR                     " 
	    "SCRIPT_HEIGHT                           " "SCRIPT_SHIFT        "
	    "                    " "BEGIN_SUPERSCRIPT                       " 
	    "BEGIN_SUBSCRIPT                         " "END_SCRIPT          "
	    "                    " "USE_FULL_WINDOW                         " 
	    "POLIMARKER_HEIGHT                       " "POLIMARKER_INTERVAL "
	    "                    " "****RFAROT                              " 
	    "TONE_CYCLE_LENGTH                       " "FONT_NUMBER         "
	    "                    " "COLORMAP_NUMBER                         " 
	    "ENABLE_PROPORTINAL_FONT                 " "ENABLE_LINE_LABEL_RO"
	    "TATION              " "LINE_LABEL_ROTATION_ANGLE               " 
	    "LINE_LABEL_SPACE                        " "LINE_CYCLE_LENGTH   "
	    "                    " "LINE_START_POSITION                     " 
	    "LABEL_CHAR_INDEX                        " "ENABLE_LINE_BUFFERIN"
	    "G                   " "LINE_BUFFERING_LENGTH                   " 
	    "RESUME_POSITION_AFTER_BUFFERING         " "DISABLE_LINE_LABEL_A"
	    "T_CURVE             " "LINE_LABEL_THRESHOLD_CURVATURE          " 
	    "FIX_ARROWHEAD_PROPORTION                " "ENABLE_ARROWHEAD_MAP"
	    "PING                " "ARROWHEAD_PROPOTION                     " 
	    "ARROWHEAD_SIZE                          " "ARROWHEAD_SIZE_MAP  "
	    "                    " "ARROWHEAD_ANGLE                         " 
	    "----LARROW                              " "ENABLE_ARROWHEAD_SHA"
	    "DE                  " "ARROWHEAD_SHADE_PATTERN                 " 
	    "++++IWS                                 " "++++ITR             "
	    "                    " "BACKGROUND_COLOR_INDEX                  " 
	    "++++VXMIN                               " "++++VXMAX           "
	    "                    " "++++VYMIN                               " 
	    "++++VYMAX                               " "++++UXMIN           "
	    "                    " "++++UXMAX                               " 
	    "++++UYMIN                               " "++++UYMAX           "
	    "                    " "++++VXOFF                               " 
	    "++++VYOFF                               " "++++PLX             "
	    "                    " "++++PLY                                 " 
	    "++++PLROT                               " "++++SIMFAC          "
	    "                    " "++++STLAT1                              " 
	    "++++STLAT2                              " "++++TXMIN           "
	    "                    " "++++TXMAX                               " 
	    "++++TYMIN                               " "++++TYMAX           "
	    "                    " "++++CXMIN                               " 
	    "++++CXMAX                               " "++++CYMIN           "
	    "                    " "++++CYMAX                               " 
	    "SATELLITE_ALTITUDE                      " "USE_DEGREE          "
	    "                    " "****NFRAME                              " 
	    "****NPAGE                               " "****NLEVEL          "
	    "                    " "****FACTOR                              " 
	    "CORNERMARK_INDEX                        " "DRAW_CORNERMARK     "
	    "                    " "DRAW_PAGE_TITLE                         " 
	    "CORNERMARK_LENGTH                       " "****XPAD            "
	    "                    " "****YPAD                                " 
	    "****LWIDE                               " "++++ITR3            "
	    "                    " "++++TILT3                               " 
	    "++++ANGLE3                              " "++++XOFF3           "
	    "                    " "++++YOFF3                               " 
	    "++++XOBJ3                               " "++++YOBJ3           "
	    "                    " "++++ZOBJ3                               " 
	    "++++XEYE3                               " "++++YEYE3           "
	    "                    " "++++ZEYE3                               " 
	    "++++IXC3                                " "++++IYC3            "
	    "                    " "++++SEC3                                " 
	    "****LFGBG                               " "****L2TO3           "
	    "                    " "++++VXMIN3                              " 
	    "++++VXMAX3                              " "++++VYMIN3          "
	    "                    " "++++VYMAX3                              " 
	    "++++VZMIN3                              " "++++VZMAX3          "
	    "                    " "++++UXMIN3                              " 
	    "++++UXMAX3                              " "++++UYMIN3          "
	    "                    " "++++UYMAX3                              " 
	    "++++UZMIN3                              " "++++UZMAX3          "
	    "                    " "++++VXORG3                              " 
	    "++++VYORG3                              " "++++VZORG3          "
	    "                    " "++++SIMFAC3                             " 
	    "++++LXLOG3                              " "++++LYLOG3          "
	    "                    " "++++LZLOG3                              " 
	    "USE_DCLMARKER_FONT                      ";

    /* System generated locals */
    address a__1[3];
    integer i__1[3];

    /* Builtin functions */
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen),
	     s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer n, id;
    extern integer lenc_(char *, ftnlen);
    static char cmsg[80];
    extern logical lchreq_(char *, char *, ftnlen, ftnlen);
    extern /* Subroutine */ int sgiqid_(char *, integer *, ftnlen), sglqid_(
	    char *, integer *, ftnlen), msgdmp_(char *, char *, char *, 
	    ftnlen, ftnlen, ftnlen), sgrqid_(char *, integer *, ftnlen), 
	    sgiqvl_(integer *, integer *), sglqvl_(integer *, integer *), 
	    sgisvl_(integer *, integer *), sglsvl_(integer *, integer *), 
	    sgrqvl_(integer *, integer *), sgrsvl_(integer *, integer *);

/*     / SHORT NAME / */
    switch(n__) {
	case 1: goto L_sgpqid;
	case 2: goto L_sgpqcp;
	case 3: goto L_sgpqcl;
	case 4: goto L_sgpqit;
	case 5: goto L_sgpqvl;
	case 6: goto L_sgpsvl;
	case 7: goto L_sgpqin;
	}

/*     / LONG  NAME / */
    *ncp = 122;
    return 0;
/* ----------------------------------------------------------------------- */

L_sgpqid:
    for (n = 1; n <= 122; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *idx = n;
	    return 0;
	}
/* L10: */
    }
/* Writing concatenation */
    i__1[0] = 11, a__1[0] = "PARAMETER '";
    i__1[1] = lenc_(cp, cp_len), a__1[1] = cp;
    i__1[2] = 17, a__1[2] = "' IS NOT DEFINED.";
    s_cat(cmsg, a__1, i__1, &c__3, (ftnlen)80);
    msgdmp_("E", "SGPQID", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
    return 0;
/* ----------------------------------------------------------------------- */

L_sgpqcp:
    if (1 <= *idx && *idx <= 122) {
	s_copy(cp, cparas + (*idx - 1 << 3), cp_len, (ftnlen)8);
    } else {
	msgdmp_("E", "SGPQCP", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_sgpqcl:
    if (1 <= *idx && *idx <= 122) {
	s_copy(cp, cparal + (*idx - 1) * 40, cp_len, (ftnlen)40);
    } else {
	msgdmp_("E", "SGPQCL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_sgpqit:
    if (1 <= *idx && *idx <= 122) {
	*itp = itype[*idx - 1];
    } else {
	msgdmp_("E", "SGPQIT", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_sgpqvl:
    if (1 <= *idx && *idx <= 122) {
	if (itype[*idx - 1] == 1) {
	    sgiqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    sgiqvl_(&id, ipara);
	} else if (itype[*idx - 1] == 2) {
	    sglqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    sglqvl_(&id, ipara);
	} else if (itype[*idx - 1] == 3) {
	    sgrqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    sgrqvl_(&id, ipara);
	}
    } else {
	msgdmp_("E", "SGPQVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_sgpsvl:
    if (1 <= *idx && *idx <= 122) {
	if (itype[*idx - 1] == 1) {
	    sgiqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    sgisvl_(&id, ipara);
	} else if (itype[*idx - 1] == 2) {
	    sglqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    sglsvl_(&id, ipara);
	} else if (itype[*idx - 1] == 3) {
	    sgrqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    sgrsvl_(&id, ipara);
	}
    } else {
	msgdmp_("E", "SGPSVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_sgpqin:
    for (n = 1; n <= 122; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *in = n;
	    return 0;
	}
/* L20: */
    }
    *in = 0;
    return 0;
} /* sgpqnp_ */

/* Subroutine */ int sgpqnp_(integer *ncp)
{
    return sgpqnp_0_(0, ncp, (char *)0, (integer *)0, (integer *)0, (integer *
	    )0, (integer *)0, (ftnint)0);
    }

/* Subroutine */ int sgpqid_(char *cp, integer *idx, ftnlen cp_len)
{
    return sgpqnp_0_(1, (integer *)0, cp, idx, (integer *)0, (integer *)0, (
	    integer *)0, cp_len);
    }

/* Subroutine */ int sgpqcp_(integer *idx, char *cp, ftnlen cp_len)
{
    return sgpqnp_0_(2, (integer *)0, cp, idx, (integer *)0, (integer *)0, (
	    integer *)0, cp_len);
    }

/* Subroutine */ int sgpqcl_(integer *idx, char *cp, ftnlen cp_len)
{
    return sgpqnp_0_(3, (integer *)0, cp, idx, (integer *)0, (integer *)0, (
	    integer *)0, cp_len);
    }

/* Subroutine */ int sgpqit_(integer *idx, integer *itp)
{
    return sgpqnp_0_(4, (integer *)0, (char *)0, idx, itp, (integer *)0, (
	    integer *)0, (ftnint)0);
    }

/* Subroutine */ int sgpqvl_(integer *idx, integer *ipara)
{
    return sgpqnp_0_(5, (integer *)0, (char *)0, idx, (integer *)0, ipara, (
	    integer *)0, (ftnint)0);
    }

/* Subroutine */ int sgpsvl_(integer *idx, integer *ipara)
{
    return sgpqnp_0_(6, (integer *)0, (char *)0, idx, (integer *)0, ipara, (
	    integer *)0, (ftnint)0);
    }

/* Subroutine */ int sgpqin_(char *cp, integer *in, ftnlen cp_len)
{
    return sgpqnp_0_(7, (integer *)0, cp, (integer *)0, (integer *)0, (
	    integer *)0, in, cp_len);
    }


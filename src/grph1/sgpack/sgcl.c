/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int sgclst_0_(int n__, char *ccfile, integer *num, integer *
	nr, integer *ng, integer *nb, ftnlen ccfile_len)
{
    /* System generated locals */
    integer i__1;
    olist o__1;
    cllist cl__1;

    /* Builtin functions */
    integer f_open(olist *), s_rsfe(cilist *), do_fio(integer *, char *, 
	    ftnlen), e_rsfe(void);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_wsfi(icilist *), e_wsfi(void), f_clos(cllist *);

    /* Local variables */
    static integer i__, ib[256], ig[256], ir[256], iu, ns, ib0, ig0, ir0;
    static char cmsg[64];
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen);
    extern integer iufopn_(void);
    static integer nplmax, max_pal__;

    /* Fortran I/O blocks */
    static cilist io___2 = { 0, 0, 0, "(i3)", 0 };
    static icilist io___5 = { 0, cmsg+27, 0, "(i2)", 2, 1 };
    static cilist io___9 = { 0, 0, 0, "(3i6)", 0 };


    switch(n__) {
	case 1: goto L_sgqcl;
	}

    iu = iufopn_();
    o__1.oerr = 0;
    o__1.ounit = iu;
    o__1.ofnmlen = ccfile_len;
    o__1.ofnm = ccfile;
    o__1.orl = 0;
    o__1.osta = 0;
    o__1.oacc = 0;
    o__1.ofm = 0;
    o__1.oblnk = 0;
    f_open(&o__1);
    io___2.ciunit = iu;
    s_rsfe(&io___2);
    do_fio(&c__1, (char *)&max_pal__, (ftnlen)sizeof(integer));
    e_rsfe();
    if (max_pal__ > 256) {
	max_pal__ = 256;
	s_copy(cmsg, "color numbers greater than xx are ignored.", (ftnlen)64,
		 (ftnlen)42);
	s_wsfi(&io___5);
	do_fio(&c__1, (char *)&nplmax, (ftnlen)sizeof(integer));
	e_wsfi();
	msgdmp_("w", "zmdopn", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)64);
    }
    ns = 256;
    i__1 = max_pal__;
    for (i__ = 1; i__ <= i__1; ++i__) {
	io___9.ciunit = iu;
	s_rsfe(&io___9);
	do_fio(&c__1, (char *)&ir0, (ftnlen)sizeof(integer));
	do_fio(&c__1, (char *)&ig0, (ftnlen)sizeof(integer));
	do_fio(&c__1, (char *)&ib0, (ftnlen)sizeof(integer));
	e_rsfe();
	ir[i__ - 1] = ir0 / ns;
	ig[i__ - 1] = ig0 / ns;
	ib[i__ - 1] = ib0 / ns;
    }
    cl__1.cerr = 0;
    cl__1.cunit = iu;
    cl__1.csta = 0;
    f_clos(&cl__1);
    return 0;
/* ------------------------------------------------------------------------ */

L_sgqcl:
    *nr = ir[*num];
    *ng = ig[*num];
    *nb = ib[*num];
    return 0;
} /* sgclst_ */

/* Subroutine */ int sgclst_(char *ccfile, ftnlen ccfile_len)
{
    return sgclst_0_(0, ccfile, (integer *)0, (integer *)0, (integer *)0, (
	    integer *)0, ccfile_len);
    }

/* Subroutine */ int sgqcl_(integer *num, integer *nr, integer *ng, integer *
	nb)
{
    return sgclst_0_(1, (char *)0, num, nr, ng, nb, (ftnint)0);
    }


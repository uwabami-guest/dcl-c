/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int sgtxzu_(real *ux, real *uy, char *chars, real *rsize, 
	integer *irota, integer *icent, integer *index, ftnlen chars_len)
{
    /* System generated locals */
    real r__1, r__2;

    /* Builtin functions */
    double cos(doublereal), sin(doublereal);

    /* Local variables */
    static real ct, st, vx, vy, wx, wy, wxr, wyr;
    extern real rd2r_(real *);
    static real theta, qsize, q2size;
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), stftrf_(real *, real *, real *, real *), sztxcl_(
	    void), stfwtr_(real *, real *, real *, real *), sztxop_(real *, 
	    integer *, integer *, integer *), sztxzu_(real *, real *, char *, 
	    ftnlen);

    if (*rsize == 0.f) {
	msgdmp_("M", "SGTXZU", "TEXT HEIGHT IS 0 / DO NOTHING.", (ftnlen)1, (
		ftnlen)6, (ftnlen)30);
	return 0;
    }
    if (*rsize < 0.f) {
	msgdmp_("E", "SGTXZU", "TEXT HEIGHT IS LESS THAN ZERO.", (ftnlen)1, (
		ftnlen)6, (ftnlen)30);
    }
    if (! (-1 <= *icent && *icent <= 1)) {
	msgdmp_("E", "SGTXZU", "CENTERING OPTION IS INVALID.", (ftnlen)1, (
		ftnlen)6, (ftnlen)28);
    }
    if (*index == 0) {
	msgdmp_("M", "SGTXZU", "TEXT INDEX IS 0 / DO NOTHING.", (ftnlen)1, (
		ftnlen)6, (ftnlen)29);
	return 0;
    }
    if (*index < 0) {
	msgdmp_("E", "SGTXZU", "TEXT INDEX IS LESS THAN 0.", (ftnlen)1, (
		ftnlen)6, (ftnlen)26);
    }
    stftrf_(ux, uy, &vx, &vy);
    stfwtr_(&vx, &vy, &wx, &wy);
    r__1 = vx + *rsize;
    r__2 = vy + *rsize;
    stfwtr_(&r__1, &r__2, &wxr, &wyr);
    qsize = wxr - wx;
    q2size = wyr - wy;
    if (q2size > qsize) {
	qsize = q2size;
    }
    r__1 = (real) (*irota);
    theta = rd2r_(&r__1);
    ct = qsize * cos(theta);
    st = qsize * sin(theta);
    sztxop_(rsize, irota, icent, index);
    sztxzu_(ux, uy, chars, chars_len);
    sztxcl_();
    return 0;
} /* sgtxzu_ */


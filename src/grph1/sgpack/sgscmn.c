/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int sgscmn_0_(int n__, integer *nc, integer *nmax)
{
    static integer iws;
    static logical lcmch, lfcatr;
    extern /* Subroutine */ int swclch_(void), swqfcc_(logical *), msgdmp_(
	    char *, char *, char *, ftnlen, ftnlen, ftnlen), swcmll_(void), 
	    swiget_(char *, integer *, ftnlen), swlget_(char *, logical *, 
	    ftnlen), swqcmn_(integer *), swistx_(char *, integer *, ftnlen);

    switch(n__) {
	case 1: goto L_sgqcmn;
	}

    swcmll_();
    swistx_("ICLRMAP", nc, (ftnlen)7);
    swqfcc_(&lfcatr);
    swlget_("LCMCH", &lcmch, (ftnlen)5);
    swiget_("IWS", &iws, (ftnlen)3);
    if (iws >= 1 && iws <= 4) {
	if (lcmch) {
	    if (! lfcatr || iws == 1) {
		msgdmp_("W", "SGSCMN", "COLORMAP MIGHT NOT CHANGE IMMEDIATEL"
			"Y.", (ftnlen)1, (ftnlen)6, (ftnlen)38);
	    }
	    swclch_();
	}
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_sgqcmn:
    swcmll_();
    swqcmn_(nmax);
    return 0;
} /* sgscmn_ */

/* Subroutine */ int sgscmn_(integer *nc)
{
    return sgscmn_0_(0, nc, (integer *)0);
    }

/* Subroutine */ int sgqcmn_(integer *nmax)
{
    return sgscmn_0_(1, (integer *)0, nmax);
    }


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__29 = 29;
static integer c__1 = 1;
static integer c__3 = 3;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int sgtrqf_0_(int n__, integer *ntx, logical *ltr, char *cts,
	 char *ctl, ftnlen cts_len, ftnlen ctl_len)
{
    /* Initialized data */

    static char ctr1[3*29] = "U-U" "U-L" "L-U" "L-L" "POL" "BPL" "ELP" "CYL" 
	    "MER" "MWD" "HMR" "EK6" "KTD" "CON" "COA" "COC" "BON" "OTG" "PST" 
	    "AZM" "AZA" "GTR" "USR" "MIL" "RBS" "SIN" "VDG" "PLC" "GNO";
    static char ctr2[20*29] = "UNI-UNI             " "UNI-LOG             " 
	    "LOG-UNI             " "LOG-LOG             " "POLAR            "
	    "   " "BIPOLAR             " "ELLIPTIC            " "EQDST. CYLIN"
	    "DRICAL  " "MERCATOR            " "MOLLWEIDE           " "HAMMER "
	    "             " "ECKERT 6            " "KITADA              " 
	    "PTOLEMAIC CONICAL   " "EQ.-AREA CONICAL    " "CONFORMAL CONICAL"
	    "   " "BONNE               " "ORTHOGRAPHIC        " "STEREOGRAPHI"
	    "C       " "EQUIDST. AZIMUTHAL  " "AZIMUTHAL EQ. AREA  " "GRIDBAS"
	    "E TRANSFORM  " "USER                " "MILLER              " 
	    "ROBINSON            " "SANSON/SINUSOIDAL   " "VAN DER GRINTEN  "
	    "   " "POLYCONIC           " "GNOMONIC            ";
    static integer ntr[29] = { 1,2,3,4,5,6,7,10,11,12,13,14,15,20,21,22,23,30,
	    31,32,33,51,99,16,17,18,19,24,34 };

    /* System generated locals */
    address a__1[3];
    integer i__1[3];

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen), s_cat(char *,
	     char **, integer *, integer *, ftnlen);

    /* Local variables */
    static integer n, ncp;
    extern integer lenc_(char *, ftnlen);
    static char cmsg[80];
    extern /* Subroutine */ int chngi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen);
    extern logical lchreq_(char *, char *, ftnlen, ftnlen);
    extern integer indxif_(integer *, integer *, integer *, integer *);
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen);

    switch(n__) {
	case 1: goto L_sgtrsl;
	case 2: goto L_sgtrsn;
	case 3: goto L_sgtrls;
	case 4: goto L_sgtrln;
	case 5: goto L_sgtrns;
	case 6: goto L_sgtrnl;
	}

    *ltr = indxif_(ntr, &c__29, &c__1, ntx) != 0;
    return 0;
/* ----------------------------------------------------------------------- */

L_sgtrsl:
    for (n = 1; n <= 29; ++n) {
	if (lchreq_(cts, ctr1 + (n - 1) * 3, cts_len, (ftnlen)3)) {
	    s_copy(ctl, ctr2 + (n - 1) * 20, ctl_len, (ftnlen)20);
	    return 0;
	}
/* L10: */
    }
    ncp = lenc_(cts, cts_len);
/* Writing concatenation */
    i__1[0] = 21, a__1[0] = "TRANSFORMATION NAME <";
    i__1[1] = ncp, a__1[1] = cts;
    i__1[2] = 17, a__1[2] = "> IS NOT DEFINED.";
    s_cat(cmsg, a__1, i__1, &c__3, (ftnlen)80);
    msgdmp_("E", "SGTRSL", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
    return 0;
/* ----------------------------------------------------------------------- */

L_sgtrsn:
    for (n = 1; n <= 29; ++n) {
	if (lchreq_(cts, ctr1 + (n - 1) * 3, cts_len, (ftnlen)3)) {
	    *ntx = ntr[n - 1];
	    return 0;
	}
/* L20: */
    }
    ncp = lenc_(cts, cts_len);
/* Writing concatenation */
    i__1[0] = 21, a__1[0] = "TRANSFORMATION NAME <";
    i__1[1] = ncp, a__1[1] = cts;
    i__1[2] = 17, a__1[2] = "> IS NOT DEFINED.";
    s_cat(cmsg, a__1, i__1, &c__3, (ftnlen)80);
    msgdmp_("E", "SGTRSN", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
    return 0;
/* ----------------------------------------------------------------------- */

L_sgtrls:
    for (n = 1; n <= 29; ++n) {
	if (lchreq_(ctl, ctr2 + (n - 1) * 20, ctl_len, (ftnlen)20)) {
	    s_copy(cts, ctr1 + (n - 1) * 3, cts_len, (ftnlen)3);
	    return 0;
	}
/* L30: */
    }
    ncp = lenc_(ctl, ctl_len);
/* Writing concatenation */
    i__1[0] = 21, a__1[0] = "TRANSFORMATION NAME <";
    i__1[1] = ncp, a__1[1] = ctl;
    i__1[2] = 17, a__1[2] = "> IS NOT DEFINED.";
    s_cat(cmsg, a__1, i__1, &c__3, (ftnlen)80);
    msgdmp_("E", "SGTRLS", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
    return 0;
/* ----------------------------------------------------------------------- */

L_sgtrln:
    for (n = 1; n <= 29; ++n) {
	if (lchreq_(ctl, ctr2 + (n - 1) * 20, ctl_len, (ftnlen)20)) {
	    *ntx = ntr[n - 1];
	    return 0;
	}
/* L40: */
    }
    ncp = lenc_(ctl, ctl_len);
/* Writing concatenation */
    i__1[0] = 21, a__1[0] = "TRANSFORMATION NAME <";
    i__1[1] = ncp, a__1[1] = ctl;
    i__1[2] = 17, a__1[2] = "> IS NOT DEFINED.";
    s_cat(cmsg, a__1, i__1, &c__3, (ftnlen)80);
    msgdmp_("E", "SGTRLN", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
    return 0;
/* ----------------------------------------------------------------------- */

L_sgtrns:
    for (n = 1; n <= 29; ++n) {
	if (*ntx == ntr[n - 1]) {
	    s_copy(cts, ctr1 + (n - 1) * 3, cts_len, (ftnlen)3);
	    return 0;
	}
/* L50: */
    }
    s_copy(cmsg, "TRANSFORMATION NUMBER <##> IS NOT DEFINED.", (ftnlen)80, (
	    ftnlen)42);
    chngi_(cmsg, "##", ntx, "(I2)", (ftnlen)80, (ftnlen)2, (ftnlen)4);
    msgdmp_("E", "SGTRNS", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
    return 0;
/* ----------------------------------------------------------------------- */

L_sgtrnl:
    for (n = 1; n <= 29; ++n) {
	if (*ntx == ntr[n - 1]) {
	    s_copy(ctl, ctr2 + (n - 1) * 20, ctl_len, (ftnlen)20);
	    return 0;
	}
/* L60: */
    }
    s_copy(cmsg, "TRANSFORMATION NUMBER <##> IS NOT DEFINED.", (ftnlen)80, (
	    ftnlen)42);
    chngi_(cmsg, "##", ntx, "(I2)", (ftnlen)80, (ftnlen)2, (ftnlen)4);
    msgdmp_("E", "SGTRNL", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
    return 0;
} /* sgtrqf_ */

/* Subroutine */ int sgtrqf_(integer *ntx, logical *ltr)
{
    return sgtrqf_0_(0, ntx, ltr, (char *)0, (char *)0, (ftnint)0, (ftnint)0);
    }

/* Subroutine */ int sgtrsl_(char *cts, char *ctl, ftnlen cts_len, ftnlen 
	ctl_len)
{
    return sgtrqf_0_(1, (integer *)0, (logical *)0, cts, ctl, cts_len, 
	    ctl_len);
    }

/* Subroutine */ int sgtrsn_(char *cts, integer *ntx, ftnlen cts_len)
{
    return sgtrqf_0_(2, ntx, (logical *)0, cts, (char *)0, cts_len, (ftnint)0)
	    ;
    }

/* Subroutine */ int sgtrls_(char *ctl, char *cts, ftnlen ctl_len, ftnlen 
	cts_len)
{
    return sgtrqf_0_(3, (integer *)0, (logical *)0, cts, ctl, cts_len, 
	    ctl_len);
    }

/* Subroutine */ int sgtrln_(char *ctl, integer *ntx, ftnlen ctl_len)
{
    return sgtrqf_0_(4, ntx, (logical *)0, (char *)0, ctl, (ftnint)0, ctl_len)
	    ;
    }

/* Subroutine */ int sgtrns_(integer *ntx, char *cts, ftnlen cts_len)
{
    return sgtrqf_0_(5, ntx, (logical *)0, cts, (char *)0, cts_len, (ftnint)0)
	    ;
    }

/* Subroutine */ int sgtrnl_(integer *ntx, char *ctl, ftnlen ctl_len)
{
    return sgtrqf_0_(6, ntx, (logical *)0, (char *)0, ctl, (ftnint)0, ctl_len)
	    ;
    }


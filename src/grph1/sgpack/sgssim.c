/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int sgssim_0_(int n__, real *simfac, real *vxoff, real *
	vyoff)
{
    static real cx, cy, vx0, vy0, cxmin, cymin, cxmax, cymax, vxmin, vymin, 
	    vxmax, vymax;
    extern /* Subroutine */ int sgrget_(char *, real *, ftnlen), sgrset_(char 
	    *, real *, ftnlen), sgqvpt_(real *, real *, real *, real *);

    switch(n__) {
	case 1: goto L_sgqsim;
	}

    sgrset_("SIMFAC", simfac, (ftnlen)6);
    sgrset_("VXOFF", vxoff, (ftnlen)5);
    sgrset_("VYOFF", vyoff, (ftnlen)5);
    sgqvpt_(&vxmin, &vxmax, &vymin, &vymax);
    cx = *simfac;
    cy = *simfac;
    vx0 = (vxmax + vxmin) / 2 + *vxoff;
    vy0 = (vymax + vymin) / 2 + *vyoff;
    cxmin = (vxmin - vx0) / cx;
    cxmax = cxmin + (vxmax - vxmin) / cx;
    cymin = (vymin - vy0) / cy;
    cymax = cymin + (vymax - vymin) / cy;
    sgrset_("CXMIN", &cxmin, (ftnlen)5);
    sgrset_("CXMAX", &cxmax, (ftnlen)5);
    sgrset_("CYMIN", &cymin, (ftnlen)5);
    sgrset_("CYMAX", &cymax, (ftnlen)5);
    return 0;
/* ----------------------------------------------------------------------- */

L_sgqsim:
    sgrget_("SIMFAC", simfac, (ftnlen)6);
    sgrget_("VXOFF", vxoff, (ftnlen)5);
    sgrget_("VYOFF", vyoff, (ftnlen)5);
    return 0;
} /* sgssim_ */

/* Subroutine */ int sgssim_(real *simfac, real *vxoff, real *vyoff)
{
    return sgssim_0_(0, simfac, vxoff, vyoff);
    }

/* Subroutine */ int sgqsim_(real *simfac, real *vxoff, real *vyoff)
{
    return sgssim_0_(1, simfac, vxoff, vyoff);
    }


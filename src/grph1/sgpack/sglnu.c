/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     LINE SUBPRIMITIVE */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int sglnu_0_(int n__, real *ux1, real *uy1, real *ux2, real *
	uy2, real *vx1, real *vy1, real *vx2, real *vy2, real *rx1, real *ry1,
	 real *rx2, real *ry2, integer *index)
{
    /* Initialized data */

    static integer indexz = 1;

    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), szlncl_(void), szlnop_(integer *), szlnzr_(real *
	    , real *, real *, real *), szlnzu_(real *, real *, real *, real *)
	    , szlnzv_(real *, real *, real *, real *);

    switch(n__) {
	case 1: goto L_sglnv;
	case 2: goto L_sglnr;
	case 3: goto L_sgslni;
	case 4: goto L_sgqlni;
	}

    if (indexz == 0) {
	msgdmp_("M", "SGLNU", "LINE INDEX IS 0 / DO NOTHING.", (ftnlen)1, (
		ftnlen)5, (ftnlen)29);
	return 0;
    }
    if (indexz < 0) {
	msgdmp_("E", "SGLNU", "LINE INDEX IS LESS THAN 0.", (ftnlen)1, (
		ftnlen)5, (ftnlen)26);
    }
    szlnop_(&indexz);
    szlnzu_(ux1, uy1, ux2, uy2);
    szlncl_();
    return 0;
/* ----------------------------------------------------------------------- */

L_sglnv:
    if (indexz == 0) {
	msgdmp_("M", "SGLNV", "LINE INDEX IS 0 / DO NOTHING.", (ftnlen)1, (
		ftnlen)5, (ftnlen)29);
	return 0;
    }
    if (indexz < 0) {
	msgdmp_("E", "SGLNV", "LINE INDEX IS LESS THAN 0.", (ftnlen)1, (
		ftnlen)5, (ftnlen)26);
    }
    szlnop_(&indexz);
    szlnzv_(vx1, vy1, vx2, vy2);
    szlncl_();
    return 0;
/* ----------------------------------------------------------------------- */

L_sglnr:
    if (indexz == 0) {
	msgdmp_("M", "SGLNR", "LINE INDEX IS 0 / DO NOTHING.", (ftnlen)1, (
		ftnlen)5, (ftnlen)29);
	return 0;
    }
    if (indexz < 0) {
	msgdmp_("E", "SGLNR", "LINE INDEX IS LESS THAN 0.", (ftnlen)1, (
		ftnlen)5, (ftnlen)26);
    }
    szlnop_(&indexz);
    szlnzr_(rx1, ry1, rx2, ry2);
    szlncl_();
    return 0;
/* ----------------------------------------------------------------------- */

L_sgslni:
    indexz = *index;
    return 0;
/* ----------------------------------------------------------------------- */

L_sgqlni:
    *index = indexz;
    return 0;
} /* sglnu_ */

/* Subroutine */ int sglnu_(real *ux1, real *uy1, real *ux2, real *uy2)
{
    return sglnu_0_(0, ux1, uy1, ux2, uy2, (real *)0, (real *)0, (real *)0, (
	    real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (integer *)
	    0);
    }

/* Subroutine */ int sglnv_(real *vx1, real *vy1, real *vx2, real *vy2)
{
    return sglnu_0_(1, (real *)0, (real *)0, (real *)0, (real *)0, vx1, vy1, 
	    vx2, vy2, (real *)0, (real *)0, (real *)0, (real *)0, (integer *)
	    0);
    }

/* Subroutine */ int sglnr_(real *rx1, real *ry1, real *rx2, real *ry2)
{
    return sglnu_0_(2, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, 
	    (real *)0, (real *)0, (real *)0, rx1, ry1, rx2, ry2, (integer *)0)
	    ;
    }

/* Subroutine */ int sgslni_(integer *index)
{
    return sglnu_0_(3, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, 
	    (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, 
	    (real *)0, index);
    }

/* Subroutine */ int sgqlni_(integer *index)
{
    return sglnu_0_(4, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, 
	    (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, 
	    (real *)0, index);
    }


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     POLYLINE PRIMITIVE */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int sgplu_0_(int n__, integer *n, real *upx, real *upy, real 
	*vpx, real *vpy, real *rpx, real *rpy, integer *itype, integer *index)
{
    /* Initialized data */

    static integer itypez = 1;
    static integer indexz = 1;

    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), szplcl_(void), szplop_(integer *, integer *), 
	    szplzr_(integer *, real *, real *), szplzu_(integer *, real *, 
	    real *), szplzv_(integer *, real *, real *);

    /* Parameter adjustments */
    if (upx) {
	--upx;
	}
    if (upy) {
	--upy;
	}
    if (vpx) {
	--vpx;
	}
    if (vpy) {
	--vpy;
	}
    if (rpx) {
	--rpx;
	}
    if (rpy) {
	--rpy;
	}

    /* Function Body */
    switch(n__) {
	case 1: goto L_sgplv;
	case 2: goto L_sgplr;
	case 3: goto L_sgsplt;
	case 4: goto L_sgqplt;
	case 5: goto L_sgspli;
	case 6: goto L_sgqpli;
	}

    if (*n < 2) {
	msgdmp_("E", "SGPLU", "NUMBER OF POINTS IS LESS THAN 2.", (ftnlen)1, (
		ftnlen)5, (ftnlen)32);
    }
    if (itypez == 0) {
	msgdmp_("M", "SGPLU", "LINETYPE IS 0 / DO NOTHING.", (ftnlen)1, (
		ftnlen)5, (ftnlen)27);
	return 0;
    }
    if (indexz == 0) {
	msgdmp_("M", "SGPLU", "POLYLINE INDEX IS 0 / DO NOTHING.", (ftnlen)1, 
		(ftnlen)5, (ftnlen)33);
	return 0;
    }
    if (indexz < 0) {
	msgdmp_("E", "SGPLU", "POLYLINE INDEX IS LESS THAN 0.", (ftnlen)1, (
		ftnlen)5, (ftnlen)30);
    }
    szplop_(&itypez, &indexz);
    szplzu_(n, &upx[1], &upy[1]);
    szplcl_();
    return 0;
/* ----------------------------------------------------------------------- */

L_sgplv:
    if (*n < 2) {
	msgdmp_("E", "SGPLV", "NUMBER OF POINTS IS LESS THAN 2.", (ftnlen)1, (
		ftnlen)5, (ftnlen)32);
    }
    if (itypez == 0) {
	msgdmp_("M", "SGPLV", "LINETYPE IS 0 / DO NOTHING.", (ftnlen)1, (
		ftnlen)5, (ftnlen)27);
	return 0;
    }
    if (indexz == 0) {
	msgdmp_("M", "SGPLV", "POLYLINE INDEX IS 0 / DO NOTHING.", (ftnlen)1, 
		(ftnlen)5, (ftnlen)33);
	return 0;
    }
    if (indexz < 0) {
	msgdmp_("E", "SGPLV", "POLYLINE INDEX IS LESS THAN 0.", (ftnlen)1, (
		ftnlen)5, (ftnlen)30);
    }
    szplop_(&itypez, &indexz);
    szplzv_(n, &vpx[1], &vpy[1]);
    szplcl_();
    return 0;
/* ----------------------------------------------------------------------- */

L_sgplr:
    if (*n < 2) {
	msgdmp_("E", "SGPLR", "NUMBER OF POINTS IS LESS THAN 2.", (ftnlen)1, (
		ftnlen)5, (ftnlen)32);
    }
    if (itypez == 0) {
	msgdmp_("M", "SGPLR", "LINETYPE IS 0 / DO NOTHING.", (ftnlen)1, (
		ftnlen)5, (ftnlen)27);
	return 0;
    }
    if (indexz == 0) {
	msgdmp_("M", "SGPLR", "POLYLINE INDEX IS 0 / DO NOTHING.", (ftnlen)1, 
		(ftnlen)5, (ftnlen)33);
	return 0;
    }
    if (indexz < 0) {
	msgdmp_("E", "SGPLR", "POLYLINE INDEX IS LESS THAN 0.", (ftnlen)1, (
		ftnlen)5, (ftnlen)30);
    }
    szplop_(&itypez, &indexz);
    szplzr_(n, &rpx[1], &rpy[1]);
    szplcl_();
    return 0;
/* ----------------------------------------------------------------------- */

L_sgsplt:
    itypez = *itype;
    return 0;
/* ----------------------------------------------------------------------- */

L_sgqplt:
    *itype = itypez;
    return 0;
/* ----------------------------------------------------------------------- */

L_sgspli:
    indexz = *index;
    return 0;
/* ----------------------------------------------------------------------- */

L_sgqpli:
    *index = indexz;
    return 0;
} /* sgplu_ */

/* Subroutine */ int sgplu_(integer *n, real *upx, real *upy)
{
    return sgplu_0_(0, n, upx, upy, (real *)0, (real *)0, (real *)0, (real *)
	    0, (integer *)0, (integer *)0);
    }

/* Subroutine */ int sgplv_(integer *n, real *vpx, real *vpy)
{
    return sgplu_0_(1, n, (real *)0, (real *)0, vpx, vpy, (real *)0, (real *)
	    0, (integer *)0, (integer *)0);
    }

/* Subroutine */ int sgplr_(integer *n, real *rpx, real *rpy)
{
    return sgplu_0_(2, n, (real *)0, (real *)0, (real *)0, (real *)0, rpx, 
	    rpy, (integer *)0, (integer *)0);
    }

/* Subroutine */ int sgsplt_(integer *itype)
{
    return sgplu_0_(3, (integer *)0, (real *)0, (real *)0, (real *)0, (real *)
	    0, (real *)0, (real *)0, itype, (integer *)0);
    }

/* Subroutine */ int sgqplt_(integer *itype)
{
    return sgplu_0_(4, (integer *)0, (real *)0, (real *)0, (real *)0, (real *)
	    0, (real *)0, (real *)0, itype, (integer *)0);
    }

/* Subroutine */ int sgspli_(integer *index)
{
    return sgplu_0_(5, (integer *)0, (real *)0, (real *)0, (real *)0, (real *)
	    0, (real *)0, (real *)0, (integer *)0, index);
    }

/* Subroutine */ int sgqpli_(integer *index)
{
    return sgplu_0_(6, (integer *)0, (real *)0, (real *)0, (real *)0, (real *)
	    0, (real *)0, (real *)0, (integer *)0, index);
    }


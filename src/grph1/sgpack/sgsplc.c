/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     CHARACTER SETTING FOR 'LCHAR'=.TRUE. */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int sgsplc_0_(int n__, char *charx, real *rsize, ftnlen 
	charx_len)
{
    /* Initialized data */

    static char charxz[32] = "A                               ";
    static real rsizez = .02f;

    /* System generated locals */
    integer i__1;
    char ch__1[1];

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer nc;
    extern integer lenc_(char *, ftnlen);
    extern /* Character */ VOID csgi_(char *, ftnlen, integer *);
    extern integer isgc_(char *, ftnlen);
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), szschz_(char *, real *, ftnlen);

    switch(n__) {
	case 1: goto L_sgqplc;
	case 2: goto L_sgspls;
	case 3: goto L_sgqpls;
	case 4: goto L_sgnplc;
	}

    nc = lenc_(charx, charx_len);
    if (nc >= 32) {
	msgdmp_("E", "SGSPLC", "TEXT LENGTH IS TOO LONG.", (ftnlen)1, (ftnlen)
		6, (ftnlen)24);
    }
    s_copy(charxz, charx, (ftnlen)32, charx_len);
    szschz_(charxz, &rsizez, (ftnlen)32);
    return 0;
/* ----------------------------------------------------------------------- */

L_sgqplc:
    s_copy(charx, charxz, charx_len, (ftnlen)32);
    return 0;
/* ----------------------------------------------------------------------- */

L_sgspls:
    if (*rsize == 0.f) {
	msgdmp_("M", "SGSPLS", "TEXT HEIGHT IS 0 / DO NOTHING.", (ftnlen)1, (
		ftnlen)6, (ftnlen)30);
	return 0;
    }
    if (*rsize < 0.f) {
	msgdmp_("E", "SGSPLS", "TEXT HEIGHT IS LESS THAN 0.", (ftnlen)1, (
		ftnlen)6, (ftnlen)27);
    }
    rsizez = *rsize;
    szschz_(charxz, &rsizez, (ftnlen)32);
    return 0;
/* ----------------------------------------------------------------------- */

L_sgqpls:
    *rsize = rsizez;
    return 0;
/* ----------------------------------------------------------------------- */

L_sgnplc:
    nc = lenc_(charxz, (ftnlen)32);
    i__1 = isgc_(charxz + (nc - 1), (ftnlen)1) + 1;
    csgi_(ch__1, (ftnlen)1, &i__1);
    *(unsigned char *)&charxz[nc - 1] = *(unsigned char *)&ch__1[0];
    szschz_(charxz, &rsizez, (ftnlen)32);
    return 0;
} /* sgsplc_ */

/* Subroutine */ int sgsplc_(char *charx, ftnlen charx_len)
{
    return sgsplc_0_(0, charx, (real *)0, charx_len);
    }

/* Subroutine */ int sgqplc_(char *charx, ftnlen charx_len)
{
    return sgsplc_0_(1, charx, (real *)0, charx_len);
    }

/* Subroutine */ int sgspls_(real *rsize)
{
    return sgsplc_0_(2, (char *)0, rsize, (ftnint)0);
    }

/* Subroutine */ int sgqpls_(real *rsize)
{
    return sgsplc_0_(3, (char *)0, rsize, (ftnint)0);
    }

/* Subroutine */ int sgnplc_(void)
{
    return sgsplc_0_(4, (char *)0, (real *)0, (ftnint)0);
    }


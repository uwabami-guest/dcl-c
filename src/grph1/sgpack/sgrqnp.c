/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__3 = 3;
static integer c__73 = 73;

/* ----------------------------------------------------------------------- */
/*     SGRQNP / SGRQID / SGRQCP / SGRQVL / SGRSVL */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int sgrqnp_0_(int n__, integer *ncp, char *cp, integer *idx, 
	real *rpara, integer *in, ftnlen cp_len)
{
    /* Initialized data */

    static char cparas[8*73] = "RDX     " "RDY     " "BITLEN  " "SMALL   " 
	    "SHIFT   " "PMFACT  " "RFAROT  " "TNBLEN  " "FWC     " "CWL     " 
	    "FFCT    " "RBUFF   " "RCURV   " "AFACT   " "CONST   " "CONSTM  " 
	    "ANGLE   " "VXMIN   " "VXMAX   " "VYMIN   " "VYMAX   " "UXMIN   " 
	    "UXMAX   " "UYMIN   " "UYMAX   " "VXOFF   " "VYOFF   " "PLX     " 
	    "PLY     " "PLROT   " "SIMFAC  " "STLAT1  " "STLAT2  " "TXMIN   " 
	    "TXMAX   " "TYMIN   " "TYMAX   " "CXMIN   " "CXMAX   " "CYMIN   " 
	    "CYMAX   " "RSAT    " "FACTOR  " "CORNER  " "XPAD    " "YPAD    " 
	    "TILT3   " "ANGLE3  " "XOFF3   " "YOFF3   " "XOBJ3   " "YOBJ3   " 
	    "ZOBJ3   " "XEYE3   " "YEYE3   " "ZEYE3   " "SEC3    " "VXMIN3  " 
	    "VXMAX3  " "VYMIN3  " "VYMAX3  " "VZMIN3  " "VZMAX3  " "UXMIN3  " 
	    "UXMAX3  " "UYMIN3  " "UYMAX3  " "UZMIN3  " "UZMAX3  " "VXORG3  " 
	    "VYORG3  " "VZORG3  " "SIMFAC3 ";
    static real rx[73] = { 5.f,5.f,.003f,.7f,.3f,2.f,.01f,.001f,1.25f,30.f,
	    .5f,.99f,1.f,.33f,.01f,5.f,20.f,0.f,1.f,0.f,1.f,0.f,1.f,0.f,1.f,
	    0.f,0.f,0.f,90.f,0.f,0.f,0.f,0.f,-180.f,180.f,-90.f,90.f,0.f,1.f,
	    0.f,1.f,0.f,1.f,.01f,1.f,1.f,0.f,30.f,0.f,0.f,.5f,.5f,0.f,-.8f,
	    -1.5f,1.5f,0.f,0.f,1.f,0.f,1.f,0.f,1.f,0.f,1.f,0.f,1.f,0.f,1.f,
	    0.f,0.f,0.f,0.f };
    static char cparal[40*73] = "INTERPOLATION_ANGLE_X                   " 
	    "INTERPOLATION_ANGLE_Y                   " "LINE_BIT_LENGTH     "
	    "                    " "SCRIPT_HEIGHT                           " 
	    "SCRIPT_SHIFT                            " "POLIMARKER_HEIGHT   "
	    "                    " "****RFAROT                              " 
	    "TONE_CYCLE_LENGTH                       " "LINE_LABEL_SPACE    "
	    "                    " "LINE_CYCLE_LENGTH                       " 
	    "LINE_START_POSITION                     " "RESUME_POSITION_AFTE"
	    "R_BUFFERING         " "LINE_LABEL_THRESHOLD_CURVATURE          " 
	    "ARROWHEAD_PROPOTION                     " "ARROWHEAD_SIZE      "
	    "                    " "ARROWHEAD_SIZE_MAP                      " 
	    "ARROWHEAD_ANGLE                         " "++++VXMIN           "
	    "                    " "++++VXMAX                               " 
	    "++++VYMIN                               " "++++VYMAX           "
	    "                    " "++++UXMIN                               " 
	    "++++UXMAX                               " "++++UYMIN           "
	    "                    " "++++UYMAX                               " 
	    "++++VXOFF                               " "++++VYOFF           "
	    "                    " "++++PLX                                 " 
	    "++++PLY                                 " "++++PLROT           "
	    "                    " "++++SIMFAC                              " 
	    "++++STLAT1                              " "++++STLAT2          "
	    "                    " "++++TXMIN                               " 
	    "++++TXMAX                               " "++++TYMIN           "
	    "                    " "++++TYMAX                               " 
	    "++++CXMIN                               " "++++CXMAX           "
	    "                    " "++++CYMIN                               " 
	    "++++CYMAX                               " "SATELLITE_ALTITUDE  "
	    "                    " "****FACTOR                              " 
	    "CORNERMARK_LENGTH                       " "****XPAD            "
	    "                    " "****YPAD                                " 
	    "++++TILT3                               " "++++ANGLE3          "
	    "                    " "++++XOFF3                               " 
	    "++++YOFF3                               " "++++XOBJ3           "
	    "                    " "++++YOBJ3                               " 
	    "++++ZOBJ3                               " "++++XEYE3           "
	    "                    " "++++YEYE3                               " 
	    "++++ZEYE3                               " "++++SEC3            "
	    "                    " "++++VXMIN3                              " 
	    "++++VXMAX3                              " "++++VYMIN3          "
	    "                    " "++++VYMAX3                              " 
	    "++++VZMIN3                              " "++++VZMAX3          "
	    "                    " "++++UXMIN3                              " 
	    "++++UXMAX3                              " "++++UYMIN3          "
	    "                    " "++++UYMAX3                              " 
	    "++++UZMIN3                              " "++++UZMAX3          "
	    "                    " "++++VXORG3                              " 
	    "++++VYORG3                              " "++++VZORG3          "
	    "                    " "++++SIMFAC3                             ";
    static logical lfirst = TRUE_;

    /* System generated locals */
    address a__1[3];
    integer i__1[3];

    /* Builtin functions */
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen),
	     s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer n;
    extern integer lenc_(char *, ftnlen);
    static char cmsg[80];
    extern logical lchreq_(char *, char *, ftnlen, ftnlen);
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), rlrget_(char *, real *, integer *, ftnlen), 
	    rtrget_(char *, char *, real *, integer *, ftnlen, ftnlen);

/*     / SHORT NAME / */
    switch(n__) {
	case 1: goto L_sgrqid;
	case 2: goto L_sgrqcp;
	case 3: goto L_sgrqcl;
	case 4: goto L_sgrqvl;
	case 5: goto L_sgrsvl;
	case 6: goto L_sgrqin;
	}

/*     / LONG  NAME / */
    *ncp = 73;
    return 0;
/* ----------------------------------------------------------------------- */

L_sgrqid:
    for (n = 1; n <= 73; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *idx = n;
	    return 0;
	}
/* L10: */
    }
/* Writing concatenation */
    i__1[0] = 11, a__1[0] = "PARAMETER '";
    i__1[1] = lenc_(cp, cp_len), a__1[1] = cp;
    i__1[2] = 17, a__1[2] = "' IS NOT DEFINED.";
    s_cat(cmsg, a__1, i__1, &c__3, (ftnlen)80);
    msgdmp_("E", "SGRQID", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
    return 0;
/* ----------------------------------------------------------------------- */

L_sgrqcp:
    if (1 <= *idx && *idx <= 73) {
	s_copy(cp, cparas + (*idx - 1 << 3), cp_len, (ftnlen)8);
    } else {
	msgdmp_("E", "SGRQCP", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_sgrqcl:
    if (1 <= *idx && *idx <= 73) {
	s_copy(cp, cparal + (*idx - 1) * 40, cp_len, (ftnlen)40);
    } else {
	msgdmp_("E", "SGRQCL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_sgrqvl:
    if (lfirst) {
	rtrget_("SG", cparas, rx, &c__73, (ftnlen)2, (ftnlen)8);
	rlrget_(cparal, rx, &c__73, (ftnlen)40);
	lfirst = FALSE_;
    }
    if (1 <= *idx && *idx <= 73) {
	*rpara = rx[*idx - 1];
    } else {
	msgdmp_("E", "SGRQVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_sgrsvl:
    if (lfirst) {
	rtrget_("SG", cparas, rx, &c__73, (ftnlen)2, (ftnlen)8);
	rlrget_(cparal, rx, &c__73, (ftnlen)40);
	lfirst = FALSE_;
    }
    if (1 <= *idx && *idx <= 73) {
	rx[*idx - 1] = *rpara;
    } else {
	msgdmp_("E", "SGRSVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_sgrqin:
    for (n = 1; n <= 73; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *in = n;
	    return 0;
	}
/* L20: */
    }
    *in = 0;
    return 0;
} /* sgrqnp_ */

/* Subroutine */ int sgrqnp_(integer *ncp)
{
    return sgrqnp_0_(0, ncp, (char *)0, (integer *)0, (real *)0, (integer *)0,
	     (ftnint)0);
    }

/* Subroutine */ int sgrqid_(char *cp, integer *idx, ftnlen cp_len)
{
    return sgrqnp_0_(1, (integer *)0, cp, idx, (real *)0, (integer *)0, 
	    cp_len);
    }

/* Subroutine */ int sgrqcp_(integer *idx, char *cp, ftnlen cp_len)
{
    return sgrqnp_0_(2, (integer *)0, cp, idx, (real *)0, (integer *)0, 
	    cp_len);
    }

/* Subroutine */ int sgrqcl_(integer *idx, char *cp, ftnlen cp_len)
{
    return sgrqnp_0_(3, (integer *)0, cp, idx, (real *)0, (integer *)0, 
	    cp_len);
    }

/* Subroutine */ int sgrqvl_(integer *idx, real *rpara)
{
    return sgrqnp_0_(4, (integer *)0, (char *)0, idx, rpara, (integer *)0, (
	    ftnint)0);
    }

/* Subroutine */ int sgrsvl_(integer *idx, real *rpara)
{
    return sgrqnp_0_(5, (integer *)0, (char *)0, idx, rpara, (integer *)0, (
	    ftnint)0);
    }

/* Subroutine */ int sgrqin_(char *cp, integer *in, ftnlen cp_len)
{
    return sgrqnp_0_(6, (integer *)0, cp, (integer *)0, (real *)0, in, cp_len)
	    ;
    }


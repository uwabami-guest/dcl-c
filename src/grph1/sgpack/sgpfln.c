/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int sgpfln_(void)
{
    /* System generated locals */
    integer i__1, i__2;
    icilist ici__1;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_wsfi(icilist *), do_fio(integer *, char *, ftnlen), e_wsfi(void)
	    , s_wsfe(cilist *), e_wsfe(void);

    /* Local variables */
    static integer i__, n, nc, iu, nx;
    extern integer lenc_(char *, ftnlen);
    static char cfln[8], cfnm[8], line[100];
    extern /* Subroutine */ int gliget_(char *, integer *, ftnlen), swcget_(
	    char *, char *, ftnlen, ftnlen);
    static integer maxfnu;
    extern /* Subroutine */ int swiget_(char *, integer *, ftnlen);

    /* Fortran I/O blocks */
    static icilist io___7 = { 0, cfnm, 0, "(A6,I2.2)", 8, 1 };
    static cilist io___11 = { 0, 0, 0, "(A)", 0 };


    gliget_("MSGUNIT", &iu, (ftnlen)7);
    swiget_("MAXFNU", &maxfnu, (ftnlen)6);
    n = 0;
    s_copy(line, " ", (ftnlen)100, (ftnlen)1);
    i__1 = maxfnu;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_wsfi(&io___7);
	do_fio(&c__1, "FLNAME", (ftnlen)6);
	do_fio(&c__1, (char *)&i__, (ftnlen)sizeof(integer));
	e_wsfi();
	swcget_(cfnm, cfln, (ftnlen)8, (ftnlen)8);
	nc = lenc_(cfln, (ftnlen)8);
	if (i__ < 10) {
	    nx = nc + 4;
	    i__2 = n;
	    ici__1.icierr = 0;
	    ici__1.icirnum = 1;
	    ici__1.icirlen = n + nx - i__2;
	    ici__1.iciunit = line + i__2;
	    ici__1.icifmt = "(TR1,I1,A1,A,A1)";
	    s_wsfi(&ici__1);
	    do_fio(&c__1, (char *)&i__, (ftnlen)sizeof(integer));
	    do_fio(&c__1, ":", (ftnlen)1);
	    do_fio(&c__1, cfln, nc);
	    do_fio(&c__1, ",", (ftnlen)1);
	    e_wsfi();
	} else {
	    nx = nc + 5;
	    i__2 = n;
	    ici__1.icierr = 0;
	    ici__1.icirnum = 1;
	    ici__1.icirlen = n + nx - i__2;
	    ici__1.iciunit = line + i__2;
	    ici__1.icifmt = "(TR1,I2,A1,A,A1)";
	    s_wsfi(&ici__1);
	    do_fio(&c__1, (char *)&i__, (ftnlen)sizeof(integer));
	    do_fio(&c__1, ":", (ftnlen)1);
	    do_fio(&c__1, cfln, nc);
	    do_fio(&c__1, ",", (ftnlen)1);
	    e_wsfi();
	}
	n += nx;
/* L10: */
    }
    ici__1.icierr = 0;
    ici__1.icirnum = 1;
    ici__1.icirlen = 2;
    ici__1.iciunit = line + (n - 1);
    ici__1.icifmt = "(A2)";
    s_wsfi(&ici__1);
    do_fio(&c__1, " ;", (ftnlen)2);
    e_wsfi();
    io___11.ciunit = iu;
    s_wsfe(&io___11);
    do_fio(&c__1, line, n + 1);
    e_wsfe();
    return 0;
} /* sgpfln_ */


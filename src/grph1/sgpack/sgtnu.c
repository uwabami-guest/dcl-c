/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     TONE PRIMITIVE */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int sgtnu_0_(int n__, integer *n, real *upx, real *upy, real 
	*vpx, real *vpy, real *rpx, real *rpy, integer *itpat)
{
    /* Initialized data */

    static integer itpatz = 1;

    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), sztncl_(void), sztnop_(integer *), sztnzr_(
	    integer *, real *, real *), sztnzu_(integer *, real *, real *), 
	    sztnzv_(integer *, real *, real *);

    /* Parameter adjustments */
    if (upx) {
	--upx;
	}
    if (upy) {
	--upy;
	}
    if (vpx) {
	--vpx;
	}
    if (vpy) {
	--vpy;
	}
    if (rpx) {
	--rpx;
	}
    if (rpy) {
	--rpy;
	}

    /* Function Body */
    switch(n__) {
	case 1: goto L_sgtnv;
	case 2: goto L_sgtnr;
	case 3: goto L_sgstnp;
	case 4: goto L_sgqtnp;
	}

    if (*n < 3) {
	msgdmp_("E", "SGTNU", "NUMBER OF POINTS IS LESS THAN 3.", (ftnlen)1, (
		ftnlen)5, (ftnlen)32);
    }
    if (itpatz == 0) {
	msgdmp_("M", "SGTNU", "TONE PAT. INDEX IS 0 / DO NOTHING.", (ftnlen)1,
		 (ftnlen)5, (ftnlen)34);
	return 0;
    }
    if (itpatz < 0) {
	msgdmp_("E", "SGTNU", "TONE PAT. INDEX IS LESS THAN 0.", (ftnlen)1, (
		ftnlen)5, (ftnlen)31);
    }
    sztnop_(&itpatz);
    sztnzu_(n, &upx[1], &upy[1]);
    sztncl_();
    return 0;
/* ----------------------------------------------------------------------- */

L_sgtnv:
    if (*n < 3) {
	msgdmp_("E", "SGTNV", "NUMBER OF POINTS IS LESS THAN 3.", (ftnlen)1, (
		ftnlen)5, (ftnlen)32);
    }
    if (itpatz == 0) {
	msgdmp_("M", "SGTNV", "TONE PAT. INDEX IS 0 / DO NOTHING.", (ftnlen)1,
		 (ftnlen)5, (ftnlen)34);
	return 0;
    }
    if (itpatz < 0) {
	msgdmp_("E", "SGTNV", "TONE PAT. INDEX IS LESS THAN 0.", (ftnlen)1, (
		ftnlen)5, (ftnlen)31);
    }
    sztnop_(&itpatz);
    sztnzv_(n, &vpx[1], &vpy[1]);
    sztncl_();
    return 0;
/* ----------------------------------------------------------------------- */

L_sgtnr:
    if (*n < 3) {
	msgdmp_("E", "SGTNR", "NUMBER OF POINTS IS LESS THAN 3.", (ftnlen)1, (
		ftnlen)5, (ftnlen)32);
    }
    if (itpatz == 0) {
	msgdmp_("M", "SGTNR", "TONE PAT. INDEX IS 0 / DO NOTHING.", (ftnlen)1,
		 (ftnlen)5, (ftnlen)34);
	return 0;
    }
    if (itpatz < 0) {
	msgdmp_("E", "SGTNR", "TONE PAT. INDEX IS LESS THAN 0.", (ftnlen)1, (
		ftnlen)5, (ftnlen)31);
    }
    sztnop_(&itpatz);
    sztnzr_(n, &rpx[1], &rpy[1]);
    sztncl_();
    return 0;
/* ----------------------------------------------------------------------- */

L_sgstnp:
    itpatz = *itpat;
    return 0;
/* ----------------------------------------------------------------------- */

L_sgqtnp:
    *itpat = itpatz;
    return 0;
} /* sgtnu_ */

/* Subroutine */ int sgtnu_(integer *n, real *upx, real *upy)
{
    return sgtnu_0_(0, n, upx, upy, (real *)0, (real *)0, (real *)0, (real *)
	    0, (integer *)0);
    }

/* Subroutine */ int sgtnv_(integer *n, real *vpx, real *vpy)
{
    return sgtnu_0_(1, n, (real *)0, (real *)0, vpx, vpy, (real *)0, (real *)
	    0, (integer *)0);
    }

/* Subroutine */ int sgtnr_(integer *n, real *rpx, real *rpy)
{
    return sgtnu_0_(2, n, (real *)0, (real *)0, (real *)0, (real *)0, rpx, 
	    rpy, (integer *)0);
    }

/* Subroutine */ int sgstnp_(integer *itpat)
{
    return sgtnu_0_(3, (integer *)0, (real *)0, (real *)0, (real *)0, (real *)
	    0, (real *)0, (real *)0, itpat);
    }

/* Subroutine */ int sgqtnp_(integer *itpat)
{
    return sgtnu_0_(4, (integer *)0, (real *)0, (real *)0, (real *)0, (real *)
	    0, (real *)0, (real *)0, itpat);
    }


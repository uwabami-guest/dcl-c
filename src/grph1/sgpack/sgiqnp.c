/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__3 = 3;
static integer c__24 = 24;

/* ----------------------------------------------------------------------- */
/*     INTEGER PARAMATER CONTROL */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int sgiqnp_0_(int n__, integer *ncp, char *cp, integer *idx, 
	integer *ipara, integer *in, ftnlen cp_len)
{
    /* Initialized data */

    static char cparas[8*24] = "IRMODE  " "MOVE    " "NBITS   " "ISUP    " 
	    "ISUB    " "IRST    " "NPMSKIP " "IFONT   " "ICLRMAP " "IROT    " 
	    "INDEXC  " "NBUFF   " "IATONE  " "IWS     " "ITR     " "IBGCLI  " 
	    "NFRAME  " "NPAGE   " "NLEVEL  " "INDEX   " "ITR3    " "IXC3    " 
	    "IYC3    " "IFCIDX  ";
    static integer ix[24] = { 0,1,16,124,95,34,1,1,1,0,0,200,999,0,1,999,0,0,
	    0,1,1,1,2,998 };
    static char cparal[40*24] = "BOUNDARY_DIRECTION                      " 
	    "LINE_RESUME_MODE                        " "PATTERN_BIT_LENGTH  "
	    "                    " "BEGIN_SUPERSCRIPT                       " 
	    "BEGIN_SUBSCRIPT                         " "END_SCRIPT          "
	    "                    " "POLIMARKER_INTERVAL                     " 
	    "FONT_NUMBER                             " "COLORMAP_NUMBER     "
	    "                    " "LINE_LABEL_ROTATION_ANGLE               " 
	    "LABEL_CHAR_INDEX                        " "LINE_BUFFERING_LENGT"
	    "H                   " "ARROWHEAD_SHADE_PATTERN                 " 
	    "++++IWS                                 " "++++ITR             "
	    "                    " "BACKGROUND_COLOR_INDEX                  " 
	    "****NFRAME                              " "****NPAGE           "
	    "                    " "****NLEVEL                              " 
	    "CORNERMARK_INDEX                        " "++++ITR3            "
	    "                    " "++++IXC3                                " 
	    "++++IYC3                                " "FULLCOLOR_INDEX     "
	    "                    ";
    static logical lfirst = TRUE_;

    /* System generated locals */
    address a__1[3];
    integer i__1[3];

    /* Builtin functions */
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen),
	     s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer n;
    extern integer lenc_(char *, ftnlen);
    static char cmsg[80];
    extern logical lchreq_(char *, char *, ftnlen, ftnlen);
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), rliget_(char *, integer *, integer *, ftnlen), 
	    rtiget_(char *, char *, integer *, integer *, ftnlen, ftnlen);

/*     / SHORT NAME / */
    switch(n__) {
	case 1: goto L_sgiqid;
	case 2: goto L_sgiqcp;
	case 3: goto L_sgiqcl;
	case 4: goto L_sgiqvl;
	case 5: goto L_sgisvl;
	case 6: goto L_sgiqin;
	}

/*     / LONG  NAME / */
    *ncp = 24;
    return 0;
/* ----------------------------------------------------------------------- */

L_sgiqid:
    for (n = 1; n <= 24; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *idx = n;
	    return 0;
	}
/* L10: */
    }
/* Writing concatenation */
    i__1[0] = 11, a__1[0] = "PARAMETER '";
    i__1[1] = lenc_(cp, cp_len), a__1[1] = cp;
    i__1[2] = 17, a__1[2] = "' IS NOT DEFINED.";
    s_cat(cmsg, a__1, i__1, &c__3, (ftnlen)80);
    msgdmp_("E", "SGIQID", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
    return 0;
/* ----------------------------------------------------------------------- */

L_sgiqcp:
    if (1 <= *idx && *idx <= 24) {
	s_copy(cp, cparas + (*idx - 1 << 3), cp_len, (ftnlen)8);
    } else {
	msgdmp_("E", "SGIQCP", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_sgiqcl:
    if (1 <= *idx && *idx <= 24) {
	s_copy(cp, cparal + (*idx - 1) * 40, cp_len, (ftnlen)40);
    } else {
	msgdmp_("E", "SGIQCL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_sgiqvl:
    if (lfirst) {
	rtiget_("SG", cparas, ix, &c__24, (ftnlen)2, (ftnlen)8);
	rliget_(cparal, ix, &c__24, (ftnlen)40);
	lfirst = FALSE_;
    }
    if (1 <= *idx && *idx <= 24) {
	*ipara = ix[*idx - 1];
    } else {
	msgdmp_("E", "SGIQVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_sgisvl:
    if (lfirst) {
	rtiget_("SG", cparas, ix, &c__24, (ftnlen)2, (ftnlen)8);
	rliget_(cparal, ix, &c__24, (ftnlen)40);
	lfirst = FALSE_;
    }
    if (1 <= *idx && *idx <= 24) {
	ix[*idx - 1] = *ipara;
    } else {
	msgdmp_("E", "SGISVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_sgiqin:
    for (n = 1; n <= 24; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *in = n;
	    return 0;
	}
/* L20: */
    }
    *in = 0;
    return 0;
} /* sgiqnp_ */

/* Subroutine */ int sgiqnp_(integer *ncp)
{
    return sgiqnp_0_(0, ncp, (char *)0, (integer *)0, (integer *)0, (integer *
	    )0, (ftnint)0);
    }

/* Subroutine */ int sgiqid_(char *cp, integer *idx, ftnlen cp_len)
{
    return sgiqnp_0_(1, (integer *)0, cp, idx, (integer *)0, (integer *)0, 
	    cp_len);
    }

/* Subroutine */ int sgiqcp_(integer *idx, char *cp, ftnlen cp_len)
{
    return sgiqnp_0_(2, (integer *)0, cp, idx, (integer *)0, (integer *)0, 
	    cp_len);
    }

/* Subroutine */ int sgiqcl_(integer *idx, char *cp, ftnlen cp_len)
{
    return sgiqnp_0_(3, (integer *)0, cp, idx, (integer *)0, (integer *)0, 
	    cp_len);
    }

/* Subroutine */ int sgiqvl_(integer *idx, integer *ipara)
{
    return sgiqnp_0_(4, (integer *)0, (char *)0, idx, ipara, (integer *)0, (
	    ftnint)0);
    }

/* Subroutine */ int sgisvl_(integer *idx, integer *ipara)
{
    return sgiqnp_0_(5, (integer *)0, (char *)0, idx, ipara, (integer *)0, (
	    ftnint)0);
    }

/* Subroutine */ int sgiqin_(char *cp, integer *in, ftnlen cp_len)
{
    return sgiqnp_0_(6, (integer *)0, cp, (integer *)0, (integer *)0, in, 
	    cp_len);
    }


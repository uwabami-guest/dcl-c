/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__100 = 100;
static integer c__1 = 1;
static integer c__65535 = 65535;
static integer c__0 = 0;
static integer c__56797 = 56797;
static integer c__41120 = 41120;
static integer c__32896 = 32896;
static integer c__49344 = 49344;
static integer c__52171 = 52171;

/* ----------------------------------------------------------------------- */
/*     PROGRAM FOR GENERATING COLORMAP */

/*       WRITTEN BY K. ISHIOKA, MODIFIED BY M. SHIOTANI (94/05/07) */

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Format strings */
    static char fmt_100[] = "(i3,a15)";
    static char fmt_200[] = "(3i6,a6)";

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_wsfe(cilist *), do_fio(integer *, char *, ftnlen), e_wsfe(void),
	     s_wsfi(icilist *), e_wsfi(void);
    /* Subroutine */ int s_stop(char *, ftnlen);

    /* Local variables */
    static integer i__, ib, ig, ir;
    static char ccolor[6];

    /* Fortran I/O blocks */
    static cilist io___2 = { 0, 6, 0, fmt_100, 0 };
    static cilist io___3 = { 0, 6, 0, fmt_200, 0 };
    static cilist io___4 = { 0, 6, 0, fmt_200, 0 };
    static cilist io___5 = { 0, 6, 0, fmt_200, 0 };
    static cilist io___6 = { 0, 6, 0, fmt_200, 0 };
    static cilist io___7 = { 0, 6, 0, fmt_200, 0 };
    static cilist io___8 = { 0, 6, 0, fmt_200, 0 };
    static cilist io___9 = { 0, 6, 0, fmt_200, 0 };
    static cilist io___10 = { 0, 6, 0, fmt_200, 0 };
    static cilist io___11 = { 0, 6, 0, fmt_200, 0 };
    static cilist io___12 = { 0, 6, 0, fmt_200, 0 };
    static icilist io___14 = { 0, ccolor+4, 0, "(I2)", 2, 1 };
    static cilist io___18 = { 0, 6, 0, fmt_200, 0 };
    static cilist io___19 = { 0, 6, 0, fmt_200, 0 };
    static cilist io___20 = { 0, 6, 0, fmt_200, 0 };
    static cilist io___21 = { 0, 6, 0, fmt_200, 0 };
    static cilist io___22 = { 0, 6, 0, fmt_200, 0 };
    static cilist io___23 = { 0, 6, 0, fmt_200, 0 };


    s_copy(ccolor, " : C**", (ftnlen)6, (ftnlen)6);
/* L100: */
/* L200: */
    s_wsfe(&io___2);
    do_fio(&c__1, (char *)&c__100, (ftnlen)sizeof(integer));
    do_fio(&c__1, ": NO._OF_COLORS", (ftnlen)15);
    e_wsfe();
    s_wsfe(&io___3);
    do_fio(&c__1, (char *)&c__65535, (ftnlen)sizeof(integer));
    do_fio(&c__1, (char *)&c__65535, (ftnlen)sizeof(integer));
    do_fio(&c__1, (char *)&c__65535, (ftnlen)sizeof(integer));
    do_fio(&c__1, " : C00", (ftnlen)6);
    e_wsfe();
    s_wsfe(&io___4);
    do_fio(&c__1, (char *)&c__0, (ftnlen)sizeof(integer));
    do_fio(&c__1, (char *)&c__0, (ftnlen)sizeof(integer));
    do_fio(&c__1, (char *)&c__0, (ftnlen)sizeof(integer));
    do_fio(&c__1, " : C01", (ftnlen)6);
    e_wsfe();
    s_wsfe(&io___5);
    do_fio(&c__1, (char *)&c__65535, (ftnlen)sizeof(integer));
    do_fio(&c__1, (char *)&c__0, (ftnlen)sizeof(integer));
    do_fio(&c__1, (char *)&c__0, (ftnlen)sizeof(integer));
    do_fio(&c__1, " : C02", (ftnlen)6);
    e_wsfe();
    s_wsfe(&io___6);
    do_fio(&c__1, (char *)&c__0, (ftnlen)sizeof(integer));
    do_fio(&c__1, (char *)&c__65535, (ftnlen)sizeof(integer));
    do_fio(&c__1, (char *)&c__0, (ftnlen)sizeof(integer));
    do_fio(&c__1, " : C03", (ftnlen)6);
    e_wsfe();
    s_wsfe(&io___7);
    do_fio(&c__1, (char *)&c__0, (ftnlen)sizeof(integer));
    do_fio(&c__1, (char *)&c__0, (ftnlen)sizeof(integer));
    do_fio(&c__1, (char *)&c__65535, (ftnlen)sizeof(integer));
    do_fio(&c__1, " : C04", (ftnlen)6);
    e_wsfe();
    s_wsfe(&io___8);
    do_fio(&c__1, (char *)&c__65535, (ftnlen)sizeof(integer));
    do_fio(&c__1, (char *)&c__65535, (ftnlen)sizeof(integer));
    do_fio(&c__1, (char *)&c__0, (ftnlen)sizeof(integer));
    do_fio(&c__1, " : C05", (ftnlen)6);
    e_wsfe();
    s_wsfe(&io___9);
    do_fio(&c__1, (char *)&c__56797, (ftnlen)sizeof(integer));
    do_fio(&c__1, (char *)&c__41120, (ftnlen)sizeof(integer));
    do_fio(&c__1, (char *)&c__56797, (ftnlen)sizeof(integer));
    do_fio(&c__1, " : C06", (ftnlen)6);
    e_wsfe();
    s_wsfe(&io___10);
    do_fio(&c__1, (char *)&c__0, (ftnlen)sizeof(integer));
    do_fio(&c__1, (char *)&c__0, (ftnlen)sizeof(integer));
    do_fio(&c__1, (char *)&c__32896, (ftnlen)sizeof(integer));
    do_fio(&c__1, " : C07", (ftnlen)6);
    e_wsfe();
    s_wsfe(&io___11);
    do_fio(&c__1, (char *)&c__65535, (ftnlen)sizeof(integer));
    do_fio(&c__1, (char *)&c__49344, (ftnlen)sizeof(integer));
    do_fio(&c__1, (char *)&c__52171, (ftnlen)sizeof(integer));
    do_fio(&c__1, " : C08", (ftnlen)6);
    e_wsfe();
    s_wsfe(&io___12);
    do_fio(&c__1, (char *)&c__0, (ftnlen)sizeof(integer));
    do_fio(&c__1, (char *)&c__65535, (ftnlen)sizeof(integer));
    do_fio(&c__1, (char *)&c__65535, (ftnlen)sizeof(integer));
    do_fio(&c__1, " : C09", (ftnlen)6);
    e_wsfe();
    for (i__ = 10; i__ <= 99; ++i__) {
	s_wsfi(&io___14);
	do_fio(&c__1, (char *)&i__, (ftnlen)sizeof(integer));
	e_wsfi();
	if (i__ <= 25) {
	    ir = 0;
	    ig = 0;
	    ib = (i__ - 10) * 4369;
	    s_wsfe(&io___18);
	    do_fio(&c__1, (char *)&ir, (ftnlen)sizeof(integer));
	    do_fio(&c__1, (char *)&ig, (ftnlen)sizeof(integer));
	    do_fio(&c__1, (char *)&ib, (ftnlen)sizeof(integer));
	    do_fio(&c__1, ccolor, (ftnlen)6);
	    e_wsfe();
	} else if (i__ <= 40) {
	    ir = 0;
	    ig = (i__ - 25) * 4369;
	    ib = 65535;
	    s_wsfe(&io___19);
	    do_fio(&c__1, (char *)&ir, (ftnlen)sizeof(integer));
	    do_fio(&c__1, (char *)&ig, (ftnlen)sizeof(integer));
	    do_fio(&c__1, (char *)&ib, (ftnlen)sizeof(integer));
	    do_fio(&c__1, ccolor, (ftnlen)6);
	    e_wsfe();
	} else if (i__ <= 55) {
	    ir = 0;
	    ig = 65535;
	    ib = (55 - i__) * 4369;
	    s_wsfe(&io___20);
	    do_fio(&c__1, (char *)&ir, (ftnlen)sizeof(integer));
	    do_fio(&c__1, (char *)&ig, (ftnlen)sizeof(integer));
	    do_fio(&c__1, (char *)&ib, (ftnlen)sizeof(integer));
	    do_fio(&c__1, ccolor, (ftnlen)6);
	    e_wsfe();
	} else if (i__ <= 70) {
	    ir = (i__ - 55) * 4369;
	    ig = 65535;
	    ib = 0;
	    s_wsfe(&io___21);
	    do_fio(&c__1, (char *)&ir, (ftnlen)sizeof(integer));
	    do_fio(&c__1, (char *)&ig, (ftnlen)sizeof(integer));
	    do_fio(&c__1, (char *)&ib, (ftnlen)sizeof(integer));
	    do_fio(&c__1, ccolor, (ftnlen)6);
	    e_wsfe();
	} else if (i__ <= 85) {
	    ir = 65535;
	    ig = (85 - i__) * 4369;
	    ib = 0;
	    s_wsfe(&io___22);
	    do_fio(&c__1, (char *)&ir, (ftnlen)sizeof(integer));
	    do_fio(&c__1, (char *)&ig, (ftnlen)sizeof(integer));
	    do_fio(&c__1, (char *)&ib, (ftnlen)sizeof(integer));
	    do_fio(&c__1, ccolor, (ftnlen)6);
	    e_wsfe();
	} else if (i__ <= 99) {
	    ir = 65535;
	    ig = (i__ - 85) * 4369;
	    ib = (i__ - 85) * 4369;
	    s_wsfe(&io___23);
	    do_fio(&c__1, (char *)&ir, (ftnlen)sizeof(integer));
	    do_fio(&c__1, (char *)&ig, (ftnlen)sizeof(integer));
	    do_fio(&c__1, (char *)&ib, (ftnlen)sizeof(integer));
	    do_fio(&c__1, ccolor, (ftnlen)6);
	    e_wsfe();
	}
/* L10: */
    }
    s_stop("", (ftnlen)0);
    return 0;
} /* MAIN__ */

/* Main program alias */ int cmap01_ () { MAIN__ (); return 0; }

/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__8 = 8;
static integer c_n64 = -64;
static integer c__256 = 256;
static integer c__6000 = 6000;

/* ----------------------------------------------------------------------- */
/*     PROGRAM FOR FONT FILE CONVERSION                       (95/05/31) */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Format strings */
    static char fmt_100[] = "(i5,3x,8(i3,1x,i3,1x))";

    /* System generated locals */
    integer i__1;
    olist o__1;
    cllist cl__1;
    alist al__1;

    /* Builtin functions */
    integer s_rsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_rsle(void), f_open(olist *), f_rew(alist *), s_rsfe(cilist *), 
	    do_fio(integer *, char *, ftnlen), e_rsfe(void), s_wsue(cilist *),
	     do_uio(integer *, char *, ftnlen), e_wsue(void), f_clos(cllist *)
	    ;

    /* Local variables */
    static integer i__, j, id, jd, nc, nl, kx[8], ky[8], jdl, ipc;
    static char ckx[1*6000], cky[1*6000];
    static logical lend;
    static char cdsni[64], cdsno[64];
    static integer iplst, iposx[256];
    extern integer indxif_(integer *, integer *, integer *, integer *);

    /* Fortran I/O blocks */
    static cilist io___1 = { 0, 5, 0, 0, 0 };
    static cilist io___7 = { 0, 11, 0, fmt_100, 0 };
    static cilist io___20 = { 0, 21, 0, 0, 0 };
    static cilist io___21 = { 0, 21, 0, 0, 0 };
    static cilist io___22 = { 0, 21, 0, 0, 0 };


/*     / READ FONT FILE NAMES / */
    s_rsle(&io___1);
    do_lio(&c__9, &c__1, cdsni, (ftnlen)64);
    do_lio(&c__9, &c__1, cdsno, (ftnlen)64);
    e_rsle();
/*     / OPEN FONT FILES / */
    o__1.oerr = 0;
    o__1.ounit = 11;
    o__1.ofnmlen = 64;
    o__1.ofnm = cdsni;
    o__1.orl = 0;
    o__1.osta = "OLD";
    o__1.oacc = 0;
    o__1.ofm = "FORMATTED";
    o__1.oblnk = 0;
    f_open(&o__1);
    al__1.aerr = 0;
    al__1.aunit = 11;
    f_rew(&al__1);
    o__1.oerr = 0;
    o__1.ounit = 21;
    o__1.ofnmlen = 64;
    o__1.ofnm = cdsno;
    o__1.orl = 0;
    o__1.osta = "NEW";
    o__1.oacc = 0;
    o__1.ofm = "UNFORMATTED";
    o__1.oblnk = 0;
    f_open(&o__1);
    al__1.aerr = 0;
    al__1.aunit = 21;
    f_rew(&al__1);
/*     / LOAD TEXT FONT / */
    nc = 0;
    iplst = 0;
L30:
    ++nc;
    nl = 0;
L20:
/*         / READ FONT DATA / */
    ++nl;
    s_rsfe(&io___7);
    do_fio(&c__1, (char *)&id, (ftnlen)sizeof(integer));
    for (i__ = 1; i__ <= 8; ++i__) {
	do_fio(&c__1, (char *)&kx[i__ - 1], (ftnlen)sizeof(integer));
	do_fio(&c__1, (char *)&ky[i__ - 1], (ftnlen)sizeof(integer));
    }
    e_rsfe();
/*         / CHECK END OF LINE / */
    jd = indxif_(ky, &c__8, &c__1, &c_n64);
    if (jd == 0) {
	jdl = 8;
    } else {
	jdl = jd;
    }
/*         / PACK IN CHARACTER / */
    i__1 = jdl;
    for (j = 1; j <= i__1; ++j) {
	ipc = (nl - 1 << 3) + j + iplst;
	*(unsigned char *)&ckx[ipc - 1] = (char) (kx[j - 1] + 64);
	*(unsigned char *)&cky[ipc - 1] = (char) (ky[j - 1] + 64);
/* L10: */
    }
/*         / CHECK END OF SEGMENT / */
    if (jd != 0) {
	if (kx[jd - 1] == -64) {
	    lend = TRUE_;
	    iposx[nc - 1] = iplst + 1;
	    iplst = (nl - 1 << 3) + jd + iplst;
	} else {
	    lend = FALSE_;
	}
    } else {
	lend = FALSE_;
    }
    if (! lend) {
	goto L20;
    }
    if (! (nc == 256)) {
	goto L30;
    }
/*     / WRITE UNFORMATTED FILE / */
    s_wsue(&io___20);
    do_uio(&c__256, (char *)&iposx[0], (ftnlen)sizeof(integer));
    e_wsue();
    s_wsue(&io___21);
    do_uio(&c__6000, ckx, (ftnlen)1);
    e_wsue();
    s_wsue(&io___22);
    do_uio(&c__6000, cky, (ftnlen)1);
    e_wsue();
/*     / CLOSE FONT FILE / */
    cl__1.cerr = 0;
    cl__1.cunit = 11;
    cl__1.csta = 0;
    f_clos(&cl__1);
    cl__1.cerr = 0;
    cl__1.cunit = 21;
    cl__1.csta = 0;
    f_clos(&cl__1);
    return 0;
} /* MAIN__ */

/* ----------------------------------------------------------------------- */
/*     INDXIF */
/* ----------------------------------------------------------------------- */
integer indxif_(integer *ix, integer *n, integer *jd, integer *ii)
{
    /* System generated locals */
    integer ret_val, i__1;

    /* Local variables */
    static integer i__, j;

    /* Parameter adjustments */
    --ix;

    /* Function Body */
    ret_val = 0;
    i__1 = *n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	j = *jd * (i__ - 1) + 1;
	if (ix[j] == *ii) {
	    ret_val = i__;
	    return ret_val;
	}
/* L10: */
    }
    return ret_val;
} /* indxif_ */

/* Main program alias */ int main () { MAIN__ (); return 0; }

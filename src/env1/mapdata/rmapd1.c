/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     PROGRAM FOR MAPDATA SUBTRUCTION                        (94/05/07) */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* System generated locals */
    integer i__1;
    olist o__1;
    cllist cl__1;
    alist al__1;

    /* Builtin functions */
    integer s_rsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_rsle(void), f_open(olist *), f_rew(alist *), s_rsfe(cilist *), 
	    do_fio(integer *, char *, ftnlen), e_rsfe(void), s_wsfe(cilist *),
	     e_wsfe(void), f_clos(cllist *);

    /* Local variables */
    static integer i__, ios, npt1, npt2;
    static real ptx1[20000], pty1[20000], ptx2[20000], pty2[20000];
    static integer igid;
    static real flim[4];
    static integer npts;
    static char cdsni[64], cdsno[64];
    extern /* Subroutine */ int rmpsub_(real *, real *, integer *, real *, 
	    real *, integer *);

    /* Fortran I/O blocks */
    static cilist io___1 = { 0, 5, 0, 0, 0 };
    static cilist io___5 = { 1, 11, 1, "(2I8,4F8.3)", 0 };
    static cilist io___11 = { 0, 11, 0, "(10F8.3)", 0 };
    static cilist io___17 = { 0, 21, 0, "(2I8,4F8.3)", 0 };
    static cilist io___18 = { 0, 21, 0, "(10F8.3)", 0 };


    s_rsle(&io___1);
    do_lio(&c__9, &c__1, cdsni, (ftnlen)64);
    do_lio(&c__9, &c__1, cdsno, (ftnlen)64);
    e_rsle();
    o__1.oerr = 0;
    o__1.ounit = 11;
    o__1.ofnmlen = 64;
    o__1.ofnm = cdsni;
    o__1.orl = 0;
    o__1.osta = "OLD";
    o__1.oacc = 0;
    o__1.ofm = "FORMATTED";
    o__1.oblnk = 0;
    f_open(&o__1);
    al__1.aerr = 0;
    al__1.aunit = 11;
    f_rew(&al__1);
    o__1.oerr = 0;
    o__1.ounit = 21;
    o__1.ofnmlen = 64;
    o__1.ofnm = cdsno;
    o__1.orl = 0;
    o__1.osta = "NEW";
    o__1.oacc = 0;
    o__1.ofm = "FORMATTED";
    o__1.oblnk = 0;
    f_open(&o__1);
    al__1.aerr = 0;
    al__1.aunit = 21;
    f_rew(&al__1);
L10:
    ios = s_rsfe(&io___5);
    if (ios != 0) {
	goto L100001;
    }
    ios = do_fio(&c__1, (char *)&npts, (ftnlen)sizeof(integer));
    if (ios != 0) {
	goto L100001;
    }
    ios = do_fio(&c__1, (char *)&igid, (ftnlen)sizeof(integer));
    if (ios != 0) {
	goto L100001;
    }
    for (i__ = 1; i__ <= 4; ++i__) {
	ios = do_fio(&c__1, (char *)&flim[i__ - 1], (ftnlen)sizeof(real));
	if (ios != 0) {
	    goto L100001;
	}
    }
    ios = e_rsfe();
L100001:
    npt1 = npts / 2;
    if (ios == 0) {
	if (npt1 >= 1) {
	    s_rsfe(&io___11);
	    i__1 = npt1;
	    for (i__ = 1; i__ <= i__1; ++i__) {
		do_fio(&c__1, (char *)&ptx1[i__ - 1], (ftnlen)sizeof(real));
		do_fio(&c__1, (char *)&pty1[i__ - 1], (ftnlen)sizeof(real));
	    }
	    e_rsfe();
	}
	rmpsub_(ptx1, pty1, &npt1, ptx2, pty2, &npt2);
	rmpsub_(ptx2, pty2, &npt2, ptx1, pty1, &npt1);
	s_wsfe(&io___17);
	i__1 = npt1 << 1;
	do_fio(&c__1, (char *)&i__1, (ftnlen)sizeof(integer));
	do_fio(&c__1, (char *)&igid, (ftnlen)sizeof(integer));
	for (i__ = 1; i__ <= 4; ++i__) {
	    do_fio(&c__1, (char *)&flim[i__ - 1], (ftnlen)sizeof(real));
	}
	e_wsfe();
	s_wsfe(&io___18);
	i__1 = npt1;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    do_fio(&c__1, (char *)&ptx1[i__ - 1], (ftnlen)sizeof(real));
	    do_fio(&c__1, (char *)&pty1[i__ - 1], (ftnlen)sizeof(real));
	}
	e_wsfe();
    }
    if (ios == 0) {
	goto L10;
    }
    cl__1.cerr = 0;
    cl__1.cunit = 11;
    cl__1.csta = 0;
    f_clos(&cl__1);
    cl__1.cerr = 0;
    cl__1.cunit = 21;
    cl__1.csta = 0;
    f_clos(&cl__1);
    return 0;
} /* MAIN__ */

/* ----------------------------------------------------------------------- */
/* Subroutine */ int rmpsub_(real *ptx1, real *pty1, integer *npt1, real *
	ptx2, real *pty2, integer *npt2)
{
    /* System generated locals */
    real r__1, r__2, r__3, r__4;

    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void);
    /* Subroutine */ int s_stop(char *, ftnlen);
    double sqrt(doublereal);

    /* Local variables */
    static integer n0, n1, n2;
    static real cx;
    static integer nx;
    static real dx1, dx2, dy1, dy2;

    /* Fortran I/O blocks */
    static cilist io___19 = { 0, 6, 0, 0, 0 };


    /* Parameter adjustments */
    --pty2;
    --ptx2;
    --pty1;
    --ptx1;

    /* Function Body */
    if (*npt1 < 2) {
	s_wsle(&io___19);
	do_lio(&c__9, &c__1, "PMAPD1/RMPSUB - ERROR IN POINTS.", (ftnlen)32);
	e_wsle();
	s_stop("", (ftnlen)0);
    }
    ptx2[1] = ptx1[1];
    pty2[1] = pty1[1];
    if (*npt1 <= 2) {
	*npt2 = 2;
	ptx2[*npt2] = ptx1[*npt1];
	pty2[*npt2] = pty1[*npt1];
	return 0;
    }
    nx = 1;
    *npt2 = 1;
L10:
    n0 = nx;
    n1 = nx + 1;
    n2 = nx + 2;
    dx1 = ptx1[n1] - ptx1[n0];
    dx2 = ptx1[n2] - ptx1[n1];
    dy1 = pty1[n1] - pty1[n0];
    dy2 = pty1[n2] - pty1[n1];
/* Computing 2nd power */
    r__1 = dx1;
/* Computing 2nd power */
    r__2 = dy1;
/* Computing 2nd power */
    r__3 = dx2;
/* Computing 2nd power */
    r__4 = dy2;
    cx = (dx1 * dx2 + dy1 * dy2) / (sqrt(r__1 * r__1 + r__2 * r__2) * sqrt(
	    r__3 * r__3 + r__4 * r__4));
    if (cx > .5f) {
	nx = n2;
    } else {
	nx = n1;
    }
    ++(*npt2);
    ptx2[*npt2] = ptx1[nx];
    pty2[*npt2] = pty1[nx];
    if (nx + 2 <= *npt1) {
	goto L10;
    }
    if (nx != *npt1) {
	++(*npt2);
	ptx2[*npt2] = ptx1[*npt1];
	pty2[*npt2] = pty1[*npt1];
    }
    return 0;
} /* rmpsub_ */

/* Main program alias */ int main () { MAIN__ (); return 0; }

/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     PROGRAM FOR MAPDATA CONVERSION                         (94/05/07) */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* System generated locals */
    integer i__1;
    olist o__1;
    cllist cl__1;
    alist al__1;

    /* Builtin functions */
    integer s_rsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_rsle(void), f_open(olist *), f_rew(alist *), s_rsfe(cilist *), 
	    do_fio(integer *, char *, ftnlen), e_rsfe(void), s_wsue(cilist *),
	     do_uio(integer *, char *, ftnlen), e_wsue(void), f_clos(cllist *)
	    ;

    /* Local variables */
    static integer i__, ios, igid;
    static real flim[4], pnts[20000];
    static integer npts;
    static char cdsni[64], cdsno[64];

    /* Fortran I/O blocks */
    static cilist io___1 = { 0, 5, 0, 0, 0 };
    static cilist io___5 = { 1, 11, 1, "(2I8,4F8.3)", 0 };
    static cilist io___10 = { 0, 11, 0, "(10F8.3)", 0 };
    static cilist io___12 = { 0, 21, 0, 0, 0 };


    s_rsle(&io___1);
    do_lio(&c__9, &c__1, cdsni, (ftnlen)64);
    do_lio(&c__9, &c__1, cdsno, (ftnlen)64);
    e_rsle();
    o__1.oerr = 0;
    o__1.ounit = 11;
    o__1.ofnmlen = 64;
    o__1.ofnm = cdsni;
    o__1.orl = 0;
    o__1.osta = "OLD";
    o__1.oacc = 0;
    o__1.ofm = "FORMATTED";
    o__1.oblnk = 0;
    f_open(&o__1);
    al__1.aerr = 0;
    al__1.aunit = 11;
    f_rew(&al__1);
    o__1.oerr = 0;
    o__1.ounit = 21;
    o__1.ofnmlen = 64;
    o__1.ofnm = cdsno;
    o__1.orl = 0;
    o__1.osta = "NEW";
    o__1.oacc = 0;
    o__1.ofm = "UNFORMATTED";
    o__1.oblnk = 0;
    f_open(&o__1);
    al__1.aerr = 0;
    al__1.aunit = 21;
    f_rew(&al__1);
L10:
    ios = s_rsfe(&io___5);
    if (ios != 0) {
	goto L100001;
    }
    ios = do_fio(&c__1, (char *)&npts, (ftnlen)sizeof(integer));
    if (ios != 0) {
	goto L100001;
    }
    ios = do_fio(&c__1, (char *)&igid, (ftnlen)sizeof(integer));
    if (ios != 0) {
	goto L100001;
    }
    for (i__ = 1; i__ <= 4; ++i__) {
	ios = do_fio(&c__1, (char *)&flim[i__ - 1], (ftnlen)sizeof(real));
	if (ios != 0) {
	    goto L100001;
	}
    }
    ios = e_rsfe();
L100001:
    if (ios == 0) {
	if (npts >= 2) {
	    s_rsfe(&io___10);
	    i__1 = npts;
	    for (i__ = 1; i__ <= i__1; ++i__) {
		do_fio(&c__1, (char *)&pnts[i__ - 1], (ftnlen)sizeof(real));
	    }
	    e_rsfe();
	}
	s_wsue(&io___12);
	do_uio(&c__1, (char *)&npts, (ftnlen)sizeof(integer));
	do_uio(&c__1, (char *)&igid, (ftnlen)sizeof(integer));
	for (i__ = 1; i__ <= 4; ++i__) {
	    do_uio(&c__1, (char *)&flim[i__ - 1], (ftnlen)sizeof(real));
	}
	i__1 = npts;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    do_uio(&c__1, (char *)&pnts[i__ - 1], (ftnlen)sizeof(real));
	}
	e_wsue();
    }
    if (ios == 0) {
	goto L10;
    }
    cl__1.cerr = 0;
    cl__1.cunit = 11;
    cl__1.csta = 0;
    f_clos(&cl__1);
    cl__1.cerr = 0;
    cl__1.cunit = 21;
    cl__1.csta = 0;
    f_clos(&cl__1);
    return 0;
} /* MAIN__ */

/* Main program alias */ int main () { MAIN__ (); return 0; }

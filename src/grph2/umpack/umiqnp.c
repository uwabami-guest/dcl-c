/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__3 = 3;
static integer c__11 = 11;

/* ----------------------------------------------------------------------- */
/*     UMIQNP / UMIQID / UMIQCP / UMIQVL / UMISVL */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int umiqnp_0_(int n__, integer *ncp, char *cp, integer *idx, 
	integer *ipara, integer *in, ftnlen cp_len)
{
    /* Initialized data */

    static char cparas[8*11] = "INDEXMJ " "INDEXMN " "ITYPEMJ " "ITYPEMN " 
	    "INDEXBND" "MAXBND  " "IGROUP  " "INDEXOUT" "ITYPEOUT" "IPATLAND" 
	    "IPATLAKE";
    static integer ix[11] = { 3,1,1,3,3,300,1,1,1,1999,9999 };
    static char cparal[40*11] = "MAP_MAJOR_LINE_INDEX                    " 
	    "MAP_MINOR_LINE_INDEX                    " "MAP_MAJOR_LINE_TYPE "
	    "                    " "MAP_MINOR_LINE_TYPE                     " 
	    "MAP_BOUNDARY_INDEX                      " "MAP_BOUNDARY_LINE_MA"
	    "X                   " "****IGROUP                              " 
	    "MAP_OUTLINE_INDEX                       " "MAP_OUTLINE_TYPE    "
	    "                    " "SHADE PATTERN ON LAND                   " 
	    "SHADE PATTERN ON LAKE                   ";
    static logical lfirst = TRUE_;

    /* System generated locals */
    address a__1[3];
    integer i__1[3];

    /* Builtin functions */
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen),
	     s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer n;
    extern integer lenc_(char *, ftnlen);
    static char cmsg[80];
    extern logical lchreq_(char *, char *, ftnlen, ftnlen);
    extern /* Subroutine */ int rliget_(char *, integer *, integer *, ftnlen),
	     msgdmp_(char *, char *, char *, ftnlen, ftnlen, ftnlen), rtiget_(
	    char *, char *, integer *, integer *, ftnlen, ftnlen);

/*     / SHORT NAME / */
    switch(n__) {
	case 1: goto L_umiqid;
	case 2: goto L_umiqcp;
	case 3: goto L_umiqcl;
	case 4: goto L_umiqvl;
	case 5: goto L_umisvl;
	case 6: goto L_umiqin;
	}

/*     / LONG NAME / */
    *ncp = 11;
    return 0;
/* ----------------------------------------------------------------------- */

L_umiqid:
    for (n = 1; n <= 11; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *idx = n;
	    return 0;
	}
/* L10: */
    }
/* Writing concatenation */
    i__1[0] = 11, a__1[0] = "PARAMETER '";
    i__1[1] = lenc_(cp, cp_len), a__1[1] = cp;
    i__1[2] = 17, a__1[2] = "' IS NOT DEFINED.";
    s_cat(cmsg, a__1, i__1, &c__3, (ftnlen)80);
    msgdmp_("E", "UMIQID", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
    return 0;
/* ----------------------------------------------------------------------- */

L_umiqcp:
    if (1 <= *idx && *idx <= 11) {
	s_copy(cp, cparas + (*idx - 1 << 3), cp_len, (ftnlen)8);
    } else {
	msgdmp_("E", "UMIQCP", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_umiqcl:
    if (1 <= *idx && *idx <= 11) {
	s_copy(cp, cparal + (*idx - 1) * 40, cp_len, (ftnlen)40);
    } else {
	msgdmp_("E", "UMIQCL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_umiqvl:
    if (lfirst) {
	rtiget_("UM", cparas, ix, &c__11, (ftnlen)2, (ftnlen)8);
	rliget_(cparal, ix, &c__11, (ftnlen)40);
	lfirst = FALSE_;
    }
    if (1 <= *idx && *idx <= 11) {
	*ipara = ix[*idx - 1];
    } else {
	msgdmp_("E", "UMIQVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_umisvl:
    if (lfirst) {
	rtiget_("UM", cparas, ix, &c__11, (ftnlen)2, (ftnlen)8);
	rliget_(cparal, ix, &c__11, (ftnlen)40);
	lfirst = FALSE_;
    }
    if (1 <= *idx && *idx <= 11) {
	ix[*idx - 1] = *ipara;
    } else {
	msgdmp_("E", "UMISVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_umiqin:
    for (n = 1; n <= 11; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *in = n;
	    return 0;
	}
/* L20: */
    }
    *in = 0;
    return 0;
} /* umiqnp_ */

/* Subroutine */ int umiqnp_(integer *ncp)
{
    return umiqnp_0_(0, ncp, (char *)0, (integer *)0, (integer *)0, (integer *
	    )0, (ftnint)0);
    }

/* Subroutine */ int umiqid_(char *cp, integer *idx, ftnlen cp_len)
{
    return umiqnp_0_(1, (integer *)0, cp, idx, (integer *)0, (integer *)0, 
	    cp_len);
    }

/* Subroutine */ int umiqcp_(integer *idx, char *cp, ftnlen cp_len)
{
    return umiqnp_0_(2, (integer *)0, cp, idx, (integer *)0, (integer *)0, 
	    cp_len);
    }

/* Subroutine */ int umiqcl_(integer *idx, char *cp, ftnlen cp_len)
{
    return umiqnp_0_(3, (integer *)0, cp, idx, (integer *)0, (integer *)0, 
	    cp_len);
    }

/* Subroutine */ int umiqvl_(integer *idx, integer *ipara)
{
    return umiqnp_0_(4, (integer *)0, (char *)0, idx, ipara, (integer *)0, (
	    ftnint)0);
    }

/* Subroutine */ int umisvl_(integer *idx, integer *ipara)
{
    return umiqnp_0_(5, (integer *)0, (char *)0, idx, ipara, (integer *)0, (
	    ftnint)0);
    }

/* Subroutine */ int umiqin_(char *cp, integer *in, ftnlen cp_len)
{
    return umiqnp_0_(6, (integer *)0, cp, (integer *)0, (integer *)0, in, 
	    cp_len);
    }


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     UMPGET / UMPSET / UMPSTX */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int umpget_0_(int n__, char *cp, integer *ipara, ftnlen 
	cp_len)
{
    static char cl[40];
    static integer ip;
    static char cx[8];
    static integer it, idx;
    extern /* Subroutine */ int rliget_(char *, integer *, integer *, ftnlen),
	     umiqid_(char *, integer *, ftnlen), rllget_(char *, integer *, 
	    integer *, ftnlen), umlqid_(char *, integer *, ftnlen), rtiget_(
	    char *, char *, integer *, integer *, ftnlen, ftnlen), umpqid_(
	    char *, integer *, ftnlen), rlrget_(char *, integer *, integer *, 
	    ftnlen), umpqcl_(integer *, char *, ftnlen), rtlget_(char *, char 
	    *, integer *, integer *, ftnlen, ftnlen), umrqid_(char *, integer 
	    *, ftnlen), umpqcp_(integer *, char *, ftnlen), rtrget_(char *, 
	    char *, integer *, integer *, ftnlen, ftnlen), umpqit_(integer *, 
	    integer *), umisvl_(integer *, integer *), umlsvl_(integer *, 
	    integer *), umpqvl_(integer *, integer *), umpsvl_(integer *, 
	    integer *), umrsvl_(integer *, integer *);

    switch(n__) {
	case 1: goto L_umpset;
	case 2: goto L_umpstx;
	}

    umpqid_(cp, &idx, cp_len);
    umpqvl_(&idx, ipara);
    return 0;
/* ----------------------------------------------------------------------- */

L_umpset:
    umpqid_(cp, &idx, cp_len);
    umpsvl_(&idx, ipara);
    return 0;
/* ----------------------------------------------------------------------- */

L_umpstx:
    ip = *ipara;
    umpqid_(cp, &idx, cp_len);
    umpqit_(&idx, &it);
    umpqcp_(&idx, cx, (ftnlen)8);
    umpqcl_(&idx, cl, (ftnlen)40);
    if (it == 1) {
	rtiget_("UM", cx, &ip, &c__1, (ftnlen)2, (ftnlen)8);
	rliget_(cl, &ip, &c__1, (ftnlen)40);
	umiqid_(cp, &idx, cp_len);
	umisvl_(&idx, &ip);
    } else if (it == 2) {
	rtlget_("UM", cx, &ip, &c__1, (ftnlen)2, (ftnlen)8);
	rllget_(cl, &ip, &c__1, (ftnlen)40);
	umlqid_(cp, &idx, cp_len);
	umlsvl_(&idx, &ip);
    } else if (it == 3) {
	rtrget_("UM", cx, &ip, &c__1, (ftnlen)2, (ftnlen)8);
	rlrget_(cl, &ip, &c__1, (ftnlen)40);
	umrqid_(cp, &idx, cp_len);
	umrsvl_(&idx, &ip);
    }
    return 0;
} /* umpget_ */

/* Subroutine */ int umpget_(char *cp, integer *ipara, ftnlen cp_len)
{
    return umpget_0_(0, cp, ipara, cp_len);
    }

/* Subroutine */ int umpset_(char *cp, integer *ipara, ftnlen cp_len)
{
    return umpget_0_(1, cp, ipara, cp_len);
    }

/* Subroutine */ int umpstx_(char *cp, integer *ipara, ftnlen cp_len)
{
    return umpget_0_(2, cp, ipara, cp_len);
    }


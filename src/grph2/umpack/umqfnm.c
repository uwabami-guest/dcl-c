/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__3 = 3;

/* ----------------------------------------------------------------------- */
/*     MAP QUERY FILENAME */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int umqfnm_(char *cpara, char *cfname, ftnlen cpara_len, 
	ftnlen cfname_len)
{
    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer n;
    extern /* Subroutine */ int glcget_(char *, char *, ftnlen, ftnlen), 
	    cfsrch_(char *, integer *, char *, integer *, char *, ftnlen, 
	    ftnlen, ftnlen);
    static char cflist[1024*3];
    extern /* Subroutine */ int clower_(char *, ftnlen);
    static char cplist[1024*3];
    extern /* Subroutine */ int cupper_(char *, ftnlen);

    s_copy(cplist, " ", (ftnlen)1024, (ftnlen)1);
    glcget_("DUPATH", cplist + 1024, (ftnlen)6, (ftnlen)1024);
    glcget_("DSPATH", cplist + 2048, (ftnlen)6, (ftnlen)1024);
    for (n = 1; n <= 3; ++n) {
	s_copy(cflist + (n - 1 << 10), cpara, (ftnlen)1024, cpara_len);
/* L10: */
    }
    clower_(cflist + 1024, (ftnlen)1024);
    cupper_(cflist + 2048, (ftnlen)1024);
    cfsrch_(cplist, &c__3, cflist, &c__3, cfname, (ftnlen)1024, (ftnlen)1024, 
	    cfname_len);
    return 0;
} /* umqfnm_ */


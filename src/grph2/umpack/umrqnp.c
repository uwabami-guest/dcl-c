/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__3 = 3;
static integer c__4 = 4;

/* ----------------------------------------------------------------------- */
/*     UMRQNP / UMRQID / UMRQCP / UMRQVL / UMRSVL */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int umrqnp_0_(int n__, integer *ncp, char *cp, integer *idx, 
	real *rpara, integer *in, ftnlen cp_len)
{
    /* Initialized data */

    static char cparas[8*4] = "DGRIDMJ " "DGRIDMN " "DGRPLMJ " "DGRPLMN ";
    static real rx[4] = { 30.f,10.f,0.f,0.f };
    static char cparal[40*4] = "MAP_MAJOR_LINE_INTERVAL                 " 
	    "MAP_MINOR_LINE_INTERVAL                 " "MAP_MAJOR_LINE_POLAR"
	    "_LIMIT              " "MAP_MINOR_LINE_POLAR_LIMIT              ";
    static logical lfirst = TRUE_;

    /* System generated locals */
    address a__1[3];
    integer i__1[3];

    /* Builtin functions */
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen),
	     s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer n;
    extern integer lenc_(char *, ftnlen);
    static char cmsg[80];
    extern logical lchreq_(char *, char *, ftnlen, ftnlen);
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), rlrget_(char *, real *, integer *, ftnlen), 
	    rtrget_(char *, char *, real *, integer *, ftnlen, ftnlen);

/*     / SHORT NAME / */
    switch(n__) {
	case 1: goto L_umrqid;
	case 2: goto L_umrqcp;
	case 3: goto L_umrqcl;
	case 4: goto L_umrqvl;
	case 5: goto L_umrsvl;
	case 6: goto L_umrqin;
	}

/*     / LONG NAME / */
    *ncp = 4;
    return 0;
/* ----------------------------------------------------------------------- */

L_umrqid:
    for (n = 1; n <= 4; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *idx = n;
	    return 0;
	}
/* L10: */
    }
/* Writing concatenation */
    i__1[0] = 11, a__1[0] = "PARAMETER '";
    i__1[1] = lenc_(cp, cp_len), a__1[1] = cp;
    i__1[2] = 17, a__1[2] = "' IS NOT DEFINED.";
    s_cat(cmsg, a__1, i__1, &c__3, (ftnlen)80);
    msgdmp_("E", "UMRQID", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
    return 0;
/* ----------------------------------------------------------------------- */

L_umrqcp:
    if (1 <= *idx && *idx <= 4) {
	s_copy(cp, cparas + (*idx - 1 << 3), cp_len, (ftnlen)8);
    } else {
	msgdmp_("E", "UMRQCP", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_umrqcl:
    if (1 <= *idx && *idx <= 4) {
	s_copy(cp, cparal + (*idx - 1) * 40, cp_len, (ftnlen)40);
    } else {
	msgdmp_("E", "UMRQCL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_umrqvl:
    if (lfirst) {
	rtrget_("UM", cparas, rx, &c__4, (ftnlen)2, (ftnlen)8);
	rlrget_(cparal, rx, &c__4, (ftnlen)40);
	lfirst = FALSE_;
    }
    if (1 <= *idx && *idx <= 4) {
	*rpara = rx[*idx - 1];
    } else {
	msgdmp_("E", "UMRQVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_umrsvl:
    if (lfirst) {
	rtrget_("UM", cparas, rx, &c__4, (ftnlen)2, (ftnlen)8);
	rlrget_(cparal, rx, &c__4, (ftnlen)40);
	lfirst = FALSE_;
    }
    if (1 <= *idx && *idx <= 4) {
	rx[*idx - 1] = *rpara;
    } else {
	msgdmp_("E", "UMRSVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_umrqin:
    for (n = 1; n <= 4; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *in = n;
	    return 0;
	}
/* L20: */
    }
    *in = 0;
    return 0;
} /* umrqnp_ */

/* Subroutine */ int umrqnp_(integer *ncp)
{
    return umrqnp_0_(0, ncp, (char *)0, (integer *)0, (real *)0, (integer *)0,
	     (ftnint)0);
    }

/* Subroutine */ int umrqid_(char *cp, integer *idx, ftnlen cp_len)
{
    return umrqnp_0_(1, (integer *)0, cp, idx, (real *)0, (integer *)0, 
	    cp_len);
    }

/* Subroutine */ int umrqcp_(integer *idx, char *cp, ftnlen cp_len)
{
    return umrqnp_0_(2, (integer *)0, cp, idx, (real *)0, (integer *)0, 
	    cp_len);
    }

/* Subroutine */ int umrqcl_(integer *idx, char *cp, ftnlen cp_len)
{
    return umrqnp_0_(3, (integer *)0, cp, idx, (real *)0, (integer *)0, 
	    cp_len);
    }

/* Subroutine */ int umrqvl_(integer *idx, real *rpara)
{
    return umrqnp_0_(4, (integer *)0, (char *)0, idx, rpara, (integer *)0, (
	    ftnint)0);
    }

/* Subroutine */ int umrsvl_(integer *idx, real *rpara)
{
    return umrqnp_0_(5, (integer *)0, (char *)0, idx, rpara, (integer *)0, (
	    ftnint)0);
    }

/* Subroutine */ int umrqin_(char *cp, integer *in, ftnlen cp_len)
{
    return umrqnp_0_(6, (integer *)0, cp, (integer *)0, (real *)0, in, cp_len)
	    ;
    }


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Common Block Declarations */

struct {
    integer itr;
    real rundef;
    integer iundef;
    real pi, cpr, cpd, cp;
} umwk1_;

#define umwk1_1 umwk1_

/* Table of constant values */

static integer c__1 = 1;
static real c_b4 = 1.f;
static real c_b13 = 0.f;

/* ----------------------------------------------------------------------- */
/*     UMSPPT : SET MAP POLE (POINTS) */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int umsppt_(void)
{
    /* System generated locals */
    integer i__1;
    real r__1, r__2;

    /* Builtin functions */
    double cos(doublereal), sin(doublereal), atan2(doublereal, doublereal);

    /* Local variables */
    static integer i__;
    static real r__, x, y, z__, x2, y2, x3, y3, z3, th, uxc, uyc, plx, ply, 
	    uxr, uyr, uxz, uyz, ymin, ymax;
    extern /* Subroutine */ int ct3sc_(real *, real *, real *, real *, real *,
	     real *), ct3cs_(real *, real *, real *, real *, real *, real *);
    static integer ndata;
    static real plrot, stlat1, stlat2;
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), sgrget_(char *, real *, ftnlen), sgqmpl_(real *, 
	    real *, real *), sgsmpl_(real *, real *, real *), sgrset_(char *, 
	    real *, ftnlen), umqptn_(integer *), umqpnt_(integer *, real *, 
	    real *);

    sgqmpl_(&plx, &ply, &plrot);
    if (plx != umwk1_1.rundef && ply != umwk1_1.rundef && plrot != 
	    umwk1_1.rundef) {
	return 0;
    }
    umqptn_(&ndata);
    if (ndata == 0) {
	return 0;
    }
    x2 = 0.f;
    y2 = 0.f;
    umqpnt_(&c__1, &uxz, &uyz);
    i__1 = ndata;
    for (i__ = 1; i__ <= i__1; ++i__) {
	umqpnt_(&i__, &uxz, &uyz);
	ymax = max(ymax,uyz);
	ymin = min(ymin,uyz);
	uxr = uxz / umwk1_1.cpr;
	uyr = uyz / umwk1_1.cpr;
	x2 += cos(uxr);
	y2 += sin(uxr);
	r__1 = umwk1_1.pi / 2.f - uyr;
	ct3sc_(&c_b4, &r__1, &uxr, &x, &y, &z__);
	x3 += x;
	y3 += y;
	z3 += z__;
/* L20: */
    }
    uxc = atan2(y2, x2) * umwk1_1.cpr;
    uyc = (ymax + ymin) / 2.f;
    if (10 <= umwk1_1.itr && umwk1_1.itr <= 19) {
	plx = uxc;
	ply = umwk1_1.cpd * 90.f;
    } else if (20 <= umwk1_1.itr && umwk1_1.itr <= 24) {
	plx = uxc;
	ply = umwk1_1.cpd * 90.f;
	if (uyc == 0.f) {
	    msgdmp_("E", "UMSPPT", "INVALID WINDOW FOR CONICAL PROJECTION.", (
		    ftnlen)1, (ftnlen)6, (ftnlen)38);
	}
	sgrget_("STLAT1", &stlat1, (ftnlen)6);
	sgrget_("STLAT2", &stlat2, (ftnlen)6);
	if (umwk1_1.itr == 22) {
	    if (stlat1 == umwk1_1.rundef) {
/* Computing MAX */
		r__1 = ymin, r__2 = umwk1_1.cpd * -89.f;
		stlat1 = max(r__1,r__2);
	    }
	    if (stlat2 == umwk1_1.rundef) {
/* Computing MIN */
		r__1 = ymax, r__2 = umwk1_1.cpd * 89.f;
		stlat2 = min(r__1,r__2);
	    }
	    sgrset_("STLAT1", &stlat1, (ftnlen)6);
	    sgrset_("STLAT2", &stlat2, (ftnlen)6);
	} else {
	    if (stlat1 == umwk1_1.rundef) {
		stlat1 = uyc;
	    }
	    sgrset_("STLAT1", &stlat1, (ftnlen)6);
	}
    } else if (30 <= umwk1_1.itr && umwk1_1.itr <= 34) {
	ct3cs_(&x3, &y3, &z3, &r__, &th, &uxr);
	plx = uxr * umwk1_1.cpr;
	ply = (umwk1_1.pi / 2.f - th) * umwk1_1.cpr;
    }
    sgsmpl_(&plx, &ply, &c_b13);
    return 0;
} /* umsppt_ */


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Common Block Declarations */

struct {
    integer itr;
    real rundef;
    integer iundef;
    real pi, cpr, cpd, cp;
} umwk1_;

#define umwk1_1 umwk1_

/* ----------------------------------------------------------------------- */
/*     UMSTVZ : SET VIEW PORT AND T-WINDOW */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int umstvz_(void)
{
    /* Builtin functions */
    double asin(doublereal);

    /* Local variables */
    static logical lprj;
    static real rsat;
    static integer iwtrf;
    static real rxmin, rymin, rxmax, rymax, vxmin, vymin, vxmax, vymax, wxmin,
	     wxmax, wymin, wymax, txmin, txmax, tymin, tymax;
    extern /* Subroutine */ int sglget_(char *, logical *, ftnlen), sgrget_(
	    char *, real *, ftnlen), sgqvpt_(real *, real *, real *, real *), 
	    sgsvpt_(real *, real *, real *, real *), sgqtxy_(real *, real *, 
	    real *, real *), sgstxy_(real *, real *, real *, real *), stqwtr_(
	    real *, real *, real *, real *, real *, real *, real *, real *, 
	    integer *);

    sgqvpt_(&vxmin, &vxmax, &vymin, &vymax);
    sglget_("L2TO3", &lprj, (ftnlen)5);
    if (lprj) {
	rxmin = 0.f;
	rxmax = 1.f;
	rymin = 0.f;
	rymax = 1.f;
    } else {
	stqwtr_(&rxmin, &rxmax, &rymin, &rymax, &wxmin, &wxmax, &wymin, &
		wymax, &iwtrf);
    }
    if (vxmin == umwk1_1.rundef) {
	vxmin = rxmin;
    }
    if (vxmax == umwk1_1.rundef) {
	vxmax = rxmax;
    }
    if (vymin == umwk1_1.rundef) {
	vymin = rymin;
    }
    if (vymax == umwk1_1.rundef) {
	vymax = rymax;
    }
    sgsvpt_(&vxmin, &vxmax, &vymin, &vymax);
/* ------------------------------- T-WINDOW ------------------------------ */
    sgqtxy_(&txmin, &txmax, &tymin, &tymax);
    if (txmin == umwk1_1.rundef) {
	txmin = -umwk1_1.cpd * 180;
    }
    if (txmax == umwk1_1.rundef) {
	txmax = umwk1_1.cpd * 180;
    }
    if (tymax == umwk1_1.rundef) {
	tymax = umwk1_1.cpd * 90;
    }
    if (tymin == umwk1_1.rundef) {
	if (umwk1_1.itr == 30) {
	    sgrget_("RSAT", &rsat, (ftnlen)4);
	    if (rsat == 0.f) {
		tymin = umwk1_1.cpd * 0.f;
	    } else {
		tymin = umwk1_1.cpr * asin(1.f / rsat);
	    }
	} else {
	    tymin = -umwk1_1.cpd * 90;
	}
    }
    sgstxy_(&txmin, &txmax, &tymin, &tymax);
    return 0;
} /* umstvz_ */


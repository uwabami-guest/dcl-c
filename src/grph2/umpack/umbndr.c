/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int umbndr_(S_fp func, S_fp ftr, real *xmin, real *xmax, 
	real *ymin, real *ymax)
{
    /* System generated locals */
    real r__1;

    /* Local variables */
    static integer i__;
    static real x, y, x1, y1, x2, y2, x3, y3, x4, y4, dx, dy, uxmin, uymin, 
	    uxmax, uymax, rundef;
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen);

    glrget_("RUNDEF", &rundef, (ftnlen)6);
    (*ftr)(&uxmin, &uxmax, &uymin, &uymax);
    if (uxmin == rundef || uxmax == rundef || uymin == rundef || uymax == 
	    rundef) {
	return 0;
    }
    dx = (uxmax - uxmin) / 360;
    dy = (uymax - uymin) / 360;
    if (*xmin == rundef) {
	(*func)(&uxmin, &uymin, xmin, ymin);
	*xmax = *xmin;
	*ymax = *ymin;
    }
    for (i__ = 0; i__ <= 360; ++i__) {
	x = uxmin + dx * i__;
	y = uymin + dy * i__;
	(*func)(&x, &uymin, &x1, &y1);
	(*func)(&x, &uymax, &x2, &y2);
	(*func)(&uxmin, &y, &x3, &y3);
	(*func)(&uxmax, &y, &x4, &y4);
/* Computing MAX */
	r__1 = max(*xmax,x1), r__1 = max(r__1,x2), r__1 = max(r__1,x3);
	*xmax = max(r__1,x4);
/* Computing MIN */
	r__1 = min(*xmin,x1), r__1 = min(r__1,x2), r__1 = min(r__1,x3);
	*xmin = min(r__1,x4);
/* Computing MAX */
	r__1 = max(*ymax,y1), r__1 = max(r__1,y2), r__1 = max(r__1,y3);
	*ymax = max(r__1,y4);
/* Computing MIN */
	r__1 = min(*ymin,y1), r__1 = min(r__1,y2), r__1 = min(r__1,y3);
	*ymin = min(r__1,y4);
/* L10: */
    }
    return 0;
} /* umbndr_ */


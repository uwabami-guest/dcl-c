/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Common Block Declarations */

struct {
    integer itr;
    real rundef;
    integer iundef;
    real pi, cpr, cpd, cp;
} umwk1_;

#define umwk1_1 umwk1_

/* Table of constant values */

static real c_b10 = 0.f;

/* ----------------------------------------------------------------------- */
/*     UMSPCW : CIRCLE WINDOW */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int umspcw_(void)
{
    /* System generated locals */
    real r__1, r__2;

    /* Local variables */
    static real r__, plx, ply, xcntr, ycntr, plrot, stlat1, stlat2;
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), sgrget_(char *, real *, ftnlen), umqcwd_(real *, 
	    real *, real *), sgqmpl_(real *, real *, real *), sgsmpl_(real *, 
	    real *, real *), sgrset_(char *, real *, ftnlen);

    sgqmpl_(&plx, &ply, &plrot);
    if (plx != umwk1_1.rundef && ply != umwk1_1.rundef && plrot != 
	    umwk1_1.rundef) {
	return 0;
    }
    umqcwd_(&xcntr, &ycntr, &r__);
    if (xcntr == umwk1_1.rundef || ycntr == umwk1_1.rundef || r__ == 
	    umwk1_1.rundef) {
	return 0;
    }
    if (10 <= umwk1_1.itr && umwk1_1.itr <= 19) {
	plx = xcntr;
	ply = umwk1_1.cpd * 90.f;
    } else if (20 <= umwk1_1.itr && umwk1_1.itr <= 24) {
	plx = xcntr;
	ply = umwk1_1.cpd * 90.f;
	if (ycntr == 0.f) {
	    msgdmp_("E", "UMSPCW", "INVALID WINDOW FOR CONICAL PROJECTION.", (
		    ftnlen)1, (ftnlen)6, (ftnlen)38);
	}
	sgrget_("STLAT1", &stlat1, (ftnlen)6);
	sgrget_("STLAT2", &stlat2, (ftnlen)6);
	if (umwk1_1.itr == 22) {
	    if (stlat1 == umwk1_1.rundef) {
/* Computing MAX */
		r__1 = ycntr - r__, r__2 = umwk1_1.cpd * -89.f;
		stlat1 = max(r__1,r__2);
	    }
	    if (stlat2 == umwk1_1.rundef) {
/* Computing MIN */
		r__1 = ycntr + r__, r__2 = umwk1_1.cpd * 89.f;
		stlat2 = min(r__1,r__2);
	    }
	    sgrset_("STLAT1", &stlat1, (ftnlen)6);
	    sgrset_("STLAT2", &stlat2, (ftnlen)6);
	} else {
	    if (stlat1 == umwk1_1.rundef) {
		stlat1 = ycntr;
	    }
	    sgrset_("STLAT1", &stlat1, (ftnlen)6);
	}
    } else if (30 <= umwk1_1.itr && umwk1_1.itr <= 34) {
	plx = xcntr;
	ply = ycntr;
    }
    sgsmpl_(&plx, &ply, &c_b10);
    return 0;
} /* umspcw_ */


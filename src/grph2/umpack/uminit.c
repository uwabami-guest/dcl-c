/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     CONTROL ROUTINES */
/* ----------------------------------------------------------------------- */
/*     INITIALIZATION */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uminit_(void)
{
    static real rundef;
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen), umscwd_(real 
	    *, real *, real *), umscnt_(real *, real *, real *), umrset_(char 
	    *, real *, ftnlen), umrpnt_(void);

    glrget_("RUNDEF", &rundef, (ftnlen)6);
    umscnt_(&rundef, &rundef, &rundef);
    umscwd_(&rundef, &rundef, &rundef);
    umrset_("DGRIDMJ", &rundef, (ftnlen)7);
    umrset_("DGRIDMN", &rundef, (ftnlen)7);
    umrpnt_();
    return 0;
} /* uminit_ */


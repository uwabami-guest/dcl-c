/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__2 = 2;
static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     DRAW MAP (GEOGRAPHIC LINES) */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int umpmap_(char *cdsn, ftnlen cdsn_len)
{
    /* System generated locals */
    address a__1[2];
    integer i__1[2], i__2;
    real r__1;
    olist o__1;
    cllist cl__1;
    alist al__1;

    /* Builtin functions */
    integer s_cmp(char *, char *, ftnlen, ftnlen);
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen);
    integer f_open(olist *), f_rew(alist *), s_rsue(cilist *), do_uio(integer 
	    *, char *, ftnlen), e_rsue(void), f_clos(cllist *);

    /* Local variables */
    static integer i__, n, n0, nm, iu;
    static real ux0, ux1, uy0, uy1;
    static integer ios, itr, igid;
    extern integer lenc_(char *, ftnlen);
    static char cmsg[80];
    static real xlat[8192], xlon[8192];
    static integer npts, index;
    static char cdsnx[1024];
    static logical llast;
    static integer itype;
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), umiget_(char *, integer *, ftnlen);
    extern integer iufopn_(void);
    static real xlatsg[8192];
    extern /* Subroutine */ int sgqwnd_(real *, real *, real *, real *), 
	    umqfnm_(char *, char *, ftnlen, ftnlen);
    static real xlatmn;
    extern /* Subroutine */ int szplcl_(void);
    static real xlonsg[8192], xlonmn, xlatmx;
    extern /* Subroutine */ int sgqtrn_(integer *);
    static real xlonmx;
    extern /* Subroutine */ int szplop_(integer *, integer *), szplzu_(
	    integer *, real *, real *);
    static logical litrone;

    /* Fortran I/O blocks */
    static cilist io___13 = { 1, 0, 1, 0, 0 };


    litrone = FALSE_;
/*     / GET INTERNAL PARAMETERS / */
    umiget_("INDEXOUT", &index, (ftnlen)8);
    umiget_("ITYPEOUT", &itype, (ftnlen)8);
    sgqwnd_(&ux0, &ux1, &uy0, &uy1);
    sgqtrn_(&itr);
    if (itr == 1) {
	litrone = TRUE_;
    } else if (1 < itr && itr < 10) {
	msgdmp_("E", "UMPMAP", "INVALID TRANSFORMATION NUMBER.", (ftnlen)1, (
		ftnlen)6, (ftnlen)30);
    }
/*     / INQUIRE OUTLINE FILE / */
    umqfnm_(cdsn, cdsnx, cdsn_len, (ftnlen)1024);
    if (s_cmp(cdsnx, " ", (ftnlen)1024, (ftnlen)1) == 0) {
/* Writing concatenation */
	i__1[0] = 15, a__1[0] = "OUTLINE FILE = ";
	i__1[1] = lenc_(cdsn, cdsn_len), a__1[1] = cdsn;
	s_cat(cmsg, a__1, i__1, &c__2, (ftnlen)80);
	msgdmp_("M", "UMPMAP", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
	msgdmp_("E", "UMPMAP", "OUTLINE FILE DOES NOT EXIST.", (ftnlen)1, (
		ftnlen)6, (ftnlen)28);
    }
/*     / OPEN OUTLINE FILE / */
    iu = iufopn_();
    o__1.oerr = 0;
    o__1.ounit = iu;
    o__1.ofnmlen = 1024;
    o__1.ofnm = cdsnx;
    o__1.orl = 0;
    o__1.osta = "OLD";
    o__1.oacc = 0;
    o__1.ofm = "UNFORMATTED";
    o__1.oblnk = 0;
    f_open(&o__1);
    al__1.aerr = 0;
    al__1.aunit = iu;
    f_rew(&al__1);
/*     / READ OUTLINE DATA AND PLOT / */
    szplop_(&itype, &index);
L10:
    io___13.ciunit = iu;
    ios = s_rsue(&io___13);
    if (ios != 0) {
	goto L100001;
    }
    ios = do_uio(&c__1, (char *)&npts, (ftnlen)sizeof(integer));
    if (ios != 0) {
	goto L100001;
    }
    ios = do_uio(&c__1, (char *)&igid, (ftnlen)sizeof(integer));
    if (ios != 0) {
	goto L100001;
    }
    ios = do_uio(&c__1, (char *)&xlatmx, (ftnlen)sizeof(real));
    if (ios != 0) {
	goto L100001;
    }
    ios = do_uio(&c__1, (char *)&xlatmn, (ftnlen)sizeof(real));
    if (ios != 0) {
	goto L100001;
    }
    ios = do_uio(&c__1, (char *)&xlonmx, (ftnlen)sizeof(real));
    if (ios != 0) {
	goto L100001;
    }
    ios = do_uio(&c__1, (char *)&xlonmn, (ftnlen)sizeof(real));
    if (ios != 0) {
	goto L100001;
    }
    i__2 = npts / 2;
    for (i__ = 1; i__ <= i__2; ++i__) {
	ios = do_uio(&c__1, (char *)&xlat[i__ - 1], (ftnlen)sizeof(real));
	if (ios != 0) {
	    goto L100001;
	}
	ios = do_uio(&c__1, (char *)&xlon[i__ - 1], (ftnlen)sizeof(real));
	if (ios != 0) {
	    goto L100001;
	}
    }
    ios = e_rsue();
L100001:
    if (ios == 0) {
	if (npts >= 2) {
	    if (litrone) {
		i__2 = npts / 2;
		for (n = 1; n <= i__2; ++n) {
		    if (ux0 > xlon[n - 1]) {
			xlon[n - 1] += 360.f;
		    }
		    if (ux1 < xlon[n - 1]) {
			xlon[n - 1] += -360.f;
		    }
/* L15: */
		}
		n0 = 1;
		llast = FALSE_;
L20:
		i__2 = npts / 2;
		for (n = n0 + 1; n <= i__2; ++n) {
		    if ((r__1 = xlon[n - 1] - xlon[n - 2], abs(r__1)) > 300.f)
			     {
			goto L30;
		    }
/* L25: */
		}
L30:
		nm = n;
		if (nm > npts / 2) {
		    llast = TRUE_;
		    nm = npts / 2;
		}
		i__2 = nm - n0 + 1;
		for (n = 1; n <= i__2; ++n) {
		    xlatsg[n - 1] = xlat[n + n0 - 2];
/* L35: */
		}
		i__2 = nm - n0;
		for (n = 1; n <= i__2; ++n) {
		    xlonsg[n - 1] = xlon[n + n0 - 2];
/* L40: */
		}
		if (llast) {
		    xlonsg[nm - n0] = xlon[nm - 1];
		} else {
		    if (xlon[nm - 1] > xlon[nm - 2]) {
			xlonsg[nm - n0] = xlon[nm - 1] - 360.f;
			xlon[nm - 2] += 360.f;
		    } else {
			xlonsg[nm - n0] = xlon[nm - 1] + 360.f;
			xlon[nm - 2] += -360.f;
		    }
		}
		i__2 = nm - n0 + 1;
		szplzu_(&i__2, xlonsg, xlatsg);
		n0 = nm - 1;
		if (nm == npts / 2) {
		    goto L45;
		}
		goto L20;
	    } else {
		i__2 = npts / 2;
		szplzu_(&i__2, xlon, xlat);
	    }
L45:
	    ;
	}
    }
    if (ios == 0) {
	goto L10;
    }
/* L50: */
    szplcl_();
    cl__1.cerr = 0;
    cl__1.cunit = iu;
    cl__1.csta = 0;
    f_clos(&cl__1);
    return 0;
} /* umpmap_ */


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;
static real c_b5 = 0.f;
static real c_b7 = 1e-4f;

/* ----------------------------------------------------------------------- */
/*     DRAW LIMB OF MAP */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int umplim_(void)
{
    /* System generated locals */
    integer i__1;

    /* Local variables */
    static integer i__, j;
    static real x, y, x1, y1, x2, y2, x3, y3, pi, xx, yy, xbnd[2], ybnd[2];
    extern real rfpi_(void);
    static integer nmax;
    static logical ldiff;
    extern logical lreqa_(real *, real *, real *);
    static integer index;
    extern /* Subroutine */ int umiget_(char *, integer *, ftnlen), szcllv_(
	    void), stftrn_(real *, real *, real *, real *), szqclx_(real *, 
	    real *), szqcly_(real *, real *), szslti_(integer *, integer *), 
	    szpllv_(real *, real *), szoplv_(void), szmvlv_(real *, real *);

    pi = rfpi_();
/*     / GET INTERNAL PARAMETERS / */
    szqclx_(xbnd, &xbnd[1]);
    szqcly_(ybnd, &ybnd[1]);
    umiget_("INDEXBND", &index, (ftnlen)8);
    umiget_("MAXBND", &nmax, (ftnlen)6);
/*     / SET INTERNAL PARAMETER / */
    szslti_(&c__1, &index);
/*     / DRAW SIDE BOUNDARIES / */
    stftrn_(xbnd, &c_b5, &x1, &y1);
    stftrn_(&xbnd[1], &c_b5, &x2, &y2);
    ldiff = ! (lreqa_(&x1, &x2, &c_b7) && lreqa_(&y1, &y2, &c_b7));
    if (ldiff) {
	for (i__ = 1; i__ <= 2; ++i__) {
	    szoplv_();
	    stftrn_(&xbnd[i__ - 1], ybnd, &xx, &yy);
	    szmvlv_(&xx, &yy);
	    i__1 = nmax;
	    for (j = 1; j <= i__1; ++j) {
		y = j * (ybnd[1] - ybnd[0]) / nmax + ybnd[0];
		stftrn_(&xbnd[i__ - 1], &y, &xx, &yy);
		szpllv_(&xx, &yy);
/* L10: */
	    }
	    szcllv_();
/* L20: */
	}
    }
/*     / DRAW POLES / */
    if (xbnd[0] == xbnd[1]) {
	xbnd[0] = -pi;
	xbnd[1] = pi;
    }
    for (i__ = 1; i__ <= 2; ++i__) {
	stftrn_(xbnd, &ybnd[i__ - 1], &x1, &y1);
	stftrn_(&xbnd[1], &ybnd[i__ - 1], &x2, &y2);
	stftrn_(&c_b5, &ybnd[i__ - 1], &x3, &y3);
	ldiff = ! (lreqa_(&x1, &x3, &c_b7) && lreqa_(&y1, &y3, &c_b7)) || ! (
		lreqa_(&x2, &x3, &c_b7) && lreqa_(&y2, &y3, &c_b7));
	if (ldiff) {
	    szoplv_();
	    stftrn_(xbnd, &ybnd[i__ - 1], &xx, &yy);
	    szmvlv_(&xx, &yy);
	    i__1 = nmax;
	    for (j = 1; j <= i__1; ++j) {
		x = j * (xbnd[1] - xbnd[0]) / nmax + xbnd[0];
		stftrn_(&x, &ybnd[i__ - 1], &xx, &yy);
		szpllv_(&xx, &yy);
/* L30: */
	    }
	    szcllv_();
	}
/* L40: */
    }
    return 0;
} /* umplim_ */


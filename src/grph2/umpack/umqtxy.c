/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Common Block Declarations */

struct {
    integer itr;
    real rundef;
    integer iundef;
    real pi, cpr, cpd, cp;
} umwk1_;

#define umwk1_1 umwk1_

/* ----------------------------------------------------------------------- */
/*     T-WINDOW */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int umqtxy_(real *txminz, real *txmaxz, real *tyminz, real *
	tymaxz)
{
    static real txmin, tymin, txmax, tymax, stlat1, stlat2;
    extern /* Subroutine */ int stfrad_(real *, real *, real *, real *), 
	    sgrget_(char *, real *, ftnlen), sgqtxy_(real *, real *, real *, 
	    real *);

    sgqtxy_(&txmin, &txmax, &tymin, &tymax);
    stfrad_(&txmin, &txmax, txminz, txmaxz);
    if (umwk1_1.itr == 11) {
	*tyminz = -umwk1_1.cp * 75.f;
	*tymaxz = umwk1_1.cp * 75.f;
    } else if (umwk1_1.itr == 31) {
	*tyminz = umwk1_1.cp * 0.f;
	*tymaxz = umwk1_1.cp * 90.f;
    } else if (umwk1_1.itr == 22) {
	sgrget_("STLAT1", &stlat1, (ftnlen)6);
	sgrget_("STLAT2", &stlat2, (ftnlen)6);
	if (stlat1 + stlat2 >= 0.f) {
	    *tyminz = umwk1_1.cp * 0.f;
	    *tymaxz = umwk1_1.cp * 90.f;
	} else {
	    *tyminz = -umwk1_1.cp * 90.f;
	    *tymaxz = umwk1_1.cp * 0.f;
	}
    } else {
	stfrad_(&tymin, &tymax, tyminz, tymaxz);
    }
    return 0;
} /* umqtxy_ */


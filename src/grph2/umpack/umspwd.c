/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Common Block Declarations */

struct {
    integer itr;
    real rundef;
    integer iundef;
    real pi, cpr, cpd, cp;
} umwk1_;

#define umwk1_1 umwk1_

/* Table of constant values */

static real c_b2 = 0.f;

/* ----------------------------------------------------------------------- */
/*     UMSPWD : WINDOW MODE */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int umspwd_(void)
{
    static real dx, plx, ply, plrot, uxmin, uxmax, uymin, uymax, stlat1, 
	    stlat2;
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), sgrget_(char *, real *, ftnlen), sgqmpl_(real *, 
	    real *, real *), sgqwnd_(real *, real *, real *, real *), sgsmpl_(
	    real *, real *, real *), sgrset_(char *, real *, ftnlen);

    sgqmpl_(&plx, &ply, &plrot);
    if (plx != umwk1_1.rundef && ply != umwk1_1.rundef && plrot != 
	    umwk1_1.rundef) {
	return 0;
    }
    sgqwnd_(&uxmin, &uxmax, &uymin, &uymax);
    if (uxmin == umwk1_1.rundef || uxmax == umwk1_1.rundef || uymin == 
	    umwk1_1.rundef || uymax == umwk1_1.rundef) {
	return 0;
    }
    dx = uxmax - uxmin;
    if (dx <= 0.f) {
	dx += umwk1_1.pi * 2 * umwk1_1.cpr;
    }
    plx = uxmin + dx / 2.f;
    if (umwk1_1.itr >= 30) {
	ply = (uymin + uymax) / 2.f;
    } else {
	ply = umwk1_1.cpd * 90.f;
    }
    sgsmpl_(&plx, &ply, &c_b2);
/* ------------------------ STANDARD LATITUDE --------------------------- */
    sgrget_("STLAT1", &stlat1, (ftnlen)6);
    sgrget_("STLAT2", &stlat2, (ftnlen)6);
    if (umwk1_1.itr == 20 || umwk1_1.itr == 21 || umwk1_1.itr == 23) {
	if (uymin != umwk1_1.rundef && uymax != umwk1_1.rundef) {
	    if (stlat1 == umwk1_1.rundef) {
		stlat1 = (uymin + uymax) / 2.f;
	    }
	    if (stlat1 == 0.f) {
		msgdmp_("E", "UMSPWD", "INVALID WINDOW FOR CONICAL PROJECTIO"
			"N.", (ftnlen)1, (ftnlen)6, (ftnlen)38);
	    }
	} else {
	    if (stlat1 == umwk1_1.rundef) {
		stlat1 = umwk1_1.cpd * 35;
	    }
	}
	sgrset_("STLAT1", &stlat1, (ftnlen)6);
    } else if (umwk1_1.itr == 22) {
	if (uymin != umwk1_1.rundef && uymax != umwk1_1.rundef) {
	    if (stlat1 == umwk1_1.rundef) {
		stlat1 = uymin;
	    }
	    if (stlat2 == umwk1_1.rundef) {
		stlat2 = uymax;
	    }
	} else {
	    if (stlat1 == umwk1_1.rundef) {
		stlat1 = umwk1_1.cpd * 35;
	    }
	    if (stlat2 == umwk1_1.rundef) {
		stlat2 = umwk1_1.cpd * 45;
	    }
	}
	sgrset_("STLAT1", &stlat1, (ftnlen)6);
	sgrset_("STLAT2", &stlat2, (ftnlen)6);
    }
    return 0;
} /* umspwd_ */


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     SCATTERED POINTS */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int umspnt_0_(int n__, integer *n, real *ux, real *uy, real *
	uxa, real *uya)
{
    /* Initialized data */

    static integer ndata = 0;

    /* System generated locals */
    integer i__1;

    /* Local variables */
    static integer i__;
    static real uxz[64], uyz[64];
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen);

    /* Parameter adjustments */
    if (ux) {
	--ux;
	}
    if (uy) {
	--uy;
	}

    /* Function Body */
    switch(n__) {
	case 1: goto L_umqpnt;
	case 2: goto L_umqptn;
	case 3: goto L_umrpnt;
	}

    i__1 = *n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	if (ndata >= 64) {
	    msgdmp_("W", "UMSPNT", "TOO MANY POINTS IGNORED.", (ftnlen)1, (
		    ftnlen)6, (ftnlen)24);
	    return 0;
	}
	++ndata;
	uxz[ndata - 1] = ux[ndata];
	uyz[ndata - 1] = uy[ndata];
/* L10: */
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_umqpnt:
    *uxa = uxz[*n - 1];
    *uya = uyz[*n - 1];
    return 0;
/* ----------------------------------------------------------------------- */

L_umqptn:
    *n = ndata;
    return 0;
/* ----------------------------------------------------------------------- */

L_umrpnt:
    ndata = 0;
    return 0;
} /* umspnt_ */

/* Subroutine */ int umspnt_(integer *n, real *ux, real *uy)
{
    return umspnt_0_(0, n, ux, uy, (real *)0, (real *)0);
    }

/* Subroutine */ int umqpnt_(integer *n, real *uxa, real *uya)
{
    return umspnt_0_(1, n, (real *)0, (real *)0, uxa, uya);
    }

/* Subroutine */ int umqptn_(integer *n)
{
    return umspnt_0_(2, n, (real *)0, (real *)0, (real *)0, (real *)0);
    }

/* Subroutine */ int umrpnt_(void)
{
    return umspnt_0_(3, (integer *)0, (real *)0, (real *)0, (real *)0, (real *
	    )0);
    }


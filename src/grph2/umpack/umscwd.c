/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     CIRCLE WINDOW */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int umscwd_0_(int n__, real *xcntr, real *ycntr, real *r__)
{
    static real rz, xcntrz, ycntrz;

    switch(n__) {
	case 1: goto L_umqcwd;
	}

    xcntrz = *xcntr;
    ycntrz = *ycntr;
    rz = *r__;
    return 0;
/* ----------------------------------------------------------------------- */

L_umqcwd:
    *xcntr = xcntrz;
    *ycntr = ycntrz;
    *r__ = rz;
    return 0;
} /* umscwd_ */

/* Subroutine */ int umscwd_(real *xcntr, real *ycntr, real *r__)
{
    return umscwd_0_(0, xcntr, ycntr, r__);
    }

/* Subroutine */ int umqcwd_(real *xcntr, real *ycntr, real *r__)
{
    return umscwd_0_(1, xcntr, ycntr, r__);
    }


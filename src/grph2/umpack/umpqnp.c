/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__3 = 3;

/* ----------------------------------------------------------------------- */
/*     UMPQNP / UMPQID / UMPQCP / UMPQVL / UMPSVL */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int umpqnp_0_(int n__, integer *ncp, char *cp, integer *idx, 
	integer *itp, integer *ipara, integer *in, ftnlen cp_len)
{
    /* Initialized data */

    static char cparas[8*20] = "INDEXMJ " "INDEXMN " "ITYPEMJ " "ITYPEMN " 
	    "DGRIDMJ " "DGRIDMN " "DGRPLMJ " "DGRPLMN " "LGRIDMJ " "LGRIDMN " 
	    "INDEXBND" "MAXBND  " "IGROUP  " "INDEXOUT" "ITYPEOUT" "LGLOBE  " 
	    "LWHINT  " "IPATLAND" "IPATLAKE" "LFILLAKE";
    static integer itype[20] = { 1,1,1,1,3,3,3,3,2,2,1,1,1,1,1,2,2,1,1,2 };
    static char cparal[40*20] = "MAP_MAJOR_LINE_INDEX                    " 
	    "MAP_MINOR_LINE_INDEX                    " "MAP_MAJOR_LINE_TYPE "
	    "                    " "MAP_MINOR_LINE_TYPE                     " 
	    "MAP_MAJOR_LINE_INTERVAL                 " "MAP_MINOR_LINE_INTER"
	    "VAL                 " "MAP_MAJOR_LINE_POLAR_LIMIT              " 
	    "MAP_MINOR_LINE_POLAR_LIMIT              " "ENABLE_MAP_MAJOR_LIN"
	    "E                   " "ENABLE_MAP_MINOR_LINE                   " 
	    "MAP_BOUNDARY_INDEX                      " "MAP_BOUNDARY_LINE_MA"
	    "X                   " "****IGROUP                              " 
	    "MAP_OUTLINE_INDEX                       " "MAP_OUTLINE_TYPE    "
	    "                    " "ENABLE_GLOBAL_MAPPING                   " 
	    "----LWHINT                              " "SHADE PATTERN ON LAN"
	    "D                   " "SHADE PATTERN ON LAKE                   " 
	    "ENABLE_PAINT_LAKE                       ";

    /* System generated locals */
    address a__1[3];
    integer i__1[3];

    /* Builtin functions */
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen),
	     s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer n, id;
    extern integer lenc_(char *, ftnlen);
    static char cmsg[80];
    extern logical lchreq_(char *, char *, ftnlen, ftnlen);
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), umiqid_(char *, integer *, ftnlen), umlqid_(char 
	    *, integer *, ftnlen), umrqid_(char *, integer *, ftnlen), 
	    umiqvl_(integer *, integer *), umlqvl_(integer *, integer *), 
	    umisvl_(integer *, integer *), umlsvl_(integer *, integer *), 
	    umrqvl_(integer *, integer *), umrsvl_(integer *, integer *);

/*     / SHORT NAME / */
    switch(n__) {
	case 1: goto L_umpqid;
	case 2: goto L_umpqcp;
	case 3: goto L_umpqcl;
	case 4: goto L_umpqit;
	case 5: goto L_umpqvl;
	case 6: goto L_umpsvl;
	case 7: goto L_umpqin;
	}

/*     / LONG NAME / */
    *ncp = 20;
    return 0;
/* ----------------------------------------------------------------------- */

L_umpqid:
    for (n = 1; n <= 20; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *idx = n;
	    return 0;
	}
/* L10: */
    }
/* Writing concatenation */
    i__1[0] = 11, a__1[0] = "PARAMETER '";
    i__1[1] = lenc_(cp, cp_len), a__1[1] = cp;
    i__1[2] = 17, a__1[2] = "' IS NOT DEFINED.";
    s_cat(cmsg, a__1, i__1, &c__3, (ftnlen)80);
    msgdmp_("E", "UMPQID", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
    return 0;
/* ----------------------------------------------------------------------- */

L_umpqcp:
    if (1 <= *idx && *idx <= 20) {
	s_copy(cp, cparas + (*idx - 1 << 3), cp_len, (ftnlen)8);
    } else {
	msgdmp_("E", "UMPQCP", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_umpqcl:
    if (1 <= *idx && *idx <= 20) {
	s_copy(cp, cparal + (*idx - 1) * 40, cp_len, (ftnlen)40);
    } else {
	msgdmp_("E", "UMPQCL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_umpqit:
    if (1 <= *idx && *idx <= 20) {
	*itp = itype[*idx - 1];
    } else {
	msgdmp_("E", "UMPQIT", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_umpqvl:
    if (1 <= *idx && *idx <= 20) {
	if (itype[*idx - 1] == 1) {
	    umiqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    umiqvl_(&id, ipara);
	} else if (itype[*idx - 1] == 2) {
	    umlqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    umlqvl_(&id, ipara);
	} else if (itype[*idx - 1] == 3) {
	    umrqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    umrqvl_(&id, ipara);
	}
    } else {
	msgdmp_("E", "UMPQVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_umpsvl:
    if (1 <= *idx && *idx <= 20) {
	if (itype[*idx - 1] == 1) {
	    umiqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    umisvl_(&id, ipara);
	} else if (itype[*idx - 1] == 2) {
	    umlqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    umlsvl_(&id, ipara);
	} else if (itype[*idx - 1] == 3) {
	    umrqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    umrsvl_(&id, ipara);
	}
    } else {
	msgdmp_("E", "UMPSVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_umpqin:
    for (n = 1; n <= 20; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *in = n;
	    return 0;
	}
/* L20: */
    }
    *in = 0;
    return 0;
} /* umpqnp_ */

/* Subroutine */ int umpqnp_(integer *ncp)
{
    return umpqnp_0_(0, ncp, (char *)0, (integer *)0, (integer *)0, (integer *
	    )0, (integer *)0, (ftnint)0);
    }

/* Subroutine */ int umpqid_(char *cp, integer *idx, ftnlen cp_len)
{
    return umpqnp_0_(1, (integer *)0, cp, idx, (integer *)0, (integer *)0, (
	    integer *)0, cp_len);
    }

/* Subroutine */ int umpqcp_(integer *idx, char *cp, ftnlen cp_len)
{
    return umpqnp_0_(2, (integer *)0, cp, idx, (integer *)0, (integer *)0, (
	    integer *)0, cp_len);
    }

/* Subroutine */ int umpqcl_(integer *idx, char *cp, ftnlen cp_len)
{
    return umpqnp_0_(3, (integer *)0, cp, idx, (integer *)0, (integer *)0, (
	    integer *)0, cp_len);
    }

/* Subroutine */ int umpqit_(integer *idx, integer *itp)
{
    return umpqnp_0_(4, (integer *)0, (char *)0, idx, itp, (integer *)0, (
	    integer *)0, (ftnint)0);
    }

/* Subroutine */ int umpqvl_(integer *idx, integer *ipara)
{
    return umpqnp_0_(5, (integer *)0, (char *)0, idx, (integer *)0, ipara, (
	    integer *)0, (ftnint)0);
    }

/* Subroutine */ int umpsvl_(integer *idx, integer *ipara)
{
    return umpqnp_0_(6, (integer *)0, (char *)0, idx, (integer *)0, ipara, (
	    integer *)0, (ftnint)0);
    }

/* Subroutine */ int umpqin_(char *cp, integer *in, ftnlen cp_len)
{
    return umpqnp_0_(7, (integer *)0, cp, (integer *)0, (integer *)0, (
	    integer *)0, in, cp_len);
    }


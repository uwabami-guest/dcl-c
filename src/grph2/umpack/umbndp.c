/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int umbndp_(real *vxmin, real *vxmax, real *vymin, real *
	vymax)
{
    /* System generated locals */
    integer i__1;

    /* Local variables */
    static integer i__;
    static real vx, vy, uxz, uyz;
    static integer ndata;
    static real rundef;
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen), stftrf_(real 
	    *, real *, real *, real *), umqptn_(integer *), umqpnt_(integer *,
	     real *, real *);

    glrget_("RUNDEF", &rundef, (ftnlen)6);
    umqptn_(&ndata);
    if (ndata == 0) {
	return 0;
    }
    if (*vxmin == rundef) {
	umqpnt_(&c__1, &uxz, &uyz);
	stftrf_(&uxz, &uyz, vxmin, vymin);
	*vxmax = *vxmin;
	*vymax = *vymin;
    }
    i__1 = ndata;
    for (i__ = 1; i__ <= i__1; ++i__) {
	umqpnt_(&i__, &uxz, &uyz);
	stftrf_(&uxz, &uyz, &vx, &vy);
	*vxmin = min(*vxmin,vx);
	*vxmax = max(*vxmax,vx);
	*vymin = min(*vymin,vy);
	*vymax = max(*vymax,vy);
/* L40: */
    }
    return 0;
} /* umbndp_ */


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     UUSIDV */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uusidv_0_(int n__, real *umin, real *umax)
{
    extern /* Subroutine */ int uurget_(char *, real *, ftnlen), uurset_(char 
	    *, real *, ftnlen);

    switch(n__) {
	case 1: goto L_uuqidv;
	}

    uurset_("UMIN", umin, (ftnlen)4);
    uurset_("UMAX", umax, (ftnlen)4);
    return 0;
/* ----------------------------------------------------------------------- */

L_uuqidv:
    uurget_("UMIN", umin, (ftnlen)4);
    uurget_("UMAX", umax, (ftnlen)4);
    return 0;
} /* uusidv_ */

/* Subroutine */ int uusidv_(real *umin, real *umax)
{
    return uusidv_0_(0, umin, umax);
    }

/* Subroutine */ int uuqidv_(real *umin, real *umax)
{
    return uusidv_0_(1, umin, umax);
    }


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uuslnt_0_(int n__, integer *itype, integer *index)
{
    /* Initialized data */

    static integer itypez = 1;
    static integer indexz = 1;

    switch(n__) {
	case 1: goto L_uuqlnt;
	case 2: goto L_uuslni;
	case 3: goto L_uuqlni;
	}

    itypez = *itype;
    return 0;
/* ----------------------------------------------------------------------- */

L_uuqlnt:
    *itype = itypez;
    return 0;
/* ----------------------------------------------------------------------- */

L_uuslni:
    indexz = *index;
    return 0;
/* ----------------------------------------------------------------------- */

L_uuqlni:
    *index = indexz;
    return 0;
} /* uuslnt_ */

/* Subroutine */ int uuslnt_(integer *itype)
{
    return uuslnt_0_(0, itype, (integer *)0);
    }

/* Subroutine */ int uuqlnt_(integer *itype)
{
    return uuslnt_0_(1, itype, (integer *)0);
    }

/* Subroutine */ int uuslni_(integer *index)
{
    return uuslnt_0_(2, (integer *)0, index);
    }

/* Subroutine */ int uuqlni_(integer *index)
{
    return uuslnt_0_(3, (integer *)0, index);
    }


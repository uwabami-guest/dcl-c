/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uusarp_0_(int n__, integer *itpt1, integer *itpt2)
{
    /* Initialized data */

    static integer itpt1z = 201;
    static integer itpt2z = 401;

    switch(n__) {
	case 1: goto L_uuqarp;
	}

    itpt1z = *itpt1;
    itpt2z = *itpt2;
    return 0;
/* ----------------------------------------------------------------------- */

L_uuqarp:
    *itpt1 = itpt1z;
    *itpt2 = itpt2z;
    return 0;
} /* uusarp_ */

/* Subroutine */ int uusarp_(integer *itpt1, integer *itpt2)
{
    return uusarp_0_(0, itpt1, itpt2);
    }

/* Subroutine */ int uuqarp_(integer *itpt1, integer *itpt2)
{
    return uusarp_0_(1, itpt1, itpt2);
    }


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     UUPGET / UUPSET / UUPSTX */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uupget_0_(int n__, char *cp, integer *ipara, ftnlen 
	cp_len)
{
    static char cl[40];
    static integer ip;
    static char cx[8];
    static integer it, idx;
    extern /* Subroutine */ int rliget_(char *, integer *, integer *, ftnlen),
	     rllget_(char *, integer *, integer *, ftnlen), rtiget_(char *, 
	    char *, integer *, integer *, ftnlen, ftnlen), rlrget_(char *, 
	    integer *, integer *, ftnlen), uuiqid_(char *, integer *, ftnlen),
	     rtlget_(char *, char *, integer *, integer *, ftnlen, ftnlen), 
	    uulqid_(char *, integer *, ftnlen), uupqid_(char *, integer *, 
	    ftnlen), rtrget_(char *, char *, integer *, integer *, ftnlen, 
	    ftnlen), uupqcl_(integer *, char *, ftnlen), uurqid_(char *, 
	    integer *, ftnlen), uupqcp_(integer *, char *, ftnlen), uupqit_(
	    integer *, integer *), uuisvl_(integer *, integer *), uulsvl_(
	    integer *, integer *), uupqvl_(integer *, integer *), uupsvl_(
	    integer *, integer *), uursvl_(integer *, integer *);

    switch(n__) {
	case 1: goto L_uupset;
	case 2: goto L_uupstx;
	}

    uupqid_(cp, &idx, cp_len);
    uupqvl_(&idx, ipara);
    return 0;
/* ----------------------------------------------------------------------- */

L_uupset:
    uupqid_(cp, &idx, cp_len);
    uupsvl_(&idx, ipara);
    return 0;
/* ----------------------------------------------------------------------- */

L_uupstx:
    ip = *ipara;
    uupqid_(cp, &idx, cp_len);
    uupqit_(&idx, &it);
    uupqcp_(&idx, cx, (ftnlen)8);
    uupqcl_(&idx, cl, (ftnlen)40);
    if (it == 1) {
	rtiget_("UU", cx, &ip, &c__1, (ftnlen)2, (ftnlen)8);
	rliget_(cl, &ip, &c__1, (ftnlen)40);
	uuiqid_(cp, &idx, cp_len);
	uuisvl_(&idx, &ip);
    } else if (it == 2) {
	rtlget_("UU", cx, &ip, &c__1, (ftnlen)2, (ftnlen)8);
	rllget_(cl, &ip, &c__1, (ftnlen)40);
	uulqid_(cp, &idx, cp_len);
	uulsvl_(&idx, &ip);
    } else if (it == 3) {
	rtrget_("UU", cx, &ip, &c__1, (ftnlen)2, (ftnlen)8);
	rlrget_(cl, &ip, &c__1, (ftnlen)40);
	uurqid_(cp, &idx, cp_len);
	uursvl_(&idx, &ip);
    }
    return 0;
} /* uupget_ */

/* Subroutine */ int uupget_(char *cp, integer *ipara, ftnlen cp_len)
{
    return uupget_0_(0, cp, ipara, cp_len);
    }

/* Subroutine */ int uupset_(char *cp, integer *ipara, ftnlen cp_len)
{
    return uupget_0_(1, cp, ipara, cp_len);
    }

/* Subroutine */ int uupstx_(char *cp, integer *ipara, ftnlen cp_len)
{
    return uupget_0_(2, cp, ipara, cp_len);
    }


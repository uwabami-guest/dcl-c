/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uusmkt_0_(int n__, integer *itype, integer *index, real *
	rsize)
{
    /* Initialized data */

    static integer itypez = 1;
    static integer indexz = 1;
    static real rsizez = .01f;

    switch(n__) {
	case 1: goto L_uuqmkt;
	case 2: goto L_uusmki;
	case 3: goto L_uuqmki;
	case 4: goto L_uusmks;
	case 5: goto L_uuqmks;
	}

    itypez = *itype;
    return 0;
/* ----------------------------------------------------------------------- */

L_uuqmkt:
    *itype = itypez;
    return 0;
/* ----------------------------------------------------------------------- */

L_uusmki:
    indexz = *index;
    return 0;
/* ----------------------------------------------------------------------- */

L_uuqmki:
    *index = indexz;
    return 0;
/* ----------------------------------------------------------------------- */

L_uusmks:
    rsizez = *rsize;
    return 0;
/* ----------------------------------------------------------------------- */

L_uuqmks:
    *rsize = rsizez;
    return 0;
} /* uusmkt_ */

/* Subroutine */ int uusmkt_(integer *itype)
{
    return uusmkt_0_(0, itype, (integer *)0, (real *)0);
    }

/* Subroutine */ int uuqmkt_(integer *itype)
{
    return uusmkt_0_(1, itype, (integer *)0, (real *)0);
    }

/* Subroutine */ int uusmki_(integer *index)
{
    return uusmkt_0_(2, (integer *)0, index, (real *)0);
    }

/* Subroutine */ int uuqmki_(integer *index)
{
    return uusmkt_0_(3, (integer *)0, index, (real *)0);
    }

/* Subroutine */ int uusmks_(real *rsize)
{
    return uusmkt_0_(4, (integer *)0, (integer *)0, rsize);
    }

/* Subroutine */ int uuqmks_(real *rsize)
{
    return uusmkt_0_(5, (integer *)0, (integer *)0, rsize);
    }


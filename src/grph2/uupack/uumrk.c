/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     POLYMARKER PRIMITIVE */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uumrk_(integer *n, real *upx, real *upy)
{
    static integer index, itype;
    static real rsize;
    extern /* Subroutine */ int uuqmki_(integer *), uuqmks_(real *), uuqmkt_(
	    integer *), uumrkz_(integer *, real *, real *, integer *, integer 
	    *, real *);

    /* Parameter adjustments */
    --upy;
    --upx;

    /* Function Body */
    uuqmkt_(&itype);
    uuqmki_(&index);
    uuqmks_(&rsize);
    uumrkz_(n, &upx[1], &upy[1], &itype, &index, &rsize);
    return 0;
} /* uumrk_ */


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__3 = 3;

/* ----------------------------------------------------------------------- */
/*     UIIQNP / UIIQID / UIIQCP / UIIQVL / UIISVL */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uiiqnp_0_(int n__, integer *ncp, char *cp, integer *idx, 
	integer *ipara, integer *in, ftnlen cp_len)
{
    /* Initialized data */

    static char cparas[8*3] = "ICOLOR1 " "ICOLOR2 " "ICOLOR3 ";
    static integer ix[3] = { 16777215,16777215,16777215 };
    static char cparal[40*3] = "COLOR_FOR_OUT_OF_DOMAIN                 " 
	    "COLOR_FOR_OUT_OF_GRID                   " "COLOR_FOR_MISSING_VA"
	    "LUE                 ";
    static logical lfirst = TRUE_;

    /* System generated locals */
    address a__1[3];
    integer i__1[3];

    /* Builtin functions */
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen),
	     s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer n;
    extern integer lenc_(char *, ftnlen);
    static char cmsg[80];
    extern logical lchreq_(char *, char *, ftnlen, ftnlen);
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), rliget_(char *, integer *, integer *, ftnlen), 
	    rtiget_(char *, char *, integer *, integer *, ftnlen, ftnlen);

/*     / SHORT NAME / */
    switch(n__) {
	case 1: goto L_uiiqid;
	case 2: goto L_uiiqcp;
	case 3: goto L_uiiqcl;
	case 4: goto L_uiiqvl;
	case 5: goto L_uiisvl;
	case 6: goto L_uiiqin;
	}

/*     / LONG NAME / */
    *ncp = 3;
    return 0;
/* ----------------------------------------------------------------------- */

L_uiiqid:
    for (n = 1; n <= 3; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *idx = n;
	    return 0;
	}
/* L10: */
    }
/* Writing concatenation */
    i__1[0] = 11, a__1[0] = "PARAMETER '";
    i__1[1] = lenc_(cp, cp_len), a__1[1] = cp;
    i__1[2] = 17, a__1[2] = "' IS NOT DEFINED.";
    s_cat(cmsg, a__1, i__1, &c__3, (ftnlen)80);
    msgdmp_("E", "UIIQID", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
    return 0;
/* ----------------------------------------------------------------------- */

L_uiiqcp:
    if (1 <= *idx && *idx <= 3) {
	s_copy(cp, cparas + (*idx - 1 << 3), cp_len, (ftnlen)8);
    } else {
	msgdmp_("E", "UIIQCP", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_uiiqcl:
    if (1 <= *idx && *idx <= 3) {
	s_copy(cp, cparal + (*idx - 1) * 40, cp_len, (ftnlen)40);
    } else {
	msgdmp_("E", "UIIQCL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_uiiqvl:
    if (lfirst) {
	rtiget_("UI", cparas, ix, &c__3, (ftnlen)2, (ftnlen)8);
	rliget_(cparal, ix, &c__3, (ftnlen)40);
	lfirst = FALSE_;
    }
    if (1 <= *idx && *idx <= 3) {
	*ipara = ix[*idx - 1];
    } else {
	msgdmp_("E", "UIIQVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_uiisvl:
    if (lfirst) {
	rtiget_("UI", cparas, ix, &c__3, (ftnlen)2, (ftnlen)8);
	rliget_(cparal, ix, &c__3, (ftnlen)40);
	lfirst = FALSE_;
    }
    if (1 <= *idx && *idx <= 3) {
	ix[*idx - 1] = *ipara;
    } else {
	msgdmp_("E", "UIISVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_uiiqin:
    for (n = 1; n <= 3; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *in = n;
	    return 0;
	}
/* L20: */
    }
    *in = 0;
    return 0;
} /* uiiqnp_ */

/* Subroutine */ int uiiqnp_(integer *ncp)
{
    return uiiqnp_0_(0, ncp, (char *)0, (integer *)0, (integer *)0, (integer *
	    )0, (ftnint)0);
    }

/* Subroutine */ int uiiqid_(char *cp, integer *idx, ftnlen cp_len)
{
    return uiiqnp_0_(1, (integer *)0, cp, idx, (integer *)0, (integer *)0, 
	    cp_len);
    }

/* Subroutine */ int uiiqcp_(integer *idx, char *cp, ftnlen cp_len)
{
    return uiiqnp_0_(2, (integer *)0, cp, idx, (integer *)0, (integer *)0, 
	    cp_len);
    }

/* Subroutine */ int uiiqcl_(integer *idx, char *cp, ftnlen cp_len)
{
    return uiiqnp_0_(3, (integer *)0, cp, idx, (integer *)0, (integer *)0, 
	    cp_len);
    }

/* Subroutine */ int uiiqvl_(integer *idx, integer *ipara)
{
    return uiiqnp_0_(4, (integer *)0, (char *)0, idx, ipara, (integer *)0, (
	    ftnint)0);
    }

/* Subroutine */ int uiisvl_(integer *idx, integer *ipara)
{
    return uiiqnp_0_(5, (integer *)0, (char *)0, idx, ipara, (integer *)0, (
	    ftnint)0);
    }

/* Subroutine */ int uiiqin_(char *cp, integer *in, ftnlen cp_len)
{
    return uiiqnp_0_(6, (integer *)0, cp, (integer *)0, (integer *)0, in, 
	    cp_len);
    }


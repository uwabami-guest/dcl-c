/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__4000 = 4000;
static integer c__2 = 2;

/* ----------------------------------------------------------------------- */
/* Subroutine */ int uipda2_(real *u, real *v, integer *mx, integer *nx, 
	integer *ny)
{
    /* System generated locals */
    integer u_dim1, u_offset, v_dim1, v_offset;

    /* Local variables */
    static integer image[4000];
    extern /* Subroutine */ int uipd2z_(real *, real *, integer *, integer *, 
	    integer *, integer *, integer *);

    /* Parameter adjustments */
    v_dim1 = *mx;
    v_offset = 1 + v_dim1;
    v -= v_offset;
    u_dim1 = *mx;
    u_offset = 1 + u_dim1;
    u -= u_offset;

    /* Function Body */
    uipd2z_(&u[u_offset], &v[v_offset], mx, nx, ny, image, &c__4000);
    return 0;
} /* uipda2_ */

/* ----------------------------------------------------------------------- */
/* Subroutine */ int uipd2z_(real *u, real *v, integer *mx, integer *nx, 
	integer *ny, integer *image, integer *maxpxl)
{
    /* Initialized data */

    static integer np[2] = { 1,1 };

    /* System generated locals */
    integer u_dim1, u_offset, v_dim1, v_offset, i__1, i__2, i__3, i__4;

    /* Local variables */
    static integer i__, j, nq[2];
    static real fx;
    static integer ns[2], ix, iy;
    static real fy, rx, ry, uu, ux, vx, wx, wy, vy, uy, vv;
    static integer ix1, iy1, ix2, iy2, ix3, iy3, ix4, iy4;
    static real wx1, wy1, wx2, wy2, wx3, wy3, wx4, wy4;
    static logical lcx, lcy;
    static integer ngx, ngy;
    static logical lfcc, limc, lcnr;
    static real umin, vmin, umax, vmax;
    extern /* Subroutine */ int uic2d_(real *, real *, integer *);
    static integer ixmin, iymin, ixmax;
    static logical lmiss;
    static integer iymax;
    extern real rvmin_(real *, integer *, integer *, integer *, integer *);
    static real rmiss;
    extern real rvmax_(real *, integer *, integer *, integer *, integer *);
    static real vxmin, vymin, vxmax, vymax;
    extern /* Subroutine */ int ui2ini_(real *, real *, real *, real *), 
	    stfpr2_(real *, real *, real *, real *), stipr2_(real *, real *, 
	    real *, real *);
    static integer iundef;
    extern /* Subroutine */ int gliget_(char *, integer *, ftnlen);
    static integer ihight;
    extern /* Subroutine */ int gllget_(char *, logical *, ftnlen);
    static real rundef;
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen), swqfcc_(
	    logical *), msgdmp_(char *, char *, char *, ftnlen, ftnlen, 
	    ftnlen), uiiget_(char *, integer *, ftnlen), uilget_(char *, 
	    logical *, ftnlen);
    static integer iwidth;
    extern /* Subroutine */ int swqimc_(logical *), swiclr_(integer *, 
	    integer *), uwdflt_(integer *, integer *), swicls_(void), swfint_(
	    real *, real *, integer *, integer *), stitrf_(real *, real *, 
	    real *, real *), swiint_(integer *, integer *, real *, real *), 
	    swiopn_(integer *, integer *, integer *, integer *, real *, real *
	    , real *, real *, real *, real *, real *, real *), sgqvpt_(real *,
	     real *, real *, real *), uwqgxi_(real *, integer *, real *), 
	    uwqgyi_(real *, integer *, real *), stfwtr_(real *, real *, real *
	    , real *), stiwtr_(real *, real *, real *, real *);
    static integer icolor1, icolor2, icolor3;

    /* Parameter adjustments */
    v_dim1 = *mx;
    v_offset = 1 + v_dim1;
    v -= v_offset;
    u_dim1 = *mx;
    u_offset = 1 + u_dim1;
    u -= u_offset;
    --image;

    /* Function Body */
/*     / CHECK IMAGE CAPABILITY / */
    swqimc_(&limc);
    if (! limc) {
	msgdmp_("E", "UIPGR2", "NO IMAGE CAPABILITY.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    swqfcc_(&lfcc);
    if (! lfcc) {
	msgdmp_("E", "UIPGR2", "NO FULL COLOR CAPABILITY.", (ftnlen)1, (
		ftnlen)6, (ftnlen)25);
    }
/*     / GET INTERNAL PARAMETERS / */
    glrget_("RUNDEF  ", &rundef, (ftnlen)8);
    gliget_("IUNDEF  ", &iundef, (ftnlen)8);
    gllget_("LMISS   ", &lmiss, (ftnlen)8);
    glrget_("RMISS   ", &rmiss, (ftnlen)8);
    uiiget_("ICOLOR1 ", &icolor1, (ftnlen)8);
    uiiget_("ICOLOR2 ", &icolor2, (ftnlen)8);
    uiiget_("ICOLOR3 ", &icolor3, (ftnlen)8);
    uilget_("LCELLX  ", &lcx, (ftnlen)8);
    uilget_("LCELLY  ", &lcy, (ftnlen)8);
    uilget_("LCORNER ", &lcnr, (ftnlen)8);
/*     / SET GRID ATTRIBUTE IF IT HAS NOT BEEN SET YET / */
    ngx = *nx;
    ngy = *ny;
    if (lcx) {
	++ngx;
    }
    if (lcy) {
	++ngy;
    }
    uwdflt_(&ngx, &ngy);
/*     / INITIALIZE / */
    sgqvpt_(&vxmin, &vxmax, &vymin, &vymax);
    stfpr2_(&vxmin, &vymin, &rx, &ry);
    stfwtr_(&rx, &ry, &wx1, &wy1);
    swfint_(&wx1, &wy1, &ix1, &iy1);
    stfpr2_(&vxmax, &vymin, &rx, &ry);
    stfwtr_(&rx, &ry, &wx2, &wy2);
    swfint_(&wx2, &wy2, &ix2, &iy2);
    stfpr2_(&vxmax, &vymax, &rx, &ry);
    stfwtr_(&rx, &ry, &wx3, &wy3);
    swfint_(&wx3, &wy3, &ix3, &iy3);
    stfpr2_(&vxmin, &vymax, &rx, &ry);
    stfwtr_(&rx, &ry, &wx4, &wy4);
    swfint_(&wx4, &wy4, &ix4, &iy4);
/* Computing MIN */
    i__1 = min(ix1,ix2), i__1 = min(i__1,ix3);
    ixmin = min(i__1,ix4);
/* Computing MIN */
    i__1 = min(iy1,iy2), i__1 = min(i__1,iy3);
    iymin = min(i__1,iy4);
/* Computing MAX */
    i__1 = max(ix1,ix2), i__1 = max(i__1,ix3);
    ixmax = max(i__1,ix4);
/* Computing MAX */
    i__1 = max(iy1,iy2), i__1 = max(i__1,iy3);
    iymax = max(i__1,iy4);
    iwidth = ixmax - ixmin + 1;
    ihight = iymax - iymin + 1;
    swiopn_(&ixmin, &iymin, &iwidth, &ihight, &wx1, &wy1, &wx2, &wy2, &wx3, &
	    wy3, &wx4, &wy4);
    ns[0] = *mx;
    ns[1] = *ny;
    nq[0] = *nx;
    nq[1] = *ny;
    umin = rvmin_(&u[u_offset], ns, np, nq, &c__2);
    umax = rvmax_(&u[u_offset], ns, np, nq, &c__2);
    vmin = rvmin_(&v[v_offset], ns, np, nq, &c__2);
    vmax = rvmax_(&v[v_offset], ns, np, nq, &c__2);
    ui2ini_(&umin, &umax, &vmin, &vmax);
/*     / LOOP FOR EACH PIXEL / */
    i__1 = ihight;
    for (j = 1; j <= i__1; ++j) {
	i__2 = iwidth;
	for (i__ = 1; i__ <= i__2; ++i__) {
	    i__3 = i__ + ixmin - 1;
	    i__4 = j + iymin - 1;
	    swiint_(&i__3, &i__4, &wx, &wy);
	    stiwtr_(&wx, &wy, &rx, &ry);
	    stipr2_(&rx, &ry, &vx, &vy);
	    stitrf_(&vx, &vy, &ux, &uy);
	    if (ux == rundef) {
		image[i__] = icolor1;
	    } else {
		uwqgxi_(&ux, &ix, &fx);
		uwqgyi_(&uy, &iy, &fy);
		if (ix == iundef || iy == iundef) {
		    image[i__] = icolor2;
		} else {
		    if (lcx && lcy) {
			if (lmiss && (u[ix + iy * u_dim1] == rmiss || v[ix + 
				iy * v_dim1] == rmiss)) {
			    image[i__] = icolor3;
			} else {
			    uic2d_(&u[ix + iy * u_dim1], &v[ix + iy * v_dim1],
				     &image[i__]);
			}
		    } else if (lcx && ! lcy) {
			if (lmiss && (u[ix + iy * u_dim1] == rmiss || u[ix + (
				iy + 1) * u_dim1] == rmiss || v[ix + iy * 
				v_dim1] == rmiss || v[ix + (iy + 1) * v_dim1] 
				== rmiss)) {
			    image[i__] = icolor3;
			} else {
			    uu = u[ix + iy * u_dim1] * (1 - fy) + u[ix + (iy 
				    + 1) * u_dim1] * fy;
			    vv = v[ix + iy * v_dim1] * (1 - fy) + v[ix + (iy 
				    + 1) * v_dim1] * fy;
			    uic2d_(&uu, &vv, &image[i__]);
			}
		    } else if (! lcx && lcy) {
			if (lmiss && (u[ix + iy * u_dim1] == rmiss || u[ix + 
				1 + iy * u_dim1] == rmiss || v[ix + iy * 
				v_dim1] == rmiss || v[ix + 1 + iy * v_dim1] ==
				 rmiss)) {
			    image[i__] = icolor3;
			} else {
			    uu = u[ix + iy * u_dim1] * (1 - fx) + u[ix + 1 + 
				    iy * u_dim1] * fx;
			    vv = v[ix + iy * v_dim1] * (1 - fx) + v[ix + 1 + 
				    iy * v_dim1] * fx;
			    uic2d_(&uu, &vv, &image[i__]);
			}
		    } else if (! lcx && ! lcy) {
			if (! lmiss || u[ix + iy * u_dim1] != rmiss && u[ix + 
				1 + iy * u_dim1] != rmiss && u[ix + (iy + 1) *
				 u_dim1] != rmiss && u[ix + 1 + (iy + 1) * 
				u_dim1] != rmiss && v[ix + iy * v_dim1] != 
				rmiss && v[ix + 1 + iy * v_dim1] != rmiss && 
				v[ix + (iy + 1) * v_dim1] != rmiss && v[ix + 
				1 + (iy + 1) * v_dim1] != rmiss) {
			    uu = (u[ix + iy * u_dim1] * (1 - fx) + u[ix + 1 + 
				    iy * u_dim1] * fx) * (1 - fy) + (u[ix + (
				    iy + 1) * u_dim1] * (1 - fx) + u[ix + 1 + 
				    (iy + 1) * u_dim1] * fx) * fy;
			    vv = (v[ix + iy * v_dim1] * (1 - fx) + v[ix + 1 + 
				    iy * v_dim1] * fx) * (1 - fy) + (v[ix + (
				    iy + 1) * v_dim1] * (1 - fx) + v[ix + 1 + 
				    (iy + 1) * v_dim1] * fx) * fy;
			    uic2d_(&uu, &vv, &image[i__]);
			} else if (lcnr && fx + fy <= 1.f && (u[ix + iy * 
				u_dim1] != rmiss && u[ix + 1 + iy * u_dim1] !=
				 rmiss && u[ix + (iy + 1) * u_dim1] != rmiss 
				&& u[ix + iy * u_dim1] != rmiss && u[ix + 1 + 
				iy * u_dim1] != rmiss && u[ix + (iy + 1) * 
				u_dim1] != rmiss)) {
			    uu = u[ix + iy * u_dim1] * (1 - fx - fy) + u[ix + 
				    1 + iy * u_dim1] * fx + u[ix + (iy + 1) * 
				    u_dim1] * fy;
			    vv = v[ix + iy * v_dim1] * (1 - fx - fy) + v[ix + 
				    1 + iy * v_dim1] * fx + v[ix + (iy + 1) * 
				    v_dim1] * fy;
			    uic2d_(&uu, &vv, &image[i__]);
			} else if (lcnr && 1 - fx + fy <= 1.f && (u[ix + iy * 
				u_dim1] != rmiss && u[ix + 1 + iy * u_dim1] !=
				 rmiss && u[ix + 1 + (iy + 1) * u_dim1] != 
				rmiss && v[ix + iy * v_dim1] != rmiss && v[ix 
				+ 1 + iy * v_dim1] != rmiss && v[ix + 1 + (iy 
				+ 1) * v_dim1] != rmiss)) {
			    uu = u[ix + 1 + iy * u_dim1] * (fx - fy) + u[ix + 
				    iy * u_dim1] * (1 - fx) + u[ix + 1 + (iy 
				    + 1) * u_dim1] * fy;
			    vv = v[ix + 1 + iy * v_dim1] * (fx - fy) + v[ix + 
				    iy * v_dim1] * (1 - fx) + v[ix + 1 + (iy 
				    + 1) * v_dim1] * fy;
			    uic2d_(&uu, &vv, &image[i__]);
			} else if (lcnr && fx + 1 - fy <= 1.f && (u[ix + iy * 
				u_dim1] != rmiss && u[ix + (iy + 1) * u_dim1] 
				!= rmiss && u[ix + 1 + (iy + 1) * u_dim1] != 
				rmiss && v[ix + iy * v_dim1] != rmiss && v[ix 
				+ (iy + 1) * v_dim1] != rmiss && v[ix + 1 + (
				iy + 1) * v_dim1] != rmiss)) {
			    uu = u[ix + (iy + 1) * u_dim1] * (fy - fx) + u[ix 
				    + 1 + (iy + 1) * u_dim1] * fx + u[ix + iy 
				    * u_dim1] * (1 - fy);
			    vv = v[ix + (iy + 1) * v_dim1] * (fy - fx) + v[ix 
				    + 1 + (iy + 1) * v_dim1] * fx + v[ix + iy 
				    * v_dim1] * (1 - fy);
			    uic2d_(&uu, &vv, &image[i__]);
			} else if (lcnr && 2 - fx - fy <= 1.f && (u[ix + 1 + 
				iy * u_dim1] != rmiss && u[ix + (iy + 1) * 
				u_dim1] != rmiss && u[ix + 1 + (iy + 1) * 
				u_dim1] != rmiss && v[ix + 1 + iy * v_dim1] !=
				 rmiss && v[ix + (iy + 1) * v_dim1] != rmiss 
				&& v[ix + 1 + (iy + 1) * v_dim1] != rmiss)) {
			    uu = u[ix + 1 + (iy + 1) * u_dim1] * (fx + fy - 1)
				     + u[ix + (iy + 1) * u_dim1] * (1 - fx) + 
				    u[ix + 1 + iy * u_dim1] * (1 - fy);
			    vv = v[ix + 1 + (iy + 1) * v_dim1] * (fx + fy - 1)
				     + v[ix + (iy + 1) * v_dim1] * (1 - fx) + 
				    v[ix + 1 + iy * v_dim1] * (1 - fy);
			    uic2d_(&uu, &vv, &image[i__]);
			} else {
			    image[i__] = icolor3;
			}
		    }
		}
	    }
/* L20: */
	}
	swiclr_(&image[1], &iwidth);
/* L30: */
    }
    swicls_();
    return 0;
} /* uipd2z_ */


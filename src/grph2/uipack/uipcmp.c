/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;
static logical c_false = FALSE_;
static integer c__2 = 2;
static integer c__3 = 3;

/* --------------------------------------------------------------- */
/* Subroutine */ int uipcmp_(real *xmin, real *xmax, real *ymin, real *ymax, 
	char *cpos, ftnlen cpos_len)
{
    static real u[4]	/* was [2][2] */, v[4]	/* was [2][2] */, u1, u2, v1, 
	    v2;
    static logical lem, lcx, lcy;
    extern /* Subroutine */ int grfig_(void), uipda2_(real *, real *, integer 
	    *, integer *, integer *), uiqcr2_(real *, real *, real *, real *),
	     uilget_(char *, logical *, ftnlen), sgswnd_(real *, real *, real 
	    *, real *), uilset_(char *, logical *, ftnlen), usdaxs_(void), 
	    sgstrf_(void), uwsgxb_(real *, real *, integer *), sgstrn_(
	    integer *), uwsgyb_(real *, real *, integer *), sgsvpt_(real *, 
	    real *, real *, real *), slpvpr_(integer *);

    uiqcr2_(&u1, &u2, &v1, &v2);
    u[0] = u1;
    u[2] = u1;
    u[1] = u2;
    u[3] = u2;
    v[0] = v1;
    v[2] = v2;
    v[1] = v1;
    v[3] = v2;
    grfig_();
    sgstrn_(&c__1);
    sgsvpt_(xmin, xmax, ymin, ymax);
    sgswnd_(&u1, &u2, &v1, &v2);
    sgstrf_();
    uilget_("CELL_MODE_X", &lcx, (ftnlen)11);
    uilget_("CELL_MODE_Y", &lcy, (ftnlen)11);
    uilget_("EMBOSS     ", &lem, (ftnlen)11);
    uilset_("CELL_MODE_X", &c_false, (ftnlen)11);
    uilset_("CELL_MODE_Y", &c_false, (ftnlen)11);
    uilset_("EMBOSS     ", &c_false, (ftnlen)11);
    uwsgxb_(&u1, &u2, &c__2);
    uwsgyb_(&v1, &v2, &c__2);
    uipda2_(u, v, &c__2, &c__2, &c__2);
    slpvpr_(&c__3);
    usdaxs_();
    uilset_("CELL_MODE_X", &lcx, (ftnlen)11);
    uilset_("CELL_MODE_Y", &lcy, (ftnlen)11);
    uilset_("EMBOSS     ", &lem, (ftnlen)11);
    return 0;
} /* uipcmp_ */


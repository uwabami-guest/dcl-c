/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Subroutine */ int uic3d_0_(int n__, real *u, real *v, real *w, integer *
	irgb, real *umin, real *umax, real *vmin, real *vmax, real *wmin, 
	real *wmax)
{
    /* Initialized data */

    static integer ir1 = 0;
    static integer ir2 = 255;
    static integer ig1 = 0;
    static integer ig2 = 255;
    static integer ib1 = 0;
    static integer ib2 = 255;
    static logical lset = FALSE_;

    static real u1, u2, v1, v2, w1, w2;
    static integer jb, jg;
    static real du, dv, dw;
    static integer jr;
    extern /* Subroutine */ int uifpac_(integer *, integer *, integer *, 
	    integer *);

    switch(n__) {
	case 1: goto L_ui3ini;
	}

    jr = ir1 - *u * (ir1 - ir2);
    jg = ig1 - *v * (ig1 - ig2);
    jb = ib1 - *w * (ib1 - ib2);
    uifpac_(&jr, &jg, &jb, irgb);
    return 0;
/* ----------------------------------------------------------------------- */

L_ui3ini:
    if (! lset) {
	u1 = *umin;
	u2 = *umax;
	v1 = *vmin;
	v2 = *vmax;
	w1 = *wmin;
	w2 = *wmax;
    }
    du = u2 - u1;
    dv = v2 - v1;
    dw = w2 - w1;
    return 0;
} /* uic3d_ */

/* Subroutine */ int uic3d_(real *u, real *v, real *w, integer *irgb)
{
    return uic3d_0_(0, u, v, w, irgb, (real *)0, (real *)0, (real *)0, (real *
	    )0, (real *)0, (real *)0);
    }

/* Subroutine */ int ui3ini_(real *umin, real *umax, real *vmin, real *vmax, 
	real *wmin, real *wmax)
{
    return uic3d_0_(1, (real *)0, (real *)0, (real *)0, (integer *)0, umin, 
	    umax, vmin, vmax, wmin, wmax);
    }


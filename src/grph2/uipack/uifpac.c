/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/* Subroutine */ int uifpac_0_(int n__, integer *ir, integer *ig, integer *ib,
	 integer *irgb)
{
    /* Builtin functions */
    integer lbit_shift(integer, integer);

    switch(n__) {
	case 1: goto L_uiipac;
	}

    *irgb = *ir << 16 | (*ig << 8 | *ib);
    return 0;
/* ----------------------------------------------------------------------- */

L_uiipac:
    *ir = lbit_shift(*irgb, (ftnlen)-16) & 255;
    *ig = lbit_shift(*irgb, (ftnlen)-8) & 255;
    *ib = *irgb & 255;
    return 0;
} /* uifpac_ */

/* Subroutine */ int uifpac_(integer *ir, integer *ig, integer *ib, integer *
	irgb)
{
    return uifpac_0_(0, ir, ig, ib, irgb);
    }

/* Subroutine */ int uiipac_(integer *irgb, integer *ir, integer *ig, integer 
	*ib)
{
    return uifpac_0_(1, ir, ig, ib, irgb);
    }


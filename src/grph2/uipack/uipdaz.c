/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__2 = 2;
static real c_b33 = -1.5707500000000001f;
static real c_b34 = 0.f;
static real c_b38 = 1.f;

/* ----------------------------------------------------------------------- */
/* Subroutine */ int uipdaz_(real *z__, integer *mx, integer *nx, integer *ny,
	 integer *image, integer *maxpxl)
{
    /* Initialized data */

    static integer np[2] = { 1,1 };

    /* System generated locals */
    integer z_dim1, z_offset, i__1, i__2, i__3, i__4, i__5;
    real r__1, r__2;

    /* Builtin functions */
    double cos(doublereal), sin(doublereal);

    /* Local variables */
    static integer i__, j;
    static real x1, y1, y2, x2;
    static integer nn;
    static real dx;
    static integer nq[2];
    static real dy;
    static integer ns[2], ix, iy;
    static real ex, ey, ez, rx, ry, fx, ux, vx, wx, wy, vy, yy, zx, zy, uy, 
	    fy, xx, zz, ex0, ey0, ez0;
    static integer ix1, iy1, ix2, iy2, ix3, iy3, ix4, iy4;
    static real wx1, wy1, wx2, wy2, wx3, wy3, wx4, wy4, ave, phi, the;
    static logical lcx, lcy;
    static integer ngx, ngy, itr;
    extern /* Subroutine */ int cr3c_(real *, real *, real *, real *, real *, 
	    real *, real *, real *, real *);
    static logical lfcc, ldeg, limc, lmap;
    static real cmsk;
    static logical lcnr;
    static real zmin, zmax, theta;
    static logical lsphe, lshdw;
    static integer ixmin, iymin, ixmax;
    static logical lmiss;
    static integer iymax;
    extern real rvmin_(real *, integer *, integer *, integer *, integer *);
    static real rmiss;
    extern real rvmax_(real *, integer *, integer *, integer *, integer *);
    static integer nxint;
    static real vxmin, vymin, vxmax, vymax;
    static integer nyint;
    extern real ruwgx_(integer *), ruwgy_(integer *);
    extern /* Subroutine */ int stfpr2_(real *, real *, real *, real *), 
	    stipr2_(real *, real *, real *, real *);
    static real height;
    static integer iundef;
    extern /* Subroutine */ int gliget_(char *, integer *, ftnlen);
    static integer ihight;
    extern /* Subroutine */ int uicrgb_(real *, real *, integer *), gllget_(
	    char *, logical *, ftnlen), uicini_(real *, real *), sgiget_(char 
	    *, integer *, ftnlen);
    static real rundef;
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen), sglget_(char 
	    *, logical *, ftnlen), swqfcc_(logical *), msgdmp_(char *, char *,
	     char *, ftnlen, ftnlen, ftnlen), uiiget_(char *, integer *, 
	    ftnlen), uilget_(char *, logical *, ftnlen);
    static integer iwidth;
    extern /* Subroutine */ int uirget_(char *, real *, ftnlen), swqimc_(
	    logical *), uishdw_(real *, real *, real *, real *, real *, real *
	    ), uwdflt_(integer *, integer *);
    static real densty;
    extern /* Subroutine */ int swiclr_(integer *, integer *), swicls_(void), 
	    swfint_(real *, real *, integer *, integer *), stitrf_(real *, 
	    real *, real *, real *), swiint_(integer *, integer *, real *, 
	    real *), swiopn_(integer *, integer *, integer *, integer *, real 
	    *, real *, real *, real *, real *, real *, real *, real *), 
	    sgqvpt_(real *, real *, real *, real *), uwqgxi_(real *, integer *
	    , real *), uwqgyi_(real *, integer *, real *), stfwtr_(real *, 
	    real *, real *, real *), stiwtr_(real *, real *, real *, real *);
    static integer icolor1, icolor2, icolor3;

    /* Parameter adjustments */
    z_dim1 = *mx;
    z_offset = 1 + z_dim1;
    z__ -= z_offset;
    --image;

    /* Function Body */
/*     / CHECK IMAGE CAPABILITY / */
    swqimc_(&limc);
    if (! limc) {
	msgdmp_("E", "UIPDAT", "NO IMAGE CAPABILITY.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    swqfcc_(&lfcc);
    if (! lfcc) {
	msgdmp_("E", "UIPDAT", "NO FULL COLOR CAPABILITY.", (ftnlen)1, (
		ftnlen)6, (ftnlen)25);
    }
/*     / GET INTERNAL PARAMETERS / */
    glrget_("RUNDEF  ", &rundef, (ftnlen)8);
    gliget_("IUNDEF  ", &iundef, (ftnlen)8);
    gllget_("LMISS   ", &lmiss, (ftnlen)8);
    glrget_("RMISS   ", &rmiss, (ftnlen)8);
    uiiget_("ICOLOR1 ", &icolor1, (ftnlen)8);
    uiiget_("ICOLOR2 ", &icolor2, (ftnlen)8);
    uiiget_("ICOLOR3 ", &icolor3, (ftnlen)8);
    uilget_("LCELLX  ", &lcx, (ftnlen)8);
    uilget_("LCELLY  ", &lcy, (ftnlen)8);
    uilget_("LCORNER ", &lcnr, (ftnlen)8);
    uilget_("LEMBOSS ", &lshdw, (ftnlen)8);
    uilget_("LSPHERE ", &lsphe, (ftnlen)8);
    uirget_("DENSITY ", &densty, (ftnlen)8);
/*     / SET GRID ATTRIBUTE IF IT HAS NOT BEEN SET YET / */
    ngx = *nx;
    ngy = *ny;
    if (lcx) {
	++ngx;
    }
    if (lcy) {
	++ngy;
    }
    uwdflt_(&ngx, &ngy);
/*     / INITIALIZE / */
    sgqvpt_(&vxmin, &vxmax, &vymin, &vymax);
    stfpr2_(&vxmin, &vymin, &rx, &ry);
    stfwtr_(&rx, &ry, &wx1, &wy1);
    swfint_(&wx1, &wy1, &ix1, &iy1);
    stfpr2_(&vxmax, &vymin, &rx, &ry);
    stfwtr_(&rx, &ry, &wx2, &wy2);
    swfint_(&wx2, &wy2, &ix2, &iy2);
    stfpr2_(&vxmax, &vymax, &rx, &ry);
    stfwtr_(&rx, &ry, &wx3, &wy3);
    swfint_(&wx3, &wy3, &ix3, &iy3);
    stfpr2_(&vxmin, &vymax, &rx, &ry);
    stfwtr_(&rx, &ry, &wx4, &wy4);
    swfint_(&wx4, &wy4, &ix4, &iy4);
/* Computing MIN */
    i__1 = min(ix1,ix2), i__1 = min(i__1,ix3);
    ixmin = min(i__1,ix4);
/* Computing MIN */
    i__1 = min(iy1,iy2), i__1 = min(i__1,iy3);
    iymin = min(i__1,iy4);
/* Computing MAX */
    i__1 = max(ix1,ix2), i__1 = max(i__1,ix3);
    ixmax = max(i__1,ix4);
/* Computing MAX */
    i__1 = max(iy1,iy2), i__1 = max(i__1,iy3);
    iymax = max(i__1,iy4);
    iwidth = ixmax - ixmin + 1;
    ihight = iymax - iymin + 1;
    swiopn_(&ixmin, &iymin, &iwidth, &ihight, &wx1, &wy1, &wx2, &wy2, &wx3, &
	    wy3, &wx4, &wy4);
    ns[0] = *mx;
    ns[1] = *ny;
    nq[0] = *nx;
    nq[1] = *ny;
    zmin = rvmin_(&z__[z_offset], ns, np, nq, &c__2);
    zmax = rvmax_(&z__[z_offset], ns, np, nq, &c__2);
    uicini_(&zmin, &zmax);
    if (lshdw) {
	sgiget_("ITR ", &itr, (ftnlen)4);
	sglget_("LDEG", &ldeg, (ftnlen)4);
	lmap = 10 <= itr && itr <= 39;
	ave = 0.f;
	nn = 0.f;
/* Computing MAX */
	i__1 = 1, i__2 = *nx / 500;
	nxint = max(i__1,i__2);
/* Computing MAX */
	i__1 = 1, i__2 = *ny / 500;
	nyint = max(i__1,i__2);
	i__1 = *nx - 1;
	i__2 = nxint;
	for (ix = 1; i__2 < 0 ? ix >= i__1 : ix <= i__1; ix += i__2) {
	    i__3 = *ny - 1;
	    i__4 = nyint;
	    for (iy = 1; i__4 < 0 ? iy >= i__3 : iy <= i__3; iy += i__4) {
		if (! lmiss || z__[ix + iy * z_dim1] != rmiss && z__[ix + 1 + 
			iy * z_dim1] != rmiss && z__[ix + (iy + 1) * z_dim1] 
			!= rmiss && z__[ix + 1 + (iy + 1) * z_dim1] != rmiss) 
			{
		    y1 = ruwgy_(&iy);
		    i__5 = iy + 1;
		    y2 = ruwgy_(&i__5);
		    i__5 = ix + 1;
		    dx = ruwgx_(&i__5) - ruwgx_(&ix);
		    dy = y2 - y1;
		    if (lmap) {
			yy = (y1 + y2) / 2.f;
			if (ldeg) {
			    yy *= .017452777777777779f;
			}
			dx *= cos(yy);
		    }
		    zx = (z__[ix + 1 + iy * z_dim1] - z__[ix + iy * z_dim1]) /
			     dx;
		    zy = (z__[ix + (iy + 1) * z_dim1] - z__[ix + iy * z_dim1])
			     / dy;
		    ave = ave + abs(zx) + abs(zy);
		    nn += 2;
		}
/* L10: */
	    }
/* L11: */
	}
	uirget_("EMBOSSMENT_HEIGHT", &height, (ftnlen)17);
	ave = ave / nn * 2 / height;
	if (lsphe) {
	    uirget_("LIGHT_LONGITUDE", &phi, (ftnlen)15);
	    uirget_("LIGHT_LATITUDE ", &the, (ftnlen)15);
	    ex0 = cos(the * .017452777777777779f) * cos(phi * 
		    .017452777777777779f);
	    ey0 = cos(the * .017452777777777779f) * sin(phi * 
		    .017452777777777779f);
	    ez0 = sin(the * .017452777777777779f);
	} else {
	    uirget_("LIGHT_DIRECTION", &theta, (ftnlen)15);
	    ex = sin(theta * .017452777777777779f);
	    ey = -cos(theta * .017452777777777779f);
	    ez = 0.f;
	}
    }
/*     / LOOP FOR EACH PIXEL / */
    i__2 = ihight;
    for (j = 1; j <= i__2; ++j) {
	i__1 = iwidth;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    i__4 = i__ + ixmin - 1;
	    i__3 = j + iymin - 1;
	    swiint_(&i__4, &i__3, &wx, &wy);
	    stiwtr_(&wx, &wy, &rx, &ry);
	    stipr2_(&rx, &ry, &vx, &vy);
	    stitrf_(&vx, &vy, &ux, &uy);
	    if (ux == rundef) {
		image[i__] = icolor1;
	    } else {
		uwqgxi_(&ux, &ix, &fx);
		uwqgyi_(&uy, &iy, &fy);
		if (lshdw) {
		    y1 = ruwgy_(&iy);
		    i__4 = iy + 1;
		    y2 = ruwgy_(&i__4);
		    x1 = ruwgx_(&ix);
		    i__4 = ix + 1;
		    x2 = ruwgx_(&i__4);
		    dx = (x2 - x1) * ave;
		    dy = (y2 - y1) * ave;
		    if (lmap) {
			xx = (x1 + x2) / 2.f;
			yy = (y1 + y2) / 2.f;
			if (ldeg) {
			    xx *= .01745328888888889f;
			    yy *= .01745328888888889f;
			}
			dx *= cos(yy);
			if (lsphe) {
			    r__1 = -yy + 1.5707500000000001f;
			    r__2 = xx;
			    cr3c_(&r__1, &r__2, &c_b33, &ex0, &ey0, &ez0, &ex,
				     &ey, &ez);
			}
		    }
		}
		if (ix == iundef || iy == iundef) {
		    image[i__] = icolor2;
		} else {
		    if (lcx && lcy) {
			if (lmiss && z__[ix + iy * z_dim1] == rmiss) {
			    image[i__] = icolor3;
			} else {
			    zz = z__[ix + iy * z_dim1];
			    uicrgb_(&zz, &c_b34, &image[i__]);
			}
		    } else if (lcx && ! lcy) {
			if (lmiss && (z__[ix + iy * z_dim1] == rmiss || z__[
				ix + (iy + 1) * z_dim1] == rmiss)) {
			    image[i__] = icolor3;
			} else {
			    zz = z__[ix + iy * z_dim1] * (1 - fy) + z__[ix + (
				    iy + 1) * z_dim1] * fy;
			    uicrgb_(&zz, &c_b34, &image[i__]);
			}
		    } else if (! lcx && lcy) {
			if (lmiss && (z__[ix + iy * z_dim1] == rmiss || z__[
				ix + 1 + iy * z_dim1] == rmiss)) {
			    image[i__] = icolor3;
			} else {
			    zz = z__[ix + iy * z_dim1] * (1 - fx) + z__[ix + 
				    1 + iy * z_dim1] * fx;
			    uicrgb_(&zz, &c_b34, &image[i__]);
			}
		    } else if (! lcx && ! lcy) {
			if (! lmiss || z__[ix + iy * z_dim1] != rmiss && z__[
				ix + 1 + iy * z_dim1] != rmiss && z__[ix + (
				iy + 1) * z_dim1] != rmiss && z__[ix + 1 + (
				iy + 1) * z_dim1] != rmiss) {
			    zz = (z__[ix + iy * z_dim1] * (1 - fx) + z__[ix + 
				    1 + iy * z_dim1] * fx) * (1 - fy) + (z__[
				    ix + (iy + 1) * z_dim1] * (1 - fx) + z__[
				    ix + 1 + (iy + 1) * z_dim1] * fx) * fy;
			    if (lshdw) {
				zx = ((z__[ix + 1 + iy * z_dim1] - z__[ix + 
					iy * z_dim1]) * (1.f - fy) + (z__[ix 
					+ 1 + (iy + 1) * z_dim1] - z__[ix + (
					iy + 1) * z_dim1]) * fy) / dx;
				zy = ((z__[ix + (iy + 1) * z_dim1] - z__[ix + 
					iy * z_dim1]) * (1.f - fx) + (z__[ix 
					+ 1 + (iy + 1) * z_dim1] - z__[ix + 1 
					+ iy * z_dim1]) * fx) / dy;
				uishdw_(&zx, &zy, &ex, &ey, &ez, &cmsk);
			    } else {
				cmsk = 0.f;
			    }
			    r__1 = cmsk * densty;
			    uicrgb_(&zz, &r__1, &image[i__]);
			} else if (lcnr && fx + fy <= 1.f && (z__[ix + iy * 
				z_dim1] != rmiss && z__[ix + 1 + iy * z_dim1] 
				!= rmiss && z__[ix + (iy + 1) * z_dim1] != 
				rmiss && z__[ix + 1 + (iy + 1) * z_dim1] == 
				rmiss)) {
			    zz = z__[ix + iy * z_dim1] * (1 - fx - fy) + z__[
				    ix + 1 + iy * z_dim1] * fx + z__[ix + (iy 
				    + 1) * z_dim1] * fy;
			    if (lshdw) {
				zx = (z__[ix + 1 + iy * z_dim1] - z__[ix + iy 
					* z_dim1]) / dx;
				zy = (z__[ix + (iy + 1) * z_dim1] - z__[ix + 
					iy * z_dim1]) / dy;
				uishdw_(&zx, &zy, &ex, &ey, &ez, &cmsk);
			    } else {
				cmsk = 0.f;
			    }
			    r__1 = cmsk * densty;
			    uicrgb_(&zz, &r__1, &image[i__]);
			} else if (lcnr && 1 - fx + fy <= 1.f && (z__[ix + iy 
				* z_dim1] != rmiss && z__[ix + 1 + iy * 
				z_dim1] != rmiss && z__[ix + (iy + 1) * 
				z_dim1] == rmiss && z__[ix + 1 + (iy + 1) * 
				z_dim1] != rmiss)) {
			    zz = z__[ix + 1 + iy * z_dim1] * (fx - fy) + z__[
				    ix + iy * z_dim1] * (1 - fx) + z__[ix + 1 
				    + (iy + 1) * z_dim1] * fy;
			    if (lshdw) {
				zx = (z__[ix + 1 + iy * z_dim1] - z__[ix + iy 
					* z_dim1]) / dx;
				zy = (z__[ix + 1 + (iy + 1) * z_dim1] - z__[
					ix + 1 + iy * z_dim1]) / dy;
				uishdw_(&zx, &zy, &ex, &ey, &ez, &cmsk);
			    } else {
				cmsk = 0.f;
			    }
			    r__1 = cmsk * densty;
			    uicrgb_(&zz, &r__1, &image[i__]);
			} else if (lcnr && fx + 1 - fy <= 1.f && (z__[ix + iy 
				* z_dim1] != rmiss && z__[ix + 1 + iy * 
				z_dim1] == rmiss && z__[ix + (iy + 1) * 
				z_dim1] != rmiss && z__[ix + 1 + (iy + 1) * 
				z_dim1] != rmiss)) {
			    zz = z__[ix + (iy + 1) * z_dim1] * (fy - fx) + 
				    z__[ix + 1 + (iy + 1) * z_dim1] * fx + 
				    z__[ix + iy * z_dim1] * (1 - fy);
			    if (lshdw) {
				zx = (z__[ix + 1 + (iy + 1) * z_dim1] - z__[
					ix + (iy + 1) * z_dim1]) / dx;
				zy = (z__[ix + (iy + 1) * z_dim1] - z__[ix + 
					iy * z_dim1]) / dy;
				uishdw_(&zx, &zy, &ex, &ey, &ez, &cmsk);
			    } else {
				cmsk = 0.f;
			    }
			    r__1 = cmsk * densty;
			    uicrgb_(&zz, &r__1, &image[i__]);
			} else if (lcnr && 2 - fx - fy <= 1.f && (z__[ix + iy 
				* z_dim1] == rmiss && z__[ix + 1 + iy * 
				z_dim1] != rmiss && z__[ix + (iy + 1) * 
				z_dim1] != rmiss && z__[ix + 1 + (iy + 1) * 
				z_dim1] != rmiss)) {
			    zz = z__[ix + 1 + (iy + 1) * z_dim1] * (fx + fy - 
				    1) + z__[ix + (iy + 1) * z_dim1] * (1 - 
				    fx) + z__[ix + 1 + iy * z_dim1] * (1 - fy)
				    ;
			    if (lshdw) {
				zx = (z__[ix + 1 + (iy + 1) * z_dim1] - z__[
					ix + (iy + 1) * z_dim1]) / dx;
				zy = (z__[ix + 1 + (iy + 1) * z_dim1] - z__[
					ix + 1 + iy * z_dim1]) / dy;
				uishdw_(&zx, &zy, &ex, &ey, &ez, &cmsk);
			    } else {
				cmsk = 0.f;
			    }
			    r__1 = cmsk * densty;
			    uicrgb_(&zz, &r__1, &image[i__]);
			} else {
			    image[i__] = icolor3;
			}
		    }
		}
	    }
/* L20: */
	}
	swiclr_(&image[1], &iwidth);
/* L30: */
    }
    swicls_();
    return 0;
} /* uipdaz_ */

/* ----------------------------------------------------------------------- */
/* Subroutine */ int uishdw_(real *dx, real *dy, real *ex, real *ey, real *ez,
	 real *cc)
{
    /* System generated locals */
    real r__1, r__2;

    /* Builtin functions */
    double sqrt(doublereal), r_sign(real *, real *);

    /* Local variables */
    static real a, b, c__, ab, cs, vx, vy, vz;

    if (*dx != 0.f && *dy != 0.f) {
	a = 1.f / *dx;
	b = 1.f / *dy;
/* Computing 2nd power */
	r__1 = a;
/* Computing 2nd power */
	r__2 = b;
	ab = sqrt(r__1 * r__1 + r__2 * r__2);
	c__ = (r__1 = a * b, abs(r__1)) / ab;
/* Computing 2nd power */
	r__1 = c__;
	cs = sqrt(r__1 * r__1 + 1.f);
	vx = r_sign(&b, &a) / ab / cs;
	vy = r_sign(&a, &b) / ab / cs;
	vz = c__ / cs;
    } else if (*dx != 0.f) {
	a = 1.f / *dx;
/* Computing 2nd power */
	r__1 = a;
	cs = sqrt(r__1 * r__1 + 1.f);
	vx = r_sign(&c_b38, &a) / cs;
	vy = 0.f;
	vz = abs(a) / cs;
    } else if (*dy != 0.f) {
	b = 1.f / *dy;
/* Computing 2nd power */
	r__1 = b;
	cs = sqrt(r__1 * r__1 + 1.f);
	vx = 0.f;
	vy = r_sign(&c_b38, &b) / cs;
	vz = abs(b) / cs;
    } else {
	vx = 0.f;
	vy = 0.f;
	vz = 1.f;
    }
    *cc = vx * *ex + vy * *ey + vz * *ez;
    return 0;
} /* uishdw_ */


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/* Subroutine */ int uic2d_0_(int n__, real *u, real *v, integer *irgb, real *
	umin, real *umax, real *vmin, real *vmax, integer *irgb1, integer *
	irgb2, integer *irgb3, integer *irgb4)
{
    /* Initialized data */

    static integer ir1 = 136;
    static integer ib2 = 0;
    static integer ib3 = 255;
    static integer ib4 = 255;
    static logical lset = FALSE_;
    static integer ir2 = 255;
    static integer ir3 = 0;
    static integer ir4 = 136;
    static integer ig1 = 255;
    static integer ig2 = 136;
    static integer ig3 = 136;
    static integer ig4 = 0;
    static integer ib1 = 0;

    static real c1, c2, c3, c4, u1, v1, u2, v2;
    static integer jb, jg;
    static real du, dv;
    static integer jr;
    static real uu, vv;
    extern /* Subroutine */ int uifpac_(integer *, integer *, integer *, 
	    integer *), uiipac_(integer *, integer *, integer *, integer *);

    switch(n__) {
	case 1: goto L_ui2ini;
	case 2: goto L_uiscr2;
	case 3: goto L_uiqcr2;
	case 4: goto L_uiscmp;
	}

    uu = (*u - u1) / du;
    vv = (*v - v1) / dv;
    c1 = (1.f - uu) * vv;
    c2 = uu * vv;
    c3 = (1.f - uu) * (1.f - vv);
    c4 = uu * (1.f - vv);
    jr = ir1 * c1 + ir2 * c2 + ir3 * c3 + ir4 * c4;
    jg = ig1 * c1 + ig2 * c2 + ig3 * c3 + ig4 * c4;
    jb = ib1 * c1 + ib2 * c2 + ib3 * c3 + ib4 * c4;
    uifpac_(&jr, &jg, &jb, irgb);
    return 0;
/* ----------------------------------------------------------------------- */

L_ui2ini:
    if (! lset) {
	u1 = *umin;
	u2 = *umax;
	v1 = *vmin;
	v2 = *vmax;
    }
    du = u2 - u1;
    dv = v2 - v1;
    return 0;
/* ----------------------------------------------------------------------- */

L_uiscr2:
    u1 = *umin;
    u2 = *umax;
    v1 = *vmin;
    v2 = *vmax;
    lset = TRUE_;
    return 0;
/* ----------------------------------------------------------------------- */

L_uiqcr2:
    *umin = u1;
    *umax = u2;
    *vmin = v1;
    *vmax = v2;
    return 0;
/* ----------------------------------------------------------------------- */

L_uiscmp:
    uiipac_(irgb1, &ir1, &ig1, &ib1);
    uiipac_(irgb2, &ir2, &ig2, &ib2);
    uiipac_(irgb3, &ir3, &ig3, &ib3);
    uiipac_(irgb4, &ir4, &ig4, &ib4);
    return 0;
} /* uic2d_ */

/* Subroutine */ int uic2d_(real *u, real *v, integer *irgb)
{
    return uic2d_0_(0, u, v, irgb, (real *)0, (real *)0, (real *)0, (real *)0,
	     (integer *)0, (integer *)0, (integer *)0, (integer *)0);
    }

/* Subroutine */ int ui2ini_(real *umin, real *umax, real *vmin, real *vmax)
{
    return uic2d_0_(1, (real *)0, (real *)0, (integer *)0, umin, umax, vmin, 
	    vmax, (integer *)0, (integer *)0, (integer *)0, (integer *)0);
    }

/* Subroutine */ int uiscr2_(real *umin, real *umax, real *vmin, real *vmax)
{
    return uic2d_0_(2, (real *)0, (real *)0, (integer *)0, umin, umax, vmin, 
	    vmax, (integer *)0, (integer *)0, (integer *)0, (integer *)0);
    }

/* Subroutine */ int uiqcr2_(real *umin, real *umax, real *vmin, real *vmax)
{
    return uic2d_0_(3, (real *)0, (real *)0, (integer *)0, umin, umax, vmin, 
	    vmax, (integer *)0, (integer *)0, (integer *)0, (integer *)0);
    }

/* Subroutine */ int uiscmp_(integer *irgb1, integer *irgb2, integer *irgb3, 
	integer *irgb4)
{
    return uic2d_0_(4, (real *)0, (real *)0, (integer *)0, (real *)0, (real *)
	    0, (real *)0, (real *)0, irgb1, irgb2, irgb3, irgb4);
    }


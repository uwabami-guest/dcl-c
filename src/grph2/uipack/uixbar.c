/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;
static real c_b3 = 0.f;
static real c_b4 = 1.f;
static logical c_false = FALSE_;
static integer c__2 = 2;
static integer c__3 = 3;

/* --------------------------------------------------------------- */
/* Subroutine */ int uixbar_(real *xmin, real *xmax, real *ymin, real *ymax, 
	real *zmin, real *zmax, char *cpos, ftnlen cpos_len)
{
    static real z__[4]	/* was [2][2] */;
    static logical lem, lcx, lcy;
    extern /* Subroutine */ int grfig_(void), uipdat_(real *, integer *, 
	    integer *, integer *), uilget_(char *, logical *, ftnlen), 
	    sgswnd_(real *, real *, real *, real *), uilset_(char *, logical *
	    , ftnlen), usaxsc_(char *, ftnlen), sgstrf_(void), uwsgxb_(real *,
	     real *, integer *), sgstrn_(integer *), uwsgyb_(real *, real *, 
	    integer *), sgsvpt_(real *, real *, real *, real *), slpvpr_(
	    integer *);

    grfig_();
    sgstrn_(&c__1);
    sgsvpt_(xmin, xmax, ymin, ymax);
    sgswnd_(zmin, zmax, &c_b3, &c_b4);
    sgstrf_();
    z__[0] = *zmin;
    z__[2] = *zmin;
    z__[1] = *zmax;
    z__[3] = *zmax;
    uilget_("CELL_MODE_X", &lcx, (ftnlen)11);
    uilget_("CELL_MODE_Y", &lcy, (ftnlen)11);
    uilget_("EMBOSS     ", &lem, (ftnlen)11);
    uilset_("CELL_MODE_X", &c_false, (ftnlen)11);
    uilset_("CELL_MODE_Y", &c_false, (ftnlen)11);
    uilset_("EMBOSS     ", &c_false, (ftnlen)11);
    uwsgxb_(zmin, zmax, &c__2);
    uwsgyb_(&c_b3, &c_b4, &c__2);
    uipdat_(z__, &c__2, &c__2, &c__2);
    slpvpr_(&c__3);
    usaxsc_(cpos, cpos_len);
    uilset_("CELL_MODE_X", &lcx, (ftnlen)11);
    uilset_("CELL_MODE_Y", &lcy, (ftnlen)11);
    uilset_("EMBOSS     ", &lem, (ftnlen)11);
    return 0;
} /* uixbar_ */


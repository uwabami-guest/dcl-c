/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static doublereal c_b2 = .33333333333333331;

/* ======================================================================= */
/* Subroutine */ int uiflab_0_(int n__, real *x, real *y, real *z__, real *ul,
	 real *a, real *b)
{
    /* System generated locals */
    real r__1;
    doublereal d__1;

    /* Builtin functions */
    double pow_dd(doublereal *, doublereal *);

    /* Local variables */
    static real x0, y0, z0, xx, yy, zz;

    switch(n__) {
	case 1: goto L_uiilab;
	}

    x0 = *x / .9804f;
    y0 = *y / 1.f;
    z0 = *z__ / 1.1812f;
    if (x0 <= .008856f) {
	xx = x0 * 7.787f + .13793103448275862f;
    } else {
	d__1 = (doublereal) x0;
	xx = pow_dd(&d__1, &c_b2);
    }
    if (y0 <= .008856f) {
	yy = y0 * 7.787f + .13793103448275862f;
    } else {
	d__1 = (doublereal) y0;
	yy = pow_dd(&d__1, &c_b2);
    }
    if (z0 <= .008856f) {
	zz = z0 * 7.787f + .13793103448275862f;
    } else {
	d__1 = (doublereal) z0;
	zz = pow_dd(&d__1, &c_b2);
    }
    *ul = yy * 116.f - 16.f;
    *a = (xx - yy) * 500.f;
    *b = (yy - zz) * 200.f;
    return 0;
/* ----------------------------------------------------------------------- */

L_uiilab:
    yy = (*ul + 16.f) / 116.f;
    xx = *a / 500.f + yy;
    zz = -(*b) / 200.f + yy;
/* Computing 3rd power */
    r__1 = xx;
    x0 = r__1 * (r__1 * r__1);
/* Computing 3rd power */
    r__1 = yy;
    y0 = r__1 * (r__1 * r__1);
/* Computing 3rd power */
    r__1 = zz;
    z0 = r__1 * (r__1 * r__1);
    if (x0 <= .008856f) {
	x0 = (xx - .13793103448275862f) / 7.787f;
    }
    if (y0 <= .008856f) {
	y0 = (yy - .13793103448275862f) / 7.787f;
    }
    if (z0 <= .008856f) {
	z0 = (zz - .13793103448275862f) / 7.787f;
    }
    *x = x0 * .9804f;
    *y = y0 * 1.f;
    *z__ = z0 * 1.1812f;
    return 0;
} /* uiflab_ */

/* Subroutine */ int uiflab_(real *x, real *y, real *z__, real *ul, real *a, 
	real *b)
{
    return uiflab_0_(0, x, y, z__, ul, a, b);
    }

/* Subroutine */ int uiilab_(real *ul, real *a, real *b, real *x, real *y, 
	real *z__)
{
    return uiflab_0_(1, x, y, z__, ul, a, b);
    }

/* ======================================================================= */
/* Subroutine */ int uifluv_0_(int n__, real *x, real *y, real *z__, real *ul,
	 real *u, real *v)
{
    /* System generated locals */
    real r__1;
    doublereal d__1;

    /* Builtin functions */
    double pow_dd(doublereal *, doublereal *);

    /* Local variables */
    static real yy, xyz;

    switch(n__) {
	case 1: goto L_uiiluv;
	}

    if (*y <= .008856f) {
	yy = *y * 7.787f + .13793103448275862f;
    } else {
	d__1 = (doublereal) (*y);
	yy = pow_dd(&d__1, &c_b2);
    }
    *ul = yy * 116.f - 16.f;
    xyz = *x + *y * 15 + *z__ * 3;
    *u = *x * 4 / xyz;
    *v = *y * 9 / xyz;
    return 0;
/* ----------------------------------------------------------------------- */

L_uiiluv:
    yy = (*ul + 16.f) / 116.f;
/* Computing 3rd power */
    r__1 = yy;
    *y = r__1 * (r__1 * r__1);
    if (*y <= .008856f) {
	*y = (yy - .13793103448275862f) / 7.787f;
    }
    *x = *u * 9.f * *y / (*v * 4.f);
    *z__ = (12.f - *u * 3.f - *v * 20.f) * *y / (*v * 4.f);
    return 0;
} /* uifluv_ */

/* Subroutine */ int uifluv_(real *x, real *y, real *z__, real *ul, real *u, 
	real *v)
{
    return uifluv_0_(0, x, y, z__, ul, u, v);
    }

/* Subroutine */ int uiiluv_(real *ul, real *u, real *v, real *x, real *y, 
	real *z__)
{
    return uifluv_0_(1, x, y, z__, ul, u, v);
    }

/* ======================================================================= */
/* Subroutine */ int uifrgb_0_(int n__, real *x, real *y, real *z__, real *
	r__, real *g, real *b)
{
    /* Initialized data */

    static real r2x[9]	/* was [3][3] */ = { .6067f,.1736f,.2001f,.2988f,
	    .5868f,.1144f,0.f,.0661f,1.115f };
    static real x2r[9]	/* was [3][3] */ = { 1.9106f,-.5326f,-.2883f,-.9843f,
	    1.9984f,-.0283f,.0584f,-.1185f,.8985f };

    switch(n__) {
	case 1: goto L_uiirgb;
	}

    *r__ = x2r[0] * *x + x2r[1] * *y + x2r[2] * *z__;
    *g = x2r[3] * *x + x2r[4] * *y + x2r[5] * *z__;
    *b = x2r[6] * *x + x2r[7] * *y + x2r[8] * *z__;
    return 0;
/* ----------------------------------------------------------------------- */

L_uiirgb:
    *x = r2x[0] * *r__ + r2x[1] * *g + r2x[2] * *b;
    *y = r2x[3] * *r__ + r2x[4] * *g + r2x[5] * *b;
    *z__ = r2x[6] * *r__ + r2x[7] * *g + r2x[8] * *b;
    return 0;
} /* uifrgb_ */

/* Subroutine */ int uifrgb_(real *x, real *y, real *z__, real *r__, real *g, 
	real *b)
{
    return uifrgb_0_(0, x, y, z__, r__, g, b);
    }

/* Subroutine */ int uiirgb_(real *r__, real *g, real *b, real *x, real *y, 
	real *z__)
{
    return uifrgb_0_(1, x, y, z__, r__, g, b);
    }

/* ======================================================================= */
/* Subroutine */ int uifyxy_0_(int n__, real *x, real *y, real *z__, real *yu,
	 real *xl, real *yl)
{
    static real xyz;

    switch(n__) {
	case 1: goto L_uiiyxy;
	}

    xyz = *x + *y + *z__;
    if (xyz > 0.f) {
	*yu = *y;
	*xl = *x / (*x + *y + *z__);
	*yl = *y / (*x + *y + *z__);
    } else {
	*yu = 0.f;
	*xl = .3f;
	*yl = .3f;
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_uiiyxy:
    xyz = *yu / *yl;
    *x = *xl * xyz;
    *y = *yu;
    *z__ = (1 - *xl - *yl) * xyz;
    return 0;
} /* uifyxy_ */

/* Subroutine */ int uifyxy_(real *x, real *y, real *z__, real *yu, real *xl, 
	real *yl)
{
    return uifyxy_0_(0, x, y, z__, yu, xl, yl);
    }

/* Subroutine */ int uiiyxy_(real *yu, real *xl, real *yl, real *x, real *y, 
	real *z__)
{
    return uifyxy_0_(1, x, y, z__, yu, xl, yl);
    }

/* ======================================================================= */
/* Subroutine */ int uiencd_0_(int n__, real *r__, real *g, real *b, integer *
	irgb)
{
    /* Initialized data */

    static real gamma = 2.2f;

    /* System generated locals */
    integer i__1, i__2;
    real r__1;
    doublereal d__1, d__2;

    /* Builtin functions */
    double pow_dd(doublereal *, doublereal *);
    integer i_nint(real *), lbit_shift(integer, integer);

    /* Local variables */
    static real bb;
    static integer ib;
    static real gg, gi;
    static integer ig, ir;
    static real rr;

    switch(n__) {
	case 1: goto L_uidecd;
	}

    gi = 1.f / gamma;
    d__1 = (doublereal) (*r__);
    d__2 = (doublereal) gi;
    rr = pow_dd(&d__1, &d__2);
    d__1 = (doublereal) (*g);
    d__2 = (doublereal) gi;
    gg = pow_dd(&d__1, &d__2);
    d__1 = (doublereal) (*b);
    d__2 = (doublereal) gi;
    bb = pow_dd(&d__1, &d__2);
/* Computing MAX */
/* Computing MIN */
    r__1 = rr * 255;
    i__2 = i_nint(&r__1);
    i__1 = min(i__2,255);
    ir = max(i__1,0);
/* Computing MAX */
/* Computing MIN */
    r__1 = gg * 255;
    i__2 = i_nint(&r__1);
    i__1 = min(i__2,255);
    ig = max(i__1,0);
/* Computing MAX */
/* Computing MIN */
    r__1 = bb * 255;
    i__2 = i_nint(&r__1);
    i__1 = min(i__2,255);
    ib = max(i__1,0);
    *irgb = ir << 16 | (ig << 8 | ib);
    return 0;
/* ----------------------------------------------------------------------- */

L_uidecd:
    rr = (real) (lbit_shift(*irgb, (ftnlen)-16) & 255) / 255.f;
    gg = (real) (lbit_shift(*irgb, (ftnlen)-8) & 255) / 255.f;
    bb = (real) (*irgb & 255) / 255.f;
    d__1 = (doublereal) rr;
    d__2 = (doublereal) gamma;
    *r__ = pow_dd(&d__1, &d__2);
    d__1 = (doublereal) gg;
    d__2 = (doublereal) gamma;
    *g = pow_dd(&d__1, &d__2);
    d__1 = (doublereal) bb;
    d__2 = (doublereal) gamma;
    *b = pow_dd(&d__1, &d__2);
    return 0;
} /* uiencd_ */

/* Subroutine */ int uiencd_(real *r__, real *g, real *b, integer *irgb)
{
    return uiencd_0_(0, r__, g, b, irgb);
    }

/* Subroutine */ int uidecd_(integer *irgb, real *r__, real *g, real *b)
{
    return uiencd_0_(1, r__, g, b, irgb);
    }


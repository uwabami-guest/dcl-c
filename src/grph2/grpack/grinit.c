/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int grinit_(void)
{
    static real rundef;
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen), grscwd_(real 
	    *, real *, real *, real *), grswnd_(real *, real *, real *, real *
	    ), grssim_(real *, real *, real *), grsmpl_(real *, real *, real *
	    ), grsvpt_(real *, real *, real *, real *), sgrstx_(char *, real *
	    , ftnlen), grstxy_(real *, real *, real *, real *);

    glrget_("RUNDEF", &rundef, (ftnlen)6);
    grsvpt_(&rundef, &rundef, &rundef, &rundef);
    grswnd_(&rundef, &rundef, &rundef, &rundef);
    grssim_(&rundef, &rundef, &rundef);
    grsmpl_(&rundef, &rundef, &rundef);
    grstxy_(&rundef, &rundef, &rundef, &rundef);
    grscwd_(&rundef, &rundef, &rundef, &rundef);
    sgrstx_("RSAT", &rundef, (ftnlen)4);
    sgrstx_("STLAT1", &rundef, (ftnlen)6);
    sgrstx_("STLAT2", &rundef, (ftnlen)6);
    return 0;
} /* grinit_ */


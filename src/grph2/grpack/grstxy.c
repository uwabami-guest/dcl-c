/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int grstxy_(real *txmin, real *txmax, real *tymin, real *
	tymax)
{
    extern /* Subroutine */ int sgrstx_(char *, real *, ftnlen);

    sgrstx_("TXMIN", txmin, (ftnlen)5);
    sgrstx_("TXMAX", txmax, (ftnlen)5);
    sgrstx_("TYMIN", tymin, (ftnlen)5);
    sgrstx_("TYMAX", tymax, (ftnlen)5);
    return 0;
} /* grstxy_ */


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Common Block Declarations */

struct {
    logical lclip;
} szbls2_;

#define szbls2_1 szbls2_

struct {
    logical lclipt;
} szbtx3_;

#define szbtx3_1 szbtx3_

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uverbz_(integer *n, real *upx, real *upy1, real *upy2, 
	integer *itype, integer *index, real *rsize)
{
    /* System generated locals */
    integer i__1;
    real r__1;

    /* Builtin functions */
    integer s_wsfi(icilist *), do_fio(integer *, char *, ftnlen), e_wsfi(void)
	    ;

    /* Local variables */
    static integer i__;
    static real dx, vy1, vy2, uxx, vxx;
    static char cobj[80];
    extern /* Subroutine */ int cdblk_(char *, ftnlen);
    static logical lmiss;
    static real rmiss;
    static logical lxuni;
    static real uxmin, uxmax;
    extern /* Subroutine */ int gllget_(char *, logical *, ftnlen);
    static real rundef;
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen), sglget_(char 
	    *, logical *, ftnlen), msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), sgrget_(char *, real *, ftnlen), stftrf_(real *, 
	    real *, real *, real *), swocls_(char *, ftnlen), uuqidv_(real *, 
	    real *), szcllv_(void), szclsv_(void), swoopn_(char *, char *, 
	    ftnlen, ftnlen), szslti_(integer *, integer *), szpllv_(real *, 
	    real *), szoplv_(void), szmvlv_(real *, real *), szplsv_(real *, 
	    real *), szopsv_(void), szmvsv_(real *, real *);

    /* Fortran I/O blocks */
    static icilist io___5 = { 0, cobj, 0, "(2I8,F8.5)", 80, 1 };


    /* Parameter adjustments */
    --upy2;
    --upy1;
    --upx;

    /* Function Body */
    if (*n < 1) {
	msgdmp_("E", "UVERBZ", "NUMBER OF POINTS IS LESS THAN 1.", (ftnlen)1, 
		(ftnlen)6, (ftnlen)32);
    }
    if (*itype == 0) {
	msgdmp_("M", "UVERBZ", "LINE TYPE IS 0 / DO NOTHING.", (ftnlen)1, (
		ftnlen)6, (ftnlen)28);
	return 0;
    }
    if (*index == 0) {
	msgdmp_("M", "UVERBZ", "LINE INDEX IS 0 / DO NOTHING.", (ftnlen)1, (
		ftnlen)6, (ftnlen)29);
	return 0;
    }
    if (*index < 0) {
	msgdmp_("E", "UVERBZ", "LINE INDEX IS LESS THAN 0.", (ftnlen)1, (
		ftnlen)6, (ftnlen)26);
    }
    if (*rsize == 0.f) {
	msgdmp_("M", "UVERBZ", "MARKER SIZE IS 0 / DO NOTHING.", (ftnlen)1, (
		ftnlen)6, (ftnlen)30);
	return 0;
    }
    if (*rsize < 0.f) {
	msgdmp_("E", "UVERBZ", "MARKER SIZE IS LESS THAN ZERO.", (ftnlen)1, (
		ftnlen)6, (ftnlen)30);
    }
    sglget_("LCLIP", &szbls2_1.lclip, (ftnlen)5);
    szbtx3_1.lclipt = szbls2_1.lclip;
    glrget_("RUNDEF", &rundef, (ftnlen)6);
    glrget_("RMISS", &rmiss, (ftnlen)5);
    gllget_("LMISS", &lmiss, (ftnlen)5);
    if (upy1[1] == rundef || upy2[1] == rundef) {
	msgdmp_("E", "UVERBZ", "RUNDEF CAN NOT BE UESED FOR UPY1 OR UPY2", (
		ftnlen)1, (ftnlen)6, (ftnlen)40);
    }
    s_wsfi(&io___5);
    do_fio(&c__1, (char *)&(*itype), (ftnlen)sizeof(integer));
    do_fio(&c__1, (char *)&(*index), (ftnlen)sizeof(integer));
    do_fio(&c__1, (char *)&(*rsize), (ftnlen)sizeof(real));
    e_wsfi();
    cdblk_(cobj, (ftnlen)80);
    swoopn_("UVERBZ", cobj, (ftnlen)6, (ftnlen)80);
    szslti_(itype, index);
    lxuni = upx[1] == rundef;
    if (lxuni) {
	uuqidv_(&uxmin, &uxmax);
	if (uxmin == rundef) {
	    sgrget_("UXMIN", &uxmin, (ftnlen)5);
	}
	if (uxmax == rundef) {
	    sgrget_("UXMAX", &uxmax, (ftnlen)5);
	}
	dx = (uxmax - uxmin) / (*n - 1);
    }
    i__1 = *n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	if (lxuni) {
	    uxx = uxmin + dx * (i__ - 1);
	} else {
	    uxx = upx[i__];
	}
	if (! ((uxx == rmiss || upy1[i__] == rmiss || upy2[i__] == rmiss) && 
		lmiss)) {
	    stftrf_(&uxx, &upy1[i__], &vxx, &vy1);
	    stftrf_(&uxx, &upy2[i__], &vxx, &vy2);
	    szoplv_();
	    szmvlv_(&vxx, &vy1);
	    szpllv_(&vxx, &vy2);
	    szcllv_();
	    szopsv_();
	    r__1 = vxx - *rsize / 2.f;
	    szmvsv_(&r__1, &vy2);
	    r__1 = vxx + *rsize / 2.f;
	    szplsv_(&r__1, &vy2);
	    r__1 = vxx - *rsize / 2.f;
	    szmvsv_(&r__1, &vy1);
	    r__1 = vxx + *rsize / 2.f;
	    szplsv_(&r__1, &vy1);
	    szclsv_();
	}
/* L20: */
    }
    swocls_("UVERBZ", (ftnlen)6);
    return 0;
} /* uverbz_ */


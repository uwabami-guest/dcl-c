/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Common Block Declarations */

struct {
    logical lclip;
} szbls2_;

#define szbls2_1 szbls2_

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uvbxfz_(integer *n, real *upx, real *upy1, real *upy2, 
	integer *itype, integer *index)
{
    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    integer s_wsfi(icilist *), do_fio(integer *, char *, ftnlen), e_wsfi(void)
	    ;

    /* Local variables */
    static integer i__;
    static real dx, ux1, ux2, uy1, uy2;
    static logical lyc1, lyc2;
    static char cobj[80];
    static real uref;
    extern /* Subroutine */ int cdblk_(char *, ftnlen);
    static logical lmiss;
    static real rmiss;
    static logical lxuni;
    static real uxmin, uxmax;
    extern /* Subroutine */ int gllget_(char *, logical *, ftnlen);
    static real rundef;
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen), sglget_(char 
	    *, logical *, ftnlen), msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), sgrget_(char *, real *, ftnlen), swocls_(char *, 
	    ftnlen), uurget_(char *, real *, ftnlen), szcllu_(void), uuqidv_(
	    real *, real *), swoopn_(char *, char *, ftnlen, ftnlen), szslti_(
	    integer *, integer *), szpllu_(real *, real *), szoplu_(void), 
	    szmvlu_(real *, real *);

    /* Fortran I/O blocks */
    static icilist io___5 = { 0, cobj, 0, "(2I8)", 80, 1 };


    /* Parameter adjustments */
    --upy2;
    --upy1;
    --upx;

    /* Function Body */
    if (*n < 1) {
	msgdmp_("E", "UVBXFZ", "NUMBER OF POINTS IS LESS THAN 1.", (ftnlen)1, 
		(ftnlen)6, (ftnlen)32);
    }
    if (*itype == 0) {
	msgdmp_("M", "UVBXFZ", "LINE TYPE IS 0 / DO NOTHING.", (ftnlen)1, (
		ftnlen)6, (ftnlen)28);
	return 0;
    }
    if (*index == 0) {
	msgdmp_("M", "UVBXFZ", "LINE INDEX IS 0 / DO NOTHING.", (ftnlen)1, (
		ftnlen)6, (ftnlen)29);
	return 0;
    }
    if (*index < 0) {
	msgdmp_("E", "UVBXFZ", "LINE INDEX IS LESS THAN 0.", (ftnlen)1, (
		ftnlen)6, (ftnlen)26);
    }
    sglget_("LCLIP", &szbls2_1.lclip, (ftnlen)5);
    glrget_("RUNDEF", &rundef, (ftnlen)6);
    glrget_("RMISS", &rmiss, (ftnlen)5);
    gllget_("LMISS", &lmiss, (ftnlen)5);
    s_wsfi(&io___5);
    do_fio(&c__1, (char *)&(*itype), (ftnlen)sizeof(integer));
    do_fio(&c__1, (char *)&(*index), (ftnlen)sizeof(integer));
    e_wsfi();
    cdblk_(cobj, (ftnlen)80);
    swoopn_("UVBXFZ", cobj, (ftnlen)6, (ftnlen)80);
    szslti_(itype, index);
    lxuni = upx[1] == rundef;
    lyc1 = upy1[1] == rundef;
    lyc2 = upy2[1] == rundef;
    if (lxuni) {
	uuqidv_(&uxmin, &uxmax);
	if (uxmin == rundef) {
	    sgrget_("UXMIN", &uxmin, (ftnlen)5);
	}
	if (uxmax == rundef) {
	    sgrget_("UXMAX", &uxmax, (ftnlen)5);
	}
	dx = (uxmax - uxmin) / *n;
    }
    if (lyc1 || lyc2) {
	uurget_("UREF", &uref, (ftnlen)4);
    }
    i__1 = *n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	if (lxuni) {
	    ux1 = uxmin + dx * (i__ - 1);
	    ux2 = uxmin + dx * i__;
	} else {
	    ux1 = upx[i__];
	    ux2 = upx[i__ + 1];
	}
	if (lyc1) {
	    uy1 = uref;
	} else {
	    uy1 = upy1[i__];
	}
	if (lyc2) {
	    uy2 = uref;
	} else {
	    uy2 = upy2[i__];
	}
	if (! ((ux1 == rmiss || ux1 == rmiss || uy1 == rmiss || uy2 == rmiss) 
		&& lmiss)) {
	    szoplu_();
	    szmvlu_(&ux1, &uy2);
	    szpllu_(&ux2, &uy2);
	    szpllu_(&ux2, &uy1);
	    szpllu_(&ux1, &uy1);
	    szpllu_(&ux1, &uy2);
	    szcllu_();
	}
/* L20: */
    }
    swocls_("UVBXFZ", (ftnlen)6);
    return 0;
} /* uvbxfz_ */


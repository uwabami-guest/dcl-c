/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Common Block Declarations */

struct {
    integer irmode, irmodr;
} szbtn2_;

#define szbtn2_1 szbtn2_

struct {
    logical lclip;
} szbtn3_;

#define szbtn3_1 szbtn3_

/* Table of constant values */

static real c_b15 = 0.f;
static real c_b18 = 1.f;
static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uvdifz_(integer *n, real *upx, real *upy1, real *upy2, 
	integer *itpat1, integer *itpat2)
{
    /* System generated locals */
    integer i__1;
    real r__1;

    /* Builtin functions */
    integer s_wsfi(icilist *), do_fio(integer *, char *, ftnlen), e_wsfi(void)
	    ;

    /* Local variables */
    static integer i__, ir;
    static real dx, rx0, ry0, rx1, ry1, rx2, ry2, dyn, dyp, vxc, vyc, rot, 
	    uxn, vxn, uxp, vxp;
    static logical lyc1, lyc2;
    static real uy1n, uy2n, uy1p, uy2p, vy1p, vy2p, vy1n, vy2n;
    static char cobj[80];
    static real uref;
    extern /* Subroutine */ int cdblk_(char *, ftnlen);
    static logical lmiss;
    static real rmiss;
    static logical lxuni;
    static real uxmin, uxmax;
    extern /* Subroutine */ int stfpr2_(real *, real *, real *, real *), 
	    gllget_(char *, logical *, ftnlen);
    static real rundef;
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen), sglget_(char 
	    *, logical *, ftnlen), msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), sgrget_(char *, real *, ftnlen), stftrf_(real *, 
	    real *, real *, real *), swocls_(char *, ftnlen), uurget_(char *, 
	    real *, ftnlen), uuqidv_(real *, real *), swoopn_(char *, char *, 
	    ftnlen, ftnlen), szcltv_(void), szstni_(integer *), szoptv_(void),
	     szsttv_(real *, real *);

    /* Fortran I/O blocks */
    static icilist io___13 = { 0, cobj, 0, "(2I8)", 80, 1 };


    /* Parameter adjustments */
    --upy2;
    --upy1;
    --upx;

    /* Function Body */
    if (*n < 1) {
	msgdmp_("E", "UVDIFZ", "NUMBER OF POINTS IS LESS THAN 1.", (ftnlen)1, 
		(ftnlen)6, (ftnlen)32);
    }
    if (*itpat1 == 0 || *itpat2 == 0) {
	msgdmp_("M", "UVDIFZ", "TONE PAT. INDEX IS 0 / DO NOTHING.", (ftnlen)
		1, (ftnlen)6, (ftnlen)34);
	return 0;
    }
    if (*itpat1 < 0 || *itpat2 < 0) {
	msgdmp_("E", "UVDIFZ", "TONE PAT. INDEX IS LESS THAN 0.", (ftnlen)1, (
		ftnlen)6, (ftnlen)31);
    }
    sglget_("LCLIP", &szbtn3_1.lclip, (ftnlen)5);
    glrget_("RUNDEF", &rundef, (ftnlen)6);
    glrget_("RMISS", &rmiss, (ftnlen)5);
    gllget_("LMISS", &lmiss, (ftnlen)5);
    stfpr2_(&c_b15, &c_b15, &rx0, &ry0);
    stfpr2_(&c_b15, &c_b18, &rx1, &ry1);
    stfpr2_(&c_b18, &c_b15, &rx2, &ry2);
    rot = (rx2 - rx0) * (ry1 - ry0) - (ry2 - ry0) * (rx1 - rx0);
    if (rot > 0.f) {
	ir = 0;
    } else {
	ir = 1;
    }
    s_wsfi(&io___13);
    do_fio(&c__1, (char *)&(*itpat1), (ftnlen)sizeof(integer));
    do_fio(&c__1, (char *)&(*itpat2), (ftnlen)sizeof(integer));
    e_wsfi();
    cdblk_(cobj, (ftnlen)80);
    swoopn_("UVDIFZ", cobj, (ftnlen)6, (ftnlen)80);
    lxuni = upx[1] == rundef;
    lyc1 = upy1[1] == rundef;
    lyc2 = upy2[1] == rundef;
    if (lxuni) {
	uuqidv_(&uxmin, &uxmax);
	if (uxmin == rundef) {
	    sgrget_("UXMIN", &uxmin, (ftnlen)5);
	}
	if (uxmax == rundef) {
	    sgrget_("UXMAX", &uxmax, (ftnlen)5);
	}
	dx = (uxmax - uxmin) / (*n - 1);
    }
    if (lyc1 || lyc2) {
	uurget_("UREF", &uref, (ftnlen)4);
    }
    i__1 = *n - 1;
    for (i__ = 1; i__ <= i__1; ++i__) {
	if (lxuni) {
	    uxp = uxmin + dx * (i__ - 1);
	    uxn = uxmin + dx * i__;
	} else {
	    uxp = upx[i__];
	    uxn = upx[i__ + 1];
	}
	if (lyc1) {
	    uy1p = uref;
	    uy1n = uref;
	} else {
	    uy1p = upy1[i__];
	    uy1n = upy1[i__ + 1];
	}
	if (lyc2) {
	    uy2p = uref;
	    uy2n = uref;
	} else {
	    uy2p = upy2[i__];
	    uy2n = upy2[i__ + 1];
	}
	if (! ((uxp == rmiss || uxn == rmiss || uy1p == rmiss || uy1n == 
		rmiss || uy2p == rmiss || uy2n == rmiss) && lmiss)) {
	    stftrf_(&uxp, &uy1p, &vxp, &vy1p);
	    stftrf_(&uxp, &uy2p, &vxp, &vy2p);
	    stftrf_(&uxn, &uy1n, &vxn, &vy1n);
	    stftrf_(&uxn, &uy2n, &vxn, &vy2n);
	    if ((vy2p - vy1p) * (vy2n - vy1n) >= 0.f) {
		if (vy2p >= vy1p) {
		    szbtn2_1.irmode = 0;
		} else {
		    szbtn2_1.irmode = 1;
		}
		szbtn2_1.irmodr = (szbtn2_1.irmode + ir) % 2;
		if (uy2p > uy1p) {
		    szstni_(itpat1);
		} else {
		    szstni_(itpat2);
		}
		szoptv_();
		szsttv_(&vxp, &vy2p);
		szsttv_(&vxp, &vy1p);
		szsttv_(&vxn, &vy1n);
		szsttv_(&vxn, &vy2n);
		szsttv_(&vxp, &vy2p);
		szcltv_();
	    } else {
		dyp = (r__1 = vy2p - vy1p, abs(r__1));
		dyn = (r__1 = vy2n - vy1n, abs(r__1));
		vxc = (vxp * dyn + vxn * dyp) / (dyp + dyn);
		vyc = (vy1p * dyn + vy1n * dyp) / (dyp + dyn);
		if (vy2p >= vy1p) {
		    szbtn2_1.irmode = 0;
		} else {
		    szbtn2_1.irmode = 1;
		}
		szbtn2_1.irmodr = (szbtn2_1.irmode + ir) % 2;
		if (uy2p > uy1p) {
		    szstni_(itpat1);
		} else {
		    szstni_(itpat2);
		}
		szoptv_();
		szsttv_(&vxp, &vy2p);
		szsttv_(&vxp, &vy1p);
		szsttv_(&vxc, &vyc);
		szsttv_(&vxp, &vy2p);
		szcltv_();
		szbtn2_1.irmode = (szbtn2_1.irmode + 1) % 2;
		szbtn2_1.irmodr = (szbtn2_1.irmode + ir) % 2;
		if (uy2n > uy1n) {
		    szstni_(itpat1);
		} else {
		    szstni_(itpat2);
		}
		szoptv_();
		szsttv_(&vxn, &vy1n);
		szsttv_(&vxn, &vy2n);
		szsttv_(&vxc, &vyc);
		szsttv_(&vxn, &vy1n);
		szcltv_();
	    }
	}
/* L20: */
    }
    swocls_("UVDIFZ", (ftnlen)6);
    return 0;
} /* uvdifz_ */


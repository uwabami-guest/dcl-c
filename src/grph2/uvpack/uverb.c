/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uverb_(integer *n, real *upx, real *upy1, real *upy2)
{
    static integer index, itype;
    static real rsize;
    extern /* Subroutine */ int uuqebi_(integer *), uuqebs_(real *), uuqebt_(
	    integer *), uverbz_(integer *, real *, real *, real *, integer *, 
	    integer *, real *);

    /* Parameter adjustments */
    --upy2;
    --upy1;
    --upx;

    /* Function Body */
    uuqebt_(&itype);
    uuqebi_(&index);
    uuqebs_(&rsize);
    uverbz_(n, &upx[1], &upy1[1], &upy2[1], &itype, &index, &rsize);
    return 0;
} /* uverb_ */


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uhbrf_(integer *n, real *upx1, real *upx2, real *upy)
{
    static integer index, itype;
    static real rsize;
    extern /* Subroutine */ int uhbrfz_(integer *, real *, real *, real *, 
	    integer *, integer *, real *), uuqfri_(integer *), uuqbrs_(real *)
	    , uuqfrt_(integer *);

    /* Parameter adjustments */
    --upy;
    --upx2;
    --upx1;

    /* Function Body */
    uuqfrt_(&itype);
    uuqfri_(&index);
    uuqbrs_(&rsize);
    uhbrfz_(n, &upx1[1], &upx2[1], &upy[1], &itype, &index, &rsize);
    return 0;
} /* uhbrf_ */


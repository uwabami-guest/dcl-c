/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Common Block Declarations */

struct {
    logical lclip;
} szbls2_;

#define szbls2_1 szbls2_

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uhbxlz_(integer *n, real *upx, real *upy, integer *itype,
	 integer *index)
{
    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    integer s_wsfi(icilist *), do_fio(integer *, char *, ftnlen), e_wsfi(void)
	    ;

    /* Local variables */
    static integer i__;
    static real dy, uy1, uy2;
    static char cobj[80];
    extern /* Subroutine */ int cdblk_(char *, ftnlen);
    static logical lflag, lmiss;
    static real rmiss;
    static logical lyuni;
    static real uymin, uymax;
    extern /* Subroutine */ int gllget_(char *, logical *, ftnlen);
    static real rundef;
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen), sglget_(char 
	    *, logical *, ftnlen), msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), sgrget_(char *, real *, ftnlen), swocls_(char *, 
	    ftnlen), uuqidv_(real *, real *), szcllu_(void), swoopn_(char *, 
	    char *, ftnlen, ftnlen), szslti_(integer *, integer *), szpllu_(
	    real *, real *), szoplu_(void), szmvlu_(real *, real *);

    /* Fortran I/O blocks */
    static icilist io___5 = { 0, cobj, 0, "(2I8)", 80, 1 };


    /* Parameter adjustments */
    --upy;
    --upx;

    /* Function Body */
    if (*n < 2) {
	msgdmp_("E", "UHBOXL", "NUMBER OF POINTS IS LESS THAN 2.", (ftnlen)1, 
		(ftnlen)6, (ftnlen)32);
    }
    if (*itype == 0) {
	msgdmp_("M", "UHBOXL", "LINE TYPE IS 0 / DO NOTHING.", (ftnlen)1, (
		ftnlen)6, (ftnlen)28);
	return 0;
    }
    if (*index == 0) {
	msgdmp_("M", "UHBOXL", "LINE INDEX IS 0 / DO NOTHING.", (ftnlen)1, (
		ftnlen)6, (ftnlen)29);
	return 0;
    }
    if (*index < 0) {
	msgdmp_("E", "UHBOXL", "LINE INDEX IS LESS THAN 0.", (ftnlen)1, (
		ftnlen)6, (ftnlen)26);
    }
    sglget_("LCLIP", &szbls2_1.lclip, (ftnlen)5);
    glrget_("RUNDEF", &rundef, (ftnlen)6);
    glrget_("RMISS", &rmiss, (ftnlen)5);
    gllget_("LMISS", &lmiss, (ftnlen)5);
    if (upx[1] == rundef) {
	msgdmp_("E", "UHBXLZ", "RUNDEF CAN NOT BE UESED FOR UPX.", (ftnlen)1, 
		(ftnlen)6, (ftnlen)32);
    }
    s_wsfi(&io___5);
    do_fio(&c__1, (char *)&(*itype), (ftnlen)sizeof(integer));
    do_fio(&c__1, (char *)&(*index), (ftnlen)sizeof(integer));
    e_wsfi();
    cdblk_(cobj, (ftnlen)80);
    swoopn_("UHBXLZ", cobj, (ftnlen)6, (ftnlen)80);
    szslti_(itype, index);
    szoplu_();
    lyuni = upy[1] == rundef;
    if (lyuni) {
	uuqidv_(&uymin, &uymax);
	if (uymin == rundef) {
	    sgrget_("UYMIN", &uymin, (ftnlen)5);
	}
	if (uymax == rundef) {
	    sgrget_("UYMAX", &uymax, (ftnlen)5);
	}
	dy = (uymax - uymin) / *n;
    }
    szoplu_();
    lflag = FALSE_;
    i__1 = *n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	if (lyuni) {
	    uy1 = uymin + dy * (i__ - 1);
	    uy2 = uymin + dy * i__;
	} else {
	    uy1 = upy[i__];
	    uy2 = upy[i__ + 1];
	}
	if ((upx[i__] == rmiss || uy1 == rmiss || uy2 == rmiss) && lmiss) {
	    lflag = FALSE_;
	} else {
	    if (lflag) {
		szpllu_(&upx[i__], &uy1);
		szpllu_(&upx[i__], &uy2);
	    } else {
		szmvlu_(&upx[i__], &uy1);
		szpllu_(&upx[i__], &uy2);
		lflag = TRUE_;
	    }
	}
/* L20: */
    }
    szcllu_();
    swocls_("UHBXLZ", (ftnlen)6);
    return 0;
} /* uhbxlz_ */


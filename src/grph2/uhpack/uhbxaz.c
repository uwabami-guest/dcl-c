/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Common Block Declarations */

struct {
    integer irmode, irmodr;
} szbtn2_;

#define szbtn2_1 szbtn2_

struct {
    logical lclip;
} szbtn3_;

#define szbtn3_1 szbtn3_

/* Table of constant values */

static real c_b15 = 0.f;
static real c_b18 = 1.f;
static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uhbxaz_(integer *n, real *upx1, real *upx2, real *upy, 
	integer *itpat1, integer *itpat2)
{
    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    integer s_wsfi(icilist *), do_fio(integer *, char *, ftnlen), e_wsfi(void)
	    ;

    /* Local variables */
    static integer i__;
    static real dy, rx0, ry0, rx1, ry1, rx2, ry2, uy1, uy2, ux1, ux2, vx1, 
	    vy1, vx2, vy2, rot, vxx;
    static logical lxc1, lxc2;
    static char cobj[80];
    static real uref;
    extern /* Subroutine */ int cdblk_(char *, ftnlen);
    static logical lmiss;
    static real rmiss;
    static logical lyuni;
    static real uymin, uymax;
    extern /* Subroutine */ int stfpr2_(real *, real *, real *, real *), 
	    gllget_(char *, logical *, ftnlen);
    static real rundef;
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen), sglget_(char 
	    *, logical *, ftnlen), msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), sgrget_(char *, real *, ftnlen), stftrf_(real *, 
	    real *, real *, real *), swocls_(char *, ftnlen), uurget_(char *, 
	    real *, ftnlen), uuqidv_(real *, real *), swoopn_(char *, char *, 
	    ftnlen, ftnlen), szcltv_(void), szstni_(integer *), szoptv_(void),
	     szsttv_(real *, real *);

    /* Fortran I/O blocks */
    static icilist io___12 = { 0, cobj, 0, "(2I8)", 80, 1 };


    /* Parameter adjustments */
    --upy;
    --upx2;
    --upx1;

    /* Function Body */
    if (*n < 1) {
	msgdmp_("E", "UHBXAZ", "NUMBER OF POINTS IS LESS THAN 1.", (ftnlen)1, 
		(ftnlen)6, (ftnlen)32);
    }
    if (*itpat1 == 0 || *itpat2 == 0) {
	msgdmp_("M", "UHBXAZ", "TONE PAT. INDEX IS 0 / DO NOTHING.", (ftnlen)
		1, (ftnlen)6, (ftnlen)34);
	return 0;
    }
    if (*itpat1 < 0 || *itpat2 < 0) {
	msgdmp_("E", "UHBXAZ", "TONE PAT. INDEX IS LESS THAN 0.", (ftnlen)1, (
		ftnlen)6, (ftnlen)31);
    }
    sglget_("LCLIP", &szbtn3_1.lclip, (ftnlen)5);
    glrget_("RUNDEF", &rundef, (ftnlen)6);
    glrget_("RMISS", &rmiss, (ftnlen)5);
    gllget_("LMISS", &lmiss, (ftnlen)5);
    stfpr2_(&c_b15, &c_b15, &rx0, &ry0);
    stfpr2_(&c_b15, &c_b18, &rx1, &ry1);
    stfpr2_(&c_b18, &c_b15, &rx2, &ry2);
    rot = (rx2 - rx0) * (ry1 - ry0) - (ry2 - ry0) * (rx1 - rx0);
    szbtn2_1.irmode = 0;
    if (rot > 0.f) {
	szbtn2_1.irmodr = szbtn2_1.irmode;
    } else {
	szbtn2_1.irmodr = (szbtn2_1.irmode + 1) % 2;
    }
    s_wsfi(&io___12);
    do_fio(&c__1, (char *)&(*itpat1), (ftnlen)sizeof(integer));
    do_fio(&c__1, (char *)&(*itpat2), (ftnlen)sizeof(integer));
    e_wsfi();
    cdblk_(cobj, (ftnlen)80);
    swoopn_("UHBXAZ", cobj, (ftnlen)6, (ftnlen)80);
    lyuni = upy[1] == rundef;
    lxc1 = upx1[1] == rundef;
    lxc2 = upx2[1] == rundef;
    if (lyuni) {
	uuqidv_(&uymin, &uymax);
	if (uymin == rundef) {
	    sgrget_("UYMIN", &uymin, (ftnlen)5);
	}
	if (uymax == rundef) {
	    sgrget_("UYMAX", &uymax, (ftnlen)5);
	}
	dy = (uymax - uymin) / *n;
    }
    if (lxc1 || lxc2) {
	uurget_("UREF", &uref, (ftnlen)4);
    }
    i__1 = *n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	if (lyuni) {
	    uy1 = uymin + dy * (i__ - 1);
	    uy2 = uymin + dy * i__;
	} else {
	    uy1 = upy[i__];
	    uy2 = upy[i__ + 1];
	}
	if (lxc1) {
	    ux1 = uref;
	} else {
	    ux1 = upx1[i__];
	}
	if (lxc2) {
	    ux2 = uref;
	} else {
	    ux2 = upx2[i__];
	}
	if (! ((ux1 == rmiss || ux1 == rmiss || uy1 == rmiss || uy2 == rmiss) 
		&& lmiss)) {
	    stftrf_(&ux1, &uy1, &vx1, &vy1);
	    stftrf_(&ux2, &uy2, &vx2, &vy2);
	    if (ux2 > ux1) {
		szstni_(itpat1);
	    } else {
		szstni_(itpat2);
	    }
	    if ((vy2 - vy1) * (vx2 - vx1) < 0.f) {
		vxx = vx1;
		vx1 = vx2;
		vx2 = vxx;
	    }
	    szoptv_();
	    szsttv_(&vx1, &vy1);
	    szsttv_(&vx2, &vy1);
	    szsttv_(&vx2, &vy2);
	    szsttv_(&vx1, &vy2);
	    szsttv_(&vx1, &vy1);
	    szcltv_();
	}
/* L20: */
    }
    swocls_("UHBXAZ", (ftnlen)6);
    return 0;
} /* uhbxaz_ */


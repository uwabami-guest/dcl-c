/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Common Block Declarations */

struct {
    logical leqdxz;
    integer nxz;
    real uxminz, uxmaxz, dxz, xpz[@MAXNGRID];
} uwblkx_;

#define uwblkx_1 uwblkx_

/* ----------------------------------------------------------------------- */
/*     RUWGX */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
real ruwgx_(integer *ix)
{
    /* System generated locals */
    real ret_val;

/*     IF (.NOT.(1.LE.IX .AND. IX.LE.NXZ)) THEN */
/*       CALL MSGDMP('E','RUWGX','IX IS OUT OF RANGE.') */
/*     END IF */
    if (uwblkx_1.leqdxz) {
	ret_val = uwblkx_1.uxminz + uwblkx_1.dxz * (*ix - 1);
    } else {
	ret_val = uwblkx_1.xpz[*ix - 1];
    }
    return ret_val;
} /* ruwgx_ */


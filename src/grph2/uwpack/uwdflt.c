/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static logical c_false = FALSE_;

/* ----------------------------------------------------------------------- */
/*     UWDFLT */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uwdflt_(integer *nx, integer *ny)
{
    static logical lset;
    static real uxmn, uymn, uxmx, uymx;
    extern /* Subroutine */ int sgqwnd_(real *, real *, real *, real *), 
	    uwsgxb_(real *, real *, integer *), uwsgyb_(real *, real *, 
	    integer *), uwqgxz_(logical *), uwqgyz_(logical *), uwsgxz_(
	    logical *), uwsgyz_(logical *);

/*     / SET GRID ATTRIBUTE IF IT HAS NOT BEEN SET YET / */
    sgqwnd_(&uxmn, &uxmx, &uymn, &uymx);
    uwqgxz_(&lset);
    if (! lset) {
	uwsgxb_(&uxmn, &uxmx, nx);
	uwsgxz_(&c_false);
    }
    uwqgyz_(&lset);
    if (! lset) {
	uwsgyb_(&uymn, &uymx, ny);
	uwsgyz_(&c_false);
    }
    return 0;
} /* uwdflt_ */


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Common Block Declarations */

struct {
    logical leqdxz;
    integer nxz;
    real uxminz, uxmaxz, dxz, xpz[@MAXNGRID];
} uwblkx_;

#define uwblkx_1 uwblkx_

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     UWSGXA / UWSGXB */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uwsgxa_0_(int n__, real *xp, integer *nx, real *uxmin, 
	real *uxmax, logical *lsetx)
{
    /* Initialized data */

    static logical lsetxz = FALSE_;

    extern /* Subroutine */ int vrset_(real *, real *, integer *, integer *, 
	    integer *), msgdmp_(char *, char *, char *, ftnlen, ftnlen, 
	    ftnlen), uwigxi_(void);

    /* Parameter adjustments */
    if (xp) {
	--xp;
	}

    /* Function Body */
    switch(n__) {
	case 1: goto L_uwqgxa;
	case 2: goto L_uwsgxb;
	case 3: goto L_uwqgxb;
	case 4: goto L_uwsgxz;
	case 5: goto L_uwqgxz;
	}

    if (*nx < 2) {
	msgdmp_("E", "UWSGXA", "NUMBER OF POINTS IS INVALID.", (ftnlen)1, (
		ftnlen)6, (ftnlen)28);
    }
    if (*nx > @MAXNGRID) {
	msgdmp_("E", "UWSGXA", "WORKING AREA IS NOT ENOUGH.", (ftnlen)1, (
		ftnlen)6, (ftnlen)27);
    }
    uwblkx_1.leqdxz = FALSE_;
    uwblkx_1.nxz = *nx;
    vrset_(&xp[1], uwblkx_1.xpz, nx, &c__1, &c__1);
    uwblkx_1.uxminz = xp[1];
    uwblkx_1.uxmaxz = xp[*nx];
    lsetxz = TRUE_;
    uwigxi_();
    return 0;
/* ----------------------------------------------------------------------- */

L_uwqgxa:
    *nx = uwblkx_1.nxz;
    vrset_(uwblkx_1.xpz, &xp[1], nx, &c__1, &c__1);
    return 0;
/* ----------------------------------------------------------------------- */

L_uwsgxb:
    if (*uxmin == *uxmax) {
	msgdmp_("E", "UWSGXB", "UXMIN = UXMAX.", (ftnlen)1, (ftnlen)6, (
		ftnlen)14);
    }
    uwblkx_1.leqdxz = TRUE_;
    uwblkx_1.nxz = *nx;
    uwblkx_1.uxminz = *uxmin;
    uwblkx_1.uxmaxz = *uxmax;
    uwblkx_1.dxz = (*uxmax - *uxmin) / (*nx - 1);
    lsetxz = TRUE_;
    uwigxi_();
    return 0;
/* ----------------------------------------------------------------------- */

L_uwqgxb:
    *nx = uwblkx_1.nxz;
    *uxmin = uwblkx_1.uxminz;
    *uxmax = uwblkx_1.uxmaxz;
    return 0;
/* ----------------------------------------------------------------------- */

L_uwsgxz:
    lsetxz = *lsetx;
    return 0;
/* ----------------------------------------------------------------------- */

L_uwqgxz:
    *lsetx = lsetxz;
    return 0;
} /* uwsgxa_ */

/* Subroutine */ int uwsgxa_(real *xp, integer *nx)
{
    return uwsgxa_0_(0, xp, nx, (real *)0, (real *)0, (logical *)0);
    }

/* Subroutine */ int uwqgxa_(real *xp, integer *nx)
{
    return uwsgxa_0_(1, xp, nx, (real *)0, (real *)0, (logical *)0);
    }

/* Subroutine */ int uwsgxb_(real *uxmin, real *uxmax, integer *nx)
{
    return uwsgxa_0_(2, (real *)0, nx, uxmin, uxmax, (logical *)0);
    }

/* Subroutine */ int uwqgxb_(real *uxmin, real *uxmax, integer *nx)
{
    return uwsgxa_0_(3, (real *)0, nx, uxmin, uxmax, (logical *)0);
    }

/* Subroutine */ int uwsgxz_(logical *lsetx)
{
    return uwsgxa_0_(4, (real *)0, (integer *)0, (real *)0, (real *)0, lsetx);
    }

/* Subroutine */ int uwqgxz_(logical *lsetx)
{
    return uwsgxa_0_(5, (real *)0, (integer *)0, (real *)0, (real *)0, lsetx);
    }


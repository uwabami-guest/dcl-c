/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Common Block Declarations */

struct {
    logical leqdyz;
    integer nyz;
    real uyminz, uymaxz, dyz, ypz[@MAXNGRID];
} uwblky_;

#define uwblky_1 uwblky_

/* ----------------------------------------------------------------------- */
/*     RUWGY */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
real ruwgy_(integer *iy)
{
    /* System generated locals */
    real ret_val;

/*     IF (.NOT.(1.LE.IY .AND. IY.LE.NYZ)) THEN */
/*       CALL MSGDMP('E','RUWGY','IY IS OUT OF RANGE.') */
/*     END IF */
    if (uwblky_1.leqdyz) {
	ret_val = uwblky_1.uyminz + uwblky_1.dyz * (*iy - 1);
    } else {
	ret_val = uwblky_1.ypz[*iy - 1];
    }
    return ret_val;
} /* ruwgy_ */


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     UGSGXA / UGSGXB */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int ugsgxa_0_(int n__, real *xp, integer *nx, real *uxmin, 
	real *uxmax, logical *lsetx)
{
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), uwqgxa_(real *, integer *), uwqgxb_(real *, real 
	    *, integer *), uwsgxa_(real *, integer *), uwsgxb_(real *, real *,
	     integer *), uwqgxz_(logical *), uwsgxz_(logical *);

    /* Parameter adjustments */
    if (xp) {
	--xp;
	}

    /* Function Body */
    switch(n__) {
	case 1: goto L_ugqgxa;
	case 2: goto L_ugsgxb;
	case 3: goto L_ugqgxb;
	case 4: goto L_ugsgxz;
	case 5: goto L_ugqgxz;
	}

    msgdmp_("M", "UGSGXA", "THIS IS OLD INTERFACE - USE UWSGXA !", (ftnlen)1, 
	    (ftnlen)6, (ftnlen)36);
    uwsgxa_(&xp[1], nx);
    return 0;
/* ----------------------------------------------------------------------- */

L_ugqgxa:
    msgdmp_("M", "UGQGXA", "THIS IS OLD INTERFACE - USE UWQGXA !", (ftnlen)1, 
	    (ftnlen)6, (ftnlen)36);
    uwqgxa_(&xp[1], nx);
    return 0;
/* ----------------------------------------------------------------------- */

L_ugsgxb:
    msgdmp_("M", "UGSGXB", "THIS IS OLD INTERFACE - USE UWSGXB !", (ftnlen)1, 
	    (ftnlen)6, (ftnlen)36);
    uwsgxb_(uxmin, uxmax, nx);
    return 0;
/* ----------------------------------------------------------------------- */

L_ugqgxb:
    msgdmp_("M", "UGQGXB", "THIS IS OLD INTERFACE - USE UWQGXB !", (ftnlen)1, 
	    (ftnlen)6, (ftnlen)36);
    uwqgxb_(uxmin, uxmax, nx);
    return 0;
/* ----------------------------------------------------------------------- */

L_ugsgxz:
    msgdmp_("M", "UGSGXZ", "THIS IS OLD INTERFACE - USE UWSGXZ !", (ftnlen)1, 
	    (ftnlen)6, (ftnlen)36);
    uwsgxz_(lsetx);
    return 0;
/* ----------------------------------------------------------------------- */

L_ugqgxz:
    msgdmp_("M", "UGQGXZ", "THIS IS OLD INTERFACE - USE UWQGXZ !", (ftnlen)1, 
	    (ftnlen)6, (ftnlen)36);
    uwqgxz_(lsetx);
    return 0;
} /* ugsgxa_ */

/* Subroutine */ int ugsgxa_(real *xp, integer *nx)
{
    return ugsgxa_0_(0, xp, nx, (real *)0, (real *)0, (logical *)0);
    }

/* Subroutine */ int ugqgxa_(real *xp, integer *nx)
{
    return ugsgxa_0_(1, xp, nx, (real *)0, (real *)0, (logical *)0);
    }

/* Subroutine */ int ugsgxb_(real *uxmin, real *uxmax, integer *nx)
{
    return ugsgxa_0_(2, (real *)0, nx, uxmin, uxmax, (logical *)0);
    }

/* Subroutine */ int ugqgxb_(real *uxmin, real *uxmax, integer *nx)
{
    return ugsgxa_0_(3, (real *)0, nx, uxmin, uxmax, (logical *)0);
    }

/* Subroutine */ int ugsgxz_(logical *lsetx)
{
    return ugsgxa_0_(4, (real *)0, (integer *)0, (real *)0, (real *)0, lsetx);
    }

/* Subroutine */ int ugqgxz_(logical *lsetx)
{
    return ugsgxa_0_(5, (real *)0, (integer *)0, (real *)0, (real *)0, lsetx);
    }


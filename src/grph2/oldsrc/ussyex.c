/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int ussyex_0_(int n__, integer *n, real *y, real *yminq, 
	real *ymaxq)
{
    static real rundef;
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen), msgdmp_(char 
	    *, char *, char *, ftnlen, ftnlen, ftnlen), usrget_(char *, real *
	    , ftnlen), usspnt_(integer *, real *, real *);

    /* Parameter adjustments */
    if (y) {
	--y;
	}

    /* Function Body */
    switch(n__) {
	case 1: goto L_usqyex;
	}

    msgdmp_("W", "USSXEY", "THIS IS OLD INTERFACE - USE USSPNT !", (ftnlen)1, 
	    (ftnlen)6, (ftnlen)36);
    glrget_("RUNDEF", &rundef, (ftnlen)6);
    usspnt_(n, &rundef, &y[1]);
    return 0;
/* ----------------------------------------------------------------------- */

L_usqyex:
    msgdmp_("W", "USQYEX", "THIS IS OLD INTERFACE - USE USRGET !", (ftnlen)1, 
	    (ftnlen)6, (ftnlen)36);
    usrget_("YDTMIN", yminq, (ftnlen)6);
    usrget_("YDTMAX", ymaxq, (ftnlen)6);
    return 0;
} /* ussyex_ */

/* Subroutine */ int ussyex_(integer *n, real *y)
{
    return ussyex_0_(0, n, y, (real *)0, (real *)0);
    }

/* Subroutine */ int usqyex_(real *yminq, real *ymaxq)
{
    return ussyex_0_(1, (integer *)0, (real *)0, yminq, ymaxq);
    }


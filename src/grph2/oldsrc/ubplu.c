/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static logical c_true = TRUE_;

/* ----------------------------------------------------------------------- */
/*     UBPACK */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int ubplu_0_(int n__, integer *n, real *upx, real *upy, real 
	*vpx, real *vpy, integer *itype, integer *index, char *charx, real *
	rsize, ftnlen charx_len)
{
    static logical lchar;
    extern /* Subroutine */ int sgplu_(integer *, real *, real *), sgplv_(
	    integer *, real *, real *), sglget_(char *, logical *, ftnlen), 
	    sgnplc_(void), msgdmp_(char *, char *, char *, ftnlen, ftnlen, 
	    ftnlen), sgqplc_(char *, ftnlen), sgsplc_(char *, ftnlen), 
	    sgqpli_(integer *), sglset_(char *, logical *, ftnlen), sgspli_(
	    integer *), sgqplt_(integer *), sgspls_(real *), sgsplt_(integer *
	    ), sgqpls_(real *);

    /* Parameter adjustments */
    if (upx) {
	--upx;
	}
    if (upy) {
	--upy;
	}
    if (vpx) {
	--vpx;
	}
    if (vpy) {
	--vpy;
	}

    /* Function Body */
    switch(n__) {
	case 1: goto L_ubplv;
	case 2: goto L_ubsplt;
	case 3: goto L_ubqplt;
	case 4: goto L_ubspli;
	case 5: goto L_ubqpli;
	case 6: goto L_ubsplc;
	case 7: goto L_ubqplc;
	case 8: goto L_ubspls;
	case 9: goto L_ubqpls;
	}

    msgdmp_("M", "UBPLU", "THIS IS OLD INTERFACE - USE SGPLU !", (ftnlen)1, (
	    ftnlen)5, (ftnlen)35);
    sglget_("LCHAR", &lchar, (ftnlen)5);
    sglset_("LCHAR", &c_true, (ftnlen)5);
    sgplu_(n, &upx[1], &upy[1]);
    sglset_("LCHAR", &lchar, (ftnlen)5);
    sgnplc_();
    return 0;
/* ----------------------------------------------------------------------- */

L_ubplv:
    msgdmp_("M", "UBPLV", "THIS IS OLD INTERFACE - USE SGPLV !", (ftnlen)1, (
	    ftnlen)5, (ftnlen)35);
    sglget_("LCHAR", &lchar, (ftnlen)5);
    sglset_("LCHAR", &c_true, (ftnlen)5);
    sgplv_(n, &vpx[1], &vpy[1]);
    sglset_("LCHAR", &lchar, (ftnlen)5);
    sgnplc_();
    return 0;
/* ----------------------------------------------------------------------- */

L_ubsplt:
    msgdmp_("M", "UBSPLT", "THIS IS OLD INTERFACE - USE SGSPLT !", (ftnlen)1, 
	    (ftnlen)6, (ftnlen)36);
    sgsplt_(itype);
    return 0;
/* ----------------------------------------------------------------------- */

L_ubqplt:
    msgdmp_("M", "UBQPLT", "THIS IS OLD INTERFACE - USE SGQPLT !", (ftnlen)1, 
	    (ftnlen)6, (ftnlen)36);
    sgqplt_(itype);
    return 0;
/* ----------------------------------------------------------------------- */

L_ubspli:
    msgdmp_("M", "UBSPLI", "THIS IS OLD INTERFACE - USE SGSPLI !", (ftnlen)1, 
	    (ftnlen)6, (ftnlen)36);
    sgspli_(index);
    return 0;
/* ----------------------------------------------------------------------- */

L_ubqpli:
    msgdmp_("M", "UBQPLI", "THIS IS OLD INTERFACE - USE SGQPLI !", (ftnlen)1, 
	    (ftnlen)6, (ftnlen)36);
    sgqpli_(index);
    return 0;
/* ----------------------------------------------------------------------- */

L_ubsplc:
    msgdmp_("M", "UBSPLC", "THIS IS OLD INTERFACE - USE SGSPLC !", (ftnlen)1, 
	    (ftnlen)6, (ftnlen)36);
    sgsplc_(charx, charx_len);
    return 0;
/* ----------------------------------------------------------------------- */

L_ubqplc:
    msgdmp_("M", "UBQPLC", "THIS IS OLD INTERFACE - USE SGQPLC !", (ftnlen)1, 
	    (ftnlen)6, (ftnlen)36);
    sgqplc_(charx, charx_len);
    return 0;
/* ----------------------------------------------------------------------- */

L_ubspls:
    msgdmp_("M", "UBSPLS", "THIS IS OLD INTERFACE - USE SGSPLS !", (ftnlen)1, 
	    (ftnlen)6, (ftnlen)36);
    sgspls_(rsize);
    return 0;
/* ----------------------------------------------------------------------- */

L_ubqpls:
    msgdmp_("M", "UBQPLS", "THIS IS OLD INTERFACE - USE SGQPLS !", (ftnlen)1, 
	    (ftnlen)6, (ftnlen)36);
    sgqpls_(rsize);
    return 0;
} /* ubplu_ */

/* Subroutine */ int ubplu_(integer *n, real *upx, real *upy)
{
    return ubplu_0_(0, n, upx, upy, (real *)0, (real *)0, (integer *)0, (
	    integer *)0, (char *)0, (real *)0, (ftnint)0);
    }

/* Subroutine */ int ubplv_(integer *n, real *vpx, real *vpy)
{
    return ubplu_0_(1, n, (real *)0, (real *)0, vpx, vpy, (integer *)0, (
	    integer *)0, (char *)0, (real *)0, (ftnint)0);
    }

/* Subroutine */ int ubsplt_(integer *itype)
{
    return ubplu_0_(2, (integer *)0, (real *)0, (real *)0, (real *)0, (real *)
	    0, itype, (integer *)0, (char *)0, (real *)0, (ftnint)0);
    }

/* Subroutine */ int ubqplt_(integer *itype)
{
    return ubplu_0_(3, (integer *)0, (real *)0, (real *)0, (real *)0, (real *)
	    0, itype, (integer *)0, (char *)0, (real *)0, (ftnint)0);
    }

/* Subroutine */ int ubspli_(integer *index)
{
    return ubplu_0_(4, (integer *)0, (real *)0, (real *)0, (real *)0, (real *)
	    0, (integer *)0, index, (char *)0, (real *)0, (ftnint)0);
    }

/* Subroutine */ int ubqpli_(integer *index)
{
    return ubplu_0_(5, (integer *)0, (real *)0, (real *)0, (real *)0, (real *)
	    0, (integer *)0, index, (char *)0, (real *)0, (ftnint)0);
    }

/* Subroutine */ int ubsplc_(char *charx, ftnlen charx_len)
{
    return ubplu_0_(6, (integer *)0, (real *)0, (real *)0, (real *)0, (real *)
	    0, (integer *)0, (integer *)0, charx, (real *)0, charx_len);
    }

/* Subroutine */ int ubqplc_(char *charx, ftnlen charx_len)
{
    return ubplu_0_(7, (integer *)0, (real *)0, (real *)0, (real *)0, (real *)
	    0, (integer *)0, (integer *)0, charx, (real *)0, charx_len);
    }

/* Subroutine */ int ubspls_(real *rsize)
{
    return ubplu_0_(8, (integer *)0, (real *)0, (real *)0, (real *)0, (real *)
	    0, (integer *)0, (integer *)0, (char *)0, rsize, (ftnint)0);
    }

/* Subroutine */ int ubqpls_(real *rsize)
{
    return ubplu_0_(9, (integer *)0, (real *)0, (real *)0, (real *)0, (real *)
	    0, (integer *)0, (integer *)0, (char *)0, rsize, (ftnint)0);
    }


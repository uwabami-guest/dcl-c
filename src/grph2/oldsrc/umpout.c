/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     DRAW CONTINENTAL OUTLINES */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int umpout_(void)
{
    static integer ig;
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), umiget_(char *, integer *, ftnlen), umpmap_(char 
	    *, ftnlen);

    msgdmp_("M", "UMPOUT", "THIS IS OLD INTERFACE - USE UMPMAP !", (ftnlen)1, 
	    (ftnlen)6, (ftnlen)36);
    umiget_("IGROUP", &ig, (ftnlen)6);
    if (ig == 1) {
	umpmap_("COAST_WORLD", (ftnlen)11);
    } else if (ig == 2) {
	umpmap_("COAST_WORLD", (ftnlen)11);
	umpmap_("BORDER_WORLD", (ftnlen)12);
    } else {
	msgdmp_("M", "UMPOUT", "INVALID GROUP NUMBER / DO NOTHING.", (ftnlen)
		1, (ftnlen)6, (ftnlen)34);
	return 0;
    }
    return 0;
} /* umpout_ */


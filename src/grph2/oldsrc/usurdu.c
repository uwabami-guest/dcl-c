/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     USPACK ROUND UMIN AND UMAX (UNIFORM)            S.Sakai  92/03/04 */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int usurdu_0_(int n__, real *umin, real *umax, real *vmin, 
	real *vmax, real *dutq)
{
    static real dut;
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), usurdt_(real *, real *, real *, real *, real *);

    switch(n__) {
	case 1: goto L_usqdut;
	}

    msgdmp_("M", "USURDU", "THIS IS OLD INTAFACE", (ftnlen)1, (ftnlen)6, (
	    ftnlen)20);
    usurdt_(umin, umax, vmin, vmax, &dut);
    return 0;

L_usqdut:
    msgdmp_("M", "USQDUT", "THIS IS OLD INTAFACE", (ftnlen)1, (ftnlen)6, (
	    ftnlen)20);
    *dutq = dut;
    return 0;
} /* usurdu_ */

/* Subroutine */ int usurdu_(real *umin, real *umax, real *vmin, real *vmax)
{
    return usurdu_0_(0, umin, umax, vmin, vmax, (real *)0);
    }

/* Subroutine */ int usqdut_(real *dutq)
{
    return usurdu_0_(1, (real *)0, (real *)0, (real *)0, (real *)0, dutq);
    }


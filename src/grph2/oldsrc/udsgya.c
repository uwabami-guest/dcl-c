/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     UDSGYA / UDSGYB */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int udsgya_0_(int n__, real *yp, integer *ny, real *uymin, 
	real *uymax, logical *lsety)
{
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), uwqgya_(real *, integer *), uwqgyb_(real *, real 
	    *, integer *), uwsgya_(real *, integer *), uwsgyb_(real *, real *,
	     integer *), uwqgyz_(logical *), uwsgyz_(logical *);

    /* Parameter adjustments */
    if (yp) {
	--yp;
	}

    /* Function Body */
    switch(n__) {
	case 1: goto L_udqgya;
	case 2: goto L_udsgyb;
	case 3: goto L_udqgyb;
	case 4: goto L_udsgyz;
	case 5: goto L_udqgyz;
	}

    msgdmp_("M", "UDSGYA", "THIS IS OLD INTERFACE - USE UWSGYA !", (ftnlen)1, 
	    (ftnlen)6, (ftnlen)36);
    uwsgya_(&yp[1], ny);
    return 0;
/* ----------------------------------------------------------------------- */

L_udqgya:
    msgdmp_("M", "UDQGYA", "THIS IS OLD INTERFACE - USE UWQGYA !", (ftnlen)1, 
	    (ftnlen)6, (ftnlen)36);
    uwqgya_(&yp[1], ny);
    return 0;
/* ----------------------------------------------------------------------- */

L_udsgyb:
    msgdmp_("M", "UDSGYB", "THIS IS OLD INTERFACE - USE UWSGYB !", (ftnlen)1, 
	    (ftnlen)6, (ftnlen)36);
    uwsgyb_(uymin, uymax, ny);
    return 0;
/* ----------------------------------------------------------------------- */

L_udqgyb:
    msgdmp_("M", "UDQGYB", "THIS IS OLD INTERFACE - USE UWQGYB !", (ftnlen)1, 
	    (ftnlen)6, (ftnlen)36);
    uwqgyb_(uymin, uymax, ny);
    return 0;
/* ----------------------------------------------------------------------- */

L_udsgyz:
    msgdmp_("M", "UDSGYZ", "THIS IS OLD INTERFACE - USE UWSGYZ !", (ftnlen)1, 
	    (ftnlen)6, (ftnlen)36);
    uwsgyz_(lsety);
    return 0;
/* ----------------------------------------------------------------------- */

L_udqgyz:
    msgdmp_("M", "UDQGYZ", "THIS IS OLD INTERFACE - USE UWQGYZ !", (ftnlen)1, 
	    (ftnlen)6, (ftnlen)36);
    uwqgyz_(lsety);
    return 0;
} /* udsgya_ */

/* Subroutine */ int udsgya_(real *yp, integer *ny)
{
    return udsgya_0_(0, yp, ny, (real *)0, (real *)0, (logical *)0);
    }

/* Subroutine */ int udqgya_(real *yp, integer *ny)
{
    return udsgya_0_(1, yp, ny, (real *)0, (real *)0, (logical *)0);
    }

/* Subroutine */ int udsgyb_(real *uymin, real *uymax, integer *ny)
{
    return udsgya_0_(2, (real *)0, ny, uymin, uymax, (logical *)0);
    }

/* Subroutine */ int udqgyb_(real *uymin, real *uymax, integer *ny)
{
    return udsgya_0_(3, (real *)0, ny, uymin, uymax, (logical *)0);
    }

/* Subroutine */ int udsgyz_(logical *lsety)
{
    return udsgya_0_(4, (real *)0, (integer *)0, (real *)0, (real *)0, lsety);
    }

/* Subroutine */ int udqgyz_(logical *lsety)
{
    return udsgya_0_(5, (real *)0, (integer *)0, (real *)0, (real *)0, lsety);
    }


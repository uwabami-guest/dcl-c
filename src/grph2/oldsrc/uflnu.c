/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     UFPACK */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uflnu_0_(int n__, real *ux1, real *uy1, real *ux2, real *
	uy2, real *vx1, real *vy1, real *vx2, real *vy2, real *rx1, real *ry1,
	 real *rx2, real *ry2, integer *index)
{
    extern /* Subroutine */ int sglar_(real *, real *, real *, real *), 
	    sglau_(real *, real *, real *, real *), sglav_(real *, real *, 
	    real *, real *), sgqlai_(integer *), sgslai_(integer *), msgdmp_(
	    char *, char *, char *, ftnlen, ftnlen, ftnlen);

    switch(n__) {
	case 1: goto L_uflnv;
	case 2: goto L_uflnr;
	case 3: goto L_ufslni;
	case 4: goto L_ufqlni;
	}

    msgdmp_("M", "UFLNU", "THIS IS OLD INTERFACE - USE SGLAU !", (ftnlen)1, (
	    ftnlen)5, (ftnlen)35);
    sglau_(ux1, uy1, ux2, uy2);
    return 0;
/* ----------------------------------------------------------------------- */

L_uflnv:
    msgdmp_("M", "UFLNV", "THIS IS OLD INTERFACE - USE SGLAV !", (ftnlen)1, (
	    ftnlen)5, (ftnlen)35);
    sglav_(vx1, vy1, vx2, vy2);
    return 0;
/* ----------------------------------------------------------------------- */

L_uflnr:
    msgdmp_("M", "UFLNR", "THIS IS OLD INTERFACE - USE SGLAR !", (ftnlen)1, (
	    ftnlen)5, (ftnlen)35);
    sglar_(rx1, ry1, rx2, ry2);
    return 0;
/* ----------------------------------------------------------------------- */

L_ufslni:
    msgdmp_("M", "UFSLNI", "THIS IS OLD INTERFACE - USE SGSLAI !", (ftnlen)1, 
	    (ftnlen)6, (ftnlen)36);
    sgslai_(index);
    return 0;
/* ----------------------------------------------------------------------- */

L_ufqlni:
    msgdmp_("M", "UFQLNI", "THIS IS OLD INTERFACE - USE SGQLAI !", (ftnlen)1, 
	    (ftnlen)6, (ftnlen)36);
    sgqlai_(index);
    return 0;
} /* uflnu_ */

/* Subroutine */ int uflnu_(real *ux1, real *uy1, real *ux2, real *uy2)
{
    return uflnu_0_(0, ux1, uy1, ux2, uy2, (real *)0, (real *)0, (real *)0, (
	    real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (integer *)
	    0);
    }

/* Subroutine */ int uflnv_(real *vx1, real *vy1, real *vx2, real *vy2)
{
    return uflnu_0_(1, (real *)0, (real *)0, (real *)0, (real *)0, vx1, vy1, 
	    vx2, vy2, (real *)0, (real *)0, (real *)0, (real *)0, (integer *)
	    0);
    }

/* Subroutine */ int uflnr_(real *rx1, real *ry1, real *rx2, real *ry2)
{
    return uflnu_0_(2, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, 
	    (real *)0, (real *)0, (real *)0, rx1, ry1, rx2, ry2, (integer *)0)
	    ;
    }

/* Subroutine */ int ufslni_(integer *index)
{
    return uflnu_0_(3, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, 
	    (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, 
	    (real *)0, index);
    }

/* Subroutine */ int ufqlni_(integer *index)
{
    return uflnu_0_(4, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, 
	    (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0, 
	    (real *)0, index);
    }


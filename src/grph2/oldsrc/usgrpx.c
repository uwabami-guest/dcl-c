/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int usgrpx_(integer *n, real *x, real *ymin, real *ymax)
{
    extern /* Subroutine */ int uulin_(integer *, real *, real *);
    static real rundef;
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen), msgdmp_(char 
	    *, char *, char *, ftnlen, ftnlen, ftnlen), sgrset_(char *, real *
	    , ftnlen), usaxis_(void), usspnt_(integer *, real *, real *);

    /* Parameter adjustments */
    --x;

    /* Function Body */
    msgdmp_("W", "USGRPX", "THIS IS OLD INTERFACE - USE USGRPH !", (ftnlen)1, 
	    (ftnlen)6, (ftnlen)36);
    glrget_("RUNDEF", &rundef, (ftnlen)6);
    sgrset_("UYMIN", ymin, (ftnlen)5);
    sgrset_("UYMAX", ymax, (ftnlen)5);
    usspnt_(n, &x[1], &rundef);
    usaxis_();
    uulin_(n, &x[1], &rundef);
    return 0;
} /* usgrpx_ */


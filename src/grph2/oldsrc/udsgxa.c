/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     UDSGXA / UDSGXB */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int udsgxa_0_(int n__, real *xp, integer *nx, real *uxmin, 
	real *uxmax, logical *lsetx)
{
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), uwqgxa_(real *, integer *), uwqgxb_(real *, real 
	    *, integer *), uwsgxa_(real *, integer *), uwsgxb_(real *, real *,
	     integer *), uwqgxz_(logical *), uwsgxz_(logical *);

    /* Parameter adjustments */
    if (xp) {
	--xp;
	}

    /* Function Body */
    switch(n__) {
	case 1: goto L_udqgxa;
	case 2: goto L_udsgxb;
	case 3: goto L_udqgxb;
	case 4: goto L_udsgxz;
	case 5: goto L_udqgxz;
	}

    msgdmp_("M", "UDSGXA", "THIS IS OLD INTERFACE - USE UWSGXA !", (ftnlen)1, 
	    (ftnlen)6, (ftnlen)36);
    uwsgxa_(&xp[1], nx);
    return 0;
/* ----------------------------------------------------------------------- */

L_udqgxa:
    msgdmp_("M", "UDQGXA", "THIS IS OLD INTERFACE - USE UWQGXA !", (ftnlen)1, 
	    (ftnlen)6, (ftnlen)36);
    uwqgxa_(&xp[1], nx);
    return 0;
/* ----------------------------------------------------------------------- */

L_udsgxb:
    msgdmp_("M", "UDSGXB", "THIS IS OLD INTERFACE - USE UWSGXB !", (ftnlen)1, 
	    (ftnlen)6, (ftnlen)36);
    uwsgxb_(uxmin, uxmax, nx);
    return 0;
/* ----------------------------------------------------------------------- */

L_udqgxb:
    msgdmp_("M", "UDQGXB", "THIS IS OLD INTERFACE - USE UWQGXB !", (ftnlen)1, 
	    (ftnlen)6, (ftnlen)36);
    uwqgxb_(uxmin, uxmax, nx);
    return 0;
/* ----------------------------------------------------------------------- */

L_udsgxz:
    msgdmp_("M", "UDSGXZ", "THIS IS OLD INTERFACE - USE UWSGXZ !", (ftnlen)1, 
	    (ftnlen)6, (ftnlen)36);
    uwsgxz_(lsetx);
    return 0;
/* ----------------------------------------------------------------------- */

L_udqgxz:
    msgdmp_("M", "UDQGXZ", "THIS IS OLD INTERFACE - USE UWQGXZ !", (ftnlen)1, 
	    (ftnlen)6, (ftnlen)36);
    uwqgxz_(lsetx);
    return 0;
} /* udsgxa_ */

/* Subroutine */ int udsgxa_(real *xp, integer *nx)
{
    return udsgxa_0_(0, xp, nx, (real *)0, (real *)0, (logical *)0);
    }

/* Subroutine */ int udqgxa_(real *xp, integer *nx)
{
    return udsgxa_0_(1, xp, nx, (real *)0, (real *)0, (logical *)0);
    }

/* Subroutine */ int udsgxb_(real *uxmin, real *uxmax, integer *nx)
{
    return udsgxa_0_(2, (real *)0, nx, uxmin, uxmax, (logical *)0);
    }

/* Subroutine */ int udqgxb_(real *uxmin, real *uxmax, integer *nx)
{
    return udsgxa_0_(3, (real *)0, nx, uxmin, uxmax, (logical *)0);
    }

/* Subroutine */ int udsgxz_(logical *lsetx)
{
    return udsgxa_0_(4, (real *)0, (integer *)0, (real *)0, (real *)0, lsetx);
    }

/* Subroutine */ int udqgxz_(logical *lsetx)
{
    return udsgxa_0_(5, (real *)0, (integer *)0, (real *)0, (real *)0, lsetx);
    }


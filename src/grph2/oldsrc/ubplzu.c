/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static logical c_true = TRUE_;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int ubplzu_(integer *n, real *upx, real *upy, integer *itype,
	 integer *index, char *charx, real *rsize, ftnlen charx_len)
{
    static logical lchar;
    extern /* Subroutine */ int sglget_(char *, logical *, ftnlen), msgdmp_(
	    char *, char *, char *, ftnlen, ftnlen, ftnlen), sgsplc_(char *, 
	    ftnlen), sglset_(char *, logical *, ftnlen), sgspls_(real *), 
	    sgplzu_(integer *, real *, real *, integer *, integer *);

    /* Parameter adjustments */
    --upy;
    --upx;

    /* Function Body */
    msgdmp_("M", "UBPLZU", "THIS IS OLD INTERFACE - USE SGPLZU !", (ftnlen)1, 
	    (ftnlen)6, (ftnlen)36);
    sglget_("LCHAR", &lchar, (ftnlen)5);
    sglset_("LCHAR", &c_true, (ftnlen)5);
    sgsplc_(charx, charx_len);
    sgspls_(rsize);
    sgplzu_(n, &upx[1], &upy[1], itype, index);
    sglset_("LCHAR", &lchar, (ftnlen)5);
    return 0;
} /* ubplzu_ */


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     UESGYA / UESGYB */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uesgya_0_(int n__, real *yp, integer *ny, real *uymin, 
	real *uymax, logical *lsety)
{
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), uwqgya_(real *, integer *), uwqgyb_(real *, real 
	    *, integer *), uwsgya_(real *, integer *), uwsgyb_(real *, real *,
	     integer *), uwqgyz_(logical *), uwsgyz_(logical *);

    /* Parameter adjustments */
    if (yp) {
	--yp;
	}

    /* Function Body */
    switch(n__) {
	case 1: goto L_ueqgya;
	case 2: goto L_uesgyb;
	case 3: goto L_ueqgyb;
	case 4: goto L_uesgyz;
	case 5: goto L_ueqgyz;
	}

    msgdmp_("M", "UESGYA", "THIS IS OLD INTERFACE - USE UWSGYA !", (ftnlen)1, 
	    (ftnlen)6, (ftnlen)36);
    uwsgya_(&yp[1], ny);
    return 0;
/* ----------------------------------------------------------------------- */

L_ueqgya:
    msgdmp_("M", "UEQGYA", "THIS IS OLD INTERFACE - USE UWQGYA !", (ftnlen)1, 
	    (ftnlen)6, (ftnlen)36);
    uwqgya_(&yp[1], ny);
    return 0;
/* ----------------------------------------------------------------------- */

L_uesgyb:
    msgdmp_("M", "UESGYB", "THIS IS OLD INTERFACE - USE UWSGYB !", (ftnlen)1, 
	    (ftnlen)6, (ftnlen)36);
    uwsgyb_(uymin, uymax, ny);
    return 0;
/* ----------------------------------------------------------------------- */

L_ueqgyb:
    msgdmp_("M", "UEQGYB", "THIS IS OLD INTERFACE - USE UWQGYB !", (ftnlen)1, 
	    (ftnlen)6, (ftnlen)36);
    uwqgyb_(uymin, uymax, ny);
    return 0;
/* ----------------------------------------------------------------------- */

L_uesgyz:
    msgdmp_("M", "UESGYZ", "THIS IS OLD INTERFACE - USE UWSGYZ !", (ftnlen)1, 
	    (ftnlen)6, (ftnlen)36);
    uwsgyz_(lsety);
    return 0;
/* ----------------------------------------------------------------------- */

L_ueqgyz:
    msgdmp_("M", "UEQGYZ", "THIS IS OLD INTERFACE - USE UWQGYZ !", (ftnlen)1, 
	    (ftnlen)6, (ftnlen)36);
    uwqgyz_(lsety);
    return 0;
} /* uesgya_ */

/* Subroutine */ int uesgya_(real *yp, integer *ny)
{
    return uesgya_0_(0, yp, ny, (real *)0, (real *)0, (logical *)0);
    }

/* Subroutine */ int ueqgya_(real *yp, integer *ny)
{
    return uesgya_0_(1, yp, ny, (real *)0, (real *)0, (logical *)0);
    }

/* Subroutine */ int uesgyb_(real *uymin, real *uymax, integer *ny)
{
    return uesgya_0_(2, (real *)0, ny, uymin, uymax, (logical *)0);
    }

/* Subroutine */ int ueqgyb_(real *uymin, real *uymax, integer *ny)
{
    return uesgya_0_(3, (real *)0, ny, uymin, uymax, (logical *)0);
    }

/* Subroutine */ int uesgyz_(logical *lsety)
{
    return uesgya_0_(4, (real *)0, (integer *)0, (real *)0, (real *)0, lsety);
    }

/* Subroutine */ int ueqgyz_(logical *lsety)
{
    return uesgya_0_(5, (real *)0, (integer *)0, (real *)0, (real *)0, lsety);
    }


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uflnzu_(real *ux1, real *uy1, real *ux2, real *uy2, 
	integer *index)
{
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), sglazu_(real *, real *, real *, real *, integer *
	    , integer *);

    msgdmp_("M", "UFLNZU", "THIS IS OLD INTERFACE - USE SGLAZU !", (ftnlen)1, 
	    (ftnlen)6, (ftnlen)36);
    sglazu_(ux1, uy1, ux2, uy2, &c__1, index);
    return 0;
} /* uflnzu_ */


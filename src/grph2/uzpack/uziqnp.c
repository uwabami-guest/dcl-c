/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__3 = 3;
static integer c__27 = 27;

/* ----------------------------------------------------------------------- */
/*     UZIQNP / UZIQID / UZIQCP / UZIQVL / UZISVL */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uziqnp_0_(int n__, integer *ncp, char *cp, integer *idx, 
	integer *ipara, integer *in, integer *iu, ftnlen cp_len)
{
    /* Initialized data */

    static char cparas[8*27] = "IROTLXB " "IROTLXT " "IROTLXU " "IROTLYL " 
	    "IROTLYR " "IROTLYU " "IROTCXB " "IROTCXT " "IROTCXU " "IROTCYL " 
	    "IROTCYR " "IROTCYU " "ICENTXB " "ICENTXT " "ICENTXU " "ICENTYL " 
	    "ICENTYR " "ICENTYU " "INDEXT0 " "INDEXT1 " "INDEXT2 " "INDEXL0 " 
	    "INDEXL1 " "INDEXL2 " "IFLAG   " "INNER   " "IUNDEF  ";
    static integer ix[27] = { 0,0,0,0,0,0,0,0,0,1,1,1,0,0,0,1,1,1,-999,1,3,
	    -999,3,3,-1,1,-999 };
    static char cparal[40*27] = "BOTTOM_LABEL_ANGLE                      " 
	    "TOP_LABEL_ANGLE                         " "HORIZONTAL_LABEL_ANG"
	    "LE                  " "LEFT_LABEL_ANGLE                        " 
	    "RIGHT_LABEL_ANGLE                       " "VERTICAL_LABEL_ANGLE"
	    "                    " "BOTTOM_TITLE_ANGLE                      " 
	    "TOP_TITLE_ANGLE                         " "HORIZONTAL_TITLE_ANG"
	    "LE                  " "LEFT_TITLE_ANGLE                        " 
	    "RIGHT_TITLE_ANGLE                       " "VERTICAL_TITLE_ANGLE"
	    "                    " "BOTTOM_LABEL_CENTERING                  " 
	    "TOP_LABEL_CENTERING                     " "HORIZONTAL_LABEL_CEN"
	    "TERING              " "LEFT_LABEL_CENTERING                    " 
	    "RIGHT_LABEL_CENTERING                   " "VERTICAL_LABEL_CENTE"
	    "RING                " "AXIS_LINE_INDEX0                        " 
	    "AXIS_LINE_INDEX1                        " "AXIS_LINE_INDEX2    "
	    "                    " "LABEL_INDEX0                            " 
	    "LABEL_INDEX1                            " "LABEL_INDEX2        "
	    "                    " "LABEL_SIDE_FOR_USER_AXIS                " 
	    "TICKMARK_SIDE                           " "----IUNDEF          "
	    "                    ";
    static logical lfirst = TRUE_;

    /* System generated locals */
    address a__1[3];
    integer i__1[3];

    /* Builtin functions */
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen),
	     s_copy(char *, char *, ftnlen, ftnlen);
    integer s_wsue(cilist *), do_uio(integer *, char *, ftnlen), e_wsue(void),
	     s_rsue(cilist *), e_rsue(void);

    /* Local variables */
    static integer n, ios;
    extern integer lenc_(char *, ftnlen);
    static char cmsg[80];
    extern logical lchreq_(char *, char *, ftnlen, ftnlen);
    extern /* Subroutine */ int rliget_(char *, integer *, integer *, ftnlen),
	     msgdmp_(char *, char *, char *, ftnlen, ftnlen, ftnlen), rtiget_(
	    char *, char *, integer *, integer *, ftnlen, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___8 = { 1, 0, 0, 0, 0 };
    static cilist io___9 = { 1, 0, 1, 0, 0 };


/*     / SHORT NAME / */
    switch(n__) {
	case 1: goto L_uziqid;
	case 2: goto L_uziqcp;
	case 3: goto L_uziqcl;
	case 4: goto L_uziqvl;
	case 5: goto L_uzisvl;
	case 6: goto L_uziqin;
	case 7: goto L_uzisav;
	case 8: goto L_uzirst;
	}

/*     / LONG NAME / */
    *ncp = 27;
    return 0;
/* ----------------------------------------------------------------------- */

L_uziqid:
    for (n = 1; n <= 27; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *idx = n;
	    return 0;
	}
/* L10: */
    }
/* Writing concatenation */
    i__1[0] = 11, a__1[0] = "PARAMETER '";
    i__1[1] = lenc_(cp, cp_len), a__1[1] = cp;
    i__1[2] = 17, a__1[2] = "' IS NOT DEFINED.";
    s_cat(cmsg, a__1, i__1, &c__3, (ftnlen)80);
    msgdmp_("E", "UZIQID", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
    return 0;
/* ----------------------------------------------------------------------- */

L_uziqcp:
    if (1 <= *idx && *idx <= 27) {
	s_copy(cp, cparas + (*idx - 1 << 3), cp_len, (ftnlen)8);
    } else {
	msgdmp_("E", "UZIQCP", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_uziqcl:
    if (1 <= *idx && *idx <= 27) {
	s_copy(cp, cparal + (*idx - 1) * 40, cp_len, (ftnlen)40);
    } else {
	msgdmp_("E", "UZIQCL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_uziqvl:
    if (lfirst) {
	rtiget_("UZ", cparas, ix, &c__27, (ftnlen)2, (ftnlen)8);
	rliget_(cparal, ix, &c__27, (ftnlen)40);
	lfirst = FALSE_;
    }
    if (1 <= *idx && *idx <= 27) {
	*ipara = ix[*idx - 1];
    } else {
	msgdmp_("E", "UZIQVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_uzisvl:
    if (lfirst) {
	rtiget_("UZ", cparas, ix, &c__27, (ftnlen)2, (ftnlen)8);
	rliget_(cparal, ix, &c__27, (ftnlen)40);
	lfirst = FALSE_;
    }
    if (1 <= *idx && *idx <= 27) {
	ix[*idx - 1] = *ipara;
    } else {
	msgdmp_("E", "UZISVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_uziqin:
    for (n = 1; n <= 27; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *in = n;
	    return 0;
	}
/* L20: */
    }
    *in = 0;
    return 0;
/* ----------------------------------------------------------------------- */

L_uzisav:
    io___8.ciunit = *iu;
    ios = s_wsue(&io___8);
    if (ios != 0) {
	goto L100001;
    }
    ios = do_uio(&c__27, (char *)&ix[0], (ftnlen)sizeof(integer));
    if (ios != 0) {
	goto L100001;
    }
    ios = e_wsue();
L100001:
    if (ios != 0) {
	msgdmp_("E", "UZISAV", "IOSTAT IS NOT ZERO.", (ftnlen)1, (ftnlen)6, (
		ftnlen)19);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_uzirst:
    io___9.ciunit = *iu;
    ios = s_rsue(&io___9);
    if (ios != 0) {
	goto L100002;
    }
    ios = do_uio(&c__27, (char *)&ix[0], (ftnlen)sizeof(integer));
    if (ios != 0) {
	goto L100002;
    }
    ios = e_rsue();
L100002:
    if (ios != 0) {
	msgdmp_("E", "UZIRST", "IOSTAT IS NOT ZERO.", (ftnlen)1, (ftnlen)6, (
		ftnlen)19);
    }
    return 0;
} /* uziqnp_ */

/* Subroutine */ int uziqnp_(integer *ncp)
{
    return uziqnp_0_(0, ncp, (char *)0, (integer *)0, (integer *)0, (integer *
	    )0, (integer *)0, (ftnint)0);
    }

/* Subroutine */ int uziqid_(char *cp, integer *idx, ftnlen cp_len)
{
    return uziqnp_0_(1, (integer *)0, cp, idx, (integer *)0, (integer *)0, (
	    integer *)0, cp_len);
    }

/* Subroutine */ int uziqcp_(integer *idx, char *cp, ftnlen cp_len)
{
    return uziqnp_0_(2, (integer *)0, cp, idx, (integer *)0, (integer *)0, (
	    integer *)0, cp_len);
    }

/* Subroutine */ int uziqcl_(integer *idx, char *cp, ftnlen cp_len)
{
    return uziqnp_0_(3, (integer *)0, cp, idx, (integer *)0, (integer *)0, (
	    integer *)0, cp_len);
    }

/* Subroutine */ int uziqvl_(integer *idx, integer *ipara)
{
    return uziqnp_0_(4, (integer *)0, (char *)0, idx, ipara, (integer *)0, (
	    integer *)0, (ftnint)0);
    }

/* Subroutine */ int uzisvl_(integer *idx, integer *ipara)
{
    return uziqnp_0_(5, (integer *)0, (char *)0, idx, ipara, (integer *)0, (
	    integer *)0, (ftnint)0);
    }

/* Subroutine */ int uziqin_(char *cp, integer *in, ftnlen cp_len)
{
    return uziqnp_0_(6, (integer *)0, cp, (integer *)0, (integer *)0, in, (
	    integer *)0, cp_len);
    }

/* Subroutine */ int uzisav_(integer *iu)
{
    return uziqnp_0_(7, (integer *)0, (char *)0, (integer *)0, (integer *)0, (
	    integer *)0, iu, (ftnint)0);
    }

/* Subroutine */ int uzirst_(integer *iu)
{
    return uziqnp_0_(8, (integer *)0, (char *)0, (integer *)0, (integer *)0, (
	    integer *)0, iu, (ftnint)0);
    }


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__3 = 3;
static integer c__2 = 2;

/* ----------------------------------------------------------------------- */
/*     UZCQNP */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uzcqnp_0_(int n__, integer *ncp, char *cp, integer *idx, 
	char *cval, integer *in, integer *iu, ftnlen cp_len, ftnlen cval_len)
{
    /* Initialized data */

    static char cparas[8*2] = "CXFMT   " "CYFMT   ";
    static char cx[80*2] = "B                                               "
	    "                                " "B                            "
	    "                                                   ";
    static char cparal[40*2] = "****CXFMT                               " 
	    "****CYFMT                               ";
    static logical lfirst = TRUE_;

    /* System generated locals */
    address a__1[3];
    integer i__1[3];

    /* Builtin functions */
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen),
	     s_copy(char *, char *, ftnlen, ftnlen);
    integer s_wsue(cilist *), do_uio(integer *, char *, ftnlen), e_wsue(void),
	     s_rsue(cilist *), e_rsue(void);

    /* Local variables */
    static integer n, ios;
    extern integer lenc_(char *, ftnlen);
    static char cmsg[80];
    extern logical lchreq_(char *, char *, ftnlen, ftnlen);
    extern /* Subroutine */ int rlcget_(char *, char *, integer *, ftnlen, 
	    ftnlen), msgdmp_(char *, char *, char *, ftnlen, ftnlen, ftnlen), 
	    rtcget_(char *, char *, char *, integer *, ftnlen, ftnlen, ftnlen)
	    ;

    /* Fortran I/O blocks */
    static cilist io___8 = { 1, 0, 0, 0, 0 };
    static cilist io___9 = { 1, 0, 1, 0, 0 };


/*     / SHORT NAME / */
    switch(n__) {
	case 1: goto L_uzcqid;
	case 2: goto L_uzcqcp;
	case 3: goto L_uzcqcl;
	case 4: goto L_uzcqvl;
	case 5: goto L_uzcsvl;
	case 6: goto L_uzcqin;
	case 7: goto L_uzcsav;
	case 8: goto L_uzcrst;
	}

/*     / LONG NAME / */
    *ncp = 2;
    return 0;
/* ----------------------------------------------------------------------- */

L_uzcqid:
    for (n = 1; n <= 2; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *idx = n;
	    return 0;
	}
/* L10: */
    }
/* Writing concatenation */
    i__1[0] = 11, a__1[0] = "PARAMETER \"";
    i__1[1] = lenc_(cp, cp_len), a__1[1] = cp;
    i__1[2] = 17, a__1[2] = "\" IS NOT DEFINED.";
    s_cat(cmsg, a__1, i__1, &c__3, (ftnlen)80);
    msgdmp_("E", "UZCQID", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
    return 0;
/* ----------------------------------------------------------------------- */

L_uzcqcp:
    if (1 <= *idx && *idx <= 2) {
	s_copy(cp, cparas + (*idx - 1 << 3), cp_len, (ftnlen)8);
    } else {
	msgdmp_("E", "UZCQCP", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_uzcqcl:
    if (1 <= *idx && *idx <= 2) {
	s_copy(cp, cparal + (*idx - 1) * 40, cp_len, (ftnlen)40);
    } else {
	msgdmp_("E", "UZCQCL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_uzcqvl:
    if (lfirst) {
	rtcget_("UZ", cparas, cx, &c__2, (ftnlen)2, (ftnlen)8, (ftnlen)80);
	rlcget_(cparal, cx, &c__2, (ftnlen)40, (ftnlen)80);
	lfirst = FALSE_;
    }
    if (1 <= *idx && *idx <= 2) {
	s_copy(cval, cx + (*idx - 1) * 80, cval_len, (ftnlen)80);
    } else {
	msgdmp_("E", "UZCQVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_uzcsvl:
    if (lfirst) {
	rtcget_("UZ", cparas, cx, &c__2, (ftnlen)2, (ftnlen)8, (ftnlen)80);
	rlcget_(cparal, cx, &c__2, (ftnlen)40, (ftnlen)80);
	lfirst = FALSE_;
    }
    if (1 <= *idx && *idx <= 2) {
	s_copy(cx + (*idx - 1) * 80, cval, (ftnlen)80, cval_len);
    } else {
	msgdmp_("E", "UZCSVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_uzcqin:
    for (n = 1; n <= 2; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *in = n;
	    return 0;
	}
/* L20: */
    }
    *in = 0;
    return 0;
/* ----------------------------------------------------------------------- */

L_uzcsav:
    io___8.ciunit = *iu;
    ios = s_wsue(&io___8);
    if (ios != 0) {
	goto L100001;
    }
    ios = do_uio(&c__2, cx, (ftnlen)80);
    if (ios != 0) {
	goto L100001;
    }
    ios = e_wsue();
L100001:
    if (ios != 0) {
	msgdmp_("E", "UZCSAV", "IOSTAT IS NOT ZERO.", (ftnlen)1, (ftnlen)6, (
		ftnlen)19);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_uzcrst:
    io___9.ciunit = *iu;
    ios = s_rsue(&io___9);
    if (ios != 0) {
	goto L100002;
    }
    ios = do_uio(&c__2, cx, (ftnlen)80);
    if (ios != 0) {
	goto L100002;
    }
    ios = e_rsue();
L100002:
    if (ios != 0) {
	msgdmp_("E", "UZCRST", "IOSTAT IS NOT ZERO.", (ftnlen)1, (ftnlen)6, (
		ftnlen)19);
    }
    return 0;
} /* uzcqnp_ */

/* Subroutine */ int uzcqnp_(integer *ncp)
{
    return uzcqnp_0_(0, ncp, (char *)0, (integer *)0, (char *)0, (integer *)0,
	     (integer *)0, (ftnint)0, (ftnint)0);
    }

/* Subroutine */ int uzcqid_(char *cp, integer *idx, ftnlen cp_len)
{
    return uzcqnp_0_(1, (integer *)0, cp, idx, (char *)0, (integer *)0, (
	    integer *)0, cp_len, (ftnint)0);
    }

/* Subroutine */ int uzcqcp_(integer *idx, char *cp, ftnlen cp_len)
{
    return uzcqnp_0_(2, (integer *)0, cp, idx, (char *)0, (integer *)0, (
	    integer *)0, cp_len, (ftnint)0);
    }

/* Subroutine */ int uzcqcl_(integer *idx, char *cp, ftnlen cp_len)
{
    return uzcqnp_0_(3, (integer *)0, cp, idx, (char *)0, (integer *)0, (
	    integer *)0, cp_len, (ftnint)0);
    }

/* Subroutine */ int uzcqvl_(integer *idx, char *cval, ftnlen cval_len)
{
    return uzcqnp_0_(4, (integer *)0, (char *)0, idx, cval, (integer *)0, (
	    integer *)0, (ftnint)0, cval_len);
    }

/* Subroutine */ int uzcsvl_(integer *idx, char *cval, ftnlen cval_len)
{
    return uzcqnp_0_(5, (integer *)0, (char *)0, idx, cval, (integer *)0, (
	    integer *)0, (ftnint)0, cval_len);
    }

/* Subroutine */ int uzcqin_(char *cp, integer *in, ftnlen cp_len)
{
    return uzcqnp_0_(6, (integer *)0, cp, (integer *)0, (char *)0, in, (
	    integer *)0, cp_len, (ftnint)0);
    }

/* Subroutine */ int uzcsav_(integer *iu)
{
    return uzcqnp_0_(7, (integer *)0, (char *)0, (integer *)0, (char *)0, (
	    integer *)0, iu, (ftnint)0, (ftnint)0);
    }

/* Subroutine */ int uzcrst_(integer *iu)
{
    return uzcqnp_0_(8, (integer *)0, (char *)0, (integer *)0, (char *)0, (
	    integer *)0, iu, (ftnint)0, (ftnint)0);
    }


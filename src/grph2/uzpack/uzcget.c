/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     UZCGET / UZCSET / UZCSTX */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uzcget_0_(int n__, char *cp, char *cpara, ftnlen cp_len, 
	ftnlen cpara_len)
{
    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static char cx[40];
    static integer idx;
    static char cpval[1024];
    extern /* Subroutine */ int rlcget_(char *, char *, integer *, ftnlen, 
	    ftnlen), rtcget_(char *, char *, char *, integer *, ftnlen, 
	    ftnlen, ftnlen), uzcqid_(char *, integer *, ftnlen), uzcqcl_(
	    integer *, char *, ftnlen), uzcqcp_(integer *, char *, ftnlen), 
	    uzcqvl_(integer *, char *, ftnlen), uzcsvl_(integer *, char *, 
	    ftnlen);

    switch(n__) {
	case 1: goto L_uzcset;
	case 2: goto L_uzcstx;
	}

    uzcqid_(cp, &idx, cp_len);
    uzcqvl_(&idx, cpara, cpara_len);
    return 0;
/* ----------------------------------------------------------------------- */

L_uzcset:
    uzcqid_(cp, &idx, cp_len);
    uzcsvl_(&idx, cpara, cpara_len);
    return 0;
/* ----------------------------------------------------------------------- */

L_uzcstx:
    s_copy(cpval, cpara, (ftnlen)1024, cpara_len);
    uzcqid_(cp, &idx, cp_len);
    uzcqcp_(&idx, cx, (ftnlen)40);
    rtcget_("UZ", cx, cpval, &c__1, (ftnlen)2, (ftnlen)40, (ftnlen)1024);
    uzcqcl_(&idx, cx, (ftnlen)40);
    rlcget_(cx, cpval, &c__1, (ftnlen)40, (ftnlen)1024);
    uzcsvl_(&idx, cpval, (ftnlen)1024);
    return 0;
} /* uzcget_ */

/* Subroutine */ int uzcget_(char *cp, char *cpara, ftnlen cp_len, ftnlen 
	cpara_len)
{
    return uzcget_0_(0, cp, cpara, cp_len, cpara_len);
    }

/* Subroutine */ int uzcset_(char *cp, char *cpara, ftnlen cp_len, ftnlen 
	cpara_len)
{
    return uzcget_0_(1, cp, cpara, cp_len, cpara_len);
    }

/* Subroutine */ int uzcstx_(char *cp, char *cpara, ftnlen cp_len, ftnlen 
	cpara_len)
{
    return uzcget_0_(2, cp, cpara, cp_len, cpara_len);
    }


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     UZPGET / UZPSET / UZPSTX */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uzpget_0_(int n__, char *cp, integer *ipara, ftnlen 
	cp_len)
{
    static char cl[40];
    static integer ip;
    static char cx[8];
    static integer it, idx;
    extern /* Subroutine */ int rliget_(char *, integer *, integer *, ftnlen),
	     rllget_(char *, integer *, integer *, ftnlen), rtiget_(char *, 
	    char *, integer *, integer *, ftnlen, ftnlen), rlrget_(char *, 
	    integer *, integer *, ftnlen), rtlget_(char *, char *, integer *, 
	    integer *, ftnlen, ftnlen), uziqid_(char *, integer *, ftnlen), 
	    rtrget_(char *, char *, integer *, integer *, ftnlen, ftnlen), 
	    uzlqid_(char *, integer *, ftnlen), uzpqid_(char *, integer *, 
	    ftnlen), uzpqcl_(integer *, char *, ftnlen), uzrqid_(char *, 
	    integer *, ftnlen), uzpqcp_(integer *, char *, ftnlen), uzpqit_(
	    integer *, integer *), uzisvl_(integer *, integer *), uzlsvl_(
	    integer *, integer *), uzpqvl_(integer *, integer *), uzpsvl_(
	    integer *, integer *), uzrsvl_(integer *, integer *);

    switch(n__) {
	case 1: goto L_uzpset;
	case 2: goto L_uzpstx;
	}

    uzpqid_(cp, &idx, cp_len);
    uzpqvl_(&idx, ipara);
    return 0;
/* ----------------------------------------------------------------------- */

L_uzpset:
    uzpqid_(cp, &idx, cp_len);
    uzpsvl_(&idx, ipara);
    return 0;
/* ----------------------------------------------------------------------- */

L_uzpstx:
    ip = *ipara;
    uzpqid_(cp, &idx, cp_len);
    uzpqit_(&idx, &it);
    uzpqcp_(&idx, cx, (ftnlen)8);
    uzpqcl_(&idx, cl, (ftnlen)40);
    if (it == 1) {
	rtiget_("UZ", cx, &ip, &c__1, (ftnlen)2, (ftnlen)8);
	rliget_(cl, &ip, &c__1, (ftnlen)40);
	uziqid_(cp, &idx, cp_len);
	uzisvl_(&idx, &ip);
    } else if (it == 2) {
	rtlget_("UZ", cx, &ip, &c__1, (ftnlen)2, (ftnlen)8);
	rllget_(cl, &ip, &c__1, (ftnlen)40);
	uzlqid_(cp, &idx, cp_len);
	uzlsvl_(&idx, &ip);
    } else if (it == 3) {
	rtrget_("UZ", cx, &ip, &c__1, (ftnlen)2, (ftnlen)8);
	rlrget_(cl, &ip, &c__1, (ftnlen)40);
	uzrqid_(cp, &idx, cp_len);
	uzrsvl_(&idx, &ip);
    }
    return 0;
} /* uzpget_ */

/* Subroutine */ int uzpget_(char *cp, integer *ipara, ftnlen cp_len)
{
    return uzpget_0_(0, cp, ipara, cp_len);
    }

/* Subroutine */ int uzpset_(char *cp, integer *ipara, ftnlen cp_len)
{
    return uzpget_0_(1, cp, ipara, cp_len);
    }

/* Subroutine */ int uzpstx_(char *cp, integer *ipara, ftnlen cp_len)
{
    return uzpget_0_(2, cp, ipara, cp_len);
    }


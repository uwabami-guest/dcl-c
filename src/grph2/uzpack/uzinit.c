/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static real c_b3 = 0.f;

/* ----------------------------------------------------------------------- */
/*     UZINIT */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uzinit_(void)
{
    extern /* Subroutine */ int uxsaxz_(char *, real *, ftnlen), uysaxz_(char 
	    *, real *, ftnlen);

    uxsaxz_("B", &c_b3, (ftnlen)1);
    uxsaxz_("T", &c_b3, (ftnlen)1);
    uxsaxz_("U", &c_b3, (ftnlen)1);
    uysaxz_("L", &c_b3, (ftnlen)1);
    uysaxz_("R", &c_b3, (ftnlen)1);
    uysaxz_("U", &c_b3, (ftnlen)1);
    return 0;
} /* uzinit_ */


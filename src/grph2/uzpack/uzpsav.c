/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     UZPSAV / UZPRST */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uzpsav_0_(int n__)
{
    /* System generated locals */
    olist o__1;
    alist al__1;

    /* Builtin functions */
    integer f_open(olist *), f_rew(alist *);

    /* Local variables */
    static integer iu;
    extern integer iufopn_(void);
    extern /* Subroutine */ int uzcsav_(integer *), uzisav_(integer *), 
	    uzlsav_(integer *), uzrsav_(integer *), uzcrst_(integer *), 
	    uzirst_(integer *), uzlrst_(integer *), uzrrst_(integer *);

    switch(n__) {
	case 1: goto L_uzprst;
	}

    iu = iufopn_();
    o__1.oerr = 0;
    o__1.ounit = iu;
    o__1.ofnm = 0;
    o__1.orl = 0;
    o__1.osta = "SCRATCH";
    o__1.oacc = 0;
    o__1.ofm = "UNFORMATTED";
    o__1.oblnk = 0;
    f_open(&o__1);
    al__1.aerr = 0;
    al__1.aunit = iu;
    f_rew(&al__1);
    uzcsav_(&iu);
    uzisav_(&iu);
    uzlsav_(&iu);
    uzrsav_(&iu);
    al__1.aerr = 0;
    al__1.aunit = iu;
    f_rew(&al__1);
    return 0;
/* ----------------------------------------------------------------------- */

L_uzprst:
    uzcrst_(&iu);
    uzirst_(&iu);
    uzlrst_(&iu);
    uzrrst_(&iu);
    al__1.aerr = 0;
    al__1.aunit = iu;
    f_rew(&al__1);
    return 0;
} /* uzpsav_ */

/* Subroutine */ int uzpsav_(void)
{
    return uzpsav_0_(0);
    }

/* Subroutine */ int uzprst_(void)
{
    return uzpsav_0_(1);
    }


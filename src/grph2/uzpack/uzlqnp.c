/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__3 = 3;
static integer c__10 = 10;

/* ----------------------------------------------------------------------- */
/*     UZLQNP / UZLQID / UZLQCP / UZLQVL / UZLSVL */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uzlqnp_0_(int n__, integer *ncp, char *cp, integer *idx, 
	logical *lpara, integer *in, integer *iu, ftnlen cp_len)
{
    /* Initialized data */

    static char cparas[8*10] = "LABELXB " "LABELXT " "LABELXU " "LABELYL " 
	    "LABELYR " "LABELYU " "LOFFSET " "LBTWN   " "LBOUND  " "LBMSG   ";
    static logical lx[10] = { TRUE_,FALSE_,TRUE_,TRUE_,FALSE_,TRUE_,FALSE_,
	    FALSE_,FALSE_,TRUE_ };
    static char cparal[40*10] = "DRAW_BOTTOM_LABEL                       " 
	    "DRAW_TOP_LABEL                          " "DRAW_HORIZONTAL_LABE"
	    "L                   " "DRAW_LEFT_LABEL                         " 
	    "DRAW_RIGHT_LABEL                        " "DRAW_VERTICAL_LABEL "
	    "                    " "ENABLE_LINEAR_OFFSET                    " 
	    "ENABLE_SPAN_LABELING                    " "TITLE_OVER_VIEWPORT "
	    "                    " "TITLE_OVER_VIEWPORT_MESSAGE             ";
    static logical lfirst = TRUE_;

    /* System generated locals */
    address a__1[3];
    integer i__1[3];

    /* Builtin functions */
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen),
	     s_copy(char *, char *, ftnlen, ftnlen);
    integer s_wsue(cilist *), do_uio(integer *, char *, ftnlen), e_wsue(void),
	     s_rsue(cilist *), e_rsue(void);

    /* Local variables */
    static integer n, ios;
    extern integer lenc_(char *, ftnlen);
    static char cmsg[80];
    extern logical lchreq_(char *, char *, ftnlen, ftnlen);
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), rllget_(char *, logical *, integer *, ftnlen), 
	    rtlget_(char *, char *, logical *, integer *, ftnlen, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___8 = { 1, 0, 0, 0, 0 };
    static cilist io___9 = { 1, 0, 1, 0, 0 };


/*     / SHORT NAME / */
    switch(n__) {
	case 1: goto L_uzlqid;
	case 2: goto L_uzlqcp;
	case 3: goto L_uzlqcl;
	case 4: goto L_uzlqvl;
	case 5: goto L_uzlsvl;
	case 6: goto L_uzlqin;
	case 7: goto L_uzlsav;
	case 8: goto L_uzlrst;
	}

/*     / LONG NAME / */
    *ncp = 10;
    return 0;
/* ----------------------------------------------------------------------- */

L_uzlqid:
    for (n = 1; n <= 10; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *idx = n;
	    return 0;
	}
/* L10: */
    }
/* Writing concatenation */
    i__1[0] = 11, a__1[0] = "PARAMETER '";
    i__1[1] = lenc_(cp, cp_len), a__1[1] = cp;
    i__1[2] = 17, a__1[2] = "' IS NOT DEFINED.";
    s_cat(cmsg, a__1, i__1, &c__3, (ftnlen)80);
    msgdmp_("E", "UZLQID", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
    return 0;
/* ----------------------------------------------------------------------- */

L_uzlqcp:
    if (1 <= *idx && *idx <= 10) {
	s_copy(cp, cparas + (*idx - 1 << 3), cp_len, (ftnlen)8);
    } else {
	msgdmp_("E", "UZLQCP", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_uzlqcl:
    if (1 <= *idx && *idx <= 10) {
	s_copy(cp, cparal + (*idx - 1) * 40, cp_len, (ftnlen)40);
    } else {
	msgdmp_("E", "UZLQCL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_uzlqvl:
    if (lfirst) {
	rtlget_("UZ", cparas, lx, &c__10, (ftnlen)2, (ftnlen)8);
	rllget_(cparal, lx, &c__10, (ftnlen)40);
	lfirst = FALSE_;
    }
    if (1 <= *idx && *idx <= 10) {
	*lpara = lx[*idx - 1];
    } else {
	msgdmp_("E", "UZLQVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_uzlsvl:
    if (lfirst) {
	rtlget_("UZ", cparas, lx, &c__10, (ftnlen)2, (ftnlen)8);
	rllget_(cparal, lx, &c__10, (ftnlen)40);
	lfirst = FALSE_;
    }
    if (1 <= *idx && *idx <= 10) {
	lx[*idx - 1] = *lpara;
    } else {
	msgdmp_("E", "UZLSVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_uzlqin:
    for (n = 1; n <= 10; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *in = n;
	    return 0;
	}
/* L20: */
    }
    *in = 0;
    return 0;
/* ----------------------------------------------------------------------- */

L_uzlsav:
    io___8.ciunit = *iu;
    ios = s_wsue(&io___8);
    if (ios != 0) {
	goto L100001;
    }
    ios = do_uio(&c__10, (char *)&lx[0], (ftnlen)sizeof(logical));
    if (ios != 0) {
	goto L100001;
    }
    ios = e_wsue();
L100001:
    if (ios != 0) {
	msgdmp_("E", "UZLSAV", "IOSTAT IS NOT ZERO.", (ftnlen)1, (ftnlen)6, (
		ftnlen)19);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_uzlrst:
    io___9.ciunit = *iu;
    ios = s_rsue(&io___9);
    if (ios != 0) {
	goto L100002;
    }
    ios = do_uio(&c__10, (char *)&lx[0], (ftnlen)sizeof(logical));
    if (ios != 0) {
	goto L100002;
    }
    ios = e_rsue();
L100002:
    if (ios != 0) {
	msgdmp_("E", "UZLRST", "IOSTAT IS NOT ZERO.", (ftnlen)1, (ftnlen)6, (
		ftnlen)19);
    }
    return 0;
} /* uzlqnp_ */

/* Subroutine */ int uzlqnp_(integer *ncp)
{
    return uzlqnp_0_(0, ncp, (char *)0, (integer *)0, (logical *)0, (integer *
	    )0, (integer *)0, (ftnint)0);
    }

/* Subroutine */ int uzlqid_(char *cp, integer *idx, ftnlen cp_len)
{
    return uzlqnp_0_(1, (integer *)0, cp, idx, (logical *)0, (integer *)0, (
	    integer *)0, cp_len);
    }

/* Subroutine */ int uzlqcp_(integer *idx, char *cp, ftnlen cp_len)
{
    return uzlqnp_0_(2, (integer *)0, cp, idx, (logical *)0, (integer *)0, (
	    integer *)0, cp_len);
    }

/* Subroutine */ int uzlqcl_(integer *idx, char *cp, ftnlen cp_len)
{
    return uzlqnp_0_(3, (integer *)0, cp, idx, (logical *)0, (integer *)0, (
	    integer *)0, cp_len);
    }

/* Subroutine */ int uzlqvl_(integer *idx, logical *lpara)
{
    return uzlqnp_0_(4, (integer *)0, (char *)0, idx, lpara, (integer *)0, (
	    integer *)0, (ftnint)0);
    }

/* Subroutine */ int uzlsvl_(integer *idx, logical *lpara)
{
    return uzlqnp_0_(5, (integer *)0, (char *)0, idx, lpara, (integer *)0, (
	    integer *)0, (ftnint)0);
    }

/* Subroutine */ int uzlqin_(char *cp, integer *in, ftnlen cp_len)
{
    return uzlqnp_0_(6, (integer *)0, cp, (integer *)0, (logical *)0, in, (
	    integer *)0, cp_len);
    }

/* Subroutine */ int uzlsav_(integer *iu)
{
    return uzlqnp_0_(7, (integer *)0, (char *)0, (integer *)0, (logical *)0, (
	    integer *)0, iu, (ftnint)0);
    }

/* Subroutine */ int uzlrst_(integer *iu)
{
    return uzlqnp_0_(8, (integer *)0, (char *)0, (integer *)0, (logical *)0, (
	    integer *)0, iu, (ftnint)0);
    }


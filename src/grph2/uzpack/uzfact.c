/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     UZFACT */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uzfact_(real *rfact)
{
    /* System generated locals */
    real r__1;

    /* Local variables */
    static real rsizc1, rsizc2, rsizl1, rsizl2, rsizt1, rsizt2;
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), uzrget_(char *, real *, ftnlen), uzrset_(char *, 
	    real *, ftnlen);

    if (*rfact <= 0.f) {
	msgdmp_("E", "UZFACT", "FACTOR IS LESS THAN ZERO.", (ftnlen)1, (
		ftnlen)6, (ftnlen)25);
    }
    uzrget_("RSIZET1", &rsizt1, (ftnlen)7);
    uzrget_("RSIZET2", &rsizt2, (ftnlen)7);
    uzrget_("RSIZEL1", &rsizl1, (ftnlen)7);
    uzrget_("RSIZEL2", &rsizl2, (ftnlen)7);
    uzrget_("RSIZEC1", &rsizc1, (ftnlen)7);
    uzrget_("RSIZEC2", &rsizc2, (ftnlen)7);
    r__1 = rsizt1 * *rfact;
    uzrset_("RSIZET1", &r__1, (ftnlen)7);
    r__1 = rsizt2 * *rfact;
    uzrset_("RSIZET2", &r__1, (ftnlen)7);
    r__1 = rsizl1 * *rfact;
    uzrset_("RSIZEL1", &r__1, (ftnlen)7);
    r__1 = rsizl2 * *rfact;
    uzrset_("RSIZEL2", &r__1, (ftnlen)7);
    r__1 = rsizc1 * *rfact;
    uzrset_("RSIZEC1", &r__1, (ftnlen)7);
    r__1 = rsizc2 * *rfact;
    uzrset_("RSIZEC2", &r__1, (ftnlen)7);
    return 0;
} /* uzfact_ */


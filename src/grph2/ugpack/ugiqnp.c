/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__3 = 3;
static integer c__10 = 10;

/* ----------------------------------------------------------------------- */
/*     UGIQNP / UGIQID / UGIQCP / UGIQVL / UGISVL */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int ugiqnp_0_(int n__, integer *ncp, char *cp, integer *idx, 
	integer *ipara, integer *in, ftnlen cp_len)
{
    /* Initialized data */

    static char cparas[8*10] = "INDEX   " "ICENT   " "ITYPE1  " "ITYPE2  " 
	    "IXINT   " "IYINT   " "IUNTTL  " "IUINDX  " "IUTXRO  " "IUTYRO  ";
    static integer ix[10] = { 3,0,5,1,1,1,0,3,0,90 };
    static char cparal[40*10] = "VECTOR_INDEX                            " 
	    "VECTOR_GRID_POSITION                    " "MISS_VECTOR_MARKER_T"
	    "YPE                 " "SMALL_VECTOR_MARKER_TYPE                " 
	    "****IXINT                               " "****IYINT           "
	    "                    " "****IUNTTL                              " 
	    "UNIT_VECTOR_TITLE_INDEX                 " "UNIT_VECTOR_X_TITLE_"
	    "ANGLE               " "UNIT_VECTOR_Y_TITLE_ANGLE               ";
    static logical lfirst = TRUE_;

    /* System generated locals */
    address a__1[3];
    integer i__1[3];

    /* Builtin functions */
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen),
	     s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer n;
    extern integer lenc_(char *, ftnlen);
    static char cmsg[80];
    extern logical lchreq_(char *, char *, ftnlen, ftnlen);
    extern /* Subroutine */ int rliget_(char *, integer *, integer *, ftnlen),
	     msgdmp_(char *, char *, char *, ftnlen, ftnlen, ftnlen), rtiget_(
	    char *, char *, integer *, integer *, ftnlen, ftnlen);

/*     / SHORT NAME / */
    switch(n__) {
	case 1: goto L_ugiqid;
	case 2: goto L_ugiqcp;
	case 3: goto L_ugiqcl;
	case 4: goto L_ugiqvl;
	case 5: goto L_ugisvl;
	case 6: goto L_ugiqin;
	}

/*     / LONG NAME / */
    *ncp = 10;
    return 0;
/* ----------------------------------------------------------------------- */

L_ugiqid:
    for (n = 1; n <= 10; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *idx = n;
	    return 0;
	}
/* L10: */
    }
/* Writing concatenation */
    i__1[0] = 11, a__1[0] = "PARAMETER '";
    i__1[1] = lenc_(cp, cp_len), a__1[1] = cp;
    i__1[2] = 17, a__1[2] = "' IS NOT DEFINED.";
    s_cat(cmsg, a__1, i__1, &c__3, (ftnlen)80);
    msgdmp_("E", "UGIQID", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
    return 0;
/* ----------------------------------------------------------------------- */

L_ugiqcp:
    if (1 <= *idx && *idx <= 10) {
	s_copy(cp, cparas + (*idx - 1 << 3), cp_len, (ftnlen)8);
    } else {
	msgdmp_("E", "UGIQCP", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_ugiqcl:
    if (1 <= *idx && *idx <= 10) {
	s_copy(cp, cparal + (*idx - 1) * 40, cp_len, (ftnlen)40);
    } else {
	msgdmp_("E", "UGIQCL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_ugiqvl:
    if (lfirst) {
	rtiget_("UG", cparas, ix, &c__10, (ftnlen)2, (ftnlen)8);
	rliget_(cparal, ix, &c__10, (ftnlen)40);
	lfirst = FALSE_;
    }
    if (1 <= *idx && *idx <= 10) {
	*ipara = ix[*idx - 1];
    } else {
	msgdmp_("E", "UGIQVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_ugisvl:
    if (lfirst) {
	rtiget_("UG", cparas, ix, &c__10, (ftnlen)2, (ftnlen)8);
	rliget_(cparal, ix, &c__10, (ftnlen)40);
	lfirst = FALSE_;
    }
    if (1 <= *idx && *idx <= 10) {
	ix[*idx - 1] = *ipara;
    } else {
	msgdmp_("E", "UGISVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_ugiqin:
    for (n = 1; n <= 10; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *in = n;
	    return 0;
	}
/* L20: */
    }
    *in = 0;
    return 0;
} /* ugiqnp_ */

/* Subroutine */ int ugiqnp_(integer *ncp)
{
    return ugiqnp_0_(0, ncp, (char *)0, (integer *)0, (integer *)0, (integer *
	    )0, (ftnint)0);
    }

/* Subroutine */ int ugiqid_(char *cp, integer *idx, ftnlen cp_len)
{
    return ugiqnp_0_(1, (integer *)0, cp, idx, (integer *)0, (integer *)0, 
	    cp_len);
    }

/* Subroutine */ int ugiqcp_(integer *idx, char *cp, ftnlen cp_len)
{
    return ugiqnp_0_(2, (integer *)0, cp, idx, (integer *)0, (integer *)0, 
	    cp_len);
    }

/* Subroutine */ int ugiqcl_(integer *idx, char *cp, ftnlen cp_len)
{
    return ugiqnp_0_(3, (integer *)0, cp, idx, (integer *)0, (integer *)0, 
	    cp_len);
    }

/* Subroutine */ int ugiqvl_(integer *idx, integer *ipara)
{
    return ugiqnp_0_(4, (integer *)0, (char *)0, idx, ipara, (integer *)0, (
	    ftnint)0);
    }

/* Subroutine */ int ugisvl_(integer *idx, integer *ipara)
{
    return ugiqnp_0_(5, (integer *)0, (char *)0, idx, ipara, (integer *)0, (
	    ftnint)0);
    }

/* Subroutine */ int ugiqin_(char *cp, integer *in, ftnlen cp_len)
{
    return ugiqnp_0_(6, (integer *)0, cp, (integer *)0, (integer *)0, in, 
	    cp_len);
    }


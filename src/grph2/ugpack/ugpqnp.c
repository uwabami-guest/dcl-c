/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__3 = 3;

/* ----------------------------------------------------------------------- */
/*     UGPQNP / UGPQID / UGPQCP / UGPQVL / UGPSVL */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int ugpqnp_0_(int n__, integer *ncp, char *cp, integer *idx, 
	integer *itp, integer *ipara, integer *in, ftnlen cp_len)
{
    /* Initialized data */

    static char cparas[8*37] = "INDEX   " "LNRMAL  " "LEQRAT  " "XFACT1  " 
	    "YFACT1  " "XFACT2  " "YFACT2  " "LMSG    " "ICENT   " "LMISSP  " 
	    "ITYPE1  " "LSMALL  " "RSMALL  " "ITYPE2  " "RSIZEM  " "RSIZET  " 
	    "XTTL    " "IXINT   " "IYINT   " "LUNIT   " "LUMSG   " "VXUOFF  " 
	    "VYUOFF  " "VXULOC  " "VYULOC  " "UXUNIT  " "UYUNIT  " "VXUNIT  " 
	    "VYUNIT  " "IUNTTL  " "RSIZEUT " "IUINDX  " "IUTXRO  " "IUTYRO  " 
	    "VUTOFF  " "RHFACT  " "RUNDEF  ";
    static integer itype[37] = { 1,2,2,3,3,3,3,2,1,2,1,2,3,1,3,3,3,1,1,2,2,3,
	    3,3,3,3,3,3,3,1,3,1,1,1,3,3,3 };
    static char cparal[40*37] = "VECTOR_INDEX                            " 
	    "ENABLE_VECTOR_NORMALIZING               " "FIX_VECTOR_PROPORTIO"
	    "N                   " "VECTOR_SCALING_FACTOR_X                 " 
	    "VECTOR_SCALING_FACTOR_Y                 " "****XFACT2          "
	    "                    " "****YFACT2                              " 
	    "ENABLE_VECTOR_MESSAGE                   " "VECTOR_GRID_POSITION"
	    "                    " "ENABLE_MISS_VECTER_MARKER               " 
	    "MISS_VECTOR_MARKER_TYPE                 " "ENABLE_SMALL_VECTOR_"
	    "MARKER              " "SMALL_VECTOR_THRESHOLD                  " 
	    "SMALL_VECTOR_MARKER_TYPE                " "MISS_VECTOR_MARKER_S"
	    "IZE                 " "VECTOR_MESSAGE_HEIGHT                   " 
	    "VECTOR_MESSAGE_POSITION                 " "****IXINT           "
	    "                    " "****IYINT                               " 
	    "DRAW_UNIT_VECTOR                        " "DRAW_UNIT_VECTOR_MES"
	    "SAGE                " "UNIT_VECTOR_X_OFFSET                    " 
	    "UNIT_VECTOR_Y_OFFSET                    " "UNIT_VECTOR_X_LOCATI"
	    "ON                  " "UNIT_VECTOR_Y_LOCATION                  " 
	    "UNIT_VECTOR_X_LENGTH                    " "UNIT_VECTOR_Y_LENGTH"
	    "                    " "UNIT_VECTOR_X_NORMALIZED_LENGTH         " 
	    "UNIT_VECTOR_Y_NORMALIZED_LENGTH         " "****IUNTTL          "
	    "                    " "UNIT_VECTOR_TITLE_HEIGHT                " 
	    "UNIT_VECTOR_TITLE_INDEX                 " "UNIT_VECTOR_X_TITLE_"
	    "ANGLE               " "UNIT_VECTOR_Y_TITLE_ANGLE               " 
	    "****VUTOFF                              " "****RHFACT          "
	    "                    " "----RUNDEF                              ";

    /* System generated locals */
    address a__1[3];
    integer i__1[3];

    /* Builtin functions */
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen),
	     s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer n, id;
    extern integer lenc_(char *, ftnlen);
    static char cmsg[80];
    extern logical lchreq_(char *, char *, ftnlen, ftnlen);
    extern /* Subroutine */ int ugiqid_(char *, integer *, ftnlen), uglqid_(
	    char *, integer *, ftnlen), msgdmp_(char *, char *, char *, 
	    ftnlen, ftnlen, ftnlen), ugrqid_(char *, integer *, ftnlen), 
	    ugiqvl_(integer *, integer *), uglqvl_(integer *, integer *), 
	    ugisvl_(integer *, integer *), uglsvl_(integer *, integer *), 
	    ugrqvl_(integer *, integer *), ugrsvl_(integer *, integer *);

/*     / SHORT NAME / */
    switch(n__) {
	case 1: goto L_ugpqid;
	case 2: goto L_ugpqcp;
	case 3: goto L_ugpqcl;
	case 4: goto L_ugpqit;
	case 5: goto L_ugpqvl;
	case 6: goto L_ugpsvl;
	case 7: goto L_ugpqin;
	}

/*     < PARAMETERS FOR UNIT VECTOR > */
/*     < PARAMETERS FOR TITLES OF UNIT VECTOR > */
/*     / LONG NAME / */
/*     < PARAMETERS FOR UNIT VECTOR > */
/*     < PARAMETERS FOR TITLES OF UNIT VECTOR > */
    *ncp = 37;
    return 0;
/* ----------------------------------------------------------------------- */

L_ugpqid:
    for (n = 1; n <= 37; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *idx = n;
	    return 0;
	}
/* L10: */
    }
/* Writing concatenation */
    i__1[0] = 11, a__1[0] = "PARAMETER '";
    i__1[1] = lenc_(cp, cp_len), a__1[1] = cp;
    i__1[2] = 17, a__1[2] = "' IS NOT DEFINED.";
    s_cat(cmsg, a__1, i__1, &c__3, (ftnlen)80);
    msgdmp_("E", "UGPQID", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
    return 0;
/* ----------------------------------------------------------------------- */

L_ugpqcp:
    if (1 <= *idx && *idx <= 37) {
	s_copy(cp, cparas + (*idx - 1 << 3), cp_len, (ftnlen)8);
    } else {
	msgdmp_("E", "UGPQCP", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_ugpqcl:
    if (1 <= *idx && *idx <= 37) {
	s_copy(cp, cparal + (*idx - 1) * 40, cp_len, (ftnlen)40);
    } else {
	msgdmp_("E", "UGPQCL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_ugpqit:
    if (1 <= *idx && *idx <= 37) {
	*itp = itype[*idx - 1];
    } else {
	msgdmp_("E", "UGPQIT", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_ugpqvl:
    if (1 <= *idx && *idx <= 37) {
	if (itype[*idx - 1] == 1) {
	    ugiqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    ugiqvl_(&id, ipara);
	} else if (itype[*idx - 1] == 2) {
	    uglqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    uglqvl_(&id, ipara);
	} else if (itype[*idx - 1] == 3) {
	    ugrqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    ugrqvl_(&id, ipara);
	}
    } else {
	msgdmp_("E", "UGPQVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_ugpsvl:
    if (1 <= *idx && *idx <= 37) {
	if (itype[*idx - 1] == 1) {
	    ugiqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    ugisvl_(&id, ipara);
	} else if (itype[*idx - 1] == 2) {
	    uglqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    uglsvl_(&id, ipara);
	} else if (itype[*idx - 1] == 3) {
	    ugrqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    ugrsvl_(&id, ipara);
	}
    } else {
	msgdmp_("E", "UGPSVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_ugpqin:
    for (n = 1; n <= 37; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *in = n;
	    return 0;
	}
/* L20: */
    }
    *in = 0;
    return 0;
} /* ugpqnp_ */

/* Subroutine */ int ugpqnp_(integer *ncp)
{
    return ugpqnp_0_(0, ncp, (char *)0, (integer *)0, (integer *)0, (integer *
	    )0, (integer *)0, (ftnint)0);
    }

/* Subroutine */ int ugpqid_(char *cp, integer *idx, ftnlen cp_len)
{
    return ugpqnp_0_(1, (integer *)0, cp, idx, (integer *)0, (integer *)0, (
	    integer *)0, cp_len);
    }

/* Subroutine */ int ugpqcp_(integer *idx, char *cp, ftnlen cp_len)
{
    return ugpqnp_0_(2, (integer *)0, cp, idx, (integer *)0, (integer *)0, (
	    integer *)0, cp_len);
    }

/* Subroutine */ int ugpqcl_(integer *idx, char *cp, ftnlen cp_len)
{
    return ugpqnp_0_(3, (integer *)0, cp, idx, (integer *)0, (integer *)0, (
	    integer *)0, cp_len);
    }

/* Subroutine */ int ugpqit_(integer *idx, integer *itp)
{
    return ugpqnp_0_(4, (integer *)0, (char *)0, idx, itp, (integer *)0, (
	    integer *)0, (ftnint)0);
    }

/* Subroutine */ int ugpqvl_(integer *idx, integer *ipara)
{
    return ugpqnp_0_(5, (integer *)0, (char *)0, idx, (integer *)0, ipara, (
	    integer *)0, (ftnint)0);
    }

/* Subroutine */ int ugpsvl_(integer *idx, integer *ipara)
{
    return ugpqnp_0_(6, (integer *)0, (char *)0, idx, (integer *)0, ipara, (
	    integer *)0, (ftnint)0);
    }

/* Subroutine */ int ugpqin_(char *cp, integer *in, ftnlen cp_len)
{
    return ugpqnp_0_(7, (integer *)0, cp, (integer *)0, (integer *)0, (
	    integer *)0, in, cp_len);
    }


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__3 = 3;
static integer c__20 = 20;

/* ----------------------------------------------------------------------- */
/*     UGRQNP / UGRQID / UGRQCP / UGRQVL / UGRSVL */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int ugrqnp_0_(int n__, integer *ncp, char *cp, integer *idx, 
	real *rpara, integer *in, ftnlen cp_len)
{
    /* Initialized data */

    static char cparas[8*20] = "XFACT1  " "YFACT1  " "XFACT2  " "YFACT2  " 
	    "RSMALL  " "RSIZEM  " "RSIZET  " "XTTL    " "VXUOFF  " "VYUOFF  " 
	    "VXULOC  " "VYULOC  " "UXUNIT  " "UYUNIT  " "VXUNIT  " "VYUNIT  " 
	    "RSIZEUT " "VUTOFF  " "RHFACT  " "RUNDEF  ";
    static real rx[20] = { 1.f,1.f,-999.f,-999.f,.001f,.01f,-999.f,0.f,.02f,
	    0.f,-999.f,-999.f,-999.f,-999.f,.05f,.05f,-999.f,.005f,1.5f,
	    -999.f };
    static char cparal[40*20] = "VECTOR_SCALING_FACTOR_X                 " 
	    "VECTOR_SCALING_FACTOR_Y                 " "****XFACT2          "
	    "                    " "****YFACT2                              " 
	    "SMALL_VECTOR_THRESHOLD                  " "MISS_VECTOR_MARKER_S"
	    "IZE                 " "VECTOR_MESSAGE_HEIGHT                   " 
	    "VECTOR_MESSAGE_POSITION                 " "UNIT_VECTOR_X_OFFSET"
	    "                    " "UNIT_VECTOR_Y_OFFSET                    " 
	    "UNIT_VECTOR_X_LOCATION                  " "UNIT_VECTOR_Y_LOCATI"
	    "ON                  " "UNIT_VECTOR_X_LENGTH                    " 
	    "UNIT_VECTOR_Y_LENGTH                    " "UNIT_VECTOR_X_NORMAL"
	    "IZED_LENGTH         " "UNIT_VECTOR_Y_NORMALIZED_LENGTH         " 
	    "UNIT_VECTOR_TITLE_HEIGHT                " "****VUTOFF          "
	    "                    " "****RHFACT                              " 
	    "----RUNDEF                              ";
    static logical lfirst = TRUE_;

    /* System generated locals */
    address a__1[3];
    integer i__1[3];

    /* Builtin functions */
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen),
	     s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer n;
    extern integer lenc_(char *, ftnlen);
    static char cmsg[80];
    extern logical lchreq_(char *, char *, ftnlen, ftnlen);
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), rlrget_(char *, real *, integer *, ftnlen), 
	    rtrget_(char *, char *, real *, integer *, ftnlen, ftnlen), 
	    uzrget_(char *, real *, ftnlen);

/*     / SHORT NAME / */
    switch(n__) {
	case 1: goto L_ugrqid;
	case 2: goto L_ugrqcp;
	case 3: goto L_ugrqcl;
	case 4: goto L_ugrqvl;
	case 5: goto L_ugrsvl;
	case 6: goto L_ugrqin;
	}

/*     / LONG NAME / */
    *ncp = 20;
    return 0;
/* ----------------------------------------------------------------------- */

L_ugrqid:
    for (n = 1; n <= 20; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *idx = n;
	    return 0;
	}
/* L10: */
    }
/* Writing concatenation */
    i__1[0] = 11, a__1[0] = "PARAMETER '";
    i__1[1] = lenc_(cp, cp_len), a__1[1] = cp;
    i__1[2] = 17, a__1[2] = "' IS NOT DEFINED.";
    s_cat(cmsg, a__1, i__1, &c__3, (ftnlen)80);
    msgdmp_("E", "UGRQID", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
    return 0;
/* ----------------------------------------------------------------------- */

L_ugrqcp:
    if (1 <= *idx && *idx <= 20) {
	s_copy(cp, cparas + (*idx - 1 << 3), cp_len, (ftnlen)8);
    } else {
	msgdmp_("E", "UGRQCP", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_ugrqcl:
    if (1 <= *idx && *idx <= 20) {
	s_copy(cp, cparal + (*idx - 1) * 40, cp_len, (ftnlen)40);
    } else {
	msgdmp_("E", "UGRQCL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_ugrqvl:
    if (lfirst) {
	rtrget_("UG", cparas, rx, &c__20, (ftnlen)2, (ftnlen)8);
	rlrget_(cparal, rx, &c__20, (ftnlen)40);
	lfirst = FALSE_;
    }
    if (1 <= *idx && *idx <= 20) {
	*rpara = rx[*idx - 1];
	if ((*idx == 7 || *idx == 17) && rx[*idx - 1] == -999.f) {
	    uzrget_("RSIZEL1", rpara, (ftnlen)7);
	}
    } else {
	msgdmp_("E", "UGRQVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_ugrsvl:
    if (lfirst) {
	rtrget_("UG", cparas, rx, &c__20, (ftnlen)2, (ftnlen)8);
	rlrget_(cparal, rx, &c__20, (ftnlen)40);
	lfirst = FALSE_;
    }
    if (1 <= *idx && *idx <= 20) {
	rx[*idx - 1] = *rpara;
    } else {
	msgdmp_("E", "UGRSVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_ugrqin:
    for (n = 1; n <= 20; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *in = n;
	    return 0;
	}
/* L20: */
    }
    *in = 0;
    return 0;
} /* ugrqnp_ */

/* Subroutine */ int ugrqnp_(integer *ncp)
{
    return ugrqnp_0_(0, ncp, (char *)0, (integer *)0, (real *)0, (integer *)0,
	     (ftnint)0);
    }

/* Subroutine */ int ugrqid_(char *cp, integer *idx, ftnlen cp_len)
{
    return ugrqnp_0_(1, (integer *)0, cp, idx, (real *)0, (integer *)0, 
	    cp_len);
    }

/* Subroutine */ int ugrqcp_(integer *idx, char *cp, ftnlen cp_len)
{
    return ugrqnp_0_(2, (integer *)0, cp, idx, (real *)0, (integer *)0, 
	    cp_len);
    }

/* Subroutine */ int ugrqcl_(integer *idx, char *cp, ftnlen cp_len)
{
    return ugrqnp_0_(3, (integer *)0, cp, idx, (real *)0, (integer *)0, 
	    cp_len);
    }

/* Subroutine */ int ugrqvl_(integer *idx, real *rpara)
{
    return ugrqnp_0_(4, (integer *)0, (char *)0, idx, rpara, (integer *)0, (
	    ftnint)0);
    }

/* Subroutine */ int ugrsvl_(integer *idx, real *rpara)
{
    return ugrqnp_0_(5, (integer *)0, (char *)0, idx, rpara, (integer *)0, (
	    ftnint)0);
    }

/* Subroutine */ int ugrqin_(char *cp, integer *in, ftnlen cp_len)
{
    return ugrqnp_0_(6, (integer *)0, cp, (integer *)0, (real *)0, in, cp_len)
	    ;
    }


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__0 = 0;
static integer c__90 = 90;

/* ----------------------------------------------------------------------- */
/*     UGSUT / UGDUT */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int ugsut_0_(int n__, char *cside, char *cttl, ftnlen 
	cside_len, ftnlen cttl_len)
{
    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static real xx, yy;
    static char cs1[1];
    static real rsz[10], xoff, yoff;
    extern integer lenz_(char *, ftnlen);
    static integer ittl, nttl;
    static real xtoff, ytoff, rsize;
    static char cttlz[32*10];
    extern /* Subroutine */ int sgtxv_(real *, real *, char *, ftnlen);
    static real rhfact;
    static char csidez[1*10];
    static real rundef;
    extern /* Subroutine */ int ugiget_(char *, integer *, ftnlen), msgdmp_(
	    char *, char *, char *, ftnlen, ftnlen, ftnlen), ugrget_(char *, 
	    real *, ftnlen), cupper_(char *, ftnlen), ugiset_(char *, integer 
	    *, ftnlen);
    static integer iuindx;
    static real vutoff;
    extern /* Subroutine */ int ugrset_(char *, real *, ftnlen);
    static real vxuoff, vyuoff;
    extern /* Subroutine */ int uzrget_(char *, real *, ftnlen);
    static real vxuloc, vyuloc;
    extern /* Subroutine */ int sgstxi_(integer *);
    static integer iutxro;
    extern /* Subroutine */ int sgstxs_(real *);
    static real uxunit, vxunit, vyunit, uyunit;
    extern /* Subroutine */ int sgstxr_(integer *);
    static integer iutyro;

/*     / CHECK INPUT ARGUMENTS / */
    switch(n__) {
	case 1: goto L_ugdut;
	}

    *(unsigned char *)cs1 = *(unsigned char *)cside;
    cupper_(cs1, (ftnlen)1);
    if (! (*(unsigned char *)cs1 == 'X' || *(unsigned char *)cs1 == 'Y')) {
	msgdmp_("W", "UGSUT ", "SIDE PARAMETER IS INVALID.", (ftnlen)1, (
		ftnlen)6, (ftnlen)26);
	msgdmp_("M", "-CNT.-", "DO NOTHING.", (ftnlen)1, (ftnlen)6, (ftnlen)
		11);
	return 0;
    }
    if (lenz_(cttl, cttl_len) > 32) {
	msgdmp_("W", "UGSUT ", "LENGTH OF TITLE IS TOO LONG.", (ftnlen)1, (
		ftnlen)6, (ftnlen)28);
	msgdmp_("M", "-CNT.-", "DO NOTHING.", (ftnlen)1, (ftnlen)6, (ftnlen)
		11);
	return 0;
    }
    ugrget_("RUNDEF", &rundef, (ftnlen)6);
    ugiget_("IUNTTL", &nttl, (ftnlen)6);
    ugrget_("RHFACT", &rhfact, (ftnlen)6);
    ++nttl;
    if (nttl == 1) {
	ugrget_("VXUOFF", &vxuoff, (ftnlen)6);
	ugrget_("VYUOFF", &vyuoff, (ftnlen)6);
	ugrget_("VUTOFF", &vutoff, (ftnlen)6);
	xtoff = vxuoff + vutoff;
	ytoff = vyuoff + vutoff;
    } else {
	ugrget_("VXUOFF", &xtoff, (ftnlen)6);
	ugrget_("VYUOFF", &ytoff, (ftnlen)6);
    }
    *(unsigned char *)&csidez[nttl - 1] = *(unsigned char *)cs1;
    s_copy(cttlz + (nttl - 1 << 5), cttl, (ftnlen)32, cttl_len);
    ugrget_("RSIZEUT", &rsize, (ftnlen)7);
    if (rsize == rundef) {
	uzrget_("RSIZEC2", &rsz[nttl - 1], (ftnlen)7);
	ugrset_("RSIZEUT", &rsz[nttl - 1], (ftnlen)7);
    } else {
	rsz[nttl - 1] = rsize;
    }
    if (*(unsigned char *)cs1 == 'X') {
	ytoff += rsz[nttl - 1] * rhfact;
    } else if (*(unsigned char *)cs1 == 'Y') {
	xtoff += rsz[nttl - 1] * rhfact;
    }
    ugiset_("IUNTTL", &nttl, (ftnlen)6);
    ugrset_("VXUOFF", &xtoff, (ftnlen)6);
    ugrset_("VYUOFF", &ytoff, (ftnlen)6);
    return 0;
/* ----------------------------------------------------------------------- */

L_ugdut:
/*     / DRAW TITLES FOR UNIT VECTOR / */
    ugiget_("IUNTTL", &nttl, (ftnlen)6);
    ugrget_("RHFACT", &rhfact, (ftnlen)6);
    if (nttl <= 0) {
	return 0;
    } else {
	ugrget_("VXULOC", &vxuloc, (ftnlen)6);
	ugrget_("VYULOC", &vyuloc, (ftnlen)6);
	ugrget_("VXUNIT", &vxunit, (ftnlen)6);
	ugrget_("VYUNIT", &vyunit, (ftnlen)6);
	ugrget_("UXUNIT", &uxunit, (ftnlen)6);
	ugrget_("UYUNIT", &uyunit, (ftnlen)6);
	ugiget_("IUINDX", &iuindx, (ftnlen)6);
	ugrget_("VUTOFF", &vutoff, (ftnlen)6);
	xoff = vxuloc - vutoff;
	yoff = vyuloc - vutoff;
	i__1 = nttl;
	for (ittl = 1; ittl <= i__1; ++ittl) {
	    sgstxs_(&rsz[ittl - 1]);
	    if (*(unsigned char *)&csidez[ittl - 1] == 'X') {
		xx = vxuloc + vxunit * .5f;
		yy = yoff - rsz[ittl - 1] * rhfact / 2;
		yoff -= rsz[ittl - 1] * rhfact;
		ugiget_("IUTXRO", &iutxro, (ftnlen)6);
		if (iutxro != 0 && lenz_(cttlz + (ittl - 1 << 5), (ftnlen)32) 
			>= 2) {
		    msgdmp_("W", "UGDUT ", "LENGTH OF TITLE IS TOO LONG TO C"
			    "HANGE ROT", (ftnlen)1, (ftnlen)6, (ftnlen)41);
		    sgstxr_(&c__0);
		} else {
		    sgstxr_(&iutxro);
		}
	    } else if (*(unsigned char *)&csidez[ittl - 1] == 'Y') {
		xx = xoff - rsz[ittl - 1] * rhfact / 2;
		yy = vyuloc + vyunit * .5f;
		xoff -= rsz[ittl - 1] * rhfact;
		ugiget_("IUTYRO", &iutyro, (ftnlen)6);
		if (iutyro != 90 && lenz_(cttlz + (ittl - 1 << 5), (ftnlen)32)
			 >= 2) {
		    msgdmp_("W", "UGDUT ", "LENGTH OF TITLE IS TOO LONG TO C"
			    "HANGE ROT", (ftnlen)1, (ftnlen)6, (ftnlen)41);
		    sgstxr_(&c__90);
		} else {
		    sgstxr_(&iutyro);
		}
	    }
	    sgstxi_(&iuindx);
	    sgtxv_(&xx, &yy, cttlz + (ittl - 1 << 5), (ftnlen)32);
/* L10: */
	}
    }
    ugiset_("IUNTTL", &c__0, (ftnlen)6);
    ugrset_("VXUOFF", &vxuoff, (ftnlen)6);
    ugrset_("VYUOFF", &vyuoff, (ftnlen)6);
    return 0;
} /* ugsut_ */

/* Subroutine */ int ugsut_(char *cside, char *cttl, ftnlen cside_len, ftnlen 
	cttl_len)
{
    return ugsut_0_(0, cside, cttl, cside_len, cttl_len);
    }

/* Subroutine */ int ugdut_(void)
{
    return ugsut_0_(1, (char *)0, (char *)0, (ftnint)0, (ftnint)0);
    }


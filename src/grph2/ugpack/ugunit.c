/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     UGUNIT */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int ugunit_(void)
{
    static real vx1, vx2, vy1, vy2, uvx1, uvx2, uvy1, uvy2;
    static integer index;
    static real vxoff, vyoff, xfact2, yfact2, rundef;
    extern /* Subroutine */ int ugiget_(char *, integer *, ftnlen), szlacl_(
	    void), ugrget_(char *, real *, ftnlen), szlaop_(integer *, 
	    integer *), ugrset_(char *, real *, ftnlen);
    static real vxuloc, vyuloc;
    extern /* Subroutine */ int sgqvpt_(real *, real *, real *, real *), 
	    szlazv_(real *, real *, real *, real *);
    static real uxunit, vxunit, uyunit, vyunit;

/*     / GET INTERNAL PARAMETERES / */
    ugrget_("RUNDEF", &rundef, (ftnlen)6);
    ugiget_("INDEX", &index, (ftnlen)5);
/*     / INQUIRE NORMALIZATION TRANSFORMATION / */
    sgqvpt_(&vx1, &vx2, &vy1, &vy2);
/*     / DETERMINE LOCATION OF UNIT VECTORS IN NDC / */
    ugrget_("VXULOC", &vxuloc, (ftnlen)6);
    if (vxuloc == rundef) {
	ugrget_("VXUOFF", &vxoff, (ftnlen)6);
	vxuloc = vx2 + vxoff;
	ugrset_("VXULOC", &vxuloc, (ftnlen)6);
    }
    ugrget_("VYULOC", &vyuloc, (ftnlen)6);
    if (vyuloc == rundef) {
	ugrget_("VYUOFF", &vyoff, (ftnlen)6);
	vyuloc = vy1 + vyoff;
	ugrset_("VYULOC", &vyuloc, (ftnlen)6);
    }
/*     / DETERMINE LENGTH OF UNIT VECTORS IN NDC / */
    ugrget_("UXUNIT", &uxunit, (ftnlen)6);
    ugrget_("XFACT2", &xfact2, (ftnlen)6);
    if (uxunit == rundef) {
	ugrget_("VXUNIT", &vxunit, (ftnlen)6);
	uxunit = vxunit / xfact2;
	ugrset_("UXUNIT", &uxunit, (ftnlen)6);
    } else {
	vxunit = uxunit * xfact2;
	ugrset_("VXUNIT", &vxunit, (ftnlen)6);
    }
    ugrget_("UYUNIT", &uyunit, (ftnlen)6);
    ugrget_("YFACT2", &yfact2, (ftnlen)6);
    if (uyunit == rundef) {
	ugrget_("VYUNIT", &vyunit, (ftnlen)6);
	uyunit = vyunit / yfact2;
	ugrset_("UYUNIT", &uyunit, (ftnlen)6);
    } else {
	vyunit = uyunit * yfact2;
	ugrset_("VYUNIT", &vyunit, (ftnlen)6);
    }
/*     / DRAW UNIT VECTORS / */
    uvx1 = vxuloc;
    uvx2 = vxuloc + vxunit;
    uvy1 = vyuloc;
    uvy2 = vyuloc + vyunit;
    szlaop_(&c__1, &index);
    szlazv_(&uvx1, &uvy1, &uvx2, &uvy1);
    szlazv_(&uvx1, &uvy1, &uvx1, &uvy2);
    szlacl_();
    return 0;
} /* ugunit_ */


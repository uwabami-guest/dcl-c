/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__2 = 2;
static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     UGVECT */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int ugvect_(real *u, integer *mu, real *v, integer *mv, 
	integer *nx, integer *ny)
{
    /* System generated locals */
    integer u_dim1, u_offset, v_dim1, v_offset, i__1, i__2, i__3, i__4;
    real r__1, r__2, r__3;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_wsfi(icilist *), do_fio(integer *, char *, ftnlen), e_wsfi(void)
	    ;
    double sqrt(doublereal);

    /* Local variables */
    static integer i__, j;
    static real r__, au, av, ue, ve, vx0, vy0, vx1, vx2, vy1, vy2;
    static logical lok;
    static char cmsg[80];
    static logical lmsg;
    static real vxmn, vymn, xttl, vxmx, vymx;
    static logical lmada;
    static integer icent, index;
    extern real rgnle_(real *);
    static logical lmiss, lumsg;
    extern /* Subroutine */ int ugdut_(void);
    static real rminu;
    static logical lunit;
    static integer ixint;
    static real rmiss;
    static integer iyint;
    static real rmaxu, rmaxv, rminv;
    extern real ruwgx_(integer *), ruwgy_(integer *);
    static real xfact1, yfact1;
    static integer itype1, itype2;
    extern /* Subroutine */ int gllget_(char *, logical *, ftnlen);
    static logical lsmall, lnrmal;
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen), ugiget_(char 
	    *, integer *, ftnlen);
    static logical leqrat;
    extern /* Subroutine */ int uglget_(char *, logical *, ftnlen);
    static real rsmall;
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), szlacl_(void), ugrget_(char *, real *, ftnlen), 
	    uwdflt_(integer *, integer *);
    static logical lmissp;
    extern /* Subroutine */ int szlaop_(integer *, integer *);
    static real rsizem;
    extern /* Subroutine */ int ugrset_(char *, real *, ftnlen), ugunit_(void)
	    , stftrf_(real *, real *, real *, real *);
    static real rsizet;
    extern /* Subroutine */ int uzrget_(char *, real *, ftnlen), sgqvpt_(real 
	    *, real *, real *, real *);
    static real rsizez;
    extern /* Subroutine */ int sgpmzv_(integer *, real *, real *, integer *, 
	    integer *, real *), szlazv_(real *, real *, real *, real *), 
	    uzrset_(char *, real *, ftnlen);
    static real uxunit, uyunit;
    extern /* Subroutine */ int uxpttl_(char *, integer *, char *, real *, 
	    ftnlen, ftnlen);

    /* Fortran I/O blocks */
    static icilist io___42 = { 0, cmsg+7, 0, "(1P,E10.3)", 10, 1 };
    static icilist io___43 = { 0, cmsg+26, 0, "(1P,E10.3)", 10, 1 };
    static icilist io___44 = { 0, cmsg+7, 0, "(1P,E10.3)", 10, 1 };
    static icilist io___45 = { 0, cmsg+26, 0, "(1P,E10.3)", 10, 1 };


/*     / GET INTERNAL PARAMETERS / */
    /* Parameter adjustments */
    u_dim1 = *mu;
    u_offset = 1 + u_dim1;
    u -= u_offset;
    v_dim1 = *mv;
    v_offset = 1 + v_dim1;
    v -= v_offset;

    /* Function Body */
    gllget_("LMISS   ", &lmiss, (ftnlen)8);
    glrget_("RMISS   ", &rmiss, (ftnlen)8);
    ugiget_("INDEX   ", &index, (ftnlen)8);
    uglget_("LNRMAL  ", &lnrmal, (ftnlen)8);
    uglget_("LEQRAT  ", &leqrat, (ftnlen)8);
    uglget_("LMSG    ", &lmsg, (ftnlen)8);
    uglget_("LUNIT   ", &lunit, (ftnlen)8);
    uglget_("LUMSG   ", &lumsg, (ftnlen)8);
    ugiget_("ICENT   ", &icent, (ftnlen)8);
    uglget_("LMISSP  ", &lmissp, (ftnlen)8);
    ugiget_("ITYPE1  ", &itype1, (ftnlen)8);
    uglget_("LSMALL  ", &lsmall, (ftnlen)8);
    ugrget_("RSMALL  ", &rsmall, (ftnlen)8);
    ugiget_("ITYPE2  ", &itype2, (ftnlen)8);
    ugrget_("RSIZEM  ", &rsizem, (ftnlen)8);
    ugrget_("RSIZET  ", &rsizet, (ftnlen)8);
    ugrget_("XTTL    ", &xttl, (ftnlen)8);
    ugiget_("IXINT   ", &ixint, (ftnlen)8);
    ugiget_("IYINT   ", &iyint, (ftnlen)8);
/*     / SET GRID ATTRIBUTE IF IT HAS NOT BEEN SET YET / */
    uwdflt_(nx, ny);
/*     / CHECK INPUT DATA / */
    lmada = TRUE_;
    i__1 = *ny;
    i__2 = iyint;
    for (j = 1; i__2 < 0 ? j >= i__1 : j <= i__1; j += i__2) {
	i__3 = *nx;
	i__4 = ixint;
	for (i__ = 1; i__4 < 0 ? i__ >= i__3 : i__ <= i__3; i__ += i__4) {
	    lok = ! (lmiss && (u[i__ + j * u_dim1] == rmiss || v[i__ + j * 
		    v_dim1] == rmiss));
/*         / CHECK MIN & MAX / */
	    if (lmada) {
		if (lok) {
		    rmaxu = u[i__ + j * u_dim1];
		    rminu = u[i__ + j * u_dim1];
		    rmaxv = v[i__ + j * v_dim1];
		    rminv = v[i__ + j * v_dim1];
		    lmada = FALSE_;
		}
	    } else {
		if (lok) {
		    if (u[i__ + j * u_dim1] > rmaxu) {
			rmaxu = u[i__ + j * u_dim1];
		    } else if (u[i__ + j * u_dim1] < rminu) {
			rminu = u[i__ + j * u_dim1];
		    }
		    if (v[i__ + j * v_dim1] > rmaxv) {
			rmaxv = v[i__ + j * v_dim1];
		    } else if (v[i__ + j * v_dim1] < rminv) {
			rminv = v[i__ + j * v_dim1];
		    }
		}
	    }
/* L15: */
	}
/* L10: */
    }
/*     / WRITE MESSAGE IF MISSING OR ZERO FIELD / */
    if (lmada || rminu == 0.f && rmaxu == 0.f && rminv == 0.f && rmaxv == 0.f)
	     {
	if (lmada) {
	    s_copy(cmsg, "MISSING FIELD.", (ftnlen)80, (ftnlen)14);
	} else {
	    s_copy(cmsg, "ZERO FIELD.", (ftnlen)80, (ftnlen)11);
	}
	msgdmp_("W", "UGVECT", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
	if (lmsg) {
	    uzrget_("RSIZEC2", &rsizez, (ftnlen)7);
	    uzrset_("RSIZEC2", &rsizet, (ftnlen)7);
	    uxpttl_("B", &c__2, "  ", &xttl, (ftnlen)1, (ftnlen)2);
	    uxpttl_("B", &c__2, cmsg, &xttl, (ftnlen)1, (ftnlen)80);
	    uzrset_("RSIZEC2", &rsizez, (ftnlen)7);
	}
	goto L100;
    }
/*     / CALCULATE NORMALIZATION FACTOR / */
    if (lnrmal) {
	sgqvpt_(&vxmn, &vxmx, &vymn, &vymx);
	ue = (vxmx - vxmn) / (*nx / ixint);
	ve = (vymx - vymn) / (*ny / iyint);
/* Computing MAX */
	r__2 = abs(rminu), r__3 = abs(rmaxu);
	r__1 = ue / max(r__2,r__3);
	au = rgnle_(&r__1);
/* Computing MAX */
	r__2 = abs(rminv), r__3 = abs(rmaxv);
	r__1 = ve / max(r__2,r__3);
	av = rgnle_(&r__1);
	if (leqrat) {
	    au = min(au,av);
	    av = min(au,av);
	}
    } else {
	ugrget_("XFACT1  ", &xfact1, (ftnlen)8);
	ugrget_("YFACT1  ", &yfact1, (ftnlen)8);
	au = xfact1;
	av = yfact1;
    }
    ugrset_("XFACT2  ", &au, (ftnlen)8);
    ugrset_("YFACT2  ", &av, (ftnlen)8);
/*     / DRAW UNIT VECTOR IF LUNIT / */
    if (lunit) {
	ugunit_();
/*       / WRITE UNIT VALUE IF LMUSG / */
	if (lumsg) {
	    ugrget_("UXUNIT  ", &uxunit, (ftnlen)8);
	    ugrget_("UYUNIT  ", &uyunit, (ftnlen)8);
	    s_copy(cmsg, "XUNIT =##########, YUNIT =##########", (ftnlen)80, (
		    ftnlen)36);
	    s_wsfi(&io___42);
	    do_fio(&c__1, (char *)&uxunit, (ftnlen)sizeof(real));
	    e_wsfi();
	    s_wsfi(&io___43);
	    do_fio(&c__1, (char *)&uyunit, (ftnlen)sizeof(real));
	    e_wsfi();
	    uzrget_("RSIZEC2", &rsizez, (ftnlen)7);
	    uzrset_("RSIZEC2", &rsizet, (ftnlen)7);
	    uxpttl_("B", &c__2, "  ", &xttl, (ftnlen)1, (ftnlen)2);
	    uxpttl_("B", &c__2, cmsg, &xttl, (ftnlen)1, (ftnlen)80);
	    uzrset_("RSIZEC2", &rsizez, (ftnlen)7);
	}
/*       / DRAW TITLE FOR UNIT VECTOR / */
	ugdut_();
/*     / WRITE SCALING FACTOR IF LMSG / */
    } else if (lmsg) {
	s_copy(cmsg, "XFACT =##########, YFACT =##########", (ftnlen)80, (
		ftnlen)36);
	s_wsfi(&io___44);
	do_fio(&c__1, (char *)&au, (ftnlen)sizeof(real));
	e_wsfi();
	s_wsfi(&io___45);
	do_fio(&c__1, (char *)&av, (ftnlen)sizeof(real));
	e_wsfi();
	uzrget_("RSIZEC2", &rsizez, (ftnlen)7);
	uzrset_("RSIZEC2", &rsizet, (ftnlen)7);
	uxpttl_("B", &c__2, "  ", &xttl, (ftnlen)1, (ftnlen)2);
	uxpttl_("B", &c__2, cmsg, &xttl, (ftnlen)1, (ftnlen)80);
	uzrset_("RSIZEC2", &rsizez, (ftnlen)7);
    }
/*     / DRAW VECTORS / */
    szlaop_(&c__1, &index);
    i__2 = *ny;
    i__1 = iyint;
    for (j = 1; i__1 < 0 ? j >= i__2 : j <= i__2; j += i__1) {
	i__4 = *nx;
	i__3 = ixint;
	for (i__ = 1; i__3 < 0 ? i__ >= i__4 : i__ <= i__4; i__ += i__3) {
	    lok = ! (lmiss && (u[i__ + j * u_dim1] == rmiss || v[i__ + j * 
		    v_dim1] == rmiss));
	    r__1 = ruwgx_(&i__);
	    r__2 = ruwgy_(&j);
	    stftrf_(&r__1, &r__2, &vx0, &vy0);
	    if (lok) {
		vx1 = vx0 - u[i__ + j * u_dim1] * (icent + 1) / 2.f * au;
		vx2 = vx0 - u[i__ + j * u_dim1] * (icent - 1) / 2.f * au;
		vy1 = vy0 - v[i__ + j * v_dim1] * (icent + 1) / 2.f * av;
		vy2 = vy0 - v[i__ + j * v_dim1] * (icent - 1) / 2.f * av;
/* Computing 2nd power */
		r__1 = vx2 - vx1;
/* Computing 2nd power */
		r__2 = vy2 - vy1;
		r__ = sqrt(r__1 * r__1 + r__2 * r__2);
		if (lsmall && r__ <= rsmall) {
		    sgpmzv_(&c__1, &vx0, &vy0, &itype2, &index, &rsizem);
		} else {
		    szlazv_(&vx1, &vy1, &vx2, &vy2);
		}
	    } else {
		if (lmissp) {
		    sgpmzv_(&c__1, &vx0, &vy0, &itype1, &index, &rsizem);
		}
	    }
/* L20: */
	}
/* L25: */
    }
    szlacl_();
L100:
    return 0;
} /* ugvect_ */


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__12 = 12;

/* ----------------------------------------------------------------------- */
/*     UXPNUM : PLOT NUMBERS */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uxpnum_(char *cside, integer *islct, real *ux, integer *
	n, ftnlen cside_len)
{
    /* System generated locals */
    integer i__1;

    /* Local variables */
    static integer i__;
    static char ch[12*40];
    extern /* Subroutine */ int chval_(char *, real *, char *, ftnlen, ftnlen)
	    ;
    static char cfmtz[16];
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen);
    extern logical luxchk_(char *, ftnlen);
    extern /* Subroutine */ int uzcget_(char *, char *, ftnlen, ftnlen), 
	    uxplbl_(char *, integer *, real *, char *, integer *, integer *, 
	    ftnlen, ftnlen);

/*     / CHECK ARGUMENTS / */
    /* Parameter adjustments */
    --ux;

    /* Function Body */
    if (! luxchk_(cside, (ftnlen)1)) {
	msgdmp_("E", "UXPNUM", "SIDE PARAMETER IS INVALID.", (ftnlen)1, (
		ftnlen)6, (ftnlen)26);
    }
    if (! (0 <= *islct && *islct <= 2)) {
	msgdmp_("E", "UXPNUM", "'ISLCT' IS INVALID.", (ftnlen)1, (ftnlen)6, (
		ftnlen)19);
    }
    if (*n <= 0) {
	msgdmp_("E", "UXPNUM", "NUMBER OF POINTS IS INVALID.", (ftnlen)1, (
		ftnlen)6, (ftnlen)28);
    } else if (*n > 40) {
	msgdmp_("E", "UXPNUM", "WORKING AREA IS NOT ENOUGH.", (ftnlen)1, (
		ftnlen)6, (ftnlen)27);
    }
/*     / GENERATE CHARACTERS / */
    uzcget_("CXFMT", cfmtz, (ftnlen)5, (ftnlen)16);
    i__1 = *n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	chval_(cfmtz, &ux[i__], ch + (i__ - 1) * 12, (ftnlen)16, (ftnlen)12);
/* L10: */
    }
/*     / UXPLBL CALL / */
    uxplbl_(cside, islct, &ux[1], ch, &c__12, n, (ftnlen)1, (ftnlen)12);
    return 0;
} /* uxpnum_ */


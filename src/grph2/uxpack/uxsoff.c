/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     UXSOFF / UXROFF */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uxsoff_0_(int n__)
{
    /* Initialized data */

    static logical lcall = FALSE_;

    static logical loff;
    static real xoff, xfct, fxmn, fxmx, uxmn, uymn, uxmx, uymx;
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), sgqwnd_(real *, real *, real *, real *), sgswnd_(
	    real *, real *, real *, real *), sgstrf_(void), uzlget_(char *, 
	    logical *, ftnlen), uzrget_(char *, real *, ftnlen);

    switch(n__) {
	case 1: goto L_uxroff;
	}

/*     / SCALING FACTOR FOR OFFSET / */
    uzlget_("LOFFSET", &loff, (ftnlen)7);
    if (loff) {
	uzrget_("XOFFSET", &xoff, (ftnlen)7);
	uzrget_("XFACT  ", &xfct, (ftnlen)7);
    } else {
	xoff = 0.f;
	xfct = 1.f;
    }
/*     / INQUIRE NORMALIZATION TRANSFORMATION / */
    sgqwnd_(&uxmn, &uxmx, &uymn, &uymx);
    fxmn = xfct * uxmn + xoff;
    fxmx = xfct * uxmx + xoff;
/*     / SET NORMALIZATION TRANSFORMATION / */
    sgswnd_(&fxmn, &fxmx, &uymn, &uymx);
    sgstrf_();
    lcall = TRUE_;
    return 0;
/* ----------------------------------------------------------------------- */

L_uxroff:
    if (! lcall) {
	msgdmp_("E", "UXROFF", "UXSOFF HAS NOT BEEN CALLED.", (ftnlen)1, (
		ftnlen)6, (ftnlen)27);
    }
/*     / RESET NORMALIZATION TRANSFORMATION / */
    sgswnd_(&uxmn, &uxmx, &uymn, &uymx);
    sgstrf_();
    return 0;
} /* uxsoff_ */

/* Subroutine */ int uxsoff_(void)
{
    return uxsoff_0_(0);
    }

/* Subroutine */ int uxroff_(void)
{
    return uxsoff_0_(1);
    }


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static logical c_false = FALSE_;

/* ----------------------------------------------------------------------- */
/*     UXPLBB : PLOT LABELS ( BETWEEN THE POINTS ) */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uxplbb_(real *ux, char *ch, integer *nc, integer *n, 
	real *upy, real *roffx, real *rsize, integer *irota, integer *icent, 
	integer *index, real *rbtwn, logical *lbound, logical *lbmsg, ftnlen 
	ch_len)
{
    /* System generated locals */
    integer i__1;
    real r__1;

    /* Local variables */
    static integer i__, lc, lcz;
    static real cwx, vpy, vpx, vwx, vpx1, vpx2;
    extern integer lenc_(char *, ftnlen);
    static real wxch, wych;
    static integer jrota;
    extern /* Subroutine */ int sglget_(char *, logical *, ftnlen), msgdmp_(
	    char *, char *, char *, ftnlen, ftnlen, ftnlen);
    static logical lclipz;
    extern /* Subroutine */ int sglset_(char *, logical *, ftnlen), stftrf_(
	    real *, real *, real *, real *), sztxcl_(void), sztxop_(real *, 
	    integer *, integer *, integer *), szqtxw_(char *, integer *, real 
	    *, real *, ftnlen), sztxzv_(real *, real *, char *, ftnlen);

    /* Parameter adjustments */
    ch -= ch_len;
    --ux;

    /* Function Body */
    if (*nc <= 0) {
	msgdmp_("E", "UXPLBB", "CHARACTER LENGTH IS LESS THAN OR EQUAL TO ZE"
		"RO.", (ftnlen)1, (ftnlen)6, (ftnlen)47);
    }
    if (*n <= 1) {
	msgdmp_("E", "UXPLBB", "NUMBER OF POINTS IS INVALID.", (ftnlen)1, (
		ftnlen)6, (ftnlen)28);
    }
    if (*rsize <= 0.f) {
	msgdmp_("E", "UXPLBB", "TEXT HEIGHT IS LESS THAN ZERO.", (ftnlen)1, (
		ftnlen)6, (ftnlen)30);
    }
    if (! (-1 <= *icent && *icent <= 1)) {
	msgdmp_("E", "UXPLBB", "CENTERING OPTION IS INVALID.", (ftnlen)1, (
		ftnlen)6, (ftnlen)28);
    }
    if (*index <= 0) {
	msgdmp_("E", "UXPLBB", "TEXT INDEX IS INVALID.", (ftnlen)1, (ftnlen)6,
		 (ftnlen)22);
    }
    sglget_("LCLIP", &lclipz, (ftnlen)5);
    sglset_("LCLIP", &c_false, (ftnlen)5);
    i__1 = *irota * 90;
    sztxop_(rsize, &i__1, icent, index);
    i__1 = *n - 1;
    for (i__ = 1; i__ <= i__1; ++i__) {
	lc = lenc_(ch + i__ * ch_len, ch_len);
	szqtxw_(ch + i__ * ch_len, &lcz, &wxch, &wych, ch_len);
	stftrf_(&ux[i__], upy, &vpx1, &vpy);
	stftrf_(&ux[i__ + 1], upy, &vpx2, &vpy);
	vwx = (r__1 = vpx2 - vpx1, abs(r__1));
	jrota = *irota % 2;
	if (jrota == 0) {
	    cwx = *rsize * wxch;
	} else {
	    cwx = *rsize * wych;
	}
	if (cwx > vwx && *lbound) {
	    if (*lbmsg) {
		msgdmp_("W", "UXPLBB", "SPACE FOR LABEL IS NOT ENOUGH.", (
			ftnlen)1, (ftnlen)6, (ftnlen)30);
	    }
	    goto L10;
	}
	vpx = (vpx1 + vpx2) / 2 + (vwx - cwx) / 2 * *rbtwn;
	vpy += *roffx;
	sztxzv_(&vpx, &vpy, ch + i__ * ch_len, lc);
L10:
	;
    }
    sztxcl_();
/* L15: */
    sglset_("LCLIP", &lclipz, (ftnlen)5);
    return 0;
} /* uxplbb_ */


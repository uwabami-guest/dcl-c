/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;
static integer c__2 = 2;
static logical c_true = TRUE_;
static logical c_false = FALSE_;

/* ----------------------------------------------------------------------- */
/*     UXPLBL : PLOT LABELS */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uxplbl_(char *cside, integer *islct, real *ux, char *ch, 
	integer *nc, integer *n, ftnlen cside_len, ftnlen ch_len)
{
    /* System generated locals */
    address a__1[2];
    integer i__1[2], i__2;
    char ch__1[6], ch__2[7];

    /* Builtin functions */
    integer s_wsfi(icilist *), do_fio(integer *, char *, ftnlen), e_wsfi(void)
	    ;
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen);
    integer i_sign(integer *, integer *);

    /* Local variables */
    static integer i__, ic;
    static real pad, rlc;
    static integer lcw;
    static real wxch, wych, uxmn, uymn, posy, uxmx, uymx;
    static integer iflag, icent, ncmax, index;
    static char cslct[1];
    static integer irota, jrota;
    static real roffx;
    static logical lbtwn;
    static real roffz, rsize, rbtwn;
    extern logical lchreq_(char *, char *, ftnlen, ftnlen);
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), uxplba_(real *, char *, integer *, integer *, 
	    real *, real *, real *, integer *, integer *, integer *, ftnlen), 
	    uxplbb_(real *, char *, integer *, integer *, real *, real *, 
	    real *, integer *, integer *, integer *, real *, logical *, 
	    logical *, ftnlen);
    extern logical luxchk_(char *, ftnlen);
    extern /* Subroutine */ int sgqwnd_(real *, real *, real *, real *), 
	    uziget_(char *, integer *, ftnlen), uzlget_(char *, logical *, 
	    ftnlen), uzrget_(char *, real *, ftnlen), uzrset_(char *, real *, 
	    ftnlen), szqtxw_(char *, integer *, real *, real *, ftnlen);

    /* Fortran I/O blocks */
    static icilist io___2 = { 0, cslct, 0, "(I1)", 1, 1 };


    /* Parameter adjustments */
    ch -= ch_len;
    --ux;

    /* Function Body */
    if (! luxchk_(cside, (ftnlen)1)) {
	msgdmp_("E", "UXPLBL", "SIDE PARAMETER IS INVALID.", (ftnlen)1, (
		ftnlen)6, (ftnlen)26);
    }
    if (! (0 <= *islct && *islct <= 2)) {
	msgdmp_("E", "UXPLBL", "'ISLCT' IS INVALID.", (ftnlen)1, (ftnlen)6, (
		ftnlen)19);
    }
    if (*nc <= 0) {
	msgdmp_("E", "UXPLBL", "CHARACTER LENGTH IS LESS THAN OR EQUAL TO ZE"
		"RO.", (ftnlen)1, (ftnlen)6, (ftnlen)47);
    }
    if (*n <= 0) {
	msgdmp_("E", "UXPLBL", "NUMBER OF POINTS IS INVALID.", (ftnlen)1, (
		ftnlen)6, (ftnlen)28);
    }
    s_wsfi(&io___2);
    do_fio(&c__1, (char *)&(*islct), (ftnlen)sizeof(integer));
    e_wsfi();
/* Writing concatenation */
    i__1[0] = 5, a__1[0] = "ROFFX";
    i__1[1] = 1, a__1[1] = cside;
    s_cat(ch__1, a__1, i__1, &c__2, (ftnlen)6);
    uzrget_(ch__1, &roffx, (ftnlen)6);
/* Writing concatenation */
    i__1[0] = 6, a__1[0] = "RSIZEL";
    i__1[1] = 1, a__1[1] = cslct;
    s_cat(ch__2, a__1, i__1, &c__2, (ftnlen)7);
    uzrget_(ch__2, &rsize, (ftnlen)7);
/* Writing concatenation */
    i__1[0] = 6, a__1[0] = "ICENTX";
    i__1[1] = 1, a__1[1] = cside;
    s_cat(ch__2, a__1, i__1, &c__2, (ftnlen)7);
    uziget_(ch__2, &icent, (ftnlen)7);
/* Writing concatenation */
    i__1[0] = 6, a__1[0] = "IROTLX";
    i__1[1] = 1, a__1[1] = cside;
    s_cat(ch__2, a__1, i__1, &c__2, (ftnlen)7);
    uziget_(ch__2, &irota, (ftnlen)7);
/* Writing concatenation */
    i__1[0] = 6, a__1[0] = "INDEXL";
    i__1[1] = 1, a__1[1] = cslct;
    s_cat(ch__2, a__1, i__1, &c__2, (ftnlen)7);
    uziget_(ch__2, &index, (ftnlen)7);
    uzrget_("PAD1", &pad, (ftnlen)4);
    uzlget_("LBTWN", &lbtwn, (ftnlen)5);
    if (lbtwn) {
	uzrget_("RBTWN", &rbtwn, (ftnlen)5);
	ncmax = *n - 1;
    } else {
	ncmax = *n;
    }
    if (! lchreq_(cside, "U", (ftnlen)1, (ftnlen)1)) {
	sgqwnd_(&uxmn, &uxmx, &uymn, &uymx);
	if (lchreq_(cside, "B", (ftnlen)1, (ftnlen)1)) {
	    posy = uymn;
	    iflag = -1;
	} else {
	    posy = uymx;
	    iflag = 1;
	}
    } else {
	uzrget_("UYUSER", &posy, (ftnlen)6);
	uziget_("IFLAG", &iflag, (ftnlen)5);
	iflag = i_sign(&c__1, &iflag);
    }
    jrota = (irota + 2) % 4 - 2;
    if (jrota == -2) {
	jrota = 0;
    }
    rlc = 1.f;
    i__2 = ncmax;
    for (i__ = 1; i__ <= i__2; ++i__) {
	szqtxw_(ch + i__ * ch_len, &lcw, &wxch, &wych, ch_len);
	if (jrota == 0 && wych > rlc) {
	    rlc = wych;
	} else if (jrota != 0 && wxch > rlc) {
	    rlc = wxch;
	}
/* L10: */
    }
    ic = jrota * icent * iflag;
    roffz = roffx + rsize * (pad + rlc * (ic + 1) * .5f) * iflag;
    roffx += rsize * (pad + rlc) * iflag;
    if (lbtwn) {
	uxplbb_(&ux[1], ch + ch_len, nc, n, &posy, &roffz, &rsize, &irota, &
		icent, &index, &rbtwn, &c_true, &c_false, ch_len);
    } else {
	uxplba_(&ux[1], ch + ch_len, nc, n, &posy, &roffz, &rsize, &irota, &
		icent, &index, ch_len);
    }
/* Writing concatenation */
    i__1[0] = 5, a__1[0] = "ROFFX";
    i__1[1] = 1, a__1[1] = cside;
    s_cat(ch__1, a__1, i__1, &c__2, (ftnlen)6);
    uzrset_(ch__1, &roffx, (ftnlen)6);
    return 0;
} /* uxplbl_ */


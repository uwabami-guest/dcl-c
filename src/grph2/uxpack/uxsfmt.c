/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uxsfmt_0_(int n__, char *cfmt, ftnlen cfmt_len)
{
    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer nc;
    extern integer lenc_(char *, ftnlen);
    static char cfmtz[16];
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), uzcget_(char *, char *, ftnlen, ftnlen), uzcset_(
	    char *, char *, ftnlen, ftnlen);

    switch(n__) {
	case 1: goto L_uxqfmt;
	}

    uzcget_("CXFMT", cfmtz, (ftnlen)5, (ftnlen)16);
    nc = lenc_(cfmt, cfmt_len);
    if (nc >= 2 && *(unsigned char *)cfmt == '+') {
	if (lenc_(cfmtz, (ftnlen)16) == 1 || *(unsigned char *)&cfmtz[1] == 
		'+') {
	    s_copy(cfmtz + 1, cfmt, nc, cfmt_len);
	} else {
	    msgdmp_("E", "UXSFMT", "SUB OPTION CANNOT BE USED.", (ftnlen)1, (
		    ftnlen)6, (ftnlen)26);
	}
    } else {
	s_copy(cfmtz, cfmt, (ftnlen)16, cfmt_len);
    }
    uzcset_("CXFMT", cfmtz, (ftnlen)5, (ftnlen)16);
    return 0;
/* ----------------------------------------------------------------------- */

L_uxqfmt:
    uzcget_("CXFMT", cfmt, (ftnlen)5, cfmt_len);
    return 0;
} /* uxsfmt_ */

/* Subroutine */ int uxsfmt_(char *cfmt, ftnlen cfmt_len)
{
    return uxsfmt_0_(0, cfmt, cfmt_len);
    }

/* Subroutine */ int uxqfmt_(char *cfmt, ftnlen cfmt_len)
{
    return uxsfmt_0_(1, cfmt, cfmt_len);
    }


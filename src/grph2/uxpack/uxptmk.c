/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;
static integer c__2 = 2;

/* ----------------------------------------------------------------------- */
/*     UXPTMK : PLOT TICKMARKS */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uxptmk_(char *cside, integer *islct, real *ux, integer *
	n, ftnlen cside_len)
{
    /* System generated locals */
    address a__1[2];
    integer i__1[2];
    real r__1;
    char ch__1[6], ch__2[7];

    /* Builtin functions */
    integer s_wsfi(icilist *), do_fio(integer *, char *, ftnlen), e_wsfi(void)
	    ;
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen);
    integer i_sign(integer *, integer *);

    /* Local variables */
    static integer jsgn;
    static real uxmn, uymn, posy, uxmx, uymx;
    static integer iflag, index;
    static char cslct[1];
    static integer inner;
    static real rtick, roffx, rofgx, rsize;
    extern logical lchreq_(char *, char *, ftnlen, ftnlen);
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen);
    extern logical luxchk_(char *, ftnlen);
    extern /* Subroutine */ int sgqwnd_(real *, real *, real *, real *), 
	    uziget_(char *, integer *, ftnlen), uzrget_(char *, real *, 
	    ftnlen), uzrset_(char *, real *, ftnlen), uxptmz_(real *, integer 
	    *, real *, real *, real *, integer *);

    /* Fortran I/O blocks */
    static icilist io___2 = { 0, cslct, 0, "(I1)", 1, 1 };


    /* Parameter adjustments */
    --ux;

    /* Function Body */
    if (! luxchk_(cside, (ftnlen)1)) {
	msgdmp_("E", "UXPTMK", "SIDE PARAMETER IS INVALID.", (ftnlen)1, (
		ftnlen)6, (ftnlen)26);
    }
    if (! (0 <= *islct && *islct <= 2)) {
	msgdmp_("E", "UXPTMK", "'ISLCT' IS INVALID.", (ftnlen)1, (ftnlen)6, (
		ftnlen)19);
    }
    if (*n <= 0) {
	msgdmp_("E", "UXPTMK", "NUMBER OF POINTS IS INVALID.", (ftnlen)1, (
		ftnlen)6, (ftnlen)28);
    }
    s_wsfi(&io___2);
    do_fio(&c__1, (char *)&(*islct), (ftnlen)sizeof(integer));
    e_wsfi();
/* Writing concatenation */
    i__1[0] = 5, a__1[0] = "ROFFX";
    i__1[1] = 1, a__1[1] = cside;
    s_cat(ch__1, a__1, i__1, &c__2, (ftnlen)6);
    uzrget_(ch__1, &roffx, (ftnlen)6);
/* Writing concatenation */
    i__1[0] = 5, a__1[0] = "ROFGX";
    i__1[1] = 1, a__1[1] = cside;
    s_cat(ch__1, a__1, i__1, &c__2, (ftnlen)6);
    uzrget_(ch__1, &rofgx, (ftnlen)6);
/* Writing concatenation */
    i__1[0] = 6, a__1[0] = "INDEXT";
    i__1[1] = 1, a__1[1] = cslct;
    s_cat(ch__2, a__1, i__1, &c__2, (ftnlen)7);
    uziget_(ch__2, &index, (ftnlen)7);
/* Writing concatenation */
    i__1[0] = 6, a__1[0] = "RSIZET";
    i__1[1] = 1, a__1[1] = cslct;
    s_cat(ch__2, a__1, i__1, &c__2, (ftnlen)7);
    uzrget_(ch__2, &rsize, (ftnlen)7);
    uziget_("INNER", &inner, (ftnlen)5);
    jsgn = i_sign(&c__1, &inner);
    if (! lchreq_(cside, "U", (ftnlen)1, (ftnlen)1)) {
	sgqwnd_(&uxmn, &uxmx, &uymn, &uymx);
	if (lchreq_(cside, "B", (ftnlen)1, (ftnlen)1)) {
	    posy = uymn;
	    iflag = -1;
	} else {
	    posy = uymx;
	    iflag = 1;
	}
    } else {
	uzrget_("UYUSER", &posy, (ftnlen)6);
	uziget_("IFLAG", &iflag, (ftnlen)5);
	iflag = i_sign(&c__1, &iflag);
    }
    rtick = -rsize * jsgn * iflag;
    if (iflag >= 0) {
/* Computing MAX */
	r__1 = rofgx + rtick;
	roffx = max(r__1,roffx);
    } else {
/* Computing MIN */
	r__1 = rofgx + rtick;
	roffx = min(r__1,roffx);
    }
    uxptmz_(&ux[1], n, &posy, &rofgx, &rtick, &index);
/* Writing concatenation */
    i__1[0] = 5, a__1[0] = "ROFFX";
    i__1[1] = 1, a__1[1] = cside;
    s_cat(ch__1, a__1, i__1, &c__2, (ftnlen)6);
    uzrset_(ch__1, &roffx, (ftnlen)6);
    return 0;
} /* uxptmk_ */


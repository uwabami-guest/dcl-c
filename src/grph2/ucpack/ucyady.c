/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__2 = 2;
static integer c__1 = 1;
static integer c__0 = 0;
static logical c_false = FALSE_;

/* ----------------------------------------------------------------------- */
/*     UCYADY : PLOT DATE AXIS */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int ucyady_(char *cside, integer *jd0, integer *nd, ftnlen 
	cside_len)
{
    /* System generated locals */
    address a__1[2];
    integer i__1, i__2[2];
    char ch__1[7];
    icilist ici__1;

    /* Builtin functions */
    integer s_wsfi(icilist *), do_fio(integer *, char *, ftnlen), e_wsfi(void)
	    ;
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen);

    /* Local variables */
    static integer i__;
    static char ch[2*100];
    static integer id, nn;
    static real uy[100];
    static integer it0, iy0, idi, iml, moi, iti, iyi;
    extern /* Subroutine */ int cladj_(char *, ftnlen);
    static logical label;
    extern /* Subroutine */ int date12_(integer *, integer *, integer *), 
	    date23_(integer *, integer *, integer *, integer *);
    static integer icent;
    extern integer ndmon_(integer *, integer *);
    static integer irota, irotc;
    static logical lbtwn;
    extern /* Subroutine */ int datef2_(integer *, integer *, integer *, 
	    integer *, integer *);
    extern integer nucday_(char *, integer *, ftnlen);
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen);
    extern logical luychk_(char *, ftnlen);
    extern /* Subroutine */ int uziget_(char *, integer *, ftnlen), uyplbl_(
	    char *, integer *, real *, char *, integer *, integer *, ftnlen, 
	    ftnlen), uzlget_(char *, logical *, ftnlen), uziset_(char *, 
	    integer *, ftnlen), uzlset_(char *, logical *, ftnlen), uypaxs_(
	    char *, integer *, ftnlen), uyptmk_(char *, integer *, real *, 
	    integer *, ftnlen);

    if (! luychk_(cside, (ftnlen)1)) {
	msgdmp_("E", "UCYADY", "SIDE PARAMETER IS INVALID.", (ftnlen)1, (
		ftnlen)6, (ftnlen)26);
    }
    if (*jd0 < 0) {
	msgdmp_("E", "UCYADY", "FIRST DATE IS LESS THAN 0.", (ftnlen)1, (
		ftnlen)6, (ftnlen)26);
    }
    if (*nd <= 0) {
	msgdmp_("E", "UCYADY", "DATE LENGTH IS LESS THAN 0.", (ftnlen)1, (
		ftnlen)6, (ftnlen)27);
    }
    uypaxs_(cside, &c__2, (ftnlen)1);
    id = nucday_("Y", nd, (ftnlen)1);
    if (id <= 0) {
	msgdmp_("W", "UCYADY", "NO DAY-AXIS.", (ftnlen)1, (ftnlen)6, (ftnlen)
		12);
	return 0;
    }
    date12_(jd0, &iy0, &it0);
    nn = 0;
    i__1 = *nd;
    for (i__ = 0; i__ <= i__1; ++i__) {
	datef2_(&i__, &iy0, &it0, &iyi, &iti);
	date23_(&iyi, &moi, &idi, &iti);
	iml = ndmon_(&iyi, &moi);
	if (idi < iml - id / 2 && idi % id == 0 || idi == iml) {
	    ++nn;
	    if (nn > 100) {
		msgdmp_("E", "UCYADY", "WORKING AREA IS NOT ENOUGH.", (ftnlen)
			1, (ftnlen)6, (ftnlen)27);
	    }
	    uy[nn - 1] = (real) i__;
	    ici__1.icierr = 0;
	    ici__1.icirnum = 1;
	    ici__1.icirlen = 2;
	    ici__1.iciunit = ch + (nn - 1 << 1);
	    ici__1.icifmt = "(I2)";
	    s_wsfi(&ici__1);
	    do_fio(&c__1, (char *)&idi, (ftnlen)sizeof(integer));
	    e_wsfi();
	    cladj_(ch + (nn - 1 << 1), (ftnlen)2);
	}
/* L10: */
    }
    if (nn == 0) {
	msgdmp_("W", "UCYADY", "THERE IS NO TICKMARK / LABEL.", (ftnlen)1, (
		ftnlen)6, (ftnlen)29);
	return 0;
    }
/* Writing concatenation */
    i__2[0] = 6, a__1[0] = "ICENTY";
    i__2[1] = 1, a__1[1] = cside;
    s_cat(ch__1, a__1, i__2, &c__2, (ftnlen)7);
    uziget_(ch__1, &icent, (ftnlen)7);
/* Writing concatenation */
    i__2[0] = 6, a__1[0] = "IROTLY";
    i__2[1] = 1, a__1[1] = cside;
    s_cat(ch__1, a__1, i__2, &c__2, (ftnlen)7);
    uziget_(ch__1, &irota, (ftnlen)7);
/* Writing concatenation */
    i__2[0] = 6, a__1[0] = "IROTCY";
    i__2[1] = 1, a__1[1] = cside;
    s_cat(ch__1, a__1, i__2, &c__2, (ftnlen)7);
    uziget_(ch__1, &irotc, (ftnlen)7);
    uzlget_("LBTWN", &lbtwn, (ftnlen)5);
/* Writing concatenation */
    i__2[0] = 6, a__1[0] = "ICENTY";
    i__2[1] = 1, a__1[1] = cside;
    s_cat(ch__1, a__1, i__2, &c__2, (ftnlen)7);
    uziset_(ch__1, &c__0, (ftnlen)7);
/* Writing concatenation */
    i__2[0] = 6, a__1[0] = "IROTLY";
    i__2[1] = 1, a__1[1] = cside;
    s_cat(ch__1, a__1, i__2, &c__2, (ftnlen)7);
    uziset_(ch__1, &irotc, (ftnlen)7);
    uzlset_("LBTWN", &c_false, (ftnlen)5);
    uyptmk_(cside, &c__1, uy, &nn, (ftnlen)1);
/* Writing concatenation */
    i__2[0] = 6, a__1[0] = "LABELY";
    i__2[1] = 1, a__1[1] = cside;
    s_cat(ch__1, a__1, i__2, &c__2, (ftnlen)7);
    uzlget_(ch__1, &label, (ftnlen)7);
    if (label) {
	uyplbl_(cside, &c__1, uy, ch, &c__2, &nn, (ftnlen)1, (ftnlen)2);
    }
/* Writing concatenation */
    i__2[0] = 6, a__1[0] = "ICENTY";
    i__2[1] = 1, a__1[1] = cside;
    s_cat(ch__1, a__1, i__2, &c__2, (ftnlen)7);
    uziset_(ch__1, &icent, (ftnlen)7);
/* Writing concatenation */
    i__2[0] = 6, a__1[0] = "IROTLY";
    i__2[1] = 1, a__1[1] = cside;
    s_cat(ch__1, a__1, i__2, &c__2, (ftnlen)7);
    uziset_(ch__1, &irota, (ftnlen)7);
    uzlset_("LBTWN", &lbtwn, (ftnlen)5);
    return 0;
} /* ucyady_ */


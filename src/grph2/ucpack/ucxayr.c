/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__2 = 2;
static integer c__1 = 1;
static integer c__0 = 0;
static logical c_true = TRUE_;
static integer c__4 = 4;

/* ----------------------------------------------------------------------- */
/*     UCXAYR : PLOT YEAR AXIS */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int ucxayr_(char *cside, integer *jd0, integer *nd, ftnlen 
	cside_len)
{
    /* System generated locals */
    address a__1[2];
    integer i__1, i__2[2];
    real r__1;
    char ch__1[7];
    icilist ici__1;

    /* Builtin functions */
    integer s_wsfi(icilist *), do_fio(integer *, char *, ftnlen), e_wsfi(void)
	    ;
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen);

    /* Local variables */
    static integer i__;
    static char ch[4*50];
    static integer nn;
    static real ux[50];
    static integer id0, mo0, iy0;
    static real vx1, vx2, vy1, vy2;
    static integer idi, moi, iyi, iyl, itdi, nchx;
    extern /* Subroutine */ int cladj_(char *, ftnlen);
    static logical label;
    extern /* Subroutine */ int date13_(integer *, integer *, integer *, 
	    integer *), date32_(integer *, integer *, integer *, integer *);
    static integer icent, irota, irotc;
    static logical lbtwn;
    static real rsize;
    extern /* Subroutine */ int datef3_(integer *, integer *, integer *, 
	    integer *, integer *, integer *, integer *);
    static real rtick2;
    extern integer ndyear_(integer *);
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen);
    extern logical luxchk_(char *, ftnlen);
    extern /* Subroutine */ int uxplbl_(char *, integer *, real *, char *, 
	    integer *, integer *, ftnlen, ftnlen), uziget_(char *, integer *, 
	    ftnlen), uzlget_(char *, logical *, ftnlen), uzrget_(char *, real 
	    *, ftnlen), uziset_(char *, integer *, ftnlen), sgqvpt_(real *, 
	    real *, real *, real *), uzlset_(char *, logical *, ftnlen), 
	    uxpaxs_(char *, integer *, ftnlen), uxptmk_(char *, integer *, 
	    real *, integer *, ftnlen), uzrset_(char *, real *, ftnlen);

    if (! luxchk_(cside, (ftnlen)1)) {
	msgdmp_("E", "UCXAYR", "SIDE PARAMETER IS INVALID.", (ftnlen)1, (
		ftnlen)6, (ftnlen)26);
    }
    if (*jd0 < 0) {
	msgdmp_("E", "UCXAYR", "FIRST DATE IS LESS THAN 0.", (ftnlen)1, (
		ftnlen)6, (ftnlen)26);
    }
    if (*nd <= 0) {
	msgdmp_("E", "UCXAYR", "DATE LENGTH IS LESS THAN 0.", (ftnlen)1, (
		ftnlen)6, (ftnlen)27);
    }
    uxpaxs_(cside, &c__2, (ftnlen)1);
    nn = 1;
    ux[nn - 1] = 0.f;
    date13_(jd0, &iy0, &mo0, &id0);
    i__1 = *nd;
    for (i__ = 1; i__ <= i__1; ++i__) {
	datef3_(&i__, &iy0, &mo0, &id0, &iyi, &moi, &idi);
	date32_(&iyi, &moi, &idi, &itdi);
	iyl = ndyear_(&iyi);
	if (itdi == iyl || i__ == *nd) {
	    ++nn;
	    if (nn > 50) {
		msgdmp_("E", "UCXAYR", "WORKING AREA IS NOT ENOUGH.", (ftnlen)
			1, (ftnlen)6, (ftnlen)27);
	    }
	    ux[nn - 1] = (real) i__;
	    ici__1.icierr = 0;
	    ici__1.icirnum = 1;
	    ici__1.icirlen = 4;
	    ici__1.iciunit = ch + (nn - 2 << 2);
	    ici__1.icifmt = "(I4)";
	    s_wsfi(&ici__1);
	    do_fio(&c__1, (char *)&iyi, (ftnlen)sizeof(integer));
	    e_wsfi();
	    cladj_(ch + (nn - 2 << 2), (ftnlen)4);
	}
/* L10: */
    }
    sgqvpt_(&vx1, &vx2, &vy1, &vy2);
    uzrget_("RSIZEL2", &rsize, (ftnlen)7);
    nchx = (vx2 - vx1) / rsize;
/* Writing concatenation */
    i__2[0] = 6, a__1[0] = "ICENTX";
    i__2[1] = 1, a__1[1] = cside;
    s_cat(ch__1, a__1, i__2, &c__2, (ftnlen)7);
    uziget_(ch__1, &icent, (ftnlen)7);
/* Writing concatenation */
    i__2[0] = 6, a__1[0] = "IROTLX";
    i__2[1] = 1, a__1[1] = cside;
    s_cat(ch__1, a__1, i__2, &c__2, (ftnlen)7);
    uziget_(ch__1, &irota, (ftnlen)7);
/* Writing concatenation */
    i__2[0] = 6, a__1[0] = "IROTCX";
    i__2[1] = 1, a__1[1] = cside;
    s_cat(ch__1, a__1, i__2, &c__2, (ftnlen)7);
    uziget_(ch__1, &irotc, (ftnlen)7);
    uzlget_("LBTWN", &lbtwn, (ftnlen)5);
/* Writing concatenation */
    i__2[0] = 6, a__1[0] = "ICENTX";
    i__2[1] = 1, a__1[1] = cside;
    s_cat(ch__1, a__1, i__2, &c__2, (ftnlen)7);
    uziset_(ch__1, &c__0, (ftnlen)7);
    if (nn << 2 <= nchx) {
/* Writing concatenation */
	i__2[0] = 6, a__1[0] = "IROTLX";
	i__2[1] = 1, a__1[1] = cside;
	s_cat(ch__1, a__1, i__2, &c__2, (ftnlen)7);
	uziset_(ch__1, &irotc, (ftnlen)7);
    } else {
/* Writing concatenation */
	i__2[0] = 6, a__1[0] = "IROTLX";
	i__2[1] = 1, a__1[1] = cside;
	s_cat(ch__1, a__1, i__2, &c__2, (ftnlen)7);
	uziset_(ch__1, &c__1, (ftnlen)7);
    }
    uzlset_("LBTWN", &c_true, (ftnlen)5);
    uzrget_("RSIZET2", &rtick2, (ftnlen)7);
    r__1 = rtick2 * 1.5f;
    uzrset_("RSIZET2", &r__1, (ftnlen)7);
    uxptmk_(cside, &c__2, ux, &nn, (ftnlen)1);
    uzrset_("RSIZET2", &rtick2, (ftnlen)7);
/* Writing concatenation */
    i__2[0] = 6, a__1[0] = "LABELX";
    i__2[1] = 1, a__1[1] = cside;
    s_cat(ch__1, a__1, i__2, &c__2, (ftnlen)7);
    uzlget_(ch__1, &label, (ftnlen)7);
    if (label) {
	uxplbl_(cside, &c__2, ux, ch, &c__4, &nn, (ftnlen)1, (ftnlen)4);
    }
/* Writing concatenation */
    i__2[0] = 6, a__1[0] = "ICENTX";
    i__2[1] = 1, a__1[1] = cside;
    s_cat(ch__1, a__1, i__2, &c__2, (ftnlen)7);
    uziset_(ch__1, &icent, (ftnlen)7);
/* Writing concatenation */
    i__2[0] = 6, a__1[0] = "IROTLX";
    i__2[1] = 1, a__1[1] = cside;
    s_cat(ch__1, a__1, i__2, &c__2, (ftnlen)7);
    uziset_(ch__1, &irota, (ftnlen)7);
    uzlset_("LBTWN", &lbtwn, (ftnlen)5);
    return 0;
} /* ucxayr_ */


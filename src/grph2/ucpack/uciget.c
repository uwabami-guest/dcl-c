/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     UCIGET / UCISET / UCISTX */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uciget_0_(int n__, char *cp, integer *ipara, ftnlen 
	cp_len)
{
    static char cl[40];
    static integer ip;
    static char cx[8];
    static integer idx;
    extern /* Subroutine */ int uciqid_(char *, integer *, ftnlen), uciqcl_(
	    integer *, char *, ftnlen), uciqcp_(integer *, char *, ftnlen), 
	    rliget_(char *, integer *, integer *, ftnlen), rtiget_(char *, 
	    char *, integer *, integer *, ftnlen, ftnlen), uciqvl_(integer *, 
	    integer *), ucisvl_(integer *, integer *);

    switch(n__) {
	case 1: goto L_uciset;
	case 2: goto L_ucistx;
	}

    uciqid_(cp, &idx, cp_len);
    uciqvl_(&idx, ipara);
    return 0;
/* ----------------------------------------------------------------------- */

L_uciset:
    uciqid_(cp, &idx, cp_len);
    ucisvl_(&idx, ipara);
    return 0;
/* ----------------------------------------------------------------------- */

L_ucistx:
    ip = *ipara;
    uciqid_(cp, &idx, cp_len);
/*     / SHORT NAME / */
    uciqcp_(&idx, cx, (ftnlen)8);
    rtiget_("UC", cx, &ip, &c__1, (ftnlen)2, (ftnlen)8);
/*     / LONG NAME / */
    uciqcl_(&idx, cl, (ftnlen)40);
    rliget_(cl, &ip, &c__1, (ftnlen)40);
    ucisvl_(&idx, &ip);
    return 0;
} /* uciget_ */

/* Subroutine */ int uciget_(char *cp, integer *ipara, ftnlen cp_len)
{
    return uciget_0_(0, cp, ipara, cp_len);
    }

/* Subroutine */ int uciset_(char *cp, integer *ipara, ftnlen cp_len)
{
    return uciget_0_(1, cp, ipara, cp_len);
    }

/* Subroutine */ int ucistx_(char *cp, integer *ipara, ftnlen cp_len)
{
    return uciget_0_(2, cp, ipara, cp_len);
    }


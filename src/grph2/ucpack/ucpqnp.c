/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__3 = 3;

/* ----------------------------------------------------------------------- */
/*     UCPQNP / UCPQID / UCPQCP / UCPQVL / UCPSVL */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int ucpqnp_0_(int n__, integer *ncp, char *cp, integer *idx, 
	integer *itp, integer *ipara, integer *in, ftnlen cp_len)
{
    /* Initialized data */

    static char cparas[8*5] = "NDAY    " "NCHAR   " "LOWER   " "DFACT   " 
	    "IUNDEF  ";
    static integer itype[5] = { 1,1,2,3,1 };
    static char cparal[40*5] = "DAY_INTERVAL                            " 
	    "MONTH_NAME_LENGTH                       " "MONTH_NAME_LOWER    "
	    "                    " "****DFACT                               " 
	    "----IUNDEF                              ";

    /* System generated locals */
    address a__1[3];
    integer i__1[3];

    /* Builtin functions */
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen),
	     s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer n, id;
    extern integer lenc_(char *, ftnlen);
    static char cmsg[80];
    extern logical lchreq_(char *, char *, ftnlen, ftnlen);
    extern /* Subroutine */ int uciqid_(char *, integer *, ftnlen), uclqid_(
	    char *, integer *, ftnlen), msgdmp_(char *, char *, char *, 
	    ftnlen, ftnlen, ftnlen), ucrqid_(char *, integer *, ftnlen), 
	    uciqvl_(integer *, integer *), uclqvl_(integer *, integer *), 
	    ucisvl_(integer *, integer *), uclsvl_(integer *, integer *), 
	    ucrqvl_(integer *, integer *), ucrsvl_(integer *, integer *);

/*     / SHORT NAME / */
    switch(n__) {
	case 1: goto L_ucpqid;
	case 2: goto L_ucpqcp;
	case 3: goto L_ucpqcl;
	case 4: goto L_ucpqit;
	case 5: goto L_ucpqvl;
	case 6: goto L_ucpsvl;
	case 7: goto L_ucpqin;
	}

/*     / LONG  NAME / */
    *ncp = 5;
    return 0;
/* ----------------------------------------------------------------------- */

L_ucpqid:
    for (n = 1; n <= 5; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *idx = n;
	    return 0;
	}
/* L10: */
    }
/* Writing concatenation */
    i__1[0] = 11, a__1[0] = "PARAMETER '";
    i__1[1] = lenc_(cp, cp_len), a__1[1] = cp;
    i__1[2] = 17, a__1[2] = "' IS NOT DEFINED.";
    s_cat(cmsg, a__1, i__1, &c__3, (ftnlen)80);
    msgdmp_("E", "UCPQID", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
    return 0;
/* ----------------------------------------------------------------------- */

L_ucpqcp:
    if (1 <= *idx && *idx <= 5) {
	s_copy(cp, cparas + (*idx - 1 << 3), cp_len, (ftnlen)8);
    } else {
	msgdmp_("E", "UCPQCP", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_ucpqcl:
    if (1 <= *idx && *idx <= 5) {
	s_copy(cp, cparal + (*idx - 1) * 40, cp_len, (ftnlen)40);
    } else {
	msgdmp_("E", "UCPQCL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_ucpqit:
    if (1 <= *idx && *idx <= 5) {
	*itp = itype[*idx - 1];
    } else {
	msgdmp_("E", "UCPQIT", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_ucpqvl:
    if (1 <= *idx && *idx <= 5) {
	if (itype[*idx - 1] == 1) {
	    uciqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    uciqvl_(&id, ipara);
	} else if (itype[*idx - 1] == 2) {
	    uclqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    uclqvl_(&id, ipara);
	} else if (itype[*idx - 1] == 3) {
	    ucrqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    ucrqvl_(&id, ipara);
	}
    } else {
	msgdmp_("E", "UCPQVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_ucpsvl:
    if (1 <= *idx && *idx <= 5) {
	if (itype[*idx - 1] == 1) {
	    uciqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    ucisvl_(&id, ipara);
	} else if (itype[*idx - 1] == 2) {
	    uclqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    uclsvl_(&id, ipara);
	} else if (itype[*idx - 1] == 3) {
	    ucrqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    ucrsvl_(&id, ipara);
	}
    } else {
	msgdmp_("E", "UCPSVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_ucpqin:
    for (n = 1; n <= 5; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *in = n;
	    return 0;
	}
/* L20: */
    }
    *in = 0;
    return 0;
} /* ucpqnp_ */

/* Subroutine */ int ucpqnp_(integer *ncp)
{
    return ucpqnp_0_(0, ncp, (char *)0, (integer *)0, (integer *)0, (integer *
	    )0, (integer *)0, (ftnint)0);
    }

/* Subroutine */ int ucpqid_(char *cp, integer *idx, ftnlen cp_len)
{
    return ucpqnp_0_(1, (integer *)0, cp, idx, (integer *)0, (integer *)0, (
	    integer *)0, cp_len);
    }

/* Subroutine */ int ucpqcp_(integer *idx, char *cp, ftnlen cp_len)
{
    return ucpqnp_0_(2, (integer *)0, cp, idx, (integer *)0, (integer *)0, (
	    integer *)0, cp_len);
    }

/* Subroutine */ int ucpqcl_(integer *idx, char *cp, ftnlen cp_len)
{
    return ucpqnp_0_(3, (integer *)0, cp, idx, (integer *)0, (integer *)0, (
	    integer *)0, cp_len);
    }

/* Subroutine */ int ucpqit_(integer *idx, integer *itp)
{
    return ucpqnp_0_(4, (integer *)0, (char *)0, idx, itp, (integer *)0, (
	    integer *)0, (ftnint)0);
    }

/* Subroutine */ int ucpqvl_(integer *idx, integer *ipara)
{
    return ucpqnp_0_(5, (integer *)0, (char *)0, idx, (integer *)0, ipara, (
	    integer *)0, (ftnint)0);
    }

/* Subroutine */ int ucpsvl_(integer *idx, integer *ipara)
{
    return ucpqnp_0_(6, (integer *)0, (char *)0, idx, (integer *)0, ipara, (
	    integer *)0, (ftnint)0);
    }

/* Subroutine */ int ucpqin_(char *cp, integer *in, ftnlen cp_len)
{
    return ucpqnp_0_(7, (integer *)0, cp, (integer *)0, (integer *)0, (
	    integer *)0, in, cp_len);
    }


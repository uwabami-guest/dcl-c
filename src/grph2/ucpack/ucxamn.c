/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__2 = 2;
static integer c__0 = 0;
static logical c_true = TRUE_;
static integer c__9 = 9;

/* ----------------------------------------------------------------------- */
/*     UCXAMN : PLOT MONTH AXIS */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int ucxamn_(char *cside, integer *jd0, integer *nd, ftnlen 
	cside_len)
{
    /* System generated locals */
    address a__1[2];
    integer i__1, i__2[2];
    char ch__1[9], ch__2[7];

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen), s_cat(char *,
	     char **, integer *, integer *, ftnlen);

    /* Local variables */
    static integer i__;
    static char ch[9*50];
    static integer mc, nc, nn;
    static real ux[50];
    static integer id0, mo0, iy0, idi, iml, moi, iyi, itdi;
    extern /* Character */ VOID cmon_(char *, ftnlen, integer *);
    static logical label;
    extern /* Subroutine */ int date13_(integer *, integer *, integer *, 
	    integer *), date32_(integer *, integer *, integer *, integer *);
    static integer icent;
    extern integer ndmon_(integer *, integer *);
    static integer irota, irotc;
    static logical lbtwn;
    extern /* Subroutine */ int datef3_(integer *, integer *, integer *, 
	    integer *, integer *, integer *, integer *);
    extern integer nucchr_(char *, integer *, ftnlen);
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), clower_(char *, ftnlen);
    extern logical luxchk_(char *, ftnlen);
    extern /* Subroutine */ int uxplbl_(char *, integer *, real *, char *, 
	    integer *, integer *, ftnlen, ftnlen), uziget_(char *, integer *, 
	    ftnlen), uzlget_(char *, logical *, ftnlen), uziset_(char *, 
	    integer *, ftnlen), uzlset_(char *, logical *, ftnlen), uxpaxs_(
	    char *, integer *, ftnlen), uxptmk_(char *, integer *, real *, 
	    integer *, ftnlen);

    if (! luxchk_(cside, (ftnlen)1)) {
	msgdmp_("E", "UCXAMN", "SIDE PARAMETER IS INVALID.", (ftnlen)1, (
		ftnlen)6, (ftnlen)26);
    }
    if (*jd0 < 0) {
	msgdmp_("E", "UCXAMN", "FIRST DATE IS LESS THAN 0.", (ftnlen)1, (
		ftnlen)6, (ftnlen)26);
    }
    if (*nd <= 0) {
	msgdmp_("E", "UCXAMN", "DATE LENGTH IS LESS THAN 0.", (ftnlen)1, (
		ftnlen)6, (ftnlen)27);
    }
    nc = nucchr_("X", nd, (ftnlen)1);
    mc = abs(nc);
    if (mc == 0) {
	msgdmp_("W", "UCXAMN", "NO MONTH-AXIS.", (ftnlen)1, (ftnlen)6, (
		ftnlen)14);
	return 0;
    }
    uxpaxs_(cside, &c__2, (ftnlen)1);
    nn = 1;
    ux[nn - 1] = 0.f;
    date13_(jd0, &iy0, &mo0, &id0);
    i__1 = *nd;
    for (i__ = 1; i__ <= i__1; ++i__) {
	datef3_(&i__, &iy0, &mo0, &id0, &iyi, &moi, &idi);
	date32_(&iyi, &moi, &idi, &itdi);
	iml = ndmon_(&iyi, &moi);
	if (idi == iml || i__ == *nd) {
	    ++nn;
	    if (nn > 50) {
		msgdmp_("E", "UCXAMN", "WORKING AREA IS NOT ENOUGH.", (ftnlen)
			1, (ftnlen)6, (ftnlen)27);
	    }
	    ux[nn - 1] = (real) i__;
	    s_copy(ch + (nn - 2) * 9, " ", (ftnlen)9, (ftnlen)1);
	    cmon_(ch__1, (ftnlen)9, &moi);
	    s_copy(ch + (nn - 2) * 9, ch__1, mc, (ftnlen)9);
	    if (nc < 0 && mc >= 2) {
		clower_(ch + ((nn - 2) * 9 + 1), mc - 1);
	    }
	}
/* L10: */
    }
/* Writing concatenation */
    i__2[0] = 6, a__1[0] = "ICENTX";
    i__2[1] = 1, a__1[1] = cside;
    s_cat(ch__2, a__1, i__2, &c__2, (ftnlen)7);
    uziget_(ch__2, &icent, (ftnlen)7);
/* Writing concatenation */
    i__2[0] = 6, a__1[0] = "IROTLX";
    i__2[1] = 1, a__1[1] = cside;
    s_cat(ch__2, a__1, i__2, &c__2, (ftnlen)7);
    uziget_(ch__2, &irota, (ftnlen)7);
/* Writing concatenation */
    i__2[0] = 6, a__1[0] = "IROTCX";
    i__2[1] = 1, a__1[1] = cside;
    s_cat(ch__2, a__1, i__2, &c__2, (ftnlen)7);
    uziget_(ch__2, &irotc, (ftnlen)7);
    uzlget_("LBTWN", &lbtwn, (ftnlen)5);
/* Writing concatenation */
    i__2[0] = 6, a__1[0] = "ICENTX";
    i__2[1] = 1, a__1[1] = cside;
    s_cat(ch__2, a__1, i__2, &c__2, (ftnlen)7);
    uziset_(ch__2, &c__0, (ftnlen)7);
/* Writing concatenation */
    i__2[0] = 6, a__1[0] = "IROTLX";
    i__2[1] = 1, a__1[1] = cside;
    s_cat(ch__2, a__1, i__2, &c__2, (ftnlen)7);
    uziset_(ch__2, &irotc, (ftnlen)7);
    uzlset_("LBTWN", &c_true, (ftnlen)5);
    uxptmk_(cside, &c__2, ux, &nn, (ftnlen)1);
/* Writing concatenation */
    i__2[0] = 6, a__1[0] = "LABELX";
    i__2[1] = 1, a__1[1] = cside;
    s_cat(ch__2, a__1, i__2, &c__2, (ftnlen)7);
    uzlget_(ch__2, &label, (ftnlen)7);
    if (label) {
	uxplbl_(cside, &c__2, ux, ch, &c__9, &nn, (ftnlen)1, (ftnlen)9);
    }
/* Writing concatenation */
    i__2[0] = 6, a__1[0] = "ICENTX";
    i__2[1] = 1, a__1[1] = cside;
    s_cat(ch__2, a__1, i__2, &c__2, (ftnlen)7);
    uziset_(ch__2, &icent, (ftnlen)7);
/* Writing concatenation */
    i__2[0] = 6, a__1[0] = "IROTLX";
    i__2[1] = 1, a__1[1] = cside;
    s_cat(ch__2, a__1, i__2, &c__2, (ftnlen)7);
    uziset_(ch__2, &irota, (ftnlen)7);
    uzlset_("LBTWN", &lbtwn, (ftnlen)5);
    return 0;
} /* ucxamn_ */


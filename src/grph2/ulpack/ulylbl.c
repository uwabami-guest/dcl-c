/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int ulylbl_0_(int n__, real *bl, integer *nbl, integer *inum)
{
    /* Initialized data */

    static real ybl[40]	/* was [10][4] */ = { 1.f,10.f,0.f,0.f,0.f,0.f,0.f,
	    0.f,0.f,0.f,1.f,2.f,10.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,1.f,2.f,5.f,
	    10.f,0.f,0.f,0.f,0.f,0.f,0.f,1.f,2.f,3.f,4.f,5.f,6.f,7.f,8.f,9.f,
	    10.f };
    static integer nybl[4] = { 1,2,3,9 };

    /* System generated locals */
    integer i__1;

    /* Local variables */
    static integer ibl;

/*     LABEL BUFFER FOR Y-AXIS */
    /* Parameter adjustments */
    --bl;

    /* Function Body */
    switch(n__) {
	case 1: goto L_ulsybl;
	case 2: goto L_ulqybl;
	}

    i__1 = nybl[(0 + (0 + (*inum - 1 << 2))) / 4] + 1;
    for (ibl = 1; ibl <= i__1; ++ibl) {
	bl[ibl] = ybl[ibl + *inum * 10 - 11];
/* L10: */
    }
    *nbl = nybl[*inum - 1];
    return 0;
/* ----------------------------------------------------------------------- */

L_ulsybl:
    nybl[3] = *nbl;
    i__1 = *nbl;
    for (ibl = 1; ibl <= i__1; ++ibl) {
	ybl[ibl + 29] = bl[ibl];
/* L20: */
    }
    ybl[*nbl + 30] = 10.f;
    return 0;
/* ----------------------------------------------------------------------- */

L_ulqybl:
    *nbl = nybl[3];
    i__1 = *nbl;
    for (ibl = 1; ibl <= i__1; ++ibl) {
	bl[ibl] = ybl[ibl + 29];
/* L30: */
    }
    return 0;
} /* ulylbl_ */

/* Subroutine */ int ulylbl_(real *bl, integer *nbl, integer *inum)
{
    return ulylbl_0_(0, bl, nbl, inum);
    }

/* Subroutine */ int ulsybl_(real *bl, integer *nbl)
{
    return ulylbl_0_(1, bl, nbl, (integer *)0);
    }

/* Subroutine */ int ulqybl_(real *bl, integer *nbl)
{
    return ulylbl_0_(2, bl, nbl, (integer *)0);
    }


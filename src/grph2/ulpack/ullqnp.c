/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__3 = 3;

/* ----------------------------------------------------------------------- */
/*     ULLQNP / ULLQID / ULLQCP / ULLQVL / ULLSVL */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int ullqnp_0_(int n__, integer *ncp, char *cp, integer *idx, 
	logical *lpara, integer *in, ftnlen cp_len)
{
    /* System generated locals */
    address a__1[3];
    integer i__1[3];

    /* Builtin functions */
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen);

    /* Local variables */
    extern integer lenc_(char *, ftnlen);
    static char cmsg[80];
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen);

    switch(n__) {
	case 1: goto L_ullqid;
	case 2: goto L_ullqcp;
	case 3: goto L_ullqcl;
	case 4: goto L_ullqvl;
	case 5: goto L_ullsvl;
	case 6: goto L_ullqin;
	}

    *ncp = 0;
    return 0;
/* ----------------------------------------------------------------------- */

L_ullqid:
    *idx = 0;
/* Writing concatenation */
    i__1[0] = 11, a__1[0] = "PARAMETER '";
    i__1[1] = lenc_(cp, cp_len), a__1[1] = cp;
    i__1[2] = 17, a__1[2] = "' IS NOT DEFINED.";
    s_cat(cmsg, a__1, i__1, &c__3, (ftnlen)80);
    msgdmp_("E", "ULLQID", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
    return 0;
/* ----------------------------------------------------------------------- */

L_ullqcp:
    msgdmp_("E", "ULLQCP", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
	    ftnlen)20);
    return 0;
/* ----------------------------------------------------------------------- */

L_ullqcl:
    msgdmp_("E", "ULLQCL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
	    ftnlen)20);
    return 0;
/* ----------------------------------------------------------------------- */

L_ullqvl:
    *lpara = FALSE_;
    msgdmp_("E", "ULLQVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
	    ftnlen)20);
    return 0;
/* ----------------------------------------------------------------------- */

L_ullsvl:
    msgdmp_("E", "ULLSVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
	    ftnlen)20);
    return 0;
/* ----------------------------------------------------------------------- */

L_ullqin:
    *in = 0;
    return 0;
} /* ullqnp_ */

/* Subroutine */ int ullqnp_(integer *ncp)
{
    return ullqnp_0_(0, ncp, (char *)0, (integer *)0, (logical *)0, (integer *
	    )0, (ftnint)0);
    }

/* Subroutine */ int ullqid_(char *cp, integer *idx, ftnlen cp_len)
{
    return ullqnp_0_(1, (integer *)0, cp, idx, (logical *)0, (integer *)0, 
	    cp_len);
    }

/* Subroutine */ int ullqcp_(integer *idx, char *cp, ftnlen cp_len)
{
    return ullqnp_0_(2, (integer *)0, cp, idx, (logical *)0, (integer *)0, 
	    cp_len);
    }

/* Subroutine */ int ullqcl_(integer *idx, char *cp, ftnlen cp_len)
{
    return ullqnp_0_(3, (integer *)0, cp, idx, (logical *)0, (integer *)0, 
	    cp_len);
    }

/* Subroutine */ int ullqvl_(integer *idx, logical *lpara)
{
    return ullqnp_0_(4, (integer *)0, (char *)0, idx, lpara, (integer *)0, (
	    ftnint)0);
    }

/* Subroutine */ int ullsvl_(integer *idx, logical *lpara)
{
    return ullqnp_0_(5, (integer *)0, (char *)0, idx, lpara, (integer *)0, (
	    ftnint)0);
    }

/* Subroutine */ int ullqin_(char *cp, integer *in, ftnlen cp_len)
{
    return ullqnp_0_(6, (integer *)0, cp, (integer *)0, (logical *)0, in, 
	    cp_len);
    }


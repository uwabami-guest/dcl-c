/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static real c_b13 = 1.f;
static logical c_true = TRUE_;
static integer c__10 = 10;
static integer c__1 = 1;
static real c_b29 = 10.f;
static integer c__3 = 3;
static integer c__2 = 2;
static integer c__16 = 16;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int ulylog_(char *cside, integer *nlbl, integer *nticks, 
	ftnlen cside_len)
{
    /* System generated locals */
    address a__1[3], a__2[2];
    integer i__1, i__2, i__3, i__4[3], i__5[2];
    real r__1, r__2;
    char ch__1[3], ch__2[3], ch__3[7];
    icilist ici__1;

    /* Builtin functions */
    double r_sign(real *, real *), pow_ri(real *, integer *);
    integer s_wsfi(icilist *), do_fio(integer *, char *, ftnlen), e_wsfi(void)
	    ;
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen), s_cat(char *,
	     char **, integer *, integer *, ftnlen);

    /* Local variables */
    static char ch[18*50];
    static integer ib;
    static real bl[10];
    static integer nb;
    static real bs[10];
    static integer ip;
    static real uy1[200], uy2[50];
    static integer nbl;
    static char chr[8], chw[16];
    static integer nbs, nbt;
    static real sgn, uyt[50], yyy;
    extern /* Subroutine */ int gnge_(real *, real *, integer *), gnle_(real *
	    , real *, integer *);
    static logical loff;
    static char cfmt[16];
    extern /* Character */ VOID usgi_(char *, ftnlen, integer *);
    extern logical lrgt_(real *, real *);
    extern integer lenz_(char *, ftnlen);
    static real ymin;
    extern logical lrlt_(real *, real *);
    static real ymax;
    static integer isup, irst;
    extern /* Subroutine */ int cladj_(char *, ftnlen);
    static logical label;
    extern /* Subroutine */ int chval_(char *, real *, char *, ftnlen, ftnlen)
	    ;
    static logical lcntl;
    static integer ipmin, iychr;
    static logical lepsl;
    static real bymax;
    static integer ipmax;
    static real bymin;
    static logical lsfnt;
    static integer itype;
    extern /* Subroutine */ int vrgnn_(real *, integer *, integer *);
    static integer jtype;
    static real uxmin, uymin, uxmax, uymax, factor;
    extern /* Subroutine */ int gllget_(char *, logical *, ftnlen), gnsblk_(
	    real *, integer *), sgiget_(char *, integer *, ftnlen), gnsave_(
	    void), sglget_(char *, logical *, ftnlen), msgdmp_(char *, char *,
	     char *, ftnlen, ftnlen, ftnlen), uliget_(char *, integer *, 
	    ftnlen), gllset_(char *, logical *, ftnlen);
    extern logical luychk_(char *, ftnlen);
    extern /* Subroutine */ int uzcget_(char *, char *, ftnlen, ftnlen), 
	    gnrset_(void), sgqwnd_(real *, real *, real *, real *), ulylbl_(
	    real *, integer *, integer *), sgswnd_(real *, real *, real *, 
	    real *), swlget_(char *, logical *, ftnlen), uyplbl_(char *, 
	    integer *, real *, char *, integer *, integer *, ftnlen, ftnlen), 
	    sgstrf_(void), uzlget_(char *, logical *, ftnlen), uzrget_(char *,
	     real *, ftnlen), uypaxs_(char *, integer *, ftnlen), uyptmk_(
	    char *, integer *, real *, integer *, ftnlen);

    /* Fortran I/O blocks */
    static icilist io___36 = { 0, chr, 0, "(I8)", 8, 1 };


/*     CSIDE : 'L','R','U' */
/*     NLBL  : 1-4          ... Label buffer number used in the axis */
/*     NTICKS: 1-9          ... Number of small ticks in 10**N-10**(N+1) */
    if (! luychk_(cside, (ftnlen)1)) {
	msgdmp_("E", "ULYLOG", "INVALID CSIDE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)14);
    }
    if (*nlbl < 1 || *nlbl > 4) {
	msgdmp_("E", "ULYLOG", "INVALID NLBL.", (ftnlen)1, (ftnlen)6, (ftnlen)
		13);
    }
    if (*nticks < 1 || *nticks > 9) {
	msgdmp_("E", "ULYLOG", "INVALID NTICKS.", (ftnlen)1, (ftnlen)6, (
		ftnlen)15);
    }
    sgqwnd_(&uxmin, &uxmax, &uymin, &uymax);
    uzlget_("LOFFSET", &loff, (ftnlen)7);
    if (loff) {
	uzrget_("YFACT", &factor, (ftnlen)5);
	ymin = uymin / factor;
	ymax = uymax / factor;
	sgswnd_(&uxmin, &uxmax, &ymin, &ymax);
	sgstrf_();
    } else {
	ymin = uymin;
	ymax = uymax;
    }
    sgn = r_sign(&c_b13, &ymin);
    if (sgn * ymin > sgn * ymax) {
	yyy = ymin;
	ymin = ymax;
	ymax = yyy;
    }
    uliget_("IYTYPE", &itype, (ftnlen)6);
    uliget_("IYCHR", &iychr, (ftnlen)5);
    ulylbl_(bl, &nb, nlbl);
    sgiget_("ISUP", &isup, (ftnlen)4);
    sgiget_("IRST", &irst, (ftnlen)4);
    gllget_("LEPSL", &lepsl, (ftnlen)5);
    sglget_("LCNTL", &lcntl, (ftnlen)5);
    swlget_("LSYSFNT", &lsfnt, (ftnlen)7);
    gllset_("LEPSL", &c_true, (ftnlen)5);
    gnsave_();
/*     SMALL TICKS */
    vrgnn_(bs, &c__10, &c__1);
    bs[*nticks] = 10.f;
    i__1 = *nticks + 1;
    gnsblk_(bs, &i__1);
    r__1 = abs(ymax);
    gnle_(&r__1, &bymax, &ipmax);
    r__1 = abs(ymin);
    gnge_(&r__1, &bymin, &ipmin);
    nbs = 0;
    i__1 = ipmax;
    for (ip = ipmin; ip <= i__1; ++ip) {
	i__2 = *nticks;
	for (ib = 1; ib <= i__2; ++ib) {
	    if (ip == ipmin && lrlt_(&bs[ib - 1], &bymin)) {
		goto L100;
	    }
	    if (ip == ipmax && lrgt_(&bs[ib - 1], &bymax)) {
		goto L100;
	    }
	    ++nbs;
	    if (nbs > 200) {
		msgdmp_("E", "ULYLOG", "TOO MANY TICKS.", (ftnlen)1, (ftnlen)
			6, (ftnlen)15);
	    }
	    uy1[nbs - 1] = sgn * bs[ib - 1] * pow_ri(&c_b29, &ip);
L100:
	    ;
	}
    }
/*     LARGE LABELS AND TICKS */
    i__2 = nb + 1;
    gnsblk_(bl, &i__2);
    r__1 = abs(ymax);
    gnle_(&r__1, &bymax, &ipmax);
    r__1 = abs(ymin);
    gnge_(&r__1, &bymin, &ipmin);
    nbl = 0;
    nbt = 0;
    jtype = itype % 2;
    i__2 = ipmax;
    for (ip = ipmin; ip <= i__2; ++ip) {
	i__1 = nb;
	for (ib = 1; ib <= i__1; ++ib) {
	    if (ip == ipmin && lrlt_(&bl[ib - 1], &bymin)) {
		goto L201;
	    }
	    if (ip == ipmax && lrgt_(&bl[ib - 1], &bymax)) {
		goto L201;
	    }
	    if (ib == 1) {
		++nbt;
		uyt[nbt - 1] = sgn * pow_ri(&c_b29, &ip);
	    }
	    ++nbl;
	    if (nbl > 50) {
		msgdmp_("E", "ULYLOG", "TOO MANY LABELS.", (ftnlen)1, (ftnlen)
			6, (ftnlen)16);
	    }
	    uy2[nbl - 1] = sgn * bl[ib - 1] * pow_ri(&c_b29, &ip);
	    if (itype <= 2) {
		if (jtype == 0 && ib != 1) {
		    ici__1.icierr = 0;
		    ici__1.icirnum = 1;
		    ici__1.icirlen = 18;
		    ici__1.iciunit = ch + (nbl - 1) * 18;
		    ici__1.icifmt = "(I1)";
		    s_wsfi(&ici__1);
		    i__3 = (integer) bl[ib - 1];
		    do_fio(&c__1, (char *)&i__3, (ftnlen)sizeof(integer));
		    e_wsfi();
		} else {
		    if (jtype == 1 && nb != 1) {
			ici__1.icierr = 0;
			ici__1.icirnum = 1;
			ici__1.icirlen = 18;
			ici__1.iciunit = ch + (nbl - 1) * 18;
			ici__1.icifmt = "(I1,A3)";
			s_wsfi(&ici__1);
			i__3 = (integer) bl[ib - 1];
			do_fio(&c__1, (char *)&i__3, (ftnlen)sizeof(integer));
			usgi_(ch__2, (ftnlen)3, &iychr);
			s_copy(ch__1, ch__2, (ftnlen)3, (ftnlen)3);
			do_fio(&c__1, ch__1, (ftnlen)3);
			e_wsfi();
		    } else {
			s_copy(ch + (nbl - 1) * 18, " ", (ftnlen)18, (ftnlen)
				1);
		    }
		    s_wsfi(&io___36);
		    do_fio(&c__1, (char *)&ip, (ftnlen)sizeof(integer));
		    e_wsfi();
		    cladj_(chr, (ftnlen)8);
/* Writing concatenation */
		    i__4[0] = 5, a__1[0] = "10\\^{";
		    i__4[1] = lenz_(chr, (ftnlen)8), a__1[1] = chr;
		    i__4[2] = 1, a__1[2] = "}";
		    s_cat(ch + ((nbl - 1) * 18 + 4), a__1, i__4, &c__3, (
			    ftnlen)14);
		    cladj_(ch + (nbl - 1) * 18, (ftnlen)18);
		}
	    } else {
		if (jtype == 0 && ib != 1) {
		    ici__1.icierr = 0;
		    ici__1.icirnum = 1;
		    ici__1.icirlen = 18;
		    ici__1.iciunit = ch + (nbl - 1) * 18;
		    ici__1.icifmt = "(I1)";
		    s_wsfi(&ici__1);
		    i__3 = (integer) bl[ib - 1];
		    do_fio(&c__1, (char *)&i__3, (ftnlen)sizeof(integer));
		    e_wsfi();
		} else {
		    uzcget_("CYFMT", cfmt, (ftnlen)5, (ftnlen)16);
		    r__2 = (r__1 = uy2[nbl - 1], abs(r__1));
		    chval_(cfmt, &r__2, ch + (nbl - 1) * 18, (ftnlen)16, (
			    ftnlen)18);
		    cladj_(ch + (nbl - 1) * 18, (ftnlen)18);
		}
	    }
L201:
	    ;
	}
    }
    if (sgn < 0.f) {
	i__1 = nbl;
	for (ib = 1; ib <= i__1; ++ib) {
/* Writing concatenation */
	    i__5[0] = 1, a__2[0] = "-";
	    i__5[1] = 18, a__2[1] = ch + (ib - 1) * 18;
	    s_cat(chw, a__2, i__5, &c__2, (ftnlen)16);
	    s_copy(ch + (ib - 1) * 18, chw, (ftnlen)18, (ftnlen)16);
/* L301: */
	}
    }
/*     DRAW AXIS, TICKS, AND LABELS */
    uypaxs_(cside, &c__2, (ftnlen)1);
    if (nbs != 0) {
	uyptmk_(cside, &c__1, uy1, &nbs, (ftnlen)1);
    }
    if (nbt != 0) {
	uyptmk_(cside, &c__2, uyt, &nbt, (ftnlen)1);
    }
/* Writing concatenation */
    i__5[0] = 6, a__2[0] = "LABELY";
    i__5[1] = 1, a__2[1] = cside;
    s_cat(ch__3, a__2, i__5, &c__2, (ftnlen)7);
    uzlget_(ch__3, &label, (ftnlen)7);
    if (label) {
	uyplbl_(cside, &c__1, uy2, ch, &c__16, &nbl, (ftnlen)1, (ftnlen)18);
    }
    gllset_("LEPSL", &lepsl, (ftnlen)5);
    gnrset_();
    if (loff) {
	sgswnd_(&uxmin, &uxmax, &uymin, &uymax);
	sgstrf_();
    }
    return 0;
} /* ulylog_ */


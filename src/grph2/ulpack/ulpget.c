/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     ULPGET / ULPSET / ULPSTX */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int ulpget_0_(int n__, char *cp, integer *ipara, ftnlen 
	cp_len)
{
    static char cl[40];
    static integer ip;
    static char cx[8];
    static integer it, idx;
    extern /* Subroutine */ int rliget_(char *, integer *, integer *, ftnlen),
	     uliqid_(char *, integer *, ftnlen), rllget_(char *, integer *, 
	    integer *, ftnlen), ullqid_(char *, integer *, ftnlen), ulpqid_(
	    char *, integer *, ftnlen), rtiget_(char *, char *, integer *, 
	    integer *, ftnlen, ftnlen), ulpqcl_(integer *, char *, ftnlen), 
	    rtlget_(char *, char *, integer *, integer *, ftnlen, ftnlen), 
	    rlrget_(char *, integer *, integer *, ftnlen), ulpqcp_(integer *, 
	    char *, ftnlen), ulrqid_(char *, integer *, ftnlen), rtrget_(char 
	    *, char *, integer *, integer *, ftnlen, ftnlen), ulpqit_(integer 
	    *, integer *), ulisvl_(integer *, integer *), ullsvl_(integer *, 
	    integer *), ulpqvl_(integer *, integer *), ulpsvl_(integer *, 
	    integer *), ulrsvl_(integer *, integer *);

    switch(n__) {
	case 1: goto L_ulpset;
	case 2: goto L_ulpstx;
	}

    ulpqid_(cp, &idx, cp_len);
    ulpqvl_(&idx, ipara);
    return 0;
/* ----------------------------------------------------------------------- */

L_ulpset:
    ulpqid_(cp, &idx, cp_len);
    ulpsvl_(&idx, ipara);
    return 0;
/* ----------------------------------------------------------------------- */

L_ulpstx:
    ip = *ipara;
    ulpqid_(cp, &idx, cp_len);
    ulpqit_(&idx, &it);
    ulpqcp_(&idx, cx, (ftnlen)8);
    ulpqcl_(&idx, cl, (ftnlen)40);
    if (it == 1) {
	rtiget_("UL", cx, &ip, &c__1, (ftnlen)2, (ftnlen)8);
	rliget_(cl, &ip, &c__1, (ftnlen)40);
	uliqid_(cp, &idx, cp_len);
	ulisvl_(&idx, &ip);
    } else if (it == 2) {
	rtlget_("UL", cx, &ip, &c__1, (ftnlen)2, (ftnlen)8);
	rllget_(cl, &ip, &c__1, (ftnlen)40);
	ullqid_(cp, &idx, cp_len);
	ullsvl_(&idx, &ip);
    } else if (it == 3) {
	rtrget_("UL", cx, &ip, &c__1, (ftnlen)2, (ftnlen)8);
	rlrget_(cl, &ip, &c__1, (ftnlen)40);
	ulrqid_(cp, &idx, cp_len);
	ulrsvl_(&idx, &ip);
    }
    return 0;
} /* ulpget_ */

/* Subroutine */ int ulpget_(char *cp, integer *ipara, ftnlen cp_len)
{
    return ulpget_0_(0, cp, ipara, cp_len);
    }

/* Subroutine */ int ulpset_(char *cp, integer *ipara, ftnlen cp_len)
{
    return ulpget_0_(1, cp, ipara, cp_len);
    }

/* Subroutine */ int ulpstx_(char *cp, integer *ipara, ftnlen cp_len)
{
    return ulpget_0_(2, cp, ipara, cp_len);
    }


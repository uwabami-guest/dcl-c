/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__3 = 3;

/* ----------------------------------------------------------------------- */
/*     ULPQNP / ULPQID / ULPQCP / ULPQVL / ULPSVL */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int ulpqnp_0_(int n__, integer *ncp, char *cp, integer *idx, 
	integer *itp, integer *ipara, integer *in, ftnlen cp_len)
{
    /* Initialized data */

    static char cparas[8*4] = "IXCHR   " "IYCHR   " "IXTYPE  " "IYTYPE  ";
    static integer itype[4] = { 1,1,1,1 };
    static char cparal[40*4] = "LOG_X_LABEL_CHAR                        " 
	    "LOG_Y_LABEL_CHAR                        " "****IXTYPE          "
	    "                    " "****IYTYPE                              ";

    /* System generated locals */
    address a__1[3];
    integer i__1[3];

    /* Builtin functions */
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen),
	     s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer n, id;
    extern integer lenc_(char *, ftnlen);
    static char cmsg[80];
    extern logical lchreq_(char *, char *, ftnlen, ftnlen);
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), uliqid_(char *, integer *, ftnlen), ullqid_(char 
	    *, integer *, ftnlen), ulrqid_(char *, integer *, ftnlen), 
	    uliqvl_(integer *, integer *), ullqvl_(integer *, integer *), 
	    ulisvl_(integer *, integer *), ullsvl_(integer *, integer *), 
	    ulrqvl_(integer *, integer *), ulrsvl_(integer *, integer *);

/*     / SHORT NAME / */
    switch(n__) {
	case 1: goto L_ulpqid;
	case 2: goto L_ulpqcp;
	case 3: goto L_ulpqcl;
	case 4: goto L_ulpqit;
	case 5: goto L_ulpqvl;
	case 6: goto L_ulpsvl;
	case 7: goto L_ulpqin;
	}

/*     / LONG NAME / */
/*     IXCHR /IYCHR  : CHARACTER NUMBER OF * FOR 5*10E1 IN X/Y-AXIS */
/*     IXTYPE/IXTYPE : 1-4  1 ... 10|2" 2*10|2" 5*10|2" 10|3"  ETC */
/*                          2 ... 10|2" 2       5       10|3"  ETC */
/*                          3 ... 100   200     500     1000   ETC */
/*                          4 ... 100   2       5       1000   ETC */
/*               FORMAT FOR 3 OR 4 DEPENDS ON THAT SET BY ULSFMT */
    *ncp = 4;
    return 0;
/* ----------------------------------------------------------------------- */

L_ulpqid:
    for (n = 1; n <= 4; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *idx = n;
	    return 0;
	}
/* L10: */
    }
/* Writing concatenation */
    i__1[0] = 11, a__1[0] = "PARAMETER '";
    i__1[1] = lenc_(cp, cp_len), a__1[1] = cp;
    i__1[2] = 17, a__1[2] = "' IS NOT DEFINED.";
    s_cat(cmsg, a__1, i__1, &c__3, (ftnlen)80);
    msgdmp_("E", "ULPQID", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
    return 0;
/* ----------------------------------------------------------------------- */

L_ulpqcp:
    if (1 <= *idx && *idx <= 4) {
	s_copy(cp, cparas + (*idx - 1 << 3), cp_len, (ftnlen)8);
    } else {
	msgdmp_("E", "ULPQCP", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_ulpqcl:
    if (1 <= *idx && *idx <= 4) {
	s_copy(cp, cparal + (*idx - 1) * 40, cp_len, (ftnlen)40);
    } else {
	msgdmp_("E", "ULPQCL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_ulpqit:
    if (1 <= *idx && *idx <= 4) {
	*itp = itype[*idx - 1];
    } else {
	msgdmp_("E", "ULPQIT", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_ulpqvl:
    if (1 <= *idx && *idx <= 4) {
	if (itype[*idx - 1] == 1) {
	    uliqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    uliqvl_(&id, ipara);
	} else if (itype[*idx - 1] == 2) {
	    ullqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    ullqvl_(&id, ipara);
	} else if (itype[*idx - 1] == 3) {
	    ulrqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    ulrqvl_(&id, ipara);
	}
    } else {
	msgdmp_("E", "ULPQVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_ulpsvl:
    if (1 <= *idx && *idx <= 4) {
	if (itype[*idx - 1] == 1) {
	    uliqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    ulisvl_(&id, ipara);
	} else if (itype[*idx - 1] == 2) {
	    ullqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    ullsvl_(&id, ipara);
	} else if (itype[*idx - 1] == 3) {
	    ulrqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    ulrsvl_(&id, ipara);
	}
    } else {
	msgdmp_("E", "ULPSVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_ulpqin:
    for (n = 1; n <= 4; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *in = n;
	    return 0;
	}
/* L20: */
    }
    *in = 0;
    return 0;
} /* ulpqnp_ */

/* Subroutine */ int ulpqnp_(integer *ncp)
{
    return ulpqnp_0_(0, ncp, (char *)0, (integer *)0, (integer *)0, (integer *
	    )0, (integer *)0, (ftnint)0);
    }

/* Subroutine */ int ulpqid_(char *cp, integer *idx, ftnlen cp_len)
{
    return ulpqnp_0_(1, (integer *)0, cp, idx, (integer *)0, (integer *)0, (
	    integer *)0, cp_len);
    }

/* Subroutine */ int ulpqcp_(integer *idx, char *cp, ftnlen cp_len)
{
    return ulpqnp_0_(2, (integer *)0, cp, idx, (integer *)0, (integer *)0, (
	    integer *)0, cp_len);
    }

/* Subroutine */ int ulpqcl_(integer *idx, char *cp, ftnlen cp_len)
{
    return ulpqnp_0_(3, (integer *)0, cp, idx, (integer *)0, (integer *)0, (
	    integer *)0, cp_len);
    }

/* Subroutine */ int ulpqit_(integer *idx, integer *itp)
{
    return ulpqnp_0_(4, (integer *)0, (char *)0, idx, itp, (integer *)0, (
	    integer *)0, (ftnint)0);
    }

/* Subroutine */ int ulpqvl_(integer *idx, integer *ipara)
{
    return ulpqnp_0_(5, (integer *)0, (char *)0, idx, (integer *)0, ipara, (
	    integer *)0, (ftnint)0);
    }

/* Subroutine */ int ulpsvl_(integer *idx, integer *ipara)
{
    return ulpqnp_0_(6, (integer *)0, (char *)0, idx, (integer *)0, ipara, (
	    integer *)0, (ftnint)0);
    }

/* Subroutine */ int ulpqin_(char *cp, integer *in, ftnlen cp_len)
{
    return ulpqnp_0_(7, (integer *)0, cp, (integer *)0, (integer *)0, (
	    integer *)0, in, cp_len);
    }


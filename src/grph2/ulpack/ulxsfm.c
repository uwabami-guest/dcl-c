/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int ulxsfm_0_(int n__, char *cxfmt, ftnlen cxfmt_len)
{
    extern /* Subroutine */ int uzcget_(char *, char *, ftnlen, ftnlen), 
	    uzcset_(char *, char *, ftnlen, ftnlen);

    switch(n__) {
	case 1: goto L_ulxqfm;
	}

    uzcset_("CXFMT", cxfmt, (ftnlen)5, cxfmt_len);
    return 0;
/* ----------------------------------------------------------------------- */

L_ulxqfm:
    uzcget_("CXFMT", cxfmt, (ftnlen)5, cxfmt_len);
    return 0;
} /* ulxsfm_ */

/* Subroutine */ int ulxsfm_(char *cxfmt, ftnlen cxfmt_len)
{
    return ulxsfm_0_(0, cxfmt, cxfmt_len);
    }

/* Subroutine */ int ulxqfm_(char *cxfmt, ftnlen cxfmt_len)
{
    return ulxsfm_0_(1, cxfmt, cxfmt_len);
    }


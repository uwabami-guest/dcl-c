/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     RUDLEV */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
real rudlev_(integer *nlev)
{
    /* System generated locals */
    integer i__1;
    real ret_val;

    /* Local variables */
    static real hl1, hl2;
    static char clv1[8], clv2[8];
    static integer indx1, indx2;
    static real zlev1, zlev2;
    static integer ityp1, ityp2, nlevz;
    extern /* Subroutine */ int udqcln_(integer *), msgdmp_(char *, char *, 
	    char *, ftnlen, ftnlen, ftnlen), udqclv_(real *, integer *, 
	    integer *, char *, real *, integer *, ftnlen);

    udqcln_(&nlevz);
    if (! (2 <= nlevz)) {
	msgdmp_("W", "IUDLEV", "NO. OF CONTOUR LEVELS IS LESS THAN 2.", (
		ftnlen)1, (ftnlen)6, (ftnlen)37);
	ret_val = 0.f;
	return ret_val;
    }
    if (! (1 <= *nlev && *nlev <= nlevz - 1)) {
	msgdmp_("W", "IUDLEV", "NLEV IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, 
		(ftnlen)21);
	ret_val = 0.f;
	return ret_val;
    }
    udqclv_(&zlev1, &indx1, &ityp1, clv1, &hl1, nlev, (ftnlen)8);
    i__1 = *nlev + 1;
    udqclv_(&zlev2, &indx2, &ityp2, clv2, &hl2, &i__1, (ftnlen)8);
    ret_val = zlev2 - zlev1;
    return ret_val;
} /* rudlev_ */


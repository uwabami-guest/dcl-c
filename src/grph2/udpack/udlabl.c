/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     UDLABL */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int udlabl_0_(int n__, real *val, char *cval, char *cfmt, 
	ftnlen cval_len, ftnlen cfmt_len)
{
    /* Initialized data */

    static char cfmtz[16] = "D               ";

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    extern /* Subroutine */ int chval_(char *, real *, char *, ftnlen, ftnlen)
	    ;

    switch(n__) {
	case 1: goto L_udsfmt;
	case 2: goto L_udqfmt;
	}

    chval_(cfmtz, val, cval, (ftnlen)16, cval_len);
    return 0;
/* ----------------------------------------------------------------------- */

L_udsfmt:
    s_copy(cfmtz, cfmt, (ftnlen)16, cfmt_len);
    return 0;
/* ----------------------------------------------------------------------- */

L_udqfmt:
    s_copy(cfmt, cfmtz, cfmt_len, (ftnlen)16);
    return 0;
} /* udlabl_ */

/* Subroutine */ int udlabl_(real *val, char *cval, ftnlen cval_len)
{
    return udlabl_0_(0, val, cval, (char *)0, cval_len, (ftnint)0);
    }

/* Subroutine */ int udsfmt_(char *cfmt, ftnlen cfmt_len)
{
    return udlabl_0_(1, (real *)0, (char *)0, cfmt, (ftnint)0, cfmt_len);
    }

/* Subroutine */ int udqfmt_(char *cfmt, ftnlen cfmt_len)
{
    return udlabl_0_(2, (real *)0, (char *)0, cfmt, (ftnint)0, cfmt_len);
    }


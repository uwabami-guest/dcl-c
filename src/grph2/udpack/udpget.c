/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     UDPGET / UDPSET / UDPSTX */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int udpget_0_(int n__, char *cp, integer *ipara, ftnlen 
	cp_len)
{
    static char cl[40];
    static integer ip;
    static char cx[8];
    static integer it, idx;
    extern /* Subroutine */ int udiqid_(char *, integer *, ftnlen), udlqid_(
	    char *, integer *, ftnlen), udpqid_(char *, integer *, ftnlen), 
	    rliget_(char *, integer *, integer *, ftnlen), udpqcl_(integer *, 
	    char *, ftnlen), rllget_(char *, integer *, integer *, ftnlen), 
	    udrqid_(char *, integer *, ftnlen), udpqcp_(integer *, char *, 
	    ftnlen), rtiget_(char *, char *, integer *, integer *, ftnlen, 
	    ftnlen), rlrget_(char *, integer *, integer *, ftnlen), rtlget_(
	    char *, char *, integer *, integer *, ftnlen, ftnlen), udpqit_(
	    integer *, integer *), udisvl_(integer *, integer *), rtrget_(
	    char *, char *, integer *, integer *, ftnlen, ftnlen), udlsvl_(
	    integer *, integer *), udpqvl_(integer *, integer *), udpsvl_(
	    integer *, integer *), udrsvl_(integer *, integer *);

    switch(n__) {
	case 1: goto L_udpset;
	case 2: goto L_udpstx;
	}

    udpqid_(cp, &idx, cp_len);
    udpqvl_(&idx, ipara);
    return 0;
/* ----------------------------------------------------------------------- */

L_udpset:
    udpqid_(cp, &idx, cp_len);
    udpsvl_(&idx, ipara);
    return 0;
/* ----------------------------------------------------------------------- */

L_udpstx:
    ip = *ipara;
    udpqid_(cp, &idx, cp_len);
    udpqit_(&idx, &it);
    udpqcp_(&idx, cx, (ftnlen)8);
    udpqcl_(&idx, cl, (ftnlen)40);
    if (it == 1) {
	rtiget_("UD", cx, &ip, &c__1, (ftnlen)2, (ftnlen)8);
	rliget_(cl, &ip, &c__1, (ftnlen)40);
	udiqid_(cp, &idx, cp_len);
	udisvl_(&idx, &ip);
    } else if (it == 2) {
	rtlget_("UD", cx, &ip, &c__1, (ftnlen)2, (ftnlen)8);
	rllget_(cl, &ip, &c__1, (ftnlen)40);
	udlqid_(cp, &idx, cp_len);
	udlsvl_(&idx, &ip);
    } else if (it == 3) {
	rtrget_("UD", cx, &ip, &c__1, (ftnlen)2, (ftnlen)8);
	rlrget_(cl, &ip, &c__1, (ftnlen)40);
	udrqid_(cp, &idx, cp_len);
	udrsvl_(&idx, &ip);
    }
    return 0;
} /* udpget_ */

/* Subroutine */ int udpget_(char *cp, integer *ipara, ftnlen cp_len)
{
    return udpget_0_(0, cp, ipara, cp_len);
    }

/* Subroutine */ int udpset_(char *cp, integer *ipara, ftnlen cp_len)
{
    return udpget_0_(1, cp, ipara, cp_len);
    }

/* Subroutine */ int udpstx_(char *cp, integer *ipara, ftnlen cp_len)
{
    return udpget_0_(2, cp, ipara, cp_len);
    }


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     UDGCLA */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int udgcla_(real *xmin, real *xmax, real *dx)
{
    /* System generated locals */
    integer i__1, i__2;
    real r__1;

    /* Builtin functions */
    integer i_nint(real *);

    /* Local variables */
    static integer i__, n;
    static real hl;
    static integer nl;
    static real dz;
    extern integer irge_(real *), imod_(integer *, integer *), irle_(real *);
    static integer indx, nlev;
    static real zmin, zmax, zlev;
    static integer ityp;
    static logical label;
    static integer idash;
    static char clabz[8];
    static logical ldash;
    extern real rgnge_(real *);
    static integer icycl, idxmj, idxmn, iofst;
    static real rsize;
    extern /* Subroutine */ int udlabl_(real *, char *, ftnlen), udiget_(char 
	    *, integer *, ftnlen);
    static integer isolid;
    extern /* Subroutine */ int udlget_(char *, logical *, ftnlen), udiclv_(
	    void), msgdmp_(char *, char *, char *, ftnlen, ftnlen, ftnlen), 
	    udrget_(char *, real *, ftnlen), udsclv_(real *, integer *, 
	    integer *, char *, real *, ftnlen);

    if (! (*xmin < *xmax)) {
	msgdmp_("E", "UDGCLA", "XMIN SHOULD BE LEAST THAN XMAX.", (ftnlen)1, (
		ftnlen)6, (ftnlen)31);
    }
    udiget_("INDXMJ  ", &idxmj, (ftnlen)8);
    udiget_("INDXMN  ", &idxmn, (ftnlen)8);
    udlget_("LDASH   ", &ldash, (ftnlen)8);
    udlget_("LABEL   ", &label, (ftnlen)8);
    udiget_("ICYCLE  ", &icycl, (ftnlen)8);
    udiget_("ISOLID  ", &isolid, (ftnlen)8);
    udiget_("IDASH   ", &idash, (ftnlen)8);
    udiget_("NLEV    ", &nlev, (ftnlen)8);
    udrget_("RSIZEL  ", &rsize, (ftnlen)8);
    if (*dx > 0.f) {
	dz = *dx;
    } else if (*dx == 0.f) {
	r__1 = (*xmax - *xmin) / nlev;
	dz = rgnge_(&r__1);
    } else {
/* Computing MAX */
	r__1 = abs(*dx);
	i__1 = 1, i__2 = i_nint(&r__1);
	nl = max(i__1,i__2);
	r__1 = (*xmax - *xmin) / nl;
	dz = rgnge_(&r__1);
    }
    r__1 = *xmin / dz;
    zmin = irge_(&r__1) * dz;
    r__1 = *xmax / dz;
    zmax = irle_(&r__1) * dz;
    r__1 = (zmax - zmin) / dz;
    n = i_nint(&r__1) + 1;
    r__1 = zmin / dz;
    i__1 = i_nint(&r__1);
    iofst = imod_(&i__1, &icycl);
    udiclv_();
    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
/*       / CONTOUR LEVEL / */
	r__1 = (zmin + (i__ - 1) * dz) / dz;
	zlev = dz * i_nint(&r__1);
	udlabl_(&zlev, clabz, (ftnlen)8);
	if ((i__ + iofst - 1) % icycl == 0) {
/*         / MAJOR LINE / */
	    indx = idxmj;
	    if (label) {
		hl = rsize;
	    } else {
		hl = 0.f;
	    }
	} else {
/*         / MINOR LINE / */
	    indx = idxmn;
	    hl = 0.f;
	}
/*       / DASH OPTION FOR NEGATIVE CONTOUR LEVEL / */
	if (ldash && zlev < 0.f) {
	    ityp = idash;
	} else {
	    ityp = isolid;
	}
/*       / SET CONTOUR LEVEL / */
	udsclv_(&zlev, &indx, &ityp, clabz, &hl, (ftnlen)8);
/* L10: */
    }
    return 0;
} /* udgcla_ */


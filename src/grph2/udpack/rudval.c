/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     RUDVAL */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
real rudval_(real *zz, real *cz)
{
    /* Initialized data */

    static logical lfst = TRUE_;

    /* System generated locals */
    real ret_val, r__1;

    /* Local variables */
    static real repsl;
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen);

    if (lfst) {
	glrget_("REPSL", &repsl, (ftnlen)5);
	lfst = FALSE_;
    }
    if (*zz == *cz) {
	if (*zz == 0.f) {
/* Computing 2nd power */
	    r__1 = repsl;
	    ret_val = r__1 * r__1;
	} else {
	    ret_val = *zz * (repsl + 1);
	}
    } else {
	ret_val = *zz;
    }
    return ret_val;
} /* rudval_ */


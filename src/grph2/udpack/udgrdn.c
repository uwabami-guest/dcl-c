/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     UDGRDN */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int udgrdn_(integer *mode, integer *ix, integer *iy, integer 
	*k, integer *jx, integer *jy, integer *jk)
{
    static integer km;

    km = 1 - *k;
    if (*mode == 1) {
	jx[0] = *ix;
	jx[1] = *ix + km;
	jx[2] = *ix + *k;
	jx[3] = *ix;
	jy[0] = *iy;
	jy[1] = *iy + *k;
	jy[2] = *iy + km;
	jy[3] = *iy;
	jk[0] = *k;
	jk[1] = km;
	jk[2] = *k;
	jk[3] = km;
    } else {
	jx[0] = *ix;
	jx[1] = *ix - *k;
	jx[2] = *ix - *k;
	jx[3] = *ix + 1 - (*k << 1);
	jy[0] = *iy;
	jy[1] = *iy - km;
	jy[2] = *iy - km;
	jy[3] = *iy + 1 - (km << 1);
	jk[0] = *k;
	jk[1] = km;
	jk[2] = *k;
	jk[3] = km;
    }
    return 0;
} /* udgrdn_ */


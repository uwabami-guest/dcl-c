/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     UDUXUY */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uduxuy_(real *z__, integer *mx, integer *ix, integer *iy,
	 integer *k, real *cx, real *ux, real *uy)
{
    /* System generated locals */
    integer z_dim1, z_offset;

    /* Local variables */
    static real cc, xd1, xd2, yd1, yd2;
    static integer ix1, ix2, iy1, iy2;
    static real zz1, zz2;
    extern real ruwgx_(integer *), ruwgy_(integer *), rudval_(real *, real *);

    /* Parameter adjustments */
    z_dim1 = *mx;
    z_offset = 1 + z_dim1;
    z__ -= z_offset;

    /* Function Body */
    ix1 = *ix;
    ix2 = *ix + 1 - *k;
    iy1 = *iy;
    iy2 = *iy + *k;
    xd1 = ruwgx_(&ix1);
    xd2 = ruwgx_(&ix2);
    yd1 = ruwgy_(&iy1);
    yd2 = ruwgy_(&iy2);
    if (z__[ix1 + iy1 * z_dim1] == *cx) {
	zz1 = rudval_(&z__[ix1 + iy1 * z_dim1], cx);
    } else {
	zz1 = z__[ix1 + iy1 * z_dim1];
    }
    if (z__[ix2 + iy2 * z_dim1] == *cx) {
	zz2 = rudval_(&z__[ix2 + iy2 * z_dim1], cx);
    } else {
	zz2 = z__[ix2 + iy2 * z_dim1];
    }
    cc = (*cx - zz1) / (zz2 - zz1);
    *ux = (xd2 - xd1) * cc + xd1;
    *uy = (yd2 - yd1) * cc + yd1;
    return 0;
} /* uduxuy_ */


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Common Block Declarations */

struct {
    integer nb, lx, ly, nbr;
} udblk1_;

#define udblk1_1 udblk1_

/* ----------------------------------------------------------------------- */
/*     LUDCHK */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
logical ludchk_(integer *i__, integer *j, integer *k, integer *l, integer *
	ibr)
{
    /* Initialized data */

    static logical lfrst = TRUE_;

    /* System generated locals */
    integer ibr_dim1, ibr_offset;
    logical ret_val;

    /* Local variables */
    static integer n1, n2, ii, jj, nn, mask[32];
    static char cbpat[32];
    extern /* Subroutine */ int crvrs_(char *, ftnlen), bitpci_(char *, 
	    integer *, ftnlen), msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen);

    /* Parameter adjustments */
    ibr_dim1 = udblk1_1.nbr;
    ibr_offset = 1 + ibr_dim1 * 0;
    ibr -= ibr_offset;

    /* Function Body */
    if (lfrst) {
	if (udblk1_1.nb != 32) {
	    msgdmp_("E", "LUDCHK", "MAXNB IS INAPPROPRIATE.", (ftnlen)1, (
		    ftnlen)6, (ftnlen)23);
	}
	for (jj = 1; jj <= 32; ++jj) {
	    for (ii = 1; ii <= 32; ++ii) {
		if (ii == jj) {
		    *(unsigned char *)&cbpat[ii - 1] = '1';
		} else {
		    *(unsigned char *)&cbpat[ii - 1] = '0';
		}
/* L10: */
	    }
	    crvrs_(cbpat, (ftnlen)32);
	    bitpci_(cbpat, &mask[jj - 1], (ftnlen)32);
/* L15: */
	}
	lfrst = FALSE_;
    }
    nn = udblk1_1.lx * (udblk1_1.ly * *k + *j) + *i__;
    n1 = nn / udblk1_1.nb + 1;
    n2 = nn % udblk1_1.nb;
    ret_val = (ibr[n1 + *l * ibr_dim1] & mask[n2]) == mask[n2];
    return ret_val;
} /* ludchk_ */


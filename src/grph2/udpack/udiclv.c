/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Common Block Declarations */

struct {
    integer nl;
    real czl[50];
    integer idx[50], ity[50];
    real hlv[50];
} udblk2_;

#define udblk2_1 udblk2_

struct {
    char clab[400];
} udblk3_;

#define udblk3_1 udblk3_

/* Table of constant values */

static integer c__1 = 1;
static integer c__50 = 50;

/* ----------------------------------------------------------------------- */
/*     UDICLV / UDSCLV / UDDCLV */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int udiclv_0_(int n__, real *zlev, integer *indx, integer *
	ityp, char *clv, real *hl, integer *nlev, logical *lset, ftnlen 
	clv_len)
{
    /* Initialized data */

    static logical lsetz = FALSE_;

    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_wsfi(icilist *), do_fio(integer *, char *, ftnlen), e_wsfi(void)
	    ;

    /* Local variables */
    static integer i__, ip, nn;
    static char cmsg[80];
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen);
    extern integer indxrf_(real *, integer *, integer *, real *);

    /* Fortran I/O blocks */
    static icilist io___5 = { 0, cmsg+50, 0, "(I2)", 2, 1 };
    static icilist io___6 = { 0, cmsg+14, 0, "(I2)", 2, 1 };
    static icilist io___7 = { 0, cmsg+37, 0, "(I2)", 2, 1 };


    switch(n__) {
	case 1: goto L_udsclv;
	case 2: goto L_udqclv;
	case 3: goto L_udqcln;
	case 4: goto L_uddclv;
	case 5: goto L_udsclz;
	case 6: goto L_udqclz;
	}

    lsetz = FALSE_;
    udblk2_1.nl = 0;
    return 0;
/* ----------------------------------------------------------------------- */

L_udsclv:
    ip = indxrf_(udblk2_1.czl, &udblk2_1.nl, &c__1, zlev);
    if (ip != 0) {
	nn = ip;
    } else {
	if (udblk2_1.nl >= 50) {
	    s_copy(cmsg, "NUMBER OF CONTOUR LEVELS IS IN EXCESS OF MAXIMUM ("
		    "##)", (ftnlen)80, (ftnlen)53);
	    s_wsfi(&io___5);
	    do_fio(&c__1, (char *)&c__50, (ftnlen)sizeof(integer));
	    e_wsfi();
	    msgdmp_("E", "UDSCLV", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
	}
	++udblk2_1.nl;
	nn = udblk2_1.nl;
    }
    udblk2_1.czl[nn - 1] = *zlev;
    udblk2_1.idx[nn - 1] = *indx;
    udblk2_1.ity[nn - 1] = *ityp;
    udblk2_1.hlv[nn - 1] = *hl;
    s_copy(udblk3_1.clab + (nn - 1 << 3), clv, (ftnlen)8, clv_len);
    lsetz = TRUE_;
    return 0;
/* ----------------------------------------------------------------------- */

L_udqclv:
    if (! (1 <= *nlev && *nlev <= udblk2_1.nl)) {
	s_copy(cmsg, "LEVEL NUMBER (##) IS OUT OF RANGE (1-##).", (ftnlen)80, 
		(ftnlen)41);
	s_wsfi(&io___6);
	do_fio(&c__1, (char *)&(*nlev), (ftnlen)sizeof(integer));
	e_wsfi();
	s_wsfi(&io___7);
	do_fio(&c__1, (char *)&udblk2_1.nl, (ftnlen)sizeof(integer));
	e_wsfi();
	msgdmp_("E", "UDQCLV", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
    }
    *zlev = udblk2_1.czl[*nlev - 1];
    *indx = udblk2_1.idx[*nlev - 1];
    *ityp = udblk2_1.ity[*nlev - 1];
    *hl = udblk2_1.hlv[*nlev - 1];
    s_copy(clv, udblk3_1.clab + (*nlev - 1 << 3), clv_len, (ftnlen)8);
    return 0;
/* ----------------------------------------------------------------------- */

L_udqcln:
    *nlev = udblk2_1.nl;
    return 0;
/* ----------------------------------------------------------------------- */

L_uddclv:
    ip = indxrf_(udblk2_1.czl, &udblk2_1.nl, &c__1, zlev);
    if (ip != 0) {
	i__1 = udblk2_1.nl - 1;
	for (i__ = ip; i__ <= i__1; ++i__) {
	    udblk2_1.czl[i__ - 1] = udblk2_1.czl[i__];
	    udblk2_1.idx[i__ - 1] = udblk2_1.idx[i__];
	    udblk2_1.ity[i__ - 1] = udblk2_1.ity[i__];
	    udblk2_1.hlv[i__ - 1] = udblk2_1.hlv[i__];
	    s_copy(udblk3_1.clab + (i__ - 1 << 3), udblk3_1.clab + (i__ << 3),
		     (ftnlen)8, (ftnlen)8);
/* L10: */
	}
	--udblk2_1.nl;
	if (udblk2_1.nl == 0) {
	    lsetz = FALSE_;
	}
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_udsclz:
    lsetz = *lset;
    return 0;
/* ----------------------------------------------------------------------- */

L_udqclz:
    *lset = lsetz;
    return 0;
} /* udiclv_ */

/* Subroutine */ int udiclv_(void)
{
    return udiclv_0_(0, (real *)0, (integer *)0, (integer *)0, (char *)0, (
	    real *)0, (integer *)0, (logical *)0, (ftnint)0);
    }

/* Subroutine */ int udsclv_(real *zlev, integer *indx, integer *ityp, char *
	clv, real *hl, ftnlen clv_len)
{
    return udiclv_0_(1, zlev, indx, ityp, clv, hl, (integer *)0, (logical *)0,
	     clv_len);
    }

/* Subroutine */ int udqclv_(real *zlev, integer *indx, integer *ityp, char *
	clv, real *hl, integer *nlev, ftnlen clv_len)
{
    return udiclv_0_(2, zlev, indx, ityp, clv, hl, nlev, (logical *)0, 
	    clv_len);
    }

/* Subroutine */ int udqcln_(integer *nlev)
{
    return udiclv_0_(3, (real *)0, (integer *)0, (integer *)0, (char *)0, (
	    real *)0, nlev, (logical *)0, (ftnint)0);
    }

/* Subroutine */ int uddclv_(real *zlev)
{
    return udiclv_0_(4, zlev, (integer *)0, (integer *)0, (char *)0, (real *)
	    0, (integer *)0, (logical *)0, (ftnint)0);
    }

/* Subroutine */ int udsclz_(logical *lset)
{
    return udiclv_0_(5, (real *)0, (integer *)0, (integer *)0, (char *)0, (
	    real *)0, (integer *)0, lset, (ftnint)0);
    }

/* Subroutine */ int udqclz_(logical *lset)
{
    return udiclv_0_(6, (real *)0, (integer *)0, (integer *)0, (char *)0, (
	    real *)0, (integer *)0, lset, (ftnint)0);
    }


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__2 = 2;

/* ----------------------------------------------------------------------- */
/*     UDGCLB */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int udgclb_(real *z__, integer *mx, integer *nx, integer *ny,
	 real *dx)
{
    /* System generated locals */
    integer z_dim1, z_offset, i__1, i__2;
    real r__1;

    /* Builtin functions */
    integer i_nint(real *);

    /* Local variables */
    static integer nl, np[2], nq[2];
    static real dz;
    static integer ns[2];
    extern integer irge_(real *), irle_(real *);
    static integer nlev;
    static real xmin, xmax, zmin, zmax;
    extern real rgnge_(real *);
    static integer lmiss;
    extern real rvmin_(real *, integer *, integer *, integer *, integer *), 
	    rvmax_(real *, integer *, integer *, integer *, integer *);
    static real rmiss;
    extern /* Subroutine */ int udgcla_(real *, real *, real *), gllget_(char 
	    *, integer *, ftnlen), udiget_(char *, integer *, ftnlen), 
	    glrget_(char *, real *, ftnlen);

    /* Parameter adjustments */
    z_dim1 = *mx;
    z_offset = 1 + z_dim1;
    z__ -= z_offset;

    /* Function Body */
    gllget_("LMISS", &lmiss, (ftnlen)5);
    glrget_("RMISS", &rmiss, (ftnlen)5);
    ns[0] = *mx;
    ns[1] = *ny;
    np[0] = 1;
    np[1] = 1;
    nq[0] = *nx;
    nq[1] = *ny;
    xmin = rvmin_(&z__[z_offset], ns, np, nq, &c__2);
    xmax = rvmax_(&z__[z_offset], ns, np, nq, &c__2);
    if (xmin == xmax) {
	return 0;
    }
    if (*dx > 0.f) {
	dz = *dx;
    } else if (*dx == 0.f) {
	udiget_("NLEV", &nlev, (ftnlen)4);
	r__1 = (xmax - xmin) / nlev;
	dz = rgnge_(&r__1);
    } else {
/* Computing MAX */
	r__1 = abs(*dx);
	i__1 = 1, i__2 = i_nint(&r__1);
	nl = max(i__1,i__2);
	r__1 = (xmax - xmin) / nl;
	dz = rgnge_(&r__1);
    }
    r__1 = xmin / dz;
    zmin = irle_(&r__1) * dz;
    r__1 = xmax / dz;
    zmax = irge_(&r__1) * dz;
    udgcla_(&zmin, &zmax, &dz);
    return 0;
} /* udgclb_ */


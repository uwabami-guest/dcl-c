/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__2000 = 2000;

/* ----------------------------------------------------------------------- */
/*     UDCNTR */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int udcntr_(real *z__, integer *mx, integer *nx, integer *ny)
{
    /* System generated locals */
    integer z_dim1, z_offset;

    /* Local variables */
    static integer ibr[2000];
    extern /* Subroutine */ int udcntz_(real *, integer *, integer *, integer 
	    *, integer *, integer *);

    /* Parameter adjustments */
    z_dim1 = *mx;
    z_offset = 1 + z_dim1;
    z__ -= z_offset;

    /* Function Body */
    udcntz_(&z__[z_offset], mx, nx, ny, ibr, &c__2000);
    return 0;
} /* udcntr_ */


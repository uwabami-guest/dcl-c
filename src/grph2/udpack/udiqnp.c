/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__3 = 3;
static integer c__7 = 7;

/* ----------------------------------------------------------------------- */
/*     UDIQNP / UDIQID / UDIQCP / UDIQVL / UDISVL */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int udiqnp_0_(int n__, integer *ncp, char *cp, integer *idx, 
	integer *ipara, integer *in, ftnlen cp_len)
{
    /* Initialized data */

    static char cparas[8*7] = "INDXMJ  " "INDXMN  " "ISOLID  " "IDASH   " 
	    "ICYCLE  " "NLEV    " "IUNDEF  ";
    static integer ix[7] = { 3,1,1,3,2,12,-999 };
    static char cparal[40*7] = "MAJOR_CONTOUR_INDEX                     " 
	    "MINOR_CONTOUR_INDEX                     " "POSITIVE_CONTOUR_TYP"
	    "E                   " "NEGATIVE_CONTOUR_TYPE                   " 
	    "MAJOR_CONTOUR_CYCLE                     " "CONTOUR_LEVEL_NUMBER"
	    "                    " "----IUNDEF                              ";
    static logical lfirst = TRUE_;

    /* System generated locals */
    address a__1[3];
    integer i__1[3];

    /* Builtin functions */
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen),
	     s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer n;
    extern integer lenc_(char *, ftnlen);
    static char cmsg[80];
    extern logical lchreq_(char *, char *, ftnlen, ftnlen);
    extern /* Subroutine */ int rliget_(char *, integer *, integer *, ftnlen),
	     msgdmp_(char *, char *, char *, ftnlen, ftnlen, ftnlen), rtiget_(
	    char *, char *, integer *, integer *, ftnlen, ftnlen);

/*     / SHORT NAME / */
    switch(n__) {
	case 1: goto L_udiqid;
	case 2: goto L_udiqcp;
	case 3: goto L_udiqcl;
	case 4: goto L_udiqvl;
	case 5: goto L_udisvl;
	case 6: goto L_udiqin;
	}

/*     / LONG NAME / */
    *ncp = 7;
    return 0;
/* ----------------------------------------------------------------------- */

L_udiqid:
    for (n = 1; n <= 7; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *idx = n;
	    return 0;
	}
/* L10: */
    }
/* Writing concatenation */
    i__1[0] = 11, a__1[0] = "PARAMETER '";
    i__1[1] = lenc_(cp, cp_len), a__1[1] = cp;
    i__1[2] = 17, a__1[2] = "' IS NOT DEFINED.";
    s_cat(cmsg, a__1, i__1, &c__3, (ftnlen)80);
    msgdmp_("E", "UDIQID", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
    return 0;
/* ----------------------------------------------------------------------- */

L_udiqcp:
    if (1 <= *idx && *idx <= 7) {
	s_copy(cp, cparas + (*idx - 1 << 3), cp_len, (ftnlen)8);
    } else {
	msgdmp_("E", "UDIQCP", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_udiqcl:
    if (1 <= *idx && *idx <= 7) {
	s_copy(cp, cparal + (*idx - 1) * 40, cp_len, (ftnlen)40);
    } else {
	msgdmp_("E", "UDIQCL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_udiqvl:
    if (lfirst) {
	rtiget_("UD", cparas, ix, &c__7, (ftnlen)2, (ftnlen)8);
	rliget_(cparal, ix, &c__7, (ftnlen)40);
	lfirst = FALSE_;
    }
    if (1 <= *idx && *idx <= 7) {
	*ipara = ix[*idx - 1];
    } else {
	msgdmp_("E", "UDIQVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_udisvl:
    if (lfirst) {
	rtiget_("UD", cparas, ix, &c__7, (ftnlen)2, (ftnlen)8);
	rliget_(cparal, ix, &c__7, (ftnlen)40);
	lfirst = FALSE_;
    }
    if (1 <= *idx && *idx <= 7) {
	ix[*idx - 1] = *ipara;
    } else {
	msgdmp_("E", "UDISVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_udiqin:
    for (n = 1; n <= 7; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *in = n;
	    return 0;
	}
/* L20: */
    }
    *in = 0;
    return 0;
} /* udiqnp_ */

/* Subroutine */ int udiqnp_(integer *ncp)
{
    return udiqnp_0_(0, ncp, (char *)0, (integer *)0, (integer *)0, (integer *
	    )0, (ftnint)0);
    }

/* Subroutine */ int udiqid_(char *cp, integer *idx, ftnlen cp_len)
{
    return udiqnp_0_(1, (integer *)0, cp, idx, (integer *)0, (integer *)0, 
	    cp_len);
    }

/* Subroutine */ int udiqcp_(integer *idx, char *cp, ftnlen cp_len)
{
    return udiqnp_0_(2, (integer *)0, cp, idx, (integer *)0, (integer *)0, 
	    cp_len);
    }

/* Subroutine */ int udiqcl_(integer *idx, char *cp, ftnlen cp_len)
{
    return udiqnp_0_(3, (integer *)0, cp, idx, (integer *)0, (integer *)0, 
	    cp_len);
    }

/* Subroutine */ int udiqvl_(integer *idx, integer *ipara)
{
    return udiqnp_0_(4, (integer *)0, (char *)0, idx, ipara, (integer *)0, (
	    ftnint)0);
    }

/* Subroutine */ int udisvl_(integer *idx, integer *ipara)
{
    return udiqnp_0_(5, (integer *)0, (char *)0, idx, ipara, (integer *)0, (
	    ftnint)0);
    }

/* Subroutine */ int udiqin_(char *cp, integer *in, ftnlen cp_len)
{
    return udiqnp_0_(6, (integer *)0, cp, (integer *)0, (integer *)0, in, 
	    cp_len);
    }


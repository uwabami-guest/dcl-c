/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Common Block Declarations */

struct {
    integer nb, lx, ly, nbr;
} udblk1_;

#define udblk1_1 udblk1_

struct {
    integer nl;
    real czl[50];
    integer idx[50], ity[50];
    real hlv[50];
} udblk2_;

#define udblk2_1 udblk2_

struct {
    char clab[400];
} udblk3_;

#define udblk3_1 udblk3_

/* Table of constant values */

static logical c_true = TRUE_;
static integer c__1 = 1;
static integer c__2 = 2;
static logical c_false = FALSE_;
static integer c__0 = 0;

/* ----------------------------------------------------------------------- */
/*     UDCNTZ */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int udcntz_(real *z__, integer *mx, integer *nx, integer *ny,
	 integer *ibr, integer *nbr2)
{
    /* Format strings */
    static char fmt_500[] = "(\002Z(\002,i3,\002,\002,i3,\002)=\002,g16.9"
	    ",\002 ===> \002,g16.9)";

    /* System generated locals */
    integer z_dim1, z_offset, i__1, i__2, i__3;
    real r__1;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_wsfi(icilist *), do_fio(integer *, char *, ftnlen), e_wsfi(void)
	    ;

    /* Local variables */
    static integer i__, j, k, k1, lc;
    static real cx;
    static integer nn, np[2], nq[2];
    static real dz;
    static integer ns[2], iy, ix, ix1, iy1, iy2, ix2;
    static real zz1, zz2, pad;
    static logical lok;
    static real xpt, ypt, xxx;
    static integer nlab;
    extern integer lenc_(char *, ftnlen);
    static logical lmap;
    static char cmsg[80];
    static logical lmsg;
    static integer nlev;
    static logical lset;
    static integer nwrd;
    static real vxmn, vymn, xttl, vxmx, vymx;
    static logical lmada, lchar, lclip, lcent;
    static integer index;
    static logical lopen, lepsl;
    static real roffx;
    static logical lexit, lmiss;
    extern real rvmin_(real *, integer *, integer *, integer *, integer *);
    static real rsize;
    extern real rvmax_(real *, integer *, integer *, integer *, integer *);
    static real rmiss, rminz, rmaxz;
    extern /* Subroutine */ int udgclb_(real *, integer *, integer *, integer 
	    *, real *);
    extern logical ludchk_(integer *, integer *, integer *, integer *, 
	    integer *);
    extern /* Subroutine */ int gliget_(char *, integer *, ftnlen), udbclr_(
	    integer *, integer *, integer *, integer *, integer *), gllget_(
	    char *, logical *, ftnlen), udline_(real *, integer *, integer *, 
	    integer *, integer *, real *, logical *, integer *), udiget_(char 
	    *, integer *, ftnlen), udiclr_(integer *, integer *), udlget_(
	    char *, logical *, ftnlen), glrget_(char *, real *, ftnlen), 
	    sglget_(char *, logical *, ftnlen), msgdmp_(char *, char *, char *
	    , ftnlen, ftnlen, ftnlen), udbset_(integer *, integer *, integer *
	    , integer *, integer *), udrget_(char *, real *, ftnlen), gllset_(
	    char *, logical *, ftnlen);
    extern real rudval_(real *, real *);
    extern /* Subroutine */ int cupper_(char *, ftnlen);
    extern real rudlev_(integer *);
    extern /* Subroutine */ int sglset_(char *, logical *, ftnlen), udqclz_(
	    logical *), udsclz_(logical *), uwdflt_(integer *, integer *);
    static logical lprint;
    extern /* Subroutine */ int uzrget_(char *, real *, ftnlen), stqtrf_(
	    logical *), sgqvpt_(real *, real *, real *, real *), szschz_(char 
	    *, real *, ftnlen), szcrst_(void), szslti_(integer *, integer *), 
	    uzrset_(char *, real *, ftnlen), sgtxzv_(real *, real *, char *, 
	    real *, integer *, integer *, integer *, ftnlen);

    /* Fortran I/O blocks */
    static icilist io___14 = { 0, cmsg+43, 0, "(I2)", 2, 1 };
    static icilist io___21 = { 0, cmsg+10, 0, "(1P,E10.3)", 10, 1 };
    static icilist io___30 = { 0, cmsg, 0, fmt_500, 80, 1 };
    static icilist io___32 = { 0, cmsg+18, 0, "(1P,E10.3)", 10, 1 };


/*     / GET INTERNAL PARAMETERS / */
    /* Parameter adjustments */
    z_dim1 = *mx;
    z_offset = 1 + z_dim1;
    z__ -= z_offset;
    --ibr;

    /* Function Body */
    udrget_("RSIZET  ", &rsize, (ftnlen)8);
    udrget_("XTTL    ", &xttl, (ftnlen)8);
    udlget_("LMSG    ", &lmsg, (ftnlen)8);
    udlget_("LCENT   ", &lcent, (ftnlen)8);
    udiget_("NLEV    ", &nlev, (ftnlen)8);
    udiget_("INDXMJ  ", &index, (ftnlen)8);
    gllget_("LMISS", &lmiss, (ftnlen)5);
    glrget_("RMISS", &rmiss, (ftnlen)5);
    gliget_("NBITSPW", &udblk1_1.nb, (ftnlen)7);
    gllget_("LEPSL", &lepsl, (ftnlen)5);
    sglget_("LCHAR", &lchar, (ftnlen)5);
    stqtrf_(&lmap);
/*     / SET INTERNAL PARAMETER / */
    gllset_("LEPSL", &c_true, (ftnlen)5);
    sglset_("LCHAR", &c_true, (ftnlen)5);
/*     / CHECK WORKING ARRAY SIZE / */
    udblk1_1.nbr = *nbr2 / 2;
    udblk1_1.lx = *nx + 2;
    udblk1_1.ly = *ny + 2;
    nwrd = (udblk1_1.lx * udblk1_1.ly << 1) / udblk1_1.nb + 1;
    if (nwrd > udblk1_1.nbr) {
	msgdmp_("M", "UDCNTR", "WORKING AREA IS NOT ENOUGH.", (ftnlen)1, (
		ftnlen)6, (ftnlen)27);
	s_copy(cmsg, "NBR2/2 SHOULD BE LARGER THAN (NX+2)*(NY+2)/##+1.", (
		ftnlen)80, (ftnlen)48);
	s_wsfi(&io___14);
	i__1 = udblk1_1.nb / 2;
	do_fio(&c__1, (char *)&i__1, (ftnlen)sizeof(integer));
	e_wsfi();
	msgdmp_("E", "-CNT.-", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
    }
/*     / SET GRID ATTRIBUTE IF IT HAS NOT BEEN SET YET / */
    uwdflt_(nx, ny);
/*     / CHECK MIN & MAX / */
    ns[0] = *mx;
    ns[1] = *ny;
    np[0] = 1;
    np[1] = 1;
    nq[0] = *nx;
    nq[1] = *ny;
    rminz = rvmin_(&z__[z_offset], ns, np, nq, &c__2);
    rmaxz = rvmax_(&z__[z_offset], ns, np, nq, &c__2);
    lmada = lmiss && rminz == rmiss && rmaxz == rmiss;
    if (lmada || rminz == rmaxz) {
/*       / MESSAGE FOR MISSING OR CONSTANT FIELD / */
	if (lmada) {
	    s_copy(cmsg, "MISSING FIELD.", (ftnlen)80, (ftnlen)14);
	} else {
	    s_copy(cmsg, "CONSTANT (##########) FIELD.", (ftnlen)80, (ftnlen)
		    28);
	    s_wsfi(&io___21);
	    do_fio(&c__1, (char *)&rminz, (ftnlen)sizeof(real));
	    e_wsfi();
	}
	msgdmp_("W", "UDCNTR", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
	lprint = lmsg;
	lexit = TRUE_;
    } else {
/*       / GENERATE CONTOUR LEVELS IF THEY HAVE NOT BEEN GENERATED YET / */
	udqclz_(&lset);
	if (! lset) {
	    r__1 = -((real) nlev);
	    udgclb_(&z__[z_offset], mx, nx, ny, &r__1);
	    udsclz_(&c_false);
	}
/*       / CHECK INAPPROPRIATE DATA / */
	i__1 = *ny;
	for (k = 1; k <= i__1; ++k) {
	    i__2 = *nx;
	    for (j = 1; j <= i__2; ++j) {
		lok = ! (lmiss && z__[j + k * z_dim1] == rmiss);
		i__3 = udblk2_1.nl;
		for (i__ = 1; i__ <= i__3; ++i__) {
		    if (lok && z__[j + k * z_dim1] == udblk2_1.czl[i__ - 1]) {
			xxx = rudval_(&z__[j + k * z_dim1], &udblk2_1.czl[i__ 
				- 1]);
			s_copy(cmsg, "INAPPROPRIATE DATA WILL BE MODIFIED IN"
				"TERNALLY.", (ftnlen)80, (ftnlen)47);
			msgdmp_("M", "UDCNTR", cmsg, (ftnlen)1, (ftnlen)6, (
				ftnlen)80);
			s_wsfi(&io___30);
			do_fio(&c__1, (char *)&j, (ftnlen)sizeof(integer));
			do_fio(&c__1, (char *)&k, (ftnlen)sizeof(integer));
			do_fio(&c__1, (char *)&z__[j + k * z_dim1], (ftnlen)
				sizeof(real));
			do_fio(&c__1, (char *)&xxx, (ftnlen)sizeof(real));
			e_wsfi();
			msgdmp_("M", "-CNT.-", cmsg, (ftnlen)1, (ftnlen)6, (
				ftnlen)80);
		    }
/* L20: */
		}
/* L15: */
	    }
/* L10: */
	}
/*       / CONTOUR INTERVAL / */
	dz = rudlev_(&c__1);
	s_copy(cmsg, "CONTOUR INTERVAL =##########", (ftnlen)80, (ftnlen)28);
	s_wsfi(&io___32);
	do_fio(&c__1, (char *)&dz, (ftnlen)sizeof(real));
	e_wsfi();
	lprint = udblk2_1.nl >= 2 && lmsg;
	lexit = FALSE_;
    }
/*     / PRINT MESSAGE / */
    if (lprint) {
	sglget_("LCLIP", &lclip, (ftnlen)5);
	sglset_("LCLIP", &c_false, (ftnlen)5);
	sgqvpt_(&vxmn, &vxmx, &vymn, &vymx);
	xpt = (vxmn + vxmx) / 2;
	if (lmap && lcent) {
	    ypt = vymn / 2;
	} else {
	    uzrget_("ROFFXB", &roffx, (ftnlen)6);
	    uzrget_("PAD1", &pad, (ftnlen)4);
	    ypt = vymn + roffx - rsize * (pad * 2 + 1.5f);
	    roffx -= rsize * (pad + 1.f) * 2;
	    uzrset_("ROFFXB", &roffx, (ftnlen)6);
	}
	cupper_(cmsg, (ftnlen)80);
	sgtxzv_(&xpt, &ypt, cmsg, &rsize, &c__0, &c__0, &index, (ftnlen)80);
	sglset_("LCLIP", &lclip, (ftnlen)5);
	if (lexit) {
	    goto L100;
	}
    }
/*     / CHECK BOUND / */
    i__1 = udblk1_1.nbr << 1;
    udiclr_(&ibr[1], &i__1);
    for (k = 0; k <= 1; ++k) {
	k1 = 1 - k;
	i__1 = *ny - k;
	for (iy = 1; iy <= i__1; ++iy) {
	    iy1 = iy + k;
	    i__2 = *nx - k1;
	    for (ix = 1; ix <= i__2; ++ix) {
		ix1 = ix + k1;
		if (! (lmiss && (z__[ix1 + iy * z_dim1] == rmiss || z__[ix + 
			iy1 * z_dim1] == rmiss))) {
		    udbset_(&ix, &iy, &k, &c__1, &ibr[1]);
		}
/* L35: */
	    }
/* L30: */
	}
/* L25: */
    }
    for (k = 0; k <= 1; ++k) {
	k1 = 1 - k;
	i__1 = *ny - k;
	for (iy = 1; iy <= i__1; ++iy) {
	    iy1 = iy + k1;
	    iy2 = iy - k1;
	    i__2 = *nx - k1;
	    for (ix = 1; ix <= i__2; ++ix) {
		ix1 = ix + k;
		ix2 = ix - k;
		if (! ludchk_(&ix1, &iy1, &k, &c__1, &ibr[1]) && ! ludchk_(&
			ix2, &iy2, &k, &c__1, &ibr[1])) {
		    udbclr_(&ix, &iy, &k, &c__1, &ibr[1]);
		}
/* L50: */
	    }
/* L45: */
	}
/* L40: */
    }
/*     / DRAW CONTOURS / */
    i__1 = udblk2_1.nl;
    for (lc = 1; lc <= i__1; ++lc) {
/*       / INITIALIZE / */
	cx = udblk2_1.czl[lc - 1];
	if (! (rminz < cx && cx < rmaxz)) {
	    goto L55;
	}
	if (udblk2_1.hlv[lc - 1] <= 0.f) {
	    szcrst_();
	} else {
	    nlab = lenc_(udblk3_1.clab + (lc - 1 << 3), (ftnlen)8);
	    szschz_(udblk3_1.clab + (lc - 1 << 3), &udblk2_1.hlv[lc - 1], 
		    nlab);
	}
	szslti_(&udblk2_1.ity[lc - 1], &udblk2_1.idx[lc - 1]);
	udiclr_(&ibr[1], &udblk1_1.nbr);
/*       / CHECK BRANCH / */
	nn = 0;
	for (k = 0; k <= 1; ++k) {
	    k1 = 1 - k;
	    i__2 = *ny - k;
	    for (iy = 1; iy <= i__2; ++iy) {
		iy1 = iy + k;
		i__3 = *nx - k1;
		for (ix = 1; ix <= i__3; ++ix) {
		    ix1 = ix + k1;
		    if (z__[ix1 + iy * z_dim1] == cx) {
			zz1 = rudval_(&z__[ix1 + iy * z_dim1], &cx);
		    } else {
			zz1 = z__[ix1 + iy * z_dim1];
		    }
		    if (z__[ix + iy1 * z_dim1] == cx) {
			zz2 = rudval_(&z__[ix + iy1 * z_dim1], &cx);
		    } else {
			zz2 = z__[ix + iy1 * z_dim1];
		    }
		    if (ludchk_(&ix, &iy, &k, &c__1, &ibr[1])) {
			if ((zz1 - cx) * (zz2 - cx) < 0.f) {
			    udbset_(&ix, &iy, &k, &c__0, &ibr[1]);
			    ++nn;
			}
		    }
/* L70: */
		}
/* L65: */
	    }
/* L60: */
	}
	if (nn != 0) {
/*         / BOUND TO BOUND CONTOURS / */
	    for (k = 0; k <= 1; ++k) {
		k1 = 1 - k;
		i__2 = *ny - k;
		for (iy = 1; iy <= i__2; ++iy) {
		    i__3 = *nx - k1;
		    for (ix = 1; ix <= i__3; ++ix) {
			if (ludchk_(&ix, &iy, &k, &c__0, &ibr[1])) {
			    ix1 = ix + k;
			    ix2 = ix - k;
			    iy1 = iy + k1;
			    iy2 = iy - k1;
			    lopen = ! (ludchk_(&ix1, &iy1, &k, &c__1, &ibr[1])
				     && ludchk_(&ix2, &iy2, &k, &c__1, &ibr[1]
				    ));
			    if (lopen) {
				udline_(&z__[z_offset], mx, &ix, &iy, &k, &cx,
					 &lopen, &ibr[1]);
			    }
			}
/* L85: */
		    }
/* L80: */
		}
/* L75: */
	    }
/*         / CLOSED CONTOURS / */
	    k = 0;
	    k1 = 1 - k;
	    i__2 = *ny - k;
	    for (iy = 1; iy <= i__2; ++iy) {
		i__3 = *nx - k1;
		for (ix = 1; ix <= i__3; ++ix) {
		    if (ludchk_(&ix, &iy, &k, &c__0, &ibr[1])) {
			ix1 = ix + k;
			ix2 = ix - k;
			iy1 = iy + k1;
			iy2 = iy - k1;
			lopen = ! (ludchk_(&ix1, &iy1, &k, &c__1, &ibr[1]) && 
				ludchk_(&ix2, &iy2, &k, &c__1, &ibr[1]));
			if (! lopen) {
			    udline_(&z__[z_offset], mx, &ix, &iy, &k, &cx, &
				    lopen, &ibr[1]);
			}
		    }
/* L95: */
		}
/* L90: */
	    }
	}
L55:
	;
    }
/*     / RESET INTERNAL PARAMETER / */
L100:
    gllset_("LEPSL", &lepsl, (ftnlen)5);
    sglset_("LCHAR", &lchar, (ftnlen)5);
    return 0;
} /* udcntz_ */


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     UDRGET / UDRSET / UDRSTX */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int udrget_0_(int n__, char *cp, real *rpara, ftnlen cp_len)
{
    static char cl[40], cx[8];
    static real rp;
    static integer idx;
    extern /* Subroutine */ int udrqid_(char *, integer *, ftnlen), udrqcl_(
	    integer *, char *, ftnlen), udrqcp_(integer *, char *, ftnlen), 
	    rlrget_(char *, real *, integer *, ftnlen), rtrget_(char *, char *
	    , real *, integer *, ftnlen, ftnlen), udrqvl_(integer *, real *), 
	    udrsvl_(integer *, real *);

    switch(n__) {
	case 1: goto L_udrset;
	case 2: goto L_udrstx;
	}

    udrqid_(cp, &idx, cp_len);
    udrqvl_(&idx, rpara);
    return 0;
/* ----------------------------------------------------------------------- */

L_udrset:
    udrqid_(cp, &idx, cp_len);
    udrsvl_(&idx, rpara);
    return 0;
/* ----------------------------------------------------------------------- */

L_udrstx:
    rp = *rpara;
    udrqid_(cp, &idx, cp_len);
/*     / SHORT NAME / */
    udrqcp_(&idx, cx, (ftnlen)8);
    rtrget_("UD", cx, &rp, &c__1, (ftnlen)2, (ftnlen)8);
/*     / LONG NAME / */
    udrqcl_(&idx, cl, (ftnlen)40);
    rlrget_(cl, &rp, &c__1, (ftnlen)40);
    udrsvl_(&idx, &rp);
    return 0;
} /* udrget_ */

/* Subroutine */ int udrget_(char *cp, real *rpara, ftnlen cp_len)
{
    return udrget_0_(0, cp, rpara, cp_len);
    }

/* Subroutine */ int udrset_(char *cp, real *rpara, ftnlen cp_len)
{
    return udrget_0_(1, cp, rpara, cp_len);
    }

/* Subroutine */ int udrstx_(char *cp, real *rpara, ftnlen cp_len)
{
    return udrget_0_(2, cp, rpara, cp_len);
    }


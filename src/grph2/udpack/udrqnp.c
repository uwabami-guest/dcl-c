/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__3 = 3;

/* ----------------------------------------------------------------------- */
/*     UDRQNP / UDRQID / UDRQCP / UDRQVL / UDRSVL */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int udrqnp_0_(int n__, integer *ncp, char *cp, integer *idx, 
	real *rpara, integer *in, ftnlen cp_len)
{
    /* Initialized data */

    static char cparas[8*3] = "RSIZEL  " "RSIZET  " "XTTL    ";
    static real rx[3] = { -999.f,-999.f,0.f };
    static char cparal[40*3] = "CONTOUR_LABEL_HEIGHT                    " 
	    "CONTOUR_MESSAGE_HEIGHT                  " "CONTOUR_MESSAGE_POSI"
	    "TION                ";
    static logical lfirst = TRUE_;

    /* System generated locals */
    address a__1[3];
    integer i__1[3];

    /* Builtin functions */
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen),
	     s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer n;
    extern integer lenc_(char *, ftnlen);
    static char cmsg[80];
    extern logical lchreq_(char *, char *, ftnlen, ftnlen);
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), rlrget_(char *, real *, integer *, ftnlen), 
	    rtrget_(char *, char *, real *, integer *, ftnlen, ftnlen), 
	    uzrget_(char *, real *, ftnlen);

/*     / SHORT NAME / */
    switch(n__) {
	case 1: goto L_udrqid;
	case 2: goto L_udrqcp;
	case 3: goto L_udrqcl;
	case 4: goto L_udrqvl;
	case 5: goto L_udrsvl;
	case 6: goto L_udrqin;
	}

/*     / LONG NAME / */
    *ncp = 3;
    return 0;
/* ----------------------------------------------------------------------- */

L_udrqid:
    for (n = 1; n <= 3; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *idx = n;
	    return 0;
	}
/* L10: */
    }
/* Writing concatenation */
    i__1[0] = 11, a__1[0] = "PARAMETER '";
    i__1[1] = lenc_(cp, cp_len), a__1[1] = cp;
    i__1[2] = 17, a__1[2] = "' IS NOT DEFINED.";
    s_cat(cmsg, a__1, i__1, &c__3, (ftnlen)80);
    msgdmp_("E", "UDRQID", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
    return 0;
/* ----------------------------------------------------------------------- */

L_udrqcp:
    if (1 <= *idx && *idx <= 3) {
	s_copy(cp, cparas + (*idx - 1 << 3), cp_len, (ftnlen)8);
    } else {
	msgdmp_("E", "UDRQCP", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_udrqcl:
    if (1 <= *idx && *idx <= 3) {
	s_copy(cp, cparal + (*idx - 1) * 40, cp_len, (ftnlen)40);
    } else {
	msgdmp_("E", "UDRQCL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_udrqvl:
    if (lfirst) {
	rtrget_("UD", cparas, rx, &c__3, (ftnlen)2, (ftnlen)8);
	rlrget_(cparal, rx, &c__3, (ftnlen)40);
	lfirst = FALSE_;
    }
    if (1 <= *idx && *idx <= 3) {
	*rpara = rx[*idx - 1];
	if ((*idx == 1 || *idx == 2) && *rpara == -999.f) {
	    uzrget_("RSIZEL1", rpara, (ftnlen)7);
	}
    } else {
	msgdmp_("E", "UDRQVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_udrsvl:
    if (lfirst) {
	rtrget_("UD", cparas, rx, &c__3, (ftnlen)2, (ftnlen)8);
	rlrget_(cparal, rx, &c__3, (ftnlen)40);
	lfirst = FALSE_;
    }
    if (1 <= *idx && *idx <= 3) {
	rx[*idx - 1] = *rpara;
    } else {
	msgdmp_("E", "UDRSVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_udrqin:
    for (n = 1; n <= 3; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *in = n;
	    return 0;
	}
/* L20: */
    }
    *in = 0;
    return 0;
} /* udrqnp_ */

/* Subroutine */ int udrqnp_(integer *ncp)
{
    return udrqnp_0_(0, ncp, (char *)0, (integer *)0, (real *)0, (integer *)0,
	     (ftnint)0);
    }

/* Subroutine */ int udrqid_(char *cp, integer *idx, ftnlen cp_len)
{
    return udrqnp_0_(1, (integer *)0, cp, idx, (real *)0, (integer *)0, 
	    cp_len);
    }

/* Subroutine */ int udrqcp_(integer *idx, char *cp, ftnlen cp_len)
{
    return udrqnp_0_(2, (integer *)0, cp, idx, (real *)0, (integer *)0, 
	    cp_len);
    }

/* Subroutine */ int udrqcl_(integer *idx, char *cp, ftnlen cp_len)
{
    return udrqnp_0_(3, (integer *)0, cp, idx, (real *)0, (integer *)0, 
	    cp_len);
    }

/* Subroutine */ int udrqvl_(integer *idx, real *rpara)
{
    return udrqnp_0_(4, (integer *)0, (char *)0, idx, rpara, (integer *)0, (
	    ftnint)0);
    }

/* Subroutine */ int udrsvl_(integer *idx, real *rpara)
{
    return udrqnp_0_(5, (integer *)0, (char *)0, idx, rpara, (integer *)0, (
	    ftnint)0);
    }

/* Subroutine */ int udrqin_(char *cp, integer *in, ftnlen cp_len)
{
    return udrqnp_0_(6, (integer *)0, cp, (integer *)0, (real *)0, in, cp_len)
	    ;
    }


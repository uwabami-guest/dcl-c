/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__3 = 3;

/* ----------------------------------------------------------------------- */
/*     UDPQNP / UDPQID / UDPQCP / UDPQVL / UDPSVL */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int udpqnp_0_(int n__, integer *ncp, char *cp, integer *idx, 
	integer *itp, integer *ipara, integer *in, ftnlen cp_len)
{
    /* Initialized data */

    static char cparas[8*14] = "INDXMJ  " "INDXMN  " "ISOLID  " "IDASH   " 
	    "LDASH   " "LABEL   " "ICYCLE  " "NLEV    " "RSIZEL  " "RSIZET  " 
	    "XTTL    " "LMSG    " "LCENT   " "IUNDEF  ";
    static integer itype[14] = { 1,1,1,1,2,2,1,1,3,3,3,2,2,1 };
    static char cparal[40*14] = "MAJOR_CONTOUR_INDEX                     " 
	    "MINOR_CONTOUR_INDEX                     " "POSITIVE_CONTOUR_TYP"
	    "E                   " "NEGATIVE_CONTOUR_TYPE                   " 
	    "ENABLE_NEGATIVE_CONTOUR                 " "ENABLE_LABELED_CONTO"
	    "UR                  " "MAJOR_CONTOUR_CYCLE                     " 
	    "CONTOUR_LEVEL_NUMBER                    " "CONTOUR_LABEL_HEIGHT"
	    "                    " "CONTOUR_MESSAGE_HEIGHT                  " 
	    "CONTOUR_MESSAGE_POSITION                " "ENABLE_CONTOUR_MESSA"
	    "GE                  " "ENABLE_MESSAGE_CENTERING                " 
	    "----IUNDEF                              ";

    /* System generated locals */
    address a__1[3];
    integer i__1[3];

    /* Builtin functions */
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen),
	     s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer n, id;
    extern integer lenc_(char *, ftnlen);
    static char cmsg[80];
    extern logical lchreq_(char *, char *, ftnlen, ftnlen);
    extern /* Subroutine */ int udiqid_(char *, integer *, ftnlen), udlqid_(
	    char *, integer *, ftnlen), msgdmp_(char *, char *, char *, 
	    ftnlen, ftnlen, ftnlen), udrqid_(char *, integer *, ftnlen), 
	    udiqvl_(integer *, integer *), udlqvl_(integer *, integer *), 
	    udisvl_(integer *, integer *), udlsvl_(integer *, integer *), 
	    udrqvl_(integer *, integer *), udrsvl_(integer *, integer *);

/*     / SHORT NAME / */
    switch(n__) {
	case 1: goto L_udpqid;
	case 2: goto L_udpqcp;
	case 3: goto L_udpqcl;
	case 4: goto L_udpqit;
	case 5: goto L_udpqvl;
	case 6: goto L_udpsvl;
	case 7: goto L_udpqin;
	}

/*     / LONG NAME / */
    *ncp = 14;
    return 0;
/* ----------------------------------------------------------------------- */

L_udpqid:
    for (n = 1; n <= 14; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *idx = n;
	    return 0;
	}
/* L10: */
    }
/* Writing concatenation */
    i__1[0] = 11, a__1[0] = "PARAMETER '";
    i__1[1] = lenc_(cp, cp_len), a__1[1] = cp;
    i__1[2] = 17, a__1[2] = "' IS NOT DEFINED.";
    s_cat(cmsg, a__1, i__1, &c__3, (ftnlen)80);
    msgdmp_("E", "UDPQID", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
    return 0;
/* ----------------------------------------------------------------------- */

L_udpqcp:
    if (1 <= *idx && *idx <= 14) {
	s_copy(cp, cparas + (*idx - 1 << 3), cp_len, (ftnlen)8);
    } else {
	msgdmp_("E", "UDPQCP", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_udpqcl:
    if (1 <= *idx && *idx <= 14) {
	s_copy(cp, cparal + (*idx - 1) * 40, cp_len, (ftnlen)40);
    } else {
	msgdmp_("E", "UDPQCL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_udpqit:
    if (1 <= *idx && *idx <= 14) {
	*itp = itype[*idx - 1];
    } else {
	msgdmp_("E", "UDPQIT", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_udpqvl:
    if (1 <= *idx && *idx <= 14) {
	if (itype[*idx - 1] == 1) {
	    udiqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    udiqvl_(&id, ipara);
	} else if (itype[*idx - 1] == 2) {
	    udlqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    udlqvl_(&id, ipara);
	} else if (itype[*idx - 1] == 3) {
	    udrqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    udrqvl_(&id, ipara);
	}
    } else {
	msgdmp_("E", "UDPQVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_udpsvl:
    if (1 <= *idx && *idx <= 14) {
	if (itype[*idx - 1] == 1) {
	    udiqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    udisvl_(&id, ipara);
	} else if (itype[*idx - 1] == 2) {
	    udlqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    udlsvl_(&id, ipara);
	} else if (itype[*idx - 1] == 3) {
	    udrqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    udrsvl_(&id, ipara);
	}
    } else {
	msgdmp_("E", "UDPSVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_udpqin:
    for (n = 1; n <= 14; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *in = n;
	    return 0;
	}
/* L20: */
    }
    *in = 0;
    return 0;
} /* udpqnp_ */

/* Subroutine */ int udpqnp_(integer *ncp)
{
    return udpqnp_0_(0, ncp, (char *)0, (integer *)0, (integer *)0, (integer *
	    )0, (integer *)0, (ftnint)0);
    }

/* Subroutine */ int udpqid_(char *cp, integer *idx, ftnlen cp_len)
{
    return udpqnp_0_(1, (integer *)0, cp, idx, (integer *)0, (integer *)0, (
	    integer *)0, cp_len);
    }

/* Subroutine */ int udpqcp_(integer *idx, char *cp, ftnlen cp_len)
{
    return udpqnp_0_(2, (integer *)0, cp, idx, (integer *)0, (integer *)0, (
	    integer *)0, cp_len);
    }

/* Subroutine */ int udpqcl_(integer *idx, char *cp, ftnlen cp_len)
{
    return udpqnp_0_(3, (integer *)0, cp, idx, (integer *)0, (integer *)0, (
	    integer *)0, cp_len);
    }

/* Subroutine */ int udpqit_(integer *idx, integer *itp)
{
    return udpqnp_0_(4, (integer *)0, (char *)0, idx, itp, (integer *)0, (
	    integer *)0, (ftnint)0);
    }

/* Subroutine */ int udpqvl_(integer *idx, integer *ipara)
{
    return udpqnp_0_(5, (integer *)0, (char *)0, idx, (integer *)0, ipara, (
	    integer *)0, (ftnint)0);
    }

/* Subroutine */ int udpsvl_(integer *idx, integer *ipara)
{
    return udpqnp_0_(6, (integer *)0, (char *)0, idx, (integer *)0, ipara, (
	    integer *)0, (ftnint)0);
    }

/* Subroutine */ int udpqin_(char *cp, integer *in, ftnlen cp_len)
{
    return udpqnp_0_(7, (integer *)0, cp, (integer *)0, (integer *)0, (
	    integer *)0, in, cp_len);
    }


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__2 = 2;
static integer c__1 = 1;
static real c_b30 = 1.f;
static integer c__3 = 3;
static logical c_false = FALSE_;

/* ----------------------------------------------------------------------- */
/*     USPACK SUB-LABEL (Y)                            S.Sakai  90/08/18 */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int usysub_(char *cya, char *cxa, char *clabel, real *rlbl, 
	ftnlen cya_len, ftnlen cxa_len, ftnlen clabel_len)
{
    /* System generated locals */
    address a__1[2], a__2[3];
    integer i__1[2], i__2[3], i__3;

    /* Builtin functions */
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen);
    integer i_sign(integer *, integer *);

    /* Local variables */
    static integer ic;
    static char cp[8];
    static real vy;
    static integer nch, icy;
    static char cxs[1], cys[1];
    static real vxp, pad1, wpad, rofg, wlbl, hsub, xmin, xmax, wsub, posx, 
	    posy;
    static integer iflag, index, inner;
    static real rsoff;
    static integer mxdgt;
    static real roffx, roffy, xlmin, xlmax, vxmin, vymin, vxmax, vymax, uxusr;
    extern /* Subroutine */ int sglget_(char *, logical *, ftnlen), msgdmp_(
	    char *, char *, char *, ftnlen, ftnlen, ftnlen);
    static integer icenty;
    static logical lclipz;
    extern /* Subroutine */ int cupper_(char *, ftnlen), usiget_(char *, 
	    integer *, ftnlen), sglset_(char *, logical *, ftnlen), uslget_(
	    char *, logical *, ftnlen), uziget_(char *, integer *, ftnlen);
    static logical lprtct;
    static real rsizel;
    extern /* Subroutine */ int stftrf_(real *, real *, real *, real *), 
	    usrget_(char *, real *, ftnlen), uzrget_(char *, real *, ftnlen);
    static real rsizet;
    static integer irotly;
    extern /* Subroutine */ int sgqvpt_(real *, real *, real *, real *), 
	    usrset_(char *, real *, ftnlen), uzrset_(char *, real *, ftnlen), 
	    sgtxzv_(real *, real *, char *, real *, integer *, integer *, 
	    integer *, ftnlen), szqtxw_(char *, integer *, real *, real *, 
	    ftnlen);

    *(unsigned char *)cxs = *(unsigned char *)cxa;
    *(unsigned char *)cys = *(unsigned char *)cya;
    cupper_(cxs, (ftnlen)1);
    cupper_(cys, (ftnlen)1);
    if (*(unsigned char *)cxs != 'T' && *(unsigned char *)cxs != 'B') {
	msgdmp_("E", "USYSUB", "INVALID CXS", (ftnlen)1, (ftnlen)6, (ftnlen)
		11);
    }
    if (*(unsigned char *)cys != 'L' && *(unsigned char *)cys != 'R' && *(
	    unsigned char *)cys != 'U') {
	msgdmp_("E", "USYSUB", "INVALID CYS", (ftnlen)1, (ftnlen)6, (ftnlen)
		11);
    }
/* Writing concatenation */
    i__1[0] = 5, a__1[0] = "ROFFY";
    i__1[1] = 1, a__1[1] = cys;
    s_cat(cp, a__1, i__1, &c__2, (ftnlen)8);
    uzrget_(cp, &roffy, (ftnlen)8);
/* Writing concatenation */
    i__1[0] = 6, a__1[0] = "ICENTY";
    i__1[1] = 1, a__1[1] = cys;
    s_cat(cp, a__1, i__1, &c__2, (ftnlen)8);
    uziget_(cp, &icenty, (ftnlen)8);
/* Writing concatenation */
    i__1[0] = 6, a__1[0] = "IROTLY";
    i__1[1] = 1, a__1[1] = cys;
    s_cat(cp, a__1, i__1, &c__2, (ftnlen)8);
    uziget_(cp, &irotly, (ftnlen)8);
    uzrget_("RSIZEL1", &rsizel, (ftnlen)7);
    uzrget_("RSIZET2", &rsizet, (ftnlen)7);
    uziget_("INDEXL1", &index, (ftnlen)7);
    uziget_("INNER", &inner, (ftnlen)5);
    uzrget_("PAD1", &pad1, (ftnlen)4);
    szqtxw_(clabel, &nch, &wsub, &hsub, clabel_len);
    sgqvpt_(&vxmin, &vxmax, &vymin, &vymax);
    irotly %= 4;
    wsub *= rsizel;
    wlbl = *rlbl * rsizel;
    wpad = pad1 * rsizel;
    ic = 1;
    if (irotly >= 2) {
	ic = -1;
    }
    icy = ic * icenty;
/* ======================== < VERTICAL LABEL > =========================== */
    if (irotly == 1 || irotly == 3) {
	if (*(unsigned char *)cxs == 'T') {
	    icenty = ic;
	    posy = vymax - wlbl / 2 * (icy - 1);
	} else {
	    icenty = -ic;
	    posy = vymin - wlbl / 2 * (icy + 1);
	}
	if (*(unsigned char *)cys == 'L') {
	    posx = vxmin;
	    iflag = -1;
	} else if (*(unsigned char *)cys == 'R') {
	    posx = vxmax;
	    iflag = 1;
	} else {
	    uzrget_("UXUSER", &uxusr, (ftnlen)6);
	    uziget_("IFLAG", &iflag, (ftnlen)5);
	    iflag = i_sign(&c__1, &iflag);
	    stftrf_(&uxusr, &c_b30, &posx, &vy);
	}
	roffy += rsizel * iflag * (pad1 + 1.f);
	posx = posx + roffy - rsizel * iflag / 2.f;
/* Writing concatenation */
	i__1[0] = 5, a__1[0] = "ROFFY";
	i__1[1] = 1, a__1[1] = cys;
	s_cat(cp, a__1, i__1, &c__2, (ftnlen)8);
	uzrset_(cp, &roffy, (ftnlen)8);
/* ====================== < HORIZONTAL LABEL > =========================== */
    } else {
	usiget_("MXDGTSY", &mxdgt, (ftnlen)7);
	uslget_("LPRTCT", &lprtct, (ftnlen)6);
/* Writing concatenation */
	i__2[0] = 5, a__2[0] = "SOFFY";
	i__2[1] = 1, a__2[1] = cys;
	i__2[2] = 1, a__2[2] = cxs;
	s_cat(cp, a__2, i__2, &c__3, (ftnlen)8);
	usrget_(cp, &rsoff, (ftnlen)8);
	if (lprtct && *(unsigned char *)cys == 'U') {
	    msgdmp_("M", "USYSUB", "X-LABEL REGION IS NOT PROTECTED.", (
		    ftnlen)1, (ftnlen)6, (ftnlen)32);
	}
	if (*(unsigned char *)cxs == 'T') {
	    posy = vymax + rsoff + rsizel * (pad1 + .5f);
	    rsoff += rsizel * (pad1 + 1.f);
	} else {
	    posy = vymin + rsoff - rsizel * (pad1 + .5f);
	    rsoff -= rsizel * (pad1 + 1.f);
	}
/* Writing concatenation */
	i__2[0] = 5, a__2[0] = "SOFFY";
	i__2[1] = 1, a__2[1] = cys;
	i__2[2] = 1, a__2[2] = cxs;
	s_cat(cp, a__2, i__2, &c__3, (ftnlen)8);
	usrset_(cp, &rsoff, (ftnlen)8);
/* Writing concatenation */
	i__1[0] = 5, a__1[0] = "ROFFX";
	i__1[1] = 1, a__1[1] = cxs;
	s_cat(cp, a__1, i__1, &c__2, (ftnlen)8);
	usrget_(cp, &roffx, (ftnlen)8);
	roffx = max(rsoff,roffx);
/* ----------------------------------------------------------------------- */
/*       IFLAG =  1 : LABEL IS RIGHT SIDE OF THE AXIS. */
/*       IFLAG = -1 : LABEL IS LEFT  SIDE OF THE AXIS. */
/* Writing concatenation */
	i__1[0] = 5, a__1[0] = "ROFGY";
	i__1[1] = 1, a__1[1] = cys;
	s_cat(cp, a__1, i__1, &c__2, (ftnlen)8);
	uzrget_(cp, &rofg, (ftnlen)8);
	if (inner < 0) {
	    wpad -= inner * rsizet;
	}
	if (*(unsigned char *)cys == 'L') {
	    iflag = -1;
	    vxp = -(vxmin + rofg);
	} else if (*(unsigned char *)cys == 'R') {
	    iflag = 1;
	    vxp = vxmax + rofg;
	} else if (*(unsigned char *)cys == 'U') {
	    uzrget_("UXUSER", &uxusr, (ftnlen)6);
	    uziget_("IFLAG", &iflag, (ftnlen)5);
	    stftrf_(&uxusr, &c_b30, &vxp, &vy);
	    vxp += rofg;
/* Writing concatenation */
	    i__1[0] = 5, a__1[0] = "ROFFX";
	    i__1[1] = 1, a__1[1] = cxs;
	    s_cat(cp, a__1, i__1, &c__2, (ftnlen)8);
	    usrset_(cp, &roffx, (ftnlen)8);
	}
	posx = vxp + wpad + wlbl * (icy * iflag + 1) / 2.f;
	xmin = vxp + wpad;
	xmax = vxp + wpad + mxdgt * rsizel;
/* ------------------------ boundary check ------------------------------- */
	xlmin = posx - wsub * (icy * iflag + 1) / 2.f;
	if (xlmin < xmin) {
	    icy = -iflag;
	    posx = xmin;
	}
	xlmax = posx - wsub * (icy * iflag - 1) / 2.f;
	if (! lprtct && xlmax > xmax) {
	    icy = iflag;
	    posx = xmax;
	}
	xlmin = posx - wsub * (icy * iflag + 1) / 2.f;
	if (xlmin < vxp) {
	    if (*(unsigned char *)cys == 'U') {
		icy = 0;
		posx = (vxp + xmax) / 2.f;
	    } else {
/* Writing concatenation */
		i__1[0] = 5, a__1[0] = "ROFFX";
		i__1[1] = 1, a__1[1] = cxs;
		s_cat(cp, a__1, i__1, &c__2, (ftnlen)8);
		usrset_(cp, &roffx, (ftnlen)8);
	    }
	}
	icenty = icy / ic;
	posx = abs(posx);
    }
    sglget_("LCLIP", &lclipz, (ftnlen)5);
    sglset_("LCLIP", &c_false, (ftnlen)5);
    i__3 = irotly * 90;
    sgtxzv_(&posx, &posy, clabel, &rsizel, &i__3, &icenty, &index, clabel_len)
	    ;
    sglset_("LCLIP", &lclipz, (ftnlen)5);
    return 0;
} /* usysub_ */


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static logical c_true = TRUE_;

/* ----------------------------------------------------------------------- */
/*     USPACK AXIS (LOG)                               S.Sakai  99/10/05 */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int usaxlg_(char *cside, integer *nlbl, integer *nticks, 
	ftnlen cside_len)
{
    /* System generated locals */
    integer i__1;

    /* Local variables */
    static integer i__;
    static char cs[1];
    static integer ncs;
    static logical loff;
    extern integer lenz_(char *, ftnlen);
    static real factor, offset;
    extern /* Subroutine */ int cupper_(char *, ftnlen), uzlget_(char *, 
	    logical *, ftnlen), ulxlog_(char *, integer *, integer *, ftnlen),
	     ulylog_(char *, integer *, integer *, ftnlen), uzlset_(char *, 
	    logical *, ftnlen), usxinz_(char *, real *, real *, ftnlen), 
	    usyinz_(char *, real *, real *, ftnlen), usxtlz_(void), usytlz_(
	    void);

    uzlget_("LOFFSET", &loff, (ftnlen)7);
    uzlset_("LOFFSET", &c_true, (ftnlen)7);
    ncs = lenz_(cside, cside_len);
    i__1 = ncs;
    for (i__ = 1; i__ <= i__1; ++i__) {
	*(unsigned char *)cs = *(unsigned char *)&cside[i__ - 1];
	cupper_(cs, (ftnlen)1);
	if (*(unsigned char *)cs == 'T' || *(unsigned char *)cs == 'B' || *(
		unsigned char *)cs == 'H') {
	    if (*(unsigned char *)cs == 'H') {
		*(unsigned char *)cs = 'U';
	    }
	    usxinz_(cs, &factor, &offset, (ftnlen)1);
	    ulxlog_(cs, nlbl, nticks, (ftnlen)1);
	    usxtlz_();
	} else if (*(unsigned char *)cs == 'L' || *(unsigned char *)cs == 'R' 
		|| *(unsigned char *)cs == 'V') {
	    if (*(unsigned char *)cs == 'V') {
		*(unsigned char *)cs = 'U';
	    }
	    usyinz_(cs, &factor, &offset, (ftnlen)1);
	    ulylog_(cs, nlbl, nticks, (ftnlen)1);
	    usytlz_();
	}
/* L100: */
    }
    uzlset_("LOFFSET", &loff, (ftnlen)7);
    return 0;
} /* usaxlg_ */


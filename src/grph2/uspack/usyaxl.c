/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__2 = 2;
static real c_b15 = 0.f;
static real c_b17 = 3.f;

/* ----------------------------------------------------------------------- */
/*     USPACK DRAW Y-AXIS (LOG)                        DCL 5.0  95/09/04 */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int usyaxl_(char *cys, ftnlen cys_len)
{
    /* System generated locals */
    address a__1[2];
    integer i__1, i__2[2];
    char ch__1[32];

    /* Builtin functions */
    integer i_len(char *, ftnlen);
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen),
	     s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer i__;
    static char cp[8];
    static integer it, itr, nys;
    static logical lab1;
    static real yfac;
    static integer nlbl;
    static char cpos[1];
    extern integer lenz_(char *, ftnlen);
    static real xmin, ymin, xmax, ymax, ymina, ymaxa;
    static char cunit__[32], cysub[32];
    extern /* Character */ VOID csblbl_(char *, ftnlen, real *, real *, char *
	    , ftnlen);
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), uliget_(char *, integer *, ftnlen), uscget_(char 
	    *, char *, ftnlen, ftnlen);
    static integer nticks;
    extern /* Subroutine */ int usiget_(char *, integer *, ftnlen), sgqwnd_(
	    real *, real *, real *, real *), uliset_(char *, integer *, 
	    ftnlen), sgswnd_(real *, real *, real *, real *), sgstrf_(void), 
	    usrget_(char *, real *, ftnlen), uzlget_(char *, logical *, 
	    ftnlen), ulylog_(char *, integer *, integer *, ftnlen), sgqtrn_(
	    integer *);
    static integer itypey;
    extern /* Subroutine */ int usysub_(char *, char *, char *, real *, 
	    ftnlen, ftnlen, ftnlen);

    usiget_("NLBLY", &nlbl, (ftnlen)5);
    usiget_("NTICKSY", &nticks, (ftnlen)7);
    usiget_("ITYPEY", &itypey, (ftnlen)6);
    usrget_("YFAC", &yfac, (ftnlen)4);
    uscget_("CYUNIT", cunit__, (ftnlen)6, (ftnlen)32);
    uliget_("IYTYPE", &it, (ftnlen)6);
    uliset_("IYTYPE", &itypey, (ftnlen)6);
    sgqtrn_(&itr);
    if (! (itr == 2 || itr == 4)) {
	msgdmp_("E", "USXAXL", "INVALID TRANSFORMATION NUMBER.", (ftnlen)1, (
		ftnlen)6, (ftnlen)30);
    }
    sgqwnd_(&xmin, &xmax, &ymin, &ymax);
    ymina = ymin * yfac;
    ymaxa = ymax * yfac;
    sgswnd_(&xmin, &xmax, &ymina, &ymaxa);
    sgstrf_();
/* ---------------------------- Y-AXIS ----------------------------------- */
/* Computing MIN */
    i__1 = i_len(cys, cys_len);
    nys = min(i__1,2);
    i__1 = nys;
    for (i__ = 1; i__ <= i__1; ++i__) {
	ulylog_(cys + (i__ - 1), &nlbl, &nticks, (ftnlen)1);
/* Writing concatenation */
	i__2[0] = 6, a__1[0] = "LABELY";
	i__2[1] = 1, a__1[1] = cys + (i__ - 1);
	s_cat(cp, a__1, i__2, &c__2, (ftnlen)8);
	uzlget_(cp, &lab1, (ftnlen)8);
	if (lab1) {
	    csblbl_(ch__1, (ftnlen)32, &yfac, &c_b15, cunit__, (ftnlen)32);
	    s_copy(cysub, ch__1, (ftnlen)32, (ftnlen)32);
	    if (lenz_(cysub, (ftnlen)32) != 0) {
		uscget_("CYSPOS", cpos, (ftnlen)6, (ftnlen)1);
		usysub_(cys + (i__ - 1), cpos, cysub, &c_b17, (ftnlen)1, (
			ftnlen)1, (ftnlen)32);
	    }
	}
/* L100: */
    }
/* ----------------------------------------------------------------------- */
    sgswnd_(&xmin, &xmax, &ymin, &ymax);
    sgstrf_();
    uliset_("IYTYPE", &it, (ftnlen)6);
    return 0;
} /* usyaxl_ */


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__2 = 2;
static integer c__1 = 1;
static real c_b30 = 1.f;
static integer c__3 = 3;
static logical c_false = FALSE_;

/* ----------------------------------------------------------------------- */
/*     USPACK SUB-LABEL (X)                            S.Sakai  90/08/18 */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int usxsub_(char *cxa, char *cya, char *clabel, real *rlbl, 
	ftnlen cxa_len, ftnlen cya_len, ftnlen clabel_len)
{
    /* System generated locals */
    address a__1[2], a__2[3];
    integer i__1[2], i__2[3], i__3;

    /* Builtin functions */
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen);
    integer i_sign(integer *, integer *);

    /* Local variables */
    static integer ic;
    static char cp[8];
    static real vx;
    static integer nch, icx;
    static char cxs[1], cys[1];
    static real vyp, pad1, wpad, rofg, wlbl, hsub, ymin, ymax, wsub, posx, 
	    posy;
    static integer iflag, index, inner;
    static real rsoff;
    static integer mxdgt;
    static real roffx, roffy, ylmin, ylmax, vxmin, vymin, vxmax, vymax, uyusr;
    extern /* Subroutine */ int sglget_(char *, logical *, ftnlen), msgdmp_(
	    char *, char *, char *, ftnlen, ftnlen, ftnlen);
    static integer icentx;
    static logical lclipz;
    extern /* Subroutine */ int cupper_(char *, ftnlen), usiget_(char *, 
	    integer *, ftnlen), sglset_(char *, logical *, ftnlen), uslget_(
	    char *, logical *, ftnlen), uziget_(char *, integer *, ftnlen);
    static logical lprtct;
    static real rsizel;
    extern /* Subroutine */ int stftrf_(real *, real *, real *, real *), 
	    usrget_(char *, real *, ftnlen), uzrget_(char *, real *, ftnlen);
    static integer irotlx;
    static real rsizet;
    static integer irotly;
    extern /* Subroutine */ int sgqvpt_(real *, real *, real *, real *), 
	    usrset_(char *, real *, ftnlen), uzrset_(char *, real *, ftnlen), 
	    sgtxzv_(real *, real *, char *, real *, integer *, integer *, 
	    integer *, ftnlen), szqtxw_(char *, integer *, real *, real *, 
	    ftnlen);

    *(unsigned char *)cxs = *(unsigned char *)cxa;
    *(unsigned char *)cys = *(unsigned char *)cya;
    cupper_(cxs, (ftnlen)1);
    cupper_(cys, (ftnlen)1);
    if (*(unsigned char *)cys != 'L' && *(unsigned char *)cys != 'R') {
	msgdmp_("E", "USXSUB", "INVALID CYS", (ftnlen)1, (ftnlen)6, (ftnlen)
		11);
    }
    if (*(unsigned char *)cxs != 'T' && *(unsigned char *)cxs != 'B' && *(
	    unsigned char *)cxs != 'U') {
	msgdmp_("E", "USXSUB", "INVALID CXS", (ftnlen)1, (ftnlen)6, (ftnlen)
		11);
    }
/* Writing concatenation */
    i__1[0] = 5, a__1[0] = "ROFFX";
    i__1[1] = 1, a__1[1] = cxs;
    s_cat(cp, a__1, i__1, &c__2, (ftnlen)8);
    uzrget_(cp, &roffx, (ftnlen)8);
/* Writing concatenation */
    i__1[0] = 6, a__1[0] = "ICENTX";
    i__1[1] = 1, a__1[1] = cxs;
    s_cat(cp, a__1, i__1, &c__2, (ftnlen)8);
    uziget_(cp, &icentx, (ftnlen)8);
/* Writing concatenation */
    i__1[0] = 6, a__1[0] = "IROTLX";
    i__1[1] = 1, a__1[1] = cxs;
    s_cat(cp, a__1, i__1, &c__2, (ftnlen)8);
    uziget_(cp, &irotlx, (ftnlen)8);
    uzrget_("RSIZEL1", &rsizel, (ftnlen)7);
    uzrget_("RSIZET2", &rsizet, (ftnlen)7);
    uziget_("INDEXL1", &index, (ftnlen)7);
    uziget_("INNER", &inner, (ftnlen)5);
    uzrget_("PAD1", &pad1, (ftnlen)4);
    szqtxw_(clabel, &nch, &wsub, &hsub, clabel_len);
    sgqvpt_(&vxmin, &vxmax, &vymin, &vymax);
    irotly %= 4;
    wsub *= rsizel;
    wlbl = *rlbl * rsizel;
    wpad = pad1 * rsizel;
    ic = 1;
    if (irotlx >= 2) {
	ic = -1;
    }
    icx = ic * icentx;
/* ======================= < HORIZONTAL LABEL > ========================== */
    if (irotlx == 0 || irotlx == 2) {
	if (*(unsigned char *)cys == 'R') {
	    icentx = ic;
	    posx = vxmax - wlbl / 2 * (icx - 1);
	} else {
	    icentx = -ic;
	    posx = vxmin - wlbl / 2 * (icx + 1);
	}
	if (*(unsigned char *)cxs == 'B') {
	    posy = vymin;
	    iflag = -1;
	} else if (*(unsigned char *)cxs == 'T') {
	    posy = vymax;
	    iflag = 1;
	} else {
	    uzrget_("UYUSER", &uyusr, (ftnlen)6);
	    uziget_("IFLAG", &iflag, (ftnlen)5);
	    iflag = i_sign(&c__1, &iflag);
	    stftrf_(&c_b30, &uyusr, &vx, &posy);
	}
	roffx += rsizel * iflag * (pad1 + 1.f);
	posy = posy + roffx - rsizel * iflag / 2.f;
/* Writing concatenation */
	i__1[0] = 5, a__1[0] = "ROFFX";
	i__1[1] = 1, a__1[1] = cxs;
	s_cat(cp, a__1, i__1, &c__2, (ftnlen)8);
	uzrset_(cp, &roffx, (ftnlen)8);
/* ======================= < VERTICAL LABEL > ============================ */
    } else {
	usiget_("MXDGTSX", &mxdgt, (ftnlen)7);
	uslget_("LPRTCT", &lprtct, (ftnlen)6);
/* Writing concatenation */
	i__2[0] = 5, a__2[0] = "SOFFX";
	i__2[1] = 1, a__2[1] = cxs;
	i__2[2] = 1, a__2[2] = cys;
	s_cat(cp, a__2, i__2, &c__3, (ftnlen)8);
	usrget_(cp, &rsoff, (ftnlen)8);
	if (lprtct && *(unsigned char *)cxs == 'U') {
	    msgdmp_("M", "USXSUB", "Y-LABEL REGION IS NOT PROTECTED.", (
		    ftnlen)1, (ftnlen)6, (ftnlen)32);
	}
	if (*(unsigned char *)cys == 'R') {
	    posx = vxmax + rsoff + rsizel * (pad1 + .5f);
	    rsoff += rsizel * (pad1 + 1.f);
	} else {
	    posx = vxmin + rsoff - rsizel * (pad1 + .5f);
	    rsoff -= rsizel * (pad1 + 1.f);
	}
/* Writing concatenation */
	i__2[0] = 5, a__2[0] = "SOFFX";
	i__2[1] = 1, a__2[1] = cxs;
	i__2[2] = 1, a__2[2] = cys;
	s_cat(cp, a__2, i__2, &c__3, (ftnlen)8);
	usrset_(cp, &rsoff, (ftnlen)8);
/* Writing concatenation */
	i__1[0] = 5, a__1[0] = "ROFFY";
	i__1[1] = 1, a__1[1] = cys;
	s_cat(cp, a__1, i__1, &c__2, (ftnlen)8);
	usrget_(cp, &roffy, (ftnlen)8);
	roffy = max(rsoff,roffy);
/* ----------------------------------------------------------------------- */
/*       IFLAG =  1 : LABEL IS RIGHT SIDE OF THE AXIS. */
/*       IFLAG = -1 : LABEL IS LEFT  SIDE OF THE AXIS. */
/* Writing concatenation */
	i__1[0] = 5, a__1[0] = "ROFGX";
	i__1[1] = 1, a__1[1] = cxs;
	s_cat(cp, a__1, i__1, &c__2, (ftnlen)8);
	uzrget_(cp, &rofg, (ftnlen)8);
	if (inner < 0) {
	    wpad -= inner * rsizet;
	}
	if (*(unsigned char *)cxs == 'B') {
	    iflag = -1;
	    vyp = -(vymin + rofg);
	} else if (*(unsigned char *)cxs == 'T') {
	    iflag = 1;
	    vyp = vymax + rofg;
	} else if (*(unsigned char *)cxs == 'U') {
	    uzrget_("UYUSER", &uyusr, (ftnlen)6);
	    uziget_("IFLAG", &iflag, (ftnlen)5);
	    stftrf_(&c_b30, &uyusr, &vx, &vyp);
	    vyp += rofg;
/* Writing concatenation */
	    i__1[0] = 5, a__1[0] = "ROFFY";
	    i__1[1] = 1, a__1[1] = cys;
	    s_cat(cp, a__1, i__1, &c__2, (ftnlen)8);
	    usrset_(cp, &roffy, (ftnlen)8);
	}
	posy = vyp + wpad + wlbl * (icx * iflag + 1) / 2.f;
	ymin = vyp + wpad;
	ymax = vyp + wpad + mxdgt * rsizel;
/* ------------------------ boundary check ------------------------------- */
	ylmin = posy - wsub * (icx * iflag + 1) / 2.f;
	if (ylmin < ymin) {
	    icx = -iflag;
	    posy = ymin;
	}
	ylmax = posy - wsub * (icx * iflag - 1) / 2.f;
	if (! lprtct && ylmax > ymax) {
	    icx = iflag;
	    posy = ymax;
	}
	ylmin = posy - wsub * (icx * iflag + 1) / 2.f;
	if (ylmin < vyp) {
	    if (*(unsigned char *)cxs == 'U') {
		icx = 0;
		posy = (vyp + ymax) / 2.f;
	    } else {
/* Writing concatenation */
		i__1[0] = 5, a__1[0] = "ROFFY";
		i__1[1] = 1, a__1[1] = cys;
		s_cat(cp, a__1, i__1, &c__2, (ftnlen)8);
		usrset_(cp, &roffy, (ftnlen)8);
	    }
	}
	icentx = icx / ic;
	posy = abs(posy);
    }
    sglget_("LCLIP", &lclipz, (ftnlen)5);
    sglset_("LCLIP", &c_false, (ftnlen)5);
    i__3 = irotlx * 90;
    sgtxzv_(&posx, &posy, clabel, &rsizel, &i__3, &icentx, &index, clabel_len)
	    ;
    sglset_("LCLIP", &lclipz, (ftnlen)5);
    return 0;
} /* usxsub_ */


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     USPACK DRAW AXIS LINE                           S.Sakai  99/10/09 */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uspaxs_(char *cside, integer *islct, ftnlen cside_len)
{
    /* System generated locals */
    integer i__1;

    /* Local variables */
    static integer i__;
    static char cs[1];
    static integer ncs;
    extern integer lenz_(char *, ftnlen);
    extern /* Subroutine */ int cupper_(char *, ftnlen), uxpaxs_(char *, 
	    integer *, ftnlen), uypaxs_(char *, integer *, ftnlen), uxsaxs_(
	    char *, ftnlen), uysaxs_(char *, ftnlen);

    ncs = lenz_(cside, cside_len);
    i__1 = ncs;
    for (i__ = 1; i__ <= i__1; ++i__) {
	*(unsigned char *)cs = *(unsigned char *)&cside[i__ - 1];
	cupper_(cs, (ftnlen)1);
	if (*(unsigned char *)cs == 'T' || *(unsigned char *)cs == 'B' || *(
		unsigned char *)cs == 'H') {
	    if (*(unsigned char *)cs == 'H') {
		*(unsigned char *)cs = 'U';
	    }
	    uxsaxs_(cs, (ftnlen)1);
	    uxpaxs_(cs, islct, (ftnlen)1);
	} else if (*(unsigned char *)cs == 'L' || *(unsigned char *)cs == 'R' 
		|| *(unsigned char *)cs == 'V') {
	    if (*(unsigned char *)cs == 'V') {
		*(unsigned char *)cs = 'U';
	    }
	    uysaxs_(cs, (ftnlen)1);
	    uypaxs_(cs, islct, (ftnlen)1);
	}
/* L100: */
    }
    return 0;
} /* uspaxs_ */


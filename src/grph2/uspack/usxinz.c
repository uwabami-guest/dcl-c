/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__2 = 2;
static integer c__3 = 3;
static logical c_true = TRUE_;
static real c_b34 = 0.f;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int usxinz_0_(int n__, char *csa, real *faca, real *offa, 
	ftnlen csa_len)
{
    /* System generated locals */
    address a__1[2], a__2[3];
    integer i__1[2], i__2[3], i__3, i__4;
    real r__1;
    char ch__1[6], ch__2[7], ch__3[32];

    /* Builtin functions */
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen);
    integer s_cmp(char *, char *, ftnlen, ftnlen);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static char cs[1];
    static integer nlt;
    static real fac0, fac1, off0, off1;
    static char cmin[16];
    static logical loff;
    static char cmax[16], csub[32];
    static real roff, rofg, rlen;
    static char cpos[1];
    static logical lsub;
    static char cttl[32];
    extern integer lenz_(char *, ftnlen);
    static real xmin, ymin, xmax, ymax;
    static char cfmt0[16], cfmt1[16];
    extern /* Subroutine */ int cladj_(char *, ftnlen);
    static logical label;
    extern /* Subroutine */ int chval_(char *, real *, char *, ftnlen, ftnlen)
	    ;
    static char cunit__[32];
    static real sizel;
    extern /* Character */ VOID csblbl_(char *, ftnlen, real *, real *, char *
	    , ftnlen);
    static real factor, rundef;
    static char ctitle[32];
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen);
    static real offset;
    extern /* Subroutine */ int uscget_(char *, char *, ftnlen, ftnlen), 
	    uzcget_(char *, char *, ftnlen, ftnlen), sgqwnd_(real *, real *, 
	    real *, real *), uslget_(char *, logical *, ftnlen), usrget_(char 
	    *, real *, ftnlen), uzlget_(char *, logical *, ftnlen), uzcset_(
	    char *, char *, ftnlen, ftnlen), uzrget_(char *, real *, ftnlen), 
	    usrset_(char *, real *, ftnlen), uzlset_(char *, logical *, 
	    ftnlen), usxsub_(char *, char *, char *, real *, ftnlen, ftnlen, 
	    ftnlen), uxsaxs_(char *, ftnlen), uzrset_(char *, real *, ftnlen),
	     uxsttl_(char *, char *, real *, ftnlen, ftnlen);

    switch(n__) {
	case 1: goto L_usxtlz;
	}

    glrget_("RUNDEF", &rundef, (ftnlen)6);
    *(unsigned char *)cs = *(unsigned char *)csa;
/* Writing concatenation */
    i__1[0] = 5, a__1[0] = "ROFFX";
    i__1[1] = 1, a__1[1] = cs;
    s_cat(ch__1, a__1, i__1, &c__2, (ftnlen)6);
    uzrget_(ch__1, &roff, (ftnlen)6);
/* Writing concatenation */
    i__1[0] = 5, a__1[0] = "ROFGX";
    i__1[1] = 1, a__1[1] = cs;
    s_cat(ch__1, a__1, i__1, &c__2, (ftnlen)6);
    uzrget_(ch__1, &rofg, (ftnlen)6);
    if (roff != rofg) {
	uxsaxs_(cs, (ftnlen)1);
    }
    uzrget_("RSIZEL1", &sizel, (ftnlen)7);
/* Writing concatenation */
    i__2[0] = 5, a__2[0] = "SOFFX";
    i__2[1] = 1, a__2[1] = cs;
    i__2[2] = 1, a__2[2] = "R";
    s_cat(ch__2, a__2, i__2, &c__3, (ftnlen)7);
    r__1 = sizel * .86f;
    usrset_(ch__2, &r__1, (ftnlen)7);
/* Writing concatenation */
    i__2[0] = 5, a__2[0] = "SOFFX";
    i__2[1] = 1, a__2[1] = cs;
    i__2[2] = 1, a__2[2] = "L";
    s_cat(ch__2, a__2, i__2, &c__3, (ftnlen)7);
    r__1 = -sizel * .86f;
    usrset_(ch__2, &r__1, (ftnlen)7);
    uzlget_("LOFFSET", &loff, (ftnlen)7);
    if (loff) {
	uzrget_("XOFFSET", &off0, (ftnlen)7);
	uzrget_("XFACT", &fac0, (ftnlen)5);
    } else {
	off0 = 0.f;
	fac0 = 1.f;
    }
    usrget_("XOFF", &off1, (ftnlen)4);
    usrget_("XFAC", &fac1, (ftnlen)4);
    if (off1 == rundef) {
	off1 = 0.f;
    }
    if (fac1 == rundef) {
	fac1 = 1.f;
    }
    *offa = off1;
    *faca = fac1;
    factor = fac0 / fac1;
    offset = (off0 - off1) / fac1;
    uzlset_("LOFFSET", &c_true, (ftnlen)7);
    uzrset_("XOFFSET", &offset, (ftnlen)7);
    uzrset_("XFACT", &factor, (ftnlen)5);
    uzcget_("CXFMT", cfmt0, (ftnlen)5, (ftnlen)16);
    uscget_("CXFMT", cfmt1, (ftnlen)5, (ftnlen)16);
    if (s_cmp(cfmt1, " ", (ftnlen)16, (ftnlen)1) == 0) {
	s_copy(cfmt1, cfmt0, (ftnlen)16, (ftnlen)16);
    }
    uzcset_("CXFMT", cfmt1, (ftnlen)5, (ftnlen)16);
    return 0;
/* ------------------------------------------------------------- */

L_usxtlz:
    sgqwnd_(&xmin, &xmax, &ymin, &ymax);
/* Writing concatenation */
    i__1[0] = 6, a__1[0] = "LABELX";
    i__1[1] = 1, a__1[1] = cs;
    s_cat(ch__2, a__1, i__1, &c__2, (ftnlen)7);
    uzlget_(ch__2, &label, (ftnlen)7);
    uscget_("CXUNIT", cunit__, (ftnlen)6, (ftnlen)32);
    uscget_("CXTTL ", cttl, (ftnlen)6, (ftnlen)32);
    if (label) {
	csblbl_(ch__3, (ftnlen)32, &fac1, &off1, cunit__, (ftnlen)32);
	s_copy(csub, ch__3, (ftnlen)32, (ftnlen)32);
	uslget_("LXSUB", &lsub, (ftnlen)5);
	if (lenz_(csub, (ftnlen)32) != 0) {
	    if (lsub) {
		r__1 = xmin * factor + offset;
		chval_(cfmt1, &r__1, cmin, (ftnlen)16, (ftnlen)16);
		r__1 = xmax * factor + offset;
		chval_(cfmt1, &r__1, cmax, (ftnlen)16, (ftnlen)16);
/* Computing MAX */
		i__3 = lenz_(cmin, (ftnlen)16), i__4 = lenz_(cmax, (ftnlen)16)
			;
		rlen = (real) max(i__3,i__4);
		uscget_("CXSPOS", cpos, (ftnlen)6, (ftnlen)1);
		usxsub_(cs, cpos, csub, &rlen, (ftnlen)1, (ftnlen)1, (ftnlen)
			32);
		s_copy(ctitle, cttl, (ftnlen)32, (ftnlen)32);
	    } else {
		nlt = lenz_(cttl, (ftnlen)32);
/* Writing concatenation */
		i__1[0] = nlt + 1, a__1[0] = cttl;
		i__1[1] = 32, a__1[1] = csub;
		s_cat(ctitle, a__1, i__1, &c__2, (ftnlen)32);
	    }
	} else {
	    s_copy(ctitle, cttl, (ftnlen)32, (ftnlen)32);
	}
	cladj_(ctitle, (ftnlen)32);
	if (lenz_(ctitle, (ftnlen)32) != 0) {
	    uxsttl_(cs, ctitle, &c_b34, (ftnlen)1, (ftnlen)32);
	}
    }
    uzrset_("XOFFSET", &off0, (ftnlen)7);
    uzrset_("XFACT", &fac0, (ftnlen)5);
    uzcset_("CXFMT", cfmt0, (ftnlen)5, (ftnlen)16);
    return 0;
} /* usxinz_ */

/* Subroutine */ int usxinz_(char *csa, real *faca, real *offa, ftnlen 
	csa_len)
{
    return usxinz_0_(0, csa, faca, offa, csa_len);
    }

/* Subroutine */ int usxtlz_(void)
{
    return usxinz_0_(1, (char *)0, (real *)0, (real *)0, (ftnint)0);
    }


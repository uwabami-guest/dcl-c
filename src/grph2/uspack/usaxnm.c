/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static logical c_true = TRUE_;

/* ----------------------------------------------------------------------- */
/*     USPACK AXIS (SPECIFIED MARK)                    S.Sakai  99/10/05 */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int usaxnm_(char *cside, real *dtick, integer *n1, real *
	dlabel, integer *n2, ftnlen cside_len)
{
    /* System generated locals */
    integer i__1, i__2;

    /* Local variables */
    static integer i__, n;
    static char cs[1];
    static integer ncs;
    static real dlz[200], dtz[200];
    static logical loff;
    extern integer lenz_(char *, ftnlen);
    static real factor, offset;
    extern /* Subroutine */ int cupper_(char *, ftnlen), uzlget_(char *, 
	    logical *, ftnlen), uxaxnm_(char *, real *, integer *, real *, 
	    integer *, ftnlen), uyaxnm_(char *, real *, integer *, real *, 
	    integer *, ftnlen), uzlset_(char *, logical *, ftnlen), usxinz_(
	    char *, real *, real *, ftnlen), usyinz_(char *, real *, real *, 
	    ftnlen), usxtlz_(void), usytlz_(void);

    /* Parameter adjustments */
    --dtick;
    --dlabel;

    /* Function Body */
    uzlget_("LOFFSET", &loff, (ftnlen)7);
    uzlset_("LOFFSET", &c_true, (ftnlen)7);
    ncs = lenz_(cside, cside_len);
    i__1 = ncs;
    for (i__ = 1; i__ <= i__1; ++i__) {
	*(unsigned char *)cs = *(unsigned char *)&cside[i__ - 1];
	cupper_(cs, (ftnlen)1);
	if (*(unsigned char *)cs == 'T' || *(unsigned char *)cs == 'B' || *(
		unsigned char *)cs == 'H') {
	    if (*(unsigned char *)cs == 'H') {
		*(unsigned char *)cs = 'U';
	    }
	    usxinz_(cs, &factor, &offset, (ftnlen)1);
	    i__2 = *n1;
	    for (n = 1; n <= i__2; ++n) {
		dtz[n - 1] = (dtick[n] - offset) / factor;
/* L200: */
	    }
	    i__2 = *n2;
	    for (n = 1; n <= i__2; ++n) {
		dlz[n - 1] = (dlabel[n] - offset) / factor;
/* L210: */
	    }
	    uxaxnm_(cs, dtz, n1, dlz, n2, (ftnlen)1);
	    usxtlz_();
	} else if (*(unsigned char *)cs == 'L' || *(unsigned char *)cs == 'R' 
		|| *(unsigned char *)cs == 'V') {
	    if (*(unsigned char *)cs == 'V') {
		*(unsigned char *)cs = 'U';
	    }
	    usyinz_(cs, &factor, &offset, (ftnlen)1);
	    i__2 = *n1;
	    for (n = 1; n <= i__2; ++n) {
		dtz[n - 1] = (dtick[n] - offset) / factor;
/* L300: */
	    }
	    i__2 = *n2;
	    for (n = 1; n <= i__2; ++n) {
		dlz[n - 1] = (dlabel[n] - offset) / factor;
/* L310: */
	    }
	    uyaxnm_(cs, dtz, n1, dlz, n2, (ftnlen)1);
	    usytlz_();
	}
/* L100: */
    }
    uzlset_("LOFFSET", &loff, (ftnlen)7);
    return 0;
} /* usaxnm_ */


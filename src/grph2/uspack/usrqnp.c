/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__3 = 3;
static integer c__30 = 30;

/* ----------------------------------------------------------------------- */
/*     USRQNP / USRQID / USRQCP / USRQVL / USRSVL */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int usrqnp_0_(int n__, integer *ncp, char *cp, integer *idx, 
	real *rpara, integer *in, ftnlen cp_len)
{
    /* Initialized data */

    static char cparas[8*30] = "RMRGN   " "XOFF    " "YOFF    " "XFAC    " 
	    "YFAC    " "DXT     " "DYT     " "DXL     " "DYL     " "TFACT   " 
	    "SOFFXTR " "SOFFXBR " "SOFFXUR " "SOFFXTL " "SOFFXBL " "SOFFXUL " 
	    "SOFFYRT " "SOFFYLT " "SOFFYUT " "SOFFYRB " "SOFFYLB " "SOFFYUB " 
	    "ROFFXT  " "ROFFXB  " "ROFFYR  " "ROFFYL  " "XDTMIN  " "XDTMAX  " 
	    "YDTMIN  " "YDTMAX  ";
    static real rx[30] = { 9.524f,-999.f,-999.f,-999.f,-999.f,-999.f,-999.f,
	    -999.f,-999.f,2.f,.018f,.018f,.018f,-.018f,-.018f,-.018f,.018f,
	    .018f,.018f,-.018f,-.018f,-.018f,0.f,0.f,0.f,0.f,-999.f,-999.f,
	    -999.f,-999.f };
    static char cparal[40*30] = "MARGIN_WIDTH                            " 
	    "X_LABEL_OFFSET                          " "Y_LABEL_OFFSET      "
	    "                    " "X_LABEL_FACTOR                          " 
	    "Y_LABEL_FACTOR                          " "X_TICK_INTERVAL     "
	    "                    " "Y_TICK_INTERVAL                         " 
	    "X_LABEL_INTERVAL                        " "Y_LABEL_INTERVAL    "
	    "                    " "MAX_TICK_INTERVAL                       " 
	    "****SOFFXTR                             " "****SOFFXBR         "
	    "                    " "****SOFFXUR                             " 
	    "****SOFFXTL                             " "****SOFFXBL         "
	    "                    " "****SOFFXUL                             " 
	    "****SOFFYRT                             " "****SOFFYLT         "
	    "                    " "****SOFFYUT                             " 
	    "****SOFFYRB                             " "****SOFFYLB         "
	    "                    " "****SOFFYUB                             " 
	    "****ROFFXT                              " "****ROFFXB          "
	    "                    " "****ROFFYR                              " 
	    "****ROFFYL                              " "****XDTMIN          "
	    "                    " "****XDTMAX                              " 
	    "****YDTMIN                              " "****YDTMAX          "
	    "                    ";
    static logical lfirst = TRUE_;

    /* System generated locals */
    address a__1[3];
    integer i__1[3];

    /* Builtin functions */
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen),
	     s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer n;
    extern integer lenc_(char *, ftnlen);
    static char cmsg[80];
    extern logical lchreq_(char *, char *, ftnlen, ftnlen);
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), rlrget_(char *, real *, integer *, ftnlen), 
	    rtrget_(char *, char *, real *, integer *, ftnlen, ftnlen);

/* ---- SHORT NAME ---- */
/*     / PARAMETERS FOR USDAXS / */
    switch(n__) {
	case 1: goto L_usrqid;
	case 2: goto L_usrqcp;
	case 3: goto L_usrqcl;
	case 4: goto L_usrqvl;
	case 5: goto L_usrsvl;
	case 6: goto L_usrqin;
	}

/*     / PARAMETERS FOR USUSCU / */
/*     / FOLLOWING 20 PARAMETERS ARE RESET BY USINIZ / */
/* ---- LONG NAME ---- */
/*     / PARAMETERS FOR USDAXS / */
/*     / PARAMETERS FOR USUSCU / */
/*     / FOLLOWING 20 PARAMETERS ARE RESET BY USINIZ / */
    *ncp = 30;
    return 0;
/* ----------------------------------------------------------------------- */

L_usrqid:
    for (n = 1; n <= 30; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *idx = n;
	    return 0;
	}
/* L10: */
    }
/* Writing concatenation */
    i__1[0] = 11, a__1[0] = "PARAMETER '";
    i__1[1] = lenc_(cp, cp_len), a__1[1] = cp;
    i__1[2] = 17, a__1[2] = "' IS NOT DEFINED.";
    s_cat(cmsg, a__1, i__1, &c__3, (ftnlen)80);
    msgdmp_("E", "USRQID", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
    return 0;
/* ----------------------------------------------------------------------- */

L_usrqcp:
    if (1 <= *idx && *idx <= 30) {
	s_copy(cp, cparas + (*idx - 1 << 3), cp_len, (ftnlen)8);
    } else {
	msgdmp_("E", "USRQCP", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_usrqcl:
    if (1 <= *idx && *idx <= 30) {
	s_copy(cp, cparal + (*idx - 1) * 40, cp_len, (ftnlen)40);
    } else {
	msgdmp_("E", "USRQCL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_usrqvl:
    if (lfirst) {
	rtrget_("US", cparas, rx, &c__30, (ftnlen)2, (ftnlen)8);
	rlrget_(cparal, rx, &c__30, (ftnlen)40);
	lfirst = FALSE_;
    }
    if (1 <= *idx && *idx <= 30) {
	*rpara = rx[*idx - 1];
    } else {
	msgdmp_("E", "USRQVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_usrsvl:
    if (lfirst) {
	rtrget_("US", cparas, rx, &c__30, (ftnlen)2, (ftnlen)8);
	rlrget_(cparal, rx, &c__30, (ftnlen)40);
	lfirst = FALSE_;
    }
    if (1 <= *idx && *idx <= 30) {
	rx[*idx - 1] = *rpara;
    } else {
	msgdmp_("E", "USRSVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_usrqin:
    for (n = 1; n <= 30; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *in = n;
	    return 0;
	}
/* L20: */
    }
    *in = 0;
    return 0;
} /* usrqnp_ */

/* Subroutine */ int usrqnp_(integer *ncp)
{
    return usrqnp_0_(0, ncp, (char *)0, (integer *)0, (real *)0, (integer *)0,
	     (ftnint)0);
    }

/* Subroutine */ int usrqid_(char *cp, integer *idx, ftnlen cp_len)
{
    return usrqnp_0_(1, (integer *)0, cp, idx, (real *)0, (integer *)0, 
	    cp_len);
    }

/* Subroutine */ int usrqcp_(integer *idx, char *cp, ftnlen cp_len)
{
    return usrqnp_0_(2, (integer *)0, cp, idx, (real *)0, (integer *)0, 
	    cp_len);
    }

/* Subroutine */ int usrqcl_(integer *idx, char *cp, ftnlen cp_len)
{
    return usrqnp_0_(3, (integer *)0, cp, idx, (real *)0, (integer *)0, 
	    cp_len);
    }

/* Subroutine */ int usrqvl_(integer *idx, real *rpara)
{
    return usrqnp_0_(4, (integer *)0, (char *)0, idx, rpara, (integer *)0, (
	    ftnint)0);
    }

/* Subroutine */ int usrsvl_(integer *idx, real *rpara)
{
    return usrqnp_0_(5, (integer *)0, (char *)0, idx, rpara, (integer *)0, (
	    ftnint)0);
    }

/* Subroutine */ int usrqin_(char *cp, integer *in, ftnlen cp_len)
{
    return usrqnp_0_(6, (integer *)0, cp, (integer *)0, (real *)0, in, cp_len)
	    ;
    }


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;
static doublereal c_b18 = 10.;
static integer c__3 = 3;
static integer c__194 = 194;
static integer c__4 = 4;
static integer c__2 = 2;

/* ----------------------------------------------------------------------- */
/*     USPACK REAL TO CHARACTER                       S. Sakai  90/03/16 */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uschvl_(real *x, char *chx, ftnlen chx_len)
{
    /* System generated locals */
    address a__1[3], a__2[4], a__3[2];
    integer i__1[3], i__2[4], i__3[2];
    char ch__1[3];

    /* Builtin functions */
    double r_lg10(real *);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_wsfi(icilist *), do_fio(integer *, char *, ftnlen), e_wsfi(void)
	    , s_rsfi(icilist *), e_rsfi(void), i_indx(char *, char *, ftnlen, 
	    ftnlen);
    double pow_di(doublereal *, integer *);
    integer i_nint(real *);
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen);
    integer s_cmp(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer n, ix;
    static real xx;
    static integer ndig;
    extern integer lenc_(char *, ftnlen);
    static char cval[16], cfmt[16];
    static real prec;
    static char cexp[16];
    extern /* Character */ VOID usgi_(char *, ftnlen, integer *);
    static logical lexp;
    static integer nexp, nlow;
    static char cval2[16], cexp2[16];
    extern /* Subroutine */ int cladj_(char *, ftnlen);
    static integer nprec;
    static logical lcntl, lsfnt;
    static integer nspace;
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen), sglget_(char 
	    *, logical *, ftnlen), swlget_(char *, logical *, ftnlen);
    static logical lcntlz;

    /* Fortran I/O blocks */
    static icilist io___7 = { 0, cfmt+5, 0, "(I1)", 1, 1 };
    static icilist io___9 = { 0, cval, 0, cfmt, 16, 1 };
    static icilist io___10 = { 0, cfmt+5, 0, "(I1)", 1, 1 };
    static icilist io___11 = { 0, cval, 0, cfmt, 16, 1 };
    static icilist io___18 = { 0, cfmt+9, 0, "(I1)", 1, 1 };
    static icilist io___19 = { 0, cval, 0, cfmt, 16, 1 };
    static icilist io___21 = { 0, cval, 0, cfmt, 16, 1 };
    static icilist io___25 = { 0, cexp2, 0, "(I3)", 16, 1 };


    sglget_("LCNTL", &lcntl, (ftnlen)5);
    lcntlz = lcntl;
    lcntl = TRUE_;
    swlget_("LSYSFNT", &lsfnt, (ftnlen)7);
    glrget_("REPSL", &prec, (ftnlen)5);
    nprec = -r_lg10(&prec);
    if (nprec > 8) {
	nprec = 8;
    }
    s_copy(cfmt, "(E16.xE3)", (ftnlen)16, (ftnlen)9);
    s_wsfi(&io___7);
    do_fio(&c__1, (char *)&nprec, (ftnlen)sizeof(integer));
    e_wsfi();
    s_wsfi(&io___9);
    do_fio(&c__1, (char *)&(*x), (ftnlen)sizeof(real));
    e_wsfi();
    s_copy(cfmt, "(F11.x, TR1, I4)", (ftnlen)16, (ftnlen)16);
    s_wsfi(&io___10);
    do_fio(&c__1, (char *)&nprec, (ftnlen)sizeof(integer));
    e_wsfi();
    s_rsfi(&io___11);
    do_fio(&c__1, (char *)&xx, (ftnlen)sizeof(real));
    do_fio(&c__1, (char *)&nexp, (ftnlen)sizeof(integer));
    e_rsfi();
/* ------------------------ effective digits ----------------------------- */
    for (n = 11; n >= 4; --n) {
	ndig = n;
	if (*(unsigned char *)&cval[n - 1] != '0') {
	    goto L20;
	}
/* L10: */
    }
L20:
    ndig -= i_indx(cval, ".", (ftnlen)16, (ftnlen)1);
/* ----------------------------- mantissa -------------------------------- */
    nlow = nexp - ndig + 1;
    lexp = nexp <= -3 || nlow >= 5;
    if (lexp) {
	xx *= 10;
	nprec = ndig - 1;
    } else {
	xx *= pow_di(&c_b18, &nexp);
	nprec = -nlow + 1;
    }
    if (nprec >= 1) {
	s_copy(cfmt, "(SP, F16.x)", (ftnlen)16, (ftnlen)11);
	s_wsfi(&io___18);
	do_fio(&c__1, (char *)&nprec, (ftnlen)sizeof(integer));
	e_wsfi();
	s_wsfi(&io___19);
	do_fio(&c__1, (char *)&xx, (ftnlen)sizeof(real));
	e_wsfi();
    } else {
	s_copy(cfmt, "(SP, I16)", (ftnlen)16, (ftnlen)9);
	ix = i_nint(&xx);
	s_wsfi(&io___21);
	do_fio(&c__1, (char *)&ix, (ftnlen)sizeof(integer));
	e_wsfi();
    }
    cladj_(cval, (ftnlen)16);
/* ------------------------ For F2C -------------------------------------- */
    nspace = 0;
    for (n = 1; n <= 13; ++n) {
	if (*(unsigned char *)&cval[n - 1] != ' ' && *(unsigned char *)&cval[
		n - 1] != '+' && *(unsigned char *)&cval[n - 1] != '-' && 
		nspace == 0) {
	    nspace = n;
	}
/* L3: */
    }
    if (*(unsigned char *)&cval[nspace - 1] == '.') {
/* Writing concatenation */
	i__1[0] = nspace - 1, a__1[0] = cval;
	i__1[1] = 1, a__1[1] = "0";
	i__1[2] = 15 - (nspace - 1), a__1[2] = cval + (nspace - 1);
	s_cat(cval2, a__1, i__1, &c__3, (ftnlen)16);
	s_copy(cval, cval2, (ftnlen)16, (ftnlen)16);
    }
/* -------------------------- characteristic ----------------------------- */
    if (lexp) {
	--nexp;
	s_wsfi(&io___25);
	do_fio(&c__1, (char *)&nexp, (ftnlen)sizeof(integer));
	e_wsfi();
	cladj_(cexp2, (ftnlen)16);
	if (lcntl) {
/* Writing concatenation */
	    usgi_(ch__1, (ftnlen)3, &c__194);
	    i__2[0] = 3, a__2[0] = ch__1;
	    i__2[1] = 5, a__2[1] = "10\\^{";
	    i__2[2] = lenc_(cexp2, (ftnlen)16), a__2[2] = cexp2;
	    i__2[3] = 1, a__2[3] = "}";
	    s_cat(cexp, a__2, i__2, &c__4, (ftnlen)16);
	} else {
/* Writing concatenation */
	    i__3[0] = 1, a__3[0] = "E";
	    i__3[1] = lenc_(cexp2, (ftnlen)16), a__3[1] = cexp2;
	    s_cat(cexp, a__3, i__3, &c__2, (ftnlen)16);
	}
    } else {
	s_copy(cexp, " ", (ftnlen)16, (ftnlen)1);
    }
/* ----------------------------------------------------------------------- */
    if (lcntl && s_cmp(cval + 1, "1 ", (ftnlen)2, (ftnlen)2) == 0 && s_cmp(
	    cexp, "  ", (ftnlen)16, (ftnlen)2) != 0) {
	usgi_(ch__1, (ftnlen)3, &c__194);
	if (s_cmp(cexp, ch__1, (ftnlen)3, (ftnlen)3) == 0) {
/* Writing concatenation */
	    i__3[0] = 1, a__3[0] = cval;
	    i__3[1] = 13, a__3[1] = cexp + 3;
	    s_cat(chx, a__3, i__3, &c__2, chx_len);
	} else {
/* Writing concatenation */
	    i__3[0] = 1, a__3[0] = cval;
	    i__3[1] = 15, a__3[1] = cexp + 1;
	    s_cat(chx, a__3, i__3, &c__2, chx_len);
	}
    } else {
/* Writing concatenation */
	i__3[0] = lenc_(cval, (ftnlen)16), a__3[0] = cval;
	i__3[1] = 16, a__3[1] = cexp;
	s_cat(chx, a__3, i__3, &c__2, chx_len);
    }
    lcntl = lcntlz;
    return 0;
} /* uschvl_ */


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__194 = 194;
static integer c__4 = 4;
static integer c__3 = 3;

/* ----------------------------------------------------------------------- */
/*     USPACK GET CHARACTER FOR SUBLABEL               S.Sakai  90/02/24 */
/*                                                              92/02/27 */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Character */ VOID csblbl_(char *ret_val, ftnlen ret_val_len, real *ufac, 
	real *uoff, char *cunit__, ftnlen cunit_len)
{
    /* System generated locals */
    address a__1[4], a__2[3];
    integer i__1[4], i__2[3];
    char ch__2[3];

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen), s_cat(char *,
	     char **, integer *, integer *, ftnlen);

    /* Local variables */
    static integer nlf, nls;
    extern integer lenc_(char *, ftnlen);
    static char csub[32];
    extern /* Character */ VOID usgi_(char *, ftnlen, integer *);
    extern integer lenz_(char *, ftnlen);
    static char chfac[16];
    extern /* Subroutine */ int cladj_(char *, ftnlen);
    static char choff[16], cblkt[2];
    extern /* Subroutine */ int uscget_(char *, char *, ftnlen, ftnlen), 
	    uschvl_(real *, char *, ftnlen);

    uscget_("CBLKT", cblkt, (ftnlen)5, (ftnlen)2);
/* ----------------------- OFFSET AND FACTOR ----------------------------- */
    s_copy(choff, " ", (ftnlen)16, (ftnlen)1);
    if (*uoff != 0.f) {
	uschvl_(uoff, choff, (ftnlen)16);
    }
    if (*ufac != 1.f) {
	uschvl_(ufac, chfac, (ftnlen)16);
/* Writing concatenation */
	usgi_(ch__2, (ftnlen)3, &c__194);
	i__1[0] = 3, a__1[0] = ch__2;
	i__1[1] = lenc_(chfac, (ftnlen)16) - 1, a__1[1] = chfac + 1;
	i__1[2] = 1, a__1[2] = " ";
	i__1[3] = 16, a__1[3] = choff;
	s_cat(csub, a__1, i__1, &c__4, (ftnlen)32);
    } else if (*uoff != 0.f) {
	s_copy(csub, choff, (ftnlen)32, (ftnlen)16);
    } else {
	s_copy(csub, " ", (ftnlen)32, (ftnlen)1);
    }
/* --------------------------- UNIT etc. --------------------------------- */
    nlf = lenz_(csub, (ftnlen)32);
    nls = lenz_(cunit__, cunit_len);
    s_copy(ret_val, csub, ret_val_len, (ftnlen)32);
    if (nlf != 0) {
	if (nls != 0) {
/* Writing concatenation */
	    i__1[0] = 1, a__1[0] = cblkt;
	    i__1[1] = nlf + 1, a__1[1] = ret_val;
	    i__1[2] = nls, a__1[2] = cunit__;
	    i__1[3] = 1, a__1[3] = cblkt + 1;
	    s_cat(csub, a__1, i__1, &c__4, (ftnlen)32);
	} else {
/* Writing concatenation */
	    i__2[0] = 1, a__2[0] = cblkt;
	    i__2[1] = nlf, a__2[1] = ret_val;
	    i__2[2] = 1, a__2[2] = cblkt + 1;
	    s_cat(csub, a__2, i__2, &c__3, (ftnlen)32);
	}
    } else {
	if (nls != 0) {
/* Writing concatenation */
	    i__2[0] = 1, a__2[0] = cblkt;
	    i__2[1] = nls, a__2[1] = cunit__;
	    i__2[2] = 1, a__2[2] = cblkt + 1;
	    s_cat(csub, a__2, i__2, &c__3, (ftnlen)32);
	} else {
	    s_copy(csub, " ", (ftnlen)32, (ftnlen)1);
	}
    }
    cladj_(csub, (ftnlen)32);
    s_copy(ret_val, csub, ret_val_len, (ftnlen)32);
    return ;
} /* csblbl_ */


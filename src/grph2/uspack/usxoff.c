/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__2 = 2;

/* ----------------------------------------------------------------------- */
/*     USPACK OFFSET CHEK (X-AXIS)                    S. Sakai  90/08/18 */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int usxoff_(char *cxs, ftnlen cxs_len)
{
    /* System generated locals */
    address a__1[2];
    integer i__1[2];

    /* Builtin functions */
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen);

    /* Local variables */
    static char cp[8];
    static real roff, soff;
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), usrget_(char *, real *, ftnlen), uzrget_(char *, 
	    real *, ftnlen), uzrset_(char *, real *, ftnlen);

    if (*(unsigned char *)cxs == 'T' || *(unsigned char *)cxs == 'B') {
/* Writing concatenation */
	i__1[0] = 5, a__1[0] = "ROFFX";
	i__1[1] = 1, a__1[1] = cxs;
	s_cat(cp, a__1, i__1, &c__2, (ftnlen)8);
	usrget_(cp, &soff, (ftnlen)8);
	uzrget_(cp, &roff, (ftnlen)8);
	roff = max(roff,soff);
	uzrset_(cp, &roff, (ftnlen)8);
    } else {
	msgdmp_("E", "USXOFF", "INVALID CXS", (ftnlen)1, (ftnlen)6, (ftnlen)
		11);
    }
    return 0;
} /* usxoff_ */


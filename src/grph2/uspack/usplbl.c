/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     USPACK DRAW LABEL                               S.Sakai  99/10/09 */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int usplbl_(char *cside, integer *islct, real *pos, char *ch,
	 integer *nc, integer *n, ftnlen cside_len, ftnlen ch_len)
{
    /* System generated locals */
    integer i__1;

    /* Local variables */
    static integer i__;
    static char cs[1];
    static integer ncs;
    extern integer lenz_(char *, ftnlen);
    extern /* Subroutine */ int cupper_(char *, ftnlen), uxplbl_(char *, 
	    integer *, real *, char *, integer *, integer *, ftnlen, ftnlen), 
	    uyplbl_(char *, integer *, real *, char *, integer *, integer *, 
	    ftnlen, ftnlen);

    /* Parameter adjustments */
    ch -= ch_len;
    --pos;

    /* Function Body */
    ncs = lenz_(cside, cside_len);
    i__1 = ncs;
    for (i__ = 1; i__ <= i__1; ++i__) {
	*(unsigned char *)cs = *(unsigned char *)&cside[i__ - 1];
	cupper_(cs, (ftnlen)1);
	if (*(unsigned char *)cs == 'T' || *(unsigned char *)cs == 'B' || *(
		unsigned char *)cs == 'H') {
	    if (*(unsigned char *)cs == 'H') {
		*(unsigned char *)cs = 'U';
	    }
	    uxplbl_(cs, islct, &pos[1], ch + ch_len, nc, n, (ftnlen)1, ch_len)
		    ;
	} else if (*(unsigned char *)cs == 'L' || *(unsigned char *)cs == 'R' 
		|| *(unsigned char *)cs == 'V') {
	    if (*(unsigned char *)cs == 'V') {
		*(unsigned char *)cs = 'U';
	    }
	    uyplbl_(cs, islct, &pos[1], ch + ch_len, nc, n, (ftnlen)1, ch_len)
		    ;
	}
/* L100: */
    }
    return 0;
} /* usplbl_ */


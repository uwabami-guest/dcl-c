/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;
static doublereal c_b18 = 10.;

/* ----------------------------------------------------------------------- */
/*      USPACK NUMBER OF DIGIT                        S. Sakai  89/03/23 */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uszdgt_(real *umin, real *umax, real *dul, integer *
	maxdgt, real *uoff, real *ufact, integer *ndgt, integer *ldgt)
{
    /* System generated locals */
    integer i__1, i__2;
    real r__1, r__2;

    /* Builtin functions */
    integer i_nint(real *);
    double r_lg10(real *);
    integer s_wsfi(icilist *), do_fio(integer *, char *, ftnlen), e_wsfi(void)
	    , s_rsfi(icilist *), e_rsfi(void);
    double pow_di(doublereal *, integer *);

    /* Local variables */
    static integer i__, j, i1;
    static real dl;
    static integer ndd, nmn, nmx, nfac;
    extern integer irge_(real *);
    static char cdul[12];
    static integer noff;
    extern integer irle_(real *);
    static integer nmin, nmax, nmns;
    extern integer irlt_(real *);
    static integer nmxs, ndmax, iuoff;
    static real uminf, umaxf;
    static integer ndtry;
    static real rundef;
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen), msgdmp_(char 
	    *, char *, char *, ftnlen, ftnlen, ftnlen);
    static integer ndmaxs;

    /* Fortran I/O blocks */
    static icilist io___16 = { 0, cdul, 0, "(E12.5)", 12, 1 };
    static icilist io___17 = { 0, cdul, 0, "(T10,I3)", 12, 1 };


    glrget_("RUNDEF", &rundef, (ftnlen)6);
    if (*umin >= *umax) {
	msgdmp_("E", "USZDGT", "NMIN MUST BE < NMAX", (ftnlen)1, (ftnlen)6, (
		ftnlen)19);
    }
    if (*dul <= 0.f) {
	msgdmp_("E", "USZDGT", "DUL MUST BE POSITIVE.", (ftnlen)1, (ftnlen)6, 
		(ftnlen)21);
    }
    r__1 = *umax / *dul;
    nmax = irle_(&r__1);
    r__1 = *umin / *dul;
    nmin = irge_(&r__1);
    if (*uoff == rundef) {
	iuoff = 0;
	i1 = 1;
    } else {
	r__1 = *uoff / *dul;
	iuoff = i_nint(&r__1);
	i1 = 2;
    }
/* ----------------------------------------------------------------------- */
    for (i__ = i1; i__ <= 2; ++i__) {
	umaxf = (nmax - iuoff) * *dul;
	uminf = (nmin - iuoff) * *dul;
	if (uminf != 0.f) {
	    r__2 = abs(uminf);
	    r__1 = r_lg10(&r__2);
	    nmn = irle_(&r__1);
	}
	if (umaxf != 0.f) {
	    r__2 = abs(umaxf);
	    r__1 = r_lg10(&r__2);
	    nmx = irle_(&r__1);
	}
	if (uminf == 0.f) {
	    nmn = nmx;
	}
	if (umaxf == 0.f) {
	    nmx = nmn;
	}
	nmxs = 0;
	nmns = 0;
	if (umaxf < 0.f) {
	    nmxs = 1;
	}
	if (uminf < 0.f) {
	    nmns = 1;
	}
	ndmax = max(nmx,nmn);
/* Computing MAX */
	i__1 = nmx + nmxs, i__2 = nmn + nmns;
	ndmaxs = max(i__1,i__2);
	s_wsfi(&io___16);
	do_fio(&c__1, (char *)&(*dul), (ftnlen)sizeof(real));
	e_wsfi();
	s_rsfi(&io___17);
	do_fio(&c__1, (char *)&ndd, (ftnlen)sizeof(integer));
	e_rsfi();
	for (j = 4; j <= 9; ++j) {
	    if (*(unsigned char *)&cdul[j - 1] == '0') {
		goto L55;
	    }
	    --ndd;
/* L50: */
	}
L55:
	ndtry = ndmaxs - ndd + 1;
	if (ndtry <= *maxdgt) {
	    goto L200;
	}
	r__2 = umaxf - uminf;
	r__1 = r_lg10(&r__2);
	noff = irle_(&r__1);
	i__1 = noff + 1;
	dl = pow_di(&c_b18, &i__1);
	r__1 = umaxf / dl;
	*uoff = irlt_(&r__1) * dl;
	r__1 = *uoff / *dul;
	iuoff = irle_(&r__1);
/* L100: */
    }
    msgdmp_("W", "USLDGT", "LABEL WIDTH IS GREATER THAN MAXDGT", (ftnlen)1, (
	    ftnlen)6, (ftnlen)34);
L200:
    *uoff = *dul * iuoff;
/* ----------------------------------------------------------------------- */
    if (*ufact != rundef) {
	r__1 = r_lg10(ufact);
	nfac = irle_(&r__1);
	ndmaxs -= nfac;
	ndmax -= nfac;
	ndd -= nfac;
    }
    if (ndd >= 0) {
	*ndgt = ndmaxs + 1;
	*ldgt = 0;
    } else if (ndmax <= 0) {
	*ndgt = -ndd + 2;
	if (uminf < 0.f) {
	    ++(*ndgt);
	}
	*ldgt = -ndd;
    } else {
	*ndgt = ndmaxs - ndd + 2;
	*ldgt = -ndd;
    }
    if (*ufact != rundef) {
	if (*ndgt > *maxdgt) {
	    msgdmp_("W", "USLDGT", "LABEL WIDTH IS GREATER THAN MAXDGT", (
		    ftnlen)1, (ftnlen)6, (ftnlen)34);
	}
    } else {
	if (*ndgt <= *maxdgt) {
	    *ufact = 1.f;
	} else {
	    *ndgt = ndmaxs - ndd + 1;
	    *ldgt = 0;
	    *ufact = pow_di(&c_b18, &ndd);
	}
    }
    return 0;
} /* uszdgt_ */


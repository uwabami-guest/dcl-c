/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__2 = 2;
static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     USPACK DRAW X-AXIS (UNIFORM)                    DCL 5.0  95/09/04 */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int usxaxu_(char *cxs, ftnlen cxs_len)
{
    /* System generated locals */
    address a__1[2];
    integer i__1, i__2[2];
    real r__1;
    char ch__1[32];

    /* Builtin functions */
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen),
	     s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rsfi(icilist *), do_fio(integer *, char *, ftnlen), e_rsfi(void)
	    ;

    /* Local variables */
    static integer i__;
    static char cp[8];
    static real dxl;
    static integer itr;
    static real dxt;
    static integer nxs;
    static logical lab1;
    static real xfac, dxla;
    static integer ndgt;
    static real dxta, xoff;
    static char cpos[1];
    extern integer lenz_(char *, ftnlen);
    static real xmin, ymin, xmax, ymax;
    static char cfmt0[8];
    static real xmina, xmaxa;
    static char cxfmt[8], cxsub[32];
    extern /* Character */ VOID csblbl_(char *, ftnlen, real *, real *, char *
	    , ftnlen);
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), uscget_(char *, char *, ftnlen, ftnlen), sgqwnd_(
	    real *, real *, real *, real *), sgswnd_(real *, real *, real *, 
	    real *), sgstrf_(void), usrget_(char *, real *, ftnlen);
    static char cxunit[32];
    extern /* Subroutine */ int uzlget_(char *, logical *, ftnlen), sgqtrn_(
	    integer *), uxaxdv_(char *, real *, real *, ftnlen), uxqfmt_(char 
	    *, ftnlen), uxsfmt_(char *, ftnlen), usxsub_(char *, char *, char 
	    *, real *, ftnlen, ftnlen, ftnlen);

    /* Fortran I/O blocks */
    static icilist io___23 = { 0, cxfmt, 0, "(T3,I1)", 8, 1 };


/* ---------------------------- PARAMETERS ------------------------------- */
    usrget_("DXT", &dxt, (ftnlen)3);
    usrget_("DXL", &dxl, (ftnlen)3);
    usrget_("XFAC", &xfac, (ftnlen)4);
    usrget_("XOFF", &xoff, (ftnlen)4);
    uscget_("CXFMT", cxfmt, (ftnlen)5, (ftnlen)8);
    uscget_("CXUNIT", cxunit, (ftnlen)6, (ftnlen)32);
    if (dxt <= 0.f || dxl <= 0.f) {
	msgdmp_("E", "USXAXU", "DXT OR DXL IS NEGATIVE.", (ftnlen)1, (ftnlen)
		6, (ftnlen)23);
    }
/* ----------------------------------------------------------------------- */
    sgqtrn_(&itr);
    if (! (itr == 1 || itr == 2)) {
	msgdmp_("E", "USXAXU", "INVALID TRANSFORMATION NUMBER.", (ftnlen)1, (
		ftnlen)6, (ftnlen)30);
    }
    sgqwnd_(&xmin, &xmax, &ymin, &ymax);
    dxta = dxt / xfac;
    dxla = dxl / xfac;
    xmina = (xmin - xoff) * xfac;
    xmaxa = (xmax - xoff) * xfac;
    sgswnd_(&xmina, &xmaxa, &ymin, &ymax);
    sgstrf_();
/* ---------------------------- X-AXIS ----------------------------------- */
    uxqfmt_(cfmt0, (ftnlen)8);
    uxsfmt_(cxfmt, (ftnlen)8);
/* Computing MIN */
    i__1 = lenz_(cxs, cxs_len);
    nxs = min(i__1,2);
    i__1 = nxs;
    for (i__ = 1; i__ <= i__1; ++i__) {
	uxaxdv_(cxs + (i__ - 1), &dxta, &dxla, (ftnlen)1);
/* Writing concatenation */
	i__2[0] = 6, a__1[0] = "LABELX";
	i__2[1] = 1, a__1[1] = cxs + (i__ - 1);
	s_cat(cp, a__1, i__2, &c__2, (ftnlen)8);
	uzlget_(cp, &lab1, (ftnlen)8);
	if (lab1) {
	    csblbl_(ch__1, (ftnlen)32, &xfac, &xoff, cxunit, (ftnlen)32);
	    s_copy(cxsub, ch__1, (ftnlen)32, (ftnlen)32);
	    if (lenz_(cxsub, (ftnlen)32) != 0) {
		uscget_("CXSPOS", cpos, (ftnlen)6, (ftnlen)1);
		s_rsfi(&io___23);
		do_fio(&c__1, (char *)&ndgt, (ftnlen)sizeof(integer));
		e_rsfi();
		r__1 = (real) ndgt;
		usxsub_(cxs + (i__ - 1), cpos, cxsub, &r__1, (ftnlen)1, (
			ftnlen)1, (ftnlen)32);
	    }
	}
/* L100: */
    }
    sgswnd_(&xmin, &xmax, &ymin, &ymax);
    sgstrf_();
    uxsfmt_(cfmt0, (ftnlen)8);
    return 0;
} /* usxaxu_ */


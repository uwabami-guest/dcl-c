/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static logical c_true = TRUE_;
static integer c__2 = 2;

/* ----------------------------------------------------------------------- */
/*     USPACK AXIS (AUTO SCALING)                      S.Sakai  99/10/07 */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int usaxsc_(char *cside, ftnlen cside_len)
{
    /* System generated locals */
    address a__1[2];
    integer i__1, i__2[2];

    /* Builtin functions */
    integer s_cmp(char *, char *, ftnlen, ftnlen);
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen);

    /* Local variables */
    static integer i__;
    static char cp[8], cs[1];
    static integer it, ncs;
    static real dxl, dyl;
    static integer itr;
    static real dxt, dyt;
    static integer mode;
    static logical loff;
    static integer nlbl;
    extern integer lenz_(char *, ftnlen);
    static real xfact, yfact;
    static integer irota;
    static real uxmin, vxmin, vymin, vxmax, vymax, uxmax, uymin, uymax, 
	    uxmin0, uymin0, uxmax0, uymax0;
    static logical lmatch;
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), uliget_(char *, integer *, ftnlen);
    static integer nticks;
    extern /* Subroutine */ int cupper_(char *, ftnlen), usiget_(char *, 
	    integer *, ftnlen), sgqwnd_(real *, real *, real *, real *), 
	    uslget_(char *, logical *, ftnlen), uliset_(char *, integer *, 
	    ftnlen), uziget_(char *, integer *, ftnlen), usaxlg_(char *, 
	    integer *, integer *, ftnlen), usrget_(char *, real *, ftnlen), 
	    uzlget_(char *, logical *, ftnlen), usaxdv_(char *, real *, real *
	    , ftnlen), sgqtrn_(integer *), ususcl_(char *, real *, real *, 
	    real *, real *, ftnlen), uzrget_(char *, real *, ftnlen);
    static integer itypex, itypey;
    extern /* Subroutine */ int sgqvpt_(real *, real *, real *, real *), 
	    uzlset_(char *, logical *, ftnlen), ususcu_(char *, real *, real *
	    , real *, real *, integer *, ftnlen);
    static real xoffset, yoffset;

    if (s_cmp(cside, " ", cside_len, (ftnlen)1) == 0) {
	return 0;
    }
    sgqtrn_(&itr);
    sgqvpt_(&vxmin, &vxmax, &vymin, &vymax);
    sgqwnd_(&uxmin0, &uxmax0, &uymin0, &uymax0);
    uzrget_("XFACT", &xfact, (ftnlen)5);
    uzrget_("XOFFSET", &xoffset, (ftnlen)7);
    uzrget_("YFACT", &yfact, (ftnlen)5);
    uzrget_("YOFFSET", &yoffset, (ftnlen)7);
    uxmin = xfact * uxmin0 + xoffset;
    uxmax = xfact * uxmax0 + xoffset;
    uymin = yfact * uymin0 + yoffset;
    uymax = yfact * uymax0 + yoffset;
    ncs = lenz_(cside, cside_len);
    uzlget_("LOFFSET", &loff, (ftnlen)7);
    uzlset_("LOFFSET", &c_true, (ftnlen)7);
    i__1 = ncs;
    for (i__ = 1; i__ <= i__1; ++i__) {
	*(unsigned char *)cs = *(unsigned char *)&cside[i__ - 1];
	cupper_(cs, (ftnlen)1);
	if (*(unsigned char *)cs == 'T' || *(unsigned char *)cs == 'B' || *(
		unsigned char *)cs == 'H') {
	    if (*(unsigned char *)cs == 'H') {
		*(unsigned char *)cs = 'U';
	    }
	    if (itr == 1 || itr == 2) {
/* Writing concatenation */
		i__2[0] = 6, a__1[0] = "IROTLX";
		i__2[1] = 1, a__1[1] = cs;
		s_cat(cp, a__1, i__2, &c__2, (ftnlen)8);
		uziget_(cp, &irota, (ftnlen)8);
		uslget_("LMATCH", &lmatch, (ftnlen)6);
		mode = irota % 2;
		if (lmatch) {
		    mode = 0;
		}
		ususcu_("X", &uxmin, &uxmax, &vxmin, &vxmax, &mode, (ftnlen)1)
			;
		usrget_("DXT", &dxt, (ftnlen)3);
		usrget_("DXL", &dxl, (ftnlen)3);
		if (*(unsigned char *)cs == 'U') {
		    *(unsigned char *)cs = 'H';
		}
		usaxdv_(cs, &dxt, &dxl, (ftnlen)1);
	    } else if (itr == 3 || itr == 4) {
		ususcl_("X", &uxmin, &uxmax, &vxmin, &vxmax, (ftnlen)1);
		usiget_("NLBLX", &nlbl, (ftnlen)5);
		usiget_("NTICKSX", &nticks, (ftnlen)7);
		usiget_("ITYPEX", &itypex, (ftnlen)6);
		uliget_("IXTYPE", &it, (ftnlen)6);
		uliset_("IXTYPE", &itypex, (ftnlen)6);
		usaxlg_(cs, &nlbl, &nticks, (ftnlen)1);
		uliset_("IXTYPE", &it, (ftnlen)6);
	    } else {
		msgdmp_("E", "USXAXS", "INVALID TRANSFORMATION NUMBER.", (
			ftnlen)1, (ftnlen)6, (ftnlen)30);
	    }
	} else if (*(unsigned char *)cs == 'L' || *(unsigned char *)cs == 'R' 
		|| *(unsigned char *)cs == 'V') {
	    if (*(unsigned char *)cs == 'V') {
		*(unsigned char *)cs = 'U';
	    }
	    if (itr == 1 || itr == 3) {
/* Writing concatenation */
		i__2[0] = 6, a__1[0] = "IROTLY";
		i__2[1] = 1, a__1[1] = cs;
		s_cat(cp, a__1, i__2, &c__2, (ftnlen)8);
		uziget_(cp, &irota, (ftnlen)8);
		uslget_("LMATCH", &lmatch, (ftnlen)6);
		mode = (irota + 1) % 2;
		if (lmatch) {
		    mode = 0;
		}
		ususcu_("Y", &uymin, &uymax, &vymin, &vymax, &mode, (ftnlen)1)
			;
		usrget_("DYT", &dyt, (ftnlen)3);
		usrget_("DYL", &dyl, (ftnlen)3);
		if (*(unsigned char *)cs == 'U') {
		    *(unsigned char *)cs = 'V';
		}
		usaxdv_(cs, &dyt, &dyl, (ftnlen)1);
	    } else if (itr == 2 || itr == 4) {
		ususcl_("Y", &uymin, &uymax, &vymin, &vymax, (ftnlen)1);
		usiget_("NLBLY", &nlbl, (ftnlen)5);
		usiget_("NTICKSY", &nticks, (ftnlen)7);
		usiget_("ITYPEY", &itypey, (ftnlen)6);
		uliget_("IYTYPE", &it, (ftnlen)6);
		uliset_("IYTYPE", &itypey, (ftnlen)6);
		usaxlg_(cs, &nlbl, &nticks, (ftnlen)1);
		uliset_("IYTYPE", &it, (ftnlen)6);
	    } else {
		msgdmp_("E", "USYAXS", "INVALID TRANSFORMATION NUMBER.", (
			ftnlen)1, (ftnlen)6, (ftnlen)30);
	    }
	}
/* L100: */
    }
    uzlset_("LOFFSET", &loff, (ftnlen)7);
    return 0;
} /* usaxsc_ */


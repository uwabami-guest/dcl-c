/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__3 = 3;
static logical c_true = TRUE_;
static integer c__4 = 4;
static integer c__2 = 2;
static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     USPACK AUTO SCALING ROUTINE (UNIFORM)           DCL 5.0  95/09/04 */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int ususcu_(char *caxis, real *umin, real *umax, real *vmin, 
	real *vmax, integer *mode, ftnlen caxis_len)
{
    /* Initialized data */

    static real sc1[4] = { 1.f,2.f,5.f,10.f };
    static real sc2[4] = { 1.f,2.f,4.f,10.f };

    /* Format strings */
    static char fmt_500[] = "(\002(I\002,i1,\002)\002)";
    static char fmt_510[] = "(\002(F\002,i1,\002.\002,i1,\002)\002)";

    /* System generated locals */
    address a__1[3], a__2[2];
    integer i__1[3], i__2[2], i__3, i__4;

    /* Builtin functions */
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen);
    integer s_cmp(char *, char *, ftnlen, ftnlen), s_wsfi(icilist *), do_fio(
	    integer *, char *, ftnlen), e_wsfi(void);

    /* Local variables */
    static integer nb;
    static real cw, bx, u2v, fac, dul, dvl, dut, dul0;
    static char cfac[8];
    static integer ifac;
    static char coff[8];
    static real ufac;
    extern /* Subroutine */ int gnle_(real *, real *, integer *);
    static char cdul[8], cfmt[8];
    static integer ldgt;
    static char cmxd[8];
    static integer ndgt, maxd;
    static char cdut[8];
    static real uoff, rmin;
    static integer iexp;
    static real rmax;
    static integer ntry;
    static real ufac0, uoff0;
    static char ccfmt[8];
    extern real rgnge_(real *);
    static logical lepsl;
    extern /* Subroutine */ int gllget_(char *, logical *, ftnlen), gnsblk_(
	    real *, integer *);
    static real rundef;
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen), gnsave_(void)
	    ;
    static integer maxdgt;
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), gllset_(char *, logical *, ftnlen), uscget_(char 
	    *, char *, ftnlen, ftnlen), usiget_(char *, integer *, ftnlen), 
	    gnrset_(void), uscset_(char *, char *, ftnlen, ftnlen), usrget_(
	    char *, real *, ftnlen), uzrget_(char *, real *, ftnlen), uszdgt_(
	    real *, real *, real *, integer *, real *, real *, integer *, 
	    integer *), usrset_(char *, real *, ftnlen), usurdt_(real *, real 
	    *, real *, real *, real *);

    /* Fortran I/O blocks */
    static icilist io___34 = { 0, cfmt, 0, fmt_500, 8, 1 };
    static icilist io___35 = { 0, cfmt, 0, fmt_510, 8, 1 };


/* ------------------------- ARGUMENT CHECK ------------------------------ */
    if (*(unsigned char *)caxis != 'X' && *(unsigned char *)caxis != 'Y') {
	msgdmp_("E", "USUSCU", "INVALID CAXIS", (ftnlen)1, (ftnlen)6, (ftnlen)
		13);
    }
    if (*vmin >= *vmax) {
	msgdmp_("E", "USUSCU", "VMIN>VMAX", (ftnlen)1, (ftnlen)6, (ftnlen)9);
    }
/* -------------------------- PARAMETERS --------------------------------- */
    glrget_("RUNDEF", &rundef, (ftnlen)6);
    uzrget_("RSIZEL1", &cw, (ftnlen)7);
    if (*mode == 0) {
	usiget_("NBLANK1", &nb, (ftnlen)7);
    } else {
	usiget_("NBLANK2", &nb, (ftnlen)7);
    }
/* ------------------------------- DUT ----------------------------------- */
/* Writing concatenation */
    i__1[0] = 1, a__1[0] = "D";
    i__1[1] = 1, a__1[1] = caxis;
    i__1[2] = 1, a__1[2] = "T";
    s_cat(cdut, a__1, i__1, &c__3, (ftnlen)8);
    usrget_(cdut, &dut, (ftnlen)8);
    if (dut == rundef) {
	rmin = min(*umin,*umax);
	rmax = max(*umin,*umax);
	usurdt_(&rmin, &rmax, vmin, vmax, &dut);
	usrset_(cdut, &dut, (ftnlen)8);
    }
/* ------------------------------- DUL ----------------------------------- */
    gllget_("LEPSL", &lepsl, (ftnlen)5);
    gllset_("LEPSL", &c_true, (ftnlen)5);
    gnsave_();
    gnsblk_(sc1, &c__4);
    gnle_(&dut, &bx, &iexp);
    rmin = min(*umin,*umax);
    rmax = max(*umin,*umax);
    u2v = (*vmax - *vmin) / (rmax - rmin);
/* Writing concatenation */
    i__1[0] = 1, a__1[0] = "D";
    i__1[1] = 1, a__1[1] = caxis;
    i__1[2] = 1, a__1[2] = "L";
    s_cat(cdul, a__1, i__1, &c__3, (ftnlen)8);
/* Writing concatenation */
    i__2[0] = 1, a__2[0] = caxis;
    i__2[1] = 3, a__2[1] = "OFF";
    s_cat(coff, a__2, i__2, &c__2, (ftnlen)8);
/* Writing concatenation */
    i__2[0] = 1, a__2[0] = caxis;
    i__2[1] = 3, a__2[1] = "FAC";
    s_cat(cfac, a__2, i__2, &c__2, (ftnlen)8);
/* Writing concatenation */
    i__2[0] = 5, a__2[0] = "MXDGT";
    i__2[1] = 1, a__2[1] = caxis;
    s_cat(cmxd, a__2, i__2, &c__2, (ftnlen)8);
    usrget_(cdul, &dul, (ftnlen)8);
    usrget_(coff, &uoff, (ftnlen)8);
    usrget_(cfac, &ufac, (ftnlen)8);
    usiget_(cmxd, &maxdgt, (ftnlen)8);
    if (dul == rundef) {
	dul0 = dut * sc1[1];
	for (ntry = 1; ntry <= 2; ++ntry) {
	    if (*mode == 0) {
		uoff0 = uoff;
		ufac0 = ufac;
		uszdgt_(&rmin, &rmax, &dul0, &maxdgt, &uoff0, &ufac0, &ndgt, &
			ldgt);
	    } else {
		ndgt = 1;
	    }
	    fac = cw * (ndgt + nb) / (u2v * dut);
	    if (bx == 5.f) {
		gnsblk_(sc2, &c__4);
		fac = rgnge_(&fac);
	    } else {
		gnsblk_(sc1, &c__4);
		fac = rgnge_(&fac);
	    }
	    if (fac < 2.f) {
		fac = 2.f;
	    }
	    ifac = (integer) (fac + .1f);
	    dul = dut * ifac;
	    dul = max(dul,dul0);
	    if (ifac < 10) {
		goto L200;
	    }
	    dul0 = dul / 2.f;
/* L100: */
	}
L200:
	usrset_(cdul, &dul, (ftnlen)8);
    }
/* ------------------------------ CFMT ---------------------------------- */
/* Writing concatenation */
    i__1[0] = 1, a__1[0] = "C";
    i__1[1] = 1, a__1[1] = caxis;
    i__1[2] = 3, a__1[2] = "FMT";
    s_cat(ccfmt, a__1, i__1, &c__3, (ftnlen)8);
    uscget_(ccfmt, cfmt, (ftnlen)8, (ftnlen)8);
    if (s_cmp(cfmt, " ", (ftnlen)8, (ftnlen)1) == 0) {
	dvl = dul * u2v;
	if (*mode == 0) {
/* Computing MIN */
	    i__3 = maxdgt, i__4 = (integer) (dvl / cw) - nb;
	    maxd = min(i__3,i__4);
	} else {
	    maxd = maxdgt;
	}
	uszdgt_(&rmin, &rmax, &dul, &maxd, &uoff, &ufac, &ndgt, &ldgt);
	if (ldgt == 0) {
	    s_wsfi(&io___34);
	    do_fio(&c__1, (char *)&ndgt, (ftnlen)sizeof(integer));
	    e_wsfi();
	} else {
	    s_wsfi(&io___35);
	    do_fio(&c__1, (char *)&ndgt, (ftnlen)sizeof(integer));
	    do_fio(&c__1, (char *)&ldgt, (ftnlen)sizeof(integer));
	    e_wsfi();
	}
	usrset_(coff, &uoff, (ftnlen)8);
	usrset_(cfac, &ufac, (ftnlen)8);
	uscset_(ccfmt, cfmt, (ftnlen)8, (ftnlen)8);
    }
    gnrset_();
    gllset_("LEPSL", &lepsl, (ftnlen)5);
    return 0;
} /* ususcu_ */


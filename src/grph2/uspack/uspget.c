/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     USPGET / USPSET / USPSTX */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uspget_0_(int n__, char *cp, integer *ipara, ftnlen 
	cp_len)
{
    static char cl[40];
    static integer ip;
    static char cx[8];
    static integer it, idx;
    extern /* Subroutine */ int rliget_(char *, integer *, integer *, ftnlen),
	     rllget_(char *, integer *, integer *, ftnlen), rtiget_(char *, 
	    char *, integer *, integer *, ftnlen, ftnlen), usiqid_(char *, 
	    integer *, ftnlen), rlrget_(char *, integer *, integer *, ftnlen),
	     rtlget_(char *, char *, integer *, integer *, ftnlen, ftnlen), 
	    uslqid_(char *, integer *, ftnlen), uspqid_(char *, integer *, 
	    ftnlen), uspqcl_(integer *, char *, ftnlen), rtrget_(char *, char 
	    *, integer *, integer *, ftnlen, ftnlen), usrqid_(char *, integer 
	    *, ftnlen), uspqcp_(integer *, char *, ftnlen), uspqit_(integer *,
	     integer *), usisvl_(integer *, integer *), uslsvl_(integer *, 
	    integer *), uspqvl_(integer *, integer *), uspsvl_(integer *, 
	    integer *), usrsvl_(integer *, integer *);

    switch(n__) {
	case 1: goto L_uspset;
	case 2: goto L_uspstx;
	}

    uspqid_(cp, &idx, cp_len);
    uspqvl_(&idx, ipara);
    return 0;
/* ----------------------------------------------------------------------- */

L_uspset:
    uspqid_(cp, &idx, cp_len);
    uspsvl_(&idx, ipara);
    return 0;
/* ----------------------------------------------------------------------- */

L_uspstx:
    ip = *ipara;
    uspqid_(cp, &idx, cp_len);
    uspqit_(&idx, &it);
    uspqcp_(&idx, cx, (ftnlen)8);
    uspqcl_(&idx, cl, (ftnlen)40);
    if (it == 1) {
	rtiget_("US", cx, &ip, &c__1, (ftnlen)2, (ftnlen)8);
	rliget_(cl, &ip, &c__1, (ftnlen)40);
	usiqid_(cp, &idx, cp_len);
	usisvl_(&idx, &ip);
    } else if (it == 2) {
	rtlget_("US", cx, &ip, &c__1, (ftnlen)2, (ftnlen)8);
	rllget_(cl, &ip, &c__1, (ftnlen)40);
	uslqid_(cp, &idx, cp_len);
	uslsvl_(&idx, &ip);
    } else if (it == 3) {
	rtrget_("US", cx, &ip, &c__1, (ftnlen)2, (ftnlen)8);
	rlrget_(cl, &ip, &c__1, (ftnlen)40);
	usrqid_(cp, &idx, cp_len);
	usrsvl_(&idx, &ip);
    }
    return 0;
} /* uspget_ */

/* Subroutine */ int uspget_(char *cp, integer *ipara, ftnlen cp_len)
{
    return uspget_0_(0, cp, ipara, cp_len);
    }

/* Subroutine */ int uspset_(char *cp, integer *ipara, ftnlen cp_len)
{
    return uspget_0_(1, cp, ipara, cp_len);
    }

/* Subroutine */ int uspstx_(char *cp, integer *ipara, ftnlen cp_len)
{
    return uspget_0_(2, cp, ipara, cp_len);
    }


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;
static integer c__2 = 2;

/* ----------------------------------------------------------------------- */
/*     USPACK SET POINTS                             S.Sakai    95/03/02 */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int usspnt_(integer *n, real *x, real *y)
{
    extern real rmin_(real *, integer *, integer *), rmax_(real *, integer *, 
	    integer *);
    static real xmind[2], ymind[2], xmaxd[2], ymaxd[2], rundef;
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen);
    static real xdtmin, ydtmin, xdtmax, ydtmax;
    extern /* Subroutine */ int usrget_(char *, real *, ftnlen), usrset_(char 
	    *, real *, ftnlen);

    /* Parameter adjustments */
    --y;
    --x;

    /* Function Body */
    glrget_("RUNDEF", &rundef, (ftnlen)6);
/* ---------------------------- XMAX/XMIN -------------------------------- */
    if (x[1] != rundef) {
	usrget_("XDTMIN", xmind, (ftnlen)6);
	usrget_("XDTMAX", xmaxd, (ftnlen)6);
	xmind[1] = rmin_(&x[1], n, &c__1);
	xmaxd[1] = rmax_(&x[1], n, &c__1);
	if (xmind[0] != rundef) {
	    xdtmin = rmin_(xmind, &c__2, &c__1);
	    xdtmax = rmax_(xmaxd, &c__2, &c__1);
	} else {
	    xdtmin = xmind[1];
	    xdtmax = xmaxd[1];
	}
	usrset_("XDTMIN", &xdtmin, (ftnlen)6);
	usrset_("XDTMAX", &xdtmax, (ftnlen)6);
    }
/* ---------------------------- YMAX/YMIN -------------------------------- */
    if (y[1] != rundef) {
	usrget_("YDTMIN", ymind, (ftnlen)6);
	usrget_("YDTMAX", ymaxd, (ftnlen)6);
	ymind[1] = rmin_(&y[1], n, &c__1);
	ymaxd[1] = rmax_(&y[1], n, &c__1);
	if (ymind[0] != rundef) {
	    ydtmin = rmin_(ymind, &c__2, &c__1);
	    ydtmax = rmax_(ymaxd, &c__2, &c__1);
	} else {
	    ydtmin = ymind[1];
	    ydtmax = ymaxd[1];
	}
	usrset_("YDTMIN", &ydtmin, (ftnlen)6);
	usrset_("YDTMAX", &ydtmax, (ftnlen)6);
    }
    return 0;
} /* usspnt_ */


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__3 = 3;
static integer c__13 = 13;

/* ----------------------------------------------------------------------- */
/*     USIQNP / USIQID / USIQCP / USIQVL / USISVL */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int usiqnp_0_(int n__, integer *ncp, char *cp, integer *idx, 
	integer *ipara, integer *in, ftnlen cp_len)
{
    /* Initialized data */

    static char cparas[8*13] = "IRESET  " "MXDGTX  " "MXDGTY  " "NBLANK1 " 
	    "NBLANK2 " "NLBLX   " "NLBLY   " "NTICKSX " "NTICKSY " "ITYPEX  " 
	    "ITYPEY  " "MXDGTSX " "MXDGTSY ";
    static integer ix[13] = { 1,4,4,1,2,-999,-999,-999,-999,-999,-999,6,6 };
    static char cparal[40*13] = "****IRESET                              " 
	    "X_LABEL_MAX_CHAR                        " "X_LABEL_MAX_CHAR    "
	    "                    " "LABEL_GAP_PARALLEL                      " 
	    "LABEL_GAP_RIGHT_ANGLE                   " "LOG_X_LABEL_NUMBER  "
	    "                    " "LOG_Y_LABEL_NUMBER                      " 
	    "LOG_X_TICKS_NUMBER                      " "LOG_X_TICKS_NUMBER  "
	    "                    " "LOG_X_LABEL_FORMAT                      " 
	    "LOG_X_LABEL_FORMAT                      " "X_SUBLABEL_MAX_CHAR "
	    "                    " "Y_SUBLABEL_MAX_CHAR                     ";
    static logical lfirst = TRUE_;

    /* System generated locals */
    address a__1[3];
    integer i__1[3];

    /* Builtin functions */
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen),
	     s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer n;
    extern integer lenc_(char *, ftnlen);
    static char cmsg[80];
    extern logical lchreq_(char *, char *, ftnlen, ftnlen);
    extern /* Subroutine */ int rliget_(char *, integer *, integer *, ftnlen),
	     msgdmp_(char *, char *, char *, ftnlen, ftnlen, ftnlen), rtiget_(
	    char *, char *, integer *, integer *, ftnlen, ftnlen);

/* ---- SHORT NAME ---- */
/*     / CONTROL PARAMETERS / */
    switch(n__) {
	case 1: goto L_usiqid;
	case 2: goto L_usiqcp;
	case 3: goto L_usiqcl;
	case 4: goto L_usiqvl;
	case 5: goto L_usisvl;
	case 6: goto L_usiqin;
	}

/*     / PARAMETERS FOR USUSCU / */
/*     / PARAMETERS FOR USUSCL / */
/*     / PARAMETERS FOR USXSUB & USYSUB / */
/* ---- LONG NAME ---- */
/*     / CONTROL PARAMETERS / */
/*     / PARAMETERS FOR USUSCU / */
/*     / PARAMETERS FOR USUSCL / */
/*     / PARAMETERS FOR USXSUB & USYSUB / */
    *ncp = 13;
    return 0;
/* ----------------------------------------------------------------------- */

L_usiqid:
    for (n = 1; n <= 13; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *idx = n;
	    return 0;
	}
/* L10: */
    }
/* Writing concatenation */
    i__1[0] = 11, a__1[0] = "PARAMETER '";
    i__1[1] = lenc_(cp, cp_len), a__1[1] = cp;
    i__1[2] = 17, a__1[2] = "' IS NOT DEFINED.";
    s_cat(cmsg, a__1, i__1, &c__3, (ftnlen)80);
    msgdmp_("E", "USIQID", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
    return 0;
/* ----------------------------------------------------------------------- */

L_usiqcp:
    if (1 <= *idx && *idx <= 13) {
	s_copy(cp, cparas + (*idx - 1 << 3), cp_len, (ftnlen)8);
    } else {
	msgdmp_("E", "USIQCP", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_usiqcl:
    if (1 <= *idx && *idx <= 13) {
	s_copy(cp, cparal + (*idx - 1) * 40, cp_len, (ftnlen)40);
    } else {
	msgdmp_("E", "USIQCL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_usiqvl:
    if (lfirst) {
	rtiget_("US", cparas, ix, &c__13, (ftnlen)2, (ftnlen)8);
	rliget_(cparal, ix, &c__13, (ftnlen)40);
	lfirst = FALSE_;
    }
    if (1 <= *idx && *idx <= 13) {
	*ipara = ix[*idx - 1];
    } else {
	msgdmp_("E", "USIQVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_usisvl:
    if (lfirst) {
	rtiget_("US", cparas, ix, &c__13, (ftnlen)2, (ftnlen)8);
	rliget_(cparal, ix, &c__13, (ftnlen)40);
	lfirst = FALSE_;
    }
    if (1 <= *idx && *idx <= 13) {
	ix[*idx - 1] = *ipara;
    } else {
	msgdmp_("E", "USISVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_usiqin:
    for (n = 1; n <= 13; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *in = n;
	    return 0;
	}
/* L20: */
    }
    *in = 0;
    return 0;
} /* usiqnp_ */

/* Subroutine */ int usiqnp_(integer *ncp)
{
    return usiqnp_0_(0, ncp, (char *)0, (integer *)0, (integer *)0, (integer *
	    )0, (ftnint)0);
    }

/* Subroutine */ int usiqid_(char *cp, integer *idx, ftnlen cp_len)
{
    return usiqnp_0_(1, (integer *)0, cp, idx, (integer *)0, (integer *)0, 
	    cp_len);
    }

/* Subroutine */ int usiqcp_(integer *idx, char *cp, ftnlen cp_len)
{
    return usiqnp_0_(2, (integer *)0, cp, idx, (integer *)0, (integer *)0, 
	    cp_len);
    }

/* Subroutine */ int usiqcl_(integer *idx, char *cp, ftnlen cp_len)
{
    return usiqnp_0_(3, (integer *)0, cp, idx, (integer *)0, (integer *)0, 
	    cp_len);
    }

/* Subroutine */ int usiqvl_(integer *idx, integer *ipara)
{
    return usiqnp_0_(4, (integer *)0, (char *)0, idx, ipara, (integer *)0, (
	    ftnint)0);
    }

/* Subroutine */ int usisvl_(integer *idx, integer *ipara)
{
    return usiqnp_0_(5, (integer *)0, (char *)0, idx, ipara, (integer *)0, (
	    ftnint)0);
    }

/* Subroutine */ int usiqin_(char *cp, integer *in, ftnlen cp_len)
{
    return usiqnp_0_(6, (integer *)0, cp, (integer *)0, (integer *)0, in, 
	    cp_len);
    }


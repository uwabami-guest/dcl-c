/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     USRGET / USRSET / USRSTX */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int usrget_0_(int n__, char *cp, real *rpara, ftnlen cp_len)
{
    static char cl[40], cx[8];
    static real rp;
    static integer idx;
    extern /* Subroutine */ int rlrget_(char *, real *, integer *, ftnlen), 
	    usrqid_(char *, integer *, ftnlen), rtrget_(char *, char *, real *
	    , integer *, ftnlen, ftnlen), usrqcl_(integer *, char *, ftnlen), 
	    usrqcp_(integer *, char *, ftnlen), usrqvl_(integer *, real *), 
	    usrsvl_(integer *, real *);

    switch(n__) {
	case 1: goto L_usrset;
	case 2: goto L_usrstx;
	}

    usrqid_(cp, &idx, cp_len);
    usrqvl_(&idx, rpara);
    return 0;
/* ----------------------------------------------------------------------- */

L_usrset:
    usrqid_(cp, &idx, cp_len);
    usrsvl_(&idx, rpara);
    return 0;
/* ----------------------------------------------------------------------- */

L_usrstx:
    rp = *rpara;
    usrqid_(cp, &idx, cp_len);
/*     / SHORT NAME / */
    usrqcp_(&idx, cx, (ftnlen)8);
    rtrget_("US", cx, &rp, &c__1, (ftnlen)2, (ftnlen)8);
/*     / LONG NAME / */
    usrqcl_(&idx, cl, (ftnlen)40);
    rlrget_(cl, &rp, &c__1, (ftnlen)40);
    usrsvl_(&idx, &rp);
    return 0;
} /* usrget_ */

/* Subroutine */ int usrget_(char *cp, real *rpara, ftnlen cp_len)
{
    return usrget_0_(0, cp, rpara, cp_len);
    }

/* Subroutine */ int usrset_(char *cp, real *rpara, ftnlen cp_len)
{
    return usrget_0_(1, cp, rpara, cp_len);
    }

/* Subroutine */ int usrstx_(char *cp, real *rpara, ftnlen cp_len)
{
    return usrget_0_(2, cp, rpara, cp_len);
    }


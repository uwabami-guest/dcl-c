/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static logical c_true = TRUE_;
static doublereal c_b11 = 10.;
static integer c__4 = 4;

/* ----------------------------------------------------------------------- */
/*     USPACK ROUND UMIN AND UMAX (LOG)               S. Sakai  90/03/04 */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int usurdl_(real *umin, real *umax, real *vmin, real *vmax)
{
    /* Initialized data */

    static real sc[4] = { 1.f,2.f,5.f,10.f };

    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    double r_lg10(real *), pow_di(doublereal *, integer *);

    /* Local variables */
    extern /* Subroutine */ int gnge_(real *, real *, integer *), gnle_(real *
	    , real *, integer *);
    extern integer irge_(real *), irle_(real *);
    static integer nlog;
    static real bumin, bumax;
    static integer ipmin, ipmax;
    static logical lepsl;
    static real rlmin, rlmax;
    extern /* Subroutine */ int gllget_(char *, logical *, ftnlen), gnsblk_(
	    real *, integer *), gnsave_(void), msgdmp_(char *, char *, char *,
	     ftnlen, ftnlen, ftnlen), gllset_(char *, logical *, ftnlen), 
	    gnrset_(void);

/* ------------------------- ARGUMENT CHECK ------------------------------ */
    if (*umin <= 0.f || *umax <= 0.f) {
	msgdmp_("E", "USURDL", "NEGATIVE UMIN OR UMAX.", (ftnlen)1, (ftnlen)6,
		 (ftnlen)22);
    }
    if (*umin > *umax) {
	msgdmp_("E", "USURDL", "UMIN > UMAX.", (ftnlen)1, (ftnlen)6, (ftnlen)
		12);
    }
/* ----------------------------------------------------------------------- */
    gllget_("LEPSL", &lepsl, (ftnlen)5);
    gllset_("LEPSL", &c_true, (ftnlen)5);
    rlmax = r_lg10(umax);
    rlmin = r_lg10(umin);
    nlog = irge_(&rlmax) - irle_(&rlmin);
    if (nlog >= 4) {
	i__1 = irge_(&rlmax);
	*umax = pow_di(&c_b11, &i__1);
	i__1 = irle_(&rlmin);
	*umin = pow_di(&c_b11, &i__1);
    } else {
	gnsave_();
	gnsblk_(sc, &c__4);
	gnge_(umax, &bumax, &ipmax);
	gnle_(umin, &bumin, &ipmin);
	*umax = bumax * pow_di(&c_b11, &ipmax);
	*umin = bumin * pow_di(&c_b11, &ipmin);
	gnrset_();
    }
    gllset_("LEPSL", &lepsl, (ftnlen)5);
    return 0;
} /* usurdl_ */


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     USPACK EASY GRAPH */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int usgrph_(integer *n, real *x, real *y)
{
    extern /* Subroutine */ int uulin_(integer *, real *, real *), grstrf_(
	    void), usdaxs_(void), uspfit_(void), usspnt_(integer *, real *, 
	    real *);

    /* Parameter adjustments */
    --y;
    --x;

    /* Function Body */
    usspnt_(n, &x[1], &y[1]);
    uspfit_();
    grstrf_();
    usdaxs_();
    uulin_(n, &x[1], &y[1]);
    return 0;
} /* usgrph_ */


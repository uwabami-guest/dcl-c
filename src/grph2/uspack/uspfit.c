/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     USPACK SET PARAMETER */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uspfit_(void)
{
    /* Builtin functions */
    integer i_indx(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static real dt, cw, rmc;
    static integer itr;
    static real xfac, yfac;
    static logical loff;
    static real xoff, yoff, xmin, ymin, xmax, ymax;
    static integer itrw;
    static real rmrgn;
    static logical lxinv, lyinv;
    static real vxmin, vxmax, vymin, vymax, wxmin, wxmax, wymin, wymax, uxmax,
	     uxmin, uymax, uymin, vxmin0, vymin0, vxmax0, vymax0;
    static char cxside[2], cyside[2];
    extern /* Subroutine */ int sgiget_(char *, integer *, ftnlen);
    static real rundef;
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen), msgdmp_(char 
	    *, char *, char *, ftnlen, ftnlen, ftnlen), uscget_(char *, char *
	    , ftnlen, ftnlen), sgrget_(char *, real *, ftnlen), cupper_(char *
	    , ftnlen), uslget_(char *, logical *, ftnlen), sgswnd_(real *, 
	    real *, real *, real *), usrget_(char *, real *, ftnlen), uzlget_(
	    char *, logical *, ftnlen), usurdl_(real *, real *, real *, real *
	    ), uzrget_(char *, real *, ftnlen), sgstrn_(integer *), usurdt_(
	    real *, real *, real *, real *, real *), sgsvpt_(real *, real *, 
	    real *, real *), uswapz_(real *, real *, integer *);
    static real uxuser;
    extern /* Subroutine */ int usspnt_(integer *, real *, real *);
    static real uyuser;
    extern /* Subroutine */ int stqwtr_(real *, real *, real *, real *, real *
	    , real *, real *, real *, integer *);

    glrget_("RUNDEF", &rundef, (ftnlen)6);
    uscget_("CXSIDE", cxside, (ftnlen)6, (ftnlen)2);
    uscget_("CYSIDE", cyside, (ftnlen)6, (ftnlen)2);
    cupper_(cxside, (ftnlen)2);
    cupper_(cyside, (ftnlen)2);
    if (i_indx(cyside, "U", (ftnlen)2, (ftnlen)1) != 0) {
	uzrget_("UXUSER", &uxuser, (ftnlen)6);
	usspnt_(&c__1, &uxuser, &rundef);
    }
    if (i_indx(cxside, "U", (ftnlen)2, (ftnlen)1) != 0) {
	uzrget_("UYUSER", &uyuser, (ftnlen)6);
	usspnt_(&c__1, &rundef, &uyuser);
    }
    sgiget_("ITR", &itr, (ftnlen)3);
    uslget_("LXINV", &lxinv, (ftnlen)5);
    uslget_("LYINV", &lyinv, (ftnlen)5);
    uzlget_("LOFFSET", &loff, (ftnlen)7);
/* --------------------------- VIEW PORT --------------------------------- */
    sgrget_("VXMIN", &vxmin, (ftnlen)5);
    sgrget_("VXMAX", &vxmax, (ftnlen)5);
    sgrget_("VYMIN", &vymin, (ftnlen)5);
    sgrget_("VYMAX", &vymax, (ftnlen)5);
    usrget_("RMRGN", &rmrgn, (ftnlen)5);
    uzrget_("RSIZEL1", &cw, (ftnlen)7);
    rmc = rmrgn * cw;
    stqwtr_(&vxmin0, &vxmax0, &vymin0, &vymax0, &wxmin, &wxmax, &wymin, &
	    wymax, &itrw);
    if (vxmin == rundef) {
	vxmin = vxmin0 + rmc;
    }
    if (vxmax == rundef) {
	vxmax = vxmax0 - rmc;
    }
    if (vymin == rundef) {
	vymin = vymin0 + rmc;
    }
    if (vymax == rundef) {
	vymax = vymax0 - rmc;
    }
/* ----------------------------- X-AXIS ---------------------------------- */
    sgrget_("UXMAX", &xmax, (ftnlen)5);
    sgrget_("UXMIN", &xmin, (ftnlen)5);
    if (xmin != rundef && xmax != rundef && xmin > xmax) {
	lxinv = TRUE_;
	uswapz_(&xmin, &xmax, &c__1);
    }
    usrget_("XDTMAX", &uxmax, (ftnlen)6);
    usrget_("XDTMIN", &uxmin, (ftnlen)6);
    if (xmin != rundef) {
	uxmin = xmin;
    }
    if (xmax != rundef) {
	uxmax = xmax;
    }
    if (uxmin == rundef || uxmax == rundef) {
	msgdmp_("E", "USPFIT", "XMIN OR XMAX IS NOT DEFINED.", (ftnlen)1, (
		ftnlen)6, (ftnlen)28);
    }
    if (loff) {
	uzrget_("XOFFSET", &xoff, (ftnlen)7);
	uzrget_("XFACT", &xfac, (ftnlen)5);
	uxmin = xfac * uxmin + xoff;
	uxmax = xfac * uxmax + xoff;
    }
    if (itr == 1 || itr == 2) {
	usurdt_(&uxmin, &uxmax, &vxmin, &vxmax, &dt);
    } else if ((real) itr == 3.f || itr == 4) {
	usurdl_(&uxmin, &uxmax, &vxmin, &vxmax);
    } else {
	msgdmp_("E", "USPFIT", "INVALID ITR", (ftnlen)1, (ftnlen)6, (ftnlen)
		11);
    }
    if (xmin != rundef) {
	uxmin = xmin;
    }
    if (xmax != rundef) {
	uxmax = xmax;
    }
/* ----------------------------- Y-AXIS ---------------------------------- */
    sgrget_("UYMAX", &ymax, (ftnlen)5);
    sgrget_("UYMIN", &ymin, (ftnlen)5);
    if (ymin != rundef && ymax != rundef && ymin > ymax) {
	lyinv = TRUE_;
	uswapz_(&ymin, &ymax, &c__1);
    }
    usrget_("YDTMAX", &uymax, (ftnlen)6);
    usrget_("YDTMIN", &uymin, (ftnlen)6);
    if (ymin != rundef) {
	uymin = ymin;
    }
    if (ymax != rundef) {
	uymax = ymax;
    }
    if (uymin == rundef || uymax == rundef) {
	msgdmp_("E", "USPFIT", "YMIN OR YMAX IS NOT DEFINED.", (ftnlen)1, (
		ftnlen)6, (ftnlen)28);
    }
    if (loff) {
	uzrget_("YOFFSET", &yoff, (ftnlen)7);
	uzrget_("YFACT", &yfac, (ftnlen)5);
	uymin = yfac * uymin + yoff;
	uymax = yfac * uymax + yoff;
    }
    if (itr == 1 || itr == 3) {
	usurdt_(&uymin, &uymax, &vymin, &vymax, &dt);
    } else {
	usurdl_(&uymin, &uymax, &vymin, &vymax);
    }
    if (ymin != rundef) {
	uymin = ymin;
    }
    if (ymax != rundef) {
	uymax = ymax;
    }
/* ----------------------------------------------------------------------- */
    if (lxinv) {
	uswapz_(&uxmin, &uxmax, &c__1);
    }
    if (lyinv) {
	uswapz_(&uymin, &uymax, &c__1);
    }
    sgswnd_(&uxmin, &uxmax, &uymin, &uymax);
    sgsvpt_(&vxmin, &vxmax, &vymin, &vymax);
    sgstrn_(&itr);
    return 0;
} /* uspfit_ */


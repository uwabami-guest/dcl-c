/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*    USPACK SETTING TITLE                             S.Sakai  92/02/29 */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int ussttl_(char *cxttl, char *cxunit, char *cyttl, char *
	cyunit, ftnlen cxttl_len, ftnlen cxunit_len, ftnlen cyttl_len, ftnlen 
	cyunit_len)
{
    extern /* Subroutine */ int uscstx_(char *, char *, ftnlen, ftnlen);

    uscstx_("CXTTL", cxttl, (ftnlen)5, cxttl_len);
    uscstx_("CXUNIT", cxunit, (ftnlen)6, cxunit_len);
    uscstx_("CYTTL", cyttl, (ftnlen)5, cyttl_len);
    uscstx_("CYUNIT", cyunit, (ftnlen)6, cyunit_len);
    return 0;
} /* ussttl_ */


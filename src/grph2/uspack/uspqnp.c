/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__3 = 3;

/* ----------------------------------------------------------------------- */
/*     USPQNP / USPQID / USPQCP / USPQVL / USPSVL */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uspqnp_0_(int n__, integer *ncp, char *cp, integer *idx, 
	integer *itp, integer *ipara, integer *in, ftnlen cp_len)
{
    /* Initialized data */

    static char cparas[8*49] = "IRESET  " "LXINV   " "LYINV   " "LMATCH  " 
	    "RMRGN   " "XOFF    " "YOFF    " "XFAC    " "YFAC    " "DXT     " 
	    "DYT     " "DXL     " "DYL     " "TFACT   " "MXDGTX  " "MXDGTY  " 
	    "NBLANK1 " "NBLANK2 " "NLBLX   " "NLBLY   " "NTICKSX " "NTICKSY " 
	    "ITYPEX  " "ITYPEY  " "MXDGTSX " "MXDGTSY " "LPRTCT  " "LXSUB   " 
	    "LYSUB   " "SOFFXTR " "SOFFXBR " "SOFFXUR " "SOFFXTL " "SOFFXBL " 
	    "SOFFXUL " "SOFFYRT " "SOFFYLT " "SOFFYUT " "SOFFYRB " "SOFFYLB " 
	    "SOFFYUB " "ROFFXT  " "ROFFXB  " "ROFFYR  " "ROFFYL  " "XDTMIN  " 
	    "XDTMAX  " "YDTMIN  " "YDTMAX  ";
    static integer itype[49] = { 1,2,2,2,3,3,3,3,3,3,3,3,3,3,1,1,1,1,1,1,1,1,
	    1,1,1,1,2,2,2,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3 };
    static char cparal[40*49] = "****IRESET                              " 
	    "INV_X_AXIS                              " "INV_Y_AXIS          "
	    "                    " "ENABLE_XY_MATCHING                      " 
	    "MARGIN_WIDTH                            " "X_LABEL_OFFSET      "
	    "                    " "Y_LABEL_OFFSET                          " 
	    "X_LABEL_FACTOR                          " "Y_LABEL_FACTOR      "
	    "                    " "X_TICK_INTERVAL                         " 
	    "Y_TICK_INTERVAL                         " "X_LABEL_INTERVAL    "
	    "                    " "Y_LABEL_INTERVAL                        " 
	    "MAX_TICK_INTERVAL                       " "X_LABEL_MAX_CHAR    "
	    "                    " "X_LABEL_MAX_CHAR                        " 
	    "LABEL_GAP_PARALLEL                      " "LABEL_GAP_RIGHT_ANGL"
	    "E                   " "LOG_X_LABEL_NUMBER                      " 
	    "LOG_Y_LABEL_NUMBER                      " "LOG_X_TICKS_NUMBER  "
	    "                    " "LOG_X_TICKS_NUMBER                      " 
	    "LOG_X_LABEL_FORMAT                      " "LOG_X_LABEL_FORMAT  "
	    "                    " "X_SUBLABEL_MAX_CHAR                     " 
	    "Y_SUBLABEL_MAX_CHAR                     " "PROTECT_REGION      "
	    "                    " "DRAW_X_SUBLABEL                         " 
	    "DRAW_Y_SUBLABEL                         " "****SOFFXTR         "
	    "                    " "****SOFFXBR                             " 
	    "****SOFFXUR                             " "****SOFFXTL         "
	    "                    " "****SOFFXBL                             " 
	    "****SOFFXUL                             " "****SOFFYRT         "
	    "                    " "****SOFFYLT                             " 
	    "****SOFFYUT                             " "****SOFFYRB         "
	    "                    " "****SOFFYLB                             " 
	    "****SOFFYUB                             " "****ROFFXT          "
	    "                    " "****ROFFXB                              " 
	    "****ROFFYR                              " "****ROFFYL          "
	    "                    " "****XDTMIN                              " 
	    "****XDTMAX                              " "****YDTMIN          "
	    "                    " "****YDTMAX                              ";

    /* System generated locals */
    address a__1[3];
    integer i__1[3];

    /* Builtin functions */
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen),
	     s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer n, id;
    extern integer lenc_(char *, ftnlen);
    static char cmsg[80];
    extern logical lchreq_(char *, char *, ftnlen, ftnlen);
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), usiqid_(char *, integer *, ftnlen), uslqid_(char 
	    *, integer *, ftnlen), usrqid_(char *, integer *, ftnlen), 
	    usiqvl_(integer *, integer *), uslqvl_(integer *, integer *), 
	    usisvl_(integer *, integer *), uslsvl_(integer *, integer *), 
	    usrqvl_(integer *, integer *), usrsvl_(integer *, integer *);

/* ---- SHORT NAME ---- */
/*     / CONTROL PARAMETERS / */
    switch(n__) {
	case 1: goto L_uspqid;
	case 2: goto L_uspqcp;
	case 3: goto L_uspqcl;
	case 4: goto L_uspqit;
	case 5: goto L_uspqvl;
	case 6: goto L_uspsvl;
	case 7: goto L_uspqin;
	}

/*     / PARAMETERS FOR USDAXS / */
/*     / PARAMETERS FOR USUSCU / */
/*     / PARAMETERS FOR USUSCL / */
/*     / PARAMETERS FOR USXSUB & USYSUB / */
/*     / FOLLOWING 20 PARAMETERS ARE RESET BY USINIZ / */
/* ---- LONG NAME ---- */
/*     / CONTROL PARAMETERS / */
/*     / PARAMETERS FOR USDAXS / */
/*     / PARAMETERS FOR USUSCU / */
/*     / PARAMETERS FOR USUSCL / */
/*     / PARAMETERS FOR USXSUB & USYSUB / */
/*     / FOLLOWING 20 PARAMETERS ARE RESET BY USINIZ / */
    *ncp = 49;
    return 0;
/* ----------------------------------------------------------------------- */

L_uspqid:
    for (n = 1; n <= 49; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *idx = n;
	    return 0;
	}
/* L10: */
    }
/* Writing concatenation */
    i__1[0] = 11, a__1[0] = "PARAMETER '";
    i__1[1] = lenc_(cp, cp_len), a__1[1] = cp;
    i__1[2] = 17, a__1[2] = "' IS NOT DEFINED.";
    s_cat(cmsg, a__1, i__1, &c__3, (ftnlen)80);
    msgdmp_("E", "USPQID", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
    return 0;
/* ----------------------------------------------------------------------- */

L_uspqcp:
    if (1 <= *idx && *idx <= 49) {
	s_copy(cp, cparas + (*idx - 1 << 3), cp_len, (ftnlen)8);
    } else {
	msgdmp_("E", "USPQCP", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_uspqcl:
    if (1 <= *idx && *idx <= 49) {
	s_copy(cp, cparal + (*idx - 1) * 40, cp_len, (ftnlen)40);
    } else {
	msgdmp_("E", "USPQCL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_uspqit:
    if (1 <= *idx && *idx <= 49) {
	*itp = itype[*idx - 1];
    } else {
	msgdmp_("E", "USPQIT", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_uspqvl:
    if (1 <= *idx && *idx <= 49) {
	if (itype[*idx - 1] == 1) {
	    usiqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    usiqvl_(&id, ipara);
	} else if (itype[*idx - 1] == 2) {
	    uslqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    uslqvl_(&id, ipara);
	} else if (itype[*idx - 1] == 3) {
	    usrqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    usrqvl_(&id, ipara);
	}
    } else {
	msgdmp_("E", "USPQVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_uspsvl:
    if (1 <= *idx && *idx <= 49) {
	if (itype[*idx - 1] == 1) {
	    usiqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    usisvl_(&id, ipara);
	} else if (itype[*idx - 1] == 2) {
	    uslqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    uslsvl_(&id, ipara);
	} else if (itype[*idx - 1] == 3) {
	    usrqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    usrsvl_(&id, ipara);
	}
    } else {
	msgdmp_("E", "USPSVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_uspqin:
    for (n = 1; n <= 49; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *in = n;
	    return 0;
	}
/* L20: */
    }
    *in = 0;
    return 0;
} /* uspqnp_ */

/* Subroutine */ int uspqnp_(integer *ncp)
{
    return uspqnp_0_(0, ncp, (char *)0, (integer *)0, (integer *)0, (integer *
	    )0, (integer *)0, (ftnint)0);
    }

/* Subroutine */ int uspqid_(char *cp, integer *idx, ftnlen cp_len)
{
    return uspqnp_0_(1, (integer *)0, cp, idx, (integer *)0, (integer *)0, (
	    integer *)0, cp_len);
    }

/* Subroutine */ int uspqcp_(integer *idx, char *cp, ftnlen cp_len)
{
    return uspqnp_0_(2, (integer *)0, cp, idx, (integer *)0, (integer *)0, (
	    integer *)0, cp_len);
    }

/* Subroutine */ int uspqcl_(integer *idx, char *cp, ftnlen cp_len)
{
    return uspqnp_0_(3, (integer *)0, cp, idx, (integer *)0, (integer *)0, (
	    integer *)0, cp_len);
    }

/* Subroutine */ int uspqit_(integer *idx, integer *itp)
{
    return uspqnp_0_(4, (integer *)0, (char *)0, idx, itp, (integer *)0, (
	    integer *)0, (ftnint)0);
    }

/* Subroutine */ int uspqvl_(integer *idx, integer *ipara)
{
    return uspqnp_0_(5, (integer *)0, (char *)0, idx, (integer *)0, ipara, (
	    integer *)0, (ftnint)0);
    }

/* Subroutine */ int uspsvl_(integer *idx, integer *ipara)
{
    return uspqnp_0_(6, (integer *)0, (char *)0, idx, (integer *)0, ipara, (
	    integer *)0, (ftnint)0);
    }

/* Subroutine */ int uspqin_(char *cp, integer *in, ftnlen cp_len)
{
    return uspqnp_0_(7, (integer *)0, cp, (integer *)0, (integer *)0, (
	    integer *)0, in, cp_len);
    }


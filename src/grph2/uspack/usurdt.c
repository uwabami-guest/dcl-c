/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__4 = 4;
static logical c_true = TRUE_;
static real c_b12 = 0.f;
static doublereal c_b16 = 10.;

/* ----------------------------------------------------------------------- */
/*     USPACK ROUND UMIN AND UMAX (UNIFORM)            S.Sakai  95/09/06 */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int usurdt_(real *umin, real *umax, real *vmin, real *vmax, 
	real *dut)
{
    /* Initialized data */

    static real sc1[4] = { 1.f,2.f,5.f,10.f };

    /* System generated locals */
    real r__1;

    /* Builtin functions */
    double pow_di(doublereal *, integer *);

    /* Local variables */
    static integer i__;
    static real ct, cw, bx, dvt;
    extern /* Subroutine */ int gnle_(real *, real *, integer *);
    extern integer irge_(real *), irle_(real *);
    extern logical lreq_(real *, real *);
    static integer iexp;
    static logical lepsl;
    extern /* Subroutine */ int gllget_(char *, logical *, ftnlen), gnsblk_(
	    real *, integer *), gnsave_(void), msgdmp_(char *, char *, char *,
	     ftnlen, ftnlen, ftnlen), gllset_(char *, logical *, ftnlen), 
	    gnrset_(void), usrget_(char *, real *, ftnlen), uzrget_(char *, 
	    real *, ftnlen);

/* ------------------------- ARGUMENT CHECK ------------------------------ */
    if (*umin > *umax) {
	msgdmp_("E", "USURDT", "UMIN > UMAX.", (ftnlen)1, (ftnlen)6, (ftnlen)
		12);
    }
    if (*vmin > *vmax) {
	msgdmp_("E", "USURDT", "VMIN > VMAX.", (ftnlen)1, (ftnlen)6, (ftnlen)
		12);
    }
/* ----------------------------------------------------------------------- */
    gnsave_();
    gnsblk_(sc1, &c__4);
    gllget_("LEPSL", &lepsl, (ftnlen)5);
    gllset_("LEPSL", &c_true, (ftnlen)5);
    if (lreq_(umax, umin)) {
	if (lreq_(umax, &c_b12)) {
	    *umax = 1.f;
	    *umin = -1.f;
	} else {
	    *umax = max(*umax,0.f);
	    *umin = min(*umin,0.f);
	}
    }
/* ----------------------------------------------------------------------- */
    usrget_("TFACT", &ct, (ftnlen)5);
    uzrget_("RSIZEL1", &cw, (ftnlen)7);
    dvt = cw * ct;
    for (i__ = 1; i__ <= 2; ++i__) {
	*dut = (r__1 = (*umax - *umin) / (*vmax - *vmin) * dvt, abs(r__1));
	gnle_(dut, &bx, &iexp);
	*dut = bx * pow_di(&c_b16, &iexp);
	r__1 = *umax / *dut;
	*umax = *dut * irge_(&r__1);
	r__1 = *umin / *dut;
	*umin = *dut * irle_(&r__1);
/* L100: */
    }
    gllset_("LEPSL", &lepsl, (ftnlen)5);
    gnrset_();
    return 0;
} /* usurdt_ */


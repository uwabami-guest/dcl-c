/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;
static integer c__2 = 2;

/* ----------------------------------------------------------------------- */
/*     UYPTMK : PLOT TICKMARKS */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uyptmk_(char *cside, integer *islct, real *uy, integer *
	n, ftnlen cside_len)
{
    /* System generated locals */
    address a__1[2];
    integer i__1[2];
    real r__1;
    char ch__1[6], ch__2[7];

    /* Builtin functions */
    integer s_wsfi(icilist *), do_fio(integer *, char *, ftnlen), e_wsfi(void)
	    ;
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen);
    integer i_sign(integer *, integer *);

    /* Local variables */
    static integer jsgn;
    static real uxmn, uymn, posx, uxmx, uymx;
    static integer iflag, index;
    static char cslct[1];
    static integer inner;
    static real rtick, roffy, rofgy, rsize;
    extern logical lchreq_(char *, char *, ftnlen, ftnlen);
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen);
    extern logical luychk_(char *, ftnlen);
    extern /* Subroutine */ int sgqwnd_(real *, real *, real *, real *), 
	    uziget_(char *, integer *, ftnlen), uzrget_(char *, real *, 
	    ftnlen), uzrset_(char *, real *, ftnlen), uyptmz_(real *, integer 
	    *, real *, real *, real *, integer *);

    /* Fortran I/O blocks */
    static icilist io___2 = { 0, cslct, 0, "(I1)", 1, 1 };


    /* Parameter adjustments */
    --uy;

    /* Function Body */
    if (! luychk_(cside, (ftnlen)1)) {
	msgdmp_("E", "UYPTMK", "SIDE PARAMETER IS INVALID.", (ftnlen)1, (
		ftnlen)6, (ftnlen)26);
    }
    if (! (0 <= *islct && *islct <= 2)) {
	msgdmp_("E", "UYPTMK", "'ISLCT' IS INVALID.", (ftnlen)1, (ftnlen)6, (
		ftnlen)19);
    }
    if (*n <= 0) {
	msgdmp_("E", "UYPTMK", "NUMBER OF POINTS IS INVALID.", (ftnlen)1, (
		ftnlen)6, (ftnlen)28);
    }
    s_wsfi(&io___2);
    do_fio(&c__1, (char *)&(*islct), (ftnlen)sizeof(integer));
    e_wsfi();
/* Writing concatenation */
    i__1[0] = 5, a__1[0] = "ROFFY";
    i__1[1] = 1, a__1[1] = cside;
    s_cat(ch__1, a__1, i__1, &c__2, (ftnlen)6);
    uzrget_(ch__1, &roffy, (ftnlen)6);
/* Writing concatenation */
    i__1[0] = 5, a__1[0] = "ROFGY";
    i__1[1] = 1, a__1[1] = cside;
    s_cat(ch__1, a__1, i__1, &c__2, (ftnlen)6);
    uzrget_(ch__1, &rofgy, (ftnlen)6);
/* Writing concatenation */
    i__1[0] = 6, a__1[0] = "INDEXT";
    i__1[1] = 1, a__1[1] = cslct;
    s_cat(ch__2, a__1, i__1, &c__2, (ftnlen)7);
    uziget_(ch__2, &index, (ftnlen)7);
/* Writing concatenation */
    i__1[0] = 6, a__1[0] = "RSIZET";
    i__1[1] = 1, a__1[1] = cslct;
    s_cat(ch__2, a__1, i__1, &c__2, (ftnlen)7);
    uzrget_(ch__2, &rsize, (ftnlen)7);
    uziget_("INNER", &inner, (ftnlen)5);
    jsgn = i_sign(&c__1, &inner);
    if (! lchreq_(cside, "U", (ftnlen)1, (ftnlen)1)) {
	sgqwnd_(&uxmn, &uxmx, &uymn, &uymx);
	if (lchreq_(cside, "L", (ftnlen)1, (ftnlen)1)) {
	    posx = uxmn;
	    iflag = -1;
	} else {
	    posx = uxmx;
	    iflag = 1;
	}
    } else {
	uzrget_("UXUSER", &posx, (ftnlen)6);
	uziget_("IFLAG", &iflag, (ftnlen)5);
	iflag = i_sign(&c__1, &iflag);
    }
    rtick = -rsize * jsgn * iflag;
    if (iflag >= 0) {
/* Computing MAX */
	r__1 = rofgy + rtick;
	roffy = max(r__1,roffy);
    } else {
/* Computing MIN */
	r__1 = rofgy + rtick;
	roffy = min(r__1,roffy);
    }
    uyptmz_(&uy[1], n, &posx, &rofgy, &rtick, &index);
/* Writing concatenation */
    i__1[0] = 5, a__1[0] = "ROFFY";
    i__1[1] = 1, a__1[1] = cside;
    s_cat(ch__1, a__1, i__1, &c__2, (ftnlen)6);
    uzrset_(ch__1, &roffy, (ftnlen)6);
    return 0;
} /* uyptmk_ */


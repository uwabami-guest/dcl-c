/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     LUYCHK : CHECK SIDE PARAMETER */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
logical luychk_(char *cside, ftnlen cside_len)
{
    /* System generated locals */
    logical ret_val;

    /* Local variables */
    static char csx[1];
    extern /* Subroutine */ int cupper_(char *, ftnlen);

    *(unsigned char *)csx = *(unsigned char *)cside;
    cupper_(csx, (ftnlen)1);
    ret_val = *(unsigned char *)csx == 'L' || *(unsigned char *)csx == 'R' || 
	    *(unsigned char *)csx == 'U';
    return ret_val;
} /* luychk_ */


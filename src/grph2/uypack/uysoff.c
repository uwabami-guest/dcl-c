/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     UYSOFF / UYROFF */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uysoff_0_(int n__)
{
    /* Initialized data */

    static logical lcall = FALSE_;

    static logical loff;
    static real yoff, yfct, fymn, fymx, uxmn, uymn, uxmx, uymx;
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), sgqwnd_(real *, real *, real *, real *), sgswnd_(
	    real *, real *, real *, real *), sgstrf_(void), uzlget_(char *, 
	    logical *, ftnlen), uzrget_(char *, real *, ftnlen);

    switch(n__) {
	case 1: goto L_uyroff;
	}

/*     / SCALING FACTOR FOR OFFSET / */
    uzlget_("LOFFSET", &loff, (ftnlen)7);
    if (loff) {
	uzrget_("YOFFSET", &yoff, (ftnlen)7);
	uzrget_("YFACT  ", &yfct, (ftnlen)7);
    } else {
	yoff = 0.f;
	yfct = 1.f;
    }
/*     / INQUIRE NORMALIZATION TRANSFORMATION / */
    sgqwnd_(&uxmn, &uxmx, &uymn, &uymx);
    fymn = yfct * uymn + yoff;
    fymx = yfct * uymx + yoff;
/*     / SET NORMALIZATION TRANSFORMATION / */
    sgswnd_(&uxmn, &uxmx, &fymn, &fymx);
    sgstrf_();
    lcall = TRUE_;
    return 0;
/* ----------------------------------------------------------------------- */

L_uyroff:
    if (! lcall) {
	msgdmp_("E", "UYROFF", "UYSOFF HAS NOT BEEN CALLED.", (ftnlen)1, (
		ftnlen)6, (ftnlen)27);
    }
/*     / RESET NORMALIZATION TRANSFORMATION / */
    sgswnd_(&uxmn, &uxmx, &uymn, &uymx);
    sgstrf_();
    return 0;
} /* uysoff_ */

/* Subroutine */ int uysoff_(void)
{
    return uysoff_0_(0);
    }

/* Subroutine */ int uyroff_(void)
{
    return uysoff_0_(1);
    }


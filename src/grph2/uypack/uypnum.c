/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__12 = 12;

/* ----------------------------------------------------------------------- */
/*     UYPNUM : PLOT NUMBERS */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uypnum_(char *cside, integer *islct, real *uy, integer *
	n, ftnlen cside_len)
{
    /* System generated locals */
    integer i__1;

    /* Local variables */
    static integer i__;
    static char ch[12*40];
    extern /* Subroutine */ int chval_(char *, real *, char *, ftnlen, ftnlen)
	    ;
    static char cfmtz[16];
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen);
    extern logical luychk_(char *, ftnlen);
    extern /* Subroutine */ int uzcget_(char *, char *, ftnlen, ftnlen), 
	    uyplbl_(char *, integer *, real *, char *, integer *, integer *, 
	    ftnlen, ftnlen);

/*     / CHECK ARGUMENTS / */
    /* Parameter adjustments */
    --uy;

    /* Function Body */
    if (! luychk_(cside, (ftnlen)1)) {
	msgdmp_("E", "UYPNUM", "SIDE PARAMETER IS INVALID.", (ftnlen)1, (
		ftnlen)6, (ftnlen)26);
    }
    if (! (0 <= *islct && *islct <= 2)) {
	msgdmp_("E", "UYPNUM", "'ISLCT' IS INVALID.", (ftnlen)1, (ftnlen)6, (
		ftnlen)19);
    }
    if (*n <= 0) {
	msgdmp_("E", "UYPNUM", "NUMBER OF POINTS IS INVALID.", (ftnlen)1, (
		ftnlen)6, (ftnlen)28);
    } else if (*n > 40) {
	msgdmp_("E", "UYPNUM", "WORKING AREA IS NOT ENOUGH.", (ftnlen)1, (
		ftnlen)6, (ftnlen)27);
    }
/*     / GENERATE CHARACTERS / */
    uzcget_("CYFMT", cfmtz, (ftnlen)5, (ftnlen)16);
    i__1 = *n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	chval_(cfmtz, &uy[i__], ch + (i__ - 1) * 12, (ftnlen)16, (ftnlen)12);
/* L10: */
    }
/*     / UYPLBL CALL / */
    uyplbl_(cside, islct, &uy[1], ch, &c__12, n, (ftnlen)1, (ftnlen)12);
    return 0;
} /* uypnum_ */


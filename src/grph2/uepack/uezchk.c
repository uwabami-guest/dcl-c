/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Common Block Declarations */

struct {
    real tl1[100], tl2[100];
    integer ipt[100], nt;
    logical lascnd;
} ueblk1_;

#define ueblk1_1 ueblk1_

/* Table of constant values */

static integer c__2 = 2;
static integer c__1 = 1;
static logical c_false = FALSE_;

/* ----------------------------------------------------------------------- */
/*     UEZCHK */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uezchk_(real *z__, integer *mx, integer *nx, integer *ny,
	 char *cname, integer *istat, ftnlen cname_len)
{
    /* System generated locals */
    integer z_dim1, z_offset;
    real r__1;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_wsfi(icilist *), do_fio(integer *, char *, ftnlen), e_wsfi(void)
	    ;

    /* Local variables */
    static integer npx[2], nqx[2], nsx[2];
    static char cmsg[80];
    static integer ipat, nlev;
    static logical lset;
    static real rlev;
    static logical lmada, ltone, lmiss;
    extern real rvmin_(real *, integer *, integer *, integer *, integer *);
    static real rmiss;
    extern real rvmax_(real *, integer *, integer *, integer *, integer *);
    static real rminz, rmaxz;
    extern /* Subroutine */ int gllget_(char *, logical *, ftnlen), ueiget_(
	    char *, integer *, ftnlen), uegtlb_(real *, integer *, integer *, 
	    integer *, real *), glrget_(char *, real *, ftnlen), uelget_(char 
	    *, logical *, ftnlen), msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), uerget_(char *, real *, ftnlen), uestlv_(real *, 
	    real *, integer *), ueqtlz_(logical *), uestlz_(logical *);

    /* Fortran I/O blocks */
    static icilist io___10 = { 0, cmsg+10, 0, "(1P,E10.3)", 10, 1 };


/*     / GET INTERNAL PARAMETERS / */
    /* Parameter adjustments */
    z_dim1 = *mx;
    z_offset = 1 + z_dim1;
    z__ -= z_offset;

    /* Function Body */
    gllget_("LMISS   ", &lmiss, (ftnlen)8);
    glrget_("RMISS   ", &rmiss, (ftnlen)8);
/*     / CHECK MIN & MAX / */
    nsx[0] = *mx;
    nsx[1] = *ny;
    npx[0] = 1;
    npx[1] = 1;
    nqx[0] = *nx;
    nqx[1] = *ny;
    rminz = rvmin_(&z__[z_offset], nsx, npx, nqx, &c__2);
    rmaxz = rvmax_(&z__[z_offset], nsx, npx, nqx, &c__2);
    lmada = lmiss && rminz == rmiss && rmaxz == rmiss;
    *istat = 0;
    if (lmada || rminz == rmaxz) {
/*       / MESSAGE FOR MISSING OR CONSTANT FIELD / */
	if (lmada) {
	    s_copy(cmsg, "MISSING FIELD.", (ftnlen)80, (ftnlen)14);
	    *istat = 1;
	} else {
	    s_copy(cmsg, "CONSTANT (##########) FIELD.", (ftnlen)80, (ftnlen)
		    28);
	    s_wsfi(&io___10);
	    do_fio(&c__1, (char *)&rminz, (ftnlen)sizeof(real));
	    e_wsfi();
	    *istat = 2;
	}
	msgdmp_("W", cname, cmsg, (ftnlen)1, cname_len, (ftnlen)80);
    }
/*     / GENERATE TONE LEVELS IF THEY HAVE NOT BEEN GENERATED YET / */
    ueqtlz_(&lset);
    if (! lset) {
	uelget_("LTONE", &ltone, (ftnlen)5);
	if (ltone) {
	    ueiget_("NLEV", &nlev, (ftnlen)4);
	    r__1 = -((real) nlev);
	    uegtlb_(&z__[z_offset], mx, nx, ny, &r__1);
	} else {
	    ueiget_("IPAT", &ipat, (ftnlen)4);
	    uerget_("RLEV", &rlev, (ftnlen)4);
	    uestlv_(&rmiss, &rlev, &ipat);
	}
	uestlz_(&c_false);
    }
    return 0;
} /* uezchk_ */


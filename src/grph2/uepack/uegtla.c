/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static logical c_true = TRUE_;

/* ----------------------------------------------------------------------- */
/*     UEGTLA */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uegtla_(real *xmin, real *xmax, real *dx)
{
    /* System generated locals */
    integer i__1, i__2;
    real r__1;

    /* Builtin functions */
    integer i_nint(real *);

    /* Local variables */
    static integer i__, n, nl;
    static real dz;
    extern integer irge_(real *), irle_(real *);
    static integer ipat, nlev;
    static real zmin, zmax;
    static integer iclr1, iclr2;
    static real tlev1, tlev2;
    extern real rgnge_(real *);
    static logical lepsl;
    static integer itpat;
    extern /* Subroutine */ int gllget_(char *, logical *, ftnlen), ueiget_(
	    char *, integer *, ftnlen);
    static logical lbound;
    extern /* Subroutine */ int uelget_(char *, logical *, ftnlen), msgdmp_(
	    char *, char *, char *, ftnlen, ftnlen, ftnlen), gllset_(char *, 
	    logical *, ftnlen), ueitlv_(void), uestlv_(real *, real *, 
	    integer *);

    if (! (*xmin < *xmax)) {
	msgdmp_("E", "UEGTLA", "XMIN SHOULD BE LEAST THAN XMAX.", (ftnlen)1, (
		ftnlen)6, (ftnlen)31);
    }
    ueiget_("NLEV    ", &nlev, (ftnlen)8);
    ueiget_("ITPAT   ", &itpat, (ftnlen)8);
    ueiget_("ICOLOR1 ", &iclr1, (ftnlen)8);
    ueiget_("ICOLOR2 ", &iclr2, (ftnlen)8);
    uelget_("LBOUND  ", &lbound, (ftnlen)8);
    gllget_("LEPSL   ", &lepsl, (ftnlen)8);
    gllset_("LEPSL   ", &c_true, (ftnlen)8);
    if (*dx > 0.f) {
	dz = *dx;
    } else if (*dx == 0.f) {
	r__1 = (*xmax - *xmin) / nlev;
	dz = rgnge_(&r__1);
    } else {
/* Computing MAX */
	r__1 = abs(*dx);
	i__1 = 1, i__2 = i_nint(&r__1);
	nl = max(i__1,i__2);
	r__1 = (*xmax - *xmin) / nl;
	dz = rgnge_(&r__1);
    }
    r__1 = *xmin / dz;
    zmin = irle_(&r__1) * dz;
    r__1 = *xmax / dz;
    zmax = irge_(&r__1) * dz;
    r__1 = (zmax - zmin) / dz;
    n = i_nint(&r__1);
    ueitlv_();
    i__1 = n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	r__1 = (zmin + (i__ - 1) * dz) / dz;
	tlev1 = dz * i_nint(&r__1);
	tlev2 = tlev1 + dz;
	if (lbound && i__ == 1) {
	    tlev1 = max(tlev1,*xmin);
	}
	if (lbound && i__ == n) {
	    tlev2 = min(tlev2,*xmax);
	}
	if (n == 1) {
	    ipat = (iclr2 - iclr1) / 2 * 1000 + itpat;
	} else {
	    r__1 = (iclr2 - iclr1) / (real) (n - 1) * (i__ - 1) + iclr1;
	    ipat = i_nint(&r__1) * 1000 + itpat;
	}
	uestlv_(&tlev1, &tlev2, &ipat);
/* L10: */
    }
    gllset_("LEPSL   ", &lepsl, (ftnlen)8);
    return 0;
} /* uegtla_ */


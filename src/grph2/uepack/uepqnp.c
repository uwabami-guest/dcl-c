/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__3 = 3;

/* ----------------------------------------------------------------------- */
/*     UEPQNP / UEPQID / UEPQCP / UEPQVL / UEPSVL */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uepqnp_0_(int n__, integer *ncp, char *cp, integer *idx, 
	integer *itp, integer *ipara, integer *in, ftnlen cp_len)
{
    /* Initialized data */

    static char cparas[8*8] = "LTONE   " "IPAT    " "RLEV    " "LBOUND  " 
	    "NLEV    " "ITPAT   " "ICOLOR1 " "ICOLOR2 ";
    static integer itype[8] = { 2,1,3,2,1,1,1,1 };
    static char cparal[40*8] = "ENABLE_AUTO_SHADE_LEVEL                 " 
	    "DEFAULT_SHADE_PATTERN                   " "DEFAULT_SHADE_THRESH"
	    "OLD                 " "????LBOUND                              " 
	    "****NLEV                                " "AUTO_SHADE_PATTERN  "
	    "                    " "SHADE_COLOR_MIN                         " 
	    "SHADE_COLOR_MAX                         ";

    /* System generated locals */
    address a__1[3];
    integer i__1[3];

    /* Builtin functions */
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen),
	     s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer n, id;
    extern integer lenc_(char *, ftnlen);
    static char cmsg[80];
    extern logical lchreq_(char *, char *, ftnlen, ftnlen);
    extern /* Subroutine */ int ueiqid_(char *, integer *, ftnlen), uelqid_(
	    char *, integer *, ftnlen), msgdmp_(char *, char *, char *, 
	    ftnlen, ftnlen, ftnlen), uerqid_(char *, integer *, ftnlen), 
	    ueiqvl_(integer *, integer *), uelqvl_(integer *, integer *), 
	    ueisvl_(integer *, integer *), uelsvl_(integer *, integer *), 
	    uerqvl_(integer *, integer *), uersvl_(integer *, integer *);

/*     / SHORT NAME / */
    switch(n__) {
	case 1: goto L_uepqid;
	case 2: goto L_uepqcp;
	case 3: goto L_uepqcl;
	case 4: goto L_uepqit;
	case 5: goto L_uepqvl;
	case 6: goto L_uepsvl;
	case 7: goto L_uepqin;
	}

/*     / LONG NAME / */
    *ncp = 8;
    return 0;
/* ----------------------------------------------------------------------- */

L_uepqid:
    for (n = 1; n <= 8; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *idx = n;
	    return 0;
	}
/* L10: */
    }
/* Writing concatenation */
    i__1[0] = 11, a__1[0] = "PARAMETER '";
    i__1[1] = lenc_(cp, cp_len), a__1[1] = cp;
    i__1[2] = 17, a__1[2] = "' IS NOT DEFINED.";
    s_cat(cmsg, a__1, i__1, &c__3, (ftnlen)80);
    msgdmp_("E", "UEPQID", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
    return 0;
/* ----------------------------------------------------------------------- */

L_uepqcp:
    if (1 <= *idx && *idx <= 8) {
	s_copy(cp, cparas + (*idx - 1 << 3), cp_len, (ftnlen)8);
    } else {
	msgdmp_("E", "UEPQCP", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_uepqcl:
    if (1 <= *idx && *idx <= 8) {
	s_copy(cp, cparal + (*idx - 1) * 40, cp_len, (ftnlen)40);
    } else {
	msgdmp_("E", "UEPQCL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_uepqit:
    if (1 <= *idx && *idx <= 8) {
	*itp = itype[*idx - 1];
    } else {
	msgdmp_("E", "UEPQIT", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_uepqvl:
    if (1 <= *idx && *idx <= 8) {
	if (itype[*idx - 1] == 1) {
	    ueiqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    ueiqvl_(&id, ipara);
	} else if (itype[*idx - 1] == 2) {
	    uelqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    uelqvl_(&id, ipara);
	} else if (itype[*idx - 1] == 3) {
	    uerqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    uerqvl_(&id, ipara);
	}
    } else {
	msgdmp_("E", "UEPQVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_uepsvl:
    if (1 <= *idx && *idx <= 8) {
	if (itype[*idx - 1] == 1) {
	    ueiqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    ueisvl_(&id, ipara);
	} else if (itype[*idx - 1] == 2) {
	    uelqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    uelsvl_(&id, ipara);
	} else if (itype[*idx - 1] == 3) {
	    uerqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    uersvl_(&id, ipara);
	}
    } else {
	msgdmp_("E", "UEPSVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_uepqin:
    for (n = 1; n <= 8; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *in = n;
	    return 0;
	}
/* L20: */
    }
    *in = 0;
    return 0;
} /* uepqnp_ */

/* Subroutine */ int uepqnp_(integer *ncp)
{
    return uepqnp_0_(0, ncp, (char *)0, (integer *)0, (integer *)0, (integer *
	    )0, (integer *)0, (ftnint)0);
    }

/* Subroutine */ int uepqid_(char *cp, integer *idx, ftnlen cp_len)
{
    return uepqnp_0_(1, (integer *)0, cp, idx, (integer *)0, (integer *)0, (
	    integer *)0, cp_len);
    }

/* Subroutine */ int uepqcp_(integer *idx, char *cp, ftnlen cp_len)
{
    return uepqnp_0_(2, (integer *)0, cp, idx, (integer *)0, (integer *)0, (
	    integer *)0, cp_len);
    }

/* Subroutine */ int uepqcl_(integer *idx, char *cp, ftnlen cp_len)
{
    return uepqnp_0_(3, (integer *)0, cp, idx, (integer *)0, (integer *)0, (
	    integer *)0, cp_len);
    }

/* Subroutine */ int uepqit_(integer *idx, integer *itp)
{
    return uepqnp_0_(4, (integer *)0, (char *)0, idx, itp, (integer *)0, (
	    integer *)0, (ftnint)0);
    }

/* Subroutine */ int uepqvl_(integer *idx, integer *ipara)
{
    return uepqnp_0_(5, (integer *)0, (char *)0, idx, (integer *)0, ipara, (
	    integer *)0, (ftnint)0);
    }

/* Subroutine */ int uepsvl_(integer *idx, integer *ipara)
{
    return uepqnp_0_(6, (integer *)0, (char *)0, idx, (integer *)0, ipara, (
	    integer *)0, (ftnint)0);
    }

/* Subroutine */ int uepqin_(char *cp, integer *in, ftnlen cp_len)
{
    return uepqnp_0_(7, (integer *)0, cp, (integer *)0, (integer *)0, (
	    integer *)0, in, cp_len);
    }


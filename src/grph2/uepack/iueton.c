/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Common Block Declarations */

struct {
    real tl1[100], tl2[100];
    integer ipt[100], nt;
    logical lascnd;
} ueblk1_;

#define ueblk1_1 ueblk1_

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
integer iueton_(real *zlev)
{
    /* Initialized data */

    static integer it = 1;

    /* System generated locals */
    integer ret_val;

    if (ueblk1_1.lascnd) {
L10:
	if (*zlev < ueblk1_1.tl1[it - 1] && it > 1) {
	    --it;
	    goto L10;
	} else if (*zlev > ueblk1_1.tl2[it - 1] && it < ueblk1_1.nt) {
	    ++it;
	    goto L10;
	}
    } else {
	if (*zlev < ueblk1_1.tl1[0]) {
	    it = 1;
	} else if (*zlev > ueblk1_1.tl2[ueblk1_1.nt - 1]) {
	    it = ueblk1_1.nt;
	} else {
	    for (it = ueblk1_1.nt; it >= 1; --it) {
		if (ueblk1_1.tl1[it - 1] <= *zlev && *zlev <= ueblk1_1.tl2[it 
			- 1]) {
		    goto L30;
		}
/* L20: */
	    }
L30:
	    ;
	}
    }
    ret_val = ueblk1_1.ipt[it - 1];
    return ret_val;
} /* iueton_ */


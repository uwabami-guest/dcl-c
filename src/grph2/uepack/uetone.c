/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Common Block Declarations */

struct {
    real tl1[100], tl2[100];
    integer ipt[100], nt;
    logical lascnd;
} ueblk1_;

#define ueblk1_1 ueblk1_

/* Table of constant values */

static integer c__4 = 4;
static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uetone_(real *z__, integer *mx, integer *nx, integer *ny)
{
    /* System generated locals */
    integer z_dim1, z_offset, i__1, i__2, i__3, i__4;
    real r__1, r__2;

    /* Local variables */
    static integer i__, j, n, i1, j1, ng, ni[2];
    static real ax[10], ay[10], xc[4], yc[4], zc[4];
    static integer np[2];
    static real dx, dy;
    static integer it;
    static real tlm[2];
    static integer itr, nxz, nyz;
    static real zmin, zmax, zmin0, zmin1, zmax0, zmax1;
    static integer istat;
    static logical lmiss;
    static real rmiss, uxmin, uymin, uxmax, uymax, vxmin, vymin, vxmax, vymax;
    extern real ruwgx_(integer *), ruwgy_(integer *);
    static integer ibgcli;
    extern /* Subroutine */ int uearea_(real *, real *, real *, real *, real *
	    , real *, integer *, integer *, integer *), gllget_(char *, 
	    logical *, ftnlen);
    static integer irmode;
    extern /* Subroutine */ int sgiget_(char *, integer *, ftnlen), glrget_(
	    char *, real *, ftnlen), ueaint_(void), uezchk_(real *, integer *,
	     integer *, integer *, char *, integer *, ftnlen), sgiset_(char *,
	     integer *, ftnlen);
    extern integer nindxr_(real *, integer *, integer *, real *);
    extern /* Subroutine */ int uwdflt_(integer *, integer *), stftrf_(real *,
	     real *, real *, real *), uwqgxb_(real *, real *, integer *), 
	    uwqgyb_(real *, real *, integer *), sztncl_(void), sztnop_(
	    integer *), sztnzu_(integer *, real *, real *);

    /* Parameter adjustments */
    z_dim1 = *mx;
    z_offset = 1 + z_dim1;
    z__ -= z_offset;

    /* Function Body */
    sgiget_("IBGCLI", &ibgcli, (ftnlen)6);
/*     / GET INTERNAL PARAMETERS / */
    gllget_("LMISS   ", &lmiss, (ftnlen)8);
    glrget_("RMISS   ", &rmiss, (ftnlen)8);
    sgiget_("ITR     ", &itr, (ftnlen)8);
/*     / SET GRID ATTRIBUTE IF IT HAS NOT BEEN SET YET / */
    uwdflt_(nx, ny);
/*     / CHECK Z VALUES AND TONE LEVEL / */
    uezchk_(&z__[z_offset], mx, nx, ny, "UETONE", &istat, (ftnlen)6);
    if (istat != 0) {
	return 0;
    }
/*     / SET TONE MODE (IRMODE) / */
    uwqgxb_(&uxmin, &uxmax, &nxz);
    uwqgyb_(&uymin, &uymax, &nyz);
    if (itr < 4) {
	stftrf_(&uxmin, &uymin, &vxmin, &vymin);
	stftrf_(&uxmax, &uymax, &vxmax, &vymax);
	dx = vxmax - vxmin;
	dy = vymax - vymin;
    } else {
	dx = uxmax - uxmin;
	dy = uymax - uymin;
    }
    irmode = 0;
    if (dx < 0.f) {
	irmode = (irmode + 1) % 2;
    }
    if (dy < 0.f) {
	irmode = (irmode + 1) % 2;
    }
    sgiset_("IRMODE", &irmode, (ftnlen)6);
/*     / INITIALIZE / */
    ueaint_();
/*     / LOOP FOR EACH TONE PATTERN / */
    i__1 = ueblk1_1.nt;
    for (it = 1; it <= i__1; ++it) {
	sztnop_(&ueblk1_1.ipt[it - 1]);
/*       / LOOP FOR EACH SECTION / */
	i__2 = *nx - 1;
	for (i__ = 1; i__ <= i__2; ++i__) {
	    i1 = i__ + 1;
/* Computing MAX */
	    r__1 = z__[i__ + z_dim1], r__2 = z__[i1 + z_dim1];
	    zmax1 = max(r__1,r__2);
/* Computing MIN */
	    r__1 = z__[i__ + z_dim1], r__2 = z__[i1 + z_dim1];
	    zmin1 = min(r__1,r__2);
	    xc[0] = ruwgx_(&i__);
	    xc[1] = ruwgx_(&i1);
	    xc[2] = xc[1];
	    xc[3] = xc[0];
	    i__3 = *ny - 1;
	    for (j = 1; j <= i__3; ++j) {
		j1 = j + 1;
		zmax0 = zmax1;
		zmin0 = zmin1;
/* Computing MAX */
		r__1 = z__[i__ + j1 * z_dim1], r__2 = z__[i1 + j1 * z_dim1];
		zmax1 = max(r__1,r__2);
/* Computing MIN */
		r__1 = z__[i__ + j1 * z_dim1], r__2 = z__[i1 + j1 * z_dim1];
		zmin1 = min(r__1,r__2);
		zmax = max(zmax0,zmax1);
		zmin = min(zmin0,zmin1);
		if (zmax < ueblk1_1.tl1[it - 1]) {
		    goto L10;
		}
		if (zmin > ueblk1_1.tl2[it - 1]) {
		    goto L10;
		}
		zc[0] = z__[i__ + j * z_dim1];
		zc[1] = z__[i1 + j * z_dim1];
		zc[2] = z__[i1 + j1 * z_dim1];
		zc[3] = z__[i__ + j1 * z_dim1];
		if (lmiss && nindxr_(zc, &c__4, &c__1, &rmiss) != 0) {
		    goto L10;
		}
		yc[0] = ruwgy_(&j);
		yc[1] = yc[0];
		yc[2] = ruwgy_(&j1);
		yc[3] = yc[2];
		tlm[0] = ueblk1_1.tl1[it - 1];
		tlm[1] = ueblk1_1.tl2[it - 1];
		uearea_(xc, yc, zc, tlm, ax, ay, np, ni, &ng);
		i__4 = ng;
		for (n = 1; n <= i__4; ++n) {
		    sztnzu_(&np[n - 1], &ax[ni[n - 1] - 1], &ay[ni[n - 1] - 1]
			    );
/* L40: */
		}
L10:
		;
	    }
/* L20: */
	}
	sztncl_();
/* L30: */
    }
    return 0;
} /* uetone_ */


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/* Subroutine */ int uetonz_(real *z__, integer *mx, integer *nx, integer *ny,
	 integer *image, integer *ni)
{
    /* System generated locals */
    integer z_dim1, z_offset, i__1, i__2, i__3, i__4;

    /* Local variables */
    static integer i__, j;
    static real fx, fy;
    static integer ix, iy;
    static real rx, ry, ux, vx, wx, wy, vy, uy, zz;
    static integer ix1, iy1, ix2, iy2, ix3, iy3, ix4, iy4;
    static real wx1, wy1, wx2, wy2, wx3, wy3, wx4, wy4;
    static logical limc;
    static integer istat, ixmin, iymin;
    static logical lmiss;
    static integer ixmax, iymax;
    static real rmiss, vxmin, vymin, vxmax, vymax;
    extern /* Subroutine */ int stfpr2_(real *, real *, real *, real *), 
	    stipr2_(real *, real *, real *, real *);
    static integer iundef;
    extern /* Subroutine */ int gliget_(char *, integer *, ftnlen);
    static integer ihight;
    extern /* Subroutine */ int gllget_(char *, logical *, ftnlen);
    static real rundef;
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen), msgdmp_(char 
	    *, char *, char *, ftnlen, ftnlen, ftnlen);
    static integer iwidth;
    extern /* Subroutine */ int uezchk_(real *, integer *, integer *, integer 
	    *, char *, integer *, ftnlen), swidat_(integer *, integer *);
    extern integer iueton_(real *);
    extern /* Subroutine */ int swqimc_(logical *), uwdflt_(integer *, 
	    integer *), swicls_(void), swfint_(real *, real *, integer *, 
	    integer *), stitrf_(real *, real *, real *, real *), swiint_(
	    integer *, integer *, real *, real *), swiopn_(integer *, integer 
	    *, integer *, integer *, real *, real *, real *, real *, real *, 
	    real *, real *, real *), sgqvpt_(real *, real *, real *, real *), 
	    uwqgxi_(real *, integer *, real *), uwqgyi_(real *, integer *, 
	    real *), stfwtr_(real *, real *, real *, real *), stiwtr_(real *, 
	    real *, real *, real *);

/*     / CHECK IMAGE CAPABILITY / */
    /* Parameter adjustments */
    z_dim1 = *mx;
    z_offset = 1 + z_dim1;
    z__ -= z_offset;
    --image;

    /* Function Body */
    swqimc_(&limc);
    if (! limc) {
	msgdmp_("E", "UETONZ", "NO IMAGE CAPABILITY.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
/*     / GET INTERNAL PARAMETERS / */
    glrget_("RUNDEF  ", &rundef, (ftnlen)8);
    gliget_("IUNDEF  ", &iundef, (ftnlen)8);
    gllget_("LMISS   ", &lmiss, (ftnlen)8);
    glrget_("RMISS   ", &rmiss, (ftnlen)8);
/*     / SET GRID ATTRIBUTE IF IT HAS NOT BEEN SET YET / */
    uwdflt_(nx, ny);
/*     / CHECK Z VALUES AND TONE LEVEL / */
    uezchk_(&z__[z_offset], mx, nx, ny, "UETONF", &istat, (ftnlen)6);
    if (istat != 0) {
	return 0;
    }
/*     / INITIALIZE / */
    sgqvpt_(&vxmin, &vxmax, &vymin, &vymax);
    stfpr2_(&vxmin, &vymin, &rx, &ry);
    stfwtr_(&rx, &ry, &wx1, &wy1);
    swfint_(&wx1, &wy1, &ix1, &iy1);
    stfpr2_(&vxmax, &vymin, &rx, &ry);
    stfwtr_(&rx, &ry, &wx2, &wy2);
    swfint_(&wx2, &wy2, &ix2, &iy2);
    stfpr2_(&vxmax, &vymax, &rx, &ry);
    stfwtr_(&rx, &ry, &wx3, &wy3);
    swfint_(&wx3, &wy3, &ix3, &iy3);
    stfpr2_(&vxmin, &vymax, &rx, &ry);
    stfwtr_(&rx, &ry, &wx4, &wy4);
    swfint_(&wx4, &wy4, &ix4, &iy4);
/* Computing MIN */
    i__1 = min(ix1,ix2), i__1 = min(i__1,ix3);
    ixmin = min(i__1,ix4);
/* Computing MIN */
    i__1 = min(iy1,iy2), i__1 = min(i__1,iy3);
    iymin = min(i__1,iy4);
/* Computing MAX */
    i__1 = max(ix1,ix2), i__1 = max(i__1,ix3);
    ixmax = max(i__1,ix4);
/* Computing MAX */
    i__1 = max(iy1,iy2), i__1 = max(i__1,iy3);
    iymax = max(i__1,iy4);
    iwidth = ixmax - ixmin + 1;
    ihight = iymax - iymin + 1;
    if (iwidth > *ni) {
	msgdmp_("E", "UETONZ", "WORKING AREA IS NOT ENOUGH.", (ftnlen)1, (
		ftnlen)6, (ftnlen)27);
    }
    swiopn_(&ixmin, &iymin, &iwidth, &ihight, &wx1, &wy1, &wx2, &wy2, &wx3, &
	    wy3, &wx4, &wy4);
/*     / LOOP FOR EACH PIXEL / */
    i__1 = ihight;
    for (j = 1; j <= i__1; ++j) {
	i__2 = iwidth;
	for (i__ = 1; i__ <= i__2; ++i__) {
	    i__3 = i__ + ixmin - 1;
	    i__4 = j + iymin - 1;
	    swiint_(&i__3, &i__4, &wx, &wy);
	    stiwtr_(&wx, &wy, &rx, &ry);
	    stipr2_(&rx, &ry, &vx, &vy);
	    stitrf_(&vx, &vy, &ux, &uy);
	    if (ux == rundef) {
		image[i__] = 0;
	    } else {
		uwqgxi_(&ux, &ix, &fx);
		uwqgyi_(&uy, &iy, &fy);
		if (ix == iundef || iy == iundef) {
		    image[i__] = 0;
		} else {
		    if (lmiss && (z__[ix + iy * z_dim1] == rmiss || z__[ix + 
			    1 + iy * z_dim1] == rmiss || z__[ix + (iy + 1) * 
			    z_dim1] == rmiss || z__[ix + 1 + (iy + 1) * 
			    z_dim1] == rmiss)) {
			image[i__] = 0;
		    } else {
			zz = (z__[ix + iy * z_dim1] * (1 - fx) + z__[ix + 1 + 
				iy * z_dim1] * fx) * (1 - fy) + (z__[ix + (iy 
				+ 1) * z_dim1] * (1 - fx) + z__[ix + 1 + (iy 
				+ 1) * z_dim1] * fx) * fy;
			image[i__] = iueton_(&zz) / 1000;
		    }
		}
	    }
/* L20: */
	}
	swidat_(&image[1], &iwidth);
/* L30: */
    }
    swicls_();
    return 0;
} /* uetonz_ */


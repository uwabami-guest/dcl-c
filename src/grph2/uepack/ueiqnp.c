/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__3 = 3;
static integer c__5 = 5;

/* ----------------------------------------------------------------------- */
/*     UEIQNP / UEIQID / UEIQCP / UEIQVL / UEISVL */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int ueiqnp_0_(int n__, integer *ncp, char *cp, integer *idx, 
	integer *ipara, integer *in, ftnlen cp_len)
{
    /* Initialized data */

    static char cparas[8*5] = "IPAT    " "NLEV    " "ITPAT   " "ICOLOR1 " 
	    "ICOLOR2 ";
    static integer ix[5] = { 1201,12,999,15,94 };
    static char cparal[40*5] = "DEFAULT_SHADE_PATTERN                   " 
	    "****NLEV                                " "AUTO_SHADE_PATTERN  "
	    "                    " "SHADE_COLOR_MIN                         " 
	    "SHADE_COLOR_MAX                         ";
    static logical lfirst = TRUE_;

    /* System generated locals */
    address a__1[3];
    integer i__1[3];

    /* Builtin functions */
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen),
	     s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer n;
    extern integer lenc_(char *, ftnlen);
    static char cmsg[80];
    extern logical lchreq_(char *, char *, ftnlen, ftnlen);
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), rliget_(char *, integer *, integer *, ftnlen), 
	    rtiget_(char *, char *, integer *, integer *, ftnlen, ftnlen);

/*     / SHORT NAME / */
    switch(n__) {
	case 1: goto L_ueiqid;
	case 2: goto L_ueiqcp;
	case 3: goto L_ueiqcl;
	case 4: goto L_ueiqvl;
	case 5: goto L_ueisvl;
	case 6: goto L_ueiqin;
	}

/*     / LONG NAME / */
    *ncp = 5;
    return 0;
/* ----------------------------------------------------------------------- */

L_ueiqid:
    for (n = 1; n <= 5; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *idx = n;
	    return 0;
	}
/* L10: */
    }
/* Writing concatenation */
    i__1[0] = 11, a__1[0] = "PARAMETER '";
    i__1[1] = lenc_(cp, cp_len), a__1[1] = cp;
    i__1[2] = 17, a__1[2] = "' IS NOT DEFINED.";
    s_cat(cmsg, a__1, i__1, &c__3, (ftnlen)80);
    msgdmp_("E", "UEIQID", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
    return 0;
/* ----------------------------------------------------------------------- */

L_ueiqcp:
    if (1 <= *idx && *idx <= 5) {
	s_copy(cp, cparas + (*idx - 1 << 3), cp_len, (ftnlen)8);
    } else {
	msgdmp_("E", "UEIQCP", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_ueiqcl:
    if (1 <= *idx && *idx <= 5) {
	s_copy(cp, cparal + (*idx - 1) * 40, cp_len, (ftnlen)40);
    } else {
	msgdmp_("E", "UEIQCL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_ueiqvl:
    if (lfirst) {
	rtiget_("UE", cparas, ix, &c__5, (ftnlen)2, (ftnlen)8);
	rliget_(cparal, ix, &c__5, (ftnlen)40);
	lfirst = FALSE_;
    }
    if (1 <= *idx && *idx <= 5) {
	*ipara = ix[*idx - 1];
    } else {
	msgdmp_("E", "UEIQVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_ueisvl:
    if (lfirst) {
	rtiget_("UE", cparas, ix, &c__5, (ftnlen)2, (ftnlen)8);
	rliget_(cparal, ix, &c__5, (ftnlen)40);
	lfirst = FALSE_;
    }
    if (1 <= *idx && *idx <= 5) {
	ix[*idx - 1] = *ipara;
    } else {
	msgdmp_("E", "UEISVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_ueiqin:
    for (n = 1; n <= 5; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *in = n;
	    return 0;
	}
/* L20: */
    }
    *in = 0;
    return 0;
} /* ueiqnp_ */

/* Subroutine */ int ueiqnp_(integer *ncp)
{
    return ueiqnp_0_(0, ncp, (char *)0, (integer *)0, (integer *)0, (integer *
	    )0, (ftnint)0);
    }

/* Subroutine */ int ueiqid_(char *cp, integer *idx, ftnlen cp_len)
{
    return ueiqnp_0_(1, (integer *)0, cp, idx, (integer *)0, (integer *)0, 
	    cp_len);
    }

/* Subroutine */ int ueiqcp_(integer *idx, char *cp, ftnlen cp_len)
{
    return ueiqnp_0_(2, (integer *)0, cp, idx, (integer *)0, (integer *)0, 
	    cp_len);
    }

/* Subroutine */ int ueiqcl_(integer *idx, char *cp, ftnlen cp_len)
{
    return ueiqnp_0_(3, (integer *)0, cp, idx, (integer *)0, (integer *)0, 
	    cp_len);
    }

/* Subroutine */ int ueiqvl_(integer *idx, integer *ipara)
{
    return ueiqnp_0_(4, (integer *)0, (char *)0, idx, ipara, (integer *)0, (
	    ftnint)0);
    }

/* Subroutine */ int ueisvl_(integer *idx, integer *ipara)
{
    return ueiqnp_0_(5, (integer *)0, (char *)0, idx, ipara, (integer *)0, (
	    ftnint)0);
    }

/* Subroutine */ int ueiqin_(char *cp, integer *in, ftnlen cp_len)
{
    return ueiqnp_0_(6, (integer *)0, cp, (integer *)0, (integer *)0, in, 
	    cp_len);
    }


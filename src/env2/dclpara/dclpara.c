/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static integer ii, io, np;
    static char cgrp[3];
    extern /* Subroutine */ int glcqcp_();
    extern /* Subroutine */ int gliget_(char *, integer *, ftnlen);
    extern logical lchreq_(char *, char *, ftnlen, ftnlen);
    extern /* Subroutine */ int dclcpz_(U_fp, U_fp, U_fp), osgarg_(integer *, 
	    char *, ftnlen);
    extern /* Subroutine */ int glcqnp_(), glpqcp_(), glcqvl_(), ucpqcp_(), 
	    udpqcp_(), sgpqcp_(), uepqcp_(), ugpqcp_(), swcqcp_(), glpqnp_();
    extern /* Subroutine */ int dclppz_(U_fp, U_fp, U_fp), osqarn_(integer *);
    extern /* Subroutine */ int ulpqcp_(), glpqvl_(), ucpqnp_(), udpqnp_(), 
	    sgpqnp_(), uepqnp_(), ucpqvl_(), swcqnp_(), sgpqvl_(), swpqcp_(), 
	    udpqvl_(), swcqvl_(), uepqvl_(), ugpqnp_(), ugpqvl_(), ulpqnp_(), 
	    ulpqvl_(), umpqnp_(), umpqcp_(), umpqvl_(), swpqnp_(), uspqnp_(), 
	    uspqcp_(), uspqvl_(), swpqvl_(), uzpqnp_(), uzpqcp_(), uzpqvl_();

    /* Fortran I/O blocks */
    static cilist io___4 = { 0, 0, 0, 0, 0 };
    static cilist io___5 = { 0, 0, 0, 0, 0 };


    osqarn_(&np);
    if (np == 0) {
	gliget_("IIUNIT", &ii, (ftnlen)6);
	gliget_("IOUNIT", &io, (ftnlen)6);
	io___4.ciunit = io;
	s_wsle(&io___4);
	do_lio(&c__9, &c__1, " GROUP NAME (C*3) (EX. GLP) ? ;", (ftnlen)31);
	e_wsle();
	io___5.ciunit = ii;
	s_rsle(&io___5);
	do_lio(&c__9, &c__1, cgrp, (ftnlen)3);
	e_rsle();
    } else {
	osgarg_(&c__1, cgrp, (ftnlen)3);
    }
    if (lchreq_(cgrp, "GLP", (ftnlen)3, (ftnlen)3)) {
	dclppz_((U_fp)glpqnp_, (U_fp)glpqcp_, (U_fp)glpqvl_);
    } else if (lchreq_(cgrp, "GLC", (ftnlen)3, (ftnlen)3)) {
	dclcpz_((U_fp)glcqnp_, (U_fp)glcqcp_, (U_fp)glcqvl_);
    } else if (lchreq_(cgrp, "SGP", (ftnlen)3, (ftnlen)3)) {
	dclppz_((U_fp)sgpqnp_, (U_fp)sgpqcp_, (U_fp)sgpqvl_);
    } else if (lchreq_(cgrp, "SWP", (ftnlen)3, (ftnlen)3)) {
	dclppz_((U_fp)swpqnp_, (U_fp)swpqcp_, (U_fp)swpqvl_);
    } else if (lchreq_(cgrp, "SWC", (ftnlen)3, (ftnlen)3)) {
	dclcpz_((U_fp)swcqnp_, (U_fp)swcqcp_, (U_fp)swcqvl_);
    } else if (lchreq_(cgrp, "UCP", (ftnlen)3, (ftnlen)3)) {
	dclppz_((U_fp)ucpqnp_, (U_fp)ucpqcp_, (U_fp)ucpqvl_);
    } else if (lchreq_(cgrp, "UDP", (ftnlen)3, (ftnlen)3)) {
	dclppz_((U_fp)udpqnp_, (U_fp)udpqcp_, (U_fp)udpqvl_);
    } else if (lchreq_(cgrp, "UEP", (ftnlen)3, (ftnlen)3)) {
	dclppz_((U_fp)uepqnp_, (U_fp)uepqcp_, (U_fp)uepqvl_);
    } else if (lchreq_(cgrp, "UGP", (ftnlen)3, (ftnlen)3)) {
	dclppz_((U_fp)ugpqnp_, (U_fp)ugpqcp_, (U_fp)ugpqvl_);
    } else if (lchreq_(cgrp, "ULP", (ftnlen)3, (ftnlen)3)) {
	dclppz_((U_fp)ulpqnp_, (U_fp)ulpqcp_, (U_fp)ulpqvl_);
    } else if (lchreq_(cgrp, "UMP", (ftnlen)3, (ftnlen)3)) {
	dclppz_((U_fp)umpqnp_, (U_fp)umpqcp_, (U_fp)umpqvl_);
    } else if (lchreq_(cgrp, "USP", (ftnlen)3, (ftnlen)3)) {
	dclppz_((U_fp)uspqnp_, (U_fp)uspqcp_, (U_fp)uspqvl_);
    } else if (lchreq_(cgrp, "UZP", (ftnlen)3, (ftnlen)3)) {
	dclppz_((U_fp)uzpqnp_, (U_fp)uzpqcp_, (U_fp)uzpqvl_);
    }
    return 0;
} /* MAIN__ */

/* ----------------------------------------------------------------------- */
/* Subroutine */ int dclppz_(S_fp xxpqnp, S_fp xxpqcp, S_fp xxpqvl)
{
    /* System generated locals */
    integer i__1;
    static real equiv_0[1];

    /* Builtin functions */
    integer s_wsfe(cilist *), do_fio(integer *, char *, ftnlen), e_wsfe(void);

    /* Local variables */
    static integer n;
    static char cp[8];
#define ip ((integer *)equiv_0)
#define lp ((logical *)equiv_0)
    static integer iu;
#define rp (equiv_0)
    static char cp1[1];
    static integer ncp;
    extern /* Subroutine */ int gliget_(char *, integer *, ftnlen), cupper_(
	    char *, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___15 = { 0, 0, 0, "(A8,A3,I10)", 0 };
    static cilist io___16 = { 0, 0, 0, "(A8,A3,L10)", 0 };
    static cilist io___17 = { 0, 0, 0, "(A8,A3,1P,E16.8)", 0 };


    gliget_("IOUNIT", &iu, (ftnlen)6);
    (*xxpqnp)(&ncp);
    i__1 = ncp;
    for (n = 1; n <= i__1; ++n) {
	(*xxpqcp)(&n, cp, (ftnlen)8);
	(*xxpqvl)(&n, ip);
	*(unsigned char *)cp1 = *(unsigned char *)cp;
	cupper_(cp1, (ftnlen)1);
	if (*(unsigned char *)cp1 == 'I' || *(unsigned char *)cp1 == 'J' || *(
		unsigned char *)cp1 == 'K' || *(unsigned char *)cp1 == 'M' || 
		*(unsigned char *)cp1 == 'N') {
	    io___15.ciunit = iu;
	    s_wsfe(&io___15);
	    do_fio(&c__1, cp, (ftnlen)8);
	    do_fio(&c__1, " = ", (ftnlen)3);
	    do_fio(&c__1, (char *)&(*ip), (ftnlen)sizeof(integer));
	    e_wsfe();
	} else if (*(unsigned char *)cp1 == 'L') {
	    io___16.ciunit = iu;
	    s_wsfe(&io___16);
	    do_fio(&c__1, cp, (ftnlen)8);
	    do_fio(&c__1, " = ", (ftnlen)3);
	    do_fio(&c__1, (char *)&(*lp), (ftnlen)sizeof(logical));
	    e_wsfe();
	} else {
	    io___17.ciunit = iu;
	    s_wsfe(&io___17);
	    do_fio(&c__1, cp, (ftnlen)8);
	    do_fio(&c__1, " = ", (ftnlen)3);
	    do_fio(&c__1, (char *)&(*rp), (ftnlen)sizeof(real));
	    e_wsfe();
	}
/* L10: */
    }
    return 0;
} /* dclppz_ */

#undef rp
#undef lp
#undef ip


/* ----------------------------------------------------------------------- */
/* Subroutine */ int dclcpz_(S_fp xxcqnp, S_fp xxcqcp, S_fp xxcqvl)
{
    /* System generated locals */
    integer i__1;
    static real equiv_0[1];

    /* Builtin functions */
    integer s_wsfe(cilist *), do_fio(integer *, char *, ftnlen), e_wsfe(void);

    /* Local variables */
    static integer n;
    static char cp[8];
#define ip ((integer *)equiv_0)
#define lp ((logical *)equiv_0)
    static integer iu;
#define rp (equiv_0)
    static integer ncp;
    extern integer lenc_(char *, ftnlen);
    static char cpara[1024];
    extern /* Subroutine */ int gliget_(char *, integer *, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___26 = { 0, 0, 0, "(A8,A3,A)", 0 };


    gliget_("IOUNIT", &iu, (ftnlen)6);
    (*xxcqnp)(&ncp);
    i__1 = ncp;
    for (n = 1; n <= i__1; ++n) {
	(*xxcqcp)(&n, cp, (ftnlen)8);
	(*xxcqvl)(&n, cpara, (ftnlen)1024);
	io___26.ciunit = iu;
	s_wsfe(&io___26);
	do_fio(&c__1, cp, (ftnlen)8);
	do_fio(&c__1, " = ", (ftnlen)3);
	do_fio(&c__1, cpara, lenc_(cpara, (ftnlen)1024));
	e_wsfe();
/* L10: */
    }
    return 0;
} /* dclcpz_ */

#undef rp
#undef lp
#undef ip


/* Main program alias */ int dclpar_ () { MAIN__ (); return 0; }

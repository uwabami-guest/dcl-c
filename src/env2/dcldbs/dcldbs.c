/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;
static integer c__3 = 3;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Initialized data */

    static char cdxlst[8*2] = "DSPATH  " "DUPATH  ";
    static char cgllst[8*1] = "DCLRC   ";
    static char cswlst[8*6] = "BITMAP  " "CL2TN   " "CMAPLIST" "CLRMAP  " 
	    "FONT1   " "FONT2   ";

    /* System generated locals */
    address a__1[3];
    integer i__1[3];
    char ch__1[1035];

    /* Builtin functions */
    integer s_wsfe(cilist *), do_fio(integer *, char *, ftnlen), e_wsfe(void);
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen);

    /* Local variables */
    static integer n, iu;
    extern integer lenc_(char *, ftnlen);
    static char cfname[1024];
    extern /* Subroutine */ int glcget_(char *, char *, ftnlen, ftnlen), 
	    gliget_(char *, integer *, ftnlen), glqfnm_(char *, char *, 
	    ftnlen, ftnlen), swqfnm_(char *, char *, ftnlen, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___5 = { 0, 0, 0, "(A)", 0 };
    static cilist io___8 = { 0, 0, 0, "(A)", 0 };
    static cilist io___9 = { 0, 0, 0, "(A)", 0 };
    static cilist io___10 = { 0, 0, 0, "(A)", 0 };
    static cilist io___11 = { 0, 0, 0, "(A)", 0 };
    static cilist io___12 = { 0, 0, 0, "(A)", 0 };


    gliget_("IOUNIT", &iu, (ftnlen)6);
    io___5.ciunit = iu;
    s_wsfe(&io___5);
    do_fio(&c__1, "*** PATH NAME FOR DATABASE", (ftnlen)26);
    e_wsfe();
    for (n = 1; n <= 2; ++n) {
	glcget_(cdxlst + (n - 1 << 3), cfname, (ftnlen)8, (ftnlen)1024);
	io___8.ciunit = iu;
	s_wsfe(&io___8);
/* Writing concatenation */
	i__1[0] = 8, a__1[0] = cdxlst + (n - 1 << 3);
	i__1[1] = 3, a__1[1] = " : ";
	i__1[2] = lenc_(cfname, (ftnlen)1024), a__1[2] = cfname;
	s_cat(ch__1, a__1, i__1, &c__3, (ftnlen)1035);
	do_fio(&c__1, ch__1, lenc_(cfname, (ftnlen)1024) + 11);
	e_wsfe();
/* L10: */
    }
    io___9.ciunit = iu;
    s_wsfe(&io___9);
    do_fio(&c__1, "*** FILE NAME FOR SYSLIB", (ftnlen)24);
    e_wsfe();
    for (n = 1; n <= 1; ++n) {
	glqfnm_(cgllst + (n - 1 << 3), cfname, (ftnlen)8, (ftnlen)1024);
	io___10.ciunit = iu;
	s_wsfe(&io___10);
/* Writing concatenation */
	i__1[0] = 8, a__1[0] = cgllst + (n - 1 << 3);
	i__1[1] = 3, a__1[1] = " : ";
	i__1[2] = lenc_(cfname, (ftnlen)1024), a__1[2] = cfname;
	s_cat(ch__1, a__1, i__1, &c__3, (ftnlen)1035);
	do_fio(&c__1, ch__1, lenc_(cfname, (ftnlen)1024) + 11);
	e_wsfe();
/* L20: */
    }
    io___11.ciunit = iu;
    s_wsfe(&io___11);
    do_fio(&c__1, "*** FILE NAME FOR SWPACK/SZPACK", (ftnlen)31);
    e_wsfe();
    for (n = 1; n <= 6; ++n) {
	swqfnm_(cswlst + (n - 1 << 3), cfname, (ftnlen)8, (ftnlen)1024);
	io___12.ciunit = iu;
	s_wsfe(&io___12);
/* Writing concatenation */
	i__1[0] = 8, a__1[0] = cswlst + (n - 1 << 3);
	i__1[1] = 3, a__1[1] = " : ";
	i__1[2] = lenc_(cfname, (ftnlen)1024), a__1[2] = cfname;
	s_cat(ch__1, a__1, i__1, &c__3, (ftnlen)1035);
	do_fio(&c__1, ch__1, lenc_(cfname, (ftnlen)1024) + 11);
	e_wsfe();
/* L30: */
    }
    return 0;
} /* MAIN__ */

/* Main program alias */ int dcldbs_ () { MAIN__ (); return 0; }

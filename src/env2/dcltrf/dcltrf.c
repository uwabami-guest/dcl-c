/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rsfi(icilist *), do_fio(integer *, char *, ftnlen), e_rsfi(void)
	    , s_wsfe(cilist *), e_wsfe(void);

    /* Local variables */
    static integer n, n1, n2, np, iu;
    static logical ltr;
    static char ctr1[3], ctr2[20];
    extern integer lenc_(char *, ftnlen);
    static char cfmt[4], cmsg[80], cpara[4];
    extern /* Subroutine */ int chngi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen), gliget_(char *, integer *, ftnlen), 
	    osgarg_(integer *, char *, ftnlen), msgdmp_(char *, char *, char *
	    , ftnlen, ftnlen, ftnlen), osqarn_(integer *), sgtrqf_(integer *, 
	    logical *), sgtrnl_(integer *, char *, ftnlen), sgtrns_(integer *,
	     char *, ftnlen);

    /* Fortran I/O blocks */
    static icilist io___6 = { 0, cpara, 0, cfmt, 4, 1 };
    static cilist io___12 = { 0, 0, 0, "(A,I2,5A)", 0 };


    osqarn_(&np);
    if (np == 0) {
	n1 = 1;
	n2 = 99;
    } else {
	osgarg_(&c__1, cpara, (ftnlen)4);
	s_copy(cfmt, "(I#)", (ftnlen)4, (ftnlen)4);
	i__1 = lenc_(cpara, (ftnlen)4);
	chngi_(cfmt, "#", &i__1, "(I1)", (ftnlen)4, (ftnlen)1, (ftnlen)4);
	s_rsfi(&io___6);
	do_fio(&c__1, (char *)&n1, (ftnlen)sizeof(integer));
	e_rsfi();
	n2 = n1;
    }
    gliget_("IOUNIT", &iu, (ftnlen)6);
    i__1 = n2;
    for (n = n1; n <= i__1; ++n) {
	sgtrqf_(&n, &ltr);
	if (ltr) {
	    sgtrns_(&n, ctr1, (ftnlen)3);
	    sgtrnl_(&n, ctr2, (ftnlen)20);
	    io___12.ciunit = iu;
	    s_wsfe(&io___12);
	    do_fio(&c__1, " NTR = ", (ftnlen)7);
	    do_fio(&c__1, (char *)&n, (ftnlen)sizeof(integer));
	    do_fio(&c__1, " ; ", (ftnlen)3);
	    do_fio(&c__1, ctr1, (ftnlen)3);
	    do_fio(&c__1, " ; ", (ftnlen)3);
	    do_fio(&c__1, ctr2, (ftnlen)20);
	    do_fio(&c__1, " ;", (ftnlen)2);
	    e_wsfe();
	} else {
	    if (n1 == n2) {
		s_copy(cmsg, "TRANSFORMATION NUMBER ## IS NOT DEFINED.", (
			ftnlen)80, (ftnlen)40);
		chngi_(cmsg, "##", &n, "(I2)", (ftnlen)80, (ftnlen)2, (ftnlen)
			4);
		msgdmp_("M", "DCLTRF", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80)
			;
	    }
	}
/* L10: */
    }
    return 0;
} /* MAIN__ */

/* Main program alias */ int dcltrf_ () { MAIN__ (); return 0; }

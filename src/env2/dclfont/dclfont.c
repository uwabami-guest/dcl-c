/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__600 = 600;
static integer c__0 = 0;
static logical c_false = FALSE_;
static integer c__1 = 1;
static real c_b17 = .05f;
static real c_b18 = .95f;
static real c_b21 = 0.f;
static real c_b22 = 16.f;
static integer c__2 = 2;
static real c_b34 = .035f;
static integer c__3 = 3;
static real c_b40 = .01f;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* System generated locals */
    real r__1, r__2;
    char ch__1[3];

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_wsfi(icilist *), do_fio(integer *, char *, ftnlen), e_wsfi(void)
	    ;

    /* Local variables */
    static integer i__;
    static char ch[3];
    static real ux, uy;
    static char cttl[16];
    extern /* Character */ VOID usgi_(char *, ftnlen, integer *);
    extern /* Subroutine */ int chngi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen), sgcls_(void), sgfrm_(void);
    static integer ifont;
    extern /* Subroutine */ int gropn_(integer *), sgiget_(char *, integer *, 
	    ftnlen), clower_(char *, ftnlen), sglset_(char *, logical *, 
	    ftnlen), sgswnd_(real *, real *, real *, real *), sgstrf_(void), 
	    glistx_(char *, integer *, ftnlen), sgstrn_(integer *), sglnzu_(
	    real *, real *, real *, real *, integer *), sglstx_(char *, 
	    logical *, ftnlen), sgsvpt_(real *, real *, real *, real *), 
	    swcstx_(char *, char *, ftnlen, ftnlen), swistx_(char *, integer *
	    , ftnlen), sgtxzu_(real *, real *, char *, real *, integer *, 
	    integer *, integer *, ftnlen);

    /* Fortran I/O blocks */
    static icilist io___7 = { 0, ch, 0, "(I3)", 3, 1 };


    swistx_("IWIDTH ", &c__600, (ftnlen)7);
    swistx_("IHEIGHT", &c__600, (ftnlen)7);
    s_copy(cttl, "DCLFONT (#)", (ftnlen)16, (ftnlen)11);
    sgiget_("IFONT", &ifont, (ftnlen)5);
    chngi_(cttl, "#", &ifont, "(I1)", (ftnlen)16, (ftnlen)1, (ftnlen)4);
    clower_(cttl, (ftnlen)16);
    swcstx_("TITLE", cttl, (ftnlen)5, (ftnlen)16);
    glistx_("MAXMSG", &c__0, (ftnlen)6);
    sglstx_("LCORNER", &c_false, (ftnlen)7);
    gropn_(&c__1);
    sglset_("LCNTL", &c_false, (ftnlen)5);
    sgfrm_();
    sgsvpt_(&c_b17, &c_b18, &c_b17, &c_b18);
    sgswnd_(&c_b21, &c_b22, &c_b21, &c_b22);
    sgstrn_(&c__1);
    sgstrf_();
    for (i__ = 0; i__ <= 16; ++i__) {
	r__1 = (real) i__;
	r__2 = (real) i__;
	sglnzu_(&r__1, &c_b21, &r__2, &c_b22, &c__2);
	r__1 = (real) i__;
	r__2 = (real) i__;
	sglnzu_(&c_b21, &r__1, &c_b22, &r__2, &c__2);
/* L10: */
    }
    for (i__ = 0; i__ <= 255; ++i__) {
	ux = i__ / 16 + .5f;
	uy = 16 - i__ % 16 - .5f;
	usgi_(ch__1, (ftnlen)3, &i__);
	sgtxzu_(&ux, &uy, ch__1, &c_b34, &c__0, &c__0, &c__3, (ftnlen)3);
	ux += .48f;
	uy += .38f;
	s_wsfi(&io___7);
	do_fio(&c__1, (char *)&i__, (ftnlen)sizeof(integer));
	e_wsfi();
	sgtxzu_(&ux, &uy, ch, &c_b40, &c__0, &c__1, &c__1, (ftnlen)3);
/* L15: */
    }
    sgcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int dclfnt_ () { MAIN__ (); return 0; }

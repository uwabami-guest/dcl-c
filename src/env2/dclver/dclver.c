/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Builtin functions */
    integer s_wsfe(cilist *), do_fio(integer *, char *, ftnlen), e_wsfe(void);

    /* Local variables */
    static integer iu;
    extern integer lenc_(char *, ftnlen);
    static char cvname[20];
    extern /* Subroutine */ int gliget_(char *, integer *, ftnlen), dclvnm_(
	    char *, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___3 = { 0, 0, 0, "(A)", 0 };


    gliget_("IOUNIT", &iu, (ftnlen)6);
    dclvnm_(cvname, (ftnlen)20);
    io___3.ciunit = iu;
    s_wsfe(&io___3);
    do_fio(&c__1, cvname, lenc_(cvname, (ftnlen)20));
    e_wsfe();
    return 0;
} /* MAIN__ */

/* Main program alias */ int dclver_ () { MAIN__ (); return 0; }

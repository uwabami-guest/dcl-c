/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Initialized data */

    static char ctimez[3] = "JST";

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_wsfe(cilist *), do_fio(integer *, char *, ftnlen), e_wsfe(void);

    /* Local variables */
    static integer iu, idate;
    static char cform[32];
    static integer itime;
    extern /* Subroutine */ int datec1_(char *, integer *, ftnlen), dateq1_(
	    integer *), timec1_(char *, integer *, ftnlen), timeq1_(integer *)
	    , gliget_(char *, integer *, ftnlen), clower_(char *, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___6 = { 0, 0, 0, "(A)", 0 };


    s_copy(cform, "WWW CCC DD ######## ### YYYY", (ftnlen)32, (ftnlen)28);
    dateq1_(&idate);
    timeq1_(&itime);
    datec1_(cform, &idate, (ftnlen)32);
    clower_(cform + 1, (ftnlen)2);
    clower_(cform + 5, (ftnlen)2);
    s_copy(cform + 11, "HH:MM:SS", (ftnlen)8, (ftnlen)8);
    timec1_(cform + 11, &itime, (ftnlen)8);
    s_copy(cform + 20, ctimez, (ftnlen)3, (ftnlen)3);
    gliget_("IOUNIT", &iu, (ftnlen)6);
    io___6.ciunit = iu;
    s_wsfe(&io___6);
    do_fio(&c__1, cform, (ftnlen)32);
    e_wsfe();
    return 0;
} /* MAIN__ */

/* Main program alias */ int dcldat_ () { MAIN__ (); return 0; }

/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__600 = 600;
static integer c__0 = 0;
static integer c__2 = 2;
static logical c_false = FALSE_;
static integer c__1 = 1;
static real c_b14 = 0.f;
static real c_b15 = 1.f;
static integer c__5 = 5;
static real c_b32 = .025f;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Initialized data */

    static real xbox[5] = { 0.f,1.f,1.f,0.f,0.f };
    static real ybox[5] = { 0.f,0.f,1.f,1.f,0.f };

    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_wsfi(icilist *), do_fio(integer *, char *, ftnlen), e_wsfi(void)
	    ;

    /* Local variables */
    static integer i__, j;
    static real x1, x2, y1, y2, xx, yy;
    static char chr[2], cttl[8];
    static integer level;
    extern /* Subroutine */ int sgcls_(void), sgfrm_(void), gropn_(integer *),
	     clower_(char *, ftnlen), sgswnd_(real *, real *, real *, real *),
	     sgstrf_(void), glistx_(char *, integer *, ftnlen), sgstrn_(
	    integer *), sgistx_(char *, integer *, ftnlen), sglstx_(char *, 
	    logical *, ftnlen), sgplzu_(integer *, real *, real *, integer *, 
	    integer *), sgsvpt_(real *, real *, real *, real *), sgtnzu_(
	    integer *, real *, real *, integer *), swcstx_(char *, char *, 
	    ftnlen, ftnlen), swistx_(char *, integer *, ftnlen), sgtxzv_(real 
	    *, real *, char *, real *, integer *, integer *, integer *, 
	    ftnlen);

    /* Fortran I/O blocks */
    static icilist io___14 = { 0, chr, 0, "(I1)", 2, 1 };
    static icilist io___15 = { 0, chr, 0, "(I2)", 2, 1 };


    swistx_("IWIDTH ", &c__600, (ftnlen)7);
    swistx_("IHEIGHT", &c__600, (ftnlen)7);
    s_copy(cttl, "DCLCLR", (ftnlen)8, (ftnlen)6);
    clower_(cttl, (ftnlen)8);
    swcstx_("TITLE", cttl, (ftnlen)5, (ftnlen)8);
    glistx_("MAXMSG", &c__0, (ftnlen)6);
    sgistx_("IFONT", &c__2, (ftnlen)5);
    sglstx_("LCORNER", &c_false, (ftnlen)7);
    gropn_(&c__1);
    sgfrm_();
    sgswnd_(&c_b14, &c_b15, &c_b14, &c_b15);
    sgstrn_(&c__1);
    for (j = 0; j <= 9; ++j) {
	for (i__ = 0; i__ <= 9; ++i__) {
	    x1 = i__ * .089999999999999997f + .07f;
	    x2 = x1 + .089999999999999997f;
	    y1 = (9 - j) * .089999999999999997f + .03f;
	    y2 = y1 + .089999999999999997f;
	    level = (i__ + j * 10) * 1000 + 999;
	    sgsvpt_(&x1, &x2, &y1, &y2);
	    sgstrf_();
	    sgtnzu_(&c__5, xbox, ybox, &level);
	    sgplzu_(&c__5, xbox, ybox, &c__1, &c__1);
/* L10: */
	}
/* L20: */
    }
    sgsvpt_(&c_b14, &c_b15, &c_b14, &c_b15);
    sgstrf_();
    for (i__ = 0; i__ <= 9; ++i__) {
	xx = (i__ + .5f) * .089999999999999997f + .07f;
	yy = .96500000000000008f;
	s_wsfi(&io___14);
	do_fio(&c__1, (char *)&i__, (ftnlen)sizeof(integer));
	e_wsfi();
	sgtxzv_(&xx, &yy, chr, &c_b32, &c__0, &c__0, &c__1, (ftnlen)2);
/* L30: */
    }
    for (j = 0; j <= 9; ++j) {
	xx = .035000000000000003f;
	yy = (9 - j + .5f) * .089999999999999997f + .03f;
	s_wsfi(&io___15);
	i__1 = j * 10;
	do_fio(&c__1, (char *)&i__1, (ftnlen)sizeof(integer));
	e_wsfi();
	sgtxzv_(&xx, &yy, chr, &c_b32, &c__0, &c__0, &c__1, (ftnlen)2);
/* L40: */
    }
    sgcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int dclclr_ () { MAIN__ (); return 0; }

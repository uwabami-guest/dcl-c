/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static logical c_true = TRUE_;
static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static integer c__2 = 2;
static real c_b17 = .05f;
static integer c_n1 = -1;
static real c_b26 = .015f;
static integer c__0 = 0;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* System generated locals */
    address a__1[2];
    integer i__1, i__2, i__3[2];
    real r__1, r__2;

    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen), s_cat(char *,
	     char **, integer *, integer *, ftnlen);
    integer s_wsfi(icilist *), do_fio(integer *, char *, ftnlen), e_wsfi(void)
	    ;

    /* Local variables */
    static char fullname[70], fontname[50];
    static integer i__, j, k;
    static real y[10], x1, x2;
    static char ch[3];
    static real xc;
    static integer pm;
    extern /* Subroutine */ int zgfontname_(integer *, char *, integer *, 
	    ftnlen);
    static char opt[20];
    static integer iws, nfnt;
    extern /* Subroutine */ int sgcls_(void), sgfrm_(void), sldiv_(char *, 
	    integer *, integer *, ftnlen), sgopn_(integer *), sglnv_(real *, 
	    real *, real *, real *), sgtxv_(real *, real *, char *, ftnlen), 
	    sgslni_(integer *), swcset_(char *, char *, ftnlen, ftnlen), 
	    sgstxc_(integer *), swftnm_(integer *), swlset_(char *, logical *,
	     ftnlen), sgpwsn_(void), sgstxs_(real *), sgtxzv_(real *, real *, 
	    char *, real *, integer *, integer *, integer *, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___2 = { 0, 6, 0, 0, 0 };
    static cilist io___3 = { 0, 5, 0, 0, 0 };
    static cilist io___9 = { 0, 6, 0, 0, 0 };
    static icilist io___18 = { 0, ch, 0, "(I3)", 3, 1 };
    static cilist io___19 = { 0, 6, 0, 0, 0 };


    swlset_("LSYSFNT", &c_true, (ftnlen)7);
    swftnm_(&nfnt);
    s_wsle(&io___2);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___3);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    i__ = 0;
    zgfontname_(&i__, fontname, &nfnt, (ftnlen)50);
    pm = nfnt / 10 + 1;
    s_copy(opt, " Regular ", (ftnlen)20, (ftnlen)9);
    s_wsle(&io___9);
    do_lio(&c__9, &c__1, opt, (ftnlen)20);
    e_wsle();
    sgopn_(&iws);
    sldiv_("T", &c__2, &c__2, (ftnlen)1);
    x1 = .01f;
    x2 = .99f;
    xc = .35f;
    i__1 = pm;
    for (j = 0; j <= i__1; ++j) {
	sgfrm_();
	sgslni_(&c__1);
	for (i__ = 1; i__ <= 10; ++i__) {
	    y[i__ - 1] = (11 - i__) * .09f;
	    r__1 = y[i__ - 1] + .05f;
	    r__2 = y[i__ - 1] + .05f;
	    sglnv_(&x1, &r__1, &x2, &r__2);
/* L10: */
	}
	sglnv_(&x1, &c_b17, &x2, &c_b17);
	sgstxc_(&c_n1);
	for (i__ = 1; i__ <= 10; ++i__) {
	    if (i__ + j * 10 > nfnt - 1) {
		goto L40;
	    }
	    i__2 = i__ + j * 10;
	    zgfontname_(&i__2, fontname, &nfnt, (ftnlen)50);
	    for (k = 1; k <= 50; ++k) {
		if (*(unsigned char *)&fontname[k - 1] == *(unsigned char *)&
			fontname[49]) {
/* Writing concatenation */
		    i__3[0] = k - 1, a__1[0] = fontname;
		    i__3[1] = 20, a__1[1] = opt;
		    s_cat(fullname, a__1, i__3, &c__2, (ftnlen)70);
		    goto L6;
		}
/* L5: */
	    }
L6:
	    swcset_("FONTNAME", fullname, (ftnlen)8, (ftnlen)70);
	    sgstxs_(&c_b26);
	    r__1 = y[i__ - 1] + .015f;
	    sgtxv_(&x1, &r__1, fullname, (ftnlen)70);
	    s_wsfi(&io___18);
	    i__2 = i__ + j * 10;
	    do_fio(&c__1, (char *)&i__2, (ftnlen)sizeof(integer));
	    e_wsfi();
	    r__1 = y[i__ - 1] + .035f;
	    sgtxzv_(&x1, &r__1, ch, &c_b26, &c__0, &c_n1, &c__1, (ftnlen)3);
	    sgstxs_(&c_b17);
	    sgtxv_(&xc, &y[i__ - 1], "ABCabs012\316\261\316\262\316\263", (
		    ftnlen)15);
	    s_wsle(&io___19);
	    do_lio(&c__9, &c__1, fullname, (ftnlen)70);
	    e_wsle();
/* L20: */
	}
/* L30: */
    }
L40:
    sgcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int sysfont_ () { MAIN__ (); return 0; }

/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static integer c__600 = 600;
static integer c__0 = 0;
static integer c__2 = 2;
static logical c_false = FALSE_;
static real c_b28 = 0.f;
static real c_b29 = 1.f;
static integer c__5 = 5;
static real c_b46 = .025f;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Initialized data */

    static real xbox[5] = { 0.f,1.f,1.f,0.f,0.f };
    static real ybox[5] = { 0.f,0.f,1.f,1.f,0.f };

    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rsfi(icilist *), do_fio(integer *, char *, ftnlen), e_rsfi(void)
	    , s_wsfi(icilist *), e_wsfi(void);

    /* Local variables */
    static integer i__, j, n;
    static real x1, x2, y1, y2;
    static integer ii, io;
    static real xx, yy;
    static char chr[2];
    static integer npr;
    extern integer lenc_(char *, ftnlen);
    static char cfmt[4], cttl[16], cpara[4];
    extern /* Subroutine */ int chngi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen);
    static integer level;
    extern /* Subroutine */ int sgcls_(void), sgfrm_(void), gropn_(integer *),
	     gliget_(char *, integer *, ftnlen), osgarg_(integer *, char *, 
	    ftnlen), clower_(char *, ftnlen), osqarn_(integer *), sgswnd_(
	    real *, real *, real *, real *), sgstrf_(void), glistx_(char *, 
	    integer *, ftnlen), sgstrn_(integer *), sgistx_(char *, integer *,
	     ftnlen), sglstx_(char *, logical *, ftnlen), sgplzu_(integer *, 
	    real *, real *, integer *, integer *), sgsvpt_(real *, real *, 
	    real *, real *), sgtnzu_(integer *, real *, real *, integer *), 
	    swcstx_(char *, char *, ftnlen, ftnlen), swistx_(char *, integer *
	    , ftnlen), sgtxzv_(real *, real *, char *, real *, integer *, 
	    integer *, integer *, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___6 = { 0, 0, 0, 0, 0 };
    static cilist io___7 = { 0, 0, 0, 0, 0 };
    static icilist io___11 = { 0, cpara, 0, cfmt, 4, 1 };
    static icilist io___23 = { 0, chr, 0, "(I1)", 2, 1 };
    static icilist io___24 = { 0, chr, 0, "(I2)", 2, 1 };


    osqarn_(&npr);
    if (npr == 0) {
	gliget_("IIUNIT", &ii, (ftnlen)6);
	gliget_("IOUNIT", &io, (ftnlen)6);
	io___6.ciunit = io;
	s_wsle(&io___6);
	do_lio(&c__9, &c__1, " TONE PATTERN (0-6) (I) ? ;", (ftnlen)27);
	e_wsle();
	io___7.ciunit = ii;
	s_rsle(&io___7);
	do_lio(&c__3, &c__1, (char *)&n, (ftnlen)sizeof(integer));
	e_rsle();
    } else {
	osgarg_(&c__1, cpara, (ftnlen)4);
	s_copy(cfmt, "(I#)", (ftnlen)4, (ftnlen)4);
	i__1 = lenc_(cpara, (ftnlen)4);
	chngi_(cfmt, "#", &i__1, "(I1)", (ftnlen)4, (ftnlen)1, (ftnlen)4);
	s_rsfi(&io___11);
	do_fio(&c__1, (char *)&n, (ftnlen)sizeof(integer));
	e_rsfi();
    }
    swistx_("IWIDTH ", &c__600, (ftnlen)7);
    swistx_("IHEIGHT", &c__600, (ftnlen)7);
    s_copy(cttl, "DCLTONE (#)", (ftnlen)16, (ftnlen)11);
    chngi_(cttl, "#", &n, "(I1)", (ftnlen)16, (ftnlen)1, (ftnlen)4);
    clower_(cttl, (ftnlen)16);
    swcstx_("TITLE", cttl, (ftnlen)5, (ftnlen)16);
    glistx_("MAXMSG", &c__0, (ftnlen)6);
    sgistx_("IFONT", &c__2, (ftnlen)5);
    sglstx_("LCORNER", &c_false, (ftnlen)7);
    gropn_(&c__1);
    sgfrm_();
    sgswnd_(&c_b28, &c_b29, &c_b28, &c_b29);
    sgstrn_(&c__1);
    for (j = 0; j <= 5; ++j) {
	for (i__ = 0; i__ <= 5; ++i__) {
	    x1 = i__ * .14999999999999999f + .08f;
	    x2 = x1 + .13500000000000001f;
	    y1 = (5 - j) * .14999999999999999f + .04f;
	    y2 = y1 + .13500000000000001f;
	    level = i__ + j * 10 + n * 100;
	    sgsvpt_(&x1, &x2, &y1, &y2);
	    sgstrf_();
	    sgtnzu_(&c__5, xbox, ybox, &level);
	    sgplzu_(&c__5, xbox, ybox, &c__1, &c__1);
/* L10: */
	}
/* L20: */
    }
    sgsvpt_(&c_b28, &c_b29, &c_b28, &c_b29);
    sgstrf_();
    for (i__ = 0; i__ <= 5; ++i__) {
	xx = (i__ + .45f) * .14999999999999999f + .08f;
	yy = .96999999999999997f;
	s_wsfi(&io___23);
	do_fio(&c__1, (char *)&i__, (ftnlen)sizeof(integer));
	e_wsfi();
	sgtxzv_(&xx, &yy, chr, &c_b46, &c__0, &c__0, &c__1, (ftnlen)2);
/* L30: */
    }
    for (j = 0; j <= 5; ++j) {
	xx = .040000000000000001f;
	yy = (5 - j + .45f) * .14999999999999999f + .04f;
	s_wsfi(&io___24);
	i__1 = j * 10;
	do_fio(&c__1, (char *)&i__1, (ftnlen)sizeof(integer));
	e_wsfi();
	sgtxzv_(&xx, &yy, chr, &c_b46, &c__0, &c__0, &c__1, (ftnlen)2);
/* L40: */
    }
    sgcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int dclton_ () { MAIN__ (); return 0; }

/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    integer s_wsfe(cilist *), do_fio(integer *, char *, ftnlen), e_wsfe(void);

    /* Local variables */
    static integer i__, nc, iu;
    static char cmd[1024];
    extern integer lenc_(char *, ftnlen);
    static integer maxcmd;
    extern /* Subroutine */ int gliget_(char *, integer *, ftnlen), swqcmd_(
	    integer *, char *, ftnlen), swcmll_(void), swqcmn_(integer *);

    /* Fortran I/O blocks */
    static cilist io___6 = { 0, 0, 0, "(TR1,I2.2,A2,A)", 0 };


    gliget_("MSGUNIT", &iu, (ftnlen)7);
    swcmll_();
    swqcmn_(&maxcmd);
    i__1 = maxcmd;
    for (i__ = 1; i__ <= i__1; ++i__) {
	swqcmd_(&i__, cmd, (ftnlen)1024);
	nc = lenc_(cmd, (ftnlen)1024);
	io___6.ciunit = iu;
	s_wsfe(&io___6);
	do_fio(&c__1, (char *)&i__, (ftnlen)sizeof(integer));
	do_fio(&c__1, ": ", (ftnlen)2);
	do_fio(&c__1, cmd, nc);
	e_wsfe();
/* L10: */
    }
    return 0;
} /* MAIN__ */

/* Main program alias */ int dclcmp_ () { MAIN__ (); return 0; }

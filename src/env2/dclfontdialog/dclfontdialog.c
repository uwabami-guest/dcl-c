/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;
static real c_b3 = .5f;
static real c_b4 = .75f;
static real c_b5 = .04f;
static integer c__0 = 0;
static integer c__3 = 3;
static real c_b11 = .7f;
static real c_b12 = .03f;
static logical c_true = TRUE_;
static real c_b29 = .45f;
static real c_b37 = .25f;
static real c_b44 = .2f;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* System generated locals */
    address a__1[3];
    integer i__1[3];
    char ch__1[1];

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen), s_cat(char *,
	     char **, integer *, integer *, ftnlen);

    /* Local variables */
    static char fontname[1024];
    extern /* Character */ VOID csgi_(char *, ftnlen, integer *);
    extern /* Subroutine */ int sgcls_(void), sgfrm_(void), sgopn_(integer *);
    static char ctext[1024], ntext[1];
    extern /* Subroutine */ int swcset_(char *, char *, ftnlen, ftnlen), 
	    swlset_(char *, logical *, ftnlen), swslft_(char *, ftnlen), 
	    sgtxzv_(real *, real *, char *, real *, integer *, integer *, 
	    integer *, ftnlen);

    sgopn_(&c__1);
    sgfrm_();
    s_copy(ctext, "Please choose a font set...", (ftnlen)1024, (ftnlen)27);
    sgtxzv_(&c_b3, &c_b4, ctext, &c_b5, &c__0, &c__0, &c__3, (ftnlen)1024);
    s_copy(ctext, "(This is drawn by DCL stroke font)", (ftnlen)1024, (ftnlen)
	    34);
    sgtxzv_(&c_b3, &c_b11, ctext, &c_b12, &c__0, &c__0, &c__3, (ftnlen)1024);
    swlset_("LSYSFNT", &c_true, (ftnlen)7);
    swslft_(fontname, (ftnlen)1024);
    csgi_(ch__1, (ftnlen)1, &c__0);
    *(unsigned char *)ntext = *(unsigned char *)&ch__1[0];
/* Writing concatenation */
    i__1[0] = 20, a__1[0] = "Selected font name: ";
    i__1[1] = 1024, a__1[1] = fontname;
    i__1[2] = 1, a__1[2] = ntext;
    s_cat(ctext, a__1, i__1, &c__3, (ftnlen)1024);
    sgtxzv_(&c_b3, &c_b3, ctext, &c_b5, &c__0, &c__0, &c__3, (ftnlen)1024);
    s_copy(ctext, "(This is drawn by DCL defalt system font)", (ftnlen)1024, (
	    ftnlen)41);
    sgtxzv_(&c_b3, &c_b29, ctext, &c_b12, &c__0, &c__0, &c__3, (ftnlen)1024);
    swcset_("FONTNAME", fontname, (ftnlen)8, (ftnlen)1024);
    s_copy(ctext, "GFD Dennou Common Library \351\233\273\350\204\263"
	    "\343\203\251\343\202\244\343\203\226\343\203\251\343\203\252 \316"
	    "\261\316\262\316\243\316\251", (ftnlen)1024, (ftnlen)56);
    sgtxzv_(&c_b3, &c_b37, ctext, &c_b12, &c__0, &c__0, &c__3, (ftnlen)1024);
    s_copy(ctext, "(This is drawn by the selected font)", (ftnlen)1024, (
	    ftnlen)36);
    sgtxzv_(&c_b3, &c_b44, ctext, &c_b12, &c__0, &c__0, &c__3, (ftnlen)1024);
    sgcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int sgfndl_ () { MAIN__ (); return 0; }

/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
integer isum1_(integer *ix, integer *n, integer *jx)
{
    /* System generated locals */
    integer ret_val, i__1, i__2;

    /* Local variables */
    static integer i__, nn, isum, imiss;
    extern /* Subroutine */ int gliget_(char *, integer *, ftnlen);

    /* Parameter adjustments */
    --ix;

    /* Function Body */
    gliget_("IMISS", &imiss, (ftnlen)5);
    isum = 0;
    nn = 0;
    i__1 = *jx * (*n - 1) + 1;
    i__2 = *jx;
    for (i__ = 1; i__2 < 0 ? i__ >= i__1 : i__ <= i__1; i__ += i__2) {
	if (ix[i__] != imiss) {
	    ++nn;
	    isum += ix[i__];
	}
/* L10: */
    }
    if (nn == 0) {
	ret_val = imiss;
    } else {
	ret_val = isum;
    }
    return ret_val;
} /* isum1_ */


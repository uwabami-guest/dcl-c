/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     DXFLOC */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int dxfloc_(integer *nd, integer *ns, integer *np, integer *
	ncp)
{
    /* System generated locals */
    integer i__1;

    /* Local variables */
    static integer n, nsx;

    /* Parameter adjustments */
    --np;
    --ns;

    /* Function Body */
    *ncp = np[1];
    nsx = 1;
    i__1 = *nd - 1;
    for (n = 1; n <= i__1; ++n) {
	nsx *= ns[n];
	*ncp += (np[n + 1] - 1) * nsx;
/* L10: */
    }
    return 0;
} /* dxfloc_ */


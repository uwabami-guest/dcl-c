/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     CRVRS */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int crvrs_(char *chr, ftnlen chr_len)
{
    /* System generated locals */
    integer i__1, i__2;

    /* Builtin functions */
    integer i_len(char *, ftnlen);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer i__, lc;
    static char ch1[1];

    lc = i_len(chr, chr_len);
    i__1 = lc / 2;
    for (i__ = 1; i__ <= i__1; ++i__) {
	i__2 = lc - i__;
	s_copy(ch1, chr + i__2, (ftnlen)1, lc - i__ + 1 - i__2);
	i__2 = lc - i__;
	s_copy(chr + i__2, chr + (i__ - 1), lc - i__ + 1 - i__2, (ftnlen)1);
	*(unsigned char *)&chr[i__ - 1] = *(unsigned char *)ch1;
/* L10: */
    }
    return 0;
} /* crvrs_ */


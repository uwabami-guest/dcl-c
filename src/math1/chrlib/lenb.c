/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     LENB */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
integer lenb_(char *c__, ftnlen c_len)
{
    /* System generated locals */
    integer ret_val;

    /* Builtin functions */
    integer i_len(char *, ftnlen);

    /* Local variables */
    static integer n, lc;
    static char cn[1], cs[1];
    static logical lchar;

    *(unsigned char *)cn = '\0';
    *(unsigned char *)cs = ' ';
    lc = i_len(c__, c_len);
    n = 1;
L15:
    lchar = n < lc && (*(unsigned char *)&c__[n - 1] == *(unsigned char *)cn 
	    || *(unsigned char *)&c__[n - 1] == *(unsigned char *)cs);
    if (! lchar) {
	goto L10;
    }
    ++n;
    goto L15;
L10:
    ret_val = n - 1;
    return ret_val;
} /* lenb_ */


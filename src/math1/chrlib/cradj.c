/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     CRADJ */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int cradj_(char *chr, ftnlen chr_len)
{
    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    integer i_len(char *, ftnlen);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer i__, lc1, lc2;
    extern integer lenc_(char *, ftnlen);

    lc1 = i_len(chr, chr_len);
    lc2 = lenc_(chr, chr_len);
    if (lc1 != lc2) {
	for (i__ = lc1; i__ >= 1; --i__) {
	    if (i__ > lc1 - lc2) {
		i__1 = i__ - (lc1 - lc2) - 1;
		s_copy(chr + (i__ - 1), chr + i__1, (ftnlen)1, i__ - (lc1 - 
			lc2) - i__1);
	    } else {
		*(unsigned char *)&chr[i__ - 1] = ' ';
	    }
/* L10: */
	}
    }
    return 0;
} /* cradj_ */


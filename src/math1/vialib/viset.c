/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     VISET */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int viset_0_(int n__, integer *ix, integer *iy, integer *n, 
	integer *jx, integer *jy)
{
    /* System generated locals */
    integer i__1;

    /* Local variables */
    static integer j, kx, ky;

/*     --------------------------- */
/*     --------------------------- */
    /* Parameter adjustments */
    --ix;
    --iy;

    /* Function Body */
    switch(n__) {
	case 1: goto L_viset0;
	case 2: goto L_viset1;
	}


L_viset0:

L_viset1:
    kx = 1 - *jx;
    ky = 1 - *jy;
    i__1 = *n;
    for (j = 1; j <= i__1; ++j) {
	kx += *jx;
	ky += *jy;
	iy[ky] = ix[kx];
/* L10: */
    }
    return 0;
} /* viset_ */

/* Subroutine */ int viset_(integer *ix, integer *iy, integer *n, integer *jx,
	 integer *jy)
{
    return viset_0_(0, ix, iy, n, jx, jy);
    }

/* Subroutine */ int viset0_(integer *ix, integer *iy, integer *n, integer *
	jx, integer *jy)
{
    return viset_0_(1, ix, iy, n, jx, jy);
    }

/* Subroutine */ int viset1_(integer *ix, integer *iy, integer *n, integer *
	jx, integer *jy)
{
    return viset_0_(2, ix, iy, n, jx, jy);
    }


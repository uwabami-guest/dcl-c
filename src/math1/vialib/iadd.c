/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     IADD */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int iadd_(integer *ix, integer *n, integer *jx, integer *ii)
{
    extern /* Subroutine */ int iadd0_(integer *, integer *, integer *, 
	    integer *), iadd1_(integer *, integer *, integer *, integer *);
    static logical lmiss;
    extern /* Subroutine */ int gllget_(char *, logical *, ftnlen);

    /* Parameter adjustments */
    --ix;

    /* Function Body */
    gllget_("LMISS", &lmiss, (ftnlen)5);
    if (lmiss) {
	iadd1_(&ix[1], n, jx, ii);
    } else {
	iadd0_(&ix[1], n, jx, ii);
    }
    return 0;
} /* iadd_ */


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     INDXRF */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
integer indxrf_(real *rx, integer *n, integer *jd, real *rr)
{
    /* System generated locals */
    integer ret_val, i__1;

    /* Local variables */
    static integer i__, j;

    /* Parameter adjustments */
    --rx;

    /* Function Body */
    ret_val = 0;
    i__1 = *n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	j = *jd * (i__ - 1) + 1;
	if (rx[j] == *rr) {
	    ret_val = i__;
	    return ret_val;
	}
/* L10: */
    }
    return ret_val;
} /* indxrf_ */


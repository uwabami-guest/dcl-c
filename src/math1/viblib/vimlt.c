/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     VIMLT */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int vimlt_(integer *ix, integer *iy, integer *iz, integer *n,
	 integer *jx, integer *jy, integer *jz)
{
    static logical lmiss;
    extern /* Subroutine */ int vimlt0_(integer *, integer *, integer *, 
	    integer *, integer *, integer *, integer *), vimlt1_(integer *, 
	    integer *, integer *, integer *, integer *, integer *, integer *),
	     gllget_(char *, logical *, ftnlen);

    /* Parameter adjustments */
    --iz;
    --iy;
    --ix;

    /* Function Body */
    gllget_("LMISS", &lmiss, (ftnlen)5);
    if (lmiss) {
	vimlt1_(&ix[1], &iy[1], &iz[1], n, jx, jy, jz);
    } else {
	vimlt0_(&ix[1], &iy[1], &iz[1], n, jx, jy, jz);
    }
    return 0;
} /* vimlt_ */


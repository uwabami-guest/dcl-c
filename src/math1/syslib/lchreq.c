/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     LCHREQ FOR ASCII CHARACTER CODE */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
logical lchreq_(char *ch1, char *ch2, ftnlen ch1_len, ftnlen ch2_len)
{
    /* System generated locals */
    integer i__1;
    logical ret_val;

    /* Builtin functions */
    integer i_len(char *, ftnlen);

    /* Local variables */
    static integer i__, lch1, lch2, idx1, idx2, iblk, lchmin, lchmax;

    ret_val = TRUE_;
    lch1 = i_len(ch1, ch1_len);
    lch2 = i_len(ch2, ch2_len);
    lchmax = max(lch1,lch2);
    lchmin = min(lch1,lch2);
    i__1 = lchmin;
    for (i__ = 1; i__ <= i__1; ++i__) {
	idx1 = *(unsigned char *)&ch1[i__ - 1];
	idx2 = *(unsigned char *)&ch2[i__ - 1];
	if (idx1 != idx2) {
	    if (65 <= idx1 && idx1 <= 90) {
		ret_val = idx2 - idx1 == 32;
	    } else if (97 <= idx1 && idx1 <= 122) {
		ret_val = idx1 - idx2 == 32;
	    } else {
		ret_val = FALSE_;
	    }
	    if (! ret_val) {
		return ret_val;
	    }
	}
/* L10: */
    }
    if (lch1 == lch2) {
	return ret_val;
    }
    iblk = ' ';
    if (lch1 > lch2) {
	i__1 = lchmax;
	for (i__ = lchmin + 1; i__ <= i__1; ++i__) {
	    ret_val = *(unsigned char *)&ch1[i__ - 1] == iblk;
	    if (! ret_val) {
		return ret_val;
	    }
/* L20: */
	}
    } else {
	i__1 = lchmax;
	for (i__ = lchmin + 1; i__ <= i__1; ++i__) {
	    ret_val = *(unsigned char *)&ch2[i__ - 1] == iblk;
	    if (! ret_val) {
		return ret_val;
	    }
/* L30: */
	}
    }
    return ret_val;
} /* lchreq_ */


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     GLRGET / GLRSET / GLRSTX */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int glrget_0_(int n__, char *cp, real *rpara, ftnlen cp_len)
{
    static char cx[40];
    static real rp;
    static integer idx;
    extern /* Subroutine */ int glrqid_(char *, integer *, ftnlen), glrqcl_(
	    integer *, char *, ftnlen), glrqcp_(integer *, char *, ftnlen), 
	    rlrget_(char *, real *, integer *, ftnlen), glrqvl_(integer *, 
	    real *), rtrget_(char *, char *, real *, integer *, ftnlen, 
	    ftnlen), glrsvl_(integer *, real *);

    switch(n__) {
	case 1: goto L_glrset;
	case 2: goto L_glrstx;
	}

    glrqid_(cp, &idx, cp_len);
    glrqvl_(&idx, rpara);
    return 0;
/* ----------------------------------------------------------------------- */

L_glrset:
    glrqid_(cp, &idx, cp_len);
    glrsvl_(&idx, rpara);
    return 0;
/* ----------------------------------------------------------------------- */

L_glrstx:
    rp = *rpara;
    glrqid_(cp, &idx, cp_len);
/*     / SHORT NAME / */
    glrqcp_(&idx, cx, (ftnlen)40);
    rtrget_("GL", cx, &rp, &c__1, (ftnlen)2, (ftnlen)40);
/*     / LONG NAME / */
    glrqcl_(&idx, cx, (ftnlen)40);
    rlrget_(cx, &rp, &c__1, (ftnlen)40);
    glrsvl_(&idx, &rp);
    return 0;
} /* glrget_ */

/* Subroutine */ int glrget_(char *cp, real *rpara, ftnlen cp_len)
{
    return glrget_0_(0, cp, rpara, cp_len);
    }

/* Subroutine */ int glrset_(char *cp, real *rpara, ftnlen cp_len)
{
    return glrget_0_(1, cp, rpara, cp_len);
    }

/* Subroutine */ int glrstx_(char *cp, real *rpara, ftnlen cp_len)
{
    return glrget_0_(2, cp, rpara, cp_len);
    }


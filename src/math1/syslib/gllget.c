/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     GLLGET / GLLSET / GLLSTX */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int gllget_0_(int n__, char *cp, logical *lpara, ftnlen 
	cp_len)
{
    static char cx[40];
    static logical lp;
    static integer idx;
    extern /* Subroutine */ int gllqid_(char *, integer *, ftnlen), gllqcl_(
	    integer *, char *, ftnlen), gllqcp_(integer *, char *, ftnlen), 
	    rllget_(char *, logical *, integer *, ftnlen), gllqvl_(integer *, 
	    logical *), rtlget_(char *, char *, logical *, integer *, ftnlen, 
	    ftnlen), gllsvl_(integer *, logical *);

    switch(n__) {
	case 1: goto L_gllset;
	case 2: goto L_gllstx;
	}

    gllqid_(cp, &idx, cp_len);
    gllqvl_(&idx, lpara);
    return 0;
/* ----------------------------------------------------------------------- */

L_gllset:
    gllqid_(cp, &idx, cp_len);
    gllsvl_(&idx, lpara);
    return 0;
/* ----------------------------------------------------------------------- */

L_gllstx:
    lp = *lpara;
    gllqid_(cp, &idx, cp_len);
/*     / SHORT NAME / */
    gllqcp_(&idx, cx, (ftnlen)40);
    rtlget_("GL", cx, &lp, &c__1, (ftnlen)2, (ftnlen)40);
/*     / LONG NAME / */
    gllqcl_(&idx, cx, (ftnlen)40);
    rllget_(cx, &lp, &c__1, (ftnlen)40);
    gllsvl_(&idx, &lp);
    return 0;
} /* gllget_ */

/* Subroutine */ int gllget_(char *cp, logical *lpara, ftnlen cp_len)
{
    return gllget_0_(0, cp, lpara, cp_len);
    }

/* Subroutine */ int gllset_(char *cp, logical *lpara, ftnlen cp_len)
{
    return gllget_0_(1, cp, lpara, cp_len);
    }

/* Subroutine */ int gllstx_(char *cp, logical *lpara, ftnlen cp_len)
{
    return gllget_0_(2, cp, lpara, cp_len);
    }


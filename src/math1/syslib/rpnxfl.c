/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;
static integer c__3 = 3;

/* ----------------------------------------------------------------------- */
/*     GET EXTERNAL PARAMETER FROM 'DCLRC' FILE */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int rpnxfl_0_(int n__, char *cpfix, char *cp, integer *ipara,
	 logical *lpara, real *rpara, char *cval, ftnlen cpfix_len, ftnlen 
	cp_len, ftnlen cval_len)
{
    /* Initialized data */

    static char csepx[1] = "_";
    static char ccomx[1] = "*";
    static char cdivx[1] = " ";

    /* System generated locals */
    address a__1[3];
    integer i__1, i__2[3];
    olist o__1;
    cllist cl__1;
    alist al__1;

    /* Builtin functions */
    integer s_cmp(char *, char *, ftnlen, ftnlen), f_open(olist *), f_rew(
	    alist *), s_rsfe(cilist *), do_fio(integer *, char *, ftnlen), 
	    e_rsfe(void), i_indx(char *, char *, ftnlen, ftnlen);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer f_clos(cllist *);
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen);

    /* Local variables */
    static integer n;
    static char cx[80*100];
    static integer nn, iu, idb;
    static char cpx[16];
    static integer ios;
    static char cadj[32], ccom[1];
    extern integer lenc_(char *, ftnlen);
    static char cdiv[1], cdsn[1024], csep[1];
    extern /* Subroutine */ int cladj_(char *, ftnlen);
    static char cpara[80*100], cline[80];
    extern logical lchreq_(char *, char *, ftnlen, ftnlen);
    extern integer ifromc_(char *, ftnlen);
    extern logical lfromc_(char *, ftnlen);
    extern /* Subroutine */ int glqfnm_(char *, char *, ftnlen, ftnlen), 
	    msgdmp_(char *, char *, char *, ftnlen, ftnlen, ftnlen);
    extern real rfromc_(char *, ftnlen);
    extern integer iufopn_(void);
    extern /* Subroutine */ int osgenv_(char *, char *, ftnlen, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___11 = { 1, 0, 1, "(A)", 0 };


    switch(n__) {
	case 1: goto L_rtixfl;
	case 2: goto L_rtlxfl;
	case 3: goto L_rtrxfl;
	case 4: goto L_rtcxfl;
	case 5: goto L_rlixfl;
	case 6: goto L_rllxfl;
	case 7: goto L_rlrxfl;
	case 8: goto L_rlcxfl;
	}

    osgenv_("DCLENVCHAR", csep, (ftnlen)10, (ftnlen)1);
    if (*(unsigned char *)csep == ' ') {
	*(unsigned char *)csep = *(unsigned char *)&csepx[0];
    }
    osgenv_("DCLCOMCHAR", ccom, (ftnlen)10, (ftnlen)1);
    if (*(unsigned char *)ccom == ' ') {
	*(unsigned char *)ccom = *(unsigned char *)&ccomx[0];
    }
    osgenv_("DCLDIVCHAR", cdiv, (ftnlen)10, (ftnlen)1);
    if (*(unsigned char *)cdiv == ' ') {
	*(unsigned char *)cdiv = *(unsigned char *)&cdivx[0];
    }
    glqfnm_("DCLRC", cdsn, (ftnlen)5, (ftnlen)1024);
    if (s_cmp(cdsn, " ", (ftnlen)1024, (ftnlen)1) == 0) {
	return 0;
    }
    iu = iufopn_();
    if (iu == 0) {
	return 0;
    }
    o__1.oerr = 0;
    o__1.ounit = iu;
    o__1.ofnmlen = 1024;
    o__1.ofnm = cdsn;
    o__1.orl = 0;
    o__1.osta = 0;
    o__1.oacc = 0;
    o__1.ofm = "FORMATTED";
    o__1.oblnk = 0;
    f_open(&o__1);
    al__1.aerr = 0;
    al__1.aunit = iu;
    f_rew(&al__1);
    nn = 0;
L10:
    io___11.ciunit = iu;
    ios = s_rsfe(&io___11);
    if (ios != 0) {
	goto L100001;
    }
    ios = do_fio(&c__1, cline, (ftnlen)80);
    if (ios != 0) {
	goto L100001;
    }
    ios = e_rsfe();
L100001:
    if (ios == 0) {
	if (*(unsigned char *)cline != *(unsigned char *)ccom) {
	    ++nn;
	    if (nn > 100) {
		msgdmp_("E", "RTNXFL", "NUMBER OF INPUT PARAMETERS IS TOO LA"
			"RGE (>100).", (ftnlen)1, (ftnlen)6, (ftnlen)47);
	    }
	    idb = i_indx(cline, cdiv, (ftnlen)80, (ftnlen)1);
	    s_copy(cpara + (nn - 1) * 80, cline, (ftnlen)80, idb - 1);
	    i__1 = idb;
	    s_copy(cx + (nn - 1) * 80, cline + i__1, (ftnlen)80, lenc_(cline, 
		    (ftnlen)80) - i__1);
	    cladj_(cx + (nn - 1) * 80, (ftnlen)80);
	}
    }
    if (ios == 0) {
	goto L10;
    }
    cl__1.cerr = 0;
    cl__1.cunit = iu;
    cl__1.csta = 0;
    f_clos(&cl__1);
    return 0;
/* ----------------------------------------------------------------------- */
/*     GET INTEGER PARAMETER WITH SHORT NAME */
/* ----------------------------------------------------------------------- */

L_rtixfl:
/* Writing concatenation */
    i__2[0] = lenc_(cpfix, cpfix_len), a__1[0] = cpfix;
    i__2[1] = 1, a__1[1] = csep;
    i__2[2] = cp_len, a__1[2] = cp;
    s_cat(cpx, a__1, i__2, &c__3, (ftnlen)16);
    i__1 = nn;
    for (n = 1; n <= i__1; ++n) {
	if (lchreq_(cpx, cpara + (n - 1) * 80, (ftnlen)16, (ftnlen)80)) {
	    s_copy(cadj, cx + (n - 1) * 80, (ftnlen)32, (ftnlen)80);
	    cladj_(cadj, (ftnlen)32);
	    *ipara = ifromc_(cadj, (ftnlen)32);
	    return 0;
	}
/* L20: */
    }
    return 0;
/* ----------------------------------------------------------------------- */
/*     GET LOGICAL PARAMETER WITH SHORT NAME */
/* ----------------------------------------------------------------------- */

L_rtlxfl:
/* Writing concatenation */
    i__2[0] = lenc_(cpfix, cpfix_len), a__1[0] = cpfix;
    i__2[1] = 1, a__1[1] = csep;
    i__2[2] = cp_len, a__1[2] = cp;
    s_cat(cpx, a__1, i__2, &c__3, (ftnlen)16);
    i__1 = nn;
    for (n = 1; n <= i__1; ++n) {
	if (lchreq_(cpx, cpara + (n - 1) * 80, (ftnlen)16, (ftnlen)80)) {
	    s_copy(cadj, cx + (n - 1) * 80, (ftnlen)32, (ftnlen)80);
	    cladj_(cadj, (ftnlen)32);
	    *lpara = lfromc_(cadj, (ftnlen)32);
	    return 0;
	}
/* L30: */
    }
    return 0;
/* ----------------------------------------------------------------------- */
/*     GET REAL PARAMETER WITH SHORT NAME */
/* ----------------------------------------------------------------------- */

L_rtrxfl:
/* Writing concatenation */
    i__2[0] = lenc_(cpfix, cpfix_len), a__1[0] = cpfix;
    i__2[1] = 1, a__1[1] = csep;
    i__2[2] = cp_len, a__1[2] = cp;
    s_cat(cpx, a__1, i__2, &c__3, (ftnlen)16);
    i__1 = nn;
    for (n = 1; n <= i__1; ++n) {
	if (lchreq_(cpx, cpara + (n - 1) * 80, (ftnlen)16, (ftnlen)80)) {
	    s_copy(cadj, cx + (n - 1) * 80, (ftnlen)32, (ftnlen)80);
	    cladj_(cadj, (ftnlen)32);
	    *rpara = rfromc_(cadj, (ftnlen)32);
	    return 0;
	}
/* L40: */
    }
    return 0;
/* ----------------------------------------------------------------------- */
/*     GET CHARACTER PARAMETER WITH SHORT NAME */
/* ----------------------------------------------------------------------- */

L_rtcxfl:
/* Writing concatenation */
    i__2[0] = lenc_(cpfix, cpfix_len), a__1[0] = cpfix;
    i__2[1] = 1, a__1[1] = csep;
    i__2[2] = cp_len, a__1[2] = cp;
    s_cat(cpx, a__1, i__2, &c__3, (ftnlen)16);
    i__1 = nn;
    for (n = 1; n <= i__1; ++n) {
	if (lchreq_(cpx, cpara + (n - 1) * 80, (ftnlen)16, (ftnlen)80)) {
	    s_copy(cval, cx + (n - 1) * 80, cval_len, (ftnlen)80);
	    return 0;
	}
/* L50: */
    }
    return 0;
/* ----------------------------------------------------------------------- */
/*     GET INTEGER PARAMETER WITH LONG NAME */
/* ----------------------------------------------------------------------- */

L_rlixfl:
    i__1 = nn;
    for (n = 1; n <= i__1; ++n) {
	if (lchreq_(cp, cpara + (n - 1) * 80, cp_len, (ftnlen)80)) {
	    s_copy(cadj, cx + (n - 1) * 80, (ftnlen)32, (ftnlen)80);
	    cladj_(cadj, (ftnlen)32);
	    *ipara = ifromc_(cadj, (ftnlen)32);
	    return 0;
	}
/* L60: */
    }
    return 0;
/* ----------------------------------------------------------------------- */
/*     GET LOGICAL PARAMETER WITH LONG NAME */
/* ----------------------------------------------------------------------- */

L_rllxfl:
    i__1 = nn;
    for (n = 1; n <= i__1; ++n) {
	if (lchreq_(cp, cpara + (n - 1) * 80, cp_len, (ftnlen)80)) {
	    s_copy(cadj, cx + (n - 1) * 80, (ftnlen)32, (ftnlen)80);
	    cladj_(cadj, (ftnlen)32);
	    *lpara = lfromc_(cadj, (ftnlen)32);
	    return 0;
	}
/* L70: */
    }
    return 0;
/* ----------------------------------------------------------------------- */
/*     GET REAL PARAMETER WITH LONG NAME */
/* ----------------------------------------------------------------------- */

L_rlrxfl:
    i__1 = nn;
    for (n = 1; n <= i__1; ++n) {
	if (lchreq_(cp, cpara + (n - 1) * 80, cp_len, (ftnlen)80)) {
	    s_copy(cadj, cx + (n - 1) * 80, (ftnlen)32, (ftnlen)80);
	    cladj_(cadj, (ftnlen)32);
	    *rpara = rfromc_(cadj, (ftnlen)32);
	    return 0;
	}
/* L80: */
    }
    return 0;
/* ----------------------------------------------------------------------- */
/*     GET CHARACTER PARAMETER WITH LONG NAME */
/* ----------------------------------------------------------------------- */

L_rlcxfl:
    i__1 = nn;
    for (n = 1; n <= i__1; ++n) {
	if (lchreq_(cp, cpara + (n - 1) * 80, cp_len, (ftnlen)80)) {
	    s_copy(cval, cx + (n - 1) * 80, cval_len, (ftnlen)80);
	    return 0;
	}
/* L90: */
    }
    return 0;
} /* rpnxfl_ */

/* Subroutine */ int rpnxfl_(void)
{
    return rpnxfl_0_(0, (char *)0, (char *)0, (integer *)0, (logical *)0, (
	    real *)0, (char *)0, (ftnint)0, (ftnint)0, (ftnint)0);
    }

/* Subroutine */ int rtixfl_(char *cpfix, char *cp, integer *ipara, ftnlen 
	cpfix_len, ftnlen cp_len)
{
    return rpnxfl_0_(1, cpfix, cp, ipara, (logical *)0, (real *)0, (char *)0, 
	    cpfix_len, cp_len, (ftnint)0);
    }

/* Subroutine */ int rtlxfl_(char *cpfix, char *cp, logical *lpara, ftnlen 
	cpfix_len, ftnlen cp_len)
{
    return rpnxfl_0_(2, cpfix, cp, (integer *)0, lpara, (real *)0, (char *)0, 
	    cpfix_len, cp_len, (ftnint)0);
    }

/* Subroutine */ int rtrxfl_(char *cpfix, char *cp, real *rpara, ftnlen 
	cpfix_len, ftnlen cp_len)
{
    return rpnxfl_0_(3, cpfix, cp, (integer *)0, (logical *)0, rpara, (char *)
	    0, cpfix_len, cp_len, (ftnint)0);
    }

/* Subroutine */ int rtcxfl_(char *cpfix, char *cp, char *cval, ftnlen 
	cpfix_len, ftnlen cp_len, ftnlen cval_len)
{
    return rpnxfl_0_(4, cpfix, cp, (integer *)0, (logical *)0, (real *)0, 
	    cval, cpfix_len, cp_len, cval_len);
    }

/* Subroutine */ int rlixfl_(char *cp, integer *ipara, ftnlen cp_len)
{
    return rpnxfl_0_(5, (char *)0, cp, ipara, (logical *)0, (real *)0, (char *
	    )0, (ftnint)0, cp_len, (ftnint)0);
    }

/* Subroutine */ int rllxfl_(char *cp, logical *lpara, ftnlen cp_len)
{
    return rpnxfl_0_(6, (char *)0, cp, (integer *)0, lpara, (real *)0, (char *
	    )0, (ftnint)0, cp_len, (ftnint)0);
    }

/* Subroutine */ int rlrxfl_(char *cp, real *rpara, ftnlen cp_len)
{
    return rpnxfl_0_(7, (char *)0, cp, (integer *)0, (logical *)0, rpara, (
	    char *)0, (ftnint)0, cp_len, (ftnint)0);
    }

/* Subroutine */ int rlcxfl_(char *cp, char *cval, ftnlen cp_len, ftnlen 
	cval_len)
{
    return rpnxfl_0_(8, (char *)0, cp, (integer *)0, (logical *)0, (real *)0, 
	    cval, (ftnint)0, cp_len, cval_len);
    }


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__2 = 2;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int cfsrch_(char *cplist, integer *np, char *cflist, integer 
	*nf, char *cfname, ftnlen cplist_len, ftnlen cflist_len, ftnlen 
	cfname_len)
{
    /* System generated locals */
    address a__1[2];
    integer i__1, i__2, i__3[2];
    inlist ioin__1;

    /* Builtin functions */
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen);
    integer f_inqu(inlist *), i_len(char *, ftnlen);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer m, n;
    extern integer lenc_(char *, ftnlen);
    static char cdsn[1024];
    extern /* Subroutine */ int cladj_(char *, ftnlen), msgdmp_(char *, char *
	    , char *, ftnlen, ftnlen, ftnlen);
    static logical lexist;

    /* Parameter adjustments */
    cflist -= 1024;
    cplist -= 1024;

    /* Function Body */
    i__1 = *np;
    for (m = 1; m <= i__1; ++m) {
	i__2 = *nf;
	for (n = 1; n <= i__2; ++n) {
/* Writing concatenation */
	    i__3[0] = lenc_(cplist + (m << 10), (ftnlen)1024), a__1[0] = 
		    cplist + (m << 10);
	    i__3[1] = 1024, a__1[1] = cflist + (n << 10);
	    s_cat(cdsn, a__1, i__3, &c__2, (ftnlen)1024);
	    cladj_(cdsn, (ftnlen)1024);
	    ioin__1.inerr = 0;
	    ioin__1.infilen = 1024;
	    ioin__1.infile = cdsn;
	    ioin__1.inex = &lexist;
	    ioin__1.inopen = 0;
	    ioin__1.innum = 0;
	    ioin__1.innamed = 0;
	    ioin__1.inname = 0;
	    ioin__1.inacc = 0;
	    ioin__1.inseq = 0;
	    ioin__1.indir = 0;
	    ioin__1.infmt = 0;
	    ioin__1.inform = 0;
	    ioin__1.inunf = 0;
	    ioin__1.inrecl = 0;
	    ioin__1.innrec = 0;
	    ioin__1.inblank = 0;
	    f_inqu(&ioin__1);
	    if (lexist) {
		if (lenc_(cdsn, (ftnlen)1024) <= i_len(cfname, cfname_len)) {
		    s_copy(cfname, cdsn, cfname_len, (ftnlen)1024);
		    return 0;
		} else {
		    msgdmp_("E", "CFSRCH", "LENGTH OF CHARACTER IS TOO SHORT"
			    " TO STORE FILE NAME.", (ftnlen)1, (ftnlen)6, (
			    ftnlen)52);
		}
	    }
/* L20: */
	}
/* L10: */
    }
    s_copy(cfname, " ", cfname_len, (ftnlen)1);
    return 0;
} /* cfsrch_ */


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__3 = 3;
static integer c__6 = 6;

/* ----------------------------------------------------------------------- */
/*     REAL PARAMETER CONTROL */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int glrqnp_0_(int n__, integer *ncp, char *cp, integer *idx, 
	real *rpara, integer *in, ftnlen cp_len)
{
    /* Initialized data */

    static char cparas[8*6] = "RMISS   " "RUNDEF  " "REPSL   " "RFACT   " 
	    "REALMAX " "REALMIN ";
    static real rx[6] = { 999.f,-999.f,@REPSL,1.f,@REALMAX,@REALMIN };
    static char cparal[40*6] = "MISSING_REAL                            " 
	    "UNDEFINED_REAL                          " "TRUNCATION_ERROR    "
	    "                    " "TRUNCATION_FACTOR                       " 
	    "MAX_REAL                                " "MIN_REAL            "
	    "                    ";
    static logical lw[6] = { TRUE_,TRUE_,FALSE_,TRUE_,FALSE_,FALSE_ };
    static logical lfirst = TRUE_;

    /* System generated locals */
    address a__1[3];
    integer i__1[3];

    /* Builtin functions */
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen),
	     s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer n;
    extern integer lenc_(char *, ftnlen);
    static char cmsg[80];
    extern logical lchreq_(char *, char *, ftnlen, ftnlen);
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), rlrget_(char *, real *, integer *, ftnlen), 
	    rtrget_(char *, char *, real *, integer *, ftnlen, ftnlen);

/*     / SHORT NAME / */
    switch(n__) {
	case 1: goto L_glrqid;
	case 2: goto L_glrqcp;
	case 3: goto L_glrqcl;
	case 4: goto L_glrqvl;
	case 5: goto L_glrsvl;
	case 6: goto L_glrqin;
	}

/*     / LONG NAME / */
    *ncp = 6;
    return 0;
/* ----------------------------------------------------------------------- */

L_glrqid:
    for (n = 1; n <= 6; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *idx = n;
	    return 0;
	}
/* L10: */
    }
/* Writing concatenation */
    i__1[0] = 11, a__1[0] = "PARAMETER '";
    i__1[1] = lenc_(cp, cp_len), a__1[1] = cp;
    i__1[2] = 17, a__1[2] = "' IS NOT DEFINED.";
    s_cat(cmsg, a__1, i__1, &c__3, (ftnlen)80);
    msgdmp_("E", "GLRQID", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
    return 0;
/* ----------------------------------------------------------------------- */

L_glrqcp:
    if (1 <= *idx && *idx <= 6) {
	s_copy(cp, cparas + (*idx - 1 << 3), cp_len, (ftnlen)8);
    } else {
	msgdmp_("E", "GLRQCP", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_glrqcl:
    if (1 <= *idx && *idx <= 6) {
	s_copy(cp, cparal + (*idx - 1) * 40, cp_len, (ftnlen)40);
    } else {
	msgdmp_("E", "GLRQCL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_glrqvl:
    if (lfirst) {
	rtrget_("GL", cparas, rx, &c__6, (ftnlen)2, (ftnlen)8);
	rlrget_(cparal, rx, &c__6, (ftnlen)40);
	lfirst = FALSE_;
    }
    if (1 <= *idx && *idx <= 6) {
	*rpara = rx[*idx - 1];
    } else {
	msgdmp_("E", "GLRQVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_glrsvl:
    if (lfirst) {
	rtrget_("GL", cparas, rx, &c__6, (ftnlen)2, (ftnlen)8);
	rlrget_(cparal, rx, &c__6, (ftnlen)40);
	lfirst = FALSE_;
    }
    if (1 <= *idx && *idx <= 6) {
	if (lw[*idx - 1]) {
	    rx[*idx - 1] = *rpara;
	    return 0;
	} else {
/* Writing concatenation */
	    i__1[0] = 10, a__1[0] = "PARAMETER'";
	    i__1[1] = 8, a__1[1] = cparas + (*idx - 1 << 3);
	    i__1[2] = 16, a__1[2] = "' CANNOT BE SET.";
	    s_cat(cmsg, a__1, i__1, &c__3, (ftnlen)80);
	    msgdmp_("E", "GLRQVL", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
	}
    } else {
	msgdmp_("E", "GLRQVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_glrqin:
    for (n = 1; n <= 6; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *in = n;
	    return 0;
	}
/* L20: */
    }
    *in = 0;
    return 0;
} /* glrqnp_ */

/* Subroutine */ int glrqnp_(integer *ncp)
{
    return glrqnp_0_(0, ncp, (char *)0, (integer *)0, (real *)0, (integer *)0,
	     (ftnint)0);
    }

/* Subroutine */ int glrqid_(char *cp, integer *idx, ftnlen cp_len)
{
    return glrqnp_0_(1, (integer *)0, cp, idx, (real *)0, (integer *)0, 
	    cp_len);
    }

/* Subroutine */ int glrqcp_(integer *idx, char *cp, ftnlen cp_len)
{
    return glrqnp_0_(2, (integer *)0, cp, idx, (real *)0, (integer *)0, 
	    cp_len);
    }

/* Subroutine */ int glrqcl_(integer *idx, char *cp, ftnlen cp_len)
{
    return glrqnp_0_(3, (integer *)0, cp, idx, (real *)0, (integer *)0, 
	    cp_len);
    }

/* Subroutine */ int glrqvl_(integer *idx, real *rpara)
{
    return glrqnp_0_(4, (integer *)0, (char *)0, idx, rpara, (integer *)0, (
	    ftnint)0);
    }

/* Subroutine */ int glrsvl_(integer *idx, real *rpara)
{
    return glrqnp_0_(5, (integer *)0, (char *)0, idx, rpara, (integer *)0, (
	    ftnint)0);
    }

/* Subroutine */ int glrqin_(char *cp, integer *in, ftnlen cp_len)
{
    return glrqnp_0_(6, (integer *)0, cp, (integer *)0, (real *)0, in, cp_len)
	    ;
    }


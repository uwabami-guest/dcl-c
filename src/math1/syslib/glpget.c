/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     GLPGET / GLPSET / GLPSTX */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int glpget_0_(int n__, char *cp, integer *ipara, ftnlen 
	cp_len)
{
    static char cl[40];
    static integer ip;
    static char cx[40];
    static integer it, idx;
    extern /* Subroutine */ int gliqid_(char *, integer *, ftnlen), gllqid_(
	    char *, integer *, ftnlen), glpqid_(char *, integer *, ftnlen), 
	    glpqcl_(integer *, char *, ftnlen), glrqid_(char *, integer *, 
	    ftnlen), glpqcp_(integer *, char *, ftnlen), rliget_(char *, 
	    integer *, integer *, ftnlen), rllget_(char *, integer *, integer 
	    *, ftnlen), rtiget_(char *, char *, integer *, integer *, ftnlen, 
	    ftnlen), glpqit_(integer *, integer *), glisvl_(integer *, 
	    integer *), rtlget_(char *, char *, integer *, integer *, ftnlen, 
	    ftnlen), gllsvl_(integer *, integer *), rlrget_(char *, integer *,
	     integer *, ftnlen), glpqvl_(integer *, integer *), glpsvl_(
	    integer *, integer *), rtrget_(char *, char *, integer *, integer 
	    *, ftnlen, ftnlen), glrsvl_(integer *, integer *);

    switch(n__) {
	case 1: goto L_glpset;
	case 2: goto L_glpstx;
	}

    glpqid_(cp, &idx, cp_len);
    glpqvl_(&idx, ipara);
    return 0;
/* ----------------------------------------------------------------------- */

L_glpset:
    glpqid_(cp, &idx, cp_len);
    glpsvl_(&idx, ipara);
    return 0;
/* ----------------------------------------------------------------------- */

L_glpstx:
    ip = *ipara;
    glpqid_(cp, &idx, cp_len);
    glpqit_(&idx, &it);
    glpqcp_(&idx, cx, (ftnlen)40);
    glpqcl_(&idx, cl, (ftnlen)40);
    if (it == 1) {
	rtiget_("GL", cx, &ip, &c__1, (ftnlen)2, (ftnlen)40);
	rliget_(cl, &ip, &c__1, (ftnlen)40);
	gliqid_(cp, &idx, cp_len);
	glisvl_(&idx, &ip);
    } else if (it == 2) {
	rtlget_("GL", cx, &ip, &c__1, (ftnlen)2, (ftnlen)40);
	rllget_(cl, &ip, &c__1, (ftnlen)40);
	gllqid_(cp, &idx, cp_len);
	gllsvl_(&idx, &ip);
    } else if (it == 3) {
	rtrget_("GL", cx, &ip, &c__1, (ftnlen)2, (ftnlen)40);
	rlrget_(cl, &ip, &c__1, (ftnlen)40);
	glrqid_(cp, &idx, cp_len);
	glrsvl_(&idx, &ip);
    }
    return 0;
} /* glpget_ */

/* Subroutine */ int glpget_(char *cp, integer *ipara, ftnlen cp_len)
{
    return glpget_0_(0, cp, ipara, cp_len);
    }

/* Subroutine */ int glpset_(char *cp, integer *ipara, ftnlen cp_len)
{
    return glpget_0_(1, cp, ipara, cp_len);
    }

/* Subroutine */ int glpstx_(char *cp, integer *ipara, ftnlen cp_len)
{
    return glpget_0_(2, cp, ipara, cp_len);
    }


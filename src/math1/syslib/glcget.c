/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     GLCGET / GLCSET / GLCSTX */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int glcget_0_(int n__, char *cp, char *cpara, ftnlen cp_len, 
	ftnlen cpara_len)
{
    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static char cx[40];
    static integer idx;
    static char cpval[80];
    extern /* Subroutine */ int glcqid_(char *, integer *, ftnlen), glcqcl_(
	    integer *, char *, ftnlen), glcqcp_(integer *, char *, ftnlen), 
	    rlcget_(char *, char *, integer *, ftnlen, ftnlen), glcqvl_(
	    integer *, char *, ftnlen), rtcget_(char *, char *, char *, 
	    integer *, ftnlen, ftnlen, ftnlen), glcsvl_(integer *, char *, 
	    ftnlen);

    switch(n__) {
	case 1: goto L_glcset;
	case 2: goto L_glcstx;
	}

    glcqid_(cp, &idx, cp_len);
    glcqvl_(&idx, cpara, cpara_len);
    return 0;
/* ----------------------------------------------------------------------- */

L_glcset:
    glcqid_(cp, &idx, cp_len);
    glcsvl_(&idx, cpara, cpara_len);
    return 0;
/* ----------------------------------------------------------------------- */

L_glcstx:
    s_copy(cpval, cpara, (ftnlen)80, cpara_len);
    glcqid_(cx, &idx, (ftnlen)40);
/*     / SHORT NAME / */
    glcqcp_(&idx, cx, (ftnlen)40);
    rtcget_("GL", cx, cpval, &c__1, (ftnlen)2, (ftnlen)40, (ftnlen)80);
/*     / LONG NAME / */
    glcqcl_(&idx, cx, (ftnlen)40);
    rlcget_(cx, cpval, &c__1, (ftnlen)40, (ftnlen)80);
    glcsvl_(&idx, cpval, (ftnlen)80);
    return 0;
} /* glcget_ */

/* Subroutine */ int glcget_(char *cp, char *cpara, ftnlen cp_len, ftnlen 
	cpara_len)
{
    return glcget_0_(0, cp, cpara, cp_len, cpara_len);
    }

/* Subroutine */ int glcset_(char *cp, char *cpara, ftnlen cp_len, ftnlen 
	cpara_len)
{
    return glcget_0_(1, cp, cpara, cp_len, cpara_len);
    }

/* Subroutine */ int glcstx_(char *cp, char *cpara, ftnlen cp_len, ftnlen 
	cpara_len)
{
    return glcget_0_(2, cp, cpara, cp_len, cpara_len);
    }


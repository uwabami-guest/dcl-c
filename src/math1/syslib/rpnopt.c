/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__4 = 4;
static integer c__2 = 2;

/* ----------------------------------------------------------------------- */
/*     GET EXTERNAL PARAMETER FROM COMMAND LINE */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int rpnopt_0_(int n__, char *cpfix, char *cp, integer *ipara,
	 logical *lpara, real *rpara, char *cpara, ftnlen cpfix_len, ftnlen 
	cp_len, ftnlen cpara_len)
{
    /* Initialized data */

    static char csepx[1] = "_";
    static char coptx[1] = "-";
    static char cdivx[1] = "=";

    /* System generated locals */
    address a__1[4], a__2[2];
    integer i__1[4], i__2, i__3, i__4[2];

    /* Builtin functions */
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen);
    integer i_indx(char *, char *, ftnlen, ftnlen);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer n, np, max__;
    static char cpx[80], carg[80];
    extern integer lenc_(char *, ftnlen);
    static char cdiv[1], csep[1], copt[1];
    extern logical lchreq_(char *, char *, ftnlen, ftnlen);
    extern integer ifromc_(char *, ftnlen);
    extern logical lfromc_(char *, ftnlen);
    extern /* Subroutine */ int osgarg_(integer *, char *, ftnlen);
    extern real rfromc_(char *, ftnlen);
    extern /* Subroutine */ int osgenv_(char *, char *, ftnlen, ftnlen), 
	    osqarn_(integer *);

    switch(n__) {
	case 1: goto L_rtiopt;
	case 2: goto L_rtlopt;
	case 3: goto L_rtropt;
	case 4: goto L_rtcopt;
	case 5: goto L_rliopt;
	case 6: goto L_rllopt;
	case 7: goto L_rlropt;
	case 8: goto L_rlcopt;
	}

    osgenv_("DCLENVCHAR", csep, (ftnlen)10, (ftnlen)1);
    if (*(unsigned char *)csep == ' ') {
	*(unsigned char *)csep = *(unsigned char *)&csepx[0];
    }
    osgenv_("DCLOPTCHAR", copt, (ftnlen)10, (ftnlen)1);
    if (*(unsigned char *)copt == ' ') {
	*(unsigned char *)copt = *(unsigned char *)&coptx[0];
    }
    osgenv_("DCLSETCHAR", cdiv, (ftnlen)10, (ftnlen)1);
    if (*(unsigned char *)cdiv == ' ') {
	*(unsigned char *)cdiv = *(unsigned char *)&cdivx[0];
    }
    return 0;
/* ----------------------------------------------------------------------- */
/*     GET INTEGER PARAMETER WITH SHORT NAME */
/* ----------------------------------------------------------------------- */

L_rtiopt:
/* Writing concatenation */
    i__1[0] = 1, a__1[0] = copt;
    i__1[1] = lenc_(cpfix, cpfix_len), a__1[1] = cpfix;
    i__1[2] = 1, a__1[2] = csep;
    i__1[3] = cp_len, a__1[3] = cp;
    s_cat(cpx, a__1, i__1, &c__4, (ftnlen)80);
    osqarn_(&max__);
    i__2 = max__;
    for (n = 1; n <= i__2; ++n) {
	osgarg_(&n, carg, (ftnlen)80);
	np = i_indx(carg, cdiv, (ftnlen)80, (ftnlen)1);
	if (np != 0) {
	    if (lchreq_(carg, cpx, np - 1, (ftnlen)80)) {
		i__3 = np;
		*ipara = ifromc_(carg + i__3, lenc_(carg, (ftnlen)80) - i__3);
		return 0;
	    }
	}
/* L10: */
    }
    return 0;
/* ----------------------------------------------------------------------- */
/*     GET LOGICAL PARAMETER WITH SHORT NAME */
/* ----------------------------------------------------------------------- */

L_rtlopt:
/* Writing concatenation */
    i__1[0] = 1, a__1[0] = copt;
    i__1[1] = lenc_(cpfix, cpfix_len), a__1[1] = cpfix;
    i__1[2] = 1, a__1[2] = csep;
    i__1[3] = cp_len, a__1[3] = cp;
    s_cat(cpx, a__1, i__1, &c__4, (ftnlen)80);
    osqarn_(&max__);
    i__2 = max__;
    for (n = 1; n <= i__2; ++n) {
	osgarg_(&n, carg, (ftnlen)80);
	np = i_indx(carg, cdiv, (ftnlen)80, (ftnlen)1);
	if (np != 0) {
	    if (lchreq_(carg, cpx, np - 1, (ftnlen)80)) {
		i__3 = np;
		*lpara = lfromc_(carg + i__3, lenc_(carg, (ftnlen)80) - i__3);
		return 0;
	    }
	}
/* L20: */
    }
    return 0;
/* ----------------------------------------------------------------------- */
/*     GET REAL PARAMETER WITH SHORT NAME */
/* ----------------------------------------------------------------------- */

L_rtropt:
/* Writing concatenation */
    i__1[0] = 1, a__1[0] = copt;
    i__1[1] = lenc_(cpfix, cpfix_len), a__1[1] = cpfix;
    i__1[2] = 1, a__1[2] = csep;
    i__1[3] = cp_len, a__1[3] = cp;
    s_cat(cpx, a__1, i__1, &c__4, (ftnlen)80);
    osqarn_(&max__);
    i__2 = max__;
    for (n = 1; n <= i__2; ++n) {
	osgarg_(&n, carg, (ftnlen)80);
	np = i_indx(carg, cdiv, (ftnlen)80, (ftnlen)1);
	if (np != 0) {
	    if (lchreq_(carg, cpx, np - 1, (ftnlen)80)) {
		i__3 = np;
		*rpara = rfromc_(carg + i__3, lenc_(carg, (ftnlen)80) - i__3);
		return 0;
	    }
	}
/* L30: */
    }
    return 0;
/* ----------------------------------------------------------------------- */
/*     GET CHARACTER PARAMETER WITH SHORT NAME */
/* ----------------------------------------------------------------------- */

L_rtcopt:
/* Writing concatenation */
    i__1[0] = 1, a__1[0] = copt;
    i__1[1] = lenc_(cpfix, cpfix_len), a__1[1] = cpfix;
    i__1[2] = 1, a__1[2] = csep;
    i__1[3] = cp_len, a__1[3] = cp;
    s_cat(cpx, a__1, i__1, &c__4, (ftnlen)80);
    osqarn_(&max__);
    i__2 = max__;
    for (n = 1; n <= i__2; ++n) {
	osgarg_(&n, carg, (ftnlen)80);
	np = i_indx(carg, cdiv, (ftnlen)80, (ftnlen)1);
	if (np != 0) {
	    if (lchreq_(carg, cpx, np - 1, (ftnlen)80)) {
		i__3 = np;
		s_copy(cpara, carg + i__3, cpara_len, lenc_(carg, (ftnlen)80) 
			- i__3);
		return 0;
	    }
	}
/* L40: */
    }
    return 0;
/* ----------------------------------------------------------------------- */
/*     GET INTEGER PARAMETER WITH LONG NAME */
/* ----------------------------------------------------------------------- */

L_rliopt:
/* Writing concatenation */
    i__4[0] = 1, a__2[0] = copt;
    i__4[1] = cp_len, a__2[1] = cp;
    s_cat(cpx, a__2, i__4, &c__2, (ftnlen)80);
    osqarn_(&max__);
    i__2 = max__;
    for (n = 1; n <= i__2; ++n) {
	osgarg_(&n, carg, (ftnlen)80);
	np = i_indx(carg, cdiv, (ftnlen)80, (ftnlen)1);
	if (np != 0) {
	    if (lchreq_(carg, cpx, np - 1, (ftnlen)80)) {
		i__3 = np;
		*ipara = ifromc_(carg + i__3, lenc_(carg, (ftnlen)80) - i__3);
		return 0;
	    }
	}
/* L50: */
    }
    return 0;
/* ----------------------------------------------------------------------- */
/*     GET LOGICAL PARAMETER WITH LONG NAME */
/* ----------------------------------------------------------------------- */

L_rllopt:
/* Writing concatenation */
    i__4[0] = 1, a__2[0] = copt;
    i__4[1] = cp_len, a__2[1] = cp;
    s_cat(cpx, a__2, i__4, &c__2, (ftnlen)80);
    osqarn_(&max__);
    i__2 = max__;
    for (n = 1; n <= i__2; ++n) {
	osgarg_(&n, carg, (ftnlen)80);
	np = i_indx(carg, cdiv, (ftnlen)80, (ftnlen)1);
	if (np != 0) {
	    if (lchreq_(carg, cpx, np - 1, (ftnlen)80)) {
		i__3 = np;
		*lpara = lfromc_(carg + i__3, lenc_(carg, (ftnlen)80) - i__3);
		return 0;
	    }
	}
/* L60: */
    }
    return 0;
/* ----------------------------------------------------------------------- */
/*     GET REAL PARAMETER WITH LONG NAME */
/* ----------------------------------------------------------------------- */

L_rlropt:
/* Writing concatenation */
    i__4[0] = 1, a__2[0] = copt;
    i__4[1] = cp_len, a__2[1] = cp;
    s_cat(cpx, a__2, i__4, &c__2, (ftnlen)80);
    osqarn_(&max__);
    i__2 = max__;
    for (n = 1; n <= i__2; ++n) {
	osgarg_(&n, carg, (ftnlen)80);
	np = i_indx(carg, cdiv, (ftnlen)80, (ftnlen)1);
	if (np != 0) {
	    if (lchreq_(carg, cpx, np - 1, (ftnlen)80)) {
		i__3 = np;
		*rpara = rfromc_(carg + i__3, lenc_(carg, (ftnlen)80) - i__3);
		return 0;
	    }
	}
/* L70: */
    }
    return 0;
/* ----------------------------------------------------------------------- */
/*     GET CHARACTER PARAMETER WITH LONG NAME */
/* ----------------------------------------------------------------------- */

L_rlcopt:
/* Writing concatenation */
    i__4[0] = 1, a__2[0] = copt;
    i__4[1] = cp_len, a__2[1] = cp;
    s_cat(cpx, a__2, i__4, &c__2, (ftnlen)80);
    osqarn_(&max__);
    i__2 = max__;
    for (n = 1; n <= i__2; ++n) {
	osgarg_(&n, carg, (ftnlen)80);
	np = i_indx(carg, cdiv, (ftnlen)80, (ftnlen)1);
	if (np != 0) {
	    if (lchreq_(carg, cpx, np - 1, (ftnlen)80)) {
		i__3 = np;
		s_copy(cpara, carg + i__3, cpara_len, lenc_(carg, (ftnlen)80) 
			- i__3);
		return 0;
	    }
	}
/* L80: */
    }
    return 0;
} /* rpnopt_ */

/* Subroutine */ int rpnopt_(void)
{
    return rpnopt_0_(0, (char *)0, (char *)0, (integer *)0, (logical *)0, (
	    real *)0, (char *)0, (ftnint)0, (ftnint)0, (ftnint)0);
    }

/* Subroutine */ int rtiopt_(char *cpfix, char *cp, integer *ipara, ftnlen 
	cpfix_len, ftnlen cp_len)
{
    return rpnopt_0_(1, cpfix, cp, ipara, (logical *)0, (real *)0, (char *)0, 
	    cpfix_len, cp_len, (ftnint)0);
    }

/* Subroutine */ int rtlopt_(char *cpfix, char *cp, logical *lpara, ftnlen 
	cpfix_len, ftnlen cp_len)
{
    return rpnopt_0_(2, cpfix, cp, (integer *)0, lpara, (real *)0, (char *)0, 
	    cpfix_len, cp_len, (ftnint)0);
    }

/* Subroutine */ int rtropt_(char *cpfix, char *cp, real *rpara, ftnlen 
	cpfix_len, ftnlen cp_len)
{
    return rpnopt_0_(3, cpfix, cp, (integer *)0, (logical *)0, rpara, (char *)
	    0, cpfix_len, cp_len, (ftnint)0);
    }

/* Subroutine */ int rtcopt_(char *cpfix, char *cp, char *cpara, ftnlen 
	cpfix_len, ftnlen cp_len, ftnlen cpara_len)
{
    return rpnopt_0_(4, cpfix, cp, (integer *)0, (logical *)0, (real *)0, 
	    cpara, cpfix_len, cp_len, cpara_len);
    }

/* Subroutine */ int rliopt_(char *cp, integer *ipara, ftnlen cp_len)
{
    return rpnopt_0_(5, (char *)0, cp, ipara, (logical *)0, (real *)0, (char *
	    )0, (ftnint)0, cp_len, (ftnint)0);
    }

/* Subroutine */ int rllopt_(char *cp, logical *lpara, ftnlen cp_len)
{
    return rpnopt_0_(6, (char *)0, cp, (integer *)0, lpara, (real *)0, (char *
	    )0, (ftnint)0, cp_len, (ftnint)0);
    }

/* Subroutine */ int rlropt_(char *cp, real *rpara, ftnlen cp_len)
{
    return rpnopt_0_(7, (char *)0, cp, (integer *)0, (logical *)0, rpara, (
	    char *)0, (ftnint)0, cp_len, (ftnint)0);
    }

/* Subroutine */ int rlcopt_(char *cp, char *cpara, ftnlen cp_len, ftnlen 
	cpara_len)
{
    return rpnopt_0_(8, (char *)0, cp, (integer *)0, (logical *)0, (real *)0, 
	    cpara, (ftnint)0, cp_len, cpara_len);
    }


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     GET EXTERNAL PARAMETERS */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int rpnget_0_(int n__, char *cpfix, char *cps, integer *
	ipara, integer *max__, logical *lpara, real *rpara, char *cpara, char 
	*cpl, ftnlen cpfix_len, ftnlen cps_len, ftnlen cpara_len, ftnlen 
	cpl_len)
{
    /* Initialized data */

    static logical lfirst = TRUE_;

    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    integer s_cmp(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer n;
    static logical lglc;
    extern integer lenc_(char *, ftnlen);
    extern logical lchreq_(char *, char *, ftnlen, ftnlen);
    extern /* Subroutine */ int rlcxfl_(char *, char *, ftnlen, ftnlen), 
	    rlcenv_(char *, char *, ftnlen, ftnlen), rlixfl_(char *, integer *
	    , ftnlen), rtcxfl_(char *, char *, char *, ftnlen, ftnlen, ftnlen)
	    , rtcenv_(char *, char *, char *, ftnlen, ftnlen, ftnlen), 
	    rlienv_(char *, integer *, ftnlen), rllxfl_(char *, logical *, 
	    ftnlen), rtienv_(char *, char *, integer *, ftnlen, ftnlen), 
	    rpnenv_(void), rpnxfl_(void), rtixfl_(char *, char *, integer *, 
	    ftnlen, ftnlen), rtlxfl_(char *, char *, logical *, ftnlen, 
	    ftnlen), rtlenv_(char *, char *, logical *, ftnlen, ftnlen), 
	    rtcopt_(char *, char *, char *, ftnlen, ftnlen, ftnlen), rliopt_(
	    char *, integer *, ftnlen), rtrenv_(char *, char *, real *, 
	    ftnlen, ftnlen), rtiopt_(char *, char *, integer *, ftnlen, 
	    ftnlen), rpnopt_(void), rtrxfl_(char *, char *, real *, ftnlen, 
	    ftnlen), rtlopt_(char *, char *, logical *, ftnlen, ftnlen), 
	    rllenv_(char *, logical *, ftnlen), rllopt_(char *, logical *, 
	    ftnlen), rlrxfl_(char *, real *, ftnlen), rlrenv_(char *, real *, 
	    ftnlen), rtropt_(char *, char *, real *, ftnlen, ftnlen), rlropt_(
	    char *, real *, ftnlen), rlcopt_(char *, char *, ftnlen, ftnlen);

    /* Parameter adjustments */
    if (cps) {
	cps -= 8;
	}
    if (ipara) {
	--ipara;
	}
    if (lpara) {
	--lpara;
	}
    if (rpara) {
	--rpara;
	}
    if (cpara) {
	cpara -= 80;
	}
    if (cpl) {
	cpl -= 40;
	}

    /* Function Body */
    switch(n__) {
	case 1: goto L_rtiget;
	case 2: goto L_rtlget;
	case 3: goto L_rtrget;
	case 4: goto L_rtcget;
	case 5: goto L_rliget;
	case 6: goto L_rllget;
	case 7: goto L_rlrget;
	case 8: goto L_rlcget;
	}

    return 0;
/* ----------------------------------------------------------------------- */
/*     GET INTEGER PARAMETERS WITH SHORT NAME */
/* ----------------------------------------------------------------------- */

L_rtiget:
    if (lfirst) {
	rpnxfl_();
	rpnenv_();
	rpnopt_();
	lfirst = FALSE_;
    }
    i__1 = *max__;
    for (n = 1; n <= i__1; ++n) {
	rtixfl_(cpfix, cps + (n << 3), &ipara[n], cpfix_len, (ftnlen)8);
	rtienv_(cpfix, cps + (n << 3), &ipara[n], cpfix_len, (ftnlen)8);
	rtiopt_(cpfix, cps + (n << 3), &ipara[n], cpfix_len, (ftnlen)8);
/* L10: */
    }
    return 0;
/* ----------------------------------------------------------------------- */
/*     GET LOGICAL PARAMETERS WITH SHORT NAME */
/* ----------------------------------------------------------------------- */

L_rtlget:
    if (lfirst) {
	rpnxfl_();
	rpnenv_();
	rpnopt_();
	lfirst = FALSE_;
    }
    i__1 = *max__;
    for (n = 1; n <= i__1; ++n) {
	rtlxfl_(cpfix, cps + (n << 3), &lpara[n], cpfix_len, (ftnlen)8);
	rtlenv_(cpfix, cps + (n << 3), &lpara[n], cpfix_len, (ftnlen)8);
	rtlopt_(cpfix, cps + (n << 3), &lpara[n], cpfix_len, (ftnlen)8);
/* L20: */
    }
    return 0;
/* ----------------------------------------------------------------------- */
/*     GET REAL PARAMETERS WITH SHORT NAME */
/* ----------------------------------------------------------------------- */

L_rtrget:
    if (lfirst) {
	rpnxfl_();
	rpnenv_();
	rpnopt_();
	lfirst = FALSE_;
    }
    i__1 = *max__;
    for (n = 1; n <= i__1; ++n) {
	rtrxfl_(cpfix, cps + (n << 3), &rpara[n], cpfix_len, (ftnlen)8);
	rtrenv_(cpfix, cps + (n << 3), &rpara[n], cpfix_len, (ftnlen)8);
	rtropt_(cpfix, cps + (n << 3), &rpara[n], cpfix_len, (ftnlen)8);
/* L30: */
    }
    return 0;
/* ----------------------------------------------------------------------- */
/*     GET CHARACTER PARAMETERS WITH SHORT NAME */
/* ----------------------------------------------------------------------- */

L_rtcget:
    lglc = s_cmp(cpfix, "GL", cpfix_len, (ftnlen)2) == 0;
    if (lfirst) {
	if (! lglc) {
	    rpnxfl_();
	    lfirst = FALSE_;
	}
	rpnenv_();
	rpnopt_();
    }
    i__1 = *max__;
    for (n = 1; n <= i__1; ++n) {
	if (! lglc) {
	    rtcxfl_(cpfix, cps + (n << 3), cpara + n * 80, cpfix_len, (ftnlen)
		    8, (ftnlen)80);
	}
	rtcenv_(cpfix, cps + (n << 3), cpara + n * 80, cpfix_len, (ftnlen)8, (
		ftnlen)80);
	rtcopt_(cpfix, cps + (n << 3), cpara + n * 80, cpfix_len, (ftnlen)8, (
		ftnlen)80);
/* L40: */
    }
    return 0;
/* ----------------------------------------------------------------------- */
/*     GET INTEGER PARAMETERS WITH LONG NAME */
/* ----------------------------------------------------------------------- */

L_rliget:
    if (lfirst) {
	rpnxfl_();
	rpnenv_();
	rpnopt_();
	lfirst = FALSE_;
    }
    i__1 = *max__;
    for (n = 1; n <= i__1; ++n) {
	rlixfl_(cpl + n * 40, &ipara[n], (ftnlen)40);
	rlienv_(cpl + n * 40, &ipara[n], (ftnlen)40);
	rliopt_(cpl + n * 40, &ipara[n], (ftnlen)40);
/* L50: */
    }
    return 0;
/* ----------------------------------------------------------------------- */
/*     GET LOGICAL PARAMETERS WITH LONG NAME */
/* ----------------------------------------------------------------------- */

L_rllget:
    if (lfirst) {
	rpnxfl_();
	rpnenv_();
	rpnopt_();
	lfirst = FALSE_;
    }
    i__1 = *max__;
    for (n = 1; n <= i__1; ++n) {
	rllxfl_(cpl + n * 40, &lpara[n], (ftnlen)40);
	rllenv_(cpl + n * 40, &lpara[n], (ftnlen)40);
	rllopt_(cpl + n * 40, &lpara[n], (ftnlen)40);
/* L60: */
    }
    return 0;
/* ----------------------------------------------------------------------- */
/*     GET REAL PARAMETERS WITH LONG NAME */
/* ----------------------------------------------------------------------- */

L_rlrget:
    if (lfirst) {
	rpnxfl_();
	rpnenv_();
	rpnopt_();
	lfirst = FALSE_;
    }
    i__1 = *max__;
    for (n = 1; n <= i__1; ++n) {
	rlrxfl_(cpl + n * 40, &rpara[n], (ftnlen)40);
	rlrenv_(cpl + n * 40, &rpara[n], (ftnlen)40);
	rlropt_(cpl + n * 40, &rpara[n], (ftnlen)40);
/* L70: */
    }
    return 0;
/* ----------------------------------------------------------------------- */
/*     GET CHARACTER PARAMETERS WITH LONG NAME */
/* ----------------------------------------------------------------------- */

L_rlcget:
    lglc = lchreq_(cpl + 40, "CONFIG_FILE", lenc_(cpl + 40, (ftnlen)40), (
	    ftnlen)11);
    if (lfirst) {
	if (! lglc) {
	    rpnxfl_();
	    lfirst = FALSE_;
	}
	rpnenv_();
	rpnopt_();
    }
    i__1 = *max__;
    for (n = 1; n <= i__1; ++n) {
	if (! lglc) {
	    rlcxfl_(cpl + n * 40, cpara + n * 80, (ftnlen)40, (ftnlen)80);
	}
	rlcenv_(cpl + n * 40, cpara + n * 80, (ftnlen)40, (ftnlen)80);
	rlcopt_(cpl + n * 40, cpara + n * 80, (ftnlen)40, (ftnlen)80);
/* L80: */
    }
    return 0;
} /* rpnget_ */

/* Subroutine */ int rpnget_(void)
{
    return rpnget_0_(0, (char *)0, (char *)0, (integer *)0, (integer *)0, (
	    logical *)0, (real *)0, (char *)0, (char *)0, (ftnint)0, (ftnint)
	    0, (ftnint)0, (ftnint)0);
    }

/* Subroutine */ int rtiget_(char *cpfix, char *cps, integer *ipara, integer *
	max__, ftnlen cpfix_len, ftnlen cps_len)
{
    return rpnget_0_(1, cpfix, cps, ipara, max__, (logical *)0, (real *)0, (
	    char *)0, (char *)0, cpfix_len, cps_len, (ftnint)0, (ftnint)0);
    }

/* Subroutine */ int rtlget_(char *cpfix, char *cps, logical *lpara, integer *
	max__, ftnlen cpfix_len, ftnlen cps_len)
{
    return rpnget_0_(2, cpfix, cps, (integer *)0, max__, lpara, (real *)0, (
	    char *)0, (char *)0, cpfix_len, cps_len, (ftnint)0, (ftnint)0);
    }

/* Subroutine */ int rtrget_(char *cpfix, char *cps, real *rpara, integer *
	max__, ftnlen cpfix_len, ftnlen cps_len)
{
    return rpnget_0_(3, cpfix, cps, (integer *)0, max__, (logical *)0, rpara, 
	    (char *)0, (char *)0, cpfix_len, cps_len, (ftnint)0, (ftnint)0);
    }

/* Subroutine */ int rtcget_(char *cpfix, char *cps, char *cpara, integer *
	max__, ftnlen cpfix_len, ftnlen cps_len, ftnlen cpara_len)
{
    return rpnget_0_(4, cpfix, cps, (integer *)0, max__, (logical *)0, (real *
	    )0, cpara, (char *)0, cpfix_len, cps_len, cpara_len, (ftnint)0);
    }

/* Subroutine */ int rliget_(char *cpl, integer *ipara, integer *max__, 
	ftnlen cpl_len)
{
    return rpnget_0_(5, (char *)0, (char *)0, ipara, max__, (logical *)0, (
	    real *)0, (char *)0, cpl, (ftnint)0, (ftnint)0, (ftnint)0, 
	    cpl_len);
    }

/* Subroutine */ int rllget_(char *cpl, logical *lpara, integer *max__, 
	ftnlen cpl_len)
{
    return rpnget_0_(6, (char *)0, (char *)0, (integer *)0, max__, lpara, (
	    real *)0, (char *)0, cpl, (ftnint)0, (ftnint)0, (ftnint)0, 
	    cpl_len);
    }

/* Subroutine */ int rlrget_(char *cpl, real *rpara, integer *max__, ftnlen 
	cpl_len)
{
    return rpnget_0_(7, (char *)0, (char *)0, (integer *)0, max__, (logical *)
	    0, rpara, (char *)0, cpl, (ftnint)0, (ftnint)0, (ftnint)0, 
	    cpl_len);
    }

/* Subroutine */ int rlcget_(char *cpl, char *cpara, integer *max__, ftnlen 
	cpl_len, ftnlen cpara_len)
{
    return rpnget_0_(8, (char *)0, (char *)0, (integer *)0, max__, (logical *)
	    0, (real *)0, cpara, cpl, (ftnint)0, (ftnint)0, cpara_len, 
	    cpl_len);
    }


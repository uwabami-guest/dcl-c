/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int mszdmp_(char *cmsg, integer *iunit, integer *lnsize, 
	ftnlen cmsg_len)
{
    /* System generated locals */
    integer i__1, i__2, i__3;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_wsfi(icilist *), do_fio(integer *, char *, ftnlen), e_wsfi(void)
	    , s_wsfe(cilist *), e_wsfe(void);

    /* Local variables */
    static integer m;
    static char cfm[12];
    extern integer lenc_(char *, ftnlen);
    static integer mmsg, nmsg;
    static char cmsgx[200];

    /* Fortran I/O blocks */
    static icilist io___5 = { 0, cfm+6, 0, "(I3)", 3, 1 };
    static cilist io___7 = { 0, 0, 0, cfm, 0 };


    s_copy(cmsgx, cmsg, (ftnlen)200, cmsg_len);
    nmsg = lenc_(cmsgx, (ftnlen)200);
    mmsg = (nmsg - 1) / *lnsize + 1;
    s_copy(cfm, "(TR1,A###)", (ftnlen)12, (ftnlen)10);
    s_wsfi(&io___5);
    do_fio(&c__1, (char *)&(*lnsize), (ftnlen)sizeof(integer));
    e_wsfi();
    i__1 = mmsg;
    for (m = 1; m <= i__1; ++m) {
	io___7.ciunit = *iunit;
	s_wsfe(&io___7);
	i__2 = *lnsize * (m - 1);
/* Computing MIN */
	i__3 = *lnsize * m;
	do_fio(&c__1, cmsgx + i__2, min(i__3,200) - i__2);
	e_wsfe();
/* L10: */
    }
    return 0;
} /* mszdmp_ */


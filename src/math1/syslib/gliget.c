/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     GLIGET / GLISET / GLISTX */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int gliget_0_(int n__, char *cp, integer *ipara, ftnlen 
	cp_len)
{
    static integer ip;
    static char cx[40];
    static integer idx;
    extern /* Subroutine */ int gliqid_(char *, integer *, ftnlen), gliqcl_(
	    integer *, char *, ftnlen), gliqcp_(integer *, char *, ftnlen), 
	    rliget_(char *, integer *, integer *, ftnlen), gliqvl_(integer *, 
	    integer *), rtiget_(char *, char *, integer *, integer *, ftnlen, 
	    ftnlen), glisvl_(integer *, integer *);

    switch(n__) {
	case 1: goto L_gliset;
	case 2: goto L_glistx;
	}

    gliqid_(cp, &idx, cp_len);
    gliqvl_(&idx, ipara);
    return 0;
/* ----------------------------------------------------------------------- */

L_gliset:
    gliqid_(cp, &idx, cp_len);
    glisvl_(&idx, ipara);
    return 0;
/* ----------------------------------------------------------------------- */

L_glistx:
    ip = *ipara;
    gliqid_(cp, &idx, cp_len);
/*     / SHORT NAME / */
    gliqcp_(&idx, cx, (ftnlen)40);
    rtiget_("GL", cx, &ip, &c__1, (ftnlen)2, (ftnlen)40);
/*     / LONG NAME / */
    gliqcl_(&idx, cx, (ftnlen)40);
    rliget_(cx, &ip, &c__1, (ftnlen)40);
    glisvl_(&idx, &ip);
    return 0;
} /* gliget_ */

/* Subroutine */ int gliget_(char *cp, integer *ipara, ftnlen cp_len)
{
    return gliget_0_(0, cp, ipara, cp_len);
    }

/* Subroutine */ int gliset_(char *cp, integer *ipara, ftnlen cp_len)
{
    return gliget_0_(1, cp, ipara, cp_len);
    }

/* Subroutine */ int glistx_(char *cp, integer *ipara, ftnlen cp_len)
{
    return gliget_0_(2, cp, ipara, cp_len);
    }


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     RCOV */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
real rcov_(real *rx, real *ry, integer *n, integer *jx, integer *jy)
{
    /* System generated locals */
    integer i__1;
    real ret_val;

    /* Local variables */
    static integer i__, kx, ky;
    static real sum, xave, yave;
    extern real rave0_(real *, integer *, integer *);

    /* Parameter adjustments */
    --ry;
    --rx;

    /* Function Body */
    xave = rave0_(&rx[1], n, jx);
    yave = rave0_(&ry[1], n, jy);
    sum = 0.f;
    kx = 1 - *jx;
    ky = 1 - *jy;
    i__1 = *n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	kx += *jx;
	ky += *jy;
	sum += (rx[kx] - xave) * (ry[ky] - yave);
/* L10: */
    }
    ret_val = sum / *n;
    return ret_val;
} /* rcov_ */


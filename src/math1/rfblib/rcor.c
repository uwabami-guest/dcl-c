/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     RCOR */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
real rcor_(real *rx, real *ry, integer *n, integer *jx, integer *jy)
{
    /* System generated locals */
    real ret_val;

    /* Builtin functions */
    double sqrt(doublereal);

    /* Local variables */
    extern real rcov_(real *, real *, integer *, integer *, integer *), 
	    rvar0_(real *, integer *, integer *);
    static real rmiss, rvarx, rvary;
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen), msgdmp_(char 
	    *, char *, char *, ftnlen, ftnlen, ftnlen);

    /* Parameter adjustments */
    --ry;
    --rx;

    /* Function Body */
    rvarx = rvar0_(&rx[1], n, jx);
    rvary = rvar0_(&ry[1], n, jy);
    if (rvarx == 0.f || rvary == 0.f) {
	glrget_("RMISS", &rmiss, (ftnlen)5);
	ret_val = rmiss;
	msgdmp_("W", "RCOR  ", "VARIANCE OF RX OR RY IS EQUAL TO 0 / MISSING"
		" VALUE IS SUBSTITUTED.", (ftnlen)1, (ftnlen)6, (ftnlen)66);
    } else {
	ret_val = rcov_(&rx[1], &ry[1], n, jx, jy) / sqrt(rvarx * rvary);
    }
    return ret_val;
} /* rcor_ */


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;
static real c_b7 = 10.f;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
real rfromc_(char *cx, ftnlen cx_len)
{
    /* System generated locals */
    integer i__1, i__2;
    real ret_val;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    double pow_ri(real *, integer *);

    /* Local variables */
    static integer lc2;
    static char cmsg[80];
    static integer iexp;
    extern integer lenz_(char *, ftnlen);
    extern real ffromc_(char *, ftnlen);
    extern integer ifromc_(char *, ftnlen), indxmf_(char *, integer *, 
	    integer *, char *, ftnlen, ftnlen);
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen);

/*     EXTERNAL  LENY,LENZ,INDXMF,IFROMC,FFROMC */
/*     LC1=LENY(CX)+1 */
    lc2 = lenz_(cx, cx_len);
    if (lc2 == 0) {
	s_copy(cmsg, "THERE IS NO VALID CHARACTER.", (ftnlen)80, (ftnlen)28);
	msgdmp_("E", "FFROMC", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
    }
    iexp = indxmf_(cx, &lc2, &c__1, "E", cx_len, (ftnlen)1);
    if (iexp == 0) {
	ret_val = ffromc_(cx, cx_len);
    } else {
	i__1 = iexp;
	i__2 = ifromc_(cx + i__1, lc2 - i__1);
	ret_val = ffromc_(cx, iexp - 1) * pow_ri(&c_b7, &i__2);
    }
    return ret_val;
} /* rfromc_ */


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__2 = 2;
static real c_b19 = 10.f;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
real ffromc_(char *cx, ftnlen cx_len)
{
    /* System generated locals */
    address a__1[2];
    integer i__1, i__2[2];
    real ret_val;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer i_indx(char *, char *, ftnlen, ftnlen);
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen);
    double pow_ri(real *, integer *);

    /* Local variables */
    static integer lc1, lc2, inx;
    static char cxx[16], cmsg[80];
    static integer idot, isgn;
    extern integer leny_(char *, ftnlen), lenz_(char *, ftnlen);
    static integer lcxx;
    extern /* Subroutine */ int cladj_(char *, ftnlen);
    extern integer jfromc_(char *, ftnlen);
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen);

    lc1 = leny_(cx, cx_len) + 1;
    lc2 = lenz_(cx, cx_len);
    if (lc2 == 0) {
	s_copy(cmsg, "THERE IS NO VALID CHARACTER.", (ftnlen)80, (ftnlen)28);
	msgdmp_("E", "FFROMC", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
    }
    if (*(unsigned char *)&cx[lc1 - 1] == '-') {
	isgn = -1;
	++lc1;
    } else if (*(unsigned char *)&cx[lc1 - 1] == '+') {
	isgn = 1;
	++lc1;
    } else {
	isgn = 1;
    }
    idot = i_indx(cx, ".", lc2, (ftnlen)1);
    if (idot == 0) {
	s_copy(cxx, cx + (lc1 - 1), (ftnlen)16, lc2 - (lc1 - 1));
	inx = 0;
    } else {
	if (idot == lc1 && idot == lc2) {
	    s_copy(cmsg, "THERE EXISTS A PERIOD ONLY.", (ftnlen)80, (ftnlen)
		    27);
	    msgdmp_("E", "FFROMC", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
	} else if (idot == lc1) {
	    i__1 = idot;
	    s_copy(cxx, cx + i__1, (ftnlen)16, lc2 - i__1);
	} else if (idot == lc2) {
	    s_copy(cxx, cx + (lc1 - 1), (ftnlen)16, idot - 1 - (lc1 - 1));
	} else {
	    i__1 = idot;
/* Writing concatenation */
	    i__2[0] = idot - 1 - (lc1 - 1), a__1[0] = cx + (lc1 - 1);
	    i__2[1] = lc2 - i__1, a__1[1] = cx + i__1;
	    s_cat(cxx, a__1, i__2, &c__2, (ftnlen)16);
	}
	inx = idot - lc2;
    }
    lcxx = lenz_(cxx, (ftnlen)16);
L10:
    if (*(unsigned char *)cxx != '0' || lcxx == 1) {
	goto L20;
    }
    *(unsigned char *)cxx = ' ';
    cladj_(cxx, (ftnlen)16);
    --lcxx;
    goto L10;
L20:
    if (lcxx > 8) {
	s_copy(cmsg, "GIVEN NUMBER IS TRUNCATED WITHIN 8 DIGITS.", (ftnlen)80,
		 (ftnlen)42);
	msgdmp_("W", "FFROMC", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
	inx += lcxx - 8;
	lcxx = 8;
    }
    ret_val = isgn * (real) jfromc_(cxx, lcxx) * pow_ri(&c_b19, &inx);
    return ret_val;
} /* ffromc_ */


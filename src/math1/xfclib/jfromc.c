/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
integer jfromc_(char *cx, ftnlen cx_len)
{
    /* Initialized data */

    static char cnum[10] = "0123456789";

    /* System generated locals */
    integer ret_val;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer i_indx(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer n, idx, lcx, idig;
    static char cmsg[80];
    extern integer lenz_(char *, ftnlen);
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen);

    lcx = lenz_(cx, cx_len);
    if (lcx == 0) {
	s_copy(cmsg, "THERE IS NO VALID CHARACTER.", (ftnlen)80, (ftnlen)28);
	msgdmp_("E", "JFROMC", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
    }
    ret_val = 0;
    idig = 1;
    for (n = lcx; n >= 1; --n) {
	idx = i_indx(cnum, cx + (n - 1), (ftnlen)10, (ftnlen)1);
	if (idx == 0) {
	    s_copy(cmsg, "THERE IS A NON-NUMERAL CHARACTER.", (ftnlen)80, (
		    ftnlen)33);
	    msgdmp_("E", "JFROMC", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
	}
	ret_val += (idx - 1) * idig;
	idig *= 10;
/* L10: */
    }
    return ret_val;
} /* jfromc_ */


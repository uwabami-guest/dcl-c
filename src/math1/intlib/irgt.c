/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     IRGT */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
integer irgt_(real *rx)
{
    /* System generated locals */
    integer ret_val;
    real r__1;

    /* Builtin functions */
    integer i_nint(real *);

    /* Local variables */
    static integer nx;
    extern logical lreq_(real *, real *);

    nx = i_nint(rx);
    r__1 = (real) nx;
    if (lreq_(rx, &r__1)) {
	ret_val = nx + 1;
    } else {
	ret_val = (integer) (*rx) + (integer) (*rx - (integer) (*rx) + 1);
    }
    return ret_val;
} /* irgt_ */


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     COORDINATE ROTATION (3D CARTESIAN) */
/*                                              93/02/19   S.SAKAI */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int cr3c_(real *theta, real *phi, real *psi, real *x0, real *
	y0, real *z0, real *x1, real *y1, real *z1)
{
    /* System generated locals */
    real r__1;

    /* Local variables */
    static real xt, yt;
    extern /* Subroutine */ int cr2c_(real *, real *, real *, real *, real *);

    cr2c_(phi, x0, y0, x1, &yt);
    r__1 = -(*theta);
    cr2c_(&r__1, x1, z0, &xt, z1);
    cr2c_(psi, &xt, &yt, x1, y1);
    return 0;
} /* cr3c_ */


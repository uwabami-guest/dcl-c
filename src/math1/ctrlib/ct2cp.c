/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     COORDINATE TRANSFORMATION (2D CARTESIAN -> POLAR) */
/*                                              93/02/18   S.SAKAI */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int ct2cp_(real *x, real *y, real *r__, real *theta)
{
    /* Builtin functions */
    double sqrt(doublereal), atan2(doublereal, doublereal);

/*                  0 < THETA < PI */
/*                -PI < PHI   < PI */
    *r__ = sqrt(*x * *x + *y * *y);
    *theta = atan2(*y, *x);
    return 0;
} /* ct2cp_ */


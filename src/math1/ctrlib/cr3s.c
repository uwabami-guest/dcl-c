/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static real c_b3 = 0.f;

/* ----------------------------------------------------------------------- */
/*     COORDINATE ROTATION (3D SPHERICAL) */
/*                                              93/02/19   S.SAKAI */
/*                                              93/10/29   S.SAKAI */
/*                                            2000/04/28   S.SAKAI */
/*                                            2002/01/17   S.SAKAI */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int cr3s_(real *theta, real *phi, real *psi, real *theta0, 
	real *phi0, real *theta1, real *phi1)
{
    /* Initialized data */

    static logical lfirst = TRUE_;

    /* System generated locals */
    real r__1, r__2;

    /* Builtin functions */
    double cos(doublereal), sin(doublereal), acos(doublereal), sqrt(
	    doublereal), asin(doublereal), atan2(doublereal, doublereal);

    /* Local variables */
    static real a, b, c__, cp, pi, sp, ct0, st0, cte, ste;
    extern real rfpi_(void);
    extern logical lreqa_(real *, real *, real *);
    static real repsl;
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen);

    if (lfirst) {
	glrget_("REPSL", &repsl, (ftnlen)5);
	pi = rfpi_();
	lfirst = FALSE_;
    }
    if (lreqa_(theta, &c_b3, &repsl)) {
	*theta1 = *theta0;
	*phi1 = *phi0 - *phi - *psi;
    } else {
	cte = cos(*theta);
	ste = sin(*theta);
	ct0 = cos(*theta0);
	st0 = sin(*theta0);
	cp = cos(*phi0 - *phi);
	sp = sin(*phi0 - *phi);
	a = ct0 * cte + st0 * ste * cp;
	if (abs(a) <= .8f) {
	    *theta1 = acos(a);
	} else {
/* Computing 2nd power */
	    r__1 = ste * sp;
/* Computing 2nd power */
	    r__2 = ct0 * ste * cp - st0 * cte;
	    c__ = sqrt(r__1 * r__1 + r__2 * r__2);
	    *theta1 = asin(c__);
	    if (a <= 0.f) {
		*theta1 = pi - *theta1;
	    }
	}
	a = sp * st0;
	b = -ct0 * ste + st0 * cte * cp;
	if (a == 0.f && b == 0.f) {
	    *phi1 = *phi0 - *phi - *psi;
	} else {
	    *phi1 = atan2(a, b) - *psi;
	}
    }
    return 0;
} /* cr3s_ */


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     COORDINATE ROTATION (2D CARTESIAN) */
/*                                              93/02/19   S.SAKAI */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int cr2c_(real *theta, real *x0, real *y0, real *x1, real *
	y1)
{
    /* Builtin functions */
    double sin(doublereal), cos(doublereal);

    /* Local variables */
    static real ct, st;

    st = sin(*theta);
    ct = cos(*theta);
    *x1 = ct * *x0 + st * *y0;
    *y1 = -st * *x0 + ct * *y0;
    return 0;
} /* cr2c_ */


/*
 *   osabrt
 *
 *    Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved.
 *
 */

#include <stdlib.h>
#ifndef WINDOWS
void osabrt_(void)
#else
void OSABRT(void)
#endif
{
    abort();
}

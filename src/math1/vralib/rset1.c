/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int rset1_(real *rx, integer *n, integer *jx, real *rr)
{
    extern /* Subroutine */ int vrcon1_(real *, real *, integer *, integer *, 
	    integer *, real *);

    /* Parameter adjustments */
    --rx;

    /* Function Body */
    vrcon1_(&rx[1], &rx[1], n, jx, jx, rr);
    return 0;
} /* rset1_ */


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int vrcon1_(real *rx, real *ry, integer *n, integer *jx, 
	integer *jy, real *rr)
{
    /* System generated locals */
    integer i__1;

    /* Local variables */
    static integer j, kx, ky;
    static real rmiss;
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen);

    /* Parameter adjustments */
    --ry;
    --rx;

    /* Function Body */
    glrget_("RMISS", &rmiss, (ftnlen)5);
    kx = 1 - *jx;
    ky = 1 - *jy;
    i__1 = *n;
    for (j = 1; j <= i__1; ++j) {
	kx += *jx;
	ky += *jy;
	if (rx[kx] != rmiss) {
	    ry[ky] = *rr;
	} else {
	    ry[ky] = rmiss;
	}
/* L10: */
    }
    return 0;
} /* vrcon1_ */


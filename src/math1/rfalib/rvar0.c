/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
real rvar0_(real *rx, integer *n, integer *jx)
{
    /* System generated locals */
    integer i__1, i__2;
    real ret_val, r__1;

    /* Local variables */
    static integer i__;
    static real ave, sum;
    extern real rave0_(real *, integer *, integer *);

    /* Parameter adjustments */
    --rx;

    /* Function Body */
    ave = rave0_(&rx[1], n, jx);
    sum = 0.f;
    i__1 = *jx * (*n - 1) + 1;
    i__2 = *jx;
    for (i__ = 1; i__2 < 0 ? i__ >= i__1 : i__ <= i__1; i__ += i__2) {
/* Computing 2nd power */
	r__1 = rx[i__] - ave;
	sum += r__1 * r__1;
/* L10: */
    }
    ret_val = sum / *n;
    return ret_val;
} /* rvar0_ */


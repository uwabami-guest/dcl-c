/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
real rvmax1_(real *rx, integer *ns, integer *np, integer *nq, integer *nd)
{
    /* System generated locals */
    integer i__1;
    real ret_val;

    /* Local variables */
    static integer n, n1, n2, nn, nad[10], ndx, nzx[10];
    static logical lmada;
    static real rmiss;
    extern /* Subroutine */ int dxfloc_(integer *, integer *, integer *, 
	    integer *), glrget_(char *, real *, ftnlen), msgdmp_(char *, char 
	    *, char *, ftnlen, ftnlen, ftnlen);

    /* Parameter adjustments */
    --nq;
    --np;
    --ns;
    --rx;

    /* Function Body */
    if (! (1 <= *nd && *nd <= 10)) {
	msgdmp_("E", "RVMAX1", "NUMBER OF DIMENSION SHOULD BE 1<= ND <= 10.", 
		(ftnlen)1, (ftnlen)6, (ftnlen)43);
    }
    i__1 = *nd;
    for (n = 1; n <= i__1; ++n) {
	if (! (1 <= np[n] && np[n] <= nq[n] && nq[n] <= ns[n])) {
	    msgdmp_("E", "RVMAX1", "RELATION SHOULD BE 1 <= NP(N) <= NQ(N) <"
		    "= NS(N).", (ftnlen)1, (ftnlen)6, (ftnlen)48);
	}
/* L10: */
    }
    glrget_("RMISS", &rmiss, (ftnlen)5);
    dxfloc_(nd, &ns[1], &np[1], &n1);
    dxfloc_(nd, &ns[1], &nq[1], &n2);
    lmada = TRUE_;
    ndx = 1;
    i__1 = *nd;
    for (nn = 1; nn <= i__1; ++nn) {
	nzx[nn - 1] = np[nn];
	nad[nn - 1] = (ns[nn] - (nq[nn] - np[nn] + 1)) * ndx;
	ndx *= ns[nn];
/* L20: */
    }
    n = n1;
L30:
    i__1 = *nd;
    for (nn = 1; nn <= i__1; ++nn) {
	if (nzx[nn - 1] > nq[nn]) {
	    nzx[nn - 1] = np[nn];
	    ++nzx[nn];
	    n += nad[nn - 1];
	} else {
	    goto L50;
	}
/* L40: */
    }
L50:
    if (lmada) {
	if (rx[n] != rmiss) {
	    ret_val = rx[n];
	    lmada = FALSE_;
	}
    } else {
	if (rx[n] != rmiss && rx[n] > ret_val) {
	    ret_val = rx[n];
	}
    }
    ++nzx[0];
    ++n;
    if (n <= n2) {
	goto L30;
    }
    if (lmada) {
	ret_val = rmiss;
    }
    return ret_val;
} /* rvmax1_ */


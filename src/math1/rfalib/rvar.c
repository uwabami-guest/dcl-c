/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     RVAR */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
real rvar_(real *rx, integer *n, integer *jx)
{
    /* System generated locals */
    real ret_val;

    /* Local variables */
    extern real rvar0_(real *, integer *, integer *), rvar1_(real *, integer *
	    , integer *);
    static logical lmiss;
    extern /* Subroutine */ int gllget_(char *, logical *, ftnlen);

    /* Parameter adjustments */
    --rx;

    /* Function Body */
    gllget_("LMISS", &lmiss, (ftnlen)5);
    if (lmiss) {
	ret_val = rvar1_(&rx[1], n, jx);
    } else {
	ret_val = rvar0_(&rx[1], n, jx);
    }
    return ret_val;
} /* rvar_ */


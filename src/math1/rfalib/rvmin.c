/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     RVMIN */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
real rvmin_(real *rx, integer *ns, integer *np, integer *nq, integer *nd)
{
    /* System generated locals */
    real ret_val;

    /* Local variables */
    static logical lmiss;
    extern real rvmin0_(real *, integer *, integer *, integer *, integer *), 
	    rvmin1_(real *, integer *, integer *, integer *, integer *);
    extern /* Subroutine */ int gllget_(char *, logical *, ftnlen);

    /* Parameter adjustments */
    --nq;
    --np;
    --ns;
    --rx;

    /* Function Body */
    gllget_("LMISS", &lmiss, (ftnlen)5);
    if (lmiss) {
	ret_val = rvmin1_(&rx[1], &ns[1], &np[1], &nq[1], nd);
    } else {
	ret_val = rvmin0_(&rx[1], &ns[1], &np[1], &nq[1], nd);
    }
    return ret_val;
} /* rvmin_ */


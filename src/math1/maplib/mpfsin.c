/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     MAP PROJECTION (SINUSOIDAL)                   2007-10-29 E. TOYODA */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int mpfsin_0_(int n__, real *xlon, real *ylat, real *x, real 
	*y)
{
    /* System generated locals */
    real r__1, r__2, r__3;

    /* Builtin functions */
    double cos(doublereal);

    /* Local variables */
    static real pi, rna;
    extern real rfpi_(void);
    static real cosy;
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen);
    extern real xmplon_(real *);

    switch(n__) {
	case 1: goto L_mpisin;
	}

    pi = rfpi_();
    *x = xmplon_(xlon);
/* Computing MAX */
/* Computing MIN */
    r__3 = pi * .5f;
    r__1 = -pi * .5f, r__2 = min(r__3,*ylat);
    *y = max(r__1,r__2);
    *x *= cos(*y);
    return 0;
/* ----------------------------------------------------------------------- */

L_mpisin:
    *ylat = *y;
    cosy = cos(*y);
    if (cosy <= 1e-5f) {
	*x = 0.f;
    } else {
	*x /= cosy;
    }
    if (abs(*xlon) <= pi + .1f && abs(*ylat) <= pi / 2) {
	return 0;
    }
    glrget_("RUNDEF", &rna, (ftnlen)6);
    *xlon = rna;
    *ylat = rna;
    return 0;
} /* mpfsin_ */

/* Subroutine */ int mpfsin_(real *xlon, real *ylat, real *x, real *y)
{
    return mpfsin_0_(0, xlon, ylat, x, y);
    }

/* Subroutine */ int mpisin_(real *x, real *y, real *xlon, real *ylat)
{
    return mpfsin_0_(1, xlon, ylat, x, y);
    }


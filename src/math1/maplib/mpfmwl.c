/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     MAP PROJECTION (MOLLWEIDE-LIKE)                  93/02/20 S.SAKAI */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int mpfmwl_0_(int n__, real *xlon, real *ylat, real *x, real 
	*y)
{
    /* Builtin functions */
    double cos(doublereal), sin(doublereal), asin(doublereal);

    /* Local variables */
    static real pi, rna;
    extern real rfpi_(void);
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen);
    extern real xmplon_(real *);

    switch(n__) {
	case 1: goto L_mpimwl;
	}

    pi = rfpi_();
    *x = xmplon_(xlon) * 2.8284272000000001f * cos(*ylat) / pi;
    *y = sin(*ylat) * 1.4142136f;
    return 0;
/* ----------------------------------------------------------------------- */

L_mpimwl:
    pi = rfpi_();
    if (abs(*y) < 1.4142136f) {
	*ylat = asin(*y / 1.4142136f);
	*xlon = *x / 1.4142136f / cos(*ylat) * pi / 2;
	if (abs(*xlon) <= pi) {
	    return 0;
	}
    } else if (abs(*y) == 1.4142136f && *x == 0.f) {
	*xlon = 0.f;
	*ylat = *y / 1.4142136f * pi / 2;
    }
    glrget_("RUNDEF", &rna, (ftnlen)6);
    *xlon = rna;
    *ylat = rna;
    return 0;
} /* mpfmwl_ */

/* Subroutine */ int mpfmwl_(real *xlon, real *ylat, real *x, real *y)
{
    return mpfmwl_0_(0, xlon, ylat, x, y);
    }

/* Subroutine */ int mpimwl_(real *x, real *y, real *xlon, real *ylat)
{
    return mpfmwl_0_(1, xlon, ylat, x, y);
    }


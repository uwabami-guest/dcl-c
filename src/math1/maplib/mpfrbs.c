/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     MAP PROJECTION (ROBINSON)                     2007-10-21 E. TOYODA */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int mpfrbs_0_(int n__, real *xlon, real *ylat, real *x, real 
	*y)
{
    /* System generated locals */
    real r__1, r__2, r__3, r__4, r__5, r__6, r__7, r__8, r__9;

    /* Local variables */
    static real pi, yy, rna, rpi;
    extern real rfpi_(void);
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen);
    extern real xmplon_(real *);

    switch(n__) {
	case 1: goto L_mpirbs;
	}

    pi = rfpi_();
    *x = xmplon_(xlon);
/* Computing MAX */
/* Computing MIN */
    r__3 = pi * .5f - 1e-5f;
    r__1 = -pi * .5f + 1e-5f, r__2 = min(r__3,*ylat);
    yy = max(r__1,r__2);
/* Computing 2nd power */
    r__1 = yy;
/* Computing 4th power */
    r__2 = yy, r__2 *= r__2;
/* Computing 6th power */
    r__3 = yy, r__3 *= r__3;
/* Computing 8th power */
    r__4 = yy, r__4 *= r__4, r__4 *= r__4;
/* Computing 10th power */
    r__5 = yy, r__5 *= r__5, r__6 = r__5, r__5 *= r__5;
/* Computing 12th power */
    r__7 = yy, r__7 *= r__7, r__7 *= r__7;
    *x *= .998668f - r__1 * r__1 * .10658f - r__2 * r__2 * .158451f + r__3 * (
	    r__3 * r__3) * .12723f - r__4 * r__4 * .0467883f + r__6 * (r__5 * 
	    r__5) * .00413844f + r__7 * (r__7 * r__7) * 9.10851e-4f;
/* Computing 3rd power */
    r__1 = yy;
/* Computing 5th power */
    r__2 = yy, r__3 = r__2, r__2 *= r__2;
/* Computing 7th power */
    r__4 = yy, r__5 = r__4, r__4 *= r__4, r__5 *= r__4;
/* Computing 9th power */
    r__6 = yy, r__7 = r__6, r__6 *= r__6, r__6 *= r__6;
    *y = (yy * .358968f + r__1 * (r__1 * r__1) * .0109118f - r__3 * (r__2 * 
	    r__2) * .0204323f + r__5 * (r__4 * r__4) * .00940259f - r__7 * (
	    r__6 * r__6) * .00215423f) * pi;
    return 0;
/* ----------------------------------------------------------------------- */

L_mpirbs:
    rpi = 1.f / rfpi_();
    yy = *y * rpi;
/* Computing 3rd power */
    r__1 = yy;
/* Computing 5th power */
    r__2 = yy, r__3 = r__2, r__2 *= r__2;
/* Computing 7th power */
    r__4 = yy, r__5 = r__4, r__4 *= r__4, r__5 *= r__4;
/* Computing 9th power */
    r__6 = yy, r__7 = r__6, r__6 *= r__6, r__6 *= r__6;
/* Computing 11th power */
    r__8 = yy, r__9 = r__8, r__8 *= r__8, r__9 *= r__8, r__8 *= r__8;
    *ylat = yy * 2.7325f + r__1 * (r__1 * r__1) * 1.75437f - r__3 * (r__2 * 
	    r__2) * 17.5922f + r__5 * (r__4 * r__4) * 61.9335f + r__7 * (r__6 
	    * r__6) * .0194069f + r__9 * (r__8 * r__8) * .010234f;
/* Computing 2nd power */
    r__1 = *ylat;
/* Computing 4th power */
    r__2 = *ylat, r__2 *= r__2;
/* Computing 6th power */
    r__3 = *ylat, r__3 *= r__3;
/* Computing 8th power */
    r__4 = *ylat, r__4 *= r__4, r__4 *= r__4;
/* Computing 10th power */
    r__5 = *ylat, r__5 *= r__5, r__6 = r__5, r__5 *= r__5;
/* Computing 12th power */
    r__7 = *ylat, r__7 *= r__7, r__7 *= r__7;
    *xlon = *x / (.998668f - r__1 * r__1 * .10658f - r__2 * r__2 * .158451f + 
	    r__3 * (r__3 * r__3) * .12723f - r__4 * r__4 * .0467883f + r__6 * 
	    (r__5 * r__5) * .00413844f + r__7 * (r__7 * r__7) * 9.10851e-4f);
    if (abs(*xlon) <= pi + .1f && abs(*ylat) <= pi / 2) {
	return 0;
    }
    glrget_("RUNDEF", &rna, (ftnlen)6);
    *xlon = rna;
    *ylat = rna;
    return 0;
} /* mpfrbs_ */

/* Subroutine */ int mpfrbs_(real *xlon, real *ylat, real *x, real *y)
{
    return mpfrbs_0_(0, xlon, ylat, x, y);
    }

/* Subroutine */ int mpirbs_(real *x, real *y, real *xlon, real *ylat)
{
    return mpfrbs_0_(1, xlon, ylat, x, y);
    }


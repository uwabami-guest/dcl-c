/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static real c_b2 = 1e-5f;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
real xmplon_(real *xlon)
{
    /* System generated locals */
    integer i__1, i__2;
    real ret_val, r__1;

    /* Builtin functions */
    integer i_nint(real *), i_sign(integer *, integer *);

    /* Local variables */
    static integer ii;
    static real pi;
    static integer ix, nx;
    static real rx;
    extern real rfpi_(void);
    extern logical lreqa_(real *, real *, real *);

    pi = rfpi_();
    rx = abs(*xlon) / pi;
    nx = i_nint(&rx);
    r__1 = (real) nx;
    if (lreqa_(&rx, &r__1, &c_b2)) {
	ii = nx - 1;
    } else {
	ii = (integer) rx - 1 + (integer) (rx - (integer) rx + 1);
    }
    i__1 = (ii + 1) / 2;
    i__2 = (integer) (*xlon);
    ix = i_sign(&i__1, &i__2);
    ret_val = *xlon - ix * pi * 2;
    return ret_val;
} /* xmplon_ */


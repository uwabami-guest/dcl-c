/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     MAP PROJECTION (EQUAL-AREA CYLINDRICAL)       2007-10-14 E. TOYODA */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int mpfcya_0_(int n__, real *xlon, real *ylat, real *x, real 
	*y)
{
    /* System generated locals */
    real r__1, r__2;

    /* Builtin functions */
    double sin(doublereal), asin(doublereal);

    /* Local variables */
    static real pi, rna;
    extern real rfpi_(void);
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen);
    extern real xmplon_(real *);

    switch(n__) {
	case 1: goto L_mpicya;
	}

    pi = rfpi_();
    *x = xmplon_(xlon);
    *y = sin(*ylat);
/* Computing MAX */
    r__1 = -1.f, r__2 = min(1.f,*y);
    *y = max(r__1,r__2);
    return 0;
/* ----------------------------------------------------------------------- */

L_mpicya:
    pi = rfpi_();
    *xlon = *x;
    *ylat = asin(*y);
    if (abs(*xlon) <= pi && abs(*ylat) <= pi / 2) {
	return 0;
    }
    glrget_("RUNDEF", &rna, (ftnlen)6);
    *xlon = rna;
    *ylat = rna;
    return 0;
} /* mpfcya_ */

/* Subroutine */ int mpfcya_(real *xlon, real *ylat, real *x, real *y)
{
    return mpfcya_0_(0, xlon, ylat, x, y);
    }

/* Subroutine */ int mpicya_(real *x, real *y, real *xlon, real *ylat)
{
    return mpfcya_0_(1, xlon, ylat, x, y);
    }


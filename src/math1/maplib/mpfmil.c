/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     MAP PROJECTION (MILLER CYLINDRICAL)          2007-10-14 E. TOYODA */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int mpfmil_0_(int n__, real *xlon, real *ylat, real *x, real 
	*y)
{
    /* System generated locals */
    real r__1, r__2, r__3;

    /* Builtin functions */
    double tan(doublereal), log(doublereal), sinh(doublereal), atan(
	    doublereal);

    /* Local variables */
    static real pi, yy, rna;
    extern real rfpi_(void);
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen);
    extern real xmplon_(real *);

    switch(n__) {
	case 1: goto L_mpimil;
	}

    pi = rfpi_();
    *x = xmplon_(xlon);
/* Computing MAX */
/* Computing MIN */
    r__3 = pi * .5f;
    r__1 = -pi * .5f, r__2 = min(r__3,*ylat);
    yy = max(r__1,r__2);
    *y = log(tan(pi * .25f + yy * .4f)) * 1.25f;
    return 0;
/* ----------------------------------------------------------------------- */

L_mpimil:
    pi = rfpi_();
    *xlon = *x;
    *ylat = atan(sinh(*y * .8f)) * 1.25f;
    if (abs(*xlon) <= pi && abs(*ylat) <= pi / 2) {
	return 0;
    }
    glrget_("RUNDEF", &rna, (ftnlen)6);
    *xlon = rna;
    *ylat = rna;
    return 0;
} /* mpfmil_ */

/* Subroutine */ int mpfmil_(real *xlon, real *ylat, real *x, real *y)
{
    return mpfmil_0_(0, xlon, ylat, x, y);
    }

/* Subroutine */ int mpimil_(real *x, real *y, real *xlon, real *ylat)
{
    return mpfmil_0_(1, xlon, ylat, x, y);
    }


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static real c_b3 = 1.f;

/* ----------------------------------------------------------------------- */
/*     MAP PROJECTION (LAMBERT CONICAL 2)               93/02/20 S.SAKAI */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int mpfcoc_0_(int n__, real *xlon, real *ylat, real *x, real 
	*y, real *ylat1, real *ylat2)
{
    /* System generated locals */
    real r__1, r__2;
    doublereal d__1, d__2;

    /* Builtin functions */
    double tan(doublereal), pow_dd(doublereal *, doublereal *), sin(
	    doublereal), cos(doublereal), sqrt(doublereal), atan2(doublereal, 
	    doublereal), atan(doublereal), r_sign(real *, real *), log(
	    doublereal);

    /* Local variables */
    static real r__, s, ck, dk, pi, th, tt, th1, th2, dlm, rna;
    extern real rfpi_(void);
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen);
    extern real xmplon_(real *);

    switch(n__) {
	case 1: goto L_mpicoc;
	case 2: goto L_mpscoc;
	}

    th = pi / 2 - s * *ylat;
    if (th >= pi - 1e-5f) {
	th = pi - 1e-5f;
    }
    tt = tan(th / 2.f);
    if (abs(tt) < 1e-6f) {
	tt = 0.f;
    }
    d__1 = (doublereal) tt;
    d__2 = (doublereal) dk;
    r__ = ck * pow_dd(&d__1, &d__2);
    if (r__ > 10.f) {
	r__ = 10.f;
    }
    dlm = xmplon_(xlon) * dk;
    *x = r__ * sin(dlm);
    *y = -s * r__ * cos(dlm);
    return 0;
/* ----------------------------------------------------------------------- */

L_mpicoc:
    r__ = sqrt(*x * *x + *y * *y);
    if (r__ == 0.f) {
	*xlon = 0.f;
	*ylat = pi / 2.f;
	return 0;
    } else {
	*xlon = atan2(*x, -s * *y) / dk;
	if (abs(*xlon) <= pi) {
	    d__1 = (doublereal) (r__ / ck);
	    d__2 = (doublereal) (1.f / dk);
	    *ylat = s * (pi / 2 - atan(pow_dd(&d__1, &d__2)) * 2);
	    return 0;
	}
    }
    *xlon = rna;
    *ylat = rna;
    return 0;
/* ----------------------------------------------------------------------- */

L_mpscoc:
    pi = rfpi_();
    glrget_("RUNDEF", &rna, (ftnlen)6);
    r__1 = *ylat1 + *ylat2;
    s = r_sign(&c_b3, &r__1);
/* Computing MAX */
    r__1 = abs(*ylat1), r__2 = abs(*ylat2);
    th1 = pi / 2 - max(r__1,r__2);
/* Computing MIN */
    r__1 = abs(*ylat1), r__2 = abs(*ylat2);
    th2 = pi / 2 - min(r__1,r__2);
    dk = (log(sin(th2)) - log(sin(th1))) / (log(tan(th2 / 2)) - log(tan(th1 / 
	    2)));
    d__1 = (doublereal) tan(th1 / 2);
    d__2 = (doublereal) dk;
    ck = sin(th1) / dk / pow_dd(&d__1, &d__2);
    return 0;
} /* mpfcoc_ */

/* Subroutine */ int mpfcoc_(real *xlon, real *ylat, real *x, real *y)
{
    return mpfcoc_0_(0, xlon, ylat, x, y, (real *)0, (real *)0);
    }

/* Subroutine */ int mpicoc_(real *x, real *y, real *xlon, real *ylat)
{
    return mpfcoc_0_(1, xlon, ylat, x, y, (real *)0, (real *)0);
    }

/* Subroutine */ int mpscoc_(real *ylat1, real *ylat2)
{
    return mpfcoc_0_(2, (real *)0, (real *)0, (real *)0, (real *)0, ylat1, 
	    ylat2);
    }


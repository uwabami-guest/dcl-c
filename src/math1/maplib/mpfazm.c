/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     MAP PROJECTION (AZIMUTHAL EQUIDISTANT)           93/02/20 S.SAKAI */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int mpfazm_0_(int n__, real *xlon, real *ylat, real *x, real 
	*y)
{
    /* Builtin functions */
    double sin(doublereal), cos(doublereal), sqrt(doublereal), atan2(
	    doublereal, doublereal);

    /* Local variables */
    static real r__, pi, rna;
    extern real rfpi_(void);
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen);

    switch(n__) {
	case 1: goto L_mpiazm;
	}

    pi = rfpi_();
    r__ = pi / 2 - *ylat;
    if (r__ > pi) {
	r__ = pi;
    }
    if (r__ < 0.f) {
	r__ = 0.f;
    }
    *x = r__ * sin(*xlon);
    *y = -r__ * cos(*xlon);
    return 0;
/* ----------------------------------------------------------------------- */

L_mpiazm:
    pi = rfpi_();
    r__ = sqrt(*x * *x + *y * *y);
    if (r__ == 0.f) {
	*xlon = 0.f;
	*ylat = pi / 2.f;
	return 0;
    } else if (r__ <= pi) {
	*xlon = atan2(*x, -(*y));
	*ylat = pi / 2 - r__;
	return 0;
    }
    glrget_("RUNDEF", &rna, (ftnlen)6);
    *xlon = rna;
    *ylat = rna;
    return 0;
} /* mpfazm_ */

/* Subroutine */ int mpfazm_(real *xlon, real *ylat, real *x, real *y)
{
    return mpfazm_0_(0, xlon, ylat, x, y);
    }

/* Subroutine */ int mpiazm_(real *x, real *y, real *xlon, real *ylat)
{
    return mpfazm_0_(1, xlon, ylat, x, y);
    }


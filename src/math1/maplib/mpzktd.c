/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     FUNCTION OF LATITUDE (KITADA) */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int mpzktd_(real *phi, real *alpha, real *f, real *df)
{
    /* Builtin functions */
    double sin(doublereal), cos(doublereal);

    /* Local variables */
    static real b, pi;
    extern real rfpi_(void);

    pi = rfpi_();
    b = pi * 2 / 3.f + .86602539999999995f;
    *f = *alpha * 2 + sin(*alpha * 2) - b * sin(*phi);
    *df = cos(*alpha * 2) * 2 + 2;
    return 0;
} /* mpzktd_ */


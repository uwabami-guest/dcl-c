/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     MAP PROJECTION (HAMMER)                          93/02/20 S.SAKAI */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int mpfhmr_0_(int n__, real *xlon, real *ylat, real *x, real 
	*y)
{
    /* System generated locals */
    real r__1, r__2;

    /* Builtin functions */
    double cos(doublereal), sqrt(doublereal), sin(doublereal), asin(
	    doublereal);

    /* Local variables */
    static real cf, rr, xx, yy, phi, rna, sqa, xlm2, cphi;
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen);
    extern real xmplon_(real *);

    switch(n__) {
	case 1: goto L_mpihmr;
	}

    xlm2 = xmplon_(xlon) / 2;
    cphi = cos(*ylat);
    cf = sqrt(cphi * cos(xlm2) + 1.f);
    *x = cphi * 2.8284272000000001f * sin(xlm2) / cf;
    *y = sin(*ylat) * 1.4142136f / cf;
    return 0;
/* ----------------------------------------------------------------------- */

L_mpihmr:
/* Computing 2nd power */
    r__1 = *x / 2 / 1.4142136f;
/* Computing 2nd power */
    r__2 = *y / 1.4142136f;
    rr = r__1 * r__1 + r__2 * r__2;
    if (rr <= 1.f) {
	sqa = sqrt(2 - rr);
	yy = sqa * *y / 1.4142136f;
	if (abs(yy) <= 1.f) {
	    phi = asin(yy);
	    xx = *x * sqa / 2 / 1.4142136f / cos(phi);
	    if (abs(xx) <= 1.f) {
		*xlon = asin(xx) * 2;
		*ylat = phi;
		return 0;
	    }
	}
    }
    glrget_("RUNDEF", &rna, (ftnlen)6);
    *xlon = rna;
    *ylat = rna;
    return 0;
} /* mpfhmr_ */

/* Subroutine */ int mpfhmr_(real *xlon, real *ylat, real *x, real *y)
{
    return mpfhmr_0_(0, xlon, ylat, x, y);
    }

/* Subroutine */ int mpihmr_(real *x, real *y, real *xlon, real *ylat)
{
    return mpfhmr_0_(1, xlon, ylat, x, y);
    }


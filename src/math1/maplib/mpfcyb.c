/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     MAP PROJECTION (BRAUN PERSPECTIVE CYLINDRICAL) 2007-10-14 E. TOYODA */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int mpfcyb_0_(int n__, real *xlon, real *ylat, real *x, real 
	*y, real *stdlat)
{
    /* Initialized data */

    static real c__ = 1.f;

    /* Builtin functions */
    double tan(doublereal), atan(doublereal), cos(doublereal);

    /* Local variables */
    static real pi, rna;
    extern real rfpi_(void);
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen);
    extern real xmplon_(real *);

    switch(n__) {
	case 1: goto L_mpicyb;
	case 2: goto L_mpscyb;
	}

    pi = rfpi_();
    *x = xmplon_(xlon);
    *y = c__ * 2.f * tan(*ylat * .5f);
    return 0;
/* ----------------------------------------------------------------------- */

L_mpicyb:
    pi = rfpi_();
    *xlon = *x;
    *ylat = atan(*y * .5f / c__) * 2.f;
    if (abs(*xlon) <= pi) {
	return 0;
    }
    glrget_("RUNDEF", &rna, (ftnlen)6);
    *xlon = rna;
    *ylat = rna;
    return 0;
/* ----------------------------------------------------------------------- */

L_mpscyb:
    c__ = cos(*stdlat);
    return 0;
} /* mpfcyb_ */

/* Subroutine */ int mpfcyb_(real *xlon, real *ylat, real *x, real *y)
{
    return mpfcyb_0_(0, xlon, ylat, x, y, (real *)0);
    }

/* Subroutine */ int mpicyb_(real *x, real *y, real *xlon, real *ylat)
{
    return mpfcyb_0_(1, xlon, ylat, x, y, (real *)0);
    }

/* Subroutine */ int mpscyb_(real *stdlat)
{
    return mpfcyb_0_(2, (real *)0, (real *)0, (real *)0, (real *)0, stdlat);
    }


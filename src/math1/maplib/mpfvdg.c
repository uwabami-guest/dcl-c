/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static logical c_false = FALSE_;

/* ----------------------------------------------------------------------- */
/*     MAP PROJECTION (VAN DER GRINTEN)              2007-11-04 E. TOYODA */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int mpxvdg_(real *xlon, real *ylat, real *x, real *y)
{
    /* System generated locals */
    real r__1, r__2, r__3, r__4, r__5, r__6, r__7, r__8, r__9, r__10, r__11, 
	    r__12, r__13;

    /* Builtin functions */
    double r_dim(real *, real *), sqrt(doublereal), asin(doublereal), sin(
	    doublereal), cos(doublereal);

    /* Local variables */
    static real a, g, p, q, pi, th, phi;
    extern real rfpi_(void);
    static real lambda, halfpi, sgnlam, sgnphi;
    extern real xmplon_(real *);

    pi = rfpi_();
    halfpi = pi * .5f;
    lambda = xmplon_(xlon);
    phi = *ylat;
    if (phi > halfpi - 1e-5f) {
	*y = pi + (phi - halfpi) * 25.f;
	*x = lambda * 1e3f * r_dim(&phi, &halfpi);
	goto L8000;
    } else if (phi < -halfpi + 1e-5f) {
	*y = -pi + (phi + halfpi) * 25.f;
	r__1 = -phi;
	*x = lambda * 1e3f * r_dim(&r__1, &halfpi);
	goto L8000;
    } else if (abs(phi) < 1e-5f) {
	*y = phi;
	if (abs(lambda) < 1e-5f) {
	    *x = lambda;
	    goto L8000;
	}
	a = (r__1 = pi / lambda - lambda / pi, abs(r__1)) * .5f;
	if (lambda > 0.f) {
/* Computing 2nd power */
	    r__1 = a;
	    *x = pi * (sqrt(r__1 * r__1 + 1) - a);
	} else {
/* Computing 2nd power */
	    r__1 = a;
	    *x = -pi * (sqrt(r__1 * r__1 + 1) - a);
	}
	goto L8000;
    }
    th = asin((r__1 = phi * 2.f / pi, abs(r__1)));
    g = cos(th) / (sin(th) + cos(th) - 1.f);
    p = g * (2.f / sin(th) - 1.f);
    if (abs(lambda) < .001f) {
	*x = lambda;
/* Computing 3rd power */
	r__1 = phi;
/* Computing 5th power */
	r__2 = phi, r__3 = r__2, r__2 *= r__2;
/* Computing 7th power */
	r__4 = phi, r__5 = r__4, r__4 *= r__4, r__5 *= r__4;
/* Computing 9th power */
	r__6 = phi, r__7 = r__6, r__6 *= r__6, r__6 *= r__6;
/* Computing 11th power */
	r__8 = phi, r__9 = r__8, r__8 *= r__8, r__9 *= r__8, r__8 *= r__8;
/* Computing 13th power */
	r__10 = phi, r__11 = r__10, r__10 *= r__10, r__10 *= r__10, r__11 *= 
		r__10;
/* Computing 15th power */
	r__12 = phi, r__13 = r__12, r__12 *= r__12, r__13 *= r__12, r__12 *= 
		r__12, r__13 *= r__12;
	*y = phi * .9770711f + r__1 * (r__1 * r__1) * .5484717f - r__3 * (
		r__2 * r__2) * 2.441382f + r__5 * (r__4 * r__4) * 5.923668f - 
		r__7 * (r__6 * r__6) * 7.208943f + r__9 * (r__8 * r__8) * 
		4.639224f - r__11 * (r__10 * r__10) * 1.500824f + r__13 * (
		r__12 * r__12) * .1924256f;
	goto L8000;
    }
    a = (r__1 = pi / lambda - lambda / pi, abs(r__1)) * .5f;
    q = a * a + g;
    if (lambda > 0.f) {
	sgnlam = 1.f;
    } else {
	sgnlam = -1.f;
    }
/* Computing 2nd power */
    r__1 = p;
/* Computing 2nd power */
    r__2 = a;
/* Computing 2nd power */
    r__4 = p;
/* Computing 2nd power */
    r__3 = g - r__4 * r__4;
/* Computing 2nd power */
    r__5 = p;
/* Computing 2nd power */
    r__6 = a;
/* Computing 2nd power */
    r__7 = g;
/* Computing 2nd power */
    r__8 = p;
    *x = sgnlam * (pi * (a * (g - r__1 * r__1) + sqrt(r__2 * r__2 * (r__3 * 
	    r__3) - (r__5 * r__5 + r__6 * r__6) * (r__7 * r__7 - r__8 * r__8))
	    )) / (p * p + a * a);
    if (phi >= 0.f) {
	sgnphi = 1.f;
    } else {
	sgnphi = -1.f;
    }
/* Computing 2nd power */
    r__2 = a;
/* Computing 2nd power */
    r__3 = p;
/* Computing 2nd power */
    r__4 = a;
/* Computing 2nd power */
    r__5 = q;
/* Computing 2nd power */
    r__6 = p;
/* Computing 2nd power */
    r__7 = a;
    *y = sgnphi * pi * (r__1 = p * q - a * sqrt((r__2 * r__2 + 1) * (r__3 * 
	    r__3 + r__4 * r__4) - r__5 * r__5), abs(r__1)) / (r__6 * r__6 + 
	    r__7 * r__7);
L8000:
    return 0;
} /* mpxvdg_ */

/* ----------------------------------------------------------------------- */
/* Subroutine */ int mpfvdg_0_(int n__, real *xlon, real *ylat, real *x, real 
	*y)
{
    extern /* Subroutine */ int mpxvdg_(real *, real *, real *, real *), 
	    mpnwtn_(real *, real *, real *, real *, S_fp, logical *);

    switch(n__) {
	case 1: goto L_mpivdg;
	}

    mpxvdg_(xlon, ylat, x, y);
    return 0;

L_mpivdg:
    *xlon = *x * .5f;
    *ylat = *y * .5f;
    mpnwtn_(x, y, xlon, ylat, (S_fp)mpxvdg_, &c_false);
    return 0;
} /* mpfvdg_ */

/* Subroutine */ int mpfvdg_(real *xlon, real *ylat, real *x, real *y)
{
    return mpfvdg_0_(0, xlon, ylat, x, y);
    }

/* Subroutine */ int mpivdg_(real *x, real *y, real *xlon, real *ylat)
{
    return mpfvdg_0_(1, xlon, ylat, x, y);
    }


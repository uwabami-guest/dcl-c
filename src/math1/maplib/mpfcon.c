/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static real c_b3 = 1.f;

/* ----------------------------------------------------------------------- */
/*     MAP PROJECTION (CONICAL)                         93/02/20 S.SAKAI */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int mpfcon_0_(int n__, real *xlon, real *ylat, real *x, real 
	*y, real *ylat0)
{
    /* Builtin functions */
    double sin(doublereal), cos(doublereal), sqrt(doublereal), atan2(
	    doublereal, doublereal), r_sign(real *, real *), tan(doublereal);

    /* Local variables */
    static real r__, s, cc, dk, pi, th, dlm, rna;
    extern real rfpi_(void);
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen);
    extern real xmplon_(real *);

    switch(n__) {
	case 1: goto L_mpicon;
	case 2: goto L_mpscon;
	}

    th = pi / 2 - s * *ylat;
    r__ = th + cc;
    dlm = xmplon_(xlon) * dk;
    *x = r__ * sin(dlm);
    *y = -s * r__ * cos(dlm);
    return 0;
/* ----------------------------------------------------------------------- */

L_mpicon:
    r__ = sqrt(*x * *x + *y * *y);
    if (r__ == 0.f) {
	*xlon = 0.f;
    } else {
	*xlon = atan2(*x, -s * *y) / dk;
    }
    *ylat = s * (pi / 2 - r__ + cc);
    if (abs(*xlon) > pi || abs(*ylat) > pi / 2) {
	*xlon = rna;
	*ylat = rna;
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_mpscon:
    pi = rfpi_();
    glrget_("RUNDEF", &rna, (ftnlen)6);
    s = r_sign(&c_b3, ylat0);
    th = pi / 2 - abs(*ylat0);
    dk = cos(th);
    cc = tan(th) - th;
    return 0;
} /* mpfcon_ */

/* Subroutine */ int mpfcon_(real *xlon, real *ylat, real *x, real *y)
{
    return mpfcon_0_(0, xlon, ylat, x, y, (real *)0);
    }

/* Subroutine */ int mpicon_(real *x, real *y, real *xlon, real *ylat)
{
    return mpfcon_0_(1, xlon, ylat, x, y, (real *)0);
    }

/* Subroutine */ int mpscon_(real *ylat0)
{
    return mpfcon_0_(2, (real *)0, (real *)0, (real *)0, (real *)0, ylat0);
    }


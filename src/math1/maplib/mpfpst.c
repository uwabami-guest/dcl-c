/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     MAP PROJECTION (POLAR STEREO)                    93/02/20 S.SAKAI */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int mpfpst_0_(int n__, real *xlon, real *ylat, real *x, real 
	*y)
{
    /* Builtin functions */
    double tan(doublereal), sin(doublereal), cos(doublereal), sqrt(doublereal)
	    , atan(doublereal), atan2(doublereal, doublereal);

    /* Local variables */
    static real r__, pi, th;
    extern real rfpi_(void);

    switch(n__) {
	case 1: goto L_mpipst;
	}

    pi = rfpi_();
    th = pi / 2 - *ylat;
    if (th >= pi - 1e-4f) {
	th = pi - 1e-4f;
    }
    r__ = tan(th / 2) * 2;
    *x = r__ * sin(*xlon);
    *y = -r__ * cos(*xlon);
    return 0;
/* ----------------------------------------------------------------------- */

L_mpipst:
    pi = rfpi_();
    r__ = sqrt(*x * *x + *y * *y);
    *ylat = pi / 2 - atan(r__ / 2) * 2;
    if (r__ == 0.f) {
	*xlon = 0.f;
    } else {
	*xlon = atan2(*x, -(*y));
    }
    return 0;
} /* mpfpst_ */

/* Subroutine */ int mpfpst_(real *xlon, real *ylat, real *x, real *y)
{
    return mpfpst_0_(0, xlon, ylat, x, y);
    }

/* Subroutine */ int mpipst_(real *x, real *y, real *xlon, real *ylat)
{
    return mpfpst_0_(1, xlon, ylat, x, y);
    }


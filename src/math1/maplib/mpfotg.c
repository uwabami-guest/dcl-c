/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     MAP PROJECTION (ORTHOGRAPIC)                     93/02/20 S.SAKAI */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int mpfotg_0_(int n__, real *xlon, real *ylat, real *x, real 
	*y, real *r0)
{
    /* Initialized data */

    static logical lsat = FALSE_;

    /* Builtin functions */
    double sin(doublereal), cos(doublereal), sqrt(doublereal), acos(
	    doublereal), atan(doublereal), atan2(doublereal, doublereal);

    /* Local variables */
    static real c__, r__, r1, r2, rr, rna, xlm;
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen);
    extern real xmplon_(real *);

    switch(n__) {
	case 1: goto L_mpiotg;
	case 2: goto L_mpsotg;
	}

    xlm = xmplon_(xlon);
    if (lsat) {
	r__ = cos(*ylat) / (1 - c__ * sin(*ylat));
    } else {
	r__ = cos(*ylat);
    }
    *x = r__ * sin(xlm);
    *y = -r__ * cos(xlm);
    return 0;
/* ----------------------------------------------------------------------- */

L_mpiotg:
    if (lsat) {
	rr = *x * *x + *y * *y;
	r1 = sqrt(rr);
	r2 = sqrt(rr / (c__ * rr + 1));
	if (r2 <= 1.f) {
	    *ylat = acos(r2) - atan(c__ * r1);
	    if (r1 == 0.f) {
		*xlon = 0.f;
		return 0;
	    } else {
		*xlon = atan2(*x, -(*y));
		return 0;
	    }
	}
    } else {
	r__ = sqrt(*x * *x + *y * *y);
	if (r__ == 0.f) {
	    *xlon = 0.f;
	    *ylat = acos(r__);
	    return 0;
	} else if (r__ <= 1.f) {
	    *xlon = atan2(*x, -(*y));
	    *ylat = acos(r__);
	    return 0;
	}
    }
    glrget_("RUNDEF", &rna, (ftnlen)6);
    *xlon = rna;
    *ylat = rna;
    return 0;
/* ----------------------------------------------------------------------- */

L_mpsotg:
    lsat = *r0 > 1.f;
    if (lsat) {
	c__ = 1.f / *r0;
    }
    return 0;
} /* mpfotg_ */

/* Subroutine */ int mpfotg_(real *xlon, real *ylat, real *x, real *y)
{
    return mpfotg_0_(0, xlon, ylat, x, y, (real *)0);
    }

/* Subroutine */ int mpiotg_(real *x, real *y, real *xlon, real *ylat)
{
    return mpfotg_0_(1, xlon, ylat, x, y, (real *)0);
    }

/* Subroutine */ int mpsotg_(real *r0)
{
    return mpfotg_0_(2, (real *)0, (real *)0, (real *)0, (real *)0, r0);
    }


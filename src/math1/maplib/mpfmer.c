/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     MAP PROJECTION (MERCATOR)                        93/02/20 S.SAKAI */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int mpfmer_0_(int n__, real *xlon, real *ylat, real *x, real 
	*y)
{
    /* Builtin functions */
    double tan(doublereal), log(doublereal), exp(doublereal), atan(doublereal)
	    ;

    /* Local variables */
    static real pi, rna;
    extern real rfpi_(void);
    static real theta;
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen);
    extern real xmplon_(real *);

    switch(n__) {
	case 1: goto L_mpimer;
	}

    pi = rfpi_();
    theta = (*ylat + pi / 2) / 2.f;
    if (theta < 1e-5f) {
	theta = 1e-5f;
    }
    if (theta > pi / 2 - 1e-5f) {
	theta = pi / 2 - 1e-5f;
    }
    *x = xmplon_(xlon);
    *y = log(tan(theta));
    return 0;
/* ----------------------------------------------------------------------- */

L_mpimer:
    pi = rfpi_();
    *xlon = *x;
    if (abs(*xlon) <= pi) {
	*ylat = atan(exp(*y)) * 2 - pi / 2;
	return 0;
    }
    glrget_("RUNDEF", &rna, (ftnlen)6);
    *xlon = rna;
    *ylat = rna;
    return 0;
} /* mpfmer_ */

/* Subroutine */ int mpfmer_(real *xlon, real *ylat, real *x, real *y)
{
    return mpfmer_0_(0, xlon, ylat, x, y);
    }

/* Subroutine */ int mpimer_(real *x, real *y, real *xlon, real *ylat)
{
    return mpfmer_0_(1, xlon, ylat, x, y);
    }


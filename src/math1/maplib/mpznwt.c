/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     NEWTON METHOD */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int mpznwt_(S_fp func, real *phi, real *alpha)
{
    static real f;
    static integer i__;
    static real df, dx;
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen);

    for (i__ = 1; i__ <= 20; ++i__) {
	(*func)(phi, alpha, &f, &df);
	if (abs(df) < 1.2e-5f) {
	    return 0;
	}
	dx = f / df;
	*alpha -= dx;
	if (abs(dx) < 1e-4f) {
	    return 0;
	}
/* L10: */
    }
    msgdmp_("E", "MPZNWT", "EXCEEDING MAXIMUM ITERATIONS.", (ftnlen)1, (
	    ftnlen)6, (ftnlen)29);
    return 0;
} /* mpznwt_ */


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     MAP PROJECTION (KITADA)                          93/02/20 S.SAKAI */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int mpfktd_0_(int n__, real *xlon, real *ylat, real *x, real 
	*y)
{
    /* Builtin functions */
    double cos(doublereal), sin(doublereal), asin(doublereal);

    /* Local variables */
    static real pi, rna;
    extern real rfpi_(void);
    static real alpha;
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen);
    extern /* Subroutine */ int mpzktd_();
    extern real xmplon_(real *);
    extern /* Subroutine */ int mpznwt_(U_fp, real *, real *);

    switch(n__) {
	case 1: goto L_mpiktd;
	}

    pi = rfpi_();
    alpha = *ylat * .9f;
    mpznwt_((U_fp)mpzktd_, ylat, &alpha);
    *x = xmplon_(xlon) * 1.535649f * cos(alpha) * 1.8f / pi;
    *y = sin(alpha) * 1.535649f;
    return 0;
/* ----------------------------------------------------------------------- */

L_mpiktd:
    pi = rfpi_();
    if (abs(*y) <= 1.3299110394845999f) {
	alpha = asin(*y / 1.535649f);
	*xlon = *x / 1.535649f / cos(alpha) / 1.8f * pi;
	if (abs(*xlon) <= pi) {
	    *ylat = asin((alpha * 2 + sin(alpha * 2)) / 2.960421f);
	    return 0;
	}
    }
    glrget_("RUNDEF", &rna, (ftnlen)6);
    *xlon = rna;
    *ylat = rna;
    return 0;
} /* mpfktd_ */

/* Subroutine */ int mpfktd_(real *xlon, real *ylat, real *x, real *y)
{
    return mpfktd_0_(0, xlon, ylat, x, y);
    }

/* Subroutine */ int mpiktd_(real *x, real *y, real *xlon, real *ylat)
{
    return mpfktd_0_(1, xlon, ylat, x, y);
    }


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static real c_b2 = 1.f;

/* ----------------------------------------------------------------------- */
/*     MAP PROJECTION (CENTRAL CONICAL)              2008-01-28 E. TOYODA */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int mpscct_0_(int n__, real *ylat0, real *xlon, real *ylat, 
	real *x, real *y)
{
    /* Initialized data */

    static real phi0 = .7854f;
    static real r0 = 1.f;
    static real n = .7071f;
    static real s = 1.f;

    /* Builtin functions */
    double atan(doublereal), tan(doublereal), sin(doublereal), r_sign(real *, 
	    real *), cos(doublereal), sqrt(doublereal), atan2(doublereal, 
	    doublereal);

    /* Local variables */
    static real r__, xx, rna;
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen);
    extern real xmplon_(real *);

    switch(n__) {
	case 1: goto L_mpfcct;
	case 2: goto L_mpicct;
	}

    phi0 = abs(*ylat0);
    r0 = tan(atan(1.f) * 4.f * .5f - phi0);
    n = sin(phi0);
    s = r_sign(&c_b2, ylat0);
    return 0;

L_mpfcct:
    xx = xmplon_(xlon);
    r__ = r0 + tan(phi0 - s * *ylat);
    *x = r__ * sin(n * xx);
    *y = -s * r__ * cos(n * xx);
    return 0;
/* ----------------------------------------------------------------------- */

L_mpicct:
    r__ = sqrt(*x * *x + *y * *y);
    if (r__ == 0.f) {
	*xlon = 0.f;
    } else {
	*xlon = atan2(*x, -s * *y) / n;
    }
    *ylat = s * (phi0 - atan(r__ - r0));
    glrget_("RUNDEF", &rna, (ftnlen)6);
    *xlon = rna;
    *ylat = rna;
    return 0;
} /* mpscct_ */

/* Subroutine */ int mpscct_(real *ylat0)
{
    return mpscct_0_(0, ylat0, (real *)0, (real *)0, (real *)0, (real *)0);
    }

/* Subroutine */ int mpfcct_(real *xlon, real *ylat, real *x, real *y)
{
    return mpscct_0_(1, (real *)0, xlon, ylat, x, y);
    }

/* Subroutine */ int mpicct_(real *x, real *y, real *xlon, real *ylat)
{
    return mpscct_0_(2, (real *)0, xlon, ylat, x, y);
    }


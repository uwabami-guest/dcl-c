/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     MAP PROJECTION (PTOLEMAIC 2ND)                2007-11-19 E. TOYODA */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int mpfpt2_0_(int n__, real *xlon, real *ylat, real *x, real 
	*y)
{
    /* System generated locals */
    real r__1, r__2, r__3;

    /* Builtin functions */
    double atan(doublereal), cos(doublereal), sin(doublereal);
    integer s_wsfe(cilist *), do_fio(integer *, char *, ftnlen), e_wsfe(void);
    double sqrt(doublereal), acos(doublereal), atan2(doublereal, doublereal);

    /* Local variables */
    static real r__, c1, c2, r0, r1, r2, r3, x1, y1, x2, y2, x3, y3, xc, yc, 
	    xx, xc1, th1, th2, th3, yc1, xc2, yc2, det, rna, rho, r0rho, 
	    alpha;
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen);
    extern real xmplon_(real *);

    /* Fortran I/O blocks */
    static cilist io___6 = { 0, 6, 0, "(a,\"(\",2f12.7,\")\")", 0 };
    static cilist io___11 = { 0, 6, 0, "(a,\"(\",2f12.7,\")\")", 0 };
    static cilist io___16 = { 0, 6, 0, "(a,\"(\",2f12.7,\")\")", 0 };
    static cilist io___18 = { 0, 6, 0, "(a,f12.7)", 0 };
    static cilist io___21 = { 0, 6, 0, "(a,\"(\",2f12.7,\")\")", 0 };
    static cilist io___28 = { 0, 6, 0, "(a,\"(\",2f12.7,\")\")", 0 };
    static cilist io___32 = { 0, 6, 0, "(a,3f12.7)", 0 };
    static cilist io___35 = { 0, 6, 0, "(a,f12.7)", 0 };


    switch(n__) {
	case 1: goto L_mpipt2;
	}

    xx = xmplon_(xlon);
    r1 = 63.f * (atan(1.f) * 4.f) / 180.f + atan(1.f) * 2.f;
    th1 = cos(63.f * (atan(1.f) * 4.f) / 180.f) * xx / (atan(1.f) * 4.f - 
	    63.f * (atan(1.f) * 4.f) / 180.f);
    x1 = r1 * sin(th1);
    y1 = atan(1.f) * 4.f - r1 * cos(th1);
    s_wsfe(&io___6);
    do_fio(&c__1, "x1,y1=", (ftnlen)6);
    do_fio(&c__1, (char *)&x1, (ftnlen)sizeof(real));
    do_fio(&c__1, (char *)&y1, (ftnlen)sizeof(real));
    e_wsfe();
    r2 = 23.833333333333332f * (atan(1.f) * 4.f) / 180.f + atan(1.f) * 2.f;
    th2 = cos(23.833333333333332f * (atan(1.f) * 4.f) / 180.f) * xx / (atan(
	    1.f) * 4.f - 23.833333333333332f * (atan(1.f) * 4.f) / 180.f);
    x2 = r2 * sin(th2);
    y2 = atan(1.f) * 4.f - r2 * cos(th2);
    s_wsfe(&io___11);
    do_fio(&c__1, "x2,y2=", (ftnlen)6);
    do_fio(&c__1, (char *)&x2, (ftnlen)sizeof(real));
    do_fio(&c__1, (char *)&y2, (ftnlen)sizeof(real));
    e_wsfe();
    r3 = -16.416666666666668f * (atan(1.f) * 4.f) / 180.f + atan(1.f) * 2.f;
    th3 = cos(-16.416666666666668f * (atan(1.f) * 4.f) / 180.f) * xx / (atan(
	    1.f) * 4.f - -16.416666666666668f * (atan(1.f) * 4.f) / 180.f);
    x3 = r3 * sin(th3);
    y3 = atan(1.f) * 4.f - r3 * cos(th3);
    s_wsfe(&io___16);
    do_fio(&c__1, "x3,y3=", (ftnlen)6);
    do_fio(&c__1, (char *)&x3, (ftnlen)sizeof(real));
    do_fio(&c__1, (char *)&y3, (ftnlen)sizeof(real));
    e_wsfe();
    det = (y1 - y2) * (x2 - x3) - (x2 - x1) * (y3 - y2);
    s_wsfe(&io___18);
    do_fio(&c__1, "det:", (ftnlen)4);
    do_fio(&c__1, (char *)&det, (ftnlen)sizeof(real));
    e_wsfe();
    c1 = ((x2 - x3) * (x3 - x1) + (y2 - y3) * (y3 - y1)) * .5f / det;
    c2 = ((x1 - x2) * (x3 - x1) + (y1 - y2) * (y3 - y1)) * .5f / det;
    s_wsfe(&io___21);
    do_fio(&c__1, "c1,c2=", (ftnlen)6);
    do_fio(&c__1, (char *)&c1, (ftnlen)sizeof(real));
    do_fio(&c__1, (char *)&c2, (ftnlen)sizeof(real));
    e_wsfe();
    xc1 = (x1 + x2) * .5f + c1 * (y1 - y2);
    yc1 = (y1 + y2) * .5f + c1 * (x2 - x1);
    xc2 = (x2 + x3) * .5f + c2 * (y2 - y3);
    yc2 = (y2 + y3) * .5f + c2 * (x3 - x2);
    xc = (xc1 + xc2) * .5f;
    yc = (yc1 + yc2) * .5f;
    s_wsfe(&io___28);
    do_fio(&c__1, "center=", (ftnlen)7);
    do_fio(&c__1, (char *)&xc, (ftnlen)sizeof(real));
    do_fio(&c__1, (char *)&yc, (ftnlen)sizeof(real));
    e_wsfe();
/* Computing 2nd power */
    r__1 = x2 - xc;
/* Computing 2nd power */
    r__2 = y2 - yc;
    r__ = sqrt(r__1 * r__1 + r__2 * r__2);
    r0 = atan(1.f) * 4.f - *ylat;
/* Computing 2nd power */
    r__1 = xc;
/* Computing 2nd power */
    r__2 = atan(1.f) * 4.f - yc;
    rho = sqrt(r__1 * r__1 + r__2 * r__2);
    s_wsfe(&io___32);
    do_fio(&c__1, "r,r0,rho=", (ftnlen)9);
    do_fio(&c__1, (char *)&r__, (ftnlen)sizeof(real));
    do_fio(&c__1, (char *)&r0, (ftnlen)sizeof(real));
    do_fio(&c__1, (char *)&rho, (ftnlen)sizeof(real));
    e_wsfe();
    if (rho > r0 + r__) {
	glrget_("RUNDEF", &rna, (ftnlen)6);
	*x = rna;
	*y = rna;
	return 0;
    }
/* Computing 2nd power */
    r__1 = r__;
/* Computing 2nd power */
    r__2 = rho;
/* Computing 2nd power */
    r__3 = r0;
    alpha = acos((-(r__1 * r__1) + r__2 * r__2 + r__3 * r__3) / (rho * 2.f * 
	    r0));
    if (atan2(x2, -y2) < atan2(xc, -yc)) {
	alpha = -alpha;
    }
    s_wsfe(&io___35);
    do_fio(&c__1, "alpha=", (ftnlen)6);
    do_fio(&c__1, (char *)&alpha, (ftnlen)sizeof(real));
    e_wsfe();
    r0rho = r0 / rho;
    *x = r0rho * (cos(alpha) * xc - sin(alpha) * (yc - atan(1.f) * 4.f));
    *y = r0rho * (sin(alpha) * xc + cos(alpha) * (yc - atan(1.f) * 4.f)) + 
	    atan(1.f) * 4.f;
    return 0;
/* ----------------------------------------------------------------------- */

L_mpipt2:
    *xlon = *x;
    *ylat = *y;
    if (abs(*xlon) <= atan(1.f) * 4.f) {
	return 0;
    }
    glrget_("RUNDEF", &rna, (ftnlen)6);
    *xlon = rna;
    *ylat = rna;
    return 0;
} /* mpfpt2_ */

/* Subroutine */ int mpfpt2_(real *xlon, real *ylat, real *x, real *y)
{
    return mpfpt2_0_(0, xlon, ylat, x, y);
    }

/* Subroutine */ int mpipt2_(real *x, real *y, real *xlon, real *ylat)
{
    return mpfpt2_0_(1, xlon, ylat, x, y);
    }


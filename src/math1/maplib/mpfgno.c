/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     MAP PROJECTION (GNOMONIC)                     2007-11-12 E.TOYODA */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int mpfgno_0_(int n__, real *xlon, real *ylat, real *x, real 
	*y)
{
    /* System generated locals */
    real r__1, r__2, r__3;

    /* Builtin functions */
    double atan(doublereal), tan(doublereal), sin(doublereal), cos(doublereal)
	    , sqrt(doublereal), atan2(doublereal, doublereal);

    /* Local variables */
    static real r__, th;

    switch(n__) {
	case 1: goto L_mpigno;
	}

/* Computing MIN */
/* Computing MAX */
    r__3 = atan(1.f) * 2.f - *ylat;
    r__1 = max(r__3,0.f), r__2 = atan(1.f) * 2.f - .001f;
    th = min(r__1,r__2);
    r__ = tan(th);
    *x = r__ * sin(*xlon);
    *y = -r__ * cos(*xlon);
    return 0;
/* ----------------------------------------------------------------------- */

L_mpigno:
    r__ = sqrt(*x * *x + *y * *y);
    if (r__ == 0.f) {
	*xlon = 0.f;
	*ylat = atan(1.f) * 2.f;
	return 0;
    } else {
	*xlon = atan2(*x, -(*y));
	*ylat = atan(1.f) * 2.f - atan(r__);
	return 0;
    }
    return 0;
} /* mpfgno_ */

/* Subroutine */ int mpfgno_(real *xlon, real *ylat, real *x, real *y)
{
    return mpfgno_0_(0, xlon, ylat, x, y);
    }

/* Subroutine */ int mpigno_(real *x, real *y, real *xlon, real *ylat)
{
    return mpfgno_0_(1, xlon, ylat, x, y);
    }


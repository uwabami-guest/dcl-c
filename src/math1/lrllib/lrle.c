/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     LRLE */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
logical lrle_(real *x, real *y)
{
    /* System generated locals */
    logical ret_val;

    /* Local variables */
    extern logical lrle0_(real *, real *), lrle1_(real *, real *);
    static logical lepsl;
    extern /* Subroutine */ int gllget_(char *, logical *, ftnlen);

    gllget_("LEPSL", &lepsl, (ftnlen)5);
    if (lepsl) {
	ret_val = lrle1_(x, y);
    } else {
	ret_val = lrle0_(x, y);
    }
    return ret_val;
} /* lrle_ */


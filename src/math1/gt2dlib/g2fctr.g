/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     G2FCTR */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int g2fctr_0_(int n__, real *ux, real *uy, real *cx, real *
	cy, logical *lini, real *cxmine, real *cxmaxe, real *cymine, real *
	cymaxe, integer *nx, integer *ny, real *uxa, real *uya, real *cxa, 
	real *cya)
{
    /* Initialized data */

    static logical linit = FALSE_;

    /* System generated locals */
    integer cxa_dim1, cxa_offset, cya_dim1, cya_offset, i__1, i__2;

    /* Local variables */
    static integer i__, j;
    static real p, q;
    static integer ii, jj;
    static real cx11, cx21, cx12, cx22, cy11, cy21, cy12, cy22;
    static integer nxs, nys;
    static real uxs[@MAXNGRID], uys[@MAXNGRID];
    extern real rmin_(real *, integer *, integer *), rmax_(real *, integer *, 
	    integer *);
    extern /* Subroutine */ int g2fbl2_(real *, real *, real *, real *, real *
	    , real *, real *, real *, real *, real *, real *, real *), 
	    g2ibl2_(real *, real *, real *, real *, real *, real *, real *, 
	    real *, real *, real *, real *, real *);
    static real cxmin, cxmax, cymin, cymax;
    extern /* Subroutine */ int g2qgrd_(integer *, integer *, real *, real *, 
	    real *, real *, real *, real *, real *, real *), g2sgrd_(real *, 
	    integer *, integer *, integer *, integer *, real *, real *, real *
	    , real *);
    extern logical lg2inq_(real *, real *, real *, real *, real *, real *, 
	    real *, real *, real *, real *);
    extern integer iblkge_(real *, integer *, real *);
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), glrget_(char *, real *, ftnlen);
    static real rundef;
    static integer iuxinc, iuyinc;

    /* Parameter adjustments */
    if (uxa) {
	--uxa;
	}
    if (uya) {
	--uya;
	}
    if (cxa) {
	cxa_dim1 = *nx;
	cxa_offset = 1 + cxa_dim1;
	cxa -= cxa_offset;
	}
    if (cya) {
	cya_dim1 = *nx;
	cya_offset = 1 + cya_dim1;
	cya -= cya_offset;
	}

    /* Function Body */
    switch(n__) {
	case 1: goto L_g2ictr;
	case 2: goto L_g2qcti;
	case 3: goto L_g2qctm;
	case 4: goto L_g2sctr;
	}

    if (! linit) {
	msgdmp_("E", "G2FCTR", "NOT YET INITIALZED", (ftnlen)1, (ftnlen)6, (
		ftnlen)18);
    }
    ii = iblkge_(uxs, &nxs, ux);
/* Computing MIN */
    i__1 = max(1,ii), i__2 = nxs - 1;
    i__ = min(i__1,i__2);
    if (ii == nxs && uxs[nxs - 1] == *ux) {
	ii = nxs - 1;
    }
    jj = iblkge_(uys, &nys, uy);
/* Computing MIN */
    i__1 = max(1,jj), i__2 = nys - 1;
    j = min(i__1,i__2);
    if (jj == nys && uys[nys - 1] == *uy) {
	jj = nys - 1;
    }
    g2qgrd_(&i__, &j, &cx11, &cx21, &cx12, &cx22, &cy11, &cy21, &cy12, &cy22);
    p = (*ux - uxs[i__ - 1]) / (uxs[i__] - uxs[i__ - 1]);
    q = (*uy - uys[j - 1]) / (uys[j] - uys[j - 1]);
    g2fbl2_(&p, &q, &cx11, &cx21, &cx12, &cx22, &cy11, &cy21, &cy12, &cy22, 
	    cx, cy);
    if (ii == 0 || ii == nxs || jj == 0 || jj == nys) {
	msgdmp_("W", "G2FCTR", "UX OR UY: OUT OF THE RANGE", (ftnlen)1, (
		ftnlen)6, (ftnlen)26);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_g2ictr:
    i__1 = nys - 1;
    for (j = 1; j <= i__1; ++j) {
	i__2 = nxs - 1;
	for (i__ = 1; i__ <= i__2; ++i__) {
	    g2qgrd_(&i__, &j, &cx11, &cx21, &cx12, &cx22, &cy11, &cy21, &cy12,
		     &cy22);
	    if (lg2inq_(cx, cy, &cx11, &cx21, &cx12, &cx22, &cy11, &cy21, &
		    cy12, &cy22)) {
		g2ibl2_(cx, cy, &cx11, &cx21, &cx12, &cx22, &cy11, &cy21, &
			cy12, &cy22, &p, &q);
		*ux = p * (uxs[i__] - uxs[i__ - 1]) + uxs[i__ - 1];
		*uy = q * (uys[j] - uys[j - 1]) + uys[j - 1];
		return 0;
	    }
/* L10: */
	}
/* L20: */
    }
    msgdmp_("E", "G2ICTR", "OUT OF THE WHOLE DOMAIN", (ftnlen)1, (ftnlen)6, (
	    ftnlen)23);
    return 0;
/* ----------------------------------------------------------------------- */
/* -- whether already initialized */

L_g2qcti:
    *lini = linit;
    return 0;
/* ----------------------------------------------------------------------- */
/* -- min & max in the destination coordinate */

L_g2qctm:
    *cxmine = cxmin;
    *cxmaxe = cxmax;
    *cymine = cymin;
    *cymaxe = cymax;
    return 0;
/* ----------------------------------------------------------------------- */

L_g2sctr:
    if (*nx > @MAXNGRID) {
	msgdmp_("E", "G2SCTR", "WORKING AREA NOT ENOUGH", (ftnlen)1, (ftnlen)
		6, (ftnlen)23);
    }
    if (*ny > @MAXNGRID) {
	msgdmp_("E", "G2SCTR", "WORKING AREA NOT ENOUGH", (ftnlen)1, (ftnlen)
		6, (ftnlen)23);
    }
    if (*nx < 2) {
	msgdmp_("E", "G2SCTR", "NX MUST BE >= 2", (ftnlen)1, (ftnlen)6, (
		ftnlen)15);
    }
    if (*ny < 2) {
	msgdmp_("E", "G2SCTR", "NY MUST BE >= 2", (ftnlen)1, (ftnlen)6, (
		ftnlen)15);
    }
    nxs = *nx;
    nys = *ny;
    glrget_("RUNDEF", &rundef, (ftnlen)6);
    if (uxa[2] > uxa[1]) {
	iuxinc = 1;
	uxs[0] = uxa[1];
	i__1 = *nx;
	for (i__ = 2; i__ <= i__1; ++i__) {
	    if (uxa[i__] <= uxa[i__ - 1]) {
		msgdmp_("E", "G2SCTR", "UX IS NOT STRICTLY MONOTONIC", (
			ftnlen)1, (ftnlen)6, (ftnlen)28);
	    }
	    uxs[i__ - 1] = uxa[i__];
/* L30: */
	}
    } else {
	iuxinc = 0;
	uxs[*nx - 1] = uxa[1];
	i__1 = *nx;
	for (i__ = 2; i__ <= i__1; ++i__) {
	    if (uxa[i__] >= uxa[i__ - 1]) {
		msgdmp_("E", "G2SCTR", "UX IS NOT STRICTLY MONOTONIC", (
			ftnlen)1, (ftnlen)6, (ftnlen)28);
	    }
	    uxs[*nx - i__] = uxa[i__];
/* L40: */
	}
    }
    if (uya[2] > uya[1]) {
	iuyinc = 1;
	uys[0] = uya[1];
	i__1 = *ny;
	for (j = 2; j <= i__1; ++j) {
	    if (uya[j] <= uya[j - 1]) {
		msgdmp_("E", "G2SCTR", "UY IS NOT STRICTLY MONOTONIC*", (
			ftnlen)1, (ftnlen)6, (ftnlen)29);
	    }
	    uys[j - 1] = uya[j];
/* L50: */
	}
    } else {
	iuyinc = 0;
	uys[*ny - 1] = uya[1];
	i__1 = *ny;
	for (j = 2; j <= i__1; ++j) {
	    if (uya[j] >= uya[j - 1]) {
		msgdmp_("E", "G2SCTR", "UY IS NOT STRICTLY MONOTONIC", (
			ftnlen)1, (ftnlen)6, (ftnlen)28);
	    }
	    uys[*ny - j] = uya[j];
/* L60: */
	}
    }
    g2sgrd_(&rundef, &iuxinc, &iuyinc, nx, ny, &cxa[cxa_offset], &cya[
	    cya_offset], uxs, uys);
    if (cxa[cxa_dim1 + 1] != rundef) {
	i__1 = *nx * *ny;
	cxmin = rmin_(&cxa[cxa_offset], &i__1, &c__1);
	i__1 = *nx * *ny;
	cxmax = rmax_(&cxa[cxa_offset], &i__1, &c__1);
    } else {
	cxmin = uxs[0];
	cxmax = uxs[*nx - 1];
    }
    if (cya[cya_dim1 + 1] != rundef) {
	i__1 = *nx * *ny;
	cymin = rmin_(&cya[cya_offset], &i__1, &c__1);
	i__1 = *nx * *ny;
	cymax = rmax_(&cya[cya_offset], &i__1, &c__1);
    } else {
	cymin = uys[0];
	cymax = uys[*ny - 1];
    }
    linit = TRUE_;
    return 0;
} /* g2fctr_ */

/* Subroutine */ int g2fctr_(real *ux, real *uy, real *cx, real *cy)
{
    return g2fctr_0_(0, ux, uy, cx, cy, (logical *)0, (real *)0, (real *)0, (
	    real *)0, (real *)0, (integer *)0, (integer *)0, (real *)0, (real 
	    *)0, (real *)0, (real *)0);
    }

/* Subroutine */ int g2ictr_(real *cx, real *cy, real *ux, real *uy)
{
    return g2fctr_0_(1, ux, uy, cx, cy, (logical *)0, (real *)0, (real *)0, (
	    real *)0, (real *)0, (integer *)0, (integer *)0, (real *)0, (real 
	    *)0, (real *)0, (real *)0);
    }

/* Subroutine */ int g2qcti_(logical *lini)
{
    return g2fctr_0_(2, (real *)0, (real *)0, (real *)0, (real *)0, lini, (
	    real *)0, (real *)0, (real *)0, (real *)0, (integer *)0, (integer 
	    *)0, (real *)0, (real *)0, (real *)0, (real *)0);
    }

/* Subroutine */ int g2qctm_(real *cxmine, real *cxmaxe, real *cymine, real *
	cymaxe)
{
    return g2fctr_0_(3, (real *)0, (real *)0, (real *)0, (real *)0, (logical *
	    )0, cxmine, cxmaxe, cymine, cymaxe, (integer *)0, (integer *)0, (
	    real *)0, (real *)0, (real *)0, (real *)0);
    }

/* Subroutine */ int g2sctr_(integer *nx, integer *ny, real *uxa, real *uya, 
	real *cxa, real *cya)
{
    return g2fctr_0_(4, (real *)0, (real *)0, (real *)0, (real *)0, (logical *
	    )0, (real *)0, (real *)0, (real *)0, (real *)0, nx, ny, uxa, uya, 
	    cxa, cya);
    }


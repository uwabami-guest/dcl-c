/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     G2FBLI */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/*     G2FBLI  Bilinear interpolation: */
/*             Interpolation if 0<=P<=1 and 0<=Q<=1. */
/*             Extraporation otherwise. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int g2fbli_(real *p, real *q, real *z00, real *z10, real *
	z01, real *z11, real *z__)
{
    *z__ = (1 - *p) * (1 - *q) * *z00 + *p * (1 - *q) * *z10 + (1 - *p) * *q *
	     *z01 + *p * *q * *z11;
    return 0;
} /* g2fbli_ */


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static real c_b2 = 0.f;
static real c_b5 = 1.f;

/* ----------------------------------------------------------------------- */
/*     LG2INQ */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/*     LG2INQ  whether included in a convex quadrilateral */
/* ----------------------------------------------------------------------- */
logical lg2inq_(real *cx, real *cy, real *cx00, real *cx10, real *cx01, real *
	cx11, real *cy00, real *cy10, real *cy01, real *cy11)
{
    /* System generated locals */
    real r__1;
    logical ret_val;

    /* Local variables */
    static real a, b, c__, d__, s, t, det;
    extern logical lrge1_(real *, real *), lrle1_(real *, real *), lreq1_(
	    real *, real *);

    a = *cx10 - *cx00;
    b = *cx01 - *cx00;
    c__ = *cy10 - *cy00;
    d__ = *cy01 - *cy00;
    det = a * d__ - b * c__;
    if (lreq1_(&det, &c_b2)) {
	ret_val = FALSE_;
	return ret_val;
    } else {
	s = (d__ * (*cx - *cx00) - b * (*cy - *cy00)) / det;
	t = (-c__ * (*cx - *cx00) + a * (*cy - *cy00)) / det;
	r__1 = s + t;
	if (lrge1_(&s, &c_b2) && lrge1_(&t, &c_b2) && lrle1_(&r__1, &c_b5)) {
	    ret_val = TRUE_;
	    return ret_val;
	}
    }
    a = *cx10 - *cx11;
    b = *cx01 - *cx11;
    c__ = *cy10 - *cy11;
    d__ = *cy01 - *cy11;
    det = a * d__ - b * c__;
    if (lreq1_(&det, &c_b2)) {
	ret_val = FALSE_;
	return ret_val;
    } else {
	s = (d__ * (*cx - *cx11) - b * (*cy - *cy11)) / det;
	t = (-c__ * (*cx - *cx11) + a * (*cy - *cy11)) / det;
	r__1 = s + t;
	if (lrge1_(&s, &c_b2) && lrge1_(&t, &c_b2) && lrle1_(&r__1, &c_b5)) {
	    ret_val = TRUE_;
	    return ret_val;
	}
    }
    ret_val = FALSE_;
    return ret_val;
} /* lg2inq_ */


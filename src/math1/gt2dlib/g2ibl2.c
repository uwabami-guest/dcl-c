/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static real c_b2 = 0.f;
static real c_b18 = 1.f;

/* ----------------------------------------------------------------------- */
/*     G2IBL2 */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/*     G2IBL2  Inverse of the bilinear interpolation of 2 params */
/*             (see G2FBL2) */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int g2ibl2_(real *x, real *y, real *x00, real *x10, real *
	x01, real *x11, real *y00, real *y10, real *y01, real *y11, real *p, 
	real *q)
{
    /* System generated locals */
    real r__1, r__2;

    /* Builtin functions */
    double sqrt(doublereal);

    /* Local variables */
    static real a, b, c__, d__, e, f, g, h__, p1, p2, sq, det;
    extern logical lrge1_(real *, real *), lrle1_(real *, real *), lreq1_(
	    real *, real *);
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen);

    a = *x00 - *x10 - *x01 + *x11;
    e = *y00 - *y10 - *y01 + *y11;
    if (lreq1_(&a, &c_b2) && lreq1_(&e, &c_b2)) {
	b = -(*x00) + *x10;
	c__ = -(*x00) + *x01;
	d__ = *x00 - *x;
	f = -(*y00) + *y10;
	g = -(*y00) + *y01;
	h__ = *y00 - *y;
	det = b * g - c__ * f;
	if (lreq1_(&det, &c_b2)) {
	    msgdmp_("W", "G2IBLI", "SOLUTION NON EXSISTENT", (ftnlen)1, (
		    ftnlen)6, (ftnlen)22);
	    *p = 0.f;
	    *q = 0.f;
	} else {
	    *p = (-g * d__ + c__ * h__) / det;
	    *q = (f * d__ - b * h__) / det;
	}
    } else if (lreq1_(&a, &c_b2)) {
	b = -(*x00) + *x10;
	c__ = -(*x00) + *x01;
	d__ = *x00 - *x;
	f = (-(*y00) + *y10) / e;
	g = (-(*y00) + *y01) / e;
	h__ = (*y00 - *y) / e;
	if (lreq1_(&b, &c_b2) && lreq1_(&c__, &c_b2)) {
	    msgdmp_("W", "G2IBLI", "SOLUTION NON EXSISTENT", (ftnlen)1, (
		    ftnlen)6, (ftnlen)22);
	    *p = 0.f;
	    *q = 0.f;
	} else if (lreq1_(&b, &c_b2)) {
	    *q = -d__ / c__;
	    *p = -(g * *q + h__) / (*q + f);
	} else if (lreq1_(&c__, &c_b2)) {
	    *p = -d__ / b;
	    *q = -(f * *p + h__) / (*p + g);
	} else {
/* Computing 2nd power */
	    r__1 = c__ * f - b * g - d__;
	    sq = r__1 * r__1 + b * 4 * (c__ * h__ - d__ * g);
	    if (lrle1_(&sq, &c_b2)) {
		sq = 0.f;
	    }
	    p1 = (c__ * f - b * g - d__ + sqrt(sq)) / (b * 2);
	    p2 = (c__ * f - b * g - d__ - sqrt(sq)) / (b * 2);
	    if (lrge1_(&p1, &c_b2) && lrle1_(&p1, &c_b18)) {
		*p = p1;
	    } else if (lrge1_(&p2, &c_b2) && lrle1_(&p2, &c_b18)) {
		*p = p2;
	    } else {
		msgdmp_("W", "G2IBLI", "NO SOLUTION IN THE DOMAIN", (ftnlen)1,
			 (ftnlen)6, (ftnlen)25);
		if (abs(p1) < abs(p2)) {
		    *p = p1;
		} else {
		    *p = p2;
		}
	    }
	    *q = -(b * *p + d__) / c__;
	}
    } else if (lreq1_(&e, &c_b2)) {
	b = (-(*x00) + *x10) / a;
	c__ = (-(*x00) + *x01) / a;
	d__ = (*x00 - *x) / a;
	f = -(*y00) + *y10;
	g = -(*y00) + *y01;
	h__ = *y00 - *y;
	if (lreq1_(&f, &c_b2) && lreq1_(&g, &c_b2)) {
	    msgdmp_("W", "G2IBLI", "SOLUTION NON EXSISTENT", (ftnlen)1, (
		    ftnlen)6, (ftnlen)22);
	    *p = 0.f;
	    *q = 0.f;
	} else if (lreq1_(&f, &c_b2)) {
	    *q = -h__ / g;
	    *p = -(c__ * *q + d__) / (*q + b);
	} else if (lreq1_(&g, &c_b2)) {
	    *p = -h__ / f;
	    *q = -(b * *p + d__) / (*p + c__);
	} else {
/* Computing 2nd power */
	    r__1 = g * b - f * c__ - h__;
	    sq = r__1 * r__1 + f * 4 * (g * d__ - h__ * c__);
	    if (lrle1_(&sq, &c_b2)) {
		sq = 0.f;
	    }
	    p1 = (g * b - f * c__ - h__ + sqrt(sq)) / (f * 2);
	    p2 = (g * b - f * c__ - h__ - sqrt(sq)) / (f * 2);
	    if (lrge1_(&p1, &c_b2) && lrle1_(&p1, &c_b18)) {
		*p = p1;
	    } else if (lrge1_(&p2, &c_b2) && lrle1_(&p2, &c_b18)) {
		*p = p2;
	    } else {
		msgdmp_("W", "G2IBLI", "NO SOLUTION IN THE DOMAIN", (ftnlen)1,
			 (ftnlen)6, (ftnlen)25);
		if (abs(p1) < abs(p2)) {
		    *p = p1;
		} else {
		    *p = p2;
		}
	    }
	    *q = -(f * *p + h__) / g;
	}
    } else {
	b = (-(*x00) + *x10) / a;
	c__ = (-(*x00) + *x01) / a;
	d__ = (*x00 - *x) / a;
	f = (-(*y00) + *y10) / e;
	g = (-(*y00) + *y01) / e;
	h__ = (*y00 - *y) / e;
	r__1 = b - f;
	r__2 = c__ - g;
	if (lreq1_(&r__1, &c_b2) && lreq1_(&r__2, &c_b2)) {
	    msgdmp_("W", "G2IBLI", "SOLUTION NON EXSISTENT", (ftnlen)1, (
		    ftnlen)6, (ftnlen)22);
	    *p = 0.f;
	    *q = 0.f;
	} else /* if(complicated condition) */ {
	    r__1 = b - f;
	    if (lreq1_(&r__1, &c_b2)) {
		*q = (-d__ + h__) / (c__ - g);
		*p = -(c__ * *q + d__) / (*q + b);
	    } else /* if(complicated condition) */ {
		r__1 = c__ - g;
		if (lreq1_(&r__1, &c_b2)) {
		    *p = -(d__ - h__) / (b - f);
		    *q = -(b * *p + d__) / (*p + c__);
		} else {
/* Computing 2nd power */
		    r__1 = c__ * f - b * g - d__ + h__;
		    sq = r__1 * r__1 + (b - f) * 4 * (c__ * h__ - d__ * g);
		    if (lrle1_(&sq, &c_b2)) {
			sq = 0.f;
		    }
		    p1 = (c__ * f - b * g - d__ + h__ + sqrt(sq)) / ((b - f) *
			     2);
		    p2 = (c__ * f - b * g - d__ + h__ - sqrt(sq)) / ((b - f) *
			     2);
		    if (lrge1_(&p1, &c_b2) && lrle1_(&p1, &c_b18)) {
			*p = p1;
		    } else if (lrge1_(&p2, &c_b2) && lrle1_(&p2, &c_b18)) {
			*p = p2;
		    } else {
			msgdmp_("W", "G2IBLI", "NO SOLUTION IN THE DOMAIN", (
				ftnlen)1, (ftnlen)6, (ftnlen)25);
			if (abs(p1) < abs(p2)) {
			    *p = p1;
			} else {
			    *p = p2;
			}
		    }
		    *q = -((b - f) * *p + (d__ - h__)) / (c__ - g);
		}
	    }
	}
    }
    return 0;
} /* g2ibl2_ */


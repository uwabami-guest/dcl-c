/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;
static real c_b12 = 10.f;

/* ----------------------------------------------------------------------- */
/*     GNSBLK / GNQBLK / GNLT / GNLE / GNGT / GNGE */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int gnsblk_0_(int n__, real *xb, integer *nb, real *rx, real 
	*bx, integer *ip)
{
    /* Initialized data */

    static integer nbz = 11;
    static real xbz[20] = { 1.f,1.2f,1.5f,2.f,2.5f,3.f,4.f,5.f,6.f,8.f,10.f };

    /* System generated locals */
    integer i__1, i__2;

    /* Builtin functions */
    double r_lg10(real *), pow_ri(real *, integer *), r_sign(real *, real *);

    /* Local variables */
    static integer ib;
    static real ps, sx, tx;
    static integer ibd, ibe;
    extern integer imod_(integer *, integer *), igus_(real *);
    extern /* Subroutine */ int vrset_(real *, real *, integer *, integer *, 
	    integer *);
    extern integer iblkge_(real *, integer *, real *), iblkle_(real *, 
	    integer *, real *), iblkgt_(real *, integer *, real *), iblklt_(
	    real *, integer *, real *);
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen);

    /* Parameter adjustments */
    if (xb) {
	--xb;
	}

    /* Function Body */
    switch(n__) {
	case 1: goto L_gnqblk;
	case 2: goto L_gnlt;
	case 3: goto L_gnle;
	case 4: goto L_gngt;
	case 5: goto L_gnge;
	}

    if (! (2 <= *nb && *nb <= 20)) {
	msgdmp_("E", "GNSBLK", "NUMBER OF BLOCKS IS INVALID.", (ftnlen)1, (
		ftnlen)6, (ftnlen)28);
    }
    if (! (xb[1] == 1.f && xb[*nb] == 10.f)) {
	msgdmp_("E", "GNSBLK", "XB(1).NE.1 OR XB(NB).NE.10.", (ftnlen)1, (
		ftnlen)6, (ftnlen)27);
    }
    nbz = *nb;
    vrset_(&xb[1], xbz, &nbz, &c__1, &c__1);
    return 0;
/* ----------------------------------------------------------------------- */

L_gnqblk:
    *nb = nbz;
    vrset_(xbz, &xb[1], nb, &c__1, &c__1);
    return 0;
/* ----------------------------------------------------------------------- */

L_gnlt:
    if (*rx == 0.f) {
	*bx = 0.f;
	*ip = 0;
    } else {
	sx = abs(*rx);
	ps = r_lg10(&sx);
	*ip = igus_(&ps);
	i__1 = -(*ip);
	tx = sx * pow_ri(&c_b12, &i__1);
	if (*rx > 0.f) {
	    ib = iblkgt_(xbz, &nbz, &tx);
	} else {
	    ib = iblklt_(xbz, &nbz, &tx);
	}
	i__1 = ib - 1;
	i__2 = nbz - 1;
	ibd = imod_(&i__1, &i__2) + 1;
	ibe = (ib - ibd) / (nbz - 1);
	*bx = r_sign(&xbz[ibd - 1], rx);
	*ip += ibe;
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_gnle:
    if (*rx == 0.f) {
	*bx = 0.f;
	*ip = 0;
    } else {
	sx = abs(*rx);
	ps = r_lg10(&sx);
	*ip = igus_(&ps);
	i__1 = -(*ip);
	tx = sx * pow_ri(&c_b12, &i__1);
	if (*rx > 0.f) {
	    ib = iblkge_(xbz, &nbz, &tx);
	} else {
	    ib = iblkle_(xbz, &nbz, &tx);
	}
	i__1 = ib - 1;
	i__2 = nbz - 1;
	ibd = imod_(&i__1, &i__2) + 1;
	ibe = (ib - ibd) / (nbz - 1);
	*bx = r_sign(&xbz[ibd - 1], rx);
	*ip += ibe;
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_gngt:
    if (*rx == 0.f) {
	*bx = 0.f;
	*ip = 0;
    } else {
	sx = abs(*rx);
	ps = r_lg10(&sx);
	*ip = igus_(&ps);
	i__1 = -(*ip);
	tx = sx * pow_ri(&c_b12, &i__1);
	if (*rx > 0.f) {
	    ib = iblklt_(xbz, &nbz, &tx);
	} else {
	    ib = iblkgt_(xbz, &nbz, &tx);
	}
	i__1 = ib - 1;
	i__2 = nbz - 1;
	ibd = imod_(&i__1, &i__2) + 1;
	ibe = (ib - ibd) / (nbz - 1);
	*bx = r_sign(&xbz[ibd - 1], rx);
	*ip += ibe;
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_gnge:
    if (*rx == 0.f) {
	*bx = 0.f;
	*ip = 0;
    } else {
	sx = abs(*rx);
	ps = r_lg10(&sx);
	*ip = igus_(&ps);
	i__1 = -(*ip);
	tx = sx * pow_ri(&c_b12, &i__1);
	if (*rx > 0.f) {
	    ib = iblkle_(xbz, &nbz, &tx);
	} else {
	    ib = iblkge_(xbz, &nbz, &tx);
	}
	i__1 = ib - 1;
	i__2 = nbz - 1;
	ibd = imod_(&i__1, &i__2) + 1;
	ibe = (ib - ibd) / (nbz - 1);
	*bx = r_sign(&xbz[ibd - 1], rx);
	*ip += ibe;
    }
    return 0;
} /* gnsblk_ */

/* Subroutine */ int gnsblk_(real *xb, integer *nb)
{
    return gnsblk_0_(0, xb, nb, (real *)0, (real *)0, (integer *)0);
    }

/* Subroutine */ int gnqblk_(real *xb, integer *nb)
{
    return gnsblk_0_(1, xb, nb, (real *)0, (real *)0, (integer *)0);
    }

/* Subroutine */ int gnlt_(real *rx, real *bx, integer *ip)
{
    return gnsblk_0_(2, (real *)0, (integer *)0, rx, bx, ip);
    }

/* Subroutine */ int gnle_(real *rx, real *bx, integer *ip)
{
    return gnsblk_0_(3, (real *)0, (integer *)0, rx, bx, ip);
    }

/* Subroutine */ int gngt_(real *rx, real *bx, integer *ip)
{
    return gnsblk_0_(4, (real *)0, (integer *)0, rx, bx, ip);
    }

/* Subroutine */ int gnge_(real *rx, real *bx, integer *ip)
{
    return gnsblk_0_(5, (real *)0, (integer *)0, rx, bx, ip);
    }


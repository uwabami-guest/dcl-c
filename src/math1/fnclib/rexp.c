/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     REXP */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
real rexp_(real *rx, integer *ib, integer *ie)
{
    /* System generated locals */
    integer i__1;
    real ret_val;

    /* Local variables */
    static integer i__;
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen);

    if (*ib == 0) {
	msgdmp_("E", "REXP  ", "IB .EQ. 0.", (ftnlen)1, (ftnlen)6, (ftnlen)10)
		;
    }
    ret_val = *rx;
    i__1 = abs(*ie);
    for (i__ = 1; i__ <= i__1; ++i__) {
	if (*ie > 0) {
	    ret_val *= *ib;
	} else {
	    ret_val /= *ib;
	}
/* L10: */
    }
    return ret_val;
} /* rexp_ */


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     RMOD */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
real rmod_(real *rx, real *rd)
{
    /* System generated locals */
    real ret_val, r__1;

    /* Builtin functions */
    double r_mod(real *, real *);

    /* Local variables */
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen);

    if (*rd <= 0.f) {
	msgdmp_("E", "RMOD  ", "RD .LE. 0.", (ftnlen)1, (ftnlen)6, (ftnlen)10)
		;
    }
    r__1 = r_mod(rx, rd) + *rd;
    ret_val = r_mod(&r__1, rd);
    return ret_val;
} /* rmod_ */


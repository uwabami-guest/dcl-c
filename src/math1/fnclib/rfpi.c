/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
real rfpi_(void)
{
    /* Initialized data */

    static logical lfirst = TRUE_;

    /* System generated locals */
    real ret_val;

    /* Builtin functions */
    double atan(doublereal);

    /* Local variables */
    static real pi;

    if (lfirst) {
	pi = atan(1.f) * 4.f;
	lfirst = FALSE_;
    }
    ret_val = pi;
    return ret_val;
} /* rfpi_ */


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     VRFNB */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int vrfnb_(real *rx, real *ry, real *rz, integer *n, integer 
	*jx, integer *jy, integer *jz, R_fp rfnb)
{
    static logical lmiss;
    extern /* Subroutine */ int vrfnb0_(real *, real *, real *, integer *, 
	    integer *, integer *, integer *, R_fp), vrfnb1_(real *, real *, 
	    real *, integer *, integer *, integer *, integer *, R_fp), 
	    gllget_(char *, logical *, ftnlen);

    /* Parameter adjustments */
    --rz;
    --ry;
    --rx;

    /* Function Body */
    gllget_("LMISS", &lmiss, (ftnlen)5);
    if (lmiss) {
	vrfnb1_(&rx[1], &ry[1], &rz[1], n, jx, jy, jz, (R_fp)rfnb);
    } else {
	vrfnb0_(&rx[1], &ry[1], &rz[1], n, jx, jy, jz, (R_fp)rfnb);
    }
    return 0;
} /* vrfnb_ */


/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;
static integer c__12 = 12;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Initialized data */

    static real rx[12] = { 0.f,1.f,2.f,999.f,4.f,3.f,999.f,999.f,0.f,-1.f,
	    -2.f,999.f };

    /* Builtin functions */
    integer s_wsfe(cilist *), do_fio(integer *, char *, ftnlen), e_wsfe(void);

    /* Local variables */
    extern /* Subroutine */ int vrintr_(real *, integer *, integer *);

    /* Fortran I/O blocks */
    static cilist io___2 = { 0, 6, 0, "(A,12F5.0)", 0 };
    static cilist io___3 = { 0, 6, 0, "(A)", 0 };
    static cilist io___4 = { 0, 6, 0, "(A,12F5.0)", 0 };


    s_wsfe(&io___2);
    do_fio(&c__1, " LIST OF RX : ", (ftnlen)14);
    do_fio(&c__12, (char *)&rx[0], (ftnlen)sizeof(real));
    e_wsfe();
    vrintr_(rx, &c__12, &c__1);
    s_wsfe(&io___3);
    do_fio(&c__1, " AFTER CALLING VRINTR(RX,12,1)", (ftnlen)30);
    e_wsfe();
    s_wsfe(&io___4);
    do_fio(&c__1, " LIST OF RX : ", (ftnlen)14);
    do_fio(&c__12, (char *)&rx[0], (ftnlen)sizeof(real));
    e_wsfe();
    return 0;
} /* MAIN__ */

/* Main program alias */ int intr01_ () { MAIN__ (); return 0; }

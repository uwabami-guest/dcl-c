/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__12 = 12;
static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Initialized data */

    static real rx[12] = { 0.f,1.f,2.f,3.f,4.f,3.f,2.f,1.f,0.f,-1.f,-2.f,-1.f 
	    };

    /* Builtin functions */
    integer s_wsfe(cilist *), do_fio(integer *, char *, ftnlen), e_wsfe(void);

    /* Local variables */
    static integer i__;
    static real a0, ak[6], bk[6], wsave[51];
    extern /* Subroutine */ int ezfftb_(integer *, real *, real *, real *, 
	    real *, real *), ezfftf_(integer *, real *, real *, real *, real *
	    , real *), ezffti_(integer *, real *);

    /* Fortran I/O blocks */
    static cilist io___3 = { 0, 6, 0, "(A,12F5.1)", 0 };
    static cilist io___7 = { 0, 6, 0, "(A,7F8.3)", 0 };
    static cilist io___9 = { 0, 6, 0, "(A,TR8,6F8.3)", 0 };
    static cilist io___10 = { 0, 6, 0, "(A,12F5.1)", 0 };


/*     DATA      RX/ 0, 1, 2, 3, 2, 1, 0, -1, -2, -3, -2, -1/ */
    ezffti_(&c__12, wsave);
    s_wsfe(&io___3);
    do_fio(&c__1, " LIST OF RX : ", (ftnlen)14);
    do_fio(&c__12, (char *)&rx[0], (ftnlen)sizeof(real));
    e_wsfe();
    ezfftf_(&c__12, rx, &a0, ak, bk, wsave);
    s_wsfe(&io___7);
    do_fio(&c__1, " LIST OF CX : ", (ftnlen)14);
    do_fio(&c__1, (char *)&a0, (ftnlen)sizeof(real));
    for (i__ = 1; i__ <= 6; ++i__) {
	do_fio(&c__1, (char *)&ak[i__ - 1], (ftnlen)sizeof(real));
    }
    e_wsfe();
    s_wsfe(&io___9);
    do_fio(&c__1, " LIST OF SX : ", (ftnlen)14);
    for (i__ = 1; i__ <= 6; ++i__) {
	do_fio(&c__1, (char *)&bk[i__ - 1], (ftnlen)sizeof(real));
    }
    e_wsfe();
    ezfftb_(&c__12, rx, &a0, ak, bk, wsave);
    s_wsfe(&io___10);
    do_fio(&c__1, " LIST OF RX : ", (ftnlen)14);
    do_fio(&c__12, (char *)&rx[0], (ftnlen)sizeof(real));
    e_wsfe();
    return 0;
} /* MAIN__ */

/* Main program alias */ int fftl01_ () { MAIN__ (); return 0; }

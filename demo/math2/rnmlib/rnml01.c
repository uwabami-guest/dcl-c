/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;
static integer c__12 = 12;
static integer c__3 = 3;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Initialized data */

    static real rx[12] = { 0.f,10.f,20.f,30.f,40.f,30.f,20.f,10.f,0.f,-10.f,
	    -20.f,-10.f };

    /* Builtin functions */
    integer s_wsfe(cilist *), do_fio(integer *, char *, ftnlen), e_wsfe(void);

    /* Local variables */
    static real ry[12];
    extern /* Subroutine */ int vrrnm_(real *, real *, integer *, integer *, 
	    integer *, integer *);

    /* Fortran I/O blocks */
    static cilist io___2 = { 0, 6, 0, "(A,12F5.0)", 0 };
    static cilist io___4 = { 0, 6, 0, "(A)", 0 };
    static cilist io___5 = { 0, 6, 0, "(A,12F5.0)", 0 };


    s_wsfe(&io___2);
    do_fio(&c__1, " LIST OF RX : ", (ftnlen)14);
    do_fio(&c__12, (char *)&rx[0], (ftnlen)sizeof(real));
    e_wsfe();
    vrrnm_(rx, ry, &c__12, &c__1, &c__1, &c__3);
    s_wsfe(&io___4);
    do_fio(&c__1, " AFTER CALLING VRRNM(RX,RY,12,1,1,3)", (ftnlen)36);
    e_wsfe();
    s_wsfe(&io___5);
    do_fio(&c__1, " LIST OF RY : ", (ftnlen)14);
    do_fio(&c__12, (char *)&ry[0], (ftnlen)sizeof(real));
    e_wsfe();
    return 0;
} /* MAIN__ */

/* Main program alias */ int rnml01_ () { MAIN__ (); return 0; }

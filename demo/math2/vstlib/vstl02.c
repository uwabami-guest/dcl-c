/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;
static integer c__3 = 3;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Initialized data */

    static real x[12]	/* was [3][4] */ = { 1.f,3.f,-1.f,2.f,-4.f,0.f,2.f,
	    1.f,3.f,2.f,1.f,0.f };
    static real y[12]	/* was [3][4] */ = { 0.f,1.f,0.f,1.f,-2.f,1.f,3.f,0.f,
	    2.f,1.f,-1.f,0.f };

    /* Format strings */
    static char fmt_100[] = "(a,3f6.1)";
    static char fmt_110[] = "(a12,3f6.1,2(/tr12,3f6.1))";

    /* Builtin functions */
    integer s_wsfe(cilist *), do_fio(integer *, char *, ftnlen), e_wsfe(void);

    /* Local variables */
    static integer i__, j, nw2[9]	/* was [3][3] */;
    static real wz2[45]	/* was [3][3][5] */;
    extern /* Subroutine */ int vs2din_(real *, integer *, integer *, integer 
	    *, real *, real *), vs2int_(real *, integer *, integer *, integer 
	    *), vs2out_(real *, integer *, integer *, integer *);

    /* Fortran I/O blocks */
    static cilist io___3 = { 0, 6, 0, "(A)", 0 };
    static cilist io___7 = { 0, 6, 0, fmt_100, 0 };
    static cilist io___9 = { 0, 6, 0, fmt_100, 0 };
    static cilist io___10 = { 0, 6, 0, fmt_110, 0 };
    static cilist io___11 = { 0, 6, 0, fmt_110, 0 };
    static cilist io___12 = { 0, 6, 0, fmt_110, 0 };
    static cilist io___13 = { 0, 6, 0, fmt_110, 0 };
    static cilist io___14 = { 0, 6, 0, fmt_110, 0 };


    s_wsfe(&io___3);
    do_fio(&c__1, " *** TEST FOR VS2INT / VS2DIN / VS2OUT", (ftnlen)38);
    e_wsfe();
    vs2int_(wz2, nw2, &c__3, &c__3);
    for (j = 1; j <= 4; ++j) {
	s_wsfe(&io___7);
	do_fio(&c__1, " INPUT(X) = ", (ftnlen)12);
	for (i__ = 1; i__ <= 3; ++i__) {
	    do_fio(&c__1, (char *)&x[i__ + j * 3 - 4], (ftnlen)sizeof(real));
	}
	e_wsfe();
	s_wsfe(&io___9);
	do_fio(&c__1, " INPUT(Y) = ", (ftnlen)12);
	for (i__ = 1; i__ <= 3; ++i__) {
	    do_fio(&c__1, (char *)&y[i__ + j * 3 - 4], (ftnlen)sizeof(real));
	}
	e_wsfe();
	vs2din_(wz2, nw2, &c__3, &c__3, &x[j * 3 - 3], &y[j * 3 - 3]);
/* L20: */
    }
    vs2out_(wz2, nw2, &c__3, &c__3);
    s_wsfe(&io___10);
    do_fio(&c__1, " AVE. (X) = ", (ftnlen)12);
    for (j = 1; j <= 3; ++j) {
	for (i__ = 1; i__ <= 3; ++i__) {
	    do_fio(&c__1, (char *)&wz2[i__ + (j + 3) * 3 - 13], (ftnlen)
		    sizeof(real));
	}
    }
    e_wsfe();
    s_wsfe(&io___11);
    do_fio(&c__1, " AVE. (Y) = ", (ftnlen)12);
    for (j = 1; j <= 3; ++j) {
	for (i__ = 1; i__ <= 3; ++i__) {
	    do_fio(&c__1, (char *)&wz2[i__ + (j + 6) * 3 - 13], (ftnlen)
		    sizeof(real));
	}
    }
    e_wsfe();
    s_wsfe(&io___12);
    do_fio(&c__1, " VAR. (X) = ", (ftnlen)12);
    for (j = 1; j <= 3; ++j) {
	for (i__ = 1; i__ <= 3; ++i__) {
	    do_fio(&c__1, (char *)&wz2[i__ + (j + 9) * 3 - 13], (ftnlen)
		    sizeof(real));
	}
    }
    e_wsfe();
    s_wsfe(&io___13);
    do_fio(&c__1, " VAR. (Y) = ", (ftnlen)12);
    for (j = 1; j <= 3; ++j) {
	for (i__ = 1; i__ <= 3; ++i__) {
	    do_fio(&c__1, (char *)&wz2[i__ + (j + 12) * 3 - 13], (ftnlen)
		    sizeof(real));
	}
    }
    e_wsfe();
    s_wsfe(&io___14);
    do_fio(&c__1, " COV(X,Y) = ", (ftnlen)12);
    for (j = 1; j <= 3; ++j) {
	for (i__ = 1; i__ <= 3; ++i__) {
	    do_fio(&c__1, (char *)&wz2[i__ + (j + 15) * 3 - 13], (ftnlen)
		    sizeof(real));
	}
    }
    e_wsfe();
    return 0;
} /* MAIN__ */

/* Main program alias */ int vstl02_ () { MAIN__ (); return 0; }

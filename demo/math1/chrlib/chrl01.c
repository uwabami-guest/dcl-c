/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_wsfe(cilist *), do_fio(integer *, char *, ftnlen), e_wsfe(void);

    /* Local variables */
    static char chr[8], chr1[8], chr2[8], chr3[8];
    extern integer lenb_(char *, ftnlen), lenc_(char *, ftnlen), leny_(char *,
	     ftnlen), lenz_(char *, ftnlen);
    extern /* Subroutine */ int cladj_(char *, ftnlen), cradj_(char *, ftnlen)
	    , crvrs_(char *, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___2 = { 0, 6, 0, "(3A)", 0 };
    static cilist io___3 = { 0, 6, 0, "(A,I4)", 0 };
    static cilist io___4 = { 0, 6, 0, "(A,I4)", 0 };
    static cilist io___5 = { 0, 6, 0, "(A,I4)", 0 };
    static cilist io___6 = { 0, 6, 0, "(A,I4)", 0 };
    static cilist io___8 = { 0, 6, 0, "(3A)", 0 };
    static cilist io___10 = { 0, 6, 0, "(3A)", 0 };
    static cilist io___12 = { 0, 6, 0, "(3A)", 0 };


    s_copy(chr, " DENNOU ", (ftnlen)8, (ftnlen)8);
    s_wsfe(&io___2);
    do_fio(&c__1, " CHR = '", (ftnlen)8);
    do_fio(&c__1, chr, (ftnlen)8);
    do_fio(&c__1, "'", (ftnlen)1);
    e_wsfe();
    s_wsfe(&io___3);
    do_fio(&c__1, " LENB = ", (ftnlen)8);
    i__1 = lenb_(chr, (ftnlen)8);
    do_fio(&c__1, (char *)&i__1, (ftnlen)sizeof(integer));
    e_wsfe();
    s_wsfe(&io___4);
    do_fio(&c__1, " LENC = ", (ftnlen)8);
    i__1 = lenc_(chr, (ftnlen)8);
    do_fio(&c__1, (char *)&i__1, (ftnlen)sizeof(integer));
    e_wsfe();
    s_wsfe(&io___5);
    do_fio(&c__1, " LENY = ", (ftnlen)8);
    i__1 = leny_(chr, (ftnlen)8);
    do_fio(&c__1, (char *)&i__1, (ftnlen)sizeof(integer));
    e_wsfe();
    s_wsfe(&io___6);
    do_fio(&c__1, " LENZ = ", (ftnlen)8);
    i__1 = lenz_(chr, (ftnlen)8);
    do_fio(&c__1, (char *)&i__1, (ftnlen)sizeof(integer));
    e_wsfe();
    s_copy(chr1, chr, (ftnlen)8, (ftnlen)8);
    cradj_(chr1, (ftnlen)8);
    s_wsfe(&io___8);
    do_fio(&c__1, " CHR (RIGHT ADJUSTED) = '", (ftnlen)25);
    do_fio(&c__1, chr1, (ftnlen)8);
    do_fio(&c__1, "'", (ftnlen)1);
    e_wsfe();
    s_copy(chr2, chr, (ftnlen)8, (ftnlen)8);
    cladj_(chr2, (ftnlen)8);
    s_wsfe(&io___10);
    do_fio(&c__1, " CHR (LEFT  ADJUSTED) = '", (ftnlen)25);
    do_fio(&c__1, chr2, (ftnlen)8);
    do_fio(&c__1, "'", (ftnlen)1);
    e_wsfe();
    s_copy(chr3, chr, (ftnlen)8, (ftnlen)8);
    crvrs_(chr3, (ftnlen)8);
    s_wsfe(&io___12);
    do_fio(&c__1, " CHR    (REVERSED)    = '", (ftnlen)25);
    do_fio(&c__1, chr3, (ftnlen)8);
    do_fio(&c__1, "'", (ftnlen)1);
    e_wsfe();
    return 0;
} /* MAIN__ */

/* Main program alias */ int chrl01_ () { MAIN__ (); return 0; }

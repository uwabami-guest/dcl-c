/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;
static integer c__8 = 8;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Initialized data */

    static real rx[8] = { -1.f,1.f,2.f,-2.f,-3.f,-4.f,12.f,-5.f };
    static real ry[8] = { 1.f,2.f,-3.f,2.f,0.f,-7.f,8.f,-3.f };

    /* System generated locals */
    real r__1;

    /* Builtin functions */
    integer s_wsfe(cilist *), do_fio(integer *, char *, ftnlen), e_wsfe(void);

    /* Local variables */
    extern real rcor_(real *, real *, integer *, integer *, integer *), rprd_(
	    real *, real *, integer *, integer *, integer *), rcov_(real *, 
	    real *, integer *, integer *, integer *);

    /* Fortran I/O blocks */
    static cilist io___3 = { 0, 6, 0, "(A,8F8.1)", 0 };
    static cilist io___4 = { 0, 6, 0, "(A,8F8.1)", 0 };
    static cilist io___5 = { 0, 6, 0, "(A,G14.6)", 0 };
    static cilist io___6 = { 0, 6, 0, "(A,G14.6)", 0 };
    static cilist io___7 = { 0, 6, 0, "(A,G14.6)", 0 };


    s_wsfe(&io___3);
    do_fio(&c__1, " LIST OF RX : ", (ftnlen)14);
    do_fio(&c__8, (char *)&rx[0], (ftnlen)sizeof(real));
    e_wsfe();
    s_wsfe(&io___4);
    do_fio(&c__1, " LIST OF RY : ", (ftnlen)14);
    do_fio(&c__8, (char *)&ry[0], (ftnlen)sizeof(real));
    e_wsfe();
    s_wsfe(&io___5);
    do_fio(&c__1, " *** RPRD(RX,RY,8,1,1) = ", (ftnlen)25);
    r__1 = rprd_(rx, ry, &c__8, &c__1, &c__1);
    do_fio(&c__1, (char *)&r__1, (ftnlen)sizeof(real));
    e_wsfe();
    s_wsfe(&io___6);
    do_fio(&c__1, " *** RCOV(RX,RY,8,1,1) = ", (ftnlen)25);
    r__1 = rcov_(rx, ry, &c__8, &c__1, &c__1);
    do_fio(&c__1, (char *)&r__1, (ftnlen)sizeof(real));
    e_wsfe();
    s_wsfe(&io___7);
    do_fio(&c__1, " *** RCOR(RX,RY,8,1,1) = ", (ftnlen)25);
    r__1 = rcor_(rx, ry, &c__8, &c__1, &c__1);
    do_fio(&c__1, (char *)&r__1, (ftnlen)sizeof(real));
    e_wsfe();
    return 0;
} /* MAIN__ */

/* Main program alias */ int rfbl01_ () { MAIN__ (); return 0; }

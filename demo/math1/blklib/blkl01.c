/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static logical c_true = TRUE_;
static integer c__1 = 1;
static integer c__10 = 10;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Initialized data */

    static real rx[10] = { 0.f,2.f,5.f,8.f,10.f,15.f,20.f,30.f,50.f,80.f };
    static real rr[3] = { 3.f,30.f,30.00003f };

    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    integer s_wsfe(cilist *), do_fio(integer *, char *, ftnlen), e_wsfe(void);

    /* Local variables */
    static integer i__;
    extern integer iblkge_(real *, integer *, real *), iblkle_(real *, 
	    integer *, real *), iblkgt_(real *, integer *, real *), iblklt_(
	    real *, integer *, real *);
    extern /* Subroutine */ int gllset_(char *, logical *, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___4 = { 0, 6, 0, "(A,10F4.0)", 0 };
    static cilist io___5 = { 0, 6, 0, "(A,G16.7)", 0 };
    static cilist io___6 = { 0, 6, 0, "(A,I4)", 0 };
    static cilist io___7 = { 0, 6, 0, "(A,I4)", 0 };
    static cilist io___8 = { 0, 6, 0, "(A,I4)", 0 };
    static cilist io___9 = { 0, 6, 0, "(A,I4)", 0 };


    gllset_("LEPSL", &c_true, (ftnlen)5);
    for (i__ = 1; i__ <= 3; ++i__) {
	s_wsfe(&io___4);
	do_fio(&c__1, " LIST OF RX (N=10) : ", (ftnlen)21);
	do_fio(&c__10, (char *)&rx[0], (ftnlen)sizeof(real));
	e_wsfe();
	s_wsfe(&io___5);
	do_fio(&c__1, " *** RR = ", (ftnlen)10);
	do_fio(&c__1, (char *)&rr[i__ - 1], (ftnlen)sizeof(real));
	e_wsfe();
	s_wsfe(&io___6);
	do_fio(&c__1, " *** IBLKGT(RX,N,RR) = ", (ftnlen)23);
	i__1 = iblkgt_(rx, &c__10, &rr[i__ - 1]);
	do_fio(&c__1, (char *)&i__1, (ftnlen)sizeof(integer));
	e_wsfe();
	s_wsfe(&io___7);
	do_fio(&c__1, " *** IBLKGE(RX,N,RR) = ", (ftnlen)23);
	i__1 = iblkge_(rx, &c__10, &rr[i__ - 1]);
	do_fio(&c__1, (char *)&i__1, (ftnlen)sizeof(integer));
	e_wsfe();
	s_wsfe(&io___8);
	do_fio(&c__1, " *** IBLKLT(RX,N,RR) = ", (ftnlen)23);
	i__1 = iblklt_(rx, &c__10, &rr[i__ - 1]);
	do_fio(&c__1, (char *)&i__1, (ftnlen)sizeof(integer));
	e_wsfe();
	s_wsfe(&io___9);
	do_fio(&c__1, " *** IBLKLE(RX,N,RR) = ", (ftnlen)23);
	i__1 = iblkle_(rx, &c__10, &rr[i__ - 1]);
	do_fio(&c__1, (char *)&i__1, (ftnlen)sizeof(integer));
	e_wsfe();
/* L10: */
    }
    return 0;
} /* MAIN__ */

/* Main program alias */ int blkl01_ () { MAIN__ (); return 0; }

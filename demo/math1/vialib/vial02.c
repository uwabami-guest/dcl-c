/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;
static integer c__10 = 10;
static integer c_n1 = -1;
static integer c__0 = 0;
static logical c_true = TRUE_;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Initialized data */

    static integer ix[10] = { 0,1,4,6,3,-4,12,999,23,-5 };

    /* Builtin functions */
    integer s_wsfe(cilist *), do_fio(integer *, char *, ftnlen), e_wsfe(void);

    /* Local variables */
    static integer iy[10];
    extern /* Subroutine */ int iadd_(integer *, integer *, integer *, 
	    integer *), iset_(integer *, integer *, integer *, integer *), 
	    imlt_(integer *, integer *, integer *, integer *), viset_(integer 
	    *, integer *, integer *, integer *, integer *), gllset_(char *, 
	    logical *, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___2 = { 0, 6, 0, "(A,10I6)", 0 };
    static cilist io___3 = { 0, 6, 0, "(A)", 0 };
    static cilist io___5 = { 0, 6, 0, "(A)", 0 };
    static cilist io___6 = { 0, 6, 0, "(A,10I6)", 0 };
    static cilist io___7 = { 0, 6, 0, "(A)", 0 };
    static cilist io___8 = { 0, 6, 0, "(A,10I6)", 0 };
    static cilist io___9 = { 0, 6, 0, "(A)", 0 };
    static cilist io___10 = { 0, 6, 0, "(A,10I6)", 0 };
    static cilist io___11 = { 0, 6, 0, "(A)", 0 };
    static cilist io___12 = { 0, 6, 0, "(A,10I6)", 0 };
    static cilist io___13 = { 0, 6, 0, "(A)", 0 };
    static cilist io___14 = { 0, 6, 0, "(A)", 0 };
    static cilist io___15 = { 0, 6, 0, "(A,10I6)", 0 };
    static cilist io___16 = { 0, 6, 0, "(A)", 0 };
    static cilist io___17 = { 0, 6, 0, "(A,10I6)", 0 };
    static cilist io___18 = { 0, 6, 0, "(A)", 0 };
    static cilist io___19 = { 0, 6, 0, "(A,10I6)", 0 };
    static cilist io___20 = { 0, 6, 0, "(A)", 0 };
    static cilist io___21 = { 0, 6, 0, "(A,10I6)", 0 };


    s_wsfe(&io___2);
    do_fio(&c__1, " LIST OF IX : ", (ftnlen)14);
    do_fio(&c__10, (char *)&ix[0], (ftnlen)sizeof(integer));
    e_wsfe();
    s_wsfe(&io___3);
    do_fio(&c__1, " *** OPTION LMISS = .FALSE.", (ftnlen)27);
    e_wsfe();
    viset_(ix, iy, &c__10, &c__1, &c__1);
    s_wsfe(&io___5);
    do_fio(&c__1, " AFTER CALLING VISET(IX,IY,10,1,1)", (ftnlen)34);
    e_wsfe();
    s_wsfe(&io___6);
    do_fio(&c__1, " LIST OF IY : ", (ftnlen)14);
    do_fio(&c__10, (char *)&iy[0], (ftnlen)sizeof(integer));
    e_wsfe();
    iadd_(iy, &c__10, &c__1, &c__1);
    s_wsfe(&io___7);
    do_fio(&c__1, " AFTER CALLING IADD(IY,10,1,+1)", (ftnlen)31);
    e_wsfe();
    s_wsfe(&io___8);
    do_fio(&c__1, " LIST OF IY : ", (ftnlen)14);
    do_fio(&c__10, (char *)&iy[0], (ftnlen)sizeof(integer));
    e_wsfe();
    imlt_(iy, &c__10, &c__1, &c_n1);
    s_wsfe(&io___9);
    do_fio(&c__1, " AFTER CALLING IMLT(IY,10,1,-1)", (ftnlen)31);
    e_wsfe();
    s_wsfe(&io___10);
    do_fio(&c__1, " LIST OF IY : ", (ftnlen)14);
    do_fio(&c__10, (char *)&iy[0], (ftnlen)sizeof(integer));
    e_wsfe();
    iset_(iy, &c__10, &c__1, &c__0);
    s_wsfe(&io___11);
    do_fio(&c__1, " AFTER CALLING ISET(IY,10,1,0)", (ftnlen)30);
    e_wsfe();
    s_wsfe(&io___12);
    do_fio(&c__1, " LIST OF IY : ", (ftnlen)14);
    do_fio(&c__10, (char *)&iy[0], (ftnlen)sizeof(integer));
    e_wsfe();
    gllset_("LMISS", &c_true, (ftnlen)5);
    s_wsfe(&io___13);
    do_fio(&c__1, " *** OPTION LMISS = .TRUE.", (ftnlen)26);
    e_wsfe();
    viset_(ix, iy, &c__10, &c__1, &c__1);
    s_wsfe(&io___14);
    do_fio(&c__1, " AFTER CALLING VISET(IX,IY,10,1,1)", (ftnlen)34);
    e_wsfe();
    s_wsfe(&io___15);
    do_fio(&c__1, " LIST OF IY : ", (ftnlen)14);
    do_fio(&c__10, (char *)&iy[0], (ftnlen)sizeof(integer));
    e_wsfe();
    iadd_(iy, &c__10, &c__1, &c__1);
    s_wsfe(&io___16);
    do_fio(&c__1, " AFTER CALLING IADD(IY,10,1,+1)", (ftnlen)31);
    e_wsfe();
    s_wsfe(&io___17);
    do_fio(&c__1, " LIST OF IY : ", (ftnlen)14);
    do_fio(&c__10, (char *)&iy[0], (ftnlen)sizeof(integer));
    e_wsfe();
    imlt_(iy, &c__10, &c__1, &c_n1);
    s_wsfe(&io___18);
    do_fio(&c__1, " AFTER CALLING IMLT(IY,10,1,-1)", (ftnlen)31);
    e_wsfe();
    s_wsfe(&io___19);
    do_fio(&c__1, " LIST OF IY : ", (ftnlen)14);
    do_fio(&c__10, (char *)&iy[0], (ftnlen)sizeof(integer));
    e_wsfe();
    iset_(iy, &c__10, &c__1, &c__0);
    s_wsfe(&io___20);
    do_fio(&c__1, " AFTER CALLING ISET(IY,10,1,0)", (ftnlen)30);
    e_wsfe();
    s_wsfe(&io___21);
    do_fio(&c__1, " LIST OF IY : ", (ftnlen)14);
    do_fio(&c__10, (char *)&iy[0], (ftnlen)sizeof(integer));
    e_wsfe();
    return 0;
} /* MAIN__ */

/* Main program alias */ int vial02_ () { MAIN__ (); return 0; }

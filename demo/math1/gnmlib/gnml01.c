/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static logical c_true = TRUE_;
static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Initialized data */

    static real rr[2] = { 1.23f,30.00003f };

    /* System generated locals */
    real r__1;

    /* Builtin functions */
    integer s_wsfe(cilist *), do_fio(integer *, char *, ftnlen), e_wsfe(void);

    /* Local variables */
    static integer i__;
    static real br;
    static integer ip;
    extern /* Subroutine */ int gnge_(real *, real *, integer *), gnle_(real *
	    , real *, integer *), gngt_(real *, real *, integer *), gnlt_(
	    real *, real *, integer *);
    extern real rgnge_(real *), rgnle_(real *), rgngt_(real *), rgnlt_(real *)
	    ;
    extern /* Subroutine */ int gllset_(char *, logical *, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___3 = { 0, 6, 0, "(A,G16.7)", 0 };
    static cilist io___6 = { 0, 6, 0, "(A,F8.4,A,I4)", 0 };
    static cilist io___7 = { 0, 6, 0, "(A,F8.4,A,I4)", 0 };
    static cilist io___8 = { 0, 6, 0, "(A,F8.4,A,I4)", 0 };
    static cilist io___9 = { 0, 6, 0, "(A,F8.4,A,I4)", 0 };
    static cilist io___10 = { 0, 6, 0, "(A,G16.7)", 0 };
    static cilist io___11 = { 0, 6, 0, "(A,G16.7)", 0 };
    static cilist io___12 = { 0, 6, 0, "(A,G16.7)", 0 };
    static cilist io___13 = { 0, 6, 0, "(A,G16.7)", 0 };


    gllset_("LEPSL", &c_true, (ftnlen)5);
    for (i__ = 1; i__ <= 2; ++i__) {
	s_wsfe(&io___3);
	do_fio(&c__1, " *** RR = ", (ftnlen)10);
	do_fio(&c__1, (char *)&rr[i__ - 1], (ftnlen)sizeof(real));
	e_wsfe();
	gnlt_(&rr[i__ - 1], &br, &ip);
	s_wsfe(&io___6);
	do_fio(&c__1, " *** CALL GNLT(RR,BR,IP) : BR = ", (ftnlen)32);
	do_fio(&c__1, (char *)&br, (ftnlen)sizeof(real));
	do_fio(&c__1, " : IP = ", (ftnlen)8);
	do_fio(&c__1, (char *)&ip, (ftnlen)sizeof(integer));
	e_wsfe();
	gnle_(&rr[i__ - 1], &br, &ip);
	s_wsfe(&io___7);
	do_fio(&c__1, " *** CALL GNLE(RR,BR,IP) : BR = ", (ftnlen)32);
	do_fio(&c__1, (char *)&br, (ftnlen)sizeof(real));
	do_fio(&c__1, " : IP = ", (ftnlen)8);
	do_fio(&c__1, (char *)&ip, (ftnlen)sizeof(integer));
	e_wsfe();
	gngt_(&rr[i__ - 1], &br, &ip);
	s_wsfe(&io___8);
	do_fio(&c__1, " *** CALL GNGT(RR,BR,IP) : BR = ", (ftnlen)32);
	do_fio(&c__1, (char *)&br, (ftnlen)sizeof(real));
	do_fio(&c__1, " : IP = ", (ftnlen)8);
	do_fio(&c__1, (char *)&ip, (ftnlen)sizeof(integer));
	e_wsfe();
	gnge_(&rr[i__ - 1], &br, &ip);
	s_wsfe(&io___9);
	do_fio(&c__1, " *** CALL GNGE(RR,BR,IP) : BR = ", (ftnlen)32);
	do_fio(&c__1, (char *)&br, (ftnlen)sizeof(real));
	do_fio(&c__1, " : IP = ", (ftnlen)8);
	do_fio(&c__1, (char *)&ip, (ftnlen)sizeof(integer));
	e_wsfe();
	s_wsfe(&io___10);
	do_fio(&c__1, " *** RGNLT(RR) = ", (ftnlen)17);
	r__1 = rgnlt_(&rr[i__ - 1]);
	do_fio(&c__1, (char *)&r__1, (ftnlen)sizeof(real));
	e_wsfe();
	s_wsfe(&io___11);
	do_fio(&c__1, " *** RGNLE(RR) = ", (ftnlen)17);
	r__1 = rgnle_(&rr[i__ - 1]);
	do_fio(&c__1, (char *)&r__1, (ftnlen)sizeof(real));
	e_wsfe();
	s_wsfe(&io___12);
	do_fio(&c__1, " *** RGNGT(RR) = ", (ftnlen)17);
	r__1 = rgngt_(&rr[i__ - 1]);
	do_fio(&c__1, (char *)&r__1, (ftnlen)sizeof(real));
	e_wsfe();
	s_wsfe(&io___13);
	do_fio(&c__1, " *** RGNGE(RR) = ", (ftnlen)17);
	r__1 = rgnge_(&rr[i__ - 1]);
	do_fio(&c__1, (char *)&r__1, (ftnlen)sizeof(real));
	e_wsfe();
/* L10: */
    }
    return 0;
} /* MAIN__ */

/* Main program alias */ int gnml01_ () { MAIN__ (); return 0; }

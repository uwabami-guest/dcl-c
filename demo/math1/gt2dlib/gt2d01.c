/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__15 = 15;
static integer c__9 = 9;
static integer c__1 = 1;
static integer c__4 = 4;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* System generated locals */
    real r__1;

    /* Builtin functions */
    double exp(doublereal);
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void);

    /* Local variables */
    static integer i__, j;
    static real cx[225]	/* was [15][15] */, cy[225]	/* was [15][15] */, 
	    ux[15], uy[15], cxp, cyp, uxp, uyp;
    extern /* Subroutine */ int g2fctr_(real *, real *, real *, real *), 
	    g2ictr_(real *, real *, real *, real *), g2sctr_(integer *, 
	    integer *, real *, real *, real *, real *);
    static real rundef;
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen);
    static real terrain[15];

    /* Fortran I/O blocks */
    static cilist io___9 = { 0, 6, 0, 0, 0 };
    static cilist io___12 = { 0, 6, 0, 0, 0 };
    static cilist io___15 = { 0, 6, 0, 0, 0 };
    static cilist io___16 = { 0, 6, 0, 0, 0 };


    glrget_("RUNDEF", &rundef, (ftnlen)6);
    for (i__ = 1; i__ <= 15; ++i__) {
	ux[i__ - 1] = (i__ - 1.f) / 14.f - .5f;
/* Computing 2nd power */
	r__1 = ux[i__ - 1];
	terrain[i__ - 1] = exp(r__1 * r__1 * -24) * .1f;
/* L10: */
    }
    for (j = 1; j <= 15; ++j) {
	uy[j - 1] = (j - 1.f) / 14.f;
/* L15: */
    }
    cx[0] = rundef;
    for (j = 1; j <= 15; ++j) {
	for (i__ = 1; i__ <= 15; ++i__) {
	    cy[i__ + j * 15 - 16] = uy[j - 1] * (1.f - terrain[i__ - 1]) + 
		    terrain[i__ - 1];
/* L20: */
	}
/* L25: */
    }
    g2sctr_(&c__15, &c__15, ux, uy, cx, cy);
    s_wsle(&io___9);
    do_lio(&c__9, &c__1, "TERRAIN FOLLOWING COORDINATE", (ftnlen)28);
    e_wsle();
    uxp = .2f;
    uyp = .3f;
    s_wsle(&io___12);
    do_lio(&c__9, &c__1, "ORIGINAL UX, UY:", (ftnlen)16);
    do_lio(&c__4, &c__1, (char *)&uxp, (ftnlen)sizeof(real));
    do_lio(&c__4, &c__1, (char *)&uyp, (ftnlen)sizeof(real));
    e_wsle();
    g2fctr_(&uxp, &uyp, &cxp, &cyp);
    s_wsle(&io___15);
    do_lio(&c__9, &c__1, "TRANSFORMED CX, CY:", (ftnlen)19);
    do_lio(&c__4, &c__1, (char *)&cxp, (ftnlen)sizeof(real));
    do_lio(&c__4, &c__1, (char *)&cyp, (ftnlen)sizeof(real));
    e_wsle();
    g2ictr_(&cxp, &cyp, &uxp, &uyp);
    s_wsle(&io___16);
    do_lio(&c__9, &c__1, "INVERTED UX, UY:", (ftnlen)16);
    do_lio(&c__4, &c__1, (char *)&uxp, (ftnlen)sizeof(real));
    do_lio(&c__4, &c__1, (char *)&uyp, (ftnlen)sizeof(real));
    e_wsle();
    return 0;
} /* MAIN__ */

/* Main program alias */ int gt2d01_ () { MAIN__ (); return 0; }

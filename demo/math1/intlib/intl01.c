/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static logical c_true = TRUE_;
static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Initialized data */

    static real rr[3] = { 2.5f,3.f,3.000003f };

    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    integer s_wsfe(cilist *), do_fio(integer *, char *, ftnlen), e_wsfe(void);

    /* Local variables */
    static integer i__;
    extern integer irge_(real *), irle_(real *), irgt_(real *), irlt_(real *);
    extern /* Subroutine */ int gllset_(char *, logical *, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___3 = { 0, 6, 0, "(A,G16.7)", 0 };
    static cilist io___4 = { 0, 6, 0, "(A,I4)", 0 };
    static cilist io___5 = { 0, 6, 0, "(A,I4)", 0 };
    static cilist io___6 = { 0, 6, 0, "(A,I4)", 0 };
    static cilist io___7 = { 0, 6, 0, "(A,I4)", 0 };


    gllset_("LEPSL", &c_true, (ftnlen)5);
    for (i__ = 1; i__ <= 3; ++i__) {
	s_wsfe(&io___3);
	do_fio(&c__1, " *** RR = ", (ftnlen)10);
	do_fio(&c__1, (char *)&rr[i__ - 1], (ftnlen)sizeof(real));
	e_wsfe();
	s_wsfe(&io___4);
	do_fio(&c__1, " *** IRLT(RR) = ", (ftnlen)16);
	i__1 = irlt_(&rr[i__ - 1]);
	do_fio(&c__1, (char *)&i__1, (ftnlen)sizeof(integer));
	e_wsfe();
	s_wsfe(&io___5);
	do_fio(&c__1, " *** IRLE(RR) = ", (ftnlen)16);
	i__1 = irle_(&rr[i__ - 1]);
	do_fio(&c__1, (char *)&i__1, (ftnlen)sizeof(integer));
	e_wsfe();
	s_wsfe(&io___6);
	do_fio(&c__1, " *** IRGT(RR) = ", (ftnlen)16);
	i__1 = irgt_(&rr[i__ - 1]);
	do_fio(&c__1, (char *)&i__1, (ftnlen)sizeof(integer));
	e_wsfe();
	s_wsfe(&io___7);
	do_fio(&c__1, " *** IRGE(RR) = ", (ftnlen)16);
	i__1 = irge_(&rr[i__ - 1]);
	do_fio(&c__1, (char *)&i__1, (ftnlen)sizeof(integer));
	e_wsfe();
/* L10: */
    }
    return 0;
} /* MAIN__ */

/* Main program alias */ int intl01_ () { MAIN__ (); return 0; }

/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;
static integer c__10 = 10;
static integer c__2 = 2;
static integer c__0 = 0;
static real c_b33 = 1.f;
static real c_b45 = 2.f;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Initialized data */

    static integer ix[10] = { 0,1,2,3,2,1,0,-1,-2,0 };
    static real rx[10] = { 0.f,1.f,2.f,3.f,2.f,1.f,0.f,-1.f,-2.f,0.f };
    static char cx[1*10] = "A" "B" "C" "B" "A" "Z" "W" "Z" "A" "Y";

    /* Format strings */
    static char fmt_100[] = "(a,i6)";

    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    integer s_wsfe(cilist *), do_fio(integer *, char *, ftnlen), e_wsfe(void);

    /* Local variables */
    extern integer indxcf_(char *, integer *, integer *, char *, ftnlen, 
	    ftnlen), indxif_(integer *, integer *, integer *, integer *), 
	    indxcl_(char *, integer *, integer *, char *, ftnlen, ftnlen), 
	    nindxc_(char *, integer *, integer *, char *, ftnlen, ftnlen), 
	    indxil_(integer *, integer *, integer *, integer *), nindxi_(
	    integer *, integer *, integer *, integer *), indxrf_(real *, 
	    integer *, integer *, real *), indxrl_(real *, integer *, integer 
	    *, real *), nindxr_(real *, integer *, integer *, real *);

    /* Fortran I/O blocks */
    static cilist io___4 = { 0, 6, 0, "(A,10I4)", 0 };
    static cilist io___5 = { 0, 6, 0, fmt_100, 0 };
    static cilist io___6 = { 0, 6, 0, fmt_100, 0 };
    static cilist io___7 = { 0, 6, 0, fmt_100, 0 };
    static cilist io___8 = { 0, 6, 0, "(A,10F4.0)", 0 };
    static cilist io___9 = { 0, 6, 0, fmt_100, 0 };
    static cilist io___10 = { 0, 6, 0, fmt_100, 0 };
    static cilist io___11 = { 0, 6, 0, fmt_100, 0 };
    static cilist io___12 = { 0, 6, 0, "(A,10A4)", 0 };
    static cilist io___13 = { 0, 6, 0, fmt_100, 0 };
    static cilist io___14 = { 0, 6, 0, fmt_100, 0 };
    static cilist io___15 = { 0, 6, 0, fmt_100, 0 };


    s_wsfe(&io___4);
    do_fio(&c__1, " LIST OF IX (N=10) : ", (ftnlen)21);
    do_fio(&c__10, (char *)&ix[0], (ftnlen)sizeof(integer));
    e_wsfe();
    s_wsfe(&io___5);
    do_fio(&c__1, " *** INDXIF(IX,N,1,2)  = ", (ftnlen)25);
    i__1 = indxif_(ix, &c__10, &c__1, &c__2);
    do_fio(&c__1, (char *)&i__1, (ftnlen)sizeof(integer));
    e_wsfe();
    s_wsfe(&io___6);
    do_fio(&c__1, " *** INDXIL(IX,N,1,2)  = ", (ftnlen)25);
    i__1 = indxil_(ix, &c__10, &c__1, &c__2);
    do_fio(&c__1, (char *)&i__1, (ftnlen)sizeof(integer));
    e_wsfe();
    s_wsfe(&io___7);
    do_fio(&c__1, " *** NINDXI(IX,N,1,0)  = ", (ftnlen)25);
    i__1 = nindxi_(ix, &c__10, &c__1, &c__0);
    do_fio(&c__1, (char *)&i__1, (ftnlen)sizeof(integer));
    e_wsfe();
    s_wsfe(&io___8);
    do_fio(&c__1, " LIST OF RX (N=10) : ", (ftnlen)21);
    do_fio(&c__10, (char *)&rx[0], (ftnlen)sizeof(real));
    e_wsfe();
    s_wsfe(&io___9);
    do_fio(&c__1, " *** INDXRF(RX,N,1,1.0)  = ", (ftnlen)27);
    i__1 = indxrf_(rx, &c__10, &c__1, &c_b33);
    do_fio(&c__1, (char *)&i__1, (ftnlen)sizeof(integer));
    e_wsfe();
    s_wsfe(&io___10);
    do_fio(&c__1, " *** INDXRL(RX,N,1,1.0)  = ", (ftnlen)27);
    i__1 = indxrl_(rx, &c__10, &c__1, &c_b33);
    do_fio(&c__1, (char *)&i__1, (ftnlen)sizeof(integer));
    e_wsfe();
    s_wsfe(&io___11);
    do_fio(&c__1, " *** NINDXR(RX,N,1,2.0)  = ", (ftnlen)27);
    i__1 = nindxr_(rx, &c__10, &c__1, &c_b45);
    do_fio(&c__1, (char *)&i__1, (ftnlen)sizeof(integer));
    e_wsfe();
    s_wsfe(&io___12);
    do_fio(&c__1, " LIST OF CX (N=10) : ", (ftnlen)21);
    do_fio(&c__10, cx, (ftnlen)1);
    e_wsfe();
    s_wsfe(&io___13);
    do_fio(&c__1, " *** INDXCF(CX,N,1,'B')  = ", (ftnlen)27);
    i__1 = indxcf_(cx, &c__10, &c__1, "B", (ftnlen)1, (ftnlen)1);
    do_fio(&c__1, (char *)&i__1, (ftnlen)sizeof(integer));
    e_wsfe();
    s_wsfe(&io___14);
    do_fio(&c__1, " *** INDXCL(CX,N,1,'B')  = ", (ftnlen)27);
    i__1 = indxcl_(cx, &c__10, &c__1, "B", (ftnlen)1, (ftnlen)1);
    do_fio(&c__1, (char *)&i__1, (ftnlen)sizeof(integer));
    e_wsfe();
    s_wsfe(&io___15);
    do_fio(&c__1, " *** NINDXC(CX,N,1,'A')  = ", (ftnlen)27);
    i__1 = nindxc_(cx, &c__10, &c__1, "A", (ftnlen)1, (ftnlen)1);
    do_fio(&c__1, (char *)&i__1, (ftnlen)sizeof(integer));
    e_wsfe();
    return 0;
} /* MAIN__ */

/* Main program alias */ int indx01_ () { MAIN__ (); return 0; }

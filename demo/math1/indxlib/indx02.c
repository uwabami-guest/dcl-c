/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Initialized data */

    static char cc[50] = "12345678901234567890123456789012345678901234567890";
    static char cx[50] = "NANA AND MAMA GAVE TOMATOES AND BANANAS TO MASATO.";

    /* Format strings */
    static char fmt_100[] = "(a,i4,a,i4,a,a)";
    static char fmt_200[] = "(a,i4)";

    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    integer s_wsfe(cilist *), do_fio(integer *, char *, ftnlen), e_wsfe(void);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer j, n;
    static char ch[2];
    extern integer indxmf_(char *, integer *, integer *, char *, ftnlen, 
	    ftnlen), indxml_(char *, integer *, integer *, char *, ftnlen, 
	    ftnlen), nindxm_(char *, integer *, integer *, char *, ftnlen, 
	    ftnlen);

    /* Fortran I/O blocks */
    static cilist io___3 = { 0, 6, 0, "(A,A)", 0 };
    static cilist io___4 = { 0, 6, 0, "(A,A)", 0 };
    static cilist io___8 = { 0, 6, 0, fmt_100, 0 };
    static cilist io___9 = { 0, 6, 0, fmt_200, 0 };
    static cilist io___10 = { 0, 6, 0, fmt_200, 0 };
    static cilist io___11 = { 0, 6, 0, fmt_200, 0 };
    static cilist io___12 = { 0, 6, 0, fmt_100, 0 };
    static cilist io___13 = { 0, 6, 0, fmt_200, 0 };
    static cilist io___14 = { 0, 6, 0, fmt_200, 0 };
    static cilist io___15 = { 0, 6, 0, fmt_200, 0 };
    static cilist io___16 = { 0, 6, 0, fmt_100, 0 };
    static cilist io___17 = { 0, 6, 0, fmt_200, 0 };
    static cilist io___18 = { 0, 6, 0, fmt_200, 0 };
    static cilist io___19 = { 0, 6, 0, fmt_200, 0 };


    s_wsfe(&io___3);
    do_fio(&c__1, " -------------", (ftnlen)14);
    do_fio(&c__1, cc, (ftnlen)50);
    e_wsfe();
    s_wsfe(&io___4);
    do_fio(&c__1, " LIST OF CX : ", (ftnlen)14);
    do_fio(&c__1, cx, (ftnlen)50);
    e_wsfe();
    n = 25;
    j = 2;
    s_copy(ch, "NA", (ftnlen)2, (ftnlen)2);
    s_wsfe(&io___8);
    do_fio(&c__1, " N = ", (ftnlen)5);
    do_fio(&c__1, (char *)&n, (ftnlen)sizeof(integer));
    do_fio(&c__1, " : J = ", (ftnlen)7);
    do_fio(&c__1, (char *)&j, (ftnlen)sizeof(integer));
    do_fio(&c__1, " : CH = ", (ftnlen)8);
    do_fio(&c__1, ch, (ftnlen)2);
    e_wsfe();
    s_wsfe(&io___9);
    do_fio(&c__1, " *** INDXMF(CX,N,J,CH)  = ", (ftnlen)26);
    i__1 = indxmf_(cx, &n, &j, ch, (ftnlen)50, (ftnlen)2);
    do_fio(&c__1, (char *)&i__1, (ftnlen)sizeof(integer));
    e_wsfe();
    s_wsfe(&io___10);
    do_fio(&c__1, " *** INDXML(CX,N,J,CH)  = ", (ftnlen)26);
    i__1 = indxml_(cx, &n, &j, ch, (ftnlen)50, (ftnlen)2);
    do_fio(&c__1, (char *)&i__1, (ftnlen)sizeof(integer));
    e_wsfe();
    s_wsfe(&io___11);
    do_fio(&c__1, " *** NINDXM(CX,N,J,CH)  = ", (ftnlen)26);
    i__1 = nindxm_(cx, &n, &j, ch, (ftnlen)50, (ftnlen)2);
    do_fio(&c__1, (char *)&i__1, (ftnlen)sizeof(integer));
    e_wsfe();
    n = 49;
    j = 1;
    s_copy(ch, "MA", (ftnlen)2, (ftnlen)2);
    s_wsfe(&io___12);
    do_fio(&c__1, " N = ", (ftnlen)5);
    do_fio(&c__1, (char *)&n, (ftnlen)sizeof(integer));
    do_fio(&c__1, " : J = ", (ftnlen)7);
    do_fio(&c__1, (char *)&j, (ftnlen)sizeof(integer));
    do_fio(&c__1, " : CH = ", (ftnlen)8);
    do_fio(&c__1, ch, (ftnlen)2);
    e_wsfe();
    s_wsfe(&io___13);
    do_fio(&c__1, " *** INDXMF(CX,N,J,CH)  = ", (ftnlen)26);
    i__1 = indxmf_(cx, &n, &j, ch, (ftnlen)50, (ftnlen)2);
    do_fio(&c__1, (char *)&i__1, (ftnlen)sizeof(integer));
    e_wsfe();
    s_wsfe(&io___14);
    do_fio(&c__1, " *** INDXML(CX,N,J,CH)  = ", (ftnlen)26);
    i__1 = indxml_(cx, &n, &j, ch, (ftnlen)50, (ftnlen)2);
    do_fio(&c__1, (char *)&i__1, (ftnlen)sizeof(integer));
    e_wsfe();
    s_wsfe(&io___15);
    do_fio(&c__1, " *** NINDXM(CX,N,J,CH)  = ", (ftnlen)26);
    i__1 = nindxm_(cx, &n, &j, ch, (ftnlen)50, (ftnlen)2);
    do_fio(&c__1, (char *)&i__1, (ftnlen)sizeof(integer));
    e_wsfe();
    n = 49;
    j = 1;
    s_copy(ch, "ma", (ftnlen)2, (ftnlen)2);
    s_wsfe(&io___16);
    do_fio(&c__1, " N = ", (ftnlen)5);
    do_fio(&c__1, (char *)&n, (ftnlen)sizeof(integer));
    do_fio(&c__1, " : J = ", (ftnlen)7);
    do_fio(&c__1, (char *)&j, (ftnlen)sizeof(integer));
    do_fio(&c__1, " : CH = ", (ftnlen)8);
    do_fio(&c__1, ch, (ftnlen)2);
    e_wsfe();
    s_wsfe(&io___17);
    do_fio(&c__1, " *** INDXMF(CX,N,J,CH)  = ", (ftnlen)26);
    i__1 = indxmf_(cx, &n, &j, ch, (ftnlen)50, (ftnlen)2);
    do_fio(&c__1, (char *)&i__1, (ftnlen)sizeof(integer));
    e_wsfe();
    s_wsfe(&io___18);
    do_fio(&c__1, " *** INDXML(CX,N,J,CH)  = ", (ftnlen)26);
    i__1 = indxml_(cx, &n, &j, ch, (ftnlen)50, (ftnlen)2);
    do_fio(&c__1, (char *)&i__1, (ftnlen)sizeof(integer));
    e_wsfe();
    s_wsfe(&io___19);
    do_fio(&c__1, " *** NINDXM(CX,N,J,CH)  = ", (ftnlen)26);
    i__1 = nindxm_(cx, &n, &j, ch, (ftnlen)50, (ftnlen)2);
    do_fio(&c__1, (char *)&i__1, (ftnlen)sizeof(integer));
    e_wsfe();
    return 0;
} /* MAIN__ */

/* Main program alias */ int indx02_ () { MAIN__ (); return 0; }

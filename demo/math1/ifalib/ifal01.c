/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;
static integer c__10 = 10;
static logical c_true = TRUE_;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Initialized data */

    static integer ix[10] = { 0,1,4,6,3,-4,12,999,23,-5 };

    /* Builtin functions */
    integer s_wsfe(cilist *), do_fio(integer *, char *, ftnlen), e_wsfe(void);

    /* Local variables */
    extern integer imin_(integer *, integer *, integer *), imax_(integer *, 
	    integer *, integer *), isum_(integer *, integer *, integer *);
    static integer jimin, jimax, jisum;
    extern /* Subroutine */ int gllset_(char *, logical *, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___2 = { 0, 6, 0, "(A,10I6)", 0 };
    static cilist io___3 = { 0, 6, 0, "(A)", 0 };
    static cilist io___5 = { 0, 6, 0, "(A,I6)", 0 };
    static cilist io___7 = { 0, 6, 0, "(A,I6)", 0 };
    static cilist io___9 = { 0, 6, 0, "(A,I6)", 0 };
    static cilist io___10 = { 0, 6, 0, "(A)", 0 };
    static cilist io___11 = { 0, 6, 0, "(A,I6)", 0 };
    static cilist io___12 = { 0, 6, 0, "(A,I6)", 0 };
    static cilist io___13 = { 0, 6, 0, "(A,I6)", 0 };


    s_wsfe(&io___2);
    do_fio(&c__1, " LIST OF IX : ", (ftnlen)14);
    do_fio(&c__10, (char *)&ix[0], (ftnlen)sizeof(integer));
    e_wsfe();
    s_wsfe(&io___3);
    do_fio(&c__1, " *** OPTION LMISS = .FALSE.", (ftnlen)27);
    e_wsfe();
    jimax = imax_(ix, &c__10, &c__1);
    s_wsfe(&io___5);
    do_fio(&c__1, " *** IMAX = ", (ftnlen)12);
    do_fio(&c__1, (char *)&jimax, (ftnlen)sizeof(integer));
    e_wsfe();
    jimin = imin_(ix, &c__10, &c__1);
    s_wsfe(&io___7);
    do_fio(&c__1, " *** IMIN = ", (ftnlen)12);
    do_fio(&c__1, (char *)&jimin, (ftnlen)sizeof(integer));
    e_wsfe();
    jisum = isum_(ix, &c__10, &c__1);
    s_wsfe(&io___9);
    do_fio(&c__1, " *** ISUM = ", (ftnlen)12);
    do_fio(&c__1, (char *)&jisum, (ftnlen)sizeof(integer));
    e_wsfe();
    gllset_("LMISS", &c_true, (ftnlen)5);
    s_wsfe(&io___10);
    do_fio(&c__1, " *** OPTION LMISS = .TRUE.", (ftnlen)26);
    e_wsfe();
    jimax = imax_(ix, &c__10, &c__1);
    s_wsfe(&io___11);
    do_fio(&c__1, " *** IMAX = ", (ftnlen)12);
    do_fio(&c__1, (char *)&jimax, (ftnlen)sizeof(integer));
    e_wsfe();
    jimin = imin_(ix, &c__10, &c__1);
    s_wsfe(&io___12);
    do_fio(&c__1, " *** IMIN = ", (ftnlen)12);
    do_fio(&c__1, (char *)&jimin, (ftnlen)sizeof(integer));
    e_wsfe();
    jisum = isum_(ix, &c__10, &c__1);
    s_wsfe(&io___13);
    do_fio(&c__1, " *** ISUM = ", (ftnlen)12);
    do_fio(&c__1, (char *)&jisum, (ftnlen)sizeof(integer));
    e_wsfe();
    return 0;
} /* MAIN__ */

/* Main program alias */ int ifal01_ () { MAIN__ (); return 0; }

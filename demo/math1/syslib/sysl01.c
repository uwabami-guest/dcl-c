/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static integer c__4 = 4;
static integer c__8 = 8;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void);

    /* Local variables */
    static integer msgu;
    static real rfact;
    static integer nchrs;
    static logical lepsl;
    static integer nbits;
    static real rlmin, rlmax;
    static integer imiss;
    static real repsl;
    static logical lmiss;
    static real rmiss;
    static integer iundef;
    extern /* Subroutine */ int gliget_(char *, integer *, ftnlen), gllget_(
	    char *, logical *, ftnlen);
    static real rundef;
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen);
    static integer nlines, maxmsg, msglev, intmax, iiunit, iounit;

    /* Fortran I/O blocks */
    static cilist io___2 = { 0, 6, 0, 0, 0 };
    static cilist io___4 = { 0, 6, 0, 0, 0 };
    static cilist io___6 = { 0, 6, 0, 0, 0 };
    static cilist io___8 = { 0, 6, 0, 0, 0 };
    static cilist io___10 = { 0, 6, 0, 0, 0 };
    static cilist io___12 = { 0, 6, 0, 0, 0 };
    static cilist io___14 = { 0, 6, 0, 0, 0 };
    static cilist io___16 = { 0, 6, 0, 0, 0 };
    static cilist io___18 = { 0, 6, 0, 0, 0 };
    static cilist io___20 = { 0, 6, 0, 0, 0 };
    static cilist io___22 = { 0, 6, 0, 0, 0 };
    static cilist io___24 = { 0, 6, 0, 0, 0 };
    static cilist io___26 = { 0, 6, 0, 0, 0 };
    static cilist io___28 = { 0, 6, 0, 0, 0 };
    static cilist io___30 = { 0, 6, 0, 0, 0 };
    static cilist io___32 = { 0, 6, 0, 0, 0 };
    static cilist io___34 = { 0, 6, 0, 0, 0 };
    static cilist io___36 = { 0, 6, 0, 0, 0 };
    static cilist io___38 = { 0, 6, 0, 0, 0 };


    gliget_("NBITSPW ", &nbits, (ftnlen)8);
    s_wsle(&io___2);
    do_lio(&c__9, &c__1, "*** NBITSPW  = ", (ftnlen)15);
    do_lio(&c__3, &c__1, (char *)&nbits, (ftnlen)sizeof(integer));
    e_wsle();
    gliget_("NCHRSPW ", &nchrs, (ftnlen)8);
    s_wsle(&io___4);
    do_lio(&c__9, &c__1, "*** NCHRSPW  = ", (ftnlen)15);
    do_lio(&c__3, &c__1, (char *)&nchrs, (ftnlen)sizeof(integer));
    e_wsle();
    gliget_("INTMAX  ", &intmax, (ftnlen)8);
    s_wsle(&io___6);
    do_lio(&c__9, &c__1, "*** INTMAX   = ", (ftnlen)15);
    do_lio(&c__3, &c__1, (char *)&intmax, (ftnlen)sizeof(integer));
    e_wsle();
    glrget_("REALMAX ", &rlmax, (ftnlen)8);
    s_wsle(&io___8);
    do_lio(&c__9, &c__1, "*** REALMAX  = ", (ftnlen)15);
    do_lio(&c__4, &c__1, (char *)&rlmax, (ftnlen)sizeof(real));
    e_wsle();
    glrget_("REALMIN ", &rlmin, (ftnlen)8);
    s_wsle(&io___10);
    do_lio(&c__9, &c__1, "*** REALMIN  = ", (ftnlen)15);
    do_lio(&c__4, &c__1, (char *)&rlmin, (ftnlen)sizeof(real));
    e_wsle();
    gliget_("IIUNIT  ", &iiunit, (ftnlen)8);
    s_wsle(&io___12);
    do_lio(&c__9, &c__1, "*** IIUNIT   = ", (ftnlen)15);
    do_lio(&c__3, &c__1, (char *)&iiunit, (ftnlen)sizeof(integer));
    e_wsle();
    gliget_("IOUNIT  ", &iounit, (ftnlen)8);
    s_wsle(&io___14);
    do_lio(&c__9, &c__1, "*** IOUNIT   = ", (ftnlen)15);
    do_lio(&c__3, &c__1, (char *)&iounit, (ftnlen)sizeof(integer));
    e_wsle();
    gliget_("MSGUNIT ", &msgu, (ftnlen)8);
    s_wsle(&io___16);
    do_lio(&c__9, &c__1, "*** MSGUNIT  = ", (ftnlen)15);
    do_lio(&c__3, &c__1, (char *)&msgu, (ftnlen)sizeof(integer));
    e_wsle();
    gliget_("MAXMSG  ", &maxmsg, (ftnlen)8);
    s_wsle(&io___18);
    do_lio(&c__9, &c__1, "*** MAXMSG   = ", (ftnlen)15);
    do_lio(&c__3, &c__1, (char *)&maxmsg, (ftnlen)sizeof(integer));
    e_wsle();
    gliget_("MSGLEV  ", &msglev, (ftnlen)8);
    s_wsle(&io___20);
    do_lio(&c__9, &c__1, "*** MSGLEV   = ", (ftnlen)15);
    do_lio(&c__3, &c__1, (char *)&msglev, (ftnlen)sizeof(integer));
    e_wsle();
    gliget_("NLNSIZE ", &nlines, (ftnlen)8);
    s_wsle(&io___22);
    do_lio(&c__9, &c__1, "*** NLNSIZE  = ", (ftnlen)15);
    do_lio(&c__3, &c__1, (char *)&nlines, (ftnlen)sizeof(integer));
    e_wsle();
    gllget_("LMISS   ", &lmiss, (ftnlen)8);
    s_wsle(&io___24);
    do_lio(&c__9, &c__1, "*** LMISS    = ", (ftnlen)15);
    do_lio(&c__8, &c__1, (char *)&lmiss, (ftnlen)sizeof(logical));
    e_wsle();
    gliget_("IMISS   ", &imiss, (ftnlen)8);
    s_wsle(&io___26);
    do_lio(&c__9, &c__1, "*** IMISS    = ", (ftnlen)15);
    do_lio(&c__3, &c__1, (char *)&imiss, (ftnlen)sizeof(integer));
    e_wsle();
    glrget_("RMISS   ", &rmiss, (ftnlen)8);
    s_wsle(&io___28);
    do_lio(&c__9, &c__1, "*** RMISS    = ", (ftnlen)15);
    do_lio(&c__4, &c__1, (char *)&rmiss, (ftnlen)sizeof(real));
    e_wsle();
    gliget_("IUNDEF  ", &iundef, (ftnlen)8);
    s_wsle(&io___30);
    do_lio(&c__9, &c__1, "*** IUNDEF   = ", (ftnlen)15);
    do_lio(&c__3, &c__1, (char *)&iundef, (ftnlen)sizeof(integer));
    e_wsle();
    glrget_("RUNDEF  ", &rundef, (ftnlen)8);
    s_wsle(&io___32);
    do_lio(&c__9, &c__1, "*** RUNDEF   = ", (ftnlen)15);
    do_lio(&c__4, &c__1, (char *)&rundef, (ftnlen)sizeof(real));
    e_wsle();
    gllget_("LEPSL   ", &lepsl, (ftnlen)8);
    s_wsle(&io___34);
    do_lio(&c__9, &c__1, "*** LEPSL    = ", (ftnlen)15);
    do_lio(&c__8, &c__1, (char *)&lepsl, (ftnlen)sizeof(logical));
    e_wsle();
    glrget_("REPSL   ", &repsl, (ftnlen)8);
    s_wsle(&io___36);
    do_lio(&c__9, &c__1, "*** REPSL    = ", (ftnlen)15);
    do_lio(&c__4, &c__1, (char *)&repsl, (ftnlen)sizeof(real));
    e_wsle();
    glrget_("RFACT   ", &rfact, (ftnlen)8);
    s_wsle(&io___38);
    do_lio(&c__9, &c__1, "*** RFACT    = ", (ftnlen)15);
    do_lio(&c__4, &c__1, (char *)&rfact, (ftnlen)sizeof(real));
    e_wsle();
    return 0;
} /* MAIN__ */

/* Main program alias */ int sysl01_ () { MAIN__ (); return 0; }

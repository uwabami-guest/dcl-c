/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;
static integer c__10 = 10;
static logical c_true = TRUE_;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Initialized data */

    static real rx[10] = { 0.f,-1.f,-2.f,-999.f,-1.f,0.f,1.f,2.f,999.f,1.f };

    /* Builtin functions */
    integer s_wsfe(cilist *), do_fio(integer *, char *, ftnlen), e_wsfe(void);

    /* Local variables */
    extern real rave_(real *, integer *, integer *), ramp_(real *, integer *, 
	    integer *), rmin_(real *, integer *, integer *), rmax_(real *, 
	    integer *, integer *), rvar_(real *, integer *, integer *), rstd_(
	    real *, integer *, integer *), rrms_(real *, integer *, integer *)
	    , rsum_(real *, integer *, integer *);
    static real srave, sramp, srmin, srmax, srvar, srstd, srrms, srsum;
    extern /* Subroutine */ int gllset_(char *, logical *, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___2 = { 0, 6, 0, "(A,10F6.0)", 0 };
    static cilist io___3 = { 0, 6, 0, "(A)", 0 };
    static cilist io___5 = { 0, 6, 0, "(A,G14.6)", 0 };
    static cilist io___7 = { 0, 6, 0, "(A,G14.6)", 0 };
    static cilist io___9 = { 0, 6, 0, "(A,G14.6)", 0 };
    static cilist io___11 = { 0, 6, 0, "(A,G14.6)", 0 };
    static cilist io___13 = { 0, 6, 0, "(A,G14.6)", 0 };
    static cilist io___15 = { 0, 6, 0, "(A,G14.6)", 0 };
    static cilist io___17 = { 0, 6, 0, "(A,G14.6)", 0 };
    static cilist io___19 = { 0, 6, 0, "(A,G14.6)", 0 };
    static cilist io___20 = { 0, 6, 0, "(A)", 0 };
    static cilist io___21 = { 0, 6, 0, "(A,G14.6)", 0 };
    static cilist io___22 = { 0, 6, 0, "(A,G14.6)", 0 };
    static cilist io___23 = { 0, 6, 0, "(A,G14.6)", 0 };
    static cilist io___24 = { 0, 6, 0, "(A,G14.6)", 0 };
    static cilist io___25 = { 0, 6, 0, "(A,G14.6)", 0 };
    static cilist io___26 = { 0, 6, 0, "(A,G14.6)", 0 };
    static cilist io___27 = { 0, 6, 0, "(A,G14.6)", 0 };
    static cilist io___28 = { 0, 6, 0, "(A,G14.6)", 0 };


    s_wsfe(&io___2);
    do_fio(&c__1, " LIST OF RX : ", (ftnlen)14);
    do_fio(&c__10, (char *)&rx[0], (ftnlen)sizeof(real));
    e_wsfe();
    s_wsfe(&io___3);
    do_fio(&c__1, " *** OPTION LMISS = .FALSE.", (ftnlen)27);
    e_wsfe();
    srmax = rmax_(rx, &c__10, &c__1);
    s_wsfe(&io___5);
    do_fio(&c__1, " *** RMAX = ", (ftnlen)12);
    do_fio(&c__1, (char *)&srmax, (ftnlen)sizeof(real));
    e_wsfe();
    srmin = rmin_(rx, &c__10, &c__1);
    s_wsfe(&io___7);
    do_fio(&c__1, " *** RMIN = ", (ftnlen)12);
    do_fio(&c__1, (char *)&srmin, (ftnlen)sizeof(real));
    e_wsfe();
    srsum = rsum_(rx, &c__10, &c__1);
    s_wsfe(&io___9);
    do_fio(&c__1, " *** RSUM = ", (ftnlen)12);
    do_fio(&c__1, (char *)&srsum, (ftnlen)sizeof(real));
    e_wsfe();
    srave = rave_(rx, &c__10, &c__1);
    s_wsfe(&io___11);
    do_fio(&c__1, " *** RAVE = ", (ftnlen)12);
    do_fio(&c__1, (char *)&srave, (ftnlen)sizeof(real));
    e_wsfe();
    srvar = rvar_(rx, &c__10, &c__1);
    s_wsfe(&io___13);
    do_fio(&c__1, " *** RVAR = ", (ftnlen)12);
    do_fio(&c__1, (char *)&srvar, (ftnlen)sizeof(real));
    e_wsfe();
    srstd = rstd_(rx, &c__10, &c__1);
    s_wsfe(&io___15);
    do_fio(&c__1, " *** RSTD = ", (ftnlen)12);
    do_fio(&c__1, (char *)&srstd, (ftnlen)sizeof(real));
    e_wsfe();
    srrms = rrms_(rx, &c__10, &c__1);
    s_wsfe(&io___17);
    do_fio(&c__1, " *** RRMS = ", (ftnlen)12);
    do_fio(&c__1, (char *)&srrms, (ftnlen)sizeof(real));
    e_wsfe();
    sramp = ramp_(rx, &c__10, &c__1);
    s_wsfe(&io___19);
    do_fio(&c__1, " *** RAMP = ", (ftnlen)12);
    do_fio(&c__1, (char *)&sramp, (ftnlen)sizeof(real));
    e_wsfe();
    gllset_("LMISS", &c_true, (ftnlen)5);
    s_wsfe(&io___20);
    do_fio(&c__1, " *** OPTION LMISS = .TRUE.", (ftnlen)26);
    e_wsfe();
    srmax = rmax_(rx, &c__10, &c__1);
    s_wsfe(&io___21);
    do_fio(&c__1, " *** RMAX = ", (ftnlen)12);
    do_fio(&c__1, (char *)&srmax, (ftnlen)sizeof(real));
    e_wsfe();
    srmin = rmin_(rx, &c__10, &c__1);
    s_wsfe(&io___22);
    do_fio(&c__1, " *** RMIN = ", (ftnlen)12);
    do_fio(&c__1, (char *)&srmin, (ftnlen)sizeof(real));
    e_wsfe();
    srsum = rsum_(rx, &c__10, &c__1);
    s_wsfe(&io___23);
    do_fio(&c__1, " *** RSUM = ", (ftnlen)12);
    do_fio(&c__1, (char *)&srsum, (ftnlen)sizeof(real));
    e_wsfe();
    srave = rave_(rx, &c__10, &c__1);
    s_wsfe(&io___24);
    do_fio(&c__1, " *** RAVE = ", (ftnlen)12);
    do_fio(&c__1, (char *)&srave, (ftnlen)sizeof(real));
    e_wsfe();
    srvar = rvar_(rx, &c__10, &c__1);
    s_wsfe(&io___25);
    do_fio(&c__1, " *** RVAR = ", (ftnlen)12);
    do_fio(&c__1, (char *)&srvar, (ftnlen)sizeof(real));
    e_wsfe();
    srstd = rstd_(rx, &c__10, &c__1);
    s_wsfe(&io___26);
    do_fio(&c__1, " *** RSTD = ", (ftnlen)12);
    do_fio(&c__1, (char *)&srstd, (ftnlen)sizeof(real));
    e_wsfe();
    srrms = rrms_(rx, &c__10, &c__1);
    s_wsfe(&io___27);
    do_fio(&c__1, " *** RRMS = ", (ftnlen)12);
    do_fio(&c__1, (char *)&srrms, (ftnlen)sizeof(real));
    e_wsfe();
    sramp = ramp_(rx, &c__10, &c__1);
    s_wsfe(&io___28);
    do_fio(&c__1, " *** RAMP = ", (ftnlen)12);
    do_fio(&c__1, (char *)&sramp, (ftnlen)sizeof(real));
    e_wsfe();
    return 0;
} /* MAIN__ */

/* Main program alias */ int rfal01_ () { MAIN__ (); return 0; }

/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;
static integer c__8 = 8;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Builtin functions */
    integer s_wsfe(cilist *), do_fio(integer *, char *, ftnlen), e_wsfe(void);

    /* Local variables */
    static integer ix[8];
    static real rx[8];
    extern /* Subroutine */ int vignn_(integer *, integer *, integer *), 
	    vrgnn_(real *, integer *, integer *);

    /* Fortran I/O blocks */
    static cilist io___1 = { 0, 6, 0, "(A)", 0 };
    static cilist io___3 = { 0, 6, 0, "(A,8I6)", 0 };
    static cilist io___4 = { 0, 6, 0, "(A)", 0 };
    static cilist io___6 = { 0, 6, 0, "(A,8F6.1)", 0 };


    s_wsfe(&io___1);
    do_fio(&c__1, " *** TEST FOR VIGNN", (ftnlen)19);
    e_wsfe();
    vignn_(ix, &c__8, &c__1);
    s_wsfe(&io___3);
    do_fio(&c__1, " IX = ", (ftnlen)6);
    do_fio(&c__8, (char *)&ix[0], (ftnlen)sizeof(integer));
    e_wsfe();
    s_wsfe(&io___4);
    do_fio(&c__1, " *** TEST FOR VRGNN", (ftnlen)19);
    e_wsfe();
    vrgnn_(rx, &c__8, &c__1);
    s_wsfe(&io___6);
    do_fio(&c__1, " RX = ", (ftnlen)6);
    do_fio(&c__8, (char *)&rx[0], (ftnlen)sizeof(real));
    e_wsfe();
    return 0;
} /* MAIN__ */

/* Main program alias */ int subl01_ () { MAIN__ (); return 0; }

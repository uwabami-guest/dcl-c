/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__3 = 3;
static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* System generated locals */
    address a__1[3];
    integer i__1[3];
    char ch__1[96];

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_wsfe(cilist *);
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen);
    integer do_fio(integer *, char *, ftnlen), e_wsfe(void);

    /* Local variables */
    static integer nl;
    static char cmd[80];
    extern integer lenc_(char *, ftnlen);
    extern /* Subroutine */ int osexec_(char *, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___3 = { 0, 6, 0, "(A)", 0 };


    s_copy(cmd, "ls", (ftnlen)80, (ftnlen)2);
    nl = lenc_(cmd, (ftnlen)80);
    s_wsfe(&io___3);
/* Writing concatenation */
    i__1[0] = 14, a__1[0] = " CALL OSEXEC('";
    i__1[1] = nl, a__1[1] = cmd;
    i__1[2] = 2, a__1[2] = "')";
    s_cat(ch__1, a__1, i__1, &c__3, (ftnlen)96);
    do_fio(&c__1, ch__1, nl + 16);
    e_wsfe();
    osexec_(cmd, (ftnlen)80);
    return 0;
} /* MAIN__ */

/* Main program alias */ int oslb02_ () { MAIN__ (); return 0; }

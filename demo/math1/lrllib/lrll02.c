/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Initialized data */

    static real x[3] = { 1.2f,3.f,3.f };
    static real y[3] = { 1.5f,3.f,3.000003f };

    /* System generated locals */
    logical L__1;

    /* Builtin functions */
    integer s_wsfe(cilist *), do_fio(integer *, char *, ftnlen), e_wsfe(void);

    /* Local variables */
    static integer i__;
    static real epsl;
    extern logical lrgea_(real *, real *, real *), lrlea_(real *, real *, 
	    real *), lrnea_(real *, real *, real *), lreqa_(real *, real *, 
	    real *), lrgta_(real *, real *, real *), lrlta_(real *, real *, 
	    real *);

    /* Fortran I/O blocks */
    static cilist io___5 = { 0, 6, 0, "(3(A,G16.7),A)", 0 };
    static cilist io___6 = { 0, 6, 0, "(A,L4)", 0 };
    static cilist io___7 = { 0, 6, 0, "(A,L4)", 0 };
    static cilist io___8 = { 0, 6, 0, "(A,L4)", 0 };
    static cilist io___9 = { 0, 6, 0, "(A,L4)", 0 };
    static cilist io___10 = { 0, 6, 0, "(A,L4)", 0 };
    static cilist io___11 = { 0, 6, 0, "(A,L4)", 0 };


    epsl = 1e-5f;
    for (i__ = 1; i__ <= 3; ++i__) {
	s_wsfe(&io___5);
	do_fio(&c__1, "X = ", (ftnlen)4);
	do_fio(&c__1, (char *)&x[i__ - 1], (ftnlen)sizeof(real));
	do_fio(&c__1, "; Y = ", (ftnlen)6);
	do_fio(&c__1, (char *)&y[i__ - 1], (ftnlen)sizeof(real));
	do_fio(&c__1, "; EPSL = ", (ftnlen)9);
	do_fio(&c__1, (char *)&epsl, (ftnlen)sizeof(real));
	do_fio(&c__1, ";", (ftnlen)1);
	e_wsfe();
	s_wsfe(&io___6);
	do_fio(&c__1, " LREQA(X,Y,EPSL)=", (ftnlen)17);
	L__1 = lreqa_(&x[i__ - 1], &y[i__ - 1], &epsl);
	do_fio(&c__1, (char *)&L__1, (ftnlen)sizeof(logical));
	e_wsfe();
	s_wsfe(&io___7);
	do_fio(&c__1, " LRNEA(X,Y,EPSL)=", (ftnlen)17);
	L__1 = lrnea_(&x[i__ - 1], &y[i__ - 1], &epsl);
	do_fio(&c__1, (char *)&L__1, (ftnlen)sizeof(logical));
	e_wsfe();
	s_wsfe(&io___8);
	do_fio(&c__1, " LRLTA(X,Y,EPSL)=", (ftnlen)17);
	L__1 = lrlta_(&x[i__ - 1], &y[i__ - 1], &epsl);
	do_fio(&c__1, (char *)&L__1, (ftnlen)sizeof(logical));
	e_wsfe();
	s_wsfe(&io___9);
	do_fio(&c__1, " LRLEA(X,Y,EPSL)=", (ftnlen)17);
	L__1 = lrlea_(&x[i__ - 1], &y[i__ - 1], &epsl);
	do_fio(&c__1, (char *)&L__1, (ftnlen)sizeof(logical));
	e_wsfe();
	s_wsfe(&io___10);
	do_fio(&c__1, " LRGTA(X,Y,EPSL)=", (ftnlen)17);
	L__1 = lrgta_(&x[i__ - 1], &y[i__ - 1], &epsl);
	do_fio(&c__1, (char *)&L__1, (ftnlen)sizeof(logical));
	e_wsfe();
	s_wsfe(&io___11);
	do_fio(&c__1, " LRGEA(X,Y,EPSL)=", (ftnlen)17);
	L__1 = lrgea_(&x[i__ - 1], &y[i__ - 1], &epsl);
	do_fio(&c__1, (char *)&L__1, (ftnlen)sizeof(logical));
	e_wsfe();
/* L10: */
    }
    return 0;
} /* MAIN__ */

/* Main program alias */ int lrll02_ () { MAIN__ (); return 0; }

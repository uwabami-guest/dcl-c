/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static real c_b7 = 0.f;
static real c_b8 = 12.56636f;
static real c_b9 = -1.f;
static real c_b10 = 1.f;
static real c_b13 = .7f;
static real c_b14 = .9f;
static integer c__4 = 4;
static integer c__41 = 41;
static real c_b24 = .5f;
static real c_b28 = .006f;
static real c_b31 = .003f;
static real c_b38 = .3f;
static real c_b49 = .1f;
static integer c__32 = 32;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Builtin functions */
    double sin(doublereal);
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static integer n;
    static real x[41], y[41], dt;
    static integer iws;
    extern /* Subroutine */ int sgcls_(void), sgfrm_(void), sgopn_(integer *),
	     sgplu_(integer *, real *, real *);
    static integer itype;
    extern /* Subroutine */ int bitpci_(char *, integer *, ftnlen), sgiset_(
	    char *, integer *, ftnlen), sgswnd_(real *, real *, real *, real *
	    ), sgrset_(char *, real *, ftnlen), sgstrf_(void), sgsplt_(
	    integer *), sgstrn_(integer *), sgpwsn_(void), sgsvpt_(real *, 
	    real *, real *, real *);

    /* Fortran I/O blocks */
    static cilist io___5 = { 0, 6, 0, 0, 0 };
    static cilist io___6 = { 0, 5, 0, 0, 0 };


    dt = .31415899999999997f;
    for (n = 0; n <= 40; ++n) {
	x[n] = n * dt;
	y[n] = sin(x[n]);
/* L10: */
    }
    s_wsle(&io___5);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___6);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    sgopn_(&iws);
    sgfrm_();
/* --  ラインタイプ = 4 (デフォルト) ---- */
    sgswnd_(&c_b7, &c_b8, &c_b9, &c_b10);
    sgsvpt_(&c_b7, &c_b10, &c_b13, &c_b14);
    sgstrn_(&c__1);
    sgstrf_();
    sgsplt_(&c__4);
    sgplu_(&c__41, x, y);
/* --  ラインタイプ = 4 (BITLEN*2) ---- */
    sgswnd_(&c_b7, &c_b8, &c_b9, &c_b10);
    sgsvpt_(&c_b7, &c_b10, &c_b24, &c_b13);
    sgstrn_(&c__1);
    sgstrf_();
    sgrset_("BITLEN", &c_b28, (ftnlen)6);
    sgplu_(&c__41, x, y);
    sgrset_("BITLEN", &c_b31, (ftnlen)6);
/* --  ビットパターン ---- */
    sgswnd_(&c_b7, &c_b8, &c_b9, &c_b10);
    sgsvpt_(&c_b7, &c_b10, &c_b38, &c_b24);
    sgstrn_(&c__1);
    sgstrf_();
    bitpci_("1111111100100100", &itype, (ftnlen)16);
    sgsplt_(&itype);
    sgplu_(&c__41, x, y);
/* --  ビットパターン(倍長) ---- */
    sgswnd_(&c_b7, &c_b8, &c_b9, &c_b10);
    sgsvpt_(&c_b7, &c_b10, &c_b49, &c_b38);
    sgstrn_(&c__1);
    sgstrf_();
    sgiset_("NBITS", &c__32, (ftnlen)5);
    bitpci_("10010010011111000111110001111100", &itype, (ftnlen)32);
    sgsplt_(&itype);
    sgplu_(&c__41, x, y);
    sgcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int kihon9_ () { MAIN__ (); return 0; }

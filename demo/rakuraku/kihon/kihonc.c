/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static real c_b6 = 0.f;
static real c_b7 = 10.f;
static real c_b11 = 1.f;
static logical c_false = FALSE_;
static real c_b23 = .03f;
static logical c_true = TRUE_;
static integer c__655 = 655;
static real c_b38 = 3.f;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* System generated locals */
    real r__1;

    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static integer i__;
    static real x1, y1, x2, y2;
    static integer iws;
    extern /* Subroutine */ int sglau_(real *, real *, real *, real *), 
	    sgcls_(void), sgfrm_(void), sgopn_(integer *), sglnu_(real *, 
	    real *, real *, real *), sgslai_(integer *), sgslat_(integer *), 
	    sgiset_(char *, integer *, ftnlen), sgslni_(integer *), sglset_(
	    char *, logical *, ftnlen), sgswnd_(real *, real *, real *, real *
	    ), sgrset_(char *, real *, ftnlen), sgstrf_(void), sgstrn_(
	    integer *), sgpwsn_(void), sgsvpt_(real *, real *, real *, real *)
	    ;

    /* Fortran I/O blocks */
    static cilist io___1 = { 0, 6, 0, 0, 0 };
    static cilist io___2 = { 0, 5, 0, 0, 0 };


    s_wsle(&io___1);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___2);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    sgopn_(&iws);
    sgfrm_();
    sgswnd_(&c_b6, &c_b7, &c_b6, &c_b7);
    sgsvpt_(&c_b6, &c_b11, &c_b6, &c_b11);
    sgstrn_(&c__1);
    sgstrf_();
/* -- デフォルト ---- */
    y1 = 9.f;
    for (i__ = 1; i__ <= 8; ++i__) {
	x1 = (real) i__;
	x2 = x1 + i__ * .1f;
	y2 = y1 + i__ * .1f;
	sglau_(&x1, &y1, &x2, &y2);
/* L10: */
    }
/* -- 線分のラインタイプ ---- */
    y1 = 8.f;
    y2 = 8.6f;
    for (i__ = 1; i__ <= 4; ++i__) {
	x1 = (real) ((i__ << 1) - 1);
	x2 = x1 + .6f;
	sgslat_(&i__);
	sglau_(&x1, &y1, &x2, &y2);
/* L20: */
    }
    sgslat_(&c__1);
/* -- 線分のラインインデクス ---- */
    y1 = 7.f;
    y2 = 7.6f;
    for (i__ = 1; i__ <= 4; ++i__) {
	x1 = (real) (i__ << 1);
	x2 = x1 + .6f;
	sgslai_(&i__);
	sglau_(&x1, &y1, &x2, &y2);
/* L30: */
    }
    sgslai_(&c__1);
/* -- 矢じり部分の長さ ---- */
    sglset_("LPROP", &c_false, (ftnlen)5);
    sgrset_("CONST", &c_b23, (ftnlen)5);
    y1 = 6.f;
    for (i__ = 1; i__ <= 8; ++i__) {
	x1 = (real) i__;
	x2 = x1 + i__ * .1f;
	y2 = y1 + i__ * .1f;
	sglau_(&x1, &y1, &x2, &y2);
/* L40: */
    }
    sglset_("LPROP", &c_true, (ftnlen)5);
/* -- 矢じり部分の角度 ---- */
    y1 = 5.f;
    y2 = 5.6f;
    for (i__ = 1; i__ <= 8; ++i__) {
	x1 = (real) i__;
	x2 = x1 + .6f;
	r__1 = i__ * 10.f;
	sgrset_("ANGLE", &r__1, (ftnlen)5);
	sglau_(&x1, &y1, &x2, &y2);
/* L50: */
    }
/* -- 矢じり部分のぬりつぶし ---- */
    sglset_("LSOFTF", &c_true, (ftnlen)6);
    sglset_("LATONE", &c_true, (ftnlen)6);
    sgiset_("IATONE", &c__655, (ftnlen)6);
    y1 = 4.f;
    y2 = 4.6f;
    for (i__ = 1; i__ <= 8; ++i__) {
	x1 = (real) i__;
	x2 = x1 + .6f;
	r__1 = i__ * 10.f;
	sgrset_("ANGLE", &r__1, (ftnlen)5);
	sglau_(&x1, &y1, &x2, &y2);
/* L60: */
    }
/* -- ラインサブプリミティブ ---- */
/* -- デフォルト ---- */
    sglnu_(&c_b6, &c_b38, &c_b7, &c_b38);
/* -- 線分のラインインデクス ---- */
    y1 = .5f;
    y2 = 2.5f;
    for (i__ = 1; i__ <= 4; ++i__) {
	x1 = (real) ((i__ << 1) - 1);
	x2 = x1 + 2.f;
	sgslni_(&i__);
	sglnu_(&x1, &y1, &x2, &y2);
/* L70: */
    }
    sgcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int kihonc_ () { MAIN__ (); return 0; }

/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static real c_b6 = -100.f;
static real c_b7 = 100.f;
static real c_b10 = 0.f;
static real c_b11 = 1.f;
static real c_b19 = .1f;
static real c_b20 = .4f;
static real c_b21 = .6f;
static real c_b22 = .9f;
static real c_b26 = -40.f;
static real c_b27 = 40.f;
static real c_b35 = -200.f;
static integer c__41 = 41;
static integer c__4 = 4;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static integer iws;
    extern /* Subroutine */ int sgcls_(void), sgfrm_(void), aplot_(void), 
	    sgopn_(integer *), sgswnd_(real *, real *, real *, real *), 
	    sgstrf_(void), sgstrn_(integer *), sgpwsn_(void), sgsvpt_(real *, 
	    real *, real *, real *);

    /* Fortran I/O blocks */
    static cilist io___1 = { 0, 6, 0, 0, 0 };
    static cilist io___2 = { 0, 5, 0, 0, 0 };


    s_wsle(&io___1);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___2);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    sgopn_(&iws);
/*      CALL SGLSET( 'LCLIP', .TRUE. ) */
/* -- frame 1 ---- */
    sgfrm_();
/*                  XMIN,  XMAX,  YMIN, YMAX */
    sgswnd_(&c_b6, &c_b7, &c_b6, &c_b7);
    sgsvpt_(&c_b10, &c_b11, &c_b10, &c_b11);
    sgstrn_(&c__1);
    sgstrf_();
    aplot_();
/* -- frame 2 ---- */
    sgfrm_();
/* -- 左上 ----      XMIN,  XMAX,  YMIN, YMAX */
    sgswnd_(&c_b6, &c_b7, &c_b6, &c_b7);
    sgsvpt_(&c_b19, &c_b20, &c_b21, &c_b22);
    sgstrn_(&c__1);
    sgstrf_();
    aplot_();
/* -- 右上 ----       XMIN, XMAX, YMIN, YMAX */
    sgswnd_(&c_b6, &c_b7, &c_b26, &c_b27);
    sgsvpt_(&c_b21, &c_b22, &c_b21, &c_b22);
    sgstrn_(&c__1);
    sgstrf_();
    aplot_();
/* -- 左下 ----       XMIN, XMAX,  YMIN, YMAX */
    sgswnd_(&c_b6, &c_b7, &c_b35, &c_b10);
    sgsvpt_(&c_b19, &c_b20, &c_b19, &c_b20);
    sgstrn_(&c__1);
    sgstrf_();
    aplot_();
/* -- 右下 ----       XMIN, XMAX, YMIN,  YMAX */
    sgswnd_(&c_b6, &c_b7, &c_b10, &c_b35);
    sgsvpt_(&c_b21, &c_b22, &c_b19, &c_b20);
    sgstrn_(&c__1);
    sgstrf_();
    aplot_();
    sgcls_();
    return 0;
} /* MAIN__ */

/* ----------------------------------------------------------------------- */
/* Subroutine */ int aplot_(void)
{
    /* Builtin functions */
    double sin(doublereal), cos(doublereal);

    /* Local variables */
    static integer n;
    static real x[41], y[41], dt;
    extern /* Subroutine */ int sgplu_(integer *, real *, real *), sgtxu_(
	    real *, real *, char *, ftnlen), slpvpr_(integer *);

    slpvpr_(&c__1);
/* -- 円形 ---- */
    dt = .15707949999999998f;
    for (n = 0; n <= 40; ++n) {
	x[n] = sin(n * dt) * 40.f;
	y[n] = cos(n * dt) * 40.f;
/* L10: */
    }
    sgplu_(&c__41, x, y);
/* -- 三角形 ---- */
    dt = 2.0943933333333331f;
    for (n = 0; n <= 3; ++n) {
	x[n] = sin(n * dt) * 40.f;
	y[n] = cos(n * dt) * 40.f;
/* L20: */
    }
    sgplu_(&c__4, x, y);
/* -- 文字列 ---- */
    sgtxu_(&c_b10, &c_b10, "SGTXU", (ftnlen)5);
    return 0;
} /* aplot_ */

/* Main program alias */ int kihon7_ () { MAIN__ (); return 0; }

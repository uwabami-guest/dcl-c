/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static logical c_true = TRUE_;
static real c_b8 = 1.f;
static real c_b9 = 100.f;
static real c_b12 = .1f;
static real c_b13 = .4f;
static real c_b14 = .6f;
static real c_b15 = .9f;
static integer c__2 = 2;
static integer c__4 = 4;
static integer c__50 = 50;
static real c_b55 = .02f;
static real c_b56 = 30.f;
static real c_b57 = 40.f;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static integer iws;
    extern /* Subroutine */ int sgcls_(void), sgfrm_(void), bplot_(void), 
	    sgopn_(integer *), sglset_(char *, logical *, ftnlen), sgswnd_(
	    real *, real *, real *, real *), sgstrf_(void), sgstrn_(integer *)
	    , sgpwsn_(void), sgsvpt_(real *, real *, real *, real *);

    /* Fortran I/O blocks */
    static cilist io___1 = { 0, 6, 0, 0, 0 };
    static cilist io___2 = { 0, 5, 0, 0, 0 };


    s_wsle(&io___1);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___2);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    sgopn_(&iws);
    sglset_("LCLIP", &c_true, (ftnlen)5);
    sgfrm_();
/* -- 直角一様座標(左上) ---- */
    sgswnd_(&c_b8, &c_b9, &c_b8, &c_b9);
    sgsvpt_(&c_b12, &c_b13, &c_b14, &c_b15);
    sgstrn_(&c__1);
    sgstrf_();
    bplot_();
/* -- 片対数(y)座標(右上) ---- */
    sgswnd_(&c_b8, &c_b9, &c_b8, &c_b9);
    sgsvpt_(&c_b14, &c_b15, &c_b14, &c_b15);
    sgstrn_(&c__2);
    sgstrf_();
    bplot_();
/* -- 片対数(x)座標(左下) ---- */
    sgswnd_(&c_b8, &c_b9, &c_b8, &c_b9);
    sgsvpt_(&c_b12, &c_b13, &c_b12, &c_b13);
    sgstrn_(&c__3);
    sgstrf_();
    bplot_();
/* -- 対数座標(右下) ---- */
    sgswnd_(&c_b8, &c_b9, &c_b8, &c_b9);
    sgsvpt_(&c_b14, &c_b15, &c_b12, &c_b13);
    sgstrn_(&c__4);
    sgstrf_();
    bplot_();
    sgcls_();
    return 0;
} /* MAIN__ */

/* ----------------------------------------------------------------------- */
/* Subroutine */ int bplot_(void)
{
    /* Builtin functions */
    double exp(doublereal), log(doublereal);

    /* Local variables */
    static integer n;
    static real x[50], y[50];
    extern /* Subroutine */ int sgplu_(integer *, real *, real *), sgtxu_(
	    real *, real *, char *, ftnlen), sgsplt_(integer *), slpvpr_(
	    integer *), sgstxs_(real *);

    slpvpr_(&c__1);
/* -- 一次関数 ---- */
    for (n = 1; n <= 50; ++n) {
	x[n - 1] = n * 2.f;
	y[n - 1] = x[n - 1];
/* L10: */
    }
    sgsplt_(&c__1);
    sgplu_(&c__50, x, y);
/* -- 指数関数 ---- */
    for (n = 1; n <= 50; ++n) {
	y[n - 1] = exp(x[n - 1] * .05f);
/* L20: */
    }
    sgsplt_(&c__2);
    sgplu_(&c__50, x, y);
/* -- 対数関数 ---- */
    for (n = 1; n <= 50; ++n) {
	y[n - 1] = log(x[n - 1]) * 20.f;
/* L30: */
    }
    sgsplt_(&c__3);
    sgplu_(&c__50, x, y);
/* -- 文字列 ---- */
    sgstxs_(&c_b55);
    sgtxu_(&c_b56, &c_b57, "(30,40)", (ftnlen)7);
    return 0;
} /* bplot_ */

/* Main program alias */ int kihon8_ () { MAIN__ (); return 0; }

/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__2 = 2;
static integer c__51 = 51;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    static integer n;
    static real r__, x[51], y[51];
    extern /* Subroutine */ int grcls_(void), grfrm_(void), gropn_(integer *),
	     usgrph_(integer *, real *, real *);

/* -- ロジスティク模型 ---- */
    r__ = 3.7f;
    x[0] = 0.f;
    y[0] = .5f;
    for (n = 0; n <= 49; ++n) {
	x[n + 1] = x[n] + 1.f;
	y[n + 1] = r__ * y[n] * (1.f - y[n]);
/* L10: */
    }
/* -- グラフ ---- */
    gropn_(&c__2);
    grfrm_();
    usgrph_(&c__51, x, y);
    grcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int quick1_ () { MAIN__ (); return 0; }

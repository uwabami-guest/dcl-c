/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static real c_b9 = 0.f;
static real c_b10 = 360.f;
static real c_b11 = -90.f;
static real c_b12 = 90.f;
static real c_b13 = -.4f;
static real c_b14 = .4f;
static real c_b15 = -.3f;
static real c_b16 = .3f;
static integer c__2 = 2;
static real c_b20 = -.2f;
static real c_b21 = -1.1f;
static real c_b23 = 2.5f;
static real c_b28 = 10.f;
static real c_b29 = 60.f;
static real c_b38 = 30.f;
static real c_b47 = -1.9f;
static real c_b48 = 1.9f;
static real c_b52 = .2f;
static logical c_false = FALSE_;
static real c_b67 = 1.f;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* System generated locals */
    real r__1, r__2;

    /* Builtin functions */
    double sin(doublereal), sqrt(doublereal), cos(doublereal);
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static integer i__, j;
    static real p[1369]	/* was [37][37] */, xp[3], yp[3], zp[3];
    static integer iws;
    static real alat[37], alon[37], slat;
    extern /* Subroutine */ int sgcls_(void), sgfrm_(void), sgopn_(integer *),
	     szl3cl_(void), szl3op_(integer *), szl3zu_(integer *, real *, 
	    real *, real *), scsobj_(real *, real *, real *), scslog_(logical 
	    *, logical *, logical *), scseye_(real *, real *, real *), 
	    scswnd_(real *, real *, real *, real *, real *, real *), scspln_(
	    integer *, integer *, real *), scsprj_(void), sgswnd_(real *, 
	    real *, real *, real *), scstrf_(void), sgstrf_(void), scstrn_(
	    integer *), uxaxdv_(char *, real *, real *, ftnlen), sgstrn_(
	    integer *), sgpwsn_(void), uyaxdv_(char *, real *, real *, ftnlen)
	    , uzinit_(void), scsvpt_(real *, real *, real *, real *, real *, 
	    real *), sgsvpt_(real *, real *, real *, real *), uzlset_(char *, 
	    logical *, ftnlen), uxsttl_(char *, char *, real *, ftnlen, 
	    ftnlen), uysttl_(char *, char *, real *, ftnlen, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___7 = { 0, 6, 0, 0, 0 };
    static cilist io___8 = { 0, 5, 0, 0, 0 };


    for (i__ = 1; i__ <= 37; ++i__) {
	alon[i__ - 1] = (i__ - 1) * 360.f / 36 + 0.f;
/* L10: */
    }
    for (j = 1; j <= 37; ++j) {
	alat[j - 1] = (j - 1) * 180.f / 36 - 90.f;
/* L20: */
    }
    for (j = 1; j <= 37; ++j) {
	slat = sin(alat[j - 1] * .017453277777777776f);
	for (i__ = 1; i__ <= 37; ++i__) {
/* Computing 2nd power */
	    r__1 = slat;
/* Computing 2nd power */
	    r__2 = slat;
	    p[i__ + j * 37 - 38] = sqrt(1 - r__1 * r__1) * 3 * slat * cos(
		    alon[i__ - 1] * .017453277777777776f) - (r__2 * r__2 * 3 
		    - 1) * .5f;
/* L30: */
	}
    }
    s_wsle(&io___7);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I) ? ;", (ftnlen)23);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___8);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    sgopn_(&iws);
    sgfrm_();
/* -- X-Y PLANE ---- */
    sgswnd_(&c_b9, &c_b10, &c_b11, &c_b12);
    sgsvpt_(&c_b13, &c_b14, &c_b15, &c_b16);
    sgstrn_(&c__1);
    sgstrf_();
    scspln_(&c__1, &c__2, &c_b20);
    scseye_(&c_b21, &c_b21, &c_b23);
    scsobj_(&c_b9, &c_b9, &c_b9);
    scsprj_();
    uxaxdv_("B", &c_b28, &c_b29, (ftnlen)1);
    uxaxdv_("T", &c_b28, &c_b29, (ftnlen)1);
    uxsttl_("B", "LONGITUDE", &c_b9, (ftnlen)1, (ftnlen)9);
    uyaxdv_("L", &c_b28, &c_b38, (ftnlen)1);
    uyaxdv_("R", &c_b28, &c_b38, (ftnlen)1);
    uysttl_("L", "LATITUDE", &c_b9, (ftnlen)1, (ftnlen)8);
/* -- X-Z PLANE ---- */
    sgswnd_(&c_b9, &c_b10, &c_b47, &c_b48);
    sgsvpt_(&c_b13, &c_b14, &c_b20, &c_b52);
    sgstrn_(&c__1);
    sgstrf_();
    scspln_(&c__1, &c__3, &c_b16);
    scsprj_();
    uzinit_();
    uxaxdv_("T", &c_b28, &c_b29, (ftnlen)1);
    uzlset_("LABELXB", &c_false, (ftnlen)7);
    uxaxdv_("B", &c_b28, &c_b29, (ftnlen)1);
    uyaxdv_("L", &c_b52, &c_b67, (ftnlen)1);
    uyaxdv_("R", &c_b52, &c_b67, (ftnlen)1);
    uysttl_("L", "AMPLITUDE", &c_b9, (ftnlen)1, (ftnlen)9);
/* -- Y-Z PLANE ---- */
    sgswnd_(&c_b11, &c_b12, &c_b47, &c_b48);
    sgsvpt_(&c_b15, &c_b16, &c_b20, &c_b52);
    sgstrn_(&c__1);
    sgstrf_();
    scspln_(&c__2, &c__3, &c_b14);
    scsprj_();
    uzinit_();
    uxaxdv_("T", &c_b28, &c_b38, (ftnlen)1);
    uxaxdv_("B", &c_b28, &c_b38, (ftnlen)1);
    uzlset_("LABELYL", &c_false, (ftnlen)7);
    uyaxdv_("L", &c_b52, &c_b67, (ftnlen)1);
    uyaxdv_("R", &c_b52, &c_b67, (ftnlen)1);
/* ---------------- 3-D ------------------ */
    scsvpt_(&c_b13, &c_b14, &c_b15, &c_b16, &c_b20, &c_b52);
    scswnd_(&c_b9, &c_b10, &c_b11, &c_b12, &c_b47, &c_b48);
    scslog_(&c_false, &c_false, &c_false);
    scstrn_(&c__1);
    scstrf_();
    szl3op_(&c__1);
/*      CALL SZT3OP(2999,4999) */
    for (j = 36; j >= 1; --j) {
	for (i__ = 36; i__ >= 1; --i__) {
	    xp[0] = alon[i__ - 1];
	    yp[0] = alat[j - 1];
	    zp[0] = p[i__ + j * 37 - 38];
	    xp[1] = alon[i__ - 1];
	    yp[1] = alat[j];
	    zp[1] = p[i__ + (j + 1) * 37 - 38];
	    xp[2] = alon[i__];
	    yp[2] = alat[j];
	    zp[2] = p[i__ + 1 + (j + 1) * 37 - 38];
/*        CALL SZT3ZU(XP, YP, ZP) */
	    szl3zu_(&c__3, xp, yp, zp);
	    xp[0] = alon[i__];
	    yp[0] = alat[j];
	    zp[0] = p[i__ + 1 + (j + 1) * 37 - 38];
	    xp[1] = alon[i__];
	    yp[1] = alat[j - 1];
	    zp[1] = p[i__ + 1 + j * 37 - 38];
	    xp[2] = alon[i__ - 1];
	    yp[2] = alat[j - 1];
	    zp[2] = p[i__ + j * 37 - 38];
/*        CALL SZT3ZU(XP, YP, ZP) */
	    szl3zu_(&c__3, xp, yp, zp);
/* L40: */
	}
    }
    szl3cl_();
/*      CALL SZT3CL */
    sgcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int map3d6_ () { MAIN__ (); return 0; }

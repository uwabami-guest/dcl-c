/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static integer c__2 = 2;
static real c_b8 = -50.f;
static real c_b9 = 50.f;
static real c_b12 = 0.f;
static real c_b13 = .8f;
static real c_b20 = 2.5f;
static real c_b21 = -1.f;
static real c_b22 = 1.5f;
static real c_b23 = .40000000000000002f;
static integer c__41 = 41;
static integer c__4 = 4;
static real c_b59 = .07f;
static real c_b66 = -30.f;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static integer iws;
    extern /* Subroutine */ int sgcls_(void), sgfrm_(void), aplot_(integer *),
	     sgopn_(integer *), scsobj_(real *, real *, real *), scseye_(real 
	    *, real *, real *), sgiset_(char *, integer *, ftnlen), scspln_(
	    integer *, integer *, real *), scsprj_(void), sgswnd_(real *, 
	    real *, real *, real *), sgstrf_(void), sgstrn_(integer *), 
	    sgpwsn_(void), sgsvpt_(real *, real *, real *, real *);

    /* Fortran I/O blocks */
    static cilist io___1 = { 0, 6, 0, 0, 0 };
    static cilist io___2 = { 0, 5, 0, 0, 0 };


    s_wsle(&io___1);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I) ? ;", (ftnlen)23);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___2);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    sgopn_(&iws);
    sgiset_("IFONT", &c__2, (ftnlen)5);
    sgfrm_();
/* -- X-Y PLANE ---- */
    sgswnd_(&c_b8, &c_b9, &c_b8, &c_b9);
    sgsvpt_(&c_b12, &c_b13, &c_b12, &c_b13);
    sgstrn_(&c__1);
    sgstrf_();
    scspln_(&c__1, &c__2, &c_b13);
    scseye_(&c_b20, &c_b21, &c_b22);
    scsobj_(&c_b23, &c_b23, &c_b23);
    scsprj_();
    aplot_(&c__1);
/* -- X-Z PLANE ---- */
    sgswnd_(&c_b8, &c_b9, &c_b8, &c_b9);
    sgsvpt_(&c_b12, &c_b13, &c_b12, &c_b13);
    sgstrn_(&c__1);
    sgstrf_();
    scspln_(&c__1, &c__3, &c_b12);
    scsprj_();
    aplot_(&c__2);
/* -- Y-Z PLANE ---- */
    sgswnd_(&c_b8, &c_b9, &c_b8, &c_b9);
    sgsvpt_(&c_b12, &c_b13, &c_b12, &c_b13);
    sgstrn_(&c__1);
    sgstrf_();
    scspln_(&c__2, &c__3, &c_b13);
    scsprj_();
    aplot_(&c__3);
    sgcls_();
    return 0;
} /* MAIN__ */

/* ----------------------------------------------------------------------- */
/* Subroutine */ int aplot_(integer *ijk)
{
    /* Builtin functions */
    double sin(doublereal), cos(doublereal);
    integer s_wsfi(icilist *), do_fio(integer *, char *, ftnlen), e_wsfi(void)
	    ;

    /* Local variables */
    static integer n;
    static real x[41], y[41], dt;
    static char cttl[2];
    extern /* Subroutine */ int sgplu_(integer *, real *, real *), sgtxu_(
	    real *, real *, char *, ftnlen), slpvpr_(integer *), sgstxs_(real 
	    *);

    /* Fortran I/O blocks */
    static icilist io___9 = { 0, cttl, 0, "(I2.2)", 2, 1 };


    slpvpr_(&c__1);
    dt = .15707949999999998f;
    for (n = 0; n <= 40; ++n) {
	x[n] = sin(n * dt) * 40.f;
	y[n] = cos(n * dt) * 40.f;
/* L10: */
    }
    sgplu_(&c__41, x, y);
    dt = 2.0943933333333331f;
    for (n = 0; n <= 3; ++n) {
	x[n] = sin(n * dt) * 40.f;
	y[n] = cos(n * dt) * 40.f;
/* L20: */
    }
    sgplu_(&c__4, x, y);
    sgstxs_(&c_b59);
    sgtxu_(&c_b12, &c_b12, "DENNOU", (ftnlen)6);
    s_wsfi(&io___9);
    do_fio(&c__1, (char *)&(*ijk), (ftnlen)sizeof(integer));
    e_wsfi();
    sgtxu_(&c_b12, &c_b66, cttl, (ftnlen)2);
    return 0;
} /* aplot_ */

/* Main program alias */ int map3d4_ () { MAIN__ (); return 0; }

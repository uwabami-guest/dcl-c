/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static real c_b7 = 0.f;
static real c_b8 = 12.56636f;
static real c_b9 = -1.f;
static real c_b10 = 1.f;
static real c_b11 = .1f;
static real c_b12 = .9f;
static real c_b13 = .7f;
static integer c__41 = 41;
static real c_b23 = .5f;
static logical c_true = TRUE_;
static real c_b36 = .3f;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Builtin functions */
    double sin(doublereal);
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static integer n;
    static real x[41], y[41];
    static integer n1, n2;
    static real dt;
    static integer iws;
    extern /* Subroutine */ int grcls_(void), grfrm_(void), gropn_(integer *),
	     sgplu_(integer *, real *, real *), sgpmu_(integer *, real *, 
	    real *), gllset_(char *, logical *, ftnlen), grswnd_(real *, real 
	    *, real *, real *), grstrf_(void), sgspmt_(integer *), grstrn_(
	    integer *), sgpwsn_(void), grsvpt_(real *, real *, real *, real *)
	    ;

    /* Fortran I/O blocks */
    static cilist io___7 = { 0, 6, 0, 0, 0 };
    static cilist io___8 = { 0, 5, 0, 0, 0 };


    dt = .31415899999999997f;
    for (n = 0; n <= 40; ++n) {
	x[n] = n * dt;
	y[n] = sin(x[n]);
/* L10: */
    }
    n1 = 10;
    y[n1 - 1] = 999.f;
    y[n1] = 999.f;
    y[n1 + 1] = 999.f;
    n2 = n1 * 3;
    y[n2 - 1] = 999.f;
    y[n2 + 1] = 999.f;
    s_wsle(&io___7);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___8);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    gropn_(&iws);
    grfrm_();
/* -- 欠損値処理なし ---- */
    grswnd_(&c_b7, &c_b8, &c_b9, &c_b10);
    grsvpt_(&c_b11, &c_b12, &c_b13, &c_b12);
    grstrn_(&c__1);
    grstrf_();
    sgplu_(&c__41, x, y);
    grswnd_(&c_b7, &c_b8, &c_b9, &c_b10);
    grsvpt_(&c_b11, &c_b12, &c_b23, &c_b13);
    grstrn_(&c__1);
    grstrf_();
    sgspmt_(&c__3);
    sgpmu_(&c__41, x, y);
/* -- 欠損値処理 ---- */
    gllset_("LMISS", &c_true, (ftnlen)5);
    grswnd_(&c_b7, &c_b8, &c_b9, &c_b10);
    grsvpt_(&c_b11, &c_b12, &c_b36, &c_b23);
    grstrn_(&c__1);
    grstrf_();
    sgplu_(&c__41, x, y);
    grswnd_(&c_b7, &c_b8, &c_b9, &c_b10);
    grsvpt_(&c_b11, &c_b12, &c_b11, &c_b36);
    grstrn_(&c__1);
    grstrf_();
    sgpmu_(&c__41, x, y);
    grcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int miss1_ () { MAIN__ (); return 0; }

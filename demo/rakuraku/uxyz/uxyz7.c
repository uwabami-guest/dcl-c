/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static real c_b6 = 1990.f;
static real c_b7 = 1995.f;
static real c_b8 = -2.f;
static real c_b9 = 2.f;
static real c_b10 = .2f;
static real c_b11 = .8f;
static real c_b12 = .3f;
static real c_b13 = .7f;
static real c_b16 = 0.f;
static logical c_true = TRUE_;
static real c_b21 = .25f;
static real c_b22 = 1.f;
static logical c_false = FALSE_;
static integer c_n1 = -1;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static integer iws;
    extern /* Subroutine */ int grcls_(void), grfrm_(void), gropn_(integer *),
	     grswnd_(real *, real *, real *, real *), grstrf_(void), grstrn_(
	    integer *), uxaxdv_(char *, real *, real *, ftnlen), sgpwsn_(void)
	    , uyaxdv_(char *, real *, real *, ftnlen), uziset_(char *, 
	    integer *, ftnlen), grsvpt_(real *, real *, real *, real *), 
	    uzlset_(char *, logical *, ftnlen), uxsfmt_(char *, ftnlen), 
	    uysfmt_(char *, ftnlen), uzrset_(char *, real *, ftnlen), uxmttl_(
	    char *, char *, real *, ftnlen, ftnlen), uxsttl_(char *, char *, 
	    real *, ftnlen, ftnlen), uysttl_(char *, char *, real *, ftnlen, 
	    ftnlen);

    /* Fortran I/O blocks */
    static cilist io___1 = { 0, 6, 0, 0, 0 };
    static cilist io___2 = { 0, 5, 0, 0, 0 };


    s_wsle(&io___1);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___2);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    gropn_(&iws);
    grfrm_();
    grswnd_(&c_b6, &c_b7, &c_b8, &c_b9);
    grsvpt_(&c_b10, &c_b11, &c_b12, &c_b13);
    grstrn_(&c__1);
    grstrf_();
    uzrset_("UYUSER", &c_b16, (ftnlen)6);
    uzlset_("LBTWN", &c_true, (ftnlen)5);
    uxsfmt_("(I4)", (ftnlen)4);
    uxaxdv_("U", &c_b21, &c_b22, (ftnlen)1);
    uzlset_("LBTWN", &c_false, (ftnlen)5);
    uxsttl_("U", "YEAR", &c_b22, (ftnlen)1, (ftnlen)4);
    uziset_("INNER", &c_n1, (ftnlen)5);
    uysfmt_("(F4.1)", (ftnlen)6);
    uyaxdv_("L", &c_b21, &c_b22, (ftnlen)1);
    uysttl_("L", "S.O.Index", &c_b16, (ftnlen)1, (ftnlen)9);
    uxmttl_("T", "UXAXDV/UYAXDV", &c_b16, (ftnlen)1, (ftnlen)13);
    grcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int uxyz7_ () { MAIN__ (); return 0; }

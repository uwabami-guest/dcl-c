/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__51 = 51;
static integer c__1 = 1;
static integer c__9 = 9;
static integer c__3 = 3;
static real c_b10 = -1.5f;
static real c_b11 = 2.f;
static real c_b12 = .2f;
static real c_b13 = .9f;
static integer c__2 = 2;
static integer c__402 = 402;
static integer c__404 = 404;
static integer c__303 = 303;
static integer c__123 = 123;
static real c_b29 = 10.f;
static real c_b35 = 0.f;
static real c_b37 = .1f;
static real c_b38 = .5f;
static integer c__131 = 131;
static integer c__24 = 24;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* System generated locals */
    address a__1[2];
    integer i__1[2];
    char ch__1[3];

    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen);

    /* Local variables */
    static integer n;
    static real r__, x[51], y[51];
    static integer iws;
    extern real rave_(real *, integer *, integer *);
    static real yave;
    static char cttl[32];
    extern /* Character */ VOID usgi_(char *, ftnlen, integer *);
    extern /* Subroutine */ int grcls_(void), grfrm_(void), gropn_(integer *),
	     sgplu_(integer *, real *, real *), sgiset_(char *, integer *, 
	    ftnlen), sgspli_(integer *), grswnd_(real *, real *, real *, real 
	    *), grstrf_(void), grstrn_(integer *), uxaxdv_(char *, real *, 
	    real *, ftnlen), sgpwsn_(void), uyaxdv_(char *, real *, real *, 
	    ftnlen), uziset_(char *, integer *, ftnlen), grsvpt_(real *, real 
	    *, real *, real *), uxmttl_(char *, char *, real *, ftnlen, 
	    ftnlen), uxsttl_(char *, char *, real *, ftnlen, ftnlen), uysttl_(
	    char *, char *, real *, ftnlen, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___6 = { 0, 6, 0, 0, 0 };
    static cilist io___7 = { 0, 5, 0, 0, 0 };


    r__ = 3.7f;
    x[0] = 1950.f;
    y[0] = .5f;
    for (n = 0; n <= 49; ++n) {
	x[n + 1] = x[n] + 1;
	y[n + 1] = r__ * y[n] * (1.f - y[n]);
/* L10: */
    }
    yave = rave_(y, &c__51, &c__1);
    for (n = 0; n <= 50; ++n) {
	y[n] = (y[n] - yave) * -4;
/* L20: */
    }
    s_wsle(&io___6);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___7);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    gropn_(&iws);
    grfrm_();
    grswnd_(x, &x[50], &c_b10, &c_b11);
    grsvpt_(&c_b12, &c_b13, &c_b12, &c_b13);
    grstrn_(&c__1);
    grstrf_();
    sgiset_("IFONT", &c__2, (ftnlen)5);
    uziset_("INDEXT1", &c__402, (ftnlen)7);
    uziset_("INDEXT2", &c__404, (ftnlen)7);
    uziset_("INDEXL1", &c__303, (ftnlen)7);
    uziset_("INDEXL2", &c__123, (ftnlen)7);
    uxaxdv_("B", &c_b11, &c_b29, (ftnlen)1);
    uxaxdv_("T", &c_b11, &c_b29, (ftnlen)1);
    uxsttl_("B", "YEAR", &c_b35, (ftnlen)1, (ftnlen)4);
    uyaxdv_("L", &c_b37, &c_b38, (ftnlen)1);
    uyaxdv_("R", &c_b37, &c_b38, (ftnlen)1);
/* Writing concatenation */
    usgi_(ch__1, (ftnlen)3, &c__131);
    i__1[0] = 3, a__1[0] = ch__1;
    i__1[1] = 5, a__1[1] = "T [K]";
    s_cat(cttl, a__1, i__1, &c__2, (ftnlen)32);
    uysttl_("L", cttl, &c_b35, (ftnlen)1, (ftnlen)32);
    uxmttl_("T", "INTERANNUAL VARIATION", &c_b35, (ftnlen)1, (ftnlen)21);
    sgspli_(&c__24);
    sgplu_(&c__51, x, y);
    grcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int color2_ () { MAIN__ (); return 0; }

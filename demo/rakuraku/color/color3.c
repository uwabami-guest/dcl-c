/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static real c_b7 = -10.f;
static real c_b8 = 10.f;
static real c_b11 = .2f;
static real c_b12 = .8f;
static integer c__21 = 21;
static real c_b20 = 0.f;
static real c_b21 = 1.f;
static real c_b24 = .85f;
static real c_b25 = .9f;
static integer c__2 = 2;
static integer c__57 = 57;
static logical c_true = TRUE_;
static real c_b39 = .1f;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* System generated locals */
    real r__1, r__2;

    /* Builtin functions */
    double exp(doublereal);
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static integer i__, j, k;
    static real p[441]	/* was [21][21] */, x, y, dp, pi[114]	/* was [2][57]
	     */;
    static integer iws, ipat;
    static real tlev1, tlev2;
    extern /* Subroutine */ int grcls_(void), grfrm_(void), gropn_(integer *),
	     uzfact_(real *), uetone_(real *, integer *, integer *, integer *)
	    , grswnd_(real *, real *, real *, real *), grstrf_(void), usdaxs_(
	    void), grstrn_(integer *), uyaxdv_(char *, real *, real *, ftnlen)
	    , sgpwsn_(void), uestlv_(real *, real *, integer *), grsvpt_(real 
	    *, real *, real *, real *), slpvpr_(integer *), uzlset_(char *, 
	    logical *, ftnlen), uysfmt_(char *, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___6 = { 0, 6, 0, 0, 0 };
    static cilist io___7 = { 0, 5, 0, 0, 0 };


    for (j = 1; j <= 21; ++j) {
	for (i__ = 1; i__ <= 21; ++i__) {
	    x = (i__ - 1) * 20.f / 20 - 10.f;
	    y = (j - 1) * 20.f / 20 - 10.f;
/* Computing 2nd power */
	    r__1 = x;
/* Computing 2nd power */
	    r__2 = y;
	    p[i__ + j * 21 - 22] = exp(-(r__1 * r__1) / 64 - r__2 * r__2 / 25)
		    ;
/* L10: */
	}
    }
    s_wsle(&io___6);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___7);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    gropn_(&iws);
    grfrm_();
    grswnd_(&c_b7, &c_b8, &c_b7, &c_b8);
    grsvpt_(&c_b11, &c_b12, &c_b11, &c_b12);
    grstrn_(&c__1);
    grstrf_();
    dp = .017857142857142856f;
    for (k = 1; k <= 56; ++k) {
	tlev1 = (k - 1) * dp;
	tlev2 = tlev1 + dp;
	ipat = (k + 29) * 1000 + 999;
	uestlv_(&tlev1, &tlev2, &ipat);
/* L20: */
    }
    uetone_(p, &c__21, &c__21, &c__21);
    usdaxs_();
/* -- トーンバー ---- */
    grswnd_(&c_b20, &c_b21, &c_b20, &c_b21);
    grsvpt_(&c_b24, &c_b25, &c_b11, &c_b12);
    grstrn_(&c__1);
    grstrf_();
    for (k = 1; k <= 57; ++k) {
	pi[(k << 1) - 2] = (k - 1) * dp + 0.f;
	pi[(k << 1) - 1] = (k - 1) * dp + 0.f;
/* L30: */
    }
    uetone_(pi, &c__2, &c__2, &c__57);
    slpvpr_(&c__3);
    uzlset_("LABELYR", &c_true, (ftnlen)7);
    uzfact_(&c_b12);
    uysfmt_("(F4.1)", (ftnlen)6);
    uyaxdv_("R", &c_b39, &c_b11, (ftnlen)1);
    grcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int color3_ () { MAIN__ (); return 0; }

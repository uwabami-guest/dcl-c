/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static logical c_false = FALSE_;
static real c_b23 = 30.f;
static logical c_true = TRUE_;
static real c_b30 = .5f;
static real c_b32 = 0.f;
static integer c__2 = 2;
static real c_b35 = -.3f;
static real c_b36 = .12f;
static real c_b39 = 180.f;
static real c_b40 = 90.f;
static real c_b42 = .1f;
static real c_b43 = .9f;
static real c_b47 = 360.f;
static real c_b48 = -90.f;
static real c_b50 = -180.f;
static integer c__73 = 73;
static integer c__37 = 37;
static real c_b66 = .95f;
static real c_b68 = .025f;
static integer c__0 = 0;
static real c_b74 = .4f;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* System generated locals */
    integer i__1;
    olist o__1;
    cllist cl__1;

    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void), f_open(olist *), 
	    s_rsfe(cilist *), do_fio(integer *, char *, ftnlen), e_rsfe(void),
	     f_clos(cllist *);

    /* Local variables */
    static integer i__, j;
    static real r__, p1[2701]	/* was [73][37] */, p2[2701]	/* was [73][
	    37] */;
    static integer ir, idx, iws;
    static real amin, amax;
    extern /* Subroutine */ int sgcls_(void), sgfrm_(void), sgopn_(integer *),
	     scsobj_(real *, real *, real *), umpglb_(void);
    extern integer isgtrc_(char *, ftnlen);
    extern /* Subroutine */ int uetone_(real *, integer *, integer *, integer 
	    *), umpmap_(char *, ftnlen), sglset_(char *, logical *, ftnlen), 
	    scspln_(integer *, integer *, real *), scsprj_(void), sgssim_(
	    real *, real *, real *), umiset_(char *, integer *, ftnlen), 
	    sgsmpl_(real *, real *, real *), sgswnd_(real *, real *, real *, 
	    real *), umlset_(char *, logical *, ftnlen), sgstrf_(void), 
	    umrset_(char *, real *, ftnlen), sgstrn_(integer *), sgpwsn_(void)
	    , uestlv_(real *, real *, integer *), sgsvpt_(real *, real *, 
	    real *, real *), slpvpr_(integer *), slpwwr_(integer *), sgstxy_(
	    real *, real *, real *, real *), sgtxzv_(real *, real *, char *, 
	    real *, integer *, integer *, integer *, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___1 = { 0, 6, 0, 0, 0 };
    static cilist io___2 = { 0, 5, 0, 0, 0 };
    static cilist io___5 = { 0, 11, 0, "(10F8.3)", 0 };
    static cilist io___8 = { 0, 12, 0, "(10F8.3)", 0 };


    s_wsle(&io___1);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I) ? ;", (ftnlen)23);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___2);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    o__1.oerr = 0;
    o__1.ounit = 11;
    o__1.ofnmlen = 11;
    o__1.ofnm = "t810630.dat";
    o__1.orl = 0;
    o__1.osta = 0;
    o__1.oacc = 0;
    o__1.ofm = "FORMATTED";
    o__1.oblnk = 0;
    f_open(&o__1);
    o__1.oerr = 0;
    o__1.ounit = 12;
    o__1.ofnmlen = 11;
    o__1.ofnm = "t811231.dat";
    o__1.orl = 0;
    o__1.osta = 0;
    o__1.oacc = 0;
    o__1.ofm = "FORMATTED";
    o__1.oblnk = 0;
    f_open(&o__1);
    for (j = 1; j <= 37; ++j) {
	s_rsfe(&io___5);
	for (i__ = 1; i__ <= 73; ++i__) {
	    do_fio(&c__1, (char *)&p1[i__ + j * 73 - 74], (ftnlen)sizeof(real)
		    );
	}
	e_rsfe();
	s_rsfe(&io___8);
	for (i__ = 1; i__ <= 73; ++i__) {
	    do_fio(&c__1, (char *)&p2[i__ + j * 73 - 74], (ftnlen)sizeof(real)
		    );
	}
	e_rsfe();
/* L10: */
    }
    cl__1.cerr = 0;
    cl__1.cunit = 11;
    cl__1.csta = 0;
    f_clos(&cl__1);
    cl__1.cerr = 0;
    cl__1.cunit = 12;
    cl__1.csta = 0;
    f_clos(&cl__1);
    for (ir = 190; ir <= 245; ir += 5) {
	r__ = (real) ir;
	amin = r__;
	amax = r__ + 5;
/*       IDX=(R-180)*1.4*1000+999 */
	idx = (integer) ((r__ - 170) * 1.25f) * 1000 + 999;
	uestlv_(&amin, &amax, &idx);
/* L20: */
    }
    sgopn_(&iws);
    sglset_("LSOFTF", &c_false, (ftnlen)6);
    umlset_("LGRIDMJ", &c_false, (ftnlen)7);
    umrset_("DGRIDMN", &c_b23, (ftnlen)7);
    umiset_("ITYPEMN", &c__1, (ftnlen)7);
    umiset_("ITYPEOUT", &c__1, (ftnlen)8);
    sgfrm_();
    sglset_("L2TO3", &c_true, (ftnlen)5);
/*     CALL SCSEYE(-1.0, -1.5,  2.0 ) */
    scsobj_(&c_b30, &c_b30, &c_b32);
    scspln_(&c__1, &c__2, &c_b35);
    scsprj_();
    sgssim_(&c_b36, &c_b32, &c_b32);
    sgsmpl_(&c_b39, &c_b40, &c_b32);
    sgsvpt_(&c_b42, &c_b43, &c_b42, &c_b43);
    sgswnd_(&c_b32, &c_b47, &c_b48, &c_b40);
    sgstxy_(&c_b50, &c_b39, &c_b48, &c_b40);
    i__1 = isgtrc_("MER", (ftnlen)3);
    sgstrn_(&i__1);
    sgstrf_();
    sglset_("LCLIP", &c_true, (ftnlen)5);
    slpwwr_(&c__1);
    uetone_(p1, &c__73, &c__73, &c__37);
    umpmap_("coast_world", (ftnlen)11);
    umpglb_();
    slpvpr_(&c__1);
    sglset_("LCLIP", &c_false, (ftnlen)5);
    sgtxzv_(&c_b30, &c_b66, "Temp. : 10 hPa : Jun 30, 1981", &c_b68, &c__0, &
	    c__0, &c__3, (ftnlen)29);
    scspln_(&c__1, &c__2, &c_b74);
/*     CALL SGRSET( 'YOFF3', -0.25 ) */
    scsprj_();
    sglset_("LCLIP", &c_true, (ftnlen)5);
    slpwwr_(&c__1);
    uetone_(p2, &c__73, &c__73, &c__37);
    umpmap_("coast_world", (ftnlen)11);
    umpglb_();
    slpvpr_(&c__1);
    sglset_("LCLIP", &c_false, (ftnlen)5);
    sgtxzv_(&c_b30, &c_b66, "Temp. : 10 hPa : Dec 31, 1981", &c_b68, &c__0, &
	    c__0, &c__3, (ftnlen)29);
    sgcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int scpkt3_ () { MAIN__ (); return 0; }

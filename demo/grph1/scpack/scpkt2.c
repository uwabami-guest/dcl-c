/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static logical c_true = TRUE_;
static integer c__2 = 2;
static real c_b11 = 0.f;
static real c_b12 = 1.f;
static real c_b22 = .5f;
static real c_b25 = .1f;
static integer c__0 = 0;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static integer i__;
    static real x, x0, dx;
    static integer iws;
    extern /* Subroutine */ int sgcls_(void), sgfrm_(void), sgopn_(integer *),
	     sgiset_(char *, integer *, ftnlen), sglset_(char *, logical *, 
	    ftnlen), scsprj_(void), sgswnd_(real *, real *, real *, real *), 
	    sgrset_(char *, real *, ftnlen), sgstrf_(void), sgstrn_(integer *)
	    , sgpwsn_(void), sgsvpt_(real *, real *, real *, real *), slpvpr_(
	    integer *), sgtxzv_(real *, real *, char *, real *, integer *, 
	    integer *, integer *, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___1 = { 0, 6, 0, 0, 0 };
    static cilist io___2 = { 0, 5, 0, 0, 0 };


    s_wsle(&io___1);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___2);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    sglset_("L2TO3", &c_true, (ftnlen)5);
    sgiset_("IFONT", &c__2, (ftnlen)5);
    sgopn_(&iws);
    x0 = -3.f;
    dx = .3f;
    for (i__ = 1; i__ <= 20; ++i__) {
	sgfrm_();
	sgswnd_(&c_b11, &c_b12, &c_b11, &c_b12);
	sgsvpt_(&c_b11, &c_b12, &c_b11, &c_b12);
	sgstrn_(&c__1);
	sgstrf_();
	x = x0 + dx * i__;
	sgrset_("XEYE3", &x, (ftnlen)5);
	scsprj_();
	slpvpr_(&c__1);
	sgtxzv_(&c_b22, &c_b22, "DENNOU", &c_b25, &c__0, &c__0, &c__3, (
		ftnlen)6);
/* L10: */
    }
    sgcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int scpkt2_ () { MAIN__ (); return 0; }

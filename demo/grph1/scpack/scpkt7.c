/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static logical c_true = TRUE_;
static real c_b9 = 1.f;
static real c_b10 = 0.f;
static integer c__2 = 2;
static real c_b14 = 5.f;
static real c_b16 = 2.f;
static integer c__37 = 37;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static real a[37];
    static integer i__, j;
    static real r__[37];
    static integer iws;
    static real alat[37], alon[37];
    extern /* Subroutine */ int sgcls_(void), sgfrm_(void), sgopn_(integer *),
	     scplu_(integer *, real *, real *, real *), scsobj_(real *, real *
	    , real *), scseye_(real *, real *, real *), scsorg_(real *, real *
	    , real *, real *), sglset_(char *, logical *, ftnlen), scstrf_(
	    void), scsprj_(void), scstrn_(integer *), sgpwsn_(void);

    /* Fortran I/O blocks */
    static cilist io___5 = { 0, 6, 0, 0, 0 };
    static cilist io___6 = { 0, 5, 0, 0, 0 };


    for (i__ = 1; i__ <= 37; ++i__) {
	alon[i__ - 1] = (i__ - 1) * 360.f / 36 + 0.f;
	alat[i__ - 1] = (i__ - 1) * 2.f / 36 - 1.f;
	r__[i__ - 1] = 1.f;
/* L10: */
    }
    s_wsle(&io___5);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I) ? ;", (ftnlen)23);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___6);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    sgopn_(&iws);
    sgfrm_();
    sglset_("LDEG", &c_true, (ftnlen)4);
/* ---------------- 3-D ------------------ */
    scsorg_(&c_b9, &c_b10, &c_b10, &c_b10);
    scstrn_(&c__2);
    scstrf_();
    scseye_(&c_b14, &c_b9, &c_b16);
    scsobj_(&c_b10, &c_b10, &c_b10);
    scsprj_();
    for (i__ = 1; i__ <= 37; ++i__) {
	for (j = 1; j <= 37; ++j) {
	    a[j - 1] = alat[i__ - 1];
/* L110: */
	}
	scplu_(&c__37, r__, alon, a);
/* L100: */
    }
    for (i__ = 1; i__ <= 37; ++i__) {
	for (j = 1; j <= 37; ++j) {
	    a[j - 1] = alon[i__ - 1];
/* L210: */
	}
	scplu_(&c__37, r__, a, alat);
/* L200: */
    }
    sgcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int scpkt7_ () { MAIN__ (); return 0; }

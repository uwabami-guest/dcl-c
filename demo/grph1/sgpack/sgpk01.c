/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static real c_b6 = .9f;
static integer c__2 = 2;
static real c_b9 = .8f;
static real c_b12 = .7f;
static integer c__4 = 4;
static real c_b15 = .6f;
static real c_b17 = .4f;
static real c_b20 = .3f;
static real c_b23 = .2f;
static real c_b26 = .1f;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static real x1, x2;
    static integer iws;
    extern /* Subroutine */ int sgcls_(void), sgfrm_(void), sgopn_(integer *),
	     sglnv_(real *, real *, real *, real *), sgslni_(integer *), 
	    sgpwsn_(void), sglnzv_(real *, real *, real *, real *, integer *);

    /* Fortran I/O blocks */
    static cilist io___1 = { 0, 6, 0, 0, 0 };
    static cilist io___2 = { 0, 5, 0, 0, 0 };


    s_wsle(&io___1);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
/* <-- */
    s_rsle(&io___2);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    sgopn_(&iws);
/* <-- */
    sgfrm_();
/* <-- ページを用意する. */
    x1 = .1f;
    x2 = .9f;
    sglnv_(&x1, &c_b6, &x2, &c_b6);
/* <-- デフォルトのLINE INDEX */
    sgslni_(&c__2);
/* <-- LINE INDEX を2 */
    sglnv_(&x1, &c_b9, &x2, &c_b9);
    sgslni_(&c__3);
    sglnv_(&x1, &c_b12, &x2, &c_b12);
    sgslni_(&c__4);
    sglnv_(&x1, &c_b15, &x2, &c_b15);
    sglnzv_(&x1, &c_b17, &x2, &c_b17, &c__1);
/*  <-- LINE INDEX */
    sglnzv_(&x1, &c_b20, &x2, &c_b20, &c__2);
    sglnzv_(&x1, &c_b23, &x2, &c_b23, &c__3);
    sglnzv_(&x1, &c_b26, &x2, &c_b26, &c__4);
    sgcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int sgpk01_ () { MAIN__ (); return 0; }

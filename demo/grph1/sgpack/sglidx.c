/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static real c_b6 = .05f;
static real c_b8 = 0.f;
static real c_b9 = .1f;
static real c_b14 = .018f;
static real c_b20 = .015f;
static integer c__0 = 0;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* System generated locals */
    real r__1, r__2;

    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void), s_wsfi(icilist *), 
	    do_fio(integer *, char *, ftnlen), e_wsfi(void);

    /* Local variables */
    static integer i__, j;
    static real x1, x2, y1, y2;
    static integer iw;
    static char chr[2];
    static integer idx;
    extern /* Subroutine */ int sgcls_(void), sgfrm_(void), slmgn_(real *, 
	    real *, real *, real *), sgopn_(integer *), sgpwsn_(void), 
	    sglnzv_(real *, real *, real *, real *, integer *), slsttl_(char *
	    , char *, real *, real *, real *, integer *, ftnlen, ftnlen), 
	    sgtxzv_(real *, real *, char *, real *, integer *, integer *, 
	    integer *, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___1 = { 0, 6, 0, 0, 0 };
    static cilist io___2 = { 0, 5, 0, 0, 0 };
    static icilist io___12 = { 0, chr, 0, "(I2)", 2, 1 };


    s_wsle(&io___1);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I) ? ; ", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___2);
    do_lio(&c__3, &c__1, (char *)&iw, (ftnlen)sizeof(integer));
    e_rsle();
    sgopn_(&iw);
    slmgn_(&c_b6, &c_b6, &c_b8, &c_b9);
    slsttl_("DEMONSTRATION FOR LINE INDEX", "T", &c_b8, &c_b8, &c_b14, &c__1, 
	    (ftnlen)28, (ftnlen)1);
    sgfrm_();
    for (j = 0; j <= 9; ++j) {
	for (i__ = 0; i__ <= 9; ++i__) {
	    idx = j * 10 + i__;
	    if (idx == 0) {
		goto L10;
	    }
	    x1 = i__ * .1f + .01f;
	    x2 = x1 + .08f;
	    y1 = 1.f - j * .1f - .01f;
	    y2 = y1 - .08f;
	    s_wsfi(&io___12);
	    do_fio(&c__1, (char *)&idx, (ftnlen)sizeof(integer));
	    e_wsfi();
	    sglnzv_(&x1, &y1, &x2, &y2, &idx);
	    r__1 = x1 + .02f;
	    r__2 = y2 + .02f;
	    sgtxzv_(&r__1, &r__2, chr, &c_b20, &c__0, &c__1, &c__1, (ftnlen)2)
		    ;
L10:
	    ;
	}
/* L20: */
    }
    sgcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int sglidx_ () { MAIN__ (); return 0; }

/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static real c_b11 = .03f;
static integer c__0 = 0;
static logical c_true = TRUE_;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static real ux1, ux2, uy1, uy2, uxc, uyc;
    static integer iws;
    extern /* Subroutine */ int sgcls_(void), sgfrm_(void), sgopn_(integer *),
	     sglset_(char *, logical *, ftnlen), sgqwnd_(real *, real *, real 
	    *, real *), sgpwsn_(void), sglnzu_(real *, real *, real *, real *,
	     integer *), sgtxzu_(real *, real *, char *, real *, integer *, 
	    integer *, integer *, ftnlen), sgtxzv_(real *, real *, char *, 
	    real *, integer *, integer *, integer *, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___1 = { 0, 6, 0, 0, 0 };
    static cilist io___2 = { 0, 5, 0, 0, 0 };


    s_wsle(&io___1);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___2);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    sgopn_(&iws);
/*     / FIRST PAGE / */
    sgfrm_();
    sgqwnd_(&ux1, &ux2, &uy1, &uy2);
    uxc = (ux1 + ux2) / 2;
    uyc = (uy1 + uy2) / 2;
    sglnzu_(&ux1, &uy1, &ux2, &uy1, &c__1);
    sglnzu_(&ux2, &uy1, &ux2, &uy2, &c__1);
    sglnzu_(&ux2, &uy2, &ux1, &uy2, &c__1);
    sglnzu_(&ux1, &uy2, &ux1, &uy1, &c__1);
    sgtxzu_(&uxc, &uyc, "SGKS TEST : PAGE 1", &c_b11, &c__0, &c__0, &c__3, (
	    ftnlen)18);
/*     / SECOND PAGE / */
    sglset_("LFULL", &c_true, (ftnlen)5);
    sgfrm_();
    sgqwnd_(&ux1, &ux2, &uy1, &uy2);
    uxc = (ux1 + ux2) / 2;
    uyc = (uy1 + uy2) / 2;
    sglnzu_(&ux1, &uy1, &ux2, &uy1, &c__1);
    sglnzu_(&ux2, &uy1, &ux2, &uy2, &c__1);
    sglnzu_(&ux2, &uy2, &ux1, &uy2, &c__1);
    sglnzu_(&ux1, &uy2, &ux1, &uy1, &c__1);
    sgtxzv_(&uxc, &uyc, "SGKS TEST : PAGE 2", &c_b11, &c__0, &c__0, &c__3, (
	    ftnlen)18);
    sgcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int sgksx1_ () { MAIN__ (); return 0; }

/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static integer c__2 = 2;
static real c_b9 = 0.f;
static real c_b10 = 1.f;
static integer c__37 = 37;
static real c_b25 = .1f;
static real c_b26 = .4f;
static real c_b27 = .6f;
static real c_b28 = .9f;
static real c_b35 = .3f;
static real c_b36 = .7f;
static real c_b47 = -.5f;
static real c_b48 = .5f;
static logical c_true = TRUE_;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Builtin functions */
    double sin(doublereal), cos(doublereal);
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static integer i__;
    static real r__, x[37], y[37], dt, xc, yc;
    static integer iws;
    extern /* Subroutine */ int sgcls_(void), sgfrm_(void), sgopn_(integer *),
	     sgplu_(integer *, real *, real *), sgtxu_(real *, real *, char *,
	     ftnlen), sgspli_(integer *), sglset_(char *, logical *, ftnlen), 
	    sgswnd_(real *, real *, real *, real *), sgstrf_(void), sgstrn_(
	    integer *), sgpwsn_(void), sgstxi_(integer *), sgsvpt_(real *, 
	    real *, real *, real *), slpvpr_(integer *);

    /* Fortran I/O blocks */
    static cilist io___8 = { 0, 6, 0, 0, 0 };
    static cilist io___9 = { 0, 5, 0, 0, 0 };


    dt = .17453277777777776f;
    r__ = .2f;
    for (i__ = 1; i__ <= 37; ++i__) {
	x[i__ - 1] = r__ * sin(dt * (i__ - 1)) + .5f;
	y[i__ - 1] = r__ * cos(dt * (i__ - 1)) + .5f;
/* L10: */
    }
    xc = .5f;
    yc = .5f;
    s_wsle(&io___8);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___9);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    sgopn_(&iws);
/* ----------------------------- page 1 --------------------------------- */
    sgfrm_();
    sgstxi_(&c__3);
    sgspli_(&c__2);
/*                XMIN, XMAX, YMIN, YMAX */
    sgswnd_(&c_b9, &c_b10, &c_b9, &c_b10);
    sgsvpt_(&c_b9, &c_b10, &c_b9, &c_b10);
    sgstrn_(&c__1);
    sgstrf_();
    slpvpr_(&c__1);
    sgplu_(&c__37, x, y);
/* <-- */
    sgtxu_(&xc, &yc, "SGTXU", (ftnlen)5);
/* ----------------------------- page 2 --------------------------------- */
/* <-- */
    sgfrm_();
/*                 XMIN, XMAX, YMIN, YMAX */
    sgswnd_(&c_b9, &c_b10, &c_b9, &c_b10);
/* <-- 1x1 */
    sgsvpt_(&c_b25, &c_b26, &c_b27, &c_b28);
/* <-- 小さな view port */
    sgstrn_(&c__1);
    sgstrf_();
    slpvpr_(&c__1);
    sgplu_(&c__37, x, y);
/* <-- */
    sgtxu_(&xc, &yc, "SGTXU", (ftnlen)5);
/*                XMIN, XMAX, YMIN, YMAX */
/* <-- */
    sgswnd_(&c_b9, &c_b10, &c_b35, &c_b36);
/* <-- ゆがんだ window */
    sgsvpt_(&c_b27, &c_b28, &c_b27, &c_b28);
    sgstrn_(&c__1);
    sgstrf_();
    slpvpr_(&c__1);
    sgplu_(&c__37, x, y);
/* <-- */
    sgtxu_(&xc, &yc, "SGTXU", (ftnlen)5);
/*                XMIN, XMAX, YMIN, YMAX */
/* <-- */
    sgswnd_(&c_b9, &c_b10, &c_b47, &c_b48);
/* <-- はみ出し window */
    sgsvpt_(&c_b25, &c_b26, &c_b25, &c_b26);
    sgstrn_(&c__1);
    sgstrf_();
    slpvpr_(&c__1);
    sgplu_(&c__37, x, y);
/* <-- */
    sgtxu_(&xc, &yc, "SGTXU", (ftnlen)5);
/* <-- */
    sglset_("LCLIP", &c_true, (ftnlen)5);
/*                XMIN, XMAX, YMIN, YMAX */
/* <-- */
    sgswnd_(&c_b9, &c_b10, &c_b47, &c_b48);
    sgsvpt_(&c_b27, &c_b28, &c_b25, &c_b26);
    sgstrn_(&c__1);
    sgstrf_();
    slpvpr_(&c__1);
    sgplu_(&c__37, x, y);
/* <-- */
    sgtxu_(&xc, &yc, "SGTXU", (ftnlen)5);
/* <-- */
    sgcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int sgpk06_ () { MAIN__ (); return 0; }

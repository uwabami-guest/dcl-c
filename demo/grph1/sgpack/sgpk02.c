/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static real c_b7 = 0.f;
static real c_b8 = 1.f;
static real c_b17 = .2f;
static real c_b18 = .8f;
static integer c__2 = 2;
static real c_b30 = .1f;
static real c_b31 = .4f;
static real c_b32 = .6f;
static real c_b33 = .9f;
static logical c_true = TRUE_;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static integer iws;
    extern /* Subroutine */ int sgcls_(void), sgfrm_(void), sgopn_(integer *),
	     sglnu_(real *, real *, real *, real *), sgslni_(integer *), 
	    sglset_(char *, logical *, ftnlen), sgswnd_(real *, real *, real *
	    , real *), sgstrf_(void), sgstrn_(integer *), sgpwsn_(void), 
	    sglnzu_(real *, real *, real *, real *, integer *), sgsvpt_(real *
	    , real *, real *, real *), slpvpr_(integer *);

    /* Fortran I/O blocks */
    static cilist io___1 = { 0, 6, 0, 0, 0 };
    static cilist io___2 = { 0, 5, 0, 0, 0 };


    s_wsle(&io___1);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___2);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    sgopn_(&iws);
/* ----------------------------- page 1 --------------------------------- */
    sgfrm_();
    sgslni_(&c__3);
/*                  XMIN, XMAX, YMIN, YMAX */
    sgswnd_(&c_b7, &c_b8, &c_b7, &c_b8);
/*  <--+ */
    sgsvpt_(&c_b7, &c_b8, &c_b7, &c_b8);
/*     | */
    sgstrn_(&c__1);
/*     | */
    sgstrf_();
/*  <--+ */
    slpvpr_(&c__1);
/*  <--- */
    sglnzu_(&c_b17, &c_b18, &c_b18, &c_b17, &c__2);
/*  <--- X */
    sglnu_(&c_b17, &c_b17, &c_b18, &c_b18);
/* ----------------------------- page 2 --------------------------------- */
/*  <-| */
    sgfrm_();
/*                  XMIN, XMAX, YMIN, YMAX */
    sgswnd_(&c_b7, &c_b8, &c_b7, &c_b8);
/*  <-- */
    sgsvpt_(&c_b30, &c_b31, &c_b32, &c_b33);
/*  <-- */
    sgstrf_();
    slpvpr_(&c__1);
    sglnzu_(&c_b17, &c_b18, &c_b18, &c_b17, &c__2);
    sglnu_(&c_b17, &c_b17, &c_b18, &c_b18);
/*   ------------------------------------------ */
/*                  XMIN, XMAX, YMIN, YMAX */
    sgswnd_(&c_b7, &c_b8, &c_b8, &c_b7);
/*  <-- */
    sgsvpt_(&c_b32, &c_b33, &c_b32, &c_b33);
/*  <-- */
    sgstrf_();
    slpvpr_(&c__1);
    sglnzu_(&c_b17, &c_b18, &c_b18, &c_b17, &c__2);
    sglnu_(&c_b17, &c_b17, &c_b18, &c_b18);
/*   ------------------------------------------ */
/*                  XMIN, XMAX, YMIN, YMAX */
    sgswnd_(&c_b7, &c_b8, &c_b7, &c_b32);
/*  <-- */
    sgsvpt_(&c_b30, &c_b31, &c_b30, &c_b31);
/*  <-- */
    sgstrf_();
    slpvpr_(&c__1);
    sglnzu_(&c_b17, &c_b18, &c_b18, &c_b17, &c__2);
    sglnu_(&c_b17, &c_b17, &c_b18, &c_b18);
/*   ------------------------------------------ */
/*                  XMIN, XMAX, YMIN, YMAX */
    sgswnd_(&c_b7, &c_b8, &c_b7, &c_b32);
/*  <-- */
    sgsvpt_(&c_b32, &c_b33, &c_b30, &c_b31);
/*  <-- */
    sgstrf_();
    slpvpr_(&c__1);
    sglset_("LCLIP", &c_true, (ftnlen)5);
/*  <-- */
    sglnzu_(&c_b17, &c_b18, &c_b18, &c_b17, &c__2);
    sglnu_(&c_b17, &c_b17, &c_b18, &c_b18);
    sgcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int sgpk02_ () { MAIN__ (); return 0; }

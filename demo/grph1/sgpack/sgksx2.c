/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static real c_b8 = 0.f;
static real c_b9 = 10.f;
static real c_b13 = 1.f;
static real c_b17 = .1f;
static real c_b19 = 9.9f;
static integer c__5 = 5;
static integer c__2 = 2;
static real c_b43 = .04f;
static real c_b45 = 5.f;
static real c_b46 = 8.5f;
static integer c__0 = 0;
static real c_b52 = 2.5f;
static real c_b53 = 7.5f;
static real c_b56 = .25f;
static real c_b57 = .75f;
static integer c__4 = 4;
static real c_b67 = .02f;
static real c_b68 = .5f;
static logical c_true = TRUE_;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Initialized data */

    static real ux[5] = { 2.f,4.f,5.f,7.f,8.f };
    static real uy[5] = { 4.f,5.f,4.f,5.f,4.f };
    static real vx[5] = { .2f,.4f,.5f,.7f,.8f };
    static real vy[5] = { .5f,.6f,.5f,.6f,.5f };

    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static real vx1, vy1, vx2, vy2;
    static integer iws;
    extern /* Subroutine */ int sgcls_(void), sgfrm_(void), sgopn_(integer *),
	     sgiset_(char *, integer *, ftnlen), sglset_(char *, logical *, 
	    ftnlen), sgswnd_(real *, real *, real *, real *), sgstrf_(void), 
	    stftrf_(real *, real *, real *, real *), sgstrn_(integer *), 
	    sgpwsn_(void), sglnzu_(real *, real *, real *, real *, integer *),
	     sglnzv_(real *, real *, real *, real *, integer *), sgplzu_(
	    integer *, real *, real *, integer *, integer *), sgpmzu_(integer 
	    *, real *, real *, integer *, integer *, real *), sgsvpt_(real *, 
	    real *, real *, real *), sgplzv_(integer *, real *, real *, 
	    integer *, integer *), sgpmzv_(integer *, real *, real *, integer 
	    *, integer *, real *), slpvpr_(integer *), sgtxzu_(real *, real *,
	     char *, real *, integer *, integer *, integer *, ftnlen), 
	    sgtxzv_(real *, real *, char *, real *, integer *, integer *, 
	    integer *, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___5 = { 0, 6, 0, 0, 0 };
    static cilist io___6 = { 0, 5, 0, 0, 0 };


    s_wsle(&io___5);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___6);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    sgopn_(&iws);
/*     TEST POLYLINE, POLYMARKER, TEXT PRIMITIVE IN WC. */
/*     TEST POLYLINE, POLYMARKER, TEXT PRIMITIVE IN NDC. */
/*     TRANSFORMATION FUNCTION : LINEAR*LINEAR */
/*     NO-CLIPPING, FONT NO.=1 */
    sgfrm_();
    sgiset_("IFONT", &c__1, (ftnlen)5);
    sgswnd_(&c_b8, &c_b9, &c_b8, &c_b9);
    sgsvpt_(&c_b8, &c_b13, &c_b8, &c_b13);
    sgstrn_(&c__1);
    sgstrf_();
    sglnzu_(&c_b17, &c_b17, &c_b19, &c_b17, &c__1);
    sglnzu_(&c_b19, &c_b17, &c_b19, &c_b19, &c__1);
    sglnzu_(&c_b19, &c_b19, &c_b17, &c_b19, &c__1);
    sglnzu_(&c_b17, &c_b19, &c_b17, &c_b17, &c__1);
    sgplzu_(&c__5, ux, uy, &c__2, &c__3);
    sgpmzu_(&c__5, ux, uy, &c__5, &c__3, &c_b43);
    stftrf_(ux, uy, &vx1, &vy1);
    stftrf_(&ux[3], &uy[3], &vx2, &vy2);
    sglnzv_(&vx1, &vy1, &vx2, &vy2, &c__2);
    sgtxzu_(&c_b45, &c_b46, "TESTU", &c_b17, &c__0, &c__0, &c__3, (ftnlen)5);
    sgswnd_(&c_b52, &c_b53, &c_b52, &c_b53);
    sgsvpt_(&c_b56, &c_b57, &c_b56, &c_b57);
    sgstrn_(&c__1);
    sgstrf_();
    sgplzv_(&c__5, vx, vy, &c__3, &c__1);
    sgpmzv_(&c__5, vx, vy, &c__4, &c__1, &c_b67);
    sgtxzv_(&c_b68, &c_b57, "TESTV", &c_b17, &c__0, &c__0, &c__3, (ftnlen)5);
/*     TEST POLYLINE, POLYMARKER, TEXT PRIMITIVE IN WC. */
/*     TEST POLYLINE, POLYMARKER, TEXT PRIMITIVE IN NDC. */
/*     TRANSFORMATION FUNCTION : LINEAR*LINEAR */
/*     CLIPPING, FONT NO.=2 */
    sgfrm_();
    sgiset_("IFONT", &c__2, (ftnlen)5);
    sgswnd_(&c_b8, &c_b9, &c_b8, &c_b9);
    sgsvpt_(&c_b8, &c_b13, &c_b8, &c_b13);
    sgstrn_(&c__1);
    sgstrf_();
    sglnzu_(&c_b17, &c_b17, &c_b19, &c_b17, &c__1);
    sglnzu_(&c_b19, &c_b17, &c_b19, &c_b19, &c__1);
    sglnzu_(&c_b19, &c_b19, &c_b17, &c_b19, &c__1);
    sglnzu_(&c_b17, &c_b19, &c_b17, &c_b17, &c__1);
    sgplzu_(&c__5, ux, uy, &c__2, &c__3);
    sgpmzu_(&c__5, ux, uy, &c__5, &c__3, &c_b43);
    stftrf_(ux, uy, &vx1, &vy1);
    stftrf_(&ux[3], &uy[3], &vx2, &vy2);
    sglnzv_(&vx1, &vy1, &vx2, &vy2, &c__2);
    sgtxzu_(&c_b45, &c_b46, "TESTU", &c_b17, &c__0, &c__0, &c__3, (ftnlen)5);
    sgswnd_(&c_b52, &c_b53, &c_b52, &c_b53);
    sgsvpt_(&c_b56, &c_b57, &c_b56, &c_b57);
    sgstrn_(&c__1);
    sgstrf_();
    sglset_("LCLIP", &c_true, (ftnlen)5);
    slpvpr_(&c__1);
    sgplzv_(&c__5, vx, vy, &c__3, &c__1);
    sgpmzv_(&c__5, vx, vy, &c__4, &c__1, &c_b67);
    sgtxzv_(&c_b68, &c_b57, "TESTV", &c_b17, &c__0, &c__0, &c__3, (ftnlen)5);
    sgcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int sgksx2_ () { MAIN__ (); return 0; }

/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static real c_b6 = -50.f;
static real c_b7 = 50.f;
static real c_b8 = 1e3f;
static real c_b9 = .4f;
static real c_b10 = .2f;
static real c_b11 = .8f;
static integer c__2 = 2;
static integer c__21 = 21;
static integer c__4 = 4;
static integer c__5 = 5;
static real c_b25 = 0.f;
static integer c__0 = 0;
static integer c__18 = 18;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Initialized data */

    static real rx1[21] = { -50.f,-45.f,-40.f,-35.f,-30.f,-25.f,-20.f,-15.f,
	    -10.f,-5.f,0.f,5.f,10.f,15.f,20.f,25.f,30.f,35.f,40.f,45.f,50.f };
    static real rx2[5] = { -40.f,-20.f,0.f,20.f,40.f };
    static char cx2[4*5] = "40S " "20S " "EQ  " "20N " "40N ";
    static real ry2[18] = { 1e3f,850.f,700.f,500.f,400.f,300.f,250.f,200.f,
	    150.f,100.f,70.f,50.f,30.f,10.f,5.f,2.f,1.f,.4f };
    static char cy2[4*18] = "1000" "    " "    " "500 " "    " "    " "    " 
	    "200 " "    " "100 " "    " "50  " "30  " "10  " "5   " "2   " 
	    "1   " ".4  ";

    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static integer iws;
    extern /* Subroutine */ int grcls_(void), grfrm_(void), gropn_(integer *);
    static real dummy;
    extern /* Subroutine */ int uxaxlb_(char *, real *, integer *, real *, 
	    char *, integer *, integer *, ftnlen, ftnlen), uyaxlb_(char *, 
	    real *, integer *, real *, char *, integer *, integer *, ftnlen, 
	    ftnlen), sgswnd_(real *, real *, real *, real *), sgstrf_(void), 
	    sgstrn_(integer *), sgpwsn_(void), sgsvpt_(real *, real *, real *,
	     real *), uxmttl_(char *, char *, real *, ftnlen, ftnlen), 
	    uxsttl_(char *, char *, real *, ftnlen, ftnlen), uysttl_(char *, 
	    char *, real *, ftnlen, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___6 = { 0, 6, 0, 0, 0 };
    static cilist io___7 = { 0, 5, 0, 0, 0 };


    s_wsle(&io___6);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I) ? ;", (ftnlen)23);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___7);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    gropn_(&iws);
    grfrm_();
    sgswnd_(&c_b6, &c_b7, &c_b8, &c_b9);
    sgsvpt_(&c_b10, &c_b11, &c_b10, &c_b11);
    sgstrn_(&c__2);
    sgstrf_();
    uxaxlb_("B", rx1, &c__21, rx2, cx2, &c__4, &c__5, (ftnlen)1, (ftnlen)4);
    uxaxlb_("T", rx1, &c__21, rx2, cx2, &c__4, &c__5, (ftnlen)1, (ftnlen)4);
    uxsttl_("B", "LATITUDE", &c_b25, (ftnlen)1, (ftnlen)8);
    uyaxlb_("L", &dummy, &c__0, ry2, cy2, &c__4, &c__18, (ftnlen)1, (ftnlen)4)
	    ;
    uyaxlb_("R", &dummy, &c__0, ry2, cy2, &c__4, &c__18, (ftnlen)1, (ftnlen)4)
	    ;
    uysttl_("L", "PRESSURE (MB)", &c_b25, (ftnlen)1, (ftnlen)13);
    uxmttl_("T", "UXAXLB/UYAXLB", &c_b25, (ftnlen)1, (ftnlen)13);
    grcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int uxyz04_ () { MAIN__ (); return 0; }

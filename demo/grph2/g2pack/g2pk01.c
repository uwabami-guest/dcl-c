/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static real c_b10 = .15f;
static real c_b11 = .85f;
static integer c__15 = 15;
static integer c__51 = 51;
static logical c_true = TRUE_;
static real c_b37 = 0.f;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static integer i__, j;
    static real z__[225]	/* was [15][15] */, cx[225]	/* was [15][
	    15] */, cy[225]	/* was [15][15] */, ux[15], uy[15];
    static integer iws;
    static real uxw[15], uyw[15];
    extern /* Subroutine */ int grcls_(void), grfrm_(void);
    static real cxmin, cymin, cxmax, cymax;
    extern /* Subroutine */ int gropn_(integer *), sgplu_(integer *, real *, 
	    real *), g2sctr_(integer *, integer *, real *, real *, real *, 
	    real *), sgscwd_(real *, real *, real *, real *), uetone_(real *, 
	    integer *, integer *, integer *), sglset_(char *, logical *, 
	    ftnlen), uelset_(char *, logical *, ftnlen), grswnd_(real *, real 
	    *, real *, real *), grstrf_(void), usdaxs_(void), uwsgxa_(real *, 
	    integer *), grstrn_(integer *), uwsgya_(real *, integer *), 
	    sgpwsn_(void), grsvpt_(real *, real *, real *, real *), uxsttl_(
	    char *, char *, real *, ftnlen, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___11 = { 0, 6, 0, 0, 0 };
    static cilist io___12 = { 0, 5, 0, 0, 0 };


/*     / SET PARAMETERS / */
    for (i__ = 1; i__ <= 15; ++i__) {
	ux[i__ - 1] = (i__ - 1.f) / 14.f;
/* L10: */
    }
    for (j = 1; j <= 15; ++j) {
	uy[j - 1] = (j - 1.f) / 14.f;
/* L15: */
    }
    for (j = 1; j <= 15; ++j) {
	for (i__ = 1; i__ <= 15; ++i__) {
	    cx[i__ + j * 15 - 16] = ux[i__ - 1] + uy[j - 1] * .1f;
	    cy[i__ + j * 15 - 16] = ux[i__ - 1] * .2f + uy[j - 1];
/* L20: */
	}
/* L25: */
    }
    cxmin = 0.f;
    cxmax = 1.1f;
    cymin = 0.f;
    cymax = 1.1f;
/*     / GRAPHIC / */
    s_wsle(&io___11);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I) ? ;", (ftnlen)23);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___12);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    gropn_(&iws);
    grfrm_();
    grsvpt_(&c_b10, &c_b11, &c_b10, &c_b11);
    grswnd_(ux, &ux[14], uy, &uy[14]);
    g2sctr_(&c__15, &c__15, ux, uy, cx, cy);
    grstrn_(&c__51);
    sgscwd_(&cxmin, &cxmax, &cymin, &cymax);
    grstrf_();
    sglset_("LCLIP", &c_true, (ftnlen)5);
/*     / TONE / */
    for (j = 1; j <= 15; ++j) {
	for (i__ = 1; i__ <= 15; ++i__) {
	    z__[i__ + j * 15 - 16] = ux[i__ - 1] + uy[j - 1];
/* L30: */
	}
/* L35: */
    }
    uelset_("LTONE", &c_true, (ftnlen)5);
    uwsgxa_(ux, &c__15);
    uwsgya_(uy, &c__15);
    uetone_(z__, &c__15, &c__15, &c__15);
/*     / GRID LINES / */
    for (j = 1; j <= 15; ++j) {
	for (i__ = 1; i__ <= 15; ++i__) {
	    uyw[i__ - 1] = uy[j - 1];
/* L40: */
	}
	sgplu_(&c__15, ux, uyw);
/* L45: */
    }
    for (i__ = 1; i__ <= 15; ++i__) {
	for (j = 1; j <= 15; ++j) {
	    uxw[j - 1] = ux[i__ - 1];
/* L50: */
	}
	sgplu_(&c__15, uxw, uy);
/* L55: */
    }
/*     / AXES (Switch to ITR==1) / */
    grswnd_(&cxmin, &cxmax, &cymin, &cymax);
    grstrn_(&c__1);
    grstrf_();
    usdaxs_();
    uxsttl_("T", "BOTH X & Y TRANSFORMED", &c_b37, (ftnlen)1, (ftnlen)22);
    grcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int g2pk01_ () { MAIN__ (); return 0; }

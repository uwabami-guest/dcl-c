/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static real c_b11 = .15f;
static real c_b12 = .85f;
static integer c__51 = 51;
static integer c__15 = 15;
static logical c_true = TRUE_;
static real c_b36 = 0.f;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* System generated locals */
    real r__1;

    /* Builtin functions */
    double exp(doublereal);
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static integer i__, j;
    static real z__[225]	/* was [15][15] */, cx[225]	/* was [15][
	    15] */, cy[225]	/* was [15][15] */, ux[15], uy[15];
    static integer iws;
    static real uxw[15], uyw[15];
    extern /* Subroutine */ int grcls_(void), grfrm_(void);
    static real cxmin, cymin, cxmax, cymax;
    extern /* Subroutine */ int gropn_(integer *), sgplu_(integer *, real *, 
	    real *), g2qctm_(real *, real *, real *, real *), g2sctr_(integer 
	    *, integer *, real *, real *, real *, real *);
    static real rundef;
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen), uetone_(real 
	    *, integer *, integer *, integer *), uelset_(char *, logical *, 
	    ftnlen), grswnd_(real *, real *, real *, real *), grstrf_(void), 
	    usdaxs_(void), uwsgxa_(real *, integer *), grstrn_(integer *), 
	    uwsgya_(real *, integer *), sgpwsn_(void), grsvpt_(real *, real *,
	     real *, real *), uxsttl_(char *, char *, real *, ftnlen, ftnlen);
    static real terrain[15];

    /* Fortran I/O blocks */
    static cilist io___9 = { 0, 6, 0, 0, 0 };
    static cilist io___10 = { 0, 5, 0, 0, 0 };


/*     / SET PARAMETERS / */
    glrget_("RUNDEF", &rundef, (ftnlen)6);
    for (i__ = 1; i__ <= 15; ++i__) {
	ux[i__ - 1] = (i__ - 1.f) / 14.f - .5f;
/* Computing 2nd power */
	r__1 = ux[i__ - 1];
	terrain[i__ - 1] = exp(r__1 * r__1 * -24) * .1f;
/* L10: */
    }
    for (j = 1; j <= 15; ++j) {
	uy[j - 1] = (j - 1.f) / 14.f;
/* L15: */
    }
    cx[0] = rundef;
    for (j = 1; j <= 15; ++j) {
	for (i__ = 1; i__ <= 15; ++i__) {
	    cy[i__ + j * 15 - 16] = uy[j - 1] * (1.f - terrain[i__ - 1]) + 
		    terrain[i__ - 1];
/* L20: */
	}
/* L25: */
    }
/*     / GRAPHIC / */
    s_wsle(&io___9);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I) ? ;", (ftnlen)23);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___10);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    gropn_(&iws);
    grfrm_();
    grsvpt_(&c_b11, &c_b12, &c_b11, &c_b12);
    grswnd_(ux, &ux[14], uy, &uy[14]);
    grstrn_(&c__51);
    g2sctr_(&c__15, &c__15, ux, uy, cx, cy);
    grstrf_();
/*     / TONE / */
    for (j = 1; j <= 15; ++j) {
	for (i__ = 1; i__ <= 15; ++i__) {
	    z__[i__ + j * 15 - 16] = ux[i__ - 1] * (1 - uy[j - 1]);
/* L30: */
	}
/* L35: */
    }
    uelset_("LTONE", &c_true, (ftnlen)5);
    uwsgxa_(ux, &c__15);
    uwsgya_(uy, &c__15);
    uetone_(z__, &c__15, &c__15, &c__15);
/*     / GRID LINES / */
    for (j = 1; j <= 15; ++j) {
	for (i__ = 1; i__ <= 15; ++i__) {
	    uyw[i__ - 1] = uy[j - 1];
/* L40: */
	}
	sgplu_(&c__15, ux, uyw);
/* L45: */
    }
    for (i__ = 1; i__ <= 15; ++i__) {
	for (j = 1; j <= 15; ++j) {
	    uxw[j - 1] = ux[i__ - 1];
/* L50: */
	}
	sgplu_(&c__15, uxw, uy);
/* L55: */
    }
/*     / AXES  (Switch to ITR==1) / */
    g2qctm_(&cxmin, &cxmax, &cymin, &cymax);
    grswnd_(&cxmin, &cxmax, &cymin, &cymax);
    grstrn_(&c__1);
    grstrf_();
    usdaxs_();
    uxsttl_("T", "TERRAIN FOLLOWING", &c_b36, (ftnlen)1, (ftnlen)17);
    grcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int g2pk02_ () { MAIN__ (); return 0; }

/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static real c_b7 = 0.f;
static real c_b8 = 5.f;
static integer c__5 = 5;
static real c_b14 = .3f;
static real c_b15 = 4.3f;
static integer c__2999 = 2999;
static real c_b20 = .5f;
static real c_b21 = 4.5f;
static integer c__3999 = 3999;
static real c_b26 = .7f;
static real c_b27 = 4.7f;
static integer c__4999 = 4999;
static real c_b42 = .05f;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Initialized data */

    static real xnec[5] = { 1.2f,2.5f,3.3f,3.7f,3.1f };
    static real xapl[5] = { 1.3f,2.6f,3.8f,4.4f,4.5f };
    static real xibm[5] = { 1.7f,2.2f,3.5f,4.2f,5.7f };

    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static integer i__, iws;
    extern /* Subroutine */ int grcls_(void), grfrm_(void), uvbra_(integer *, 
	    real *, real *, real *), uvbrf_(integer *, real *, real *, real *)
	    , gropn_(integer *), uvbrl_(integer *, real *, real *);
    static real rundef;
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen), grswnd_(real 
	    *, real *, real *, real *), grstrf_(void), usdaxs_(void), uspfit_(
	    void), uusidv_(real *, real *), uusarp_(integer *, integer *), 
	    sgpwsn_(void), uusbrs_(real *), usspnt_(integer *, real *, real *)
	    ;

    /* Fortran I/O blocks */
    static cilist io___5 = { 0, 6, 0, 0, 0 };
    static cilist io___6 = { 0, 5, 0, 0, 0 };


    glrget_("RUNDEF", &rundef, (ftnlen)6);
    s_wsle(&io___5);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___6);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    gropn_(&iws);
    grfrm_();
    grswnd_(&c_b7, &c_b8, &rundef, &rundef);
    usspnt_(&c__5, &rundef, xibm);
    usspnt_(&c__5, &rundef, xnec);
    usspnt_(&c__5, &rundef, xapl);
    usspnt_(&c__1, &rundef, &c_b7);
    uspfit_();
    grstrf_();
    usdaxs_();
    uusidv_(&c_b14, &c_b15);
    uusarp_(&c__2999, &c__2999);
    uvbra_(&c__5, &rundef, xnec, &rundef);
    uvbrf_(&c__5, &rundef, xnec, &rundef);
    uusidv_(&c_b20, &c_b21);
    uusarp_(&c__3999, &c__3999);
    uvbra_(&c__5, &rundef, xapl, &rundef);
    uvbrf_(&c__5, &rundef, xapl, &rundef);
    uusidv_(&c_b26, &c_b27);
    uusarp_(&c__4999, &c__4999);
    uvbra_(&c__5, &rundef, xibm, &rundef);
    uvbrf_(&c__5, &rundef, xibm, &rundef);
    grfrm_();
    for (i__ = 1; i__ <= 5; ++i__) {
	xapl[i__ - 1] = xnec[i__ - 1] + xapl[i__ - 1];
	xibm[i__ - 1] = xapl[i__ - 1] + xibm[i__ - 1];
/* L10: */
    }
    grswnd_(&c_b7, &c_b8, &rundef, &rundef);
    usspnt_(&c__5, &rundef, xibm);
    usspnt_(&c__1, &rundef, &c_b7);
    uspfit_();
    grstrf_();
    usdaxs_();
    uusidv_(&c_b20, &c_b21);
    uusarp_(&c__2999, &c__2999);
    uusbrs_(&c_b42);
    uvbra_(&c__5, &rundef, xnec, &rundef);
    uusarp_(&c__3999, &c__3999);
    uvbra_(&c__5, &rundef, xnec, xapl);
    uusarp_(&c__4999, &c__4999);
    uvbra_(&c__5, &rundef, xapl, xibm);
    uvbrf_(&c__5, &rundef, xnec, &rundef);
    uvbrf_(&c__5, &rundef, xnec, xapl);
    uvbrf_(&c__5, &rundef, xapl, xibm);
    uvbrl_(&c__5, &rundef, xnec);
    uvbrl_(&c__5, &rundef, xapl);
    uvbrl_(&c__5, &rundef, xibm);
    grcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int uupk07_ () { MAIN__ (); return 0; }

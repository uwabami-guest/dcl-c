/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static logical c_true = TRUE_;
static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static real c_b12 = 0.f;
static real c_b13 = 1.f;
static integer c__30 = 30;
static integer c__2 = 2;
static integer c__4 = 4;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static doublereal a;
    static integer i__;
    static doublereal r__;
    static real y[30];
    static integer iws;
    extern /* Subroutine */ int grcls_(void), grfrm_(void), gropn_(integer *),
	     uulin_(integer *, real *, real *), uumrk_(integer *, real *, 
	    real *);
    static real rundef;
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen), gllset_(char 
	    *, logical *, ftnlen), sglset_(char *, logical *, ftnlen), 
	    grswnd_(real *, real *, real *, real *), grstrf_(void), usdaxs_(
	    void), uspfit_(void), uuslni_(integer *), sgpwsn_(void), uusmkt_(
	    integer *), usspnt_(integer *, real *, real *);

    /* Fortran I/O blocks */
    static cilist io___6 = { 0, 6, 0, 0, 0 };
    static cilist io___7 = { 0, 5, 0, 0, 0 };


/* ---------------------------- DATA DEFINITION -------------------------- */
    r__ = .2;
    a = 3.7;
    for (i__ = 1; i__ <= 30; ++i__) {
	r__ = a * r__ * (1. - r__);
	y[i__ - 1] = r__;
/* L100: */
    }
    y[14] = 999.f;
    gllset_("LMISS", &c_true, (ftnlen)5);
    glrget_("RUNDEF", &rundef, (ftnlen)6);
/* ----------------------------- GRAPH ----------------------------------- */
    s_wsle(&io___6);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___7);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    gropn_(&iws);
    grfrm_();
    sglset_("LCLIP", &c_true, (ftnlen)5);
    grswnd_(&c_b12, &c_b13, &rundef, &rundef);
    usspnt_(&c__30, &rundef, y);
    uspfit_();
    grstrf_();
    usdaxs_();
    uuslni_(&c__2);
    uulin_(&c__30, &rundef, y);
    uusmkt_(&c__4);
    uumrk_(&c__30, &rundef, y);
    grcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int uupk01_ () { MAIN__ (); return 0; }

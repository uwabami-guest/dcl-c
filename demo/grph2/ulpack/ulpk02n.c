/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static logical c_true = TRUE_;
static real c_b8 = .6f;
static real c_b9 = 1.f;
static real c_b10 = .75f;
static integer c__7 = 7;
static real c_b14 = .2f;
static real c_b15 = 200.f;
static real c_b16 = -.15f;
static real c_b17 = -200.f;
static real c_b18 = .13f;
static real c_b19 = .14f;
static real c_b20 = .1f;
static real c_b21 = .9f;
static integer c__4 = 4;
static real c_b30 = 0.f;
static integer c__2 = 2;
static integer c__194 = 194;
static integer c__5 = 5;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Initialized data */

    static real bl[2] = { 1.f,3.f };

    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static char ctl[64];
    static integer iws;
    extern /* Subroutine */ int sgcls_(void), sgfrm_(void), sldiv_(char *, 
	    integer *, integer *, ftnlen), slrat_(real *, real *), sgopn_(
	    integer *), uzfact_(real *), sglset_(char *, logical *, ftnlen), 
	    sgswnd_(real *, real *, real *, real *), uliset_(char *, integer *
	    , ftnlen), sgstrf_(void), ulsybl_(real *, integer *), ulylog_(
	    char *, integer *, integer *, ftnlen), ulysfm_(char *, ftnlen), 
	    sgstrn_(integer *), sgpwsn_(void), uzinit_(void), sgsvpt_(real *, 
	    real *, real *, real *), uysttl_(char *, char *, real *, ftnlen, 
	    ftnlen);

    /* Fortran I/O blocks */
    static cilist io___2 = { 0, 6, 0, 0, 0 };
    static cilist io___3 = { 0, 5, 0, 0, 0 };


    s_wsle(&io___2);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I) ? ;", (ftnlen)23);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___3);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    i__1 = abs(iws);
    sgopn_(&i__1);
    sglset_("LFULL", &c_true, (ftnlen)5);
    uzfact_(&c_b8);
    slrat_(&c_b9, &c_b10);
    sldiv_("Y", &c__7, &c__1, (ftnlen)1);
    sgfrm_();
    uzinit_();
    sgswnd_(&c_b14, &c_b15, &c_b16, &c_b17);
    sgsvpt_(&c_b18, &c_b19, &c_b20, &c_b21);
    sgstrn_(&c__4);
    sgstrf_();
    uliset_("IYTYPE", &c__1, (ftnlen)6);
    ulylog_("L", &c__1, &c__9, (ftnlen)1);
    s_copy(ctl, "ULYLOG (IYTYPE=1,NLBL=1,NTCK=9)", (ftnlen)64, (ftnlen)31);
    uysttl_("L", ctl, &c_b30, (ftnlen)1, (ftnlen)64);
    sglset_("LCNTL", &c_true, (ftnlen)5);
    sgfrm_();
    uzinit_();
    sgswnd_(&c_b14, &c_b15, &c_b16, &c_b17);
    sgsvpt_(&c_b18, &c_b19, &c_b20, &c_b21);
    sgstrn_(&c__4);
    sgstrf_();
    uliset_("IYTYPE", &c__1, (ftnlen)6);
    ulylog_("L", &c__3, &c__9, (ftnlen)1);
    s_copy(ctl, "ULYLOG (IYTYPE=1,NLBL=3,NTCK=9)", (ftnlen)64, (ftnlen)31);
    uysttl_("L", ctl, &c_b30, (ftnlen)1, (ftnlen)64);
    sgfrm_();
    uzinit_();
    sgswnd_(&c_b14, &c_b15, &c_b16, &c_b17);
    sgsvpt_(&c_b18, &c_b19, &c_b20, &c_b21);
    sgstrn_(&c__4);
    sgstrf_();
    uliset_("IYTYPE", &c__2, (ftnlen)6);
    ulylog_("L", &c__3, &c__9, (ftnlen)1);
    s_copy(ctl, "ULYLOG (IYTYPE=2,NLBL=3,NTCK=9)", (ftnlen)64, (ftnlen)31);
    uysttl_("L", ctl, &c_b30, (ftnlen)1, (ftnlen)64);
    sgfrm_();
    uzinit_();
    sgswnd_(&c_b14, &c_b15, &c_b16, &c_b17);
    sgsvpt_(&c_b18, &c_b19, &c_b20, &c_b21);
    sgstrn_(&c__4);
    sgstrf_();
    uliset_("IYTYPE", &c__3, (ftnlen)6);
    ulylog_("L", &c__3, &c__9, (ftnlen)1);
    s_copy(ctl, "ULYLOG (IYTYPE=3,NLBL=3,NTCK=9)", (ftnlen)64, (ftnlen)31);
    uysttl_("L", ctl, &c_b30, (ftnlen)1, (ftnlen)64);
    sgfrm_();
    uzinit_();
    sgswnd_(&c_b14, &c_b15, &c_b16, &c_b17);
    sgsvpt_(&c_b18, &c_b19, &c_b20, &c_b21);
    sgstrn_(&c__4);
    sgstrf_();
    uliset_("IYTYPE", &c__4, (ftnlen)6);
    ulylog_("L", &c__3, &c__9, (ftnlen)1);
    s_copy(ctl, "ULYLOG (IYTYPE=4,NLBL=3,NTCK=9)", (ftnlen)64, (ftnlen)31);
    uysttl_("L", ctl, &c_b30, (ftnlen)1, (ftnlen)64);
    sgfrm_();
    uzinit_();
    sgswnd_(&c_b14, &c_b15, &c_b16, &c_b17);
    sgsvpt_(&c_b18, &c_b19, &c_b20, &c_b21);
    sgstrn_(&c__4);
    sgstrf_();
    ulysfm_("(F5.1)", (ftnlen)6);
    uliset_("IYTYPE", &c__3, (ftnlen)6);
    ulylog_("L", &c__3, &c__9, (ftnlen)1);
    s_copy(ctl, "ULYLOG (IYTYPE=3,NLBL=3,NTCK=9)", (ftnlen)64, (ftnlen)31);
    uysttl_("L", ctl, &c_b30, (ftnlen)1, (ftnlen)64);
    sgfrm_();
    uzinit_();
    sgswnd_(&c_b14, &c_b15, &c_b16, &c_b17);
    sgsvpt_(&c_b18, &c_b19, &c_b20, &c_b21);
    sgstrn_(&c__4);
    sgstrf_();
    uliset_("IYTYPE", &c__1, (ftnlen)6);
    uliset_("IYCHR", &c__194, (ftnlen)5);
    ulsybl_(bl, &c__2);
    ulylog_("L", &c__4, &c__5, (ftnlen)1);
    s_copy(ctl, "ULYLOG (IYTYPE=1,NLBL=4,NTCK=5)", (ftnlen)64, (ftnlen)31);
    uysttl_("L", ctl, &c_b30, (ftnlen)1, (ftnlen)64);
    sgcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int ulpk02_ () { MAIN__ (); return 0; }

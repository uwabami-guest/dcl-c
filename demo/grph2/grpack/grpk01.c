/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static real c_b1 = 0.f;
static real c_b2 = 1.f;
static real c_b3 = -.7f;
static real c_b4 = .7f;
static real c_b5 = .2f;
static real c_b6 = .8f;
static real c_b8 = .6f;
static integer c__1 = 1;
static real c_b14 = .5f;
static real c_b17 = .02f;
static integer c__0 = 0;
static integer c__3 = 3;
static real c_b23 = -.5f;
static real c_b27 = .62f;
static real c_b28 = .82f;
static logical c_false = FALSE_;
static logical c_true = TRUE_;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    static integer iws;
    extern /* Subroutine */ int grfig_(void), grcls_(void), grfrm_(void), 
	    gropn_(integer *), uzfact_(real *), grswnd_(real *, real *, real *
	    , real *), grstrf_(void), usdaxs_(void), grstrn_(integer *), 
	    grsvpt_(real *, real *, real *, real *), uzlset_(char *, logical *
	    , ftnlen), ussttl_(char *, char *, char *, char *, ftnlen, ftnlen,
	     ftnlen, ftnlen), sgtxzu_(real *, real *, char *, real *, integer 
	    *, integer *, integer *, ftnlen);

/*     WRITE(*,*) ' WORKSTATION ID (I)  ? ;' */
/*     CALL SGPWSN */
/*     READ (*,*) IWS */
    iws = 1;
    gropn_(&iws);
    grfrm_();
    grswnd_(&c_b1, &c_b2, &c_b3, &c_b4);
    grsvpt_(&c_b5, &c_b6, &c_b5, &c_b8);
    grstrn_(&c__1);
    grstrf_();
    ussttl_("X-AXIS", " ", "Y1-AXIS", " ", (ftnlen)6, (ftnlen)1, (ftnlen)7, (
	    ftnlen)1);
    usdaxs_();
    sgtxzu_(&c_b14, &c_b1, "PAGE:1, FRAME:1", &c_b17, &c__0, &c__0, &c__3, (
	    ftnlen)15);
    grfig_();
    grswnd_(&c_b1, &c_b2, &c_b23, &c_b14);
    grsvpt_(&c_b5, &c_b6, &c_b27, &c_b28);
    grstrn_(&c__1);
    grstrf_();
    uzlset_("LABELXB", &c_false, (ftnlen)7);
    ussttl_("X-AXIS", " ", "Y2-AXIS", " ", (ftnlen)6, (ftnlen)1, (ftnlen)7, (
	    ftnlen)1);
    usdaxs_();
    sgtxzu_(&c_b14, &c_b1, "PAGE:1, FRAME:2", &c_b17, &c__0, &c__0, &c__3, (
	    ftnlen)15);
    uzlset_("LABELXB", &c_true, (ftnlen)7);
    grfrm_();
    uzfact_(&c_b14);
    grswnd_(&c_b1, &c_b2, &c_b3, &c_b4);
    grsvpt_(&c_b5, &c_b6, &c_b5, &c_b8);
    grstrn_(&c__1);
    grstrf_();
    ussttl_("X-AXIS", " ", "Y1-AXIS", " ", (ftnlen)6, (ftnlen)1, (ftnlen)7, (
	    ftnlen)1);
    usdaxs_();
    sgtxzu_(&c_b14, &c_b1, "PAGE:2, FRAME:1", &c_b17, &c__0, &c__0, &c__3, (
	    ftnlen)15);
    grfig_();
    grswnd_(&c_b1, &c_b2, &c_b23, &c_b14);
    grsvpt_(&c_b5, &c_b6, &c_b27, &c_b28);
    grstrn_(&c__1);
    grstrf_();
    uzlset_("LABELXB", &c_false, (ftnlen)7);
    ussttl_("X-AXIS", " ", "Y2-AXIS", " ", (ftnlen)6, (ftnlen)1, (ftnlen)7, (
	    ftnlen)1);
    usdaxs_();
    sgtxzu_(&c_b14, &c_b1, "PAGE:2, FRAME:2", &c_b17, &c__0, &c__0, &c__3, (
	    ftnlen)15);
    uzlset_("LABELXB", &c_true, (ftnlen)7);
    grcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int grpk01_ () { MAIN__ (); return 0; }

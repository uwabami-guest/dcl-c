/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static doublereal c_b2 = 10.;
static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static integer c__4 = 4;
static integer c__100 = 100;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* System generated locals */
    integer i__1;
    doublereal d__1;

    /* Builtin functions */
    double pow_dd(doublereal *, doublereal *), exp(doublereal);
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static doublereal a;
    static integer i__;
    static doublereal r__;
    static real x[100], y[100], r0, x2;
    static integer iws;
    static real rexp;
    extern /* Subroutine */ int grcls_(void), grfrm_(void), gropn_(integer *),
	     usgrph_(integer *, real *, real *), grstrn_(integer *), sgpwsn_(
	    void);

    /* Fortran I/O blocks */
    static cilist io___9 = { 0, 6, 0, 0, 0 };
    static cilist io___10 = { 0, 5, 0, 0, 0 };


/* ---------------------------- DATA DEFINITION -------------------------- */
    r__ = .2;
    a = 3.6;
    r0 = 0.f;
    for (i__ = 1; i__ <= 100; ++i__) {
	r__ = a * r__ * (1. - r__);
	r0 = r0 + r__ * 4 - 2.58f;
/* Computing 2nd power */
	i__1 = i__ - 50;
	x2 = (real) (i__1 * i__1);
	rexp = i__ * 4.f / 100;
	d__1 = (doublereal) rexp;
	x[i__ - 1] = pow_dd(&c_b2, &d__1);
	d__1 = (doublereal) r0;
	y[i__ - 1] = exp(-x2) * 1e5f + pow_dd(&c_b2, &d__1);
/* L100: */
    }
    y[19] = 1e4f;
    y[39] = 2e3f;
    y[64] = 3e4f;
    y[69] = 500.f;
/* ----------------------------- GRAPH ----------------------------------- */
    s_wsle(&io___9);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___10);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    gropn_(&iws);
    grfrm_();
    grstrn_(&c__4);
    usgrph_(&c__100, x, y);
    grcls_();
/* ----------------------------------------------------------------------- */
    return 0;
} /* MAIN__ */

/* Main program alias */ int uspk02_ () { MAIN__ (); return 0; }

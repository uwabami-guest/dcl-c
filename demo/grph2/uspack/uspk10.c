/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static integer c__400 = 400;
static real c_b9 = 0.f;
static real c_b11 = 1.25f;
static real c_b13 = .25f;
static real c_b15 = .1f;
static real c_b17 = 10.f;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* System generated locals */
    real r__1;

    /* Builtin functions */
    double sin(doublereal);
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static integer i__;
    static real t, x[400], y[400], dt, pi;
    static integer iws;
    extern /* Subroutine */ int grcls_(void), grfrm_(void), gropn_(integer *);
    static real rundef;
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen), grswnd_(real 
	    *, real *, real *, real *), usgrph_(integer *, real *, real *), 
	    sgpwsn_(void), usrset_(char *, real *, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___7 = { 0, 6, 0, 0, 0 };
    static cilist io___8 = { 0, 5, 0, 0, 0 };


    pi = 3.14159f;
    dt = pi * 2 / 399;
    for (i__ = 1; i__ <= 400; ++i__) {
	t = dt * (i__ - 1);
	x[i__ - 1] = t;
	y[i__ - 1] = sin(t * 2.f) * 10.f;
/* L100: */
    }
/* ---------------------------- 1ST PAGE --------------------------------- */
    s_wsle(&io___7);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___8);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    gropn_(&iws);
    grfrm_();
    usgrph_(&c__400, x, y);
/* ---------------------------- 2ND PAGE --------------------------------- */
    glrget_("RUNDEF", &rundef, (ftnlen)6);
    grfrm_();
    r__1 = pi * 2.f;
    grswnd_(&c_b9, &r__1, &rundef, &rundef);
    usrset_("DXL", &c_b11, (ftnlen)3);
    usrset_("DXT", &c_b13, (ftnlen)3);
    usrset_("XFAC", &c_b15, (ftnlen)4);
    usrset_("YFAC", &c_b17, (ftnlen)4);
    usgrph_(&c__400, x, y);
/* ----------------------------------------------------------------------- */
    grcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int uspk10_ () { MAIN__ (); return 0; }

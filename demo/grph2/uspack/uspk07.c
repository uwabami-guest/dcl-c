/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c_n1 = -1;
static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static real c_b11 = 0.f;
static integer c__0 = 0;
static integer c__200 = 200;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Builtin functions */
    integer pow_ii(integer *, integer *);
    double cos(doublereal);
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static real a[10];
    static integer i__, j;
    static real t, x[200], y[200];
    static integer jj;
    static real dt, pi, yd, yy;
    static integer iws;
    extern /* Subroutine */ int grcls_(void), grfrm_(void), gropn_(integer *),
	     uscset_(char *, char *, ftnlen, ftnlen), usgrph_(integer *, real 
	    *, real *), sgpwsn_(void), uziset_(char *, integer *, ftnlen), 
	    uzrset_(char *, real *, ftnlen), ussttl_(char *, char *, char *, 
	    char *, ftnlen, ftnlen, ftnlen, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___12 = { 0, 6, 0, 0, 0 };
    static cilist io___13 = { 0, 5, 0, 0, 0 };


/* ----------------------------------------------------------------------- */
    dt = .0050251256281407036f;
    pi = 3.14159f;
    for (j = 1; j <= 10; ++j) {
	jj = (j << 1) - 1;
	a[j - 1] = pow_ii(&c_n1, &j) * 2.f / (jj * pi);
/* L50: */
    }
    for (i__ = 1; i__ <= 200; ++i__) {
	t = dt * (i__ - 1) * 2 * pi;
	x[i__ - 1] = dt * (i__ - 1);
	yy = 0.f;
	for (j = 1; j <= 10; ++j) {
	    jj = (j << 1) - 1;
	    yd = a[j - 1] * cos(jj * t);
	    yy += yd;
/* L150: */
	}
	y[i__ - 1] = yy * 2.f;
/* L100: */
    }
/* ----------------------------------------------------------------------- */
    s_wsle(&io___12);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___13);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    gropn_(&iws);
    grfrm_();
/*          --- X AXIS --- */
    uzrset_("UYUSER", &c_b11, (ftnlen)6);
    uscset_("CXSIDE", "U", (ftnlen)6, (ftnlen)1);
/*          --- Y AXIS --- */
    uziset_("IROTLYL", &c__1, (ftnlen)7);
    uziset_("ICENTYL", &c__0, (ftnlen)7);
    uscset_("CYSIDE", "L", (ftnlen)6, (ftnlen)1);
/*          --- etc. --- */
    uziset_("INNER", &c_n1, (ftnlen)5);
    uscset_("CBLKT", "[]", (ftnlen)5, (ftnlen)2);
    ussttl_("TIME", "SEC", "VOLTAGE", "mV", (ftnlen)4, (ftnlen)3, (ftnlen)7, (
	    ftnlen)2);
    usgrph_(&c__200, x, y);
    grcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int uspk07_ () { MAIN__ (); return 0; }

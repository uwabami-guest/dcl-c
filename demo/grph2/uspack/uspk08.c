/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static logical c_true = TRUE_;
static integer c__228 = 228;
static integer c__200 = 200;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* System generated locals */
    address a__1[3];
    integer i__1[3];
    real r__1;
    char ch__1[6], ch__2[3];

    /* Builtin functions */
    double exp(doublereal);
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen);

    /* Local variables */
    static doublereal a;
    static integer i__;
    static doublereal r__;
    static real t[200], z__[200], z2;
    static integer iws;
    extern /* Character */ VOID usgi_(char *, ftnlen, integer *);
    extern /* Subroutine */ int grcls_(void), grfrm_(void), gropn_(integer *);
    static logical lsfnt;
    extern /* Subroutine */ int swlget_(char *, logical *, ftnlen), uscset_(
	    char *, char *, ftnlen, ftnlen), usgrph_(integer *, real *, real *
	    ), uslset_(char *, logical *, ftnlen), sgpwsn_(void), uzlset_(
	    char *, logical *, ftnlen), ussttl_(char *, char *, char *, char *
	    , ftnlen, ftnlen, ftnlen, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___7 = { 0, 6, 0, 0, 0 };
    static cilist io___8 = { 0, 5, 0, 0, 0 };


    r__ = .2;
    a = 4.;
    for (i__ = 1; i__ <= 200; ++i__) {
	r__ = a * r__ * (1. - r__);
/* Computing 2nd power */
	r__1 = (real) (i__ - 5) / 40.f;
	z2 = r__1 * r__1;
	t[i__ - 1] = exp(-z2) * 20.f + r__ * 2.f;
	z__[i__ - 1] = (real) (i__ << 1);
/* L10: */
    }
/* ----------------------------------------------------------------------- */
    s_wsle(&io___7);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___8);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    gropn_(&iws);
    grfrm_();
/*          --- OMAJINAI --- */
    uslset_("LYINV", &c_true, (ftnlen)5);
    uzlset_("LABELXT", &c_true, (ftnlen)7);
    uscset_("CYSPOS", "B", (ftnlen)6, (ftnlen)1);
    uscset_("CXSIDE", "T", (ftnlen)6, (ftnlen)1);
    uscset_("CYSIDE", "L", (ftnlen)6, (ftnlen)1);
    swlget_("LSYSFNT", &lsfnt, (ftnlen)7);
/* Writing concatenation */
    i__1[0] = 2, a__1[0] = "\\^";
    usgi_(ch__2, (ftnlen)3, &c__228);
    i__1[1] = 3, a__1[1] = ch__2;
    i__1[2] = 1, a__1[2] = "C";
    s_cat(ch__1, a__1, i__1, &c__3, (ftnlen)6);
    ussttl_("TEMPERATURE", ch__1, "DEPTH", "m", (ftnlen)11, (ftnlen)6, (
	    ftnlen)5, (ftnlen)1);
    usgrph_(&c__200, t, z__);
    grcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int uspk08_ () { MAIN__ (); return 0; }

/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static real c_b6 = 2.f;
static real c_b7 = 3.f;
static integer c__2 = 2;
static real c_b12 = 45.f;
static real c_b14 = 30.f;
static logical c_false = FALSE_;
static real c_b20 = 0.f;
static real c_b23 = 90.f;
static real c_b25 = .1f;
static real c_b26 = .9f;
static real c_b29 = -180.f;
static real c_b30 = 180.f;
static real c_b35 = -90.f;
static logical c_true = TRUE_;
static real c_b41 = .5f;
static real c_b42 = .95f;
static real c_b43 = .03f;
static integer c__0 = 0;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Initialized data */

    static integer ntr[14] = { 10,11,12,13,14,15,20,21,22,23,30,31,32,33 };
    static real fct[14] = { .12f,.12f,.14f,.14f,.14f,.14f,.11f,.16f,.12f,.12f,
	    .4f,.12f,.12f,.17f };

    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static integer i__, iws;
    static char cttl[32];
    extern /* Subroutine */ int sgcls_(void), sgfrm_(void), sldiv_(char *, 
	    integer *, integer *, ftnlen), slrat_(real *, real *), sgopn_(
	    integer *), umpglb_(void), umpmap_(char *, ftnlen), sglset_(char *
	    , logical *, ftnlen), sgssim_(real *, real *, real *), umiset_(
	    char *, integer *, ftnlen), sgrset_(char *, real *, ftnlen), 
	    sgsmpl_(real *, real *, real *), umlset_(char *, logical *, 
	    ftnlen), sgstrf_(void), sgtrnl_(integer *, char *, ftnlen), 
	    sgstrn_(integer *), sgpwsn_(void), sgsvpt_(real *, real *, real *,
	     real *), slpvpr_(integer *), slpwwr_(integer *), sgstxy_(real *, 
	    real *, real *, real *), sgtxzr_(real *, real *, char *, real *, 
	    integer *, integer *, integer *, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___3 = { 0, 6, 0, 0, 0 };
    static cilist io___4 = { 0, 5, 0, 0, 0 };


    s_wsle(&io___3);
    do_lio(&c__9, &c__1, " WORKSTATION IS (I) ? ;", (ftnlen)23);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___4);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    i__1 = -abs(iws);
    sgopn_(&i__1);
    slrat_(&c_b6, &c_b7);
    sldiv_("Y", &c__2, &c__3, (ftnlen)1);
    sgrset_("STLAT1", &c_b12, (ftnlen)6);
    sgrset_("STLAT2", &c_b14, (ftnlen)6);
    umlset_("LGRIDMN", &c_false, (ftnlen)7);
    umiset_("INDEXMJ", &c__1, (ftnlen)7);
    for (i__ = 1; i__ <= 14; ++i__) {
	sgfrm_();
	sgssim_(&fct[i__ - 1], &c_b20, &c_b20);
	sgsmpl_(&c_b20, &c_b23, &c_b20);
	sgsvpt_(&c_b25, &c_b26, &c_b25, &c_b26);
	if (ntr[i__ - 1] == 30) {
	    sgstxy_(&c_b29, &c_b30, &c_b20, &c_b23);
	} else {
	    sgstxy_(&c_b29, &c_b30, &c_b35, &c_b23);
	}
	sgstrn_(&ntr[i__ - 1]);
	sgstrf_();
	sglset_("LCLIP", &c_true, (ftnlen)5);
	slpwwr_(&c__1);
	slpvpr_(&c__1);
	sgtrnl_(&ntr[i__ - 1], cttl, (ftnlen)32);
	sgtxzr_(&c_b41, &c_b42, cttl, &c_b43, &c__0, &c__0, &c__3, (ftnlen)32)
		;
	umpmap_("coast_world", (ftnlen)11);
	umpglb_();
	if (ntr[i__ - 1] == 23) {
	    sgfrm_();
	    sgfrm_();
	}
/* L10: */
    }
    sgcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int umpk01_ () { MAIN__ (); return 0; }

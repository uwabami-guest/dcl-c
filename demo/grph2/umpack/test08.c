/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static logical c_true = TRUE_;
static real c_b8 = 1.f;
static real c_b9 = .6f;
static real c_b10 = 140.f;
static real c_b11 = 90.f;
static real c_b12 = 0.f;
static integer c__30 = 30;
static integer c__20 = 20;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static integer iws;
    extern /* Subroutine */ int grcls_(void), grfrm_(void), gropn_(integer *),
	     slrat_(real *, real *), umpglb_(void);
    extern integer isgtrc_(char *, ftnlen);
    extern /* Subroutine */ int umpmap_(char *, ftnlen), sglset_(char *, 
	    logical *, ftnlen), grsmpl_(real *, real *, real *), umpfit_(void)
	    , umiset_(char *, integer *, ftnlen), grstrf_(void), grstrn_(
	    integer *), sgpwsn_(void);

    /* Fortran I/O blocks */
    static cilist io___1 = { 0, 6, 0, 0, 0 };
    static cilist io___2 = { 0, 5, 0, 0, 0 };


    s_wsle(&io___1);
    do_lio(&c__9, &c__1, " WORKSTATION IS (I) ? ;", (ftnlen)23);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___2);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    sglset_("LFULL", &c_true, (ftnlen)5);
    gropn_(&iws);
    slrat_(&c_b8, &c_b9);
    grfrm_();
    grsmpl_(&c_b10, &c_b11, &c_b12);
    i__1 = isgtrc_("HMR", (ftnlen)3);
    grstrn_(&i__1);
    umpfit_();
    grstrf_();
    umpglb_();
    umiset_("INDEXOUT", &c__3, (ftnlen)8);
    umpmap_("coast_world", (ftnlen)11);
    umiset_("INDEXOUT", &c__30, (ftnlen)8);
    umpmap_("border_world", (ftnlen)12);
    umiset_("INDEXOUT", &c__20, (ftnlen)8);
    umpmap_("plate_world", (ftnlen)11);
    grcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int test08_ () { MAIN__ (); return 0; }

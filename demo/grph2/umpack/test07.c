/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static real c_b7 = 123.f;
static real c_b8 = 147.f;
static real c_b9 = 30.f;
static real c_b10 = 46.f;
static real c_b11 = .1f;
static real c_b12 = .9f;
static logical c_true = TRUE_;
static real c_b19 = .5f;
static real c_b20 = .95f;
static real c_b21 = .03f;
static integer c__0 = 0;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Initialized data */

    static char ctr[3*14] = "CYL" "MER" "MWD" "HMR" "EK6" "KTD" "CON" "COA" 
	    "COC" "BON" "OTG" "PST" "AZM" "AZA";

    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static integer i__, iws;
    static char cttl[32];
    extern /* Subroutine */ int grcls_(void), grfrm_(void), gropn_(integer *),
	     umpglb_(void);
    extern integer isgtrc_(char *, ftnlen);
    extern /* Subroutine */ int umpmap_(char *, ftnlen), sglset_(char *, 
	    logical *, ftnlen), grswnd_(real *, real *, real *, real *), 
	    umpfit_(void), grstrf_(void), sgtrnl_(integer *, char *, ftnlen), 
	    grstrn_(integer *), sgpwsn_(void), grsvpt_(real *, real *, real *,
	     real *), slpvpr_(integer *), slpwwr_(integer *), sgtxzr_(real *, 
	    real *, char *, real *, integer *, integer *, integer *, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___2 = { 0, 6, 0, 0, 0 };
    static cilist io___3 = { 0, 5, 0, 0, 0 };


    s_wsle(&io___2);
    do_lio(&c__9, &c__1, " WORKSTATION IS (I) ? ;", (ftnlen)23);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___3);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    gropn_(&iws);
    for (i__ = 1; i__ <= 14; ++i__) {
	grfrm_();
	grswnd_(&c_b7, &c_b8, &c_b9, &c_b10);
	grsvpt_(&c_b11, &c_b12, &c_b11, &c_b12);
	i__1 = isgtrc_(ctr + (i__ - 1) * 3, (ftnlen)3);
	grstrn_(&i__1);
	umpfit_();
	grstrf_();
	sglset_("LCLIP", &c_true, (ftnlen)5);
	slpwwr_(&c__1);
	slpvpr_(&c__1);
	i__1 = isgtrc_(ctr + (i__ - 1) * 3, (ftnlen)3);
	sgtrnl_(&i__1, cttl, (ftnlen)32);
	sgtxzr_(&c_b19, &c_b20, cttl, &c_b21, &c__0, &c__0, &c__3, (ftnlen)32)
		;
	umpmap_("coast_japan", (ftnlen)11);
	umpglb_();
/* L10: */
    }
    grcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int test07_ () { MAIN__ (); return 0; }

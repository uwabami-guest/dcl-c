/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static logical c_true = TRUE_;
static integer c__36 = 36;
static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static real c_b17 = 0.f;
static real c_b18 = 360.f;
static real c_b19 = -90.f;
static real c_b20 = 90.f;
static real c_b21 = .1f;
static real c_b22 = .9f;
static real c_b25 = .4f;
static real c_b28 = 165.f;
static real c_b29 = 60.f;
static real c_b31 = -180.f;
static real c_b32 = 180.f;
static integer c__30 = 30;
static logical c_false = FALSE_;
static real c_b41 = 30.f;
static integer c__37 = 37;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* System generated locals */
    integer i__1;
    real r__1, r__2;

    /* Builtin functions */
    double sin(doublereal), cos(doublereal);
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static integer i__, j;
    static real p[1369]	/* was [37][37] */, u[1369]	/* was [37][37] */, v[
	    1369]	/* was [37][37] */;
    static integer iws;
    static real alat[37];
    extern integer imod_(integer *, integer *);
    static real alon[37], slat;
    extern /* Subroutine */ int grcls_(void), grfrm_(void), gropn_(integer *);
    static real rmiss;
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen), umpglb_(void)
	    , gllset_(char *, logical *, ftnlen), umpmap_(char *, ftnlen), 
	    udcntr_(real *, integer *, integer *, integer *), sglset_(char *, 
	    logical *, ftnlen), grswnd_(real *, real *, real *, real *), 
	    grssim_(real *, real *, real *), grsmpl_(real *, real *, real *), 
	    grstrf_(void), sglazu_(real *, real *, real *, real *, integer *, 
	    integer *), umlset_(char *, logical *, ftnlen), grstrn_(integer *)
	    , umrset_(char *, real *, ftnlen), sgpwsn_(void), grsvpt_(real *, 
	    real *, real *, real *), grstxy_(real *, real *, real *, real *);

    /* Fortran I/O blocks */
    static cilist io___10 = { 0, 6, 0, 0, 0 };
    static cilist io___11 = { 0, 5, 0, 0, 0 };


    glrget_("RMISS", &rmiss, (ftnlen)5);
    gllset_("LMISS", &c_true, (ftnlen)5);
    for (i__ = 1; i__ <= 37; ++i__) {
	alon[i__ - 1] = (i__ - 1) * 360.f / 36 + 0.f;
/* L10: */
    }
    for (j = 1; j <= 37; ++j) {
	alat[j - 1] = (j - 1) * 180.f / 36 - 90.f;
/* L20: */
    }
    for (j = 1; j <= 37; ++j) {
	for (i__ = 1; i__ <= 37; ++i__) {
	    slat = sin(alat[j - 1] * .01745328888888889f);
/* Computing 2nd power */
	    r__1 = slat;
	    p[i__ + j * 37 - 38] = cos(alon[i__ - 1] * .01745328888888889f) * 
		    (1 - r__1 * r__1) * sin(slat * 6.2831840000000003f) + 
		    .05f;
/* L30: */
	}
/* L40: */
    }
    for (j = 1; j <= 37; ++j) {
	for (i__ = 1; i__ <= 37; ++i__) {
	    if (j == 1 || j == 37) {
		u[i__ + j * 37 - 38] = rmiss;
		v[i__ + j * 37 - 38] = rmiss;
	    } else {
		u[i__ + j * 37 - 38] = (p[i__ + (j - 1) * 37 - 38] - p[i__ + (
			j + 1) * 37 - 38]) * 10.f;
		i__1 = i__ - 2;
		v[i__ + j * 37 - 38] = (p[imod_(&i__, &c__36) + 1 + j * 37 - 
			38] - p[imod_(&i__1, &c__36) + 1 + j * 37 - 38]) * 
			10.f;
	    }
/* L50: */
	}
/* L60: */
    }
    s_wsle(&io___10);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___11);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    gropn_(&iws);
    grfrm_();
    grswnd_(&c_b17, &c_b18, &c_b19, &c_b20);
    grsvpt_(&c_b21, &c_b22, &c_b21, &c_b22);
    grssim_(&c_b25, &c_b17, &c_b17);
    grsmpl_(&c_b28, &c_b29, &c_b17);
    grstxy_(&c_b31, &c_b32, &c_b17, &c_b20);
    grstrn_(&c__30);
    grstrf_();
    sglset_("LCLIP", &c_true, (ftnlen)5);
    umlset_("LGRIDMJ", &c_false, (ftnlen)7);
    umrset_("DGRIDMN", &c_b41, (ftnlen)7);
    umpmap_("coast_world", (ftnlen)11);
    umpglb_();
    udcntr_(p, &c__37, &c__37, &c__37);
    for (j = 1; j <= 37; ++j) {
	for (i__ = 1; i__ <= 37; ++i__) {
	    if (! (u[i__ + j * 37 - 38] == rmiss || v[i__ + j * 37 - 38] == 
		    rmiss)) {
		r__1 = alon[i__ - 1] + u[i__ + j * 37 - 38];
		r__2 = alat[j - 1] + v[i__ + j * 37 - 38];
		sglazu_(&alon[i__ - 1], &alat[j - 1], &r__1, &r__2, &c__1, &
			c__3);
	    }
/* L70: */
	}
/* L80: */
    }
    grcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int test03_ () { MAIN__ (); return 0; }

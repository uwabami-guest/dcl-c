/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static logical c_false = FALSE_;
static real c_b11 = 0.f;
static real c_b12 = 360.f;
static real c_b13 = -90.f;
static real c_b14 = 90.f;
static real c_b15 = .1f;
static real c_b16 = .9f;
static real c_b19 = .4f;
static real c_b22 = 165.f;
static real c_b23 = 60.f;
static real c_b25 = -180.f;
static real c_b26 = 180.f;
static integer c__30 = 30;
static logical c_true = TRUE_;
static real c_b32 = -.2f;
static integer c__201 = 201;
static real c_b34 = .2f;
static real c_b35 = .40000000000000002f;
static integer c__401 = 401;
static integer c__402 = 402;
static integer c__19 = 19;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* System generated locals */
    real r__1;

    /* Builtin functions */
    double sin(doublereal), cos(doublereal);
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static integer i__, j;
    static real p[361]	/* was [19][19] */;
    static integer iws;
    static real alat, alon, slat;
    extern /* Subroutine */ int grcls_(void), grfrm_(void), gropn_(integer *);
    static real rmiss;
    extern /* Subroutine */ int udgclb_(real *, integer *, integer *, integer 
	    *, real *), glrget_(char *, real *, ftnlen), umpglb_(void), 
	    uetone_(real *, integer *, integer *, integer *), udcntr_(real *, 
	    integer *, integer *, integer *), sglset_(char *, logical *, 
	    ftnlen), umpmap_(char *, ftnlen), grswnd_(real *, real *, real *, 
	    real *), grssim_(real *, real *, real *), grsmpl_(real *, real *, 
	    real *), grstrf_(void), grstrn_(integer *), sgpwsn_(void), 
	    uestlv_(real *, real *, integer *), grsvpt_(real *, real *, real *
	    , real *), grstxy_(real *, real *, real *, real *);

    /* Fortran I/O blocks */
    static cilist io___7 = { 0, 6, 0, 0, 0 };
    static cilist io___8 = { 0, 5, 0, 0, 0 };


    for (j = 1; j <= 19; ++j) {
	for (i__ = 1; i__ <= 19; ++i__) {
	    alon = ((i__ - 1) * 360.f / 18 + 0.f) * .01745328888888889f;
	    alat = ((j - 1) * 180.f / 18 - 90.f) * .01745328888888889f;
	    slat = sin(alat);
/* Computing 2nd power */
	    r__1 = slat;
	    p[i__ + j * 19 - 20] = cos(alon) * (1 - r__1 * r__1) * sin(slat * 
		    6.2831840000000003f) + .05f;
/* L10: */
	}
/* L20: */
    }
    s_wsle(&io___7);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___8);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    gropn_(&iws);
    glrget_("RMISS", &rmiss, (ftnlen)5);
    sglset_("LSOFTF", &c_false, (ftnlen)6);
    grfrm_();
    grswnd_(&c_b11, &c_b12, &c_b13, &c_b14);
    grsvpt_(&c_b15, &c_b16, &c_b15, &c_b16);
    grssim_(&c_b19, &c_b11, &c_b11);
    grsmpl_(&c_b22, &c_b23, &c_b11);
    grstxy_(&c_b25, &c_b26, &c_b11, &c_b14);
    grstrn_(&c__30);
    grstrf_();
    sglset_("LCLIP", &c_true, (ftnlen)5);
    uestlv_(&rmiss, &c_b32, &c__201);
    uestlv_(&c_b34, &c_b35, &c__401);
    uestlv_(&c_b35, &rmiss, &c__402);
    uetone_(p, &c__19, &c__19, &c__19);
    udgclb_(p, &c__19, &c__19, &c__19, &c_b34);
    udcntr_(p, &c__19, &c__19, &c__19);
    umpmap_("coast_world", (ftnlen)11);
    umpglb_();
    grcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int test02_ () { MAIN__ (); return 0; }

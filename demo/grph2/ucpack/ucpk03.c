/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static logical c_true = TRUE_;
static real c_b8 = .7f;
static real c_b9 = 1.f;
static integer c__7 = 7;
static real c_b14 = 0.f;
static real c_b17 = .1f;
static real c_b18 = .9f;
static real c_b20 = .11f;
static integer c_b23 = 19920401;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* System generated locals */
    real r__1;

    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static integer nd, iws;
    extern /* Subroutine */ int sgcls_(void), sgfrm_(void), sldiv_(char *, 
	    integer *, integer *, ftnlen), slrat_(real *, real *), sgopn_(
	    integer *), ucxacl_(char *, integer *, integer *, ftnlen), 
	    uzfact_(real *), sglset_(char *, logical *, ftnlen), sgswnd_(real 
	    *, real *, real *, real *), sgstrf_(void), sgstrn_(integer *), 
	    sgpwsn_(void), uzinit_(void), sgsvpt_(real *, real *, real *, 
	    real *);

    /* Fortran I/O blocks */
    static cilist io___1 = { 0, 6, 0, 0, 0 };
    static cilist io___2 = { 0, 5, 0, 0, 0 };


    s_wsle(&io___1);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I) ? ;", (ftnlen)23);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___2);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    sgopn_(&iws);
    sglset_("LFULL", &c_true, (ftnlen)5);
    uzfact_(&c_b8);
    slrat_(&c_b9, &c_b9);
    sldiv_("T", &c__1, &c__7, (ftnlen)1);
    sgfrm_();
    uzinit_();
    nd = 30;
    r__1 = (real) nd;
    sgswnd_(&c_b14, &r__1, &c_b14, &c_b9);
    sgsvpt_(&c_b17, &c_b18, &c_b17, &c_b20);
    sgstrn_(&c__1);
    sgstrf_();
    ucxacl_("B", &c_b23, &nd, (ftnlen)1);
    sgfrm_();
    uzinit_();
    nd = 90;
    r__1 = (real) nd;
    sgswnd_(&c_b14, &r__1, &c_b14, &c_b9);
    sgsvpt_(&c_b17, &c_b18, &c_b17, &c_b20);
    sgstrn_(&c__1);
    sgstrf_();
    ucxacl_("B", &c_b23, &nd, (ftnlen)1);
    sgfrm_();
    uzinit_();
    nd = 180;
    r__1 = (real) nd;
    sgswnd_(&c_b14, &r__1, &c_b14, &c_b9);
    sgsvpt_(&c_b17, &c_b18, &c_b17, &c_b20);
    sgstrn_(&c__1);
    sgstrf_();
    ucxacl_("B", &c_b23, &nd, (ftnlen)1);
    sgfrm_();
    uzinit_();
    nd = 400;
    r__1 = (real) nd;
    sgswnd_(&c_b14, &r__1, &c_b14, &c_b9);
    sgsvpt_(&c_b17, &c_b18, &c_b17, &c_b20);
    sgstrn_(&c__1);
    sgstrf_();
    ucxacl_("B", &c_b23, &nd, (ftnlen)1);
    sgfrm_();
    uzinit_();
    nd = 1000;
    r__1 = (real) nd;
    sgswnd_(&c_b14, &r__1, &c_b14, &c_b9);
    sgsvpt_(&c_b17, &c_b18, &c_b17, &c_b20);
    sgstrn_(&c__1);
    sgstrf_();
    ucxacl_("B", &c_b23, &nd, (ftnlen)1);
    sgfrm_();
    uzinit_();
    nd = 3000;
    r__1 = (real) nd;
    sgswnd_(&c_b14, &r__1, &c_b14, &c_b9);
    sgsvpt_(&c_b17, &c_b18, &c_b17, &c_b20);
    sgstrn_(&c__1);
    sgstrf_();
    ucxacl_("B", &c_b23, &nd, (ftnlen)1);
    sgfrm_();
    uzinit_();
    nd = 9000;
    r__1 = (real) nd;
    sgswnd_(&c_b14, &r__1, &c_b14, &c_b9);
    sgsvpt_(&c_b17, &c_b18, &c_b17, &c_b20);
    sgstrn_(&c__1);
    sgstrf_();
    ucxacl_("B", &c_b23, &nd, (ftnlen)1);
    sgcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int ucpk01_ () { MAIN__ (); return 0; }

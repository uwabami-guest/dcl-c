/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static logical c_false = FALSE_;
static logical c_true = TRUE_;
static real c_b30 = .1f;
static real c_b34 = 1.f;
static real c_b37 = 0.f;
static real c_b38 = 16.f;
static real c_b41 = .05f;
static real c_b42 = .95f;
static real c_b44 = .9f;
static integer c__2 = 2;
static real c_b55 = .5f;
static real c_b57 = .03f;
static integer c__0 = 0;
static real c_b62 = .035f;
static real c_b68 = .01f;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Initialized data */

    static char cttl[32] = "FONT NO. = #                    ";

    /* System generated locals */
    real r__1, r__2;
    char ch__1[3];

    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void), s_wsfi(icilist *), 
	    do_fio(integer *, char *, ftnlen), e_wsfi(void);

    /* Local variables */
    static char fontname[70];
    static integer i__, n;
    static char ch[3];
    static real ux, uy;
    static integer iws;
    extern /* Character */ VOID usgi_(char *, ftnlen, integer *);
    static integer isys;
    extern /* Subroutine */ int sgcls_(void), sgfrm_(void), slmgn_(real *, 
	    real *, real *, real *), slrat_(real *, real *), sgopn_(integer *)
	    , sgiset_(char *, integer *, ftnlen), sglset_(char *, logical *, 
	    ftnlen), sgswnd_(real *, real *, real *, real *), swcset_(char *, 
	    char *, ftnlen, ftnlen), sgstrf_(void), sgstrn_(integer *), 
	    sgpwsn_(void), swlset_(char *, logical *, ftnlen), swslft_(char *,
	     ftnlen), sglnzu_(real *, real *, real *, real *, integer *), 
	    sgsvpt_(real *, real *, real *, real *), sgtxzu_(real *, real *, 
	    char *, real *, integer *, integer *, integer *, ftnlen), sgtxzv_(
	    real *, real *, char *, real *, integer *, integer *, integer *, 
	    ftnlen);

    /* Fortran I/O blocks */
    static cilist io___2 = { 0, 6, 0, 0, 0 };
    static cilist io___3 = { 0, 5, 0, 0, 0 };
    static cilist io___5 = { 0, 6, 0, 0, 0 };
    static cilist io___6 = { 0, 5, 0, 0, 0 };
    static cilist io___8 = { 0, 6, 0, 0, 0 };
    static cilist io___9 = { 0, 5, 0, 0, 0 };
    static cilist io___11 = { 0, 6, 0, 0, 0 };
    static cilist io___12 = { 0, 5, 0, 0, 0 };
    static icilist io___15 = { 0, cttl+11, 0, "(I1)", 1, 1 };
    static icilist io___19 = { 0, ch, 0, "(I3)", 3, 1 };


    s_wsle(&io___2);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___3);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    s_wsle(&io___5);
    do_lio(&c__9, &c__1, " STROKE FONT(1) / SYSTEM FONT(2)  ? ;", (ftnlen)37);
    e_wsle();
    s_rsle(&io___6);
    do_lio(&c__3, &c__1, (char *)&isys, (ftnlen)sizeof(integer));
    e_rsle();
    sglset_("LCNTL", &c_false, (ftnlen)5);
    if (isys == 2) {
	swlset_("LSYSFNT", &c_true, (ftnlen)7);
	s_wsle(&io___8);
	do_lio(&c__9, &c__1, " FONT NO (1,2) (I) OR DIALOG(3) ? ;", (ftnlen)
		35);
	e_wsle();
	s_rsle(&io___9);
	do_lio(&c__3, &c__1, (char *)&n, (ftnlen)sizeof(integer));
	e_rsle();
    } else {
	swlset_("LSYSFNT", &c_false, (ftnlen)7);
	s_wsle(&io___11);
	do_lio(&c__9, &c__1, " FONT NO (1,2) (I)  ? ;", (ftnlen)23);
	e_wsle();
	s_rsle(&io___12);
	do_lio(&c__3, &c__1, (char *)&n, (ftnlen)sizeof(integer));
	e_rsle();
    }
    sgopn_(&iws);
    if (isys == 2 && n == 3) {
	swslft_(fontname, (ftnlen)70);
	swcset_("FONTNAME", fontname, (ftnlen)8, (ftnlen)70);
    }
    sglset_("LCNTL", &c_false, (ftnlen)5);
    slmgn_(&c_b30, &c_b30, &c_b30, &c_b30);
    slrat_(&c_b34, &c_b34);
    sgfrm_();
    sgiset_("IFONT", &n, (ftnlen)5);
    sgswnd_(&c_b37, &c_b38, &c_b37, &c_b38);
    sgsvpt_(&c_b41, &c_b42, &c_b37, &c_b44);
    sgstrn_(&c__1);
    sgstrf_();
    for (i__ = 0; i__ <= 16; ++i__) {
	r__1 = (real) i__;
	r__2 = (real) i__;
	sglnzu_(&r__1, &c_b37, &r__2, &c_b38, &c__2);
	r__1 = (real) i__;
	r__2 = (real) i__;
	sglnzu_(&c_b37, &r__1, &c_b38, &r__2, &c__2);
/* L10: */
    }
    s_wsfi(&io___15);
    do_fio(&c__1, (char *)&n, (ftnlen)sizeof(integer));
    e_wsfi();
    sgtxzv_(&c_b55, &c_b42, cttl, &c_b57, &c__0, &c__0, &c__3, (ftnlen)32);
    for (i__ = 0; i__ <= 255; ++i__) {
	ux = i__ / 16 + .5f;
	uy = 16 - i__ % 16 - .5f;
	usgi_(ch__1, (ftnlen)3, &i__);
	sgtxzu_(&ux, &uy, ch__1, &c_b62, &c__0, &c__0, &c__3, (ftnlen)3);
	ux += .48f;
	uy += .38f;
	s_wsfi(&io___19);
	do_fio(&c__1, (char *)&i__, (ftnlen)sizeof(integer));
	e_wsfi();
	sgtxzu_(&ux, &uy, ch, &c_b68, &c__0, &c__1, &c__1, (ftnlen)3);
/* L15: */
    }
    sgcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int sgfont_ () { MAIN__ (); return 0; }

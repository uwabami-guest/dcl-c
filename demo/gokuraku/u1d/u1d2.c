/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static real c_b8 = 0.f;
static real c_b9 = 50.f;
static integer c__51 = 51;
static integer c__2 = 2;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* System generated locals */
    real r__1, r__2;

    /* Builtin functions */
    double exp(doublereal);
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static integer n;
    static real y, x1[51], x2[51];
    static integer iws;
    extern real rngu0_(integer *);
    static integer iseed;
    extern /* Subroutine */ int grcls_(void), grfrm_(void), gropn_(integer *),
	     uulin_(integer *, real *, real *), uumrk_(integer *, real *, 
	    real *);
    static real rundef;
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen), grswnd_(real 
	    *, real *, real *, real *), grstrf_(void), usdaxs_(void), uspfit_(
	    void), grstrn_(integer *), sgpwsn_(void), uusmkt_(integer *), 
	    usspnt_(integer *, real *, real *), ussttl_(char *, char *, char *
	    , char *, ftnlen, ftnlen, ftnlen, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___7 = { 0, 6, 0, 0, 0 };
    static cilist io___8 = { 0, 5, 0, 0, 0 };


/* -- データ ---- */
    iseed = 1;
    for (n = 0; n <= 50; ++n) {
	y = n * 50.f / 50 + 0.f;
/* Computing 2nd power */
	r__1 = exp(-y / 20);
/* Computing 2nd power */
	r__2 = exp((rngu0_(&iseed) - .5f) * 2);
	x1[n] = r__1 * r__1 * 10.f * (r__2 * r__2);
/* Computing 2nd power */
	r__1 = exp(-y / 20);
	x2[n] = r__1 * r__1 * 10.f;
/* L10: */
    }
    glrget_("RUNDEF", &rundef, (ftnlen)6);
/* -- グラフ ---- */
    s_wsle(&io___7);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___8);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    gropn_(&iws);
    grfrm_();
    grswnd_(&rundef, &rundef, &c_b8, &c_b9);
    usspnt_(&c__51, x1, &rundef);
    usspnt_(&c__51, x2, &rundef);
    grstrn_(&c__3);
    uspfit_();
    grstrf_();
    ussttl_("MIXING RATIO", "ppmv", "HEIGHT", "km", (ftnlen)12, (ftnlen)4, (
	    ftnlen)6, (ftnlen)2);
    usdaxs_();
    uusmkt_(&c__2);
    uumrk_(&c__51, x1, &rundef);
    uulin_(&c__51, x2, &rundef);
    grcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int u1d2_ () { MAIN__ (); return 0; }

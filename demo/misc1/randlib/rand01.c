/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__4 = 4;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* System generated locals */
    real r__1;

    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void);

    /* Local variables */
    static integer i__;
    static real x, y;
    static integer nn;
    extern real rngu0_(integer *);
    static integer iseed;

    /* Fortran I/O blocks */
    static cilist io___6 = { 0, 6, 0, 0, 0 };


    iseed = 1;
    x = rngu0_(&iseed);
    nn = 0;
    for (i__ = 1; i__ <= 10000; ++i__) {
	y = x;
	x = rngu0_(&iseed);
	if (x * x + y * y <= 1.f) {
	    ++nn;
	}
/* L10: */
    }
    s_wsle(&io___6);
    do_lio(&c__9, &c__1, "PI = ", (ftnlen)5);
    r__1 = nn * 4.f / 10000;
    do_lio(&c__4, &c__1, (char *)&r__1, (ftnlen)sizeof(real));
    e_wsle();
    return 0;
} /* MAIN__ */

/* Main program alias */ int rand01_ () { MAIN__ (); return 0; }

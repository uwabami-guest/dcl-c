/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void);

    /* Local variables */
    static char ca[8];
    static integer ii;
    extern integer lenc_(char *, ftnlen);
    static char cfmt[16];
    extern /* Subroutine */ int chngi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen);
    static char ctext[80];

    /* Fortran I/O blocks */
    static cilist io___5 = { 0, 6, 0, 0, 0 };
    static cilist io___6 = { 0, 6, 0, 0, 0 };
    static cilist io___7 = { 0, 6, 0, 0, 0 };
    static cilist io___8 = { 0, 6, 0, 0, 0 };
    static cilist io___9 = { 0, 6, 0, 0, 0 };
    static cilist io___10 = { 0, 6, 0, 0, 0 };


    s_copy(ctext, "ERROR NUMBER = ###.", (ftnlen)80, (ftnlen)19);
    s_copy(ca, "###", (ftnlen)8, (ftnlen)3);
    ii = 12;
    s_copy(cfmt, "(I3.3)", (ftnlen)16, (ftnlen)6);
    s_wsle(&io___5);
    do_lio(&c__9, &c__1, "CTEXT = ", (ftnlen)8);
    do_lio(&c__9, &c__1, ctext, lenc_(ctext, (ftnlen)80));
    e_wsle();
    s_wsle(&io___6);
    do_lio(&c__9, &c__1, "CA = ", (ftnlen)5);
    do_lio(&c__9, &c__1, ca, (ftnlen)8);
    e_wsle();
    s_wsle(&io___7);
    do_lio(&c__9, &c__1, "II = ", (ftnlen)5);
    do_lio(&c__3, &c__1, (char *)&ii, (ftnlen)sizeof(integer));
    e_wsle();
    s_wsle(&io___8);
    do_lio(&c__9, &c__1, "CFMT = ", (ftnlen)7);
    do_lio(&c__9, &c__1, cfmt, (ftnlen)16);
    e_wsle();
    s_wsle(&io___9);
    do_lio(&c__9, &c__1, "AFTER CALLING CHNGI(CTEXT,CA,II,CFMT)", (ftnlen)37);
    e_wsle();
    chngi_(ctext, ca, &ii, cfmt, lenc_(ctext, (ftnlen)80), lenc_(ca, (ftnlen)
	    8), (ftnlen)16);
    s_wsle(&io___10);
    do_lio(&c__9, &c__1, "CTEXT = ", (ftnlen)8);
    do_lio(&c__9, &c__1, ctext, lenc_(ctext, (ftnlen)80));
    e_wsle();
    return 0;
} /* MAIN__ */

/* Main program alias */ int chnl02_ () { MAIN__ (); return 0; }

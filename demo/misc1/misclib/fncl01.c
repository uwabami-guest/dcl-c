/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;
static integer c_n1 = -1;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* System generated locals */
    char ch__1[1], ch__2[1];

    /* Builtin functions */
    integer s_wsfe(cilist *), do_fio(integer *, char *, ftnlen), e_wsfe(void);

    /* Local variables */
    extern /* Character */ VOID cns_(char *, ftnlen, integer *);
    static char cver[12];
    static integer iunit;
    extern /* Subroutine */ int dclvnm_(char *, ftnlen);
    extern integer iufopn_(void);

    /* Fortran I/O blocks */
    static cilist io___1 = { 0, 6, 0, "(A)", 0 };
    static cilist io___3 = { 0, 6, 0, "(2A)", 0 };
    static cilist io___4 = { 0, 6, 0, "(A)", 0 };
    static cilist io___6 = { 0, 6, 0, "(A,I2)", 0 };
    static cilist io___7 = { 0, 6, 0, "(A)", 0 };
    static cilist io___8 = { 0, 6, 0, "(2A)", 0 };
    static cilist io___9 = { 0, 6, 0, "(2A)", 0 };


    s_wsfe(&io___1);
    do_fio(&c__1, " *** TEST FOR DCLVNM", (ftnlen)20);
    e_wsfe();
    dclvnm_(cver, (ftnlen)12);
    s_wsfe(&io___3);
    do_fio(&c__1, " VERSION NAME = ", (ftnlen)16);
    do_fio(&c__1, cver, (ftnlen)12);
    e_wsfe();
    s_wsfe(&io___4);
    do_fio(&c__1, " *** TEST FOR IUFOPN", (ftnlen)20);
    e_wsfe();
    iunit = iufopn_();
    s_wsfe(&io___6);
    do_fio(&c__1, " AVAILABLE UNIT NUMBER = ", (ftnlen)25);
    do_fio(&c__1, (char *)&iunit, (ftnlen)sizeof(integer));
    e_wsfe();
    s_wsfe(&io___7);
    do_fio(&c__1, " *** TEST FOR CNS", (ftnlen)17);
    e_wsfe();
    s_wsfe(&io___8);
    do_fio(&c__1, " CNS(+1) = ", (ftnlen)11);
    cns_(ch__2, (ftnlen)1, &c__1);
    *(unsigned char *)&ch__1[0] = *(unsigned char *)&ch__2[0];
    do_fio(&c__1, ch__1, (ftnlen)1);
    e_wsfe();
    s_wsfe(&io___9);
    do_fio(&c__1, " CNS(-1) = ", (ftnlen)11);
    cns_(ch__2, (ftnlen)1, &c_n1);
    *(unsigned char *)&ch__1[0] = *(unsigned char *)&ch__2[0];
    do_fio(&c__1, ch__1, (ftnlen)1);
    e_wsfe();
    return 0;
} /* MAIN__ */

/* Main program alias */ int fncl01_ () { MAIN__ (); return 0; }

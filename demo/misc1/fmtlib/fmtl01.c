/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__4 = 4;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Initialized data */

    static real val[3] = { 0.f,-.012f,1.23e6f };

    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void);

    /* Local variables */
    static integer i__;
    static char cval[8];
    extern /* Subroutine */ int chval_(char *, real *, char *, ftnlen, ftnlen)
	    ;

    /* Fortran I/O blocks */
    static cilist io___3 = { 0, 6, 0, 0, 0 };
    static cilist io___5 = { 0, 6, 0, 0, 0 };
    static cilist io___6 = { 0, 6, 0, 0, 0 };
    static cilist io___7 = { 0, 6, 0, 0, 0 };
    static cilist io___8 = { 0, 6, 0, 0, 0 };


    for (i__ = 1; i__ <= 3; ++i__) {
	s_wsle(&io___3);
	do_lio(&c__9, &c__1, "VAL = ", (ftnlen)6);
	do_lio(&c__4, &c__1, (char *)&val[i__ - 1], (ftnlen)sizeof(real));
	e_wsle();
	chval_("A", &val[i__ - 1], cval, (ftnlen)1, (ftnlen)8);
	s_wsle(&io___5);
	do_lio(&c__9, &c__1, "OPTION=A : VAL = ", (ftnlen)17);
	do_lio(&c__9, &c__1, cval, (ftnlen)8);
	e_wsle();
	chval_("B", &val[i__ - 1], cval, (ftnlen)1, (ftnlen)8);
	s_wsle(&io___6);
	do_lio(&c__9, &c__1, "OPTION=B : VAL = ", (ftnlen)17);
	do_lio(&c__9, &c__1, cval, (ftnlen)8);
	e_wsle();
	chval_("C", &val[i__ - 1], cval, (ftnlen)1, (ftnlen)8);
	s_wsle(&io___7);
	do_lio(&c__9, &c__1, "OPTION=C : VAL = ", (ftnlen)17);
	do_lio(&c__9, &c__1, cval, (ftnlen)8);
	e_wsle();
	chval_("D", &val[i__ - 1], cval, (ftnlen)1, (ftnlen)8);
	s_wsle(&io___8);
	do_lio(&c__9, &c__1, "OPTION=D : VAL = ", (ftnlen)17);
	do_lio(&c__9, &c__1, cval, (ftnlen)8);
	e_wsle();
/* L10: */
    }
    return 0;
} /* MAIN__ */

/* Main program alias */ int fmtl01_ () { MAIN__ (); return 0; }

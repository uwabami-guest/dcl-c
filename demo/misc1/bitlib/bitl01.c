/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Initialized data */

    static integer ip = 255;

    /* Builtin functions */
    integer s_wsfe(cilist *), do_fio(integer *, char *, ftnlen), e_wsfe(void);

    /* Local variables */
    static char cp[32];
    static integer jp;
    extern /* Subroutine */ int bitpic_(integer *, char *, ftnlen), bitpci_(
	    char *, integer *, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___4 = { 0, 6, 0, "(A,I8)", 0 };
    static cilist io___5 = { 0, 6, 0, "(A)", 0 };
    static cilist io___6 = { 0, 6, 0, "(A,A)", 0 };
    static cilist io___7 = { 0, 6, 0, "(A)", 0 };
    static cilist io___8 = { 0, 6, 0, "(A,I8)", 0 };


    bitpic_(&ip, cp, (ftnlen)32);
    bitpci_(cp, &jp, (ftnlen)32);
    s_wsfe(&io___4);
    do_fio(&c__1, " INTEGER CONSTANT IP = ", (ftnlen)23);
    do_fio(&c__1, (char *)&ip, (ftnlen)sizeof(integer));
    e_wsfe();
    s_wsfe(&io___5);
    do_fio(&c__1, " AFTER CALLING BITPIC(IP,CP)", (ftnlen)28);
    e_wsfe();
    s_wsfe(&io___6);
    do_fio(&c__1, " BIT PATTERN CP = ", (ftnlen)18);
    do_fio(&c__1, cp, (ftnlen)32);
    e_wsfe();
    s_wsfe(&io___7);
    do_fio(&c__1, " AFTER CALLING BITPCI(CP,JP)", (ftnlen)28);
    e_wsfe();
    s_wsfe(&io___8);
    do_fio(&c__1, " INTEGER CONSTANT JP = ", (ftnlen)23);
    do_fio(&c__1, (char *)&jp, (ftnlen)sizeof(integer));
    e_wsfe();
    return 0;
} /* MAIN__ */

/* Main program alias */ int bitl01_ () { MAIN__ (); return 0; }

/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer ih, im, is, itt;
    static char cform[24];
    static integer itime;
    extern /* Subroutine */ int timec1_(char *, integer *, ftnlen), timec2_(
	    char *, integer *, ftnlen), timec3_(char *, integer *, integer *, 
	    integer *, ftnlen), timeq1_(integer *), timeq2_(integer *), 
	    timeq3_(integer *, integer *, integer *);

    /* Fortran I/O blocks */
    static cilist io___1 = { 0, 6, 0, 0, 0 };
    static cilist io___2 = { 0, 6, 0, 0, 0 };
    static cilist io___4 = { 0, 6, 0, 0, 0 };
    static cilist io___6 = { 0, 6, 0, 0, 0 };
    static cilist io___7 = { 0, 6, 0, 0, 0 };
    static cilist io___8 = { 0, 6, 0, 0, 0 };
    static cilist io___9 = { 0, 6, 0, 0, 0 };
    static cilist io___10 = { 0, 6, 0, 0, 0 };
    static cilist io___12 = { 0, 6, 0, 0, 0 };
    static cilist io___13 = { 0, 6, 0, 0, 0 };
    static cilist io___14 = { 0, 6, 0, 0, 0 };
    static cilist io___15 = { 0, 6, 0, 0, 0 };
    static cilist io___16 = { 0, 6, 0, 0, 0 };
    static cilist io___20 = { 0, 6, 0, 0, 0 };
    static cilist io___21 = { 0, 6, 0, 0, 0 };
    static cilist io___22 = { 0, 6, 0, 0, 0 };


    s_wsle(&io___1);
    do_lio(&c__9, &c__1, "*** TEST FOR CHARACTER TIME & TIME INQUIRY", (
	    ftnlen)42);
    e_wsle();
    s_wsle(&io___2);
    do_lio(&c__9, &c__1, "*** TYPE-1", (ftnlen)10);
    e_wsle();
    s_copy(cform, "HH,MM,SS", (ftnlen)24, (ftnlen)8);
    s_wsle(&io___4);
    do_lio(&c__9, &c__1, "CFORM = ", (ftnlen)8);
    do_lio(&c__9, &c__1, cform, (ftnlen)24);
    e_wsle();
    timeq1_(&itime);
    timec1_(cform, &itime, (ftnlen)24);
    s_wsle(&io___6);
    do_lio(&c__9, &c__1, "AFTER CALLING TIMEQ1(ITIME)", (ftnlen)27);
    e_wsle();
    s_wsle(&io___7);
    do_lio(&c__9, &c__1, "    AND       TIMEC1(CFORM,ITIME)", (ftnlen)33);
    e_wsle();
    s_wsle(&io___8);
    do_lio(&c__9, &c__1, "CFORM = ", (ftnlen)8);
    do_lio(&c__9, &c__1, cform, (ftnlen)24);
    e_wsle();
    s_wsle(&io___9);
    do_lio(&c__9, &c__1, "*** TYPE-2", (ftnlen)10);
    e_wsle();
    s_copy(cform, "HHhMMmSSs", (ftnlen)24, (ftnlen)9);
    s_wsle(&io___10);
    do_lio(&c__9, &c__1, "CFORM = ", (ftnlen)8);
    do_lio(&c__9, &c__1, cform, (ftnlen)24);
    e_wsle();
    timeq2_(&itt);
    timec2_(cform, &itt, (ftnlen)24);
    s_wsle(&io___12);
    do_lio(&c__9, &c__1, "AFTER CALLING TIMEQ2(ITT)", (ftnlen)25);
    e_wsle();
    s_wsle(&io___13);
    do_lio(&c__9, &c__1, "    AND       TIMEC2(CFORM,ITT)", (ftnlen)31);
    e_wsle();
    s_wsle(&io___14);
    do_lio(&c__9, &c__1, "CFORM = ", (ftnlen)8);
    do_lio(&c__9, &c__1, cform, (ftnlen)24);
    e_wsle();
    s_wsle(&io___15);
    do_lio(&c__9, &c__1, "*** TYPE-3", (ftnlen)10);
    e_wsle();
    s_copy(cform, "HH:MM:SS", (ftnlen)24, (ftnlen)8);
    s_wsle(&io___16);
    do_lio(&c__9, &c__1, "CFORM = ", (ftnlen)8);
    do_lio(&c__9, &c__1, cform, (ftnlen)24);
    e_wsle();
    timeq3_(&ih, &im, &is);
    timec3_(cform, &ih, &im, &is, (ftnlen)24);
    s_wsle(&io___20);
    do_lio(&c__9, &c__1, "AFTER CALLING TIMEQ3(IH,IM,IS)", (ftnlen)30);
    e_wsle();
    s_wsle(&io___21);
    do_lio(&c__9, &c__1, "    AND       TIMEC3(CFORM,IH,IM,IS)", (ftnlen)36);
    e_wsle();
    s_wsle(&io___22);
    do_lio(&c__9, &c__1, "CFORM = ", (ftnlen)8);
    do_lio(&c__9, &c__1, cform, (ftnlen)24);
    e_wsle();
    return 0;
} /* MAIN__ */

/* Main program alias */ int time02_ () { MAIN__ (); return 0; }

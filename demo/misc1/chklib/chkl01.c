/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Format strings */
    static char fmt_100[] = "(a)";
    static char fmt_200[] = "(4a,l4)";
    static char fmt_300[] = "(3a)";
    static char fmt_400[] = "(a,l4)";

    /* System generated locals */
    logical L__1;

    /* Builtin functions */
    integer s_wsfe(cilist *), do_fio(integer *, char *, ftnlen), e_wsfe(void);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static char ch[4], char__[8], cref[8];
    extern logical lchr_(char *, char *, ftnlen, ftnlen), lchra_(char *, 
	    ftnlen), lchrb_(char *, ftnlen), lchrc_(char *, ftnlen), lchrd_(
	    char *, ftnlen), lchrf_(char *, ftnlen), lchrl_(char *, ftnlen), 
	    lchrs_(char *, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___1 = { 0, 6, 0, fmt_100, 0 };
    static cilist io___3 = { 0, 6, 0, fmt_200, 0 };
    static cilist io___4 = { 0, 6, 0, fmt_200, 0 };
    static cilist io___5 = { 0, 6, 0, fmt_100, 0 };
    static cilist io___6 = { 0, 6, 0, fmt_200, 0 };
    static cilist io___7 = { 0, 6, 0, fmt_200, 0 };
    static cilist io___8 = { 0, 6, 0, fmt_100, 0 };
    static cilist io___9 = { 0, 6, 0, fmt_200, 0 };
    static cilist io___10 = { 0, 6, 0, fmt_200, 0 };
    static cilist io___11 = { 0, 6, 0, fmt_100, 0 };
    static cilist io___12 = { 0, 6, 0, fmt_200, 0 };
    static cilist io___13 = { 0, 6, 0, fmt_200, 0 };
    static cilist io___14 = { 0, 6, 0, fmt_100, 0 };
    static cilist io___15 = { 0, 6, 0, fmt_200, 0 };
    static cilist io___16 = { 0, 6, 0, fmt_200, 0 };
    static cilist io___17 = { 0, 6, 0, fmt_100, 0 };
    static cilist io___18 = { 0, 6, 0, fmt_200, 0 };
    static cilist io___19 = { 0, 6, 0, fmt_200, 0 };
    static cilist io___20 = { 0, 6, 0, fmt_100, 0 };
    static cilist io___21 = { 0, 6, 0, fmt_200, 0 };
    static cilist io___22 = { 0, 6, 0, fmt_200, 0 };
    static cilist io___23 = { 0, 6, 0, fmt_100, 0 };
    static cilist io___26 = { 0, 6, 0, fmt_300, 0 };
    static cilist io___27 = { 0, 6, 0, fmt_300, 0 };
    static cilist io___28 = { 0, 6, 0, fmt_400, 0 };
    static cilist io___29 = { 0, 6, 0, fmt_300, 0 };
    static cilist io___30 = { 0, 6, 0, fmt_300, 0 };
    static cilist io___31 = { 0, 6, 0, fmt_400, 0 };


    s_wsfe(&io___1);
    do_fio(&c__1, " *** TEST FOR LCHRB", (ftnlen)19);
    e_wsfe();
    s_copy(ch, "    ", (ftnlen)4, (ftnlen)4);
    s_wsfe(&io___3);
    do_fio(&c__1, " CH = '", (ftnlen)7);
    do_fio(&c__1, ch, (ftnlen)4);
    do_fio(&c__1, "'", (ftnlen)1);
    do_fio(&c__1, " : LCHRB = ", (ftnlen)11);
    L__1 = lchrb_(ch, (ftnlen)4);
    do_fio(&c__1, (char *)&L__1, (ftnlen)sizeof(logical));
    e_wsfe();
    s_copy(ch, "1  A", (ftnlen)4, (ftnlen)4);
    s_wsfe(&io___4);
    do_fio(&c__1, " CH = '", (ftnlen)7);
    do_fio(&c__1, ch, (ftnlen)4);
    do_fio(&c__1, "'", (ftnlen)1);
    do_fio(&c__1, " : LCHRB = ", (ftnlen)11);
    L__1 = lchrb_(ch, (ftnlen)4);
    do_fio(&c__1, (char *)&L__1, (ftnlen)sizeof(logical));
    e_wsfe();
    s_wsfe(&io___5);
    do_fio(&c__1, " *** TEST FOR LCHRC", (ftnlen)19);
    e_wsfe();
    s_copy(ch, "$$$$", (ftnlen)4, (ftnlen)4);
    s_wsfe(&io___6);
    do_fio(&c__1, " CH = '", (ftnlen)7);
    do_fio(&c__1, ch, (ftnlen)4);
    do_fio(&c__1, "'", (ftnlen)1);
    do_fio(&c__1, " : LCHRC = ", (ftnlen)11);
    L__1 = lchrc_(ch, (ftnlen)4);
    do_fio(&c__1, (char *)&L__1, (ftnlen)sizeof(logical));
    e_wsfe();
    s_copy(ch, "$$$A", (ftnlen)4, (ftnlen)4);
    s_wsfe(&io___7);
    do_fio(&c__1, " CH = '", (ftnlen)7);
    do_fio(&c__1, ch, (ftnlen)4);
    do_fio(&c__1, "'", (ftnlen)1);
    do_fio(&c__1, " : LCHRC = ", (ftnlen)11);
    L__1 = lchrc_(ch, (ftnlen)4);
    do_fio(&c__1, (char *)&L__1, (ftnlen)sizeof(logical));
    e_wsfe();
    s_wsfe(&io___8);
    do_fio(&c__1, " *** TEST FOR LCHRS", (ftnlen)19);
    e_wsfe();
    s_copy(ch, "*.(/", (ftnlen)4, (ftnlen)4);
    s_wsfe(&io___9);
    do_fio(&c__1, " CH = '", (ftnlen)7);
    do_fio(&c__1, ch, (ftnlen)4);
    do_fio(&c__1, "'", (ftnlen)1);
    do_fio(&c__1, " : LCHRS = ", (ftnlen)11);
    L__1 = lchrs_(ch, (ftnlen)4);
    do_fio(&c__1, (char *)&L__1, (ftnlen)sizeof(logical));
    e_wsfe();
    s_copy(ch, "2.(/", (ftnlen)4, (ftnlen)4);
    s_wsfe(&io___10);
    do_fio(&c__1, " CH = '", (ftnlen)7);
    do_fio(&c__1, ch, (ftnlen)4);
    do_fio(&c__1, "'", (ftnlen)1);
    do_fio(&c__1, " : LCHRS = ", (ftnlen)11);
    L__1 = lchrs_(ch, (ftnlen)4);
    do_fio(&c__1, (char *)&L__1, (ftnlen)sizeof(logical));
    e_wsfe();
    s_wsfe(&io___11);
    do_fio(&c__1, " *** TEST FOR LCHRL", (ftnlen)19);
    e_wsfe();
    s_copy(ch, "ABCD", (ftnlen)4, (ftnlen)4);
    s_wsfe(&io___12);
    do_fio(&c__1, " CH = '", (ftnlen)7);
    do_fio(&c__1, ch, (ftnlen)4);
    do_fio(&c__1, "'", (ftnlen)1);
    do_fio(&c__1, " : LCHRL = ", (ftnlen)11);
    L__1 = lchrl_(ch, (ftnlen)4);
    do_fio(&c__1, (char *)&L__1, (ftnlen)sizeof(logical));
    e_wsfe();
    s_copy(ch, "AB+D", (ftnlen)4, (ftnlen)4);
    s_wsfe(&io___13);
    do_fio(&c__1, " CH = '", (ftnlen)7);
    do_fio(&c__1, ch, (ftnlen)4);
    do_fio(&c__1, "'", (ftnlen)1);
    do_fio(&c__1, " : LCHRL = ", (ftnlen)11);
    L__1 = lchrl_(ch, (ftnlen)4);
    do_fio(&c__1, (char *)&L__1, (ftnlen)sizeof(logical));
    e_wsfe();
    s_wsfe(&io___14);
    do_fio(&c__1, " *** TEST FOR LCHRD", (ftnlen)19);
    e_wsfe();
    s_copy(ch, "9876", (ftnlen)4, (ftnlen)4);
    s_wsfe(&io___15);
    do_fio(&c__1, " CH = '", (ftnlen)7);
    do_fio(&c__1, ch, (ftnlen)4);
    do_fio(&c__1, "'", (ftnlen)1);
    do_fio(&c__1, " : LCHRD = ", (ftnlen)11);
    L__1 = lchrd_(ch, (ftnlen)4);
    do_fio(&c__1, (char *)&L__1, (ftnlen)sizeof(logical));
    e_wsfe();
    s_copy(ch, "987A", (ftnlen)4, (ftnlen)4);
    s_wsfe(&io___16);
    do_fio(&c__1, " CH = '", (ftnlen)7);
    do_fio(&c__1, ch, (ftnlen)4);
    do_fio(&c__1, "'", (ftnlen)1);
    do_fio(&c__1, " : LCHRD = ", (ftnlen)11);
    L__1 = lchrd_(ch, (ftnlen)4);
    do_fio(&c__1, (char *)&L__1, (ftnlen)sizeof(logical));
    e_wsfe();
    s_wsfe(&io___17);
    do_fio(&c__1, " *** TEST FOR LCHRA", (ftnlen)19);
    e_wsfe();
    s_copy(ch, "AS23", (ftnlen)4, (ftnlen)4);
    s_wsfe(&io___18);
    do_fio(&c__1, " CH = '", (ftnlen)7);
    do_fio(&c__1, ch, (ftnlen)4);
    do_fio(&c__1, "'", (ftnlen)1);
    do_fio(&c__1, " : LCHRA = ", (ftnlen)11);
    L__1 = lchra_(ch, (ftnlen)4);
    do_fio(&c__1, (char *)&L__1, (ftnlen)sizeof(logical));
    e_wsfe();
    s_copy(ch, "A 23", (ftnlen)4, (ftnlen)4);
    s_wsfe(&io___19);
    do_fio(&c__1, " CH = '", (ftnlen)7);
    do_fio(&c__1, ch, (ftnlen)4);
    do_fio(&c__1, "'", (ftnlen)1);
    do_fio(&c__1, " : LCHRA = ", (ftnlen)11);
    L__1 = lchra_(ch, (ftnlen)4);
    do_fio(&c__1, (char *)&L__1, (ftnlen)sizeof(logical));
    e_wsfe();
    s_wsfe(&io___20);
    do_fio(&c__1, " *** TEST FOR LCHRF", (ftnlen)19);
    e_wsfe();
    s_copy(ch, "A*2=", (ftnlen)4, (ftnlen)4);
    s_wsfe(&io___21);
    do_fio(&c__1, " CH = '", (ftnlen)7);
    do_fio(&c__1, ch, (ftnlen)4);
    do_fio(&c__1, "'", (ftnlen)1);
    do_fio(&c__1, " : LCHRF = ", (ftnlen)11);
    L__1 = lchrf_(ch, (ftnlen)4);
    do_fio(&c__1, (char *)&L__1, (ftnlen)sizeof(logical));
    e_wsfe();
    s_copy(ch, "A#2=", (ftnlen)4, (ftnlen)4);
    s_wsfe(&io___22);
    do_fio(&c__1, " CH = '", (ftnlen)7);
    do_fio(&c__1, ch, (ftnlen)4);
    do_fio(&c__1, "'", (ftnlen)1);
    do_fio(&c__1, " : LCHRF = ", (ftnlen)11);
    L__1 = lchrf_(ch, (ftnlen)4);
    do_fio(&c__1, (char *)&L__1, (ftnlen)sizeof(logical));
    e_wsfe();
    s_wsfe(&io___23);
    do_fio(&c__1, " *** TEST FOR LCHR", (ftnlen)18);
    e_wsfe();
    s_copy(char__, "A=B*2+12", (ftnlen)8, (ftnlen)8);
    s_copy(cref, "LSLSDSAF", (ftnlen)8, (ftnlen)8);
    s_wsfe(&io___26);
    do_fio(&c__1, " CHAR = '", (ftnlen)9);
    do_fio(&c__1, char__, (ftnlen)8);
    do_fio(&c__1, "'", (ftnlen)1);
    e_wsfe();
    s_wsfe(&io___27);
    do_fio(&c__1, " CREF = '", (ftnlen)9);
    do_fio(&c__1, cref, (ftnlen)8);
    do_fio(&c__1, "'", (ftnlen)1);
    e_wsfe();
    s_wsfe(&io___28);
    do_fio(&c__1, " LCHR = ", (ftnlen)8);
    L__1 = lchr_(char__, cref, (ftnlen)8, (ftnlen)8);
    do_fio(&c__1, (char *)&L__1, (ftnlen)sizeof(logical));
    e_wsfe();
    s_copy(char__, "A=B*2+12", (ftnlen)8, (ftnlen)8);
    s_copy(cref, "LSDSDSAF", (ftnlen)8, (ftnlen)8);
    s_wsfe(&io___29);
    do_fio(&c__1, " CHAR = '", (ftnlen)9);
    do_fio(&c__1, char__, (ftnlen)8);
    do_fio(&c__1, "'", (ftnlen)1);
    e_wsfe();
    s_wsfe(&io___30);
    do_fio(&c__1, " CREF = '", (ftnlen)9);
    do_fio(&c__1, cref, (ftnlen)8);
    do_fio(&c__1, "'", (ftnlen)1);
    e_wsfe();
    s_wsfe(&io___31);
    do_fio(&c__1, " LCHR = ", (ftnlen)8);
    L__1 = lchr_(char__, cref, (ftnlen)8, (ftnlen)8);
    do_fio(&c__1, (char *)&L__1, (ftnlen)sizeof(logical));
    e_wsfe();
    return 0;
} /* MAIN__ */

/* Main program alias */ int chkl01_ () { MAIN__ (); return 0; }

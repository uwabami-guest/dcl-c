/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* System generated locals */
    integer i__1;
    char ch__1[9], ch__2[9];

    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer id, im, iy, itd;
    extern /* Subroutine */ int date31_(integer *, integer *, integer *, 
	    integer *), date32_(integer *, integer *, integer *, integer *);
    static integer idate;
    extern /* Character */ VOID cweek_(char *, ftnlen, integer *);
    extern /* Subroutine */ int dateq3_(integer *, integer *, integer *);
    extern integer iweek1_(integer *), iweek2_(integer *, integer *), iweek3_(
	    integer *, integer *, integer *);

    /* Fortran I/O blocks */
    static cilist io___6 = { 0, 6, 0, 0, 0 };
    static cilist io___7 = { 0, 6, 0, 0, 0 };
    static cilist io___8 = { 0, 6, 0, 0, 0 };
    static cilist io___9 = { 0, 6, 0, 0, 0 };
    static cilist io___10 = { 0, 6, 0, 0, 0 };
    static cilist io___11 = { 0, 6, 0, 0, 0 };
    static cilist io___12 = { 0, 6, 0, 0, 0 };
    static cilist io___13 = { 0, 6, 0, 0, 0 };
    static cilist io___14 = { 0, 6, 0, 0, 0 };


    dateq3_(&iy, &im, &id);
    date31_(&idate, &iy, &im, &id);
    date32_(&iy, &im, &id, &itd);
    s_wsle(&io___6);
    do_lio(&c__9, &c__1, "*** TYPE-1", (ftnlen)10);
    e_wsle();
    s_wsle(&io___7);
    do_lio(&c__9, &c__1, "DATE = ", (ftnlen)7);
    do_lio(&c__3, &c__1, (char *)&idate, (ftnlen)sizeof(integer));
    do_lio(&c__9, &c__1, " ;", (ftnlen)2);
    e_wsle();
    s_wsle(&io___8);
    do_lio(&c__9, &c__1, "WEEK = ", (ftnlen)7);
    i__1 = iweek1_(&idate);
    cweek_(ch__2, (ftnlen)9, &i__1);
    s_copy(ch__1, ch__2, (ftnlen)9, (ftnlen)9);
    do_lio(&c__9, &c__1, ch__1, (ftnlen)9);
    e_wsle();
    s_wsle(&io___9);
    do_lio(&c__9, &c__1, "*** TYPE-2", (ftnlen)10);
    e_wsle();
    s_wsle(&io___10);
    do_lio(&c__9, &c__1, "YEAR = ", (ftnlen)7);
    do_lio(&c__3, &c__1, (char *)&iy, (ftnlen)sizeof(integer));
    do_lio(&c__9, &c__1, " ; TOTAL DAYS = ", (ftnlen)16);
    do_lio(&c__3, &c__1, (char *)&itd, (ftnlen)sizeof(integer));
    do_lio(&c__9, &c__1, " ;", (ftnlen)2);
    e_wsle();
    s_wsle(&io___11);
    do_lio(&c__9, &c__1, "WEEK = ", (ftnlen)7);
    i__1 = iweek2_(&iy, &itd);
    cweek_(ch__2, (ftnlen)9, &i__1);
    s_copy(ch__1, ch__2, (ftnlen)9, (ftnlen)9);
    do_lio(&c__9, &c__1, ch__1, (ftnlen)9);
    e_wsle();
    s_wsle(&io___12);
    do_lio(&c__9, &c__1, "*** TYPE-3", (ftnlen)10);
    e_wsle();
    s_wsle(&io___13);
    do_lio(&c__9, &c__1, "YEAR = ", (ftnlen)7);
    do_lio(&c__3, &c__1, (char *)&iy, (ftnlen)sizeof(integer));
    do_lio(&c__9, &c__1, " ; MONTH = ", (ftnlen)11);
    do_lio(&c__3, &c__1, (char *)&im, (ftnlen)sizeof(integer));
    do_lio(&c__9, &c__1, " ; DAY = ", (ftnlen)9);
    do_lio(&c__3, &c__1, (char *)&id, (ftnlen)sizeof(integer));
    do_lio(&c__9, &c__1, " ;", (ftnlen)2);
    e_wsle();
    s_wsle(&io___14);
    do_lio(&c__9, &c__1, "WEEK = ", (ftnlen)7);
    i__1 = iweek3_(&iy, &im, &id);
    cweek_(ch__2, (ftnlen)9, &i__1);
    s_copy(ch__1, ch__2, (ftnlen)9, (ftnlen)9);
    do_lio(&c__9, &c__1, ch__1, (ftnlen)9);
    e_wsle();
    return 0;
} /* MAIN__ */

/* Main program alias */ int date03_ () { MAIN__ (); return 0; }

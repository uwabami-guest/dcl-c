/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer id, im, iy, itd, idate;
    static char cform[24];
    extern /* Subroutine */ int datec1_(char *, integer *, ftnlen), datec2_(
	    char *, integer *, integer *, ftnlen), datec3_(char *, integer *, 
	    integer *, integer *, ftnlen), dateq1_(integer *), dateq2_(
	    integer *, integer *), dateq3_(integer *, integer *, integer *);

    /* Fortran I/O blocks */
    static cilist io___1 = { 0, 6, 0, 0, 0 };
    static cilist io___2 = { 0, 6, 0, 0, 0 };
    static cilist io___4 = { 0, 6, 0, 0, 0 };
    static cilist io___6 = { 0, 6, 0, 0, 0 };
    static cilist io___7 = { 0, 6, 0, 0, 0 };
    static cilist io___8 = { 0, 6, 0, 0, 0 };
    static cilist io___9 = { 0, 6, 0, 0, 0 };
    static cilist io___10 = { 0, 6, 0, 0, 0 };
    static cilist io___13 = { 0, 6, 0, 0, 0 };
    static cilist io___14 = { 0, 6, 0, 0, 0 };
    static cilist io___15 = { 0, 6, 0, 0, 0 };
    static cilist io___16 = { 0, 6, 0, 0, 0 };
    static cilist io___17 = { 0, 6, 0, 0, 0 };
    static cilist io___20 = { 0, 6, 0, 0, 0 };
    static cilist io___21 = { 0, 6, 0, 0, 0 };
    static cilist io___22 = { 0, 6, 0, 0, 0 };


    s_wsle(&io___1);
    do_lio(&c__9, &c__1, "*** TEST FOR CHARACTER DATE & DATE INQUIRY", (
	    ftnlen)42);
    e_wsle();
    s_wsle(&io___2);
    do_lio(&c__9, &c__1, "*** TYPE-1", (ftnlen)10);
    e_wsle();
    s_copy(cform, "YYYY,MM,DD", (ftnlen)24, (ftnlen)10);
    s_wsle(&io___4);
    do_lio(&c__9, &c__1, "CFORM = ", (ftnlen)8);
    do_lio(&c__9, &c__1, cform, (ftnlen)24);
    e_wsle();
    dateq1_(&idate);
    datec1_(cform, &idate, (ftnlen)24);
    s_wsle(&io___6);
    do_lio(&c__9, &c__1, "AFTER CALLING DATEQ1(IDATE)", (ftnlen)27);
    e_wsle();
    s_wsle(&io___7);
    do_lio(&c__9, &c__1, "    AND       DATEC1(CFORM,IDATE)", (ftnlen)33);
    e_wsle();
    s_wsle(&io___8);
    do_lio(&c__9, &c__1, "CFORM = ", (ftnlen)8);
    do_lio(&c__9, &c__1, cform, (ftnlen)24);
    e_wsle();
    s_wsle(&io___9);
    do_lio(&c__9, &c__1, "*** TYPE-2", (ftnlen)10);
    e_wsle();
    s_copy(cform, "CCCCCCCCC DD,YYYY", (ftnlen)24, (ftnlen)17);
    s_wsle(&io___10);
    do_lio(&c__9, &c__1, "CFORM = ", (ftnlen)8);
    do_lio(&c__9, &c__1, cform, (ftnlen)24);
    e_wsle();
    dateq2_(&iy, &itd);
    datec2_(cform, &iy, &itd, (ftnlen)24);
    s_wsle(&io___13);
    do_lio(&c__9, &c__1, "AFTER CALLING DATEQ2(IY,ITD)", (ftnlen)28);
    e_wsle();
    s_wsle(&io___14);
    do_lio(&c__9, &c__1, "    AND       DATEC2(CFORM,IY,ITD)", (ftnlen)34);
    e_wsle();
    s_wsle(&io___15);
    do_lio(&c__9, &c__1, "CFORM = ", (ftnlen)8);
    do_lio(&c__9, &c__1, cform, (ftnlen)24);
    e_wsle();
    s_wsle(&io___16);
    do_lio(&c__9, &c__1, "*** TYPE-3", (ftnlen)10);
    e_wsle();
    s_copy(cform, "YY/MM/DD (WWW)", (ftnlen)24, (ftnlen)14);
    s_wsle(&io___17);
    do_lio(&c__9, &c__1, "CFORM = ", (ftnlen)8);
    do_lio(&c__9, &c__1, cform, (ftnlen)24);
    e_wsle();
    dateq3_(&iy, &im, &id);
    datec3_(cform, &iy, &im, &id, (ftnlen)24);
    s_wsle(&io___20);
    do_lio(&c__9, &c__1, "AFTER CALLING DATEQ3(IY,IM,ID)", (ftnlen)30);
    e_wsle();
    s_wsle(&io___21);
    do_lio(&c__9, &c__1, "    AND       DATEC3(CFORM,IY,IM,ID)", (ftnlen)36);
    e_wsle();
    s_wsle(&io___22);
    do_lio(&c__9, &c__1, "CFORM = ", (ftnlen)8);
    do_lio(&c__9, &c__1, cform, (ftnlen)24);
    e_wsle();
    return 0;
} /* MAIN__ */

/* Main program alias */ int date04_ () { MAIN__ (); return 0; }

/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void);

    /* Local variables */
    static integer id, im, iy, itd;
    extern /* Subroutine */ int date12_(integer *, integer *, integer *), 
	    date21_(integer *, integer *, integer *), date13_(integer *, 
	    integer *, integer *, integer *), date31_(integer *, integer *, 
	    integer *, integer *);
    static integer idate, jdate, kdate;

    /* Fortran I/O blocks */
    static cilist io___1 = { 0, 6, 0, 0, 0 };
    static cilist io___3 = { 0, 6, 0, 0, 0 };
    static cilist io___6 = { 0, 6, 0, 0, 0 };
    static cilist io___7 = { 0, 6, 0, 0, 0 };
    static cilist io___9 = { 0, 6, 0, 0, 0 };
    static cilist io___10 = { 0, 6, 0, 0, 0 };
    static cilist io___13 = { 0, 6, 0, 0, 0 };
    static cilist io___14 = { 0, 6, 0, 0, 0 };
    static cilist io___16 = { 0, 6, 0, 0, 0 };
    static cilist io___17 = { 0, 6, 0, 0, 0 };


    s_wsle(&io___1);
    do_lio(&c__9, &c__1, "*** TEST FOR DATE CONVERSION", (ftnlen)28);
    e_wsle();
    idate = 19890215;
    s_wsle(&io___3);
    do_lio(&c__9, &c__1, "IDATE = ", (ftnlen)8);
    do_lio(&c__3, &c__1, (char *)&idate, (ftnlen)sizeof(integer));
    e_wsle();
    date12_(&idate, &iy, &itd);
    s_wsle(&io___6);
    do_lio(&c__9, &c__1, "AFTER CALLING DATE12(IDATE,IY,ITD)", (ftnlen)34);
    e_wsle();
    s_wsle(&io___7);
    do_lio(&c__9, &c__1, "IY = ", (ftnlen)5);
    do_lio(&c__3, &c__1, (char *)&iy, (ftnlen)sizeof(integer));
    do_lio(&c__9, &c__1, " : ITD = ", (ftnlen)9);
    do_lio(&c__3, &c__1, (char *)&itd, (ftnlen)sizeof(integer));
    e_wsle();
    date21_(&jdate, &iy, &itd);
    s_wsle(&io___9);
    do_lio(&c__9, &c__1, "AFTER CALLING DATE21(JDATE,IY,ITD)", (ftnlen)34);
    e_wsle();
    s_wsle(&io___10);
    do_lio(&c__9, &c__1, "JDATE = ", (ftnlen)8);
    do_lio(&c__3, &c__1, (char *)&jdate, (ftnlen)sizeof(integer));
    e_wsle();
    date13_(&jdate, &iy, &im, &id);
    s_wsle(&io___13);
    do_lio(&c__9, &c__1, "AFTER CALLING DATE13(JDATE,IY,IM,ID)", (ftnlen)36);
    e_wsle();
    s_wsle(&io___14);
    do_lio(&c__9, &c__1, "IY = ", (ftnlen)5);
    do_lio(&c__3, &c__1, (char *)&iy, (ftnlen)sizeof(integer));
    do_lio(&c__9, &c__1, " : IM = ", (ftnlen)8);
    do_lio(&c__3, &c__1, (char *)&im, (ftnlen)sizeof(integer));
    do_lio(&c__9, &c__1, " : ID = ", (ftnlen)8);
    do_lio(&c__3, &c__1, (char *)&id, (ftnlen)sizeof(integer));
    e_wsle();
    date31_(&kdate, &iy, &im, &id);
    s_wsle(&io___16);
    do_lio(&c__9, &c__1, "AFTER CALLING DATE31(KDATE,IY,IM,ID)", (ftnlen)36);
    e_wsle();
    s_wsle(&io___17);
    do_lio(&c__9, &c__1, "KDATE = ", (ftnlen)8);
    do_lio(&c__3, &c__1, (char *)&kdate, (ftnlen)sizeof(integer));
    e_wsle();
    return 0;
} /* MAIN__ */

/* Main program alias */ int date01_ () { MAIN__ (); return 0; }

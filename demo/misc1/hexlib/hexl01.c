/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void);

    /* Local variables */
    static integer lh;
    extern integer lenc_(char *, ftnlen);
    static char chex[8];
    static integer ihex;
    extern /* Subroutine */ int hexdci_(char *, integer *, ftnlen), hexdic_(
	    integer *, char *, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___4 = { 0, 6, 0, 0, 0 };
    static cilist io___5 = { 0, 6, 0, 0, 0 };
    static cilist io___6 = { 0, 6, 0, 0, 0 };


    s_copy(chex, "ABCD", (ftnlen)8, (ftnlen)4);
    lh = lenc_(chex, (ftnlen)8);
    hexdci_(chex, &ihex, lh);
    s_wsle(&io___4);
    do_lio(&c__9, &c__1, "CHEX = ", (ftnlen)7);
    do_lio(&c__9, &c__1, chex, lh);
    e_wsle();
    s_wsle(&io___5);
    do_lio(&c__9, &c__1, "IHEX = ", (ftnlen)7);
    do_lio(&c__3, &c__1, (char *)&ihex, (ftnlen)sizeof(integer));
    e_wsle();
    hexdic_(&ihex, chex, lh);
    s_wsle(&io___6);
    do_lio(&c__9, &c__1, "CHEX = ", (ftnlen)7);
    do_lio(&c__9, &c__1, chex, lh);
    e_wsle();
    return 0;
} /* MAIN__ */

/* Main program alias */ int hexl01_ () { MAIN__ (); return 0; }

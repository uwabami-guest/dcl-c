/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_wsfe(cilist *), do_fio(integer *, char *, ftnlen), e_wsfe(void);

    /* Local variables */
    static char ch[16];
    extern /* Subroutine */ int clower_(char *, ftnlen), cupper_(char *, 
	    ftnlen);

    /* Fortran I/O blocks */
    static cilist io___2 = { 0, 6, 0, "(A,A)", 0 };
    static cilist io___3 = { 0, 6, 0, "(A)", 0 };
    static cilist io___4 = { 0, 6, 0, "(A,A)", 0 };
    static cilist io___5 = { 0, 6, 0, "(A)", 0 };
    static cilist io___6 = { 0, 6, 0, "(A,A)", 0 };


    s_copy(ch, "ABCDefgh", (ftnlen)16, (ftnlen)8);
    s_wsfe(&io___2);
    do_fio(&c__1, " CH = ", (ftnlen)6);
    do_fio(&c__1, ch, (ftnlen)16);
    e_wsfe();
    cupper_(ch, (ftnlen)16);
    s_wsfe(&io___3);
    do_fio(&c__1, " AFTER CALLING CUPPER", (ftnlen)21);
    e_wsfe();
    s_wsfe(&io___4);
    do_fio(&c__1, " CH = ", (ftnlen)6);
    do_fio(&c__1, ch, (ftnlen)16);
    e_wsfe();
    s_copy(ch, "ABCDefgh", (ftnlen)16, (ftnlen)8);
    clower_(ch, (ftnlen)16);
    s_wsfe(&io___5);
    do_fio(&c__1, " AFTER CALLING CLOWER", (ftnlen)21);
    e_wsfe();
    s_wsfe(&io___6);
    do_fio(&c__1, " CH = ", (ftnlen)6);
    do_fio(&c__1, ch, (ftnlen)16);
    e_wsfe();
    return 0;
} /* MAIN__ */

/* Main program alias */ int chgl01_ () { MAIN__ (); return 0; }

/* Copyright (C) by GFD-Dennou Club, 1999-2000.  All rights reserved. */
#include "libtinyf2c.h"
#include "fio.h"
#include "fmt.h"
#include "fp.h"
#include "ctype.h"

#undef abs
#undef min
#undef max
#include "stdlib.h"
#include "string.h"

extern icilist *f__svic;
extern char *f__icptr;

static int mv_cur(Void)	/* shouldn't use fseek because it insists on calling fflush */
		/* instead we know too much about stdio */
{
	int cursor = f__cursor;
	f__cursor = 0;
	if(f__external == 0) {
		if(cursor < 0) {
			if(f__hiwater < f__recpos)
				f__hiwater = f__recpos;
			f__recpos += cursor;
			f__icptr += cursor;
			if(f__recpos < 0)
				err(f__elist->cierr, 110, "left off");
		}
		else if(cursor > 0) {
			if(f__recpos + cursor >= f__svic->icirlen)
				err(f__elist->cierr, 110, "recend");
			if(f__hiwater <= f__recpos)
				for(; cursor > 0; cursor--)
					(*f__putn)(' ');
			else if(f__hiwater <= f__recpos + cursor) {
				cursor -= f__hiwater - f__recpos;
				f__icptr += f__hiwater - f__recpos;
				f__recpos = f__hiwater;
				for(; cursor > 0; cursor--)
					(*f__putn)(' ');
			}
			else {
				f__icptr += cursor;
				f__recpos += cursor;
			}
		}
		return(0);
	}
	if (cursor > 0) {
		if(f__hiwater <= f__recpos)
			for(;cursor>0;cursor--) (*f__putn)(' ');
		else if(f__hiwater <= f__recpos + cursor) {
			cursor -= f__hiwater - f__recpos;
			f__recpos = f__hiwater;
			for(; cursor > 0; cursor--)
				(*f__putn)(' ');
		}
		else {
			f__recpos += cursor;
		}
	}else if (cursor < 0){
		if(cursor + f__recpos < 0)
			err(f__elist->cierr,110,"left off");
		if(f__hiwater < f__recpos)
			f__hiwater = f__recpos;
		f__recpos += cursor;
	}
	return(0);
}

static int wrt_Z(Uint *n, int w, int minlen, ftnlen len)
{
	register char *s, *se;
	register int i, w1;
	static int one = 1;
	static char hex[] = "0123456789ABCDEF";
	s = (char *)n;
	--len;
	if (*(char *)&one) {
		/* little endian */
		se = s;
		s += len;
		i = -1;
	}
	else {
		se = s + len;
		i = 1;
	}
	for(;; s += i)
		if (s == se || *s)
			break;
	w1 = (i*(se-s) << 1) + 1;
	if (*s & 0xf0)
		w1++;
	if (w1 > w){
		for(i = 0; i < w; i++)
			(*f__putn)('*');
	}else {
		if ((minlen -= w1) > 0)
			w1 += minlen;
		while(--w >= w1)
			(*f__putn)(' ');
		while(--minlen >= 0)
			(*f__putn)('0');
		if (!(*s & 0xf0)) {
			(*f__putn)(hex[*s & 0xf]);
			if (s == se)
				return 0;
			s += i;
		}
		for(;; s += i) {
			(*f__putn)(hex[*s >> 4 & 0xf]);
			(*f__putn)(hex[*s & 0xf]);
			if (s == se)
				break;
		}
	}
	return 0;
}

static int wrt_I(Uint *n, int w, ftnlen len, register int base)
{
	int ndigit,sign,spare,i;
	longint x;
	char *ans;

	if(len==sizeof(integer)) 
		x=n->il;
	else if(len == sizeof(char)) 
		x = n->ic;
#ifdef Allow_TYQUAD
	else if (len == sizeof(longint)) 
		x = n->ili;
#endif
	else
		x=n->is;
	ans=f__icvt(x,&ndigit,&sign, base);
	spare=w-ndigit;
	if(sign || f__cplus) 
		spare--;
	if(spare<0){
		for(i=0;i<w;i++) 
			(*f__putn)('*');
	}else{
		for(i=0;i<spare;i++)
			(*f__putn)(' ');
		if(sign) 
			(*f__putn)('-');
		else if(f__cplus) 
			(*f__putn)('+');
		for(i=0;i<ndigit;i++) 
			(*f__putn)(*ans++);
	}
	return(0);
}

static int wrt_IM(Uint *n, int w, int m, ftnlen len, int base)
{
	int ndigit,sign,spare,i,xsign;
	longint x;
	char *ans;
	
	if(sizeof(integer)==len) 
		x=n->il;
	else if(len == sizeof(char)) 
		x = n->ic;
#ifdef Allow_TYQUAD
	else if (len == sizeof(longint)) 
		x = n->ili;
#endif
	else 
		x=n->is;
	ans=f__icvt(x,&ndigit,&sign, base);
	if(sign || f__cplus) 
		xsign=1;
	else 
		xsign=0;
	if(ndigit+xsign>w || m+xsign>w){
		for(i=0;i<w;i++) 
			(*f__putn)('*');
		return(0);
	}
	if(x==0 && m==0){
		for(i=0;i<w;i++) 
			(*f__putn)(' ');
		return(0);
	}
	if(ndigit>=m)
		spare=w-ndigit-xsign;
	else
		spare=w-m-xsign;
	for(i=0;i<spare;i++) 
		(*f__putn)(' ');
	if(sign) 
		(*f__putn)('-');
	else if(f__cplus) 
		(*f__putn)('+');
	for(i=0;i<m-ndigit;i++) 
		(*f__putn)('0');
	for(i=0;i<ndigit;i++)
		(*f__putn)(*ans++);
	return(0);
}

static int wrt_AP(char *s)
{
	char quote;
	int i;

	if(f__cursor && (i = mv_cur()))
		return i;
	quote = *s++;
	for(;*s;s++){
		if(*s!=quote) 
			(*f__putn)(*s);
		else if(*++s==quote)
			(*f__putn)(*s);
		else 
			return(1);
	}
	return(1);
}
 
static int wrt_H(int a, char *s)
{
	int i;

	if(f__cursor && (i = mv_cur()))
		return i;
	while(a--) 
		(*f__putn)(*s++);
	return(1);
}

wrt_L(Uint *n, int len, ftnlen sz)
{
	int i;
	long x;
	if(sizeof(long)==sz) 
		x=n->il;
	else if(sz == sizeof(char)) 
		x = n->ic;
	else 
		x=n->is;
	for(i=0;i<len-1;i++)
		(*f__putn)(' ');
	if(x)
		(*f__putn)('T');
	else 
		(*f__putn)('F');
	return(0);
}
 
static int wrt_A(char *p, ftnlen len)
{
	while(len-- > 0) 
		(*f__putn)(*p++);
	return(0);
}
 
static int wrt_AW(char * p, int w, ftnlen len)
{
	while(w>len){
		w--;
		(*f__putn)(' ');
	}
	while(w-- > 0)
		(*f__putn)(*p++);
	return(0);
}


static int wrt_G(ufloat *p, int w, int d, int e, ftnlen len)
{
	double up = 1,x;
	int i=0,oldscale,n,j;
	
	x = len==sizeof(real)?p->pf:p->pd;
	if(x < 0 ) 
		x = -x;
	if(x<.1) {
		if (x != 0.)
			return(wrt_E(p,w,d,e,len));
		i = 1;
		goto have_i;
	}
	for(;i<=d;i++,up*=10){
		if(x>=up) continue;
 have_i:
		oldscale = f__scale;
		f__scale = 0;
		if(e==0) n=4;
		else	n=e+2;
		i=wrt_F(p,w-n,d-i,len);
		for(j=0;j<n;j++) 
			(*f__putn)(' ');
		f__scale=oldscale;
		return(i);
	}
	return(wrt_E(p,w,d,e,len));
}

w_ed(struct syl *p, char *ptr, ftnlen len)
{
	int i;

	if(f__cursor && (i = mv_cur()))
		return i;
	switch(p->op){
	default:
		fprintf(stderr,"w_ed, unexpected code: %d\n", p->op);
		sig_die(f__fmtbuf, 1);
	case I:	return(wrt_I((Uint *)ptr,p->p1,len, 10));
	case IM:
		return(wrt_IM((Uint *)ptr,p->p1,p->p2.i[0],len,10));

		/* O and OM don't work right for character, double, complex, */
		/* or doublecomplex, and they differ from Fortran 90 in */
		/* showing a minus sign for negative values. */

	case O:	return(wrt_I((Uint *)ptr, p->p1, len, 8));
	case OM:
		return(wrt_IM((Uint *)ptr,p->p1,p->p2.i[0],len,8));
	case L:	return(wrt_L((Uint *)ptr,p->p1, len));
	case A: return(wrt_A(ptr,len));
	case AW:
		return(wrt_AW(ptr,p->p1,len));
	case D:
	case E:
	case EE:
		return(wrt_E((ufloat *)ptr,p->p1,p->p2.i[0],p->p2.i[1],len));
	case G:
	case GE:
		return(wrt_G((ufloat *)ptr,p->p1,p->p2.i[0],p->p2.i[1],len));
	case F:	return(wrt_F((ufloat *)ptr,p->p1,p->p2.i[0],len));

		/* Z and ZM assume 8-bit bytes. */

	case Z: return(wrt_Z((Uint *)ptr,p->p1,0,len));
	case ZM:
		return(wrt_Z((Uint *)ptr,p->p1,p->p2.i[0],len));
	}
}

w_ned(struct syl *p)
{
	switch(p->op)
	{
	default: fprintf(stderr,"w_ned, unexpected code: %d\n", p->op);
		sig_die(f__fmtbuf, 1);
	case SLASH:
		return((*f__donewrec)());
	case T: f__cursor = p->p1-f__recpos - 1;
		return(1);
	case TL: f__cursor -= p->p1;
		if(f__cursor < -f__recpos)	/* TL1000, 1X */
			f__cursor = -f__recpos;
		return(1);
	case TR:
	case X:
		f__cursor += p->p1;
		return(1);
	case APOS:
		return(wrt_AP(p->p2.s));
	case H:
		return(wrt_H(p->p1,p->p2.s));
	}
}

wrt_E(ufloat *p, int w, int d, int e, ftnlen len)
{
	char buf[FMAX+EXPMAXDIGS+4], *s, *se;
	int d1, delta, e1, i, sign, signspace;
	double dd;
	int e0 = e;

	if(e <= 0)
		e = 2;
	if(f__scale) {
		if(f__scale >= d + 2 || f__scale <= -d)
			goto nogood;
	}
	if(f__scale <= 0)
		--d;
	if (len == sizeof(real))
		dd = p->pf;
	else
		dd = p->pd;
	if (dd < 0.) {
		signspace = sign = 1;
		dd = -dd;
	}
	else {
		sign = 0;
		signspace = (int)f__cplus;
		if (!dd)
			dd = 0.;	/* avoid -0 */
	}
	delta = w - (2 /* for the . and the d adjustment above */
			+ 2 /* for the E+ */ + signspace + d + e);
	if (delta < 0) {
nogood:
		while(--w >= 0)
			PUT('*');
		return(0);
	}
	if (f__scale < 0)
		d += f__scale;
	if (d > FMAX) {
		d1 = d - FMAX;
		d = FMAX;
	}
	else
		d1 = 0;
	sprintf(buf,"%#.*E", d, dd);
	/* check for NaN, Infinity */
	if (!isdigit(buf[0])) {
		switch(buf[0]) {
			case 'n':
			case 'N':
				signspace = 0;	/* no sign for NaNs */
		}
		delta = w - strlen(buf) - signspace;
		if (delta < 0)
			goto nogood;
		while(--delta >= 0)
			PUT(' ');
		if (signspace)
			PUT(sign ? '-' : '+');
		for(s = buf; *s; s++)
			PUT(*s);
		return 0;
	}
	se = buf + d + 3;
	if (dd)
		sprintf(se, "%+.2d", atoi(se) + 1 - f__scale);
	else
		strcpy(se, "+00");
	s = ++se;
	if (e < 2) {
		if (*s != '0')
			goto nogood;
		}
	/* accommodate 3 significant digits in exponent */
	if (s[2]) {
		if (!e0) {
			for(s -= 2, e1 = 2; s[0] = s[1]; s++)
				;
		}
		else if (e0 >= 0)
			goto shift;
		else
			e1 = e;
	}
	else
 shift:
		for(s += 2, e1 = 2; *s; ++e1, ++s)
			if (e1 >= e)
				goto nogood;
	while(--delta >= 0)
		PUT(' ');
	if (signspace)
		PUT(sign ? '-' : '+');
	s = buf;
	i = f__scale;
	if (f__scale <= 0) {
		PUT('.');
		for(; i < 0; ++i)
			PUT('0');
		PUT(*s);
		s += 2;
	}
	else if (f__scale > 1) {
		PUT(*s);
		s += 2;
		while(--i > 0)
			PUT(*s++);
		PUT('.');
	}
	if (d1) {
		se -= 2;
		while(s < se) PUT(*s++);
		se += 2;
		do PUT('0'); while(--d1 > 0);
	}
	while(s < se)
		PUT(*s++);
	if (e < 2)
		PUT(s[1]);
	else {
		while(++e1 <= e)
			PUT('0');
		while(*s)
			PUT(*s++);
	}
	return 0;
}

wrt_F(ufloat *p, int w, int d, ftnlen len)
{
	int d1, sign, n;
	double x;
	char *b, buf[MAXINTDIGS+MAXFRACDIGS+4], *s;

	x= (len==sizeof(real)?p->pf:p->pd);
	if (d < MAXFRACDIGS)
		d1 = 0;
	else {
		d1 = d - MAXFRACDIGS;
		d = MAXFRACDIGS;
	}
	if (x < 0.)	{ 
		x = -x; sign = 1; 
	}
	else {
		sign = 0;
		if (!x)
			x = 0.;
	}

	if (n = f__scale) {
		if (n > 0)
			do x *= 10.; while(--n > 0);
		else
			do x *= 0.1; while(++n < 0);
	}
	n = sprintf(b = buf, "%#.*f", d, x) + d1;

	if (buf[0] == '0' && d)	{ 
		++b; --n; 
	}
	if (sign) {
		/* check for all zeros */
		for(s = b;;) {
			while(*s == '0') s++;
			switch(*s) {
				case '.':
					s++; continue;
				case 0:
					sign = 0;
			}
			break;
		}
	}
	if (sign || f__cplus)
		++n;
	if (n > w) {
		while(--w >= 0)
			PUT('*');
		return 0;
	}
	for(w -= n; --w >= 0; )
		PUT(' ');
	if (sign)
		PUT('-');
	else if (f__cplus)
		PUT('+');
	while(n = *b++)
		PUT(n);
	while(--d1 >= 0)
		PUT('0');
	return 0;
}

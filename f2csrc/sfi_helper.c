/* Copyright (C) by GFD-Dennou Club, 1999-2000.  All rights reserved. */
#include "libtinyf2c.h"
#include "fio.h"
#include "fmt.h"

extern char *f__icptr;
char *f__icend;
extern icilist *f__svic;
int f__icnum;
extern int f__hiwater;

z_getc(Void)
{
	if(f__recpos++ < f__svic->icirlen) {
		if(f__icptr >= f__icend) err(f__svic->iciend,(EOF),"endfile");
		return(*(unsigned char *)f__icptr++);
	}
	return '\n';
}

void z_putc(int c)
{
	if (f__icptr < f__icend && f__recpos++ < f__svic->icirlen)
		*f__icptr++ = c;
}

z_rnew(Void)
{
	f__icptr = f__svic->iciunit + (++f__icnum)*f__svic->icirlen;
	f__recpos = 0;
	f__cursor = 0;
	f__hiwater = 0;
	return 1;
}

int z_endp(Void)
{
	(*f__donewrec)();
	return 0;
}

int iw_rev(Void)							   
{												
	if(f__workdone)
		z_endp();
	f__hiwater = f__recpos = f__cursor = 0;
	return(f__workdone=0);
}

z_wnew(Void)
{
	if (f__recpos < f__hiwater) {
		f__icptr += f__hiwater - f__recpos;
		f__recpos = f__hiwater;
	}
	while(f__recpos++ < f__svic->icirlen)
		*f__icptr++ = ' ';
	f__recpos = 0;
	f__cursor = 0;
	f__hiwater = 0;
	f__icnum++;
	return 1;
}


/* Copyright (C) by GFD-Dennou Club, 1999-2000.  All rights reserved. */
/* STARTUP PROCEDURE FOR UNIX FORTRAN PROGRAMS */

#include "stdio.h"
#include "signal1.h"

#ifndef SIGIOT
#ifdef SIGABRT
#define SIGIOT SIGABRT
#endif
#endif

#undef VOID
#include "stdlib.h"

#ifndef VOID
#define VOID void
#endif

#include "libtinyf2c.h"

extern void f_exit(void);
extern int atexit(void (*)(void));

extern void f_init(void), sig_die(char*, int);
#define Int int

static VOID sigfdie(Sigarg)
{
	Use_Sigarg;
	sig_die("Floating Exception", 1);
}


static VOID sigidie(Sigarg)
{
	Use_Sigarg;
	sig_die("IOT Trap", 1);
}

#ifdef SIGQUIT
static VOID sigqdie(Sigarg)
{
	Use_Sigarg;
	sig_die("Quit signal", 1);
}
#endif


static VOID sigindie(Sigarg)
{
	Use_Sigarg;
	sig_die("Interrupt", 0);
}

static VOID sigtdie(Sigarg)
{
	Use_Sigarg;
	sig_die("Killed", 0);
}

#ifdef SIGTRAP
static VOID sigtrdie(Sigarg)
{
	Use_Sigarg;
	sig_die("Trace trap", 1);
}
#endif

int xargc;
char **xargv;

void getarg_(ftnint *n, register char *s, ftnlen ls)
{
	register char *t;
	register int i;

	if(*n>=0 && *n<xargc)
		t = xargv[*n];
	else
		t = "";

	for(i = 0; i<ls && *t!='\0' ; ++i)
		*s++ = *t++;
	for( ; i<ls ; ++i)
		*s++ = ' ';
}

ftnint iargc_(void)
{
	extern int xargc;
	return ( xargc - 1 );
}

void init_arg_(int argc, char **argv)
{
	xargc = argc;
	xargv = argv;
}

main(int argc, char **argv)
{
	init_arg_(argc, argv);
	signal1(SIGFPE, sigfdie);	/* ignore underflow, enable overflow */
#ifdef SIGIOT
	signal1(SIGIOT, sigidie);
#endif
#ifdef SIGTRAP
	signal1(SIGTRAP, sigtrdie);
#endif
#ifdef SIGQUIT
	if(signal1(SIGQUIT,sigqdie) == SIG_IGN)
		signal1(SIGQUIT, SIG_IGN);
#endif
	if(signal1(SIGINT, sigindie) == SIG_IGN)
		signal1(SIGINT, SIG_IGN);
	signal1(SIGTERM,sigtdie);

	f_init();
	atexit(f_exit);
	exit(0);	/* exit(0) rather than return(0) to bypass Cray bug */
	return 0;	/* For compilers that complain of missing return values; */
			/* others will complain that this is unreachable code. */
}

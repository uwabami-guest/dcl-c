/* Copyright (C) by GFD-Dennou Club, 1999-2000.  All rights reserved. */
#include "libtinyf2c.h"
#include "fio.h"
#include "fmt.h"

/* functions for reading sequential formatted external file:
 *      xrd_SL(Void);
 *      x_getc(Void);
 *      x_endp(Void);
 *      x_rev(Void);
 * functions for reading sequential formatted external file:
 *      int x_wSL(Void);
 *      int xw_rev(Void);
 *      int xw_end(Void);
 */

/* read sequential formatted external */
extern char *f__fmtbuf;
xrd_SL(Void)
{	
	int ch;
	if(!f__curunit->uend)
	{
		while((ch=getc(f__cf))!='\n')
		{
			if (ch == EOF) {
				f__curunit->uend = 1;
				break;
			}
		}
	}
	f__cursor=f__recpos=0;
	return(1);
}

x_getc(Void)
{
	int ch;
	if(f__curunit->uend)
		return(EOF);
	ch = getc(f__cf);
	if(ch!=EOF && ch!='\n')
	{	
		f__recpos++;
		return(ch);
	}
	if(ch=='\n')
	{	
		(void) ungetc(ch,f__cf);
		return(ch);
	}
	if(f__curunit->uend || feof(f__cf))
	{
		errno=0;
		f__curunit->uend=1;
		return(-1);
	}
	return(-1);
}

x_endp(Void)
{
	xrd_SL();
	return f__curunit->uend == 1 ? EOF : 0;
}

x_rev(Void)
{
	(void) xrd_SL();
	return(0);
}

/*write sequential formatted external*/
extern int f__hiwater;
int x_wSL(Void)
{
	int n = f__putbuf('\n');
	f__hiwater = f__recpos = f__cursor = 0;
	return(n == 0);
}

int xw_end(Void)
{
	int n;
	if(f__nonl) {
		f__putbuf(n = 0);
		fflush(f__cf);
	}
	else
		n = f__putbuf('\n');
	f__hiwater = f__recpos = f__cursor = 0;
	return n;
}

int xw_rev(Void)
{
	int n = 0;
	if(f__workdone) {
		n = f__putbuf('\n');
		f__workdone = 0;
	}
	f__hiwater = f__recpos = f__cursor = 0;
	return n;
}


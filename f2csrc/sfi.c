/* Copyright (C) by GFD-Dennou Club, 1999-2000.  All rights reserved. */
#include "libtinyf2c.h"
#include "fio.h"
#include "fmt.h"

extern char *f__icptr;
extern char *f__icend;
extern icilist *f__svic;
extern int f__icnum;
extern int f__hiwater;

extern z_getc(void);
extern void z_putc(int c);
extern int iw_rev(void);							   
extern z_wnew(void);
extern int z_endp(void);

/* check the sequential formatted internal file.
 */
c_si(icilist *a)
{
	f__elist = (cilist *)a;
	f__fmtbuf=a->icifmt;
	f__curunit = 0;
	f__sequential=f__formatted=1;
	f__external=0;
	if(pars_f(f__fmtbuf)<0)
		err(a->icierr,100,"startint");
	fmt_bg();
	f__cblank=f__cplus=f__scale=0;
	f__svic=a;
	f__icnum=f__recpos=0;
	f__cursor = 0;
	f__hiwater = 0;
	f__icptr = a->iciunit;
	f__icend = f__icptr + a->icirlen*a->icirnum;
	f__cf = 0;
	return(0);
}

/* start reading to the sequential formatted internal file.
 */
integer s_rsfi(icilist *a)
{
	int n;
	if(n=c_si(a)) 
		return(n);
	f__reading=1;
	f__doed=rd_ed;
	f__doned=rd_ned;
	f__getn=z_getc;
	f__dorevert = z_endp;
	f__donewrec = z_rnew;
	f__doend = z_endp;
	return(0);
}

/* end reading to the sequential formatted internal file.
 */
integer e_rsfi(Void)
{	
	int n = en_fio();
	f__fmtbuf = NULL;
	return(n);
}

/* start writing to the sequential formatted internal file.
 */
integer s_wsfi(icilist *a)
{	
	int n;
	
	if(n=c_si(a)) 
		return(n);
	f__reading=0;
	f__doed=w_ed;
	f__doned=w_ned;
	f__putn=z_putc;
	f__dorevert = iw_rev;
	f__donewrec = z_wnew;
	f__doend = z_endp;
	return(0);
}

/* end writing to the sequential formatted internal file.
 */
integer e_wsfi(Void)
{
	int n;
	n = en_fio();
	f__fmtbuf = NULL;
	if(f__svic->icirnum != 1
	 && (f__icnum >  f__svic->icirnum
	 || (f__icnum == f__svic->icirnum && (f__recpos | f__hiwater))))
		err(f__svic->icierr,110,"inwrite");
	if (f__recpos < f__hiwater)
		f__recpos = f__hiwater;
	if (f__recpos >= f__svic->icirlen)
		err(f__svic->icierr,110,"recend");
	if (!f__recpos && f__icnum)
		return n;
	while(f__recpos++ < f__svic->icirlen)
		*f__icptr++ = ' ';
	return n;
}

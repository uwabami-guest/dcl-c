/* Copyright (C) by GFD-Dennou Club, 1999-2000.  All rights reserved. */
#include "libtinyf2c.h"

#undef abs
#include "math.h"

extern void sig_die(char*,int);
extern void z_div(doublecomplex*, doublecomplex*, doublecomplex*);
extern double f__cabs(double, double);

/* all the functions in this file return the value of (*ap)**(*bp).
   (1) integer pow_ii(integer *ap, integer *bp)
   (2) double pow_ri(real *ap, integer *bp)
   (3) double pow_di(doublereal *ap, integer *bp)
   add 2000.7.16
   (4)void pow_ci(complex *p, complex *a, integer *b)
   (5)void pow_zz(doublecomplex *r, doublecomplex *a, doublecomplex *b)
   (6)void pow_zi(doublecomplex *p, doublecomplex *a, integer *b)
*/

integer pow_ii(integer *ap, integer *bp)
{
	integer pow, x, n;
	unsigned long u;

	x = *ap;
	n = *bp;

	if (n <= 0) {
		if (n == 0 || x == 1)
			return 1;
		if (x != -1)
			return x == 0 ? 1/x : 0;
		n = -n;
	}
	u = n;
	for(pow = 1; ; )
	{
		if(u & 01)
			pow *= x;
		if(u >>= 1)
			x *= x;
		else
			break;
	}
	return(pow);
}

double pow_ri(real *ap, integer *bp)
{
	double pow, x;
	integer n;
	unsigned long u;

	pow = 1;
	x = *ap;
	n = *bp;

	if(n != 0)
	{
		if(n < 0)
		{
			n = -n;
			x = 1/x;
		}
		for(u = n; ; )
		{
			if(u & 01)
			  pow *= x;
			if(u >>= 1)
			  x *= x;
			else
			  break;
		}
	}
	return(pow);
}

double pow_di(doublereal *ap, integer *bp)
{
  double pow, x;
  integer n;
  unsigned long u;

	pow = 1;
	x = *ap;
	n = *bp;

	if(n != 0)
	{
		if(n < 0)
		{
		  n = -n;
		  x = 1/x;
		}
		for(u = n; ; )
		{
			if(u & 01)
				pow *= x;
			if(u >>= 1)
				x *= x;
			else
				break;
		}
	}
	return(pow);
}

void pow_zi(doublecomplex *p, doublecomplex *a, integer *b) 	/* p = a**b  */
{
	integer n;
	unsigned long u;
	double t;
	doublecomplex q, x;
	static doublecomplex one = {1.0, 0.0};

	n = *b;
	q.r = 1;
	q.i = 0;

	if(n == 0)
		goto done;
	if(n < 0)
		{
		n = -n;
		z_div(&x, &one, a);
		}
	else
		{
		x.r = a->r;
		x.i = a->i;
		}

	for(u = n; ; )
		{
		if(u & 01)
			{
			t = q.r * x.r - q.i * x.i;
			q.i = q.r * x.i + q.i * x.r;
			q.r = t;
			}
		if(u >>= 1)
			{
			t = x.r * x.r - x.i * x.i;
			x.i = 2 * x.r * x.i;
			x.r = t;
			}
		else
			break;
		}
 done:
	p->i = q.i;
	p->r = q.r;
	}


void pow_ci(complex *p, complex *a, integer *b) 	/* p = a**b  */
{
  doublecomplex p1, a1;
  
  a1.r = a->r;
  a1.i = a->i;
  
  pow_zi(&p1, &a1, b);
  
  p->r = p1.r;
  p->i = p1.i;
}

void pow_zz(doublecomplex *r, doublecomplex *a, doublecomplex *b)
{
  double logr, logi, x, y;
  
  logr = log( f__cabs(a->r, a->i) );
  logi = atan2(a->i, a->r);

  x = exp( logr * b->r - logi * b->i );
  y = logr * b->i + logi * b->r;

  r->r = x * cos(y);
  r->i = x * sin(y);
}



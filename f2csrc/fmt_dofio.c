/* Copyright (C) by GFD-Dennou Club, 1999-2000.  All rights reserved. */
#include "libtinyf2c.h"
#include "fio.h"
#include "fmt.h"

#define skip(s) while(*s==' ') s++

#define SYLMX 300
#define GLITCH '\2'
	/* special quote character for stu */
extern int f__cursor,f__scale;
extern flag f__cblank,f__cplus;	/*blanks in I and compulsory plus*/
extern struct syl f__syl[SYLMX];
extern int f__parenlvl,f__pc,f__revloc;

extern int type_f(int n);

/* do real formatted I/O operations for (*number) times, using the format 
 * saved in f__syl. 
 */
integer do_fio(ftnint *number, char *ptr, ftnlen len)
{
	struct syl *p;
	int n,i;
	
	for(i=0;i<*number;i++,ptr+=len)
	{
loop:	switch(type_f((p= &f__syl[f__pc])->op))
		{
		default:
			fprintf(stderr,"unknown code in do_fio: %d\n%s\n",
				p->op,f__fmtbuf);
			err(f__elist->cierr,100,"do_fio");
		case NED:
			if((*f__doned)(p))
			{	
				f__pc++;
				goto loop;
			}
			f__pc++;
			continue;
		case ED:
			if(f__cnt[f__cp]<=0)
			{	
				f__cp--;
				f__pc++;
				goto loop;
			}
			if(ptr==NULL)
				return((*f__doend)());
			f__cnt[f__cp]--;
			f__workdone=1;
			if((n=(*f__doed)(p,ptr,len))>0)
				errfl(f__elist->cierr,errno,"fmt");
			if(n<0)
				err(f__elist->ciend,(EOF),"fmt");
			continue;
		case STACK:
			f__cnt[++f__cp]=p->p1;
			f__pc++;
			goto loop;
		case RET1:
			f__ret[++f__rp]=p->p1;
			f__pc++;
			goto loop;
		case GOTO:
			if(--f__cnt[f__cp]<=0)
			{	
				f__cp--;
				f__rp--;
				f__pc++;
				goto loop;
			}
			f__pc=1+f__ret[f__rp--];
			goto loop;
		case REVERT:
			f__rp=f__cp=0;
			f__pc = p->p1;
			if(ptr==NULL)
				return((*f__doend)());
			if(!f__workdone) 
				return(0);
			if((n=(*f__dorevert)()) != 0) 
				return(n);
			goto loop;
		case COLON:
			if(ptr==NULL)
				return((*f__doend)());
			f__pc++;
			goto loop;
		case NONL:
			f__nonl = 1;
			f__pc++;
			goto loop;
		case S:
		case SS:
			f__cplus=0;
			f__pc++;
			goto loop;
		case SP:
			f__cplus = 1;
			f__pc++;
			goto loop;
		case P:	f__scale=p->p1;
			f__pc++;
			goto loop;
		case BN:
			f__cblank=0;
			f__pc++;
			goto loop;
		case BZ:
			f__cblank=1;
			f__pc++;	   
			goto loop;
		}
	}
	return(0);
}

/* end the formatted I/O operations 
 */
en_fio(Void)
{
	ftnint one=1;
	return(do_fio(&one,(char *)NULL,(ftnint)0));
}

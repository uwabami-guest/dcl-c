/* Copyright (C) by GFD-Dennou Club, 1999-2000.  All rights reserved. */
#include "libtinyf2c.h"
#include "fio.h"

extern uiolen f__reclen;
long f__recloc;

/* this file is used for i/o of sequential unformatted external file.
in fortran, just like: 
      open(iu,file='XXXX',form='unformatted')
	  read(iu) ii   !write(iu) ii
	  close(iu)
in fortran,unformatted file's format: [data-length1][data1][data-length1][data-length2][data2][data-length2]...
*/

/* check for sequential unformatted external file, and set some flags for future 
   use. if the file is not opened , then it maybe a temp file. so we call fk_open(..)
   to open it. if it is opened, we just get the file pointer.
*/
c_sue(cilist *a)
{
	f__external=f__sequential=1;  /* it is external and sequential */
	f__formatted=0;				  /* but it is unformatted */
	f__curunit = &f__units[a->ciunit];
	if(a->ciunit >= MXUNIT || a->ciunit < 0)
		err(a->cierr,101,"startio");
	f__elist=a;
	if(f__curunit->ufd==NULL && fk_open(SEQ,UNF,a->ciunit))
		err(a->cierr,114,"sue");
	f__cf=f__curunit->ufd;
	if(f__curunit->ufmt) 
		err(a->cierr,103,"sue")
	if(!f__curunit->useek) 
		err(a->cierr,103,"sue")
	return(0);
}

/* start reading for the sequential unformatted external file.
   if it is not initialied we will call f_init() to do it. then 
   we will read this file according the file format of unformatted 
   file in fortran.
*/
integer s_rsue(cilist *a)
{
	int n;
	if(!f__init) 
		f_init();
	f__reading=1;
	if(n=c_sue(a)) 
		return(n);
	f__recpos=0;
	if(f__curunit->uwrt && f__nowreading(f__curunit))
		err(a->cierr, errno, "read start");
	if(fread((char *)&f__reclen,sizeof(uiolen),1,f__cf)!= 1)
	{
		if(feof(f__cf))
		{	f__curunit->uend = 1;
			err(a->ciend, EOF, "start");
		}
		clearerr(f__cf);
		err(a->cierr, errno, "start");
	}
	return(0);
}

/* end reading for the sequential unformatted external file .
   just skip the data-length field.
*/
integer e_rsue(Void)
{
	(void) fseek(f__cf,(long)(f__reclen-f__recpos+sizeof(uiolen)),SEEK_CUR);
	return(0);
}

/* start writing for the sequential unformatted external file.
   first need to skip the data-length field. 
*/
integer s_wsue(cilist *a)
{
	int n;
	
	if(!f__init) 
		f_init();
	if(n=c_sue(a)) 
		return(n);
	f__reading=0;
	f__reclen=0;
	if(f__curunit->uwrt != 1 && f__nowwriting(f__curunit))
		err(a->cierr, errno, "write start");
	f__recloc=ftell(f__cf);
	(void) fseek(f__cf,(long)sizeof(uiolen),SEEK_CUR);
	return(0);
}

/* end writing for the sequential unformatted external file.
   it need to write twice the data-length to the file just before 
   the true data and just after the true data.
*/
integer e_wsue(Void)
{
	long loc;
	
	fwrite((char *)&f__reclen,sizeof(uiolen),1,f__cf);
#ifdef ALWAYS_FLUSH
	if (fflush(f__cf))
		err(f__elist->cierr, errno, "write end");
#endif
	loc=ftell(f__cf);
	fseek(f__cf,f__recloc,SEEK_SET);
	fwrite((char *)&f__reclen,sizeof(uiolen),1,f__cf);
	fseek(f__cf,loc,SEEK_SET);
	return(0);
}

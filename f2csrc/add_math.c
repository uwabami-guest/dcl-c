/* Copyright (C) by GFD-Dennou Club, 1999-2000.  All rights reserved. */
#include "libtinyf2c.h"
#undef abs
#undef min
#undef max
#include "stdio.h"
#include "stdlib.h"

extern void sig_die(char*,int);
#define log10e 0.43429448190325182765

double r_lg10(real *x);
double r_mod(real *x, real *y);
double r_dim(real *a, real *b);
double r_sign(real *a, real *b);
integer i_sign(integer *a, integer *b);
integer i_nint(real *x);
void c_div(complex *c, complex *a, complex *b);
void z_div(doublecomplex *c, doublecomplex *a, doublecomplex *b);
double f__cabs(double real, double imag);
integer lbit_shift(integer a, integer b);

/* caculate the result of 2 complex number and the result is c.
 * that is, c=a/b.
 */
void c_div(complex *c, complex *a, complex *b)
{
	double div;

	div=b->r*b->r+b->i*b->i;
	if(div == 0)
		sig_die("complex division by zero", 1);
	c->r=(a->r*b->r+a->i*b->i)/div;
	c->i=(a->i*b->r-a->r*b->i)/div;
}

#include "math.h"

/* Returns the 10 based logarithm of (*x)
 */
double r_lg10(real *x)
{
	return( log10e * log(*x) );
}

/* Returns a remainder.
 */
double r_mod(real *x, real *y)
{
	double quotient;
	
	if( (quotient = (double)*x / *y) >= 0)
		quotient = floor(quotient);
	else
		quotient = -floor(-quotient);
	return(*x - (*y) * quotient );
}

/* Returns the difference x-y if the result is positive;
 * otherwise returns zero.
 */
double r_dim(real *a, real *b)
{
	return (*a > *b ? *a - *b : 0);
}

/* Performs a sign transfer by returning the absolute value of 
 * the first argument multiplied by the sign of the second argument.
 */
double r_sign(real *a, real *b)
{
	double x;
	
	x = (*a >= 0 ? *a : - *a);
	return( *b >= 0 ? x : -x);
}

/* return the integer with sign of b but absolute value of a.
*/
integer i_sign(integer *a, integer *b)
{
	integer x;
	x = (*a >= 0 ? *a : - *a);
	return( *b >= 0 ? x : -x);
}

/*	return the most nearest integer for float number x.
*/
integer i_nint(real *x)
{
	return (integer)(*x >= 0 ? floor(*x + .5) : -floor(.5 - *x));
}

/* return the value of (*ap)**(*bp)
 */
double pow_dd(doublereal *ap, doublereal *bp)
{
	return(pow(*ap, *bp) );
}

void z_div(doublecomplex *c, doublecomplex *a, doublecomplex *b)

{
	double ratio, den;
	double abr, abi, cr;

	if( (abr = b->r) < 0.)
		abr = - abr;
	if( (abi = b->i) < 0.)
		abi = - abi;
	if( abr <= abi )
		{
		if(abi == 0)
			sig_die("complex division by zero", 1);
		ratio = b->r / b->i ;
		den = b->i * (1 + ratio*ratio);
		cr = (a->r*ratio + a->i) / den;
		c->i = (a->i*ratio - a->r) / den;
		}

	else
		{
		ratio = b->i / b->r ;
		den = b->r * (1 + ratio*ratio);
		cr = (a->r + a->i*ratio) / den;
		c->i = (a->i - a->r*ratio) / den;
		}
	c->r = cr;
	}


double f__cabs(double real, double imag)
{
  double temp;

  if(real < 0)
    real = -real;
  if(imag < 0)
    imag = -imag;
  if(imag > real){
    temp = real;
    real = imag;
    imag = temp;
  }
  if((real+imag) == real)
    return(real);
  
  temp = imag/real;
  temp = real*sqrt(1.0 + temp*temp);  /*overflow!!*/
  return(temp);
}


integer lbit_shift(integer a, integer b)
{
  return b >= 0 ? a << b : (integer) ((uinteger) a >> -b);
}

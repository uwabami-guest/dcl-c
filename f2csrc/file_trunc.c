/* Copyright (C) by GFD-Dennou Club, 1999-2000.  All rights reserved. */
#include "libtinyf2c.h"
#include "fio.h"
#undef abs	
#undef min
#undef max
#include "stdlib.h"
#include "string.h"

extern char *f__r_mode[], *f__w_mode[];

/* copy a part of fixed length of one file to another. 
 */
static int copy(FILE *from, register long len, FILE *to)
{
	int len1;
	char buf[BUFSIZ];

	while(fread(buf, len1 = len > BUFSIZ ? BUFSIZ : (int)len, 1, from)) {
		if (!fwrite(buf, len1, 1, to))
			return 1;
		if ((len -= len1) <= 0)
			break;
	}
	return 0;
}

/* truncate the file from the current file pointer.
 */
int t_runc(alist *a)
{
	long loc, len;
	unit *b;
	FILE *bf, *tf;
	int rc = 0;

	b = &f__units[a->aunit];
	if(b->url)
		return(0);	/*don't truncate direct files*/
	loc=ftell(bf = b->ufd);
	fseek(bf,0L,SEEK_END);
	len=ftell(bf);
	if (loc >= len || b->useek == 0 || b->ufnm == NULL)
		return(0);
	fclose(b->ufd);
	if (!loc) {
		if (!(bf = fopen(b->ufnm, f__w_mode[b->ufmt])))
			rc = 1;
		if (b->uwrt)
			b->uwrt = 1;
		goto done;
	}
	if (!(bf = fopen(b->ufnm, f__r_mode[0]))
	 || !(tf = tmpfile())) {
		rc = 1;
		goto done;
	}
	if (copy(bf, loc, tf)) {
 bad1:
		rc = 1;
		goto done1;
	}
	if (!(bf = freopen(b->ufnm, f__w_mode[0], bf)))
		goto bad1;
	rewind(tf);
	if (copy(tf, loc, bf))
		goto bad1;
	b->urw = 2;
done1:			  
	fclose(tf);
done:
	f__cf = b->ufd = bf;
	if (rc)
		err(a->aerr,111,"endfile");
	return 0;
}

/* Copyright (C) by GFD-Dennou Club, 1999-2000.  All rights reserved. */
#include "libtinyf2c.h"
#include "fio.h"

uiolen f__reclen;

do_us(ftnint *number, char *ptr, ftnlen len)
{
	if(f__reading)
	{
		f__recpos += (int)(*number * len);
		if(f__recpos>f__reclen)				
			err(f__elist->cierr, 110, "do_us");
		if (fread(ptr,(int)len,(int)(*number),f__cf) != *number)
			err(f__elist->ciend, EOF, "do_us");
		return(0);
	}
	else
	{
		f__reclen += *number * len;
		(void) fwrite(ptr,(int)len,(int)(*number),f__cf);
		return(0);
	}
}

integer do_ud(ftnint *number, char *ptr, ftnlen len)
{
	f__recpos += (int)(*number * len);
	if(f__recpos > f__curunit->url && f__curunit->url!=1)
		err(f__elist->cierr,110,"do_ud");
	if(f__reading)
	{
		if(fread(ptr,(int)len,(int)(*number),f__cf) != *number)
			err(f__elist->cierr,EOF,"do_ud")
		else return(0);
	}
	(void) fwrite(ptr,(int)len,(int)(*number),f__cf);
	return(0);
}

/* do unformatted external file i/o
   if it is sequential call do_us(...)
   if it is not sequential call do_ud(...)
*/
integer do_uio(ftnint *number, char *ptr, ftnlen len)
{
	if(f__sequential)
		return(do_us(number,ptr,len));
	else	
		return(do_ud(number,ptr,len));
}

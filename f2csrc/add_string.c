/* Copyright (C) by GFD-Dennou Club, 1999-2000.  All rights reserved. */
#include "libtinyf2c.h"
#include "string.h"
#include "stdio.h"
#undef abs
#undef min
#undef max
#include "stdlib.h"

static integer memfailure = 3;
extern void exit_(integer*);

char *F77_aloc(integer Len, char *whence);
integer i_len(char *s, ftnlen n);
integer i_indx(char *a, char *b, ftnlen la, ftnlen lb);
VOID s_cat(char *lp, char *rpp[], ftnlen rnp[], ftnlen *np, ftnlen ll);
integer s_cmp(char *a0, char *b0, ftnlen la, ftnlen lb);
void s_copy(register char *a, register char *b, ftnlen la, ftnlen lb);
VOID b_char(char *a, char *b, ftnlen blen);
VOID g_char(char *a, ftnlen alen, char *b);

/*return the length of string s
*/
integer i_len(char *s, ftnlen n)
{
	return(n);
}

/* find the index of string b in string a. and if it doesn't find , 
 * just return 0.
 */
integer i_indx(char *a, char *b, ftnlen la, ftnlen lb)
{
	ftnlen i, n;
	char *s, *t, *bend;

	n = la - lb + 1;
	bend = b + lb;

	for(i = 0 ; i < n ; ++i){
		s = a + i;
		t = b;
		while(t < bend)	{
			if(*s++ != *t++)
				goto no;
		}
		return(i+1);
no:		;
	}
	return(0);
}

/* concatenate strings to a string
 */
VOID s_cat(char *lp, char *rpp[], ftnlen rnp[], ftnlen *np, ftnlen ll)
{
	ftnlen i, nc;
	char *rp;
	ftnlen n = *np;
	ftnlen L, m;
	char *lp0, *lp1;

	lp0 = 0;
	lp1 = lp;
	L = ll;
	i = 0;
	while(i < n) {
		rp = rpp[i];
		m = rnp[i++];
		if (rp >= lp1 || rp + m <= lp) {
			if ((L -= m) <= 0) {
				n = i;
				break;
			}
			lp1 += m;
			continue;
		}
		lp0 = lp;
		lp = lp1 = F77_aloc(L = ll, "s_cat");
		break;
	}
	lp1 = lp;
	for(i = 0 ; i < n ; ++i) {
		nc = ll;
		if(rnp[i] < nc)
			nc = rnp[i];
		ll -= nc;
		rp = rpp[i];
		while(--nc >= 0)
			*lp++ = *rp++;
	}
	while(--ll >= 0)
		*lp++ = ' ';
	if (lp0) {
		memcpy(lp0, lp1, L);
		free(lp1);
	}
}

/* compare two strings */
integer s_cmp(char *a0, char *b0, ftnlen la, ftnlen lb)
{
	register unsigned char *a, *aend, *b, *bend;
	
	a = (unsigned char *)a0;
	b = (unsigned char *)b0;
	aend = a + la;
	bend = b + lb;
	if(la <= lb) {
		while(a < aend) {
			if(*a != *b)
				return( *a - *b );
			else { 
				++a;
				++b;
			}
		}
		while(b < bend) {
			if(*b != ' ')
				return( ' ' - *b );
			else	
				++b;
		}
	} else {
		while(b < bend) {
			if(*a == *b)
			{
				++a; ++b; 
			}
			else
				return( *a - *b );
		}
		while(a < aend)	{
			if(*a != ' ')
				return(*a - ' ');
			else	
				++a;
		}
	}
	return(0);
}

/* assign strings:  a = b 
 */
void s_copy(register char *a, register char *b, ftnlen la, ftnlen lb)
{
	register char *aend, *bend;

	aend = a + la;

	if(la <= lb){
		if (a <= b || a >= b + la)
			while(a < aend)
				*a++ = *b++;
		else
			for(b += la; a < aend; )
				*--aend = *--b;
	}else {
		bend = b + lb;
		if (a <= b || a >= bend){
			while(b < bend)
				*a++ = *b++;
		}
		else {
			a += lb;
			while(b < bend)
				*--a = *--bend;
			a += lb;
		}
		while(a < aend)
			*a++ = ' ';
	}
}

/* allocat memory of legth of Len
*/
char *F77_aloc(integer Len, char *whence)
{
	char *rv;
	unsigned int uLen = (unsigned int) Len;	/* for K&R C */

	if (!(rv = (char*)malloc(uLen))) {
		fprintf(stderr, "malloc(%u) failure in %s\n",
			uLen, whence);
		exit_(&memfailure);
	}
	return rv;
}

/* fill string a into string b. if a is ended with blank spaces,
 * get rid of them at first.
 */
VOID g_char(char *a, ftnlen alen, char *b)
{
	char *x = a + alen, *y = b + alen;

	for(;; y--) {
		if (x <= a) {
			*b = 0;
			return;
		}
		if (*--x != ' ')
			break;
	}
	*y-- = 0;
	do *y-- = *x;
		while(x-- > a);
}

/* copy string a to b, and if the length of b is greater than a, then
 * fill b with blank space.
 */
VOID b_char(char *a, char *b, ftnlen blen)
{	
	int i;

	for(i=0;i<blen && *a!=0;i++) 
		*b++= *a++;
	for(;i<blen;i++) 
		*b++=' ';
}

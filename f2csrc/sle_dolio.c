/* Copyright (C) by GFD-Dennou Club, 1999-2000.  All rights reserved. */
#include "libtinyf2c.h"

extern int (*f__lioproc)(ftnint*, char*, ftnlen, ftnint);

/* do input and output  to the external stream with the I/O proc.
*/
integer do_lio(ftnint *type, ftnint *number, char *ptr, ftnlen len)
{
	return((*f__lioproc)(number,ptr,len,*type));
}

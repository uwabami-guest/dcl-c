/* Copyright (C) by GFD-Dennou Club, 1999-2000.  All rights reserved. */
#include "libtinyf2c.h"
#include "fio.h"
#include "fmt.h"

extern char *f__fmtbuf;

extern x_getc(Void);
extern x_endp(Void);
extern x_rev(Void);
extern int xw_end(Void);
extern int xw_rev(Void);

/* check sequential formatted external I/O reading
 */
c_sfe(cilist *a)
{	
	unit *p;
	f__curunit = p = &f__units[a->ciunit];
	if(a->ciunit >= MXUNIT || a->ciunit<0)
		err(a->cierr,101,"startio");
	if(p->ufd==NULL && fk_open(SEQ,FMT,a->ciunit)) 
		err(a->cierr,114,"sfe");
	if(!p->ufmt) 
		err(a->cierr,102,"sfe");
	return(0);
}

/* start sequential formatted external I/O reading
 */
integer s_rsfe(cilist *a) 
{
	int n;
	
	if(!f__init) 
		f_init();
	f__reading=1;
	f__sequential=1;
	f__formatted=1;
	f__external=1;
	if(n=c_sfe(a)) 
		return(n);
	f__elist=a;
	f__cursor=f__recpos=0;
	f__scale=0;
	f__fmtbuf=a->cifmt;
	f__cf=f__curunit->ufd;
	if(pars_f(f__fmtbuf)<0) 
		err(a->cierr,100,"startio");
	f__getn= x_getc;
	f__doed= rd_ed;
	f__doned= rd_ned;
	fmt_bg();
	f__doend=x_endp;
	f__donewrec=xrd_SL;
	f__dorevert=x_rev;
	f__cblank=f__curunit->ublnk;
	f__cplus=0;
	if(f__curunit->uwrt && f__nowreading(f__curunit))
		err(a->cierr,errno,"read start");
	if(f__curunit->uend)
		err(f__elist->ciend,(EOF),"read start");
	return(0);
}

/* end sequential formatted external I/O reading
 */
integer e_rsfe(Void)
{	
	int n;

	n=en_fio();
	f__fmtbuf=NULL;
	return(n);
}

/* start sequential formatted external I/O writing
 */
integer s_wsfe(cilist *a)
{
	int n;
	
	if(!f__init) 
		f_init();
	f__reading=0;
	f__sequential=1;
	f__formatted=1;
	f__external=1;
	if(n=c_sfe(a))
		return(n);
	f__elist=a;
	f__hiwater = f__cursor=f__recpos=0;
	f__nonl = 0;
	f__scale=0;
	f__fmtbuf=a->cifmt;
	f__cf=f__curunit->ufd;
	if(pars_f(f__fmtbuf)<0) 
		err(a->cierr,100,"startio");
	f__putn= x_putc;
	f__doed= w_ed;
	f__doned= w_ned;
	f__doend=xw_end;
	f__dorevert=xw_rev;
	f__donewrec=x_wSL;
	fmt_bg();
	f__cplus=0;
	f__cblank=f__curunit->ublnk;
	if(f__curunit->uwrt != 1 && f__nowwriting(f__curunit))
		err(a->cierr,errno,"write start");
	return(0);
}

/* start sequential formatted external I/O writing
 */
integer e_wsfe(Void)
{
	int n = en_fio();
	f__fmtbuf = NULL;
	return n;
}

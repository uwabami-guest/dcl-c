/* Copyright (C) by GFD-Dennou Club, 1999-2000.  All rights reserved. */
#include "libtinyf2c.h"
#include "fio.h"
#include "fmt.h"
#include "lio.h"
#include "string.h"

extern flag f__lquit;
extern int f__lcount,f__ltype,nml_read;
extern char *f__lchar;
extern double f__lx,f__ly;
extern int l_eof;

extern t_getc(void);
extern int un_getc(int x, FILE *f__cf);
extern double atof();
extern void *malloc(), *realloc();
int (*f__lioproc)(ftnint*, char*, ftnlen, ftnint), (*l_getc)(void),
	(*l_ungetc)(int,FILE*);

/* initial the units and check the current external list file 
*/
int c_le(cilist *a)
{
	if(!f__init)
		f_init();
	f__fmtbuf="list io";
	f__curunit = &f__units[a->ciunit];
	if(a->ciunit>=MXUNIT || a->ciunit<0)
		err(a->cierr,101,"stler");
	f__scale=f__recpos=0;
	f__elist=a;
	if(f__curunit->ufd==NULL && fk_open(SEQ,FMT,a->ciunit))
		err(a->cierr,102,"lio");
	f__cf=f__curunit->ufd;
	if(!f__curunit->ufmt) 
		err(a->cierr,103,"lio")
	
	return(0);
}

/* start the reading from external list file. here set up 
 * the functions for reading.
 */
integer s_rsle(cilist *a)
{
	int n;

	f__reading=1;
	f__external=1;
	f__formatted=1;
	if(n=c_le(a)) 
		return(n);
	f__lioproc = l_read;
	f__lquit = 0;
	f__lcount = 0;
	l_eof = 0;
	if(f__curunit->uwrt && f__nowreading(f__curunit))
		err(a->cierr,errno,"read start");
	if(f__curunit->uend)
		err(f__elist->ciend,(EOF),"read start");
	l_getc = t_getc;
	l_ungetc = un_getc;
	f__doend = xrd_SL;
	return(0);
}

/* end the reading from external list file. 
*/
integer e_rsle(Void)
{
	int ch;
	if(f__curunit->uend) return(0);
	while((ch=t_getc())!='\n')
		if (ch == EOF) {
			if(feof(f__cf))
				f__curunit->uend = l_eof = 1;
			return EOF;
			}
	return(0);
}

/* start the writing to external list file. here set up 
 * the functions for writing.
 */
integer s_wsle(cilist *a)
{
	int n;
	
	if(n=c_le(a)) 
		return(n);
	f__reading=0;
	f__external=1;
	f__formatted=1;
	f__putn = x_putc;
	f__lioproc = l_write;
	L_len = LINE;
	f__donewrec = x_wSL;
	if(f__curunit->uwrt != 1 && f__nowwriting(f__curunit))
		err(a->cierr, errno, "list output start");
	return(0);
}

/* end the writing to external list file. 
*/
integer e_wsle(Void)
{
	int n = f__putbuf('\n');
	f__recpos=0;
#ifdef ALWAYS_FLUSH
	if (!n && fflush(f__cf))
		err(f__elist->cierr, errno, "write end");
#endif
	return(n);
}
